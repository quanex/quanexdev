package dev.ap_transactions_0_1;

import routines.DataOperation;
import routines.TalendDataGenerator;
import routines.DataQuality;
import routines.Relational;
import routines.DataQualityDependencies;
import routines.Mathematical;
import routines.SQLike;
import routines.Numeric;
import routines.TalendStringUtil;
import routines.TalendString;
import routines.DQTechnical;
import routines.StringHandling;
import routines.DataMasking;
import routines.TalendDate;
import routines.DqStringHandling;
import routines.system.*;
import routines.system.api.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.math.BigDecimal;
import java.io.ByteArrayOutputStream;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;
import java.io.IOException;
import java.util.Comparator;

@SuppressWarnings("unused")
/**
 * Job: AP_Transactions Purpose: Export AP Transactions<br>
 * Description:  <br>
 * @author jason.bennett@quanex.com
 * @version 7.1.1.20181026_1147
 * @status 
 */
public class AP_Transactions implements TalendJob {
	static {
		System.setProperty("TalendJob.log", "AP_Transactions.log");
	}
	private static org.apache.log4j.Logger log = org.apache.log4j.Logger
			.getLogger(AP_Transactions.class);

	protected static void logIgnoredError(String message, Throwable cause) {
		log.error(message, cause);

	}

	public final Object obj = new Object();

	// for transmiting parameters purpose
	private Object valueObject = null;

	public Object getValueObject() {
		return this.valueObject;
	}

	public void setValueObject(Object valueObject) {
		this.valueObject = valueObject;
	}

	private final static String defaultCharset = java.nio.charset.Charset
			.defaultCharset().name();

	private final static String utf8Charset = "UTF-8";

	// contains type for every context property
	public class PropertiesWithType extends java.util.Properties {
		private static final long serialVersionUID = 1L;
		private java.util.Map<String, String> propertyTypes = new java.util.HashMap<>();

		public PropertiesWithType(java.util.Properties properties) {
			super(properties);
		}

		public PropertiesWithType() {
			super();
		}

		public void setContextType(String key, String type) {
			propertyTypes.put(key, type);
		}

		public String getContextType(String key) {
			return propertyTypes.get(key);
		}
	}

	// create and load default properties
	private java.util.Properties defaultProps = new java.util.Properties();

	// create application properties with default
	public class ContextProperties extends PropertiesWithType {

		private static final long serialVersionUID = 1L;

		public ContextProperties(java.util.Properties properties) {
			super(properties);
		}

		public ContextProperties() {
			super();
		}

		public void synchronizeContext() {

		}

	}

	protected ContextProperties context = new ContextProperties(); // will be
																	// instanciated
																	// by MS.

	public ContextProperties getContext() {
		return this.context;
	}

	private final String jobVersion = "0.1";
	private final String jobName = "AP_Transactions";
	private final String projectName = "DEV";
	public Integer errorCode = null;
	private String currentComponent = "";

	private final java.util.Map<String, Object> globalMap = new java.util.HashMap<String, Object>();
	private final static java.util.Map<String, Object> junitGlobalMap = new java.util.HashMap<String, Object>();

	private final java.util.Map<String, Long> start_Hash = new java.util.HashMap<String, Long>();
	private final java.util.Map<String, Long> end_Hash = new java.util.HashMap<String, Long>();
	private final java.util.Map<String, Boolean> ok_Hash = new java.util.HashMap<String, Boolean>();
	public final java.util.List<String[]> globalBuffer = new java.util.ArrayList<String[]>();

	private RunStat runStat = new RunStat();

	// OSGi DataSource
	private final static String KEY_DB_DATASOURCES = "KEY_DB_DATASOURCES";

	private final static String KEY_DB_DATASOURCES_RAW = "KEY_DB_DATASOURCES_RAW";

	public void setDataSources(
			java.util.Map<String, javax.sql.DataSource> dataSources) {
		java.util.Map<String, routines.system.TalendDataSource> talendDataSources = new java.util.HashMap<String, routines.system.TalendDataSource>();
		for (java.util.Map.Entry<String, javax.sql.DataSource> dataSourceEntry : dataSources
				.entrySet()) {
			talendDataSources.put(
					dataSourceEntry.getKey(),
					new routines.system.TalendDataSource(dataSourceEntry
							.getValue()));
		}
		globalMap.put(KEY_DB_DATASOURCES, talendDataSources);
		globalMap
				.put(KEY_DB_DATASOURCES_RAW,
						new java.util.HashMap<String, javax.sql.DataSource>(
								dataSources));
	}

	private final java.io.ByteArrayOutputStream baos = new java.io.ByteArrayOutputStream();
	private final java.io.PrintStream errorMessagePS = new java.io.PrintStream(
			new java.io.BufferedOutputStream(baos));

	public String getExceptionStackTrace() {
		if ("failure".equals(this.getStatus())) {
			errorMessagePS.flush();
			return baos.toString();
		}
		return null;
	}

	private Exception exception;

	public Exception getException() {
		if ("failure".equals(this.getStatus())) {
			return this.exception;
		}
		return null;
	}

	private class TalendException extends Exception {

		private static final long serialVersionUID = 1L;

		private java.util.Map<String, Object> globalMap = null;
		private Exception e = null;
		private String currentComponent = null;
		private String virtualComponentName = null;

		public void setVirtualComponentName(String virtualComponentName) {
			this.virtualComponentName = virtualComponentName;
		}

		private TalendException(Exception e, String errorComponent,
				final java.util.Map<String, Object> globalMap) {
			this.currentComponent = errorComponent;
			this.globalMap = globalMap;
			this.e = e;
		}

		public Exception getException() {
			return this.e;
		}

		public String getCurrentComponent() {
			return this.currentComponent;
		}

		public String getExceptionCauseMessage(Exception e) {
			Throwable cause = e;
			String message = null;
			int i = 10;
			while (null != cause && 0 < i--) {
				message = cause.getMessage();
				if (null == message) {
					cause = cause.getCause();
				} else {
					break;
				}
			}
			if (null == message) {
				message = e.getClass().getName();
			}
			return message;
		}

		@Override
		public void printStackTrace() {
			if (!(e instanceof TalendException || e instanceof TDieException)) {
				if (virtualComponentName != null
						&& currentComponent.indexOf(virtualComponentName + "_") == 0) {
					globalMap.put(virtualComponentName + "_ERROR_MESSAGE",
							getExceptionCauseMessage(e));
				}
				globalMap.put(currentComponent + "_ERROR_MESSAGE",
						getExceptionCauseMessage(e));
				System.err.println("Exception in component " + currentComponent
						+ " (" + jobName + ")");
			}
			if (!(e instanceof TDieException)) {
				if (e instanceof TalendException) {
					e.printStackTrace();
				} else {
					e.printStackTrace();
					e.printStackTrace(errorMessagePS);
					AP_Transactions.this.exception = e;
				}
			}
			if (!(e instanceof TalendException)) {
				try {
					for (java.lang.reflect.Method m : this.getClass()
							.getEnclosingClass().getMethods()) {
						if (m.getName().compareTo(currentComponent + "_error") == 0) {
							m.invoke(AP_Transactions.this, new Object[] { e,
									currentComponent, globalMap });
							break;
						}
					}

					if (!(e instanceof TDieException)) {
					}
				} catch (Exception e) {
					this.e.printStackTrace();
				}
			}
		}
	}

	public void tFileInputDelimited_1_error(Exception exception,
			String errorComponent, final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tFileInputDelimited_1_onSubJobError(exception, errorComponent,
				globalMap);
	}

	public void tMap_3_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tFileInputDelimited_1_onSubJobError(exception, errorComponent,
				globalMap);
	}

	public void tDBOutput_3_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tFileInputDelimited_1_onSubJobError(exception, errorComponent,
				globalMap);
	}

	public void tDBSP_1_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tDBSP_1_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tDBInput_1_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tDBInput_1_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tMap_2_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tDBInput_1_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tDBOutput_4_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tDBInput_1_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tDBInput_4_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tDBInput_4_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tMap_1_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tDBInput_4_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tDBOutput_2_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tDBInput_4_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tDBInput_3_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tDBInput_3_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tAdvancedHash_row1_error(Exception exception,
			String errorComponent, final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tDBInput_3_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tFileInputDelimited_1_onSubJobError(Exception exception,
			String errorComponent, final java.util.Map<String, Object> globalMap)
			throws TalendException {

		resumeUtil.addLog("SYSTEM_LOG", "NODE:" + errorComponent, "", Thread
				.currentThread().getId() + "", "FATAL", "",
				exception.getMessage(),
				ResumeUtil.getExceptionStackTrace(exception), "");

	}

	public void tDBSP_1_onSubJobError(Exception exception,
			String errorComponent, final java.util.Map<String, Object> globalMap)
			throws TalendException {

		resumeUtil.addLog("SYSTEM_LOG", "NODE:" + errorComponent, "", Thread
				.currentThread().getId() + "", "FATAL", "",
				exception.getMessage(),
				ResumeUtil.getExceptionStackTrace(exception), "");

	}

	public void tDBInput_1_onSubJobError(Exception exception,
			String errorComponent, final java.util.Map<String, Object> globalMap)
			throws TalendException {

		resumeUtil.addLog("SYSTEM_LOG", "NODE:" + errorComponent, "", Thread
				.currentThread().getId() + "", "FATAL", "",
				exception.getMessage(),
				ResumeUtil.getExceptionStackTrace(exception), "");

	}

	public void tDBInput_4_onSubJobError(Exception exception,
			String errorComponent, final java.util.Map<String, Object> globalMap)
			throws TalendException {

		resumeUtil.addLog("SYSTEM_LOG", "NODE:" + errorComponent, "", Thread
				.currentThread().getId() + "", "FATAL", "",
				exception.getMessage(),
				ResumeUtil.getExceptionStackTrace(exception), "");

	}

	public void tDBInput_3_onSubJobError(Exception exception,
			String errorComponent, final java.util.Map<String, Object> globalMap)
			throws TalendException {

		resumeUtil.addLog("SYSTEM_LOG", "NODE:" + errorComponent, "", Thread
				.currentThread().getId() + "", "FATAL", "",
				exception.getMessage(),
				ResumeUtil.getExceptionStackTrace(exception), "");

	}

	public static class StageDbStruct implements
			routines.system.IPersistableRow<StageDbStruct> {
		final static byte[] commonByteArrayLock_DEV_AP_Transactions = new byte[0];
		static byte[] commonByteArray_DEV_AP_Transactions = new byte[0];

		public String Employee_ID;

		public String getEmployee_ID() {
			return this.Employee_ID;
		}

		public java.util.Date Start_Date;

		public java.util.Date getStart_Date() {
			return this.Start_Date;
		}

		public java.util.Date End_Date;

		public java.util.Date getEnd_Date() {
			return this.End_Date;
		}

		public java.util.Date Process_Date;

		public java.util.Date getProcess_Date() {
			return this.Process_Date;
		}

		public String Approval_Code;

		public String getApproval_Code() {
			return this.Approval_Code;
		}

		public java.util.Date Expense_Date;

		public java.util.Date getExpense_Date() {
			return this.Expense_Date;
		}

		public String GL_Codes;

		public String getGL_Codes() {
			return this.GL_Codes;
		}

		public String Dept_Code;

		public String getDept_Code() {
			return this.Dept_Code;
		}

		public String PSID;

		public String getPSID() {
			return this.PSID;
		}

		public Float Amount;

		public Float getAmount() {
			return this.Amount;
		}

		public Integer Voucher;

		public Integer getVoucher() {
			return this.Voucher;
		}

		private String readString(ObjectInputStream dis) throws IOException {
			String strReturn = null;
			int length = 0;
			length = dis.readInt();
			if (length == -1) {
				strReturn = null;
			} else {
				if (length > commonByteArray_DEV_AP_Transactions.length) {
					if (length < 1024
							&& commonByteArray_DEV_AP_Transactions.length == 0) {
						commonByteArray_DEV_AP_Transactions = new byte[1024];
					} else {
						commonByteArray_DEV_AP_Transactions = new byte[2 * length];
					}
				}
				dis.readFully(commonByteArray_DEV_AP_Transactions, 0, length);
				strReturn = new String(commonByteArray_DEV_AP_Transactions, 0,
						length, utf8Charset);
			}
			return strReturn;
		}

		private void writeString(String str, ObjectOutputStream dos)
				throws IOException {
			if (str == null) {
				dos.writeInt(-1);
			} else {
				byte[] byteArray = str.getBytes(utf8Charset);
				dos.writeInt(byteArray.length);
				dos.write(byteArray);
			}
		}

		private java.util.Date readDate(ObjectInputStream dis)
				throws IOException {
			java.util.Date dateReturn = null;
			int length = 0;
			length = dis.readByte();
			if (length == -1) {
				dateReturn = null;
			} else {
				dateReturn = new Date(dis.readLong());
			}
			return dateReturn;
		}

		private void writeDate(java.util.Date date1, ObjectOutputStream dos)
				throws IOException {
			if (date1 == null) {
				dos.writeByte(-1);
			} else {
				dos.writeByte(0);
				dos.writeLong(date1.getTime());
			}
		}

		private Integer readInteger(ObjectInputStream dis) throws IOException {
			Integer intReturn;
			int length = 0;
			length = dis.readByte();
			if (length == -1) {
				intReturn = null;
			} else {
				intReturn = dis.readInt();
			}
			return intReturn;
		}

		private void writeInteger(Integer intNum, ObjectOutputStream dos)
				throws IOException {
			if (intNum == null) {
				dos.writeByte(-1);
			} else {
				dos.writeByte(0);
				dos.writeInt(intNum);
			}
		}

		public void readData(ObjectInputStream dis) {

			synchronized (commonByteArrayLock_DEV_AP_Transactions) {

				try {

					int length = 0;

					this.Employee_ID = readString(dis);

					this.Start_Date = readDate(dis);

					this.End_Date = readDate(dis);

					this.Process_Date = readDate(dis);

					this.Approval_Code = readString(dis);

					this.Expense_Date = readDate(dis);

					this.GL_Codes = readString(dis);

					this.Dept_Code = readString(dis);

					this.PSID = readString(dis);

					length = dis.readByte();
					if (length == -1) {
						this.Amount = null;
					} else {
						this.Amount = dis.readFloat();
					}

					this.Voucher = readInteger(dis);

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void writeData(ObjectOutputStream dos) {
			try {

				// String

				writeString(this.Employee_ID, dos);

				// java.util.Date

				writeDate(this.Start_Date, dos);

				// java.util.Date

				writeDate(this.End_Date, dos);

				// java.util.Date

				writeDate(this.Process_Date, dos);

				// String

				writeString(this.Approval_Code, dos);

				// java.util.Date

				writeDate(this.Expense_Date, dos);

				// String

				writeString(this.GL_Codes, dos);

				// String

				writeString(this.Dept_Code, dos);

				// String

				writeString(this.PSID, dos);

				// Float

				if (this.Amount == null) {
					dos.writeByte(-1);
				} else {
					dos.writeByte(0);
					dos.writeFloat(this.Amount);
				}

				// Integer

				writeInteger(this.Voucher, dos);

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public String toString() {

			StringBuilder sb = new StringBuilder();
			sb.append(super.toString());
			sb.append("[");
			sb.append("Employee_ID=" + Employee_ID);
			sb.append(",Start_Date=" + String.valueOf(Start_Date));
			sb.append(",End_Date=" + String.valueOf(End_Date));
			sb.append(",Process_Date=" + String.valueOf(Process_Date));
			sb.append(",Approval_Code=" + Approval_Code);
			sb.append(",Expense_Date=" + String.valueOf(Expense_Date));
			sb.append(",GL_Codes=" + GL_Codes);
			sb.append(",Dept_Code=" + Dept_Code);
			sb.append(",PSID=" + PSID);
			sb.append(",Amount=" + String.valueOf(Amount));
			sb.append(",Voucher=" + String.valueOf(Voucher));
			sb.append("]");

			return sb.toString();
		}

		public String toLogString() {
			StringBuilder sb = new StringBuilder();

			if (Employee_ID == null) {
				sb.append("<null>");
			} else {
				sb.append(Employee_ID);
			}

			sb.append("|");

			if (Start_Date == null) {
				sb.append("<null>");
			} else {
				sb.append(Start_Date);
			}

			sb.append("|");

			if (End_Date == null) {
				sb.append("<null>");
			} else {
				sb.append(End_Date);
			}

			sb.append("|");

			if (Process_Date == null) {
				sb.append("<null>");
			} else {
				sb.append(Process_Date);
			}

			sb.append("|");

			if (Approval_Code == null) {
				sb.append("<null>");
			} else {
				sb.append(Approval_Code);
			}

			sb.append("|");

			if (Expense_Date == null) {
				sb.append("<null>");
			} else {
				sb.append(Expense_Date);
			}

			sb.append("|");

			if (GL_Codes == null) {
				sb.append("<null>");
			} else {
				sb.append(GL_Codes);
			}

			sb.append("|");

			if (Dept_Code == null) {
				sb.append("<null>");
			} else {
				sb.append(Dept_Code);
			}

			sb.append("|");

			if (PSID == null) {
				sb.append("<null>");
			} else {
				sb.append(PSID);
			}

			sb.append("|");

			if (Amount == null) {
				sb.append("<null>");
			} else {
				sb.append(Amount);
			}

			sb.append("|");

			if (Voucher == null) {
				sb.append("<null>");
			} else {
				sb.append(Voucher);
			}

			sb.append("|");

			return sb.toString();
		}

		/**
		 * Compare keys
		 */
		public int compareTo(StageDbStruct other) {

			int returnValue = -1;

			return returnValue;
		}

		private int checkNullsAndCompare(Object object1, Object object2) {
			int returnValue = 0;
			if (object1 instanceof Comparable && object2 instanceof Comparable) {
				returnValue = ((Comparable) object1).compareTo(object2);
			} else if (object1 != null && object2 != null) {
				returnValue = compareStrings(object1.toString(),
						object2.toString());
			} else if (object1 == null && object2 != null) {
				returnValue = 1;
			} else if (object1 != null && object2 == null) {
				returnValue = -1;
			} else {
				returnValue = 0;
			}

			return returnValue;
		}

		private int compareStrings(String string1, String string2) {
			return string1.compareTo(string2);
		}

	}

	public static class FileImportStruct implements
			routines.system.IPersistableRow<FileImportStruct> {
		final static byte[] commonByteArrayLock_DEV_AP_Transactions = new byte[0];
		static byte[] commonByteArray_DEV_AP_Transactions = new byte[0];

		public String Employee_ID;

		public String getEmployee_ID() {
			return this.Employee_ID;
		}

		public java.util.Date Start_Date;

		public java.util.Date getStart_Date() {
			return this.Start_Date;
		}

		public java.util.Date End_Date;

		public java.util.Date getEnd_Date() {
			return this.End_Date;
		}

		public java.util.Date Process_Date;

		public java.util.Date getProcess_Date() {
			return this.Process_Date;
		}

		public String Approval_Code;

		public String getApproval_Code() {
			return this.Approval_Code;
		}

		public java.util.Date Expense_Date;

		public java.util.Date getExpense_Date() {
			return this.Expense_Date;
		}

		public String GL_Codes;

		public String getGL_Codes() {
			return this.GL_Codes;
		}

		public String Dept_Code;

		public String getDept_Code() {
			return this.Dept_Code;
		}

		public String PSID;

		public String getPSID() {
			return this.PSID;
		}

		public Float Amount;

		public Float getAmount() {
			return this.Amount;
		}

		private String readString(ObjectInputStream dis) throws IOException {
			String strReturn = null;
			int length = 0;
			length = dis.readInt();
			if (length == -1) {
				strReturn = null;
			} else {
				if (length > commonByteArray_DEV_AP_Transactions.length) {
					if (length < 1024
							&& commonByteArray_DEV_AP_Transactions.length == 0) {
						commonByteArray_DEV_AP_Transactions = new byte[1024];
					} else {
						commonByteArray_DEV_AP_Transactions = new byte[2 * length];
					}
				}
				dis.readFully(commonByteArray_DEV_AP_Transactions, 0, length);
				strReturn = new String(commonByteArray_DEV_AP_Transactions, 0,
						length, utf8Charset);
			}
			return strReturn;
		}

		private void writeString(String str, ObjectOutputStream dos)
				throws IOException {
			if (str == null) {
				dos.writeInt(-1);
			} else {
				byte[] byteArray = str.getBytes(utf8Charset);
				dos.writeInt(byteArray.length);
				dos.write(byteArray);
			}
		}

		private java.util.Date readDate(ObjectInputStream dis)
				throws IOException {
			java.util.Date dateReturn = null;
			int length = 0;
			length = dis.readByte();
			if (length == -1) {
				dateReturn = null;
			} else {
				dateReturn = new Date(dis.readLong());
			}
			return dateReturn;
		}

		private void writeDate(java.util.Date date1, ObjectOutputStream dos)
				throws IOException {
			if (date1 == null) {
				dos.writeByte(-1);
			} else {
				dos.writeByte(0);
				dos.writeLong(date1.getTime());
			}
		}

		public void readData(ObjectInputStream dis) {

			synchronized (commonByteArrayLock_DEV_AP_Transactions) {

				try {

					int length = 0;

					this.Employee_ID = readString(dis);

					this.Start_Date = readDate(dis);

					this.End_Date = readDate(dis);

					this.Process_Date = readDate(dis);

					this.Approval_Code = readString(dis);

					this.Expense_Date = readDate(dis);

					this.GL_Codes = readString(dis);

					this.Dept_Code = readString(dis);

					this.PSID = readString(dis);

					length = dis.readByte();
					if (length == -1) {
						this.Amount = null;
					} else {
						this.Amount = dis.readFloat();
					}

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void writeData(ObjectOutputStream dos) {
			try {

				// String

				writeString(this.Employee_ID, dos);

				// java.util.Date

				writeDate(this.Start_Date, dos);

				// java.util.Date

				writeDate(this.End_Date, dos);

				// java.util.Date

				writeDate(this.Process_Date, dos);

				// String

				writeString(this.Approval_Code, dos);

				// java.util.Date

				writeDate(this.Expense_Date, dos);

				// String

				writeString(this.GL_Codes, dos);

				// String

				writeString(this.Dept_Code, dos);

				// String

				writeString(this.PSID, dos);

				// Float

				if (this.Amount == null) {
					dos.writeByte(-1);
				} else {
					dos.writeByte(0);
					dos.writeFloat(this.Amount);
				}

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public String toString() {

			StringBuilder sb = new StringBuilder();
			sb.append(super.toString());
			sb.append("[");
			sb.append("Employee_ID=" + Employee_ID);
			sb.append(",Start_Date=" + String.valueOf(Start_Date));
			sb.append(",End_Date=" + String.valueOf(End_Date));
			sb.append(",Process_Date=" + String.valueOf(Process_Date));
			sb.append(",Approval_Code=" + Approval_Code);
			sb.append(",Expense_Date=" + String.valueOf(Expense_Date));
			sb.append(",GL_Codes=" + GL_Codes);
			sb.append(",Dept_Code=" + Dept_Code);
			sb.append(",PSID=" + PSID);
			sb.append(",Amount=" + String.valueOf(Amount));
			sb.append("]");

			return sb.toString();
		}

		public String toLogString() {
			StringBuilder sb = new StringBuilder();

			if (Employee_ID == null) {
				sb.append("<null>");
			} else {
				sb.append(Employee_ID);
			}

			sb.append("|");

			if (Start_Date == null) {
				sb.append("<null>");
			} else {
				sb.append(Start_Date);
			}

			sb.append("|");

			if (End_Date == null) {
				sb.append("<null>");
			} else {
				sb.append(End_Date);
			}

			sb.append("|");

			if (Process_Date == null) {
				sb.append("<null>");
			} else {
				sb.append(Process_Date);
			}

			sb.append("|");

			if (Approval_Code == null) {
				sb.append("<null>");
			} else {
				sb.append(Approval_Code);
			}

			sb.append("|");

			if (Expense_Date == null) {
				sb.append("<null>");
			} else {
				sb.append(Expense_Date);
			}

			sb.append("|");

			if (GL_Codes == null) {
				sb.append("<null>");
			} else {
				sb.append(GL_Codes);
			}

			sb.append("|");

			if (Dept_Code == null) {
				sb.append("<null>");
			} else {
				sb.append(Dept_Code);
			}

			sb.append("|");

			if (PSID == null) {
				sb.append("<null>");
			} else {
				sb.append(PSID);
			}

			sb.append("|");

			if (Amount == null) {
				sb.append("<null>");
			} else {
				sb.append(Amount);
			}

			sb.append("|");

			return sb.toString();
		}

		/**
		 * Compare keys
		 */
		public int compareTo(FileImportStruct other) {

			int returnValue = -1;

			return returnValue;
		}

		private int checkNullsAndCompare(Object object1, Object object2) {
			int returnValue = 0;
			if (object1 instanceof Comparable && object2 instanceof Comparable) {
				returnValue = ((Comparable) object1).compareTo(object2);
			} else if (object1 != null && object2 != null) {
				returnValue = compareStrings(object1.toString(),
						object2.toString());
			} else if (object1 == null && object2 != null) {
				returnValue = 1;
			} else if (object1 != null && object2 == null) {
				returnValue = -1;
			} else {
				returnValue = 0;
			}

			return returnValue;
		}

		private int compareStrings(String string1, String string2) {
			return string1.compareTo(string2);
		}

	}

	public static class after_tFileInputDelimited_1Struct implements
			routines.system.IPersistableRow<after_tFileInputDelimited_1Struct> {
		final static byte[] commonByteArrayLock_DEV_AP_Transactions = new byte[0];
		static byte[] commonByteArray_DEV_AP_Transactions = new byte[0];

		public String Employee_ID;

		public String getEmployee_ID() {
			return this.Employee_ID;
		}

		public java.util.Date Start_Date;

		public java.util.Date getStart_Date() {
			return this.Start_Date;
		}

		public java.util.Date End_Date;

		public java.util.Date getEnd_Date() {
			return this.End_Date;
		}

		public java.util.Date Process_Date;

		public java.util.Date getProcess_Date() {
			return this.Process_Date;
		}

		public String Approval_Code;

		public String getApproval_Code() {
			return this.Approval_Code;
		}

		public java.util.Date Expense_Date;

		public java.util.Date getExpense_Date() {
			return this.Expense_Date;
		}

		public String GL_Codes;

		public String getGL_Codes() {
			return this.GL_Codes;
		}

		public String Dept_Code;

		public String getDept_Code() {
			return this.Dept_Code;
		}

		public String PSID;

		public String getPSID() {
			return this.PSID;
		}

		public Float Amount;

		public Float getAmount() {
			return this.Amount;
		}

		private String readString(ObjectInputStream dis) throws IOException {
			String strReturn = null;
			int length = 0;
			length = dis.readInt();
			if (length == -1) {
				strReturn = null;
			} else {
				if (length > commonByteArray_DEV_AP_Transactions.length) {
					if (length < 1024
							&& commonByteArray_DEV_AP_Transactions.length == 0) {
						commonByteArray_DEV_AP_Transactions = new byte[1024];
					} else {
						commonByteArray_DEV_AP_Transactions = new byte[2 * length];
					}
				}
				dis.readFully(commonByteArray_DEV_AP_Transactions, 0, length);
				strReturn = new String(commonByteArray_DEV_AP_Transactions, 0,
						length, utf8Charset);
			}
			return strReturn;
		}

		private void writeString(String str, ObjectOutputStream dos)
				throws IOException {
			if (str == null) {
				dos.writeInt(-1);
			} else {
				byte[] byteArray = str.getBytes(utf8Charset);
				dos.writeInt(byteArray.length);
				dos.write(byteArray);
			}
		}

		private java.util.Date readDate(ObjectInputStream dis)
				throws IOException {
			java.util.Date dateReturn = null;
			int length = 0;
			length = dis.readByte();
			if (length == -1) {
				dateReturn = null;
			} else {
				dateReturn = new Date(dis.readLong());
			}
			return dateReturn;
		}

		private void writeDate(java.util.Date date1, ObjectOutputStream dos)
				throws IOException {
			if (date1 == null) {
				dos.writeByte(-1);
			} else {
				dos.writeByte(0);
				dos.writeLong(date1.getTime());
			}
		}

		public void readData(ObjectInputStream dis) {

			synchronized (commonByteArrayLock_DEV_AP_Transactions) {

				try {

					int length = 0;

					this.Employee_ID = readString(dis);

					this.Start_Date = readDate(dis);

					this.End_Date = readDate(dis);

					this.Process_Date = readDate(dis);

					this.Approval_Code = readString(dis);

					this.Expense_Date = readDate(dis);

					this.GL_Codes = readString(dis);

					this.Dept_Code = readString(dis);

					this.PSID = readString(dis);

					length = dis.readByte();
					if (length == -1) {
						this.Amount = null;
					} else {
						this.Amount = dis.readFloat();
					}

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void writeData(ObjectOutputStream dos) {
			try {

				// String

				writeString(this.Employee_ID, dos);

				// java.util.Date

				writeDate(this.Start_Date, dos);

				// java.util.Date

				writeDate(this.End_Date, dos);

				// java.util.Date

				writeDate(this.Process_Date, dos);

				// String

				writeString(this.Approval_Code, dos);

				// java.util.Date

				writeDate(this.Expense_Date, dos);

				// String

				writeString(this.GL_Codes, dos);

				// String

				writeString(this.Dept_Code, dos);

				// String

				writeString(this.PSID, dos);

				// Float

				if (this.Amount == null) {
					dos.writeByte(-1);
				} else {
					dos.writeByte(0);
					dos.writeFloat(this.Amount);
				}

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public String toString() {

			StringBuilder sb = new StringBuilder();
			sb.append(super.toString());
			sb.append("[");
			sb.append("Employee_ID=" + Employee_ID);
			sb.append(",Start_Date=" + String.valueOf(Start_Date));
			sb.append(",End_Date=" + String.valueOf(End_Date));
			sb.append(",Process_Date=" + String.valueOf(Process_Date));
			sb.append(",Approval_Code=" + Approval_Code);
			sb.append(",Expense_Date=" + String.valueOf(Expense_Date));
			sb.append(",GL_Codes=" + GL_Codes);
			sb.append(",Dept_Code=" + Dept_Code);
			sb.append(",PSID=" + PSID);
			sb.append(",Amount=" + String.valueOf(Amount));
			sb.append("]");

			return sb.toString();
		}

		public String toLogString() {
			StringBuilder sb = new StringBuilder();

			if (Employee_ID == null) {
				sb.append("<null>");
			} else {
				sb.append(Employee_ID);
			}

			sb.append("|");

			if (Start_Date == null) {
				sb.append("<null>");
			} else {
				sb.append(Start_Date);
			}

			sb.append("|");

			if (End_Date == null) {
				sb.append("<null>");
			} else {
				sb.append(End_Date);
			}

			sb.append("|");

			if (Process_Date == null) {
				sb.append("<null>");
			} else {
				sb.append(Process_Date);
			}

			sb.append("|");

			if (Approval_Code == null) {
				sb.append("<null>");
			} else {
				sb.append(Approval_Code);
			}

			sb.append("|");

			if (Expense_Date == null) {
				sb.append("<null>");
			} else {
				sb.append(Expense_Date);
			}

			sb.append("|");

			if (GL_Codes == null) {
				sb.append("<null>");
			} else {
				sb.append(GL_Codes);
			}

			sb.append("|");

			if (Dept_Code == null) {
				sb.append("<null>");
			} else {
				sb.append(Dept_Code);
			}

			sb.append("|");

			if (PSID == null) {
				sb.append("<null>");
			} else {
				sb.append(PSID);
			}

			sb.append("|");

			if (Amount == null) {
				sb.append("<null>");
			} else {
				sb.append(Amount);
			}

			sb.append("|");

			return sb.toString();
		}

		/**
		 * Compare keys
		 */
		public int compareTo(after_tFileInputDelimited_1Struct other) {

			int returnValue = -1;

			return returnValue;
		}

		private int checkNullsAndCompare(Object object1, Object object2) {
			int returnValue = 0;
			if (object1 instanceof Comparable && object2 instanceof Comparable) {
				returnValue = ((Comparable) object1).compareTo(object2);
			} else if (object1 != null && object2 != null) {
				returnValue = compareStrings(object1.toString(),
						object2.toString());
			} else if (object1 == null && object2 != null) {
				returnValue = 1;
			} else if (object1 != null && object2 == null) {
				returnValue = -1;
			} else {
				returnValue = 0;
			}

			return returnValue;
		}

		private int compareStrings(String string1, String string2) {
			return string1.compareTo(string2);
		}

	}

	public void tFileInputDelimited_1Process(
			final java.util.Map<String, Object> globalMap)
			throws TalendException {
		globalMap.put("tFileInputDelimited_1_SUBPROCESS_STATE", 0);

		final boolean execStat = this.execStat;

		String iterateId = "";

		String currentComponent = "";
		java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

		try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception()
						.getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { // start the resume
				globalResumeTicket = true;

				tDBInput_3Process(globalMap);

				FileImportStruct FileImport = new FileImportStruct();
				StageDbStruct StageDb = new StageDbStruct();

				/**
				 * [tDBOutput_3 begin ] start
				 */

				ok_Hash.put("tDBOutput_3", false);
				start_Hash.put("tDBOutput_3", System.currentTimeMillis());

				currentComponent = "tDBOutput_3";

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null) {

						runStat.updateStatOnConnection("StageDb" + iterateId,
								0, 0);

					}
				}

				int tos_count_tDBOutput_3 = 0;

				if (log.isDebugEnabled())
					log.debug("tDBOutput_3 - " + ("Start to work."));
				if (log.isDebugEnabled()) {
					class BytesLimit65535_tDBOutput_3 {
						public void limitLog4jByte() throws Exception {
							StringBuilder log4jParamters_tDBOutput_3 = new StringBuilder();
							log4jParamters_tDBOutput_3.append("Parameters:");
							log4jParamters_tDBOutput_3
									.append("USE_EXISTING_CONNECTION" + " = "
											+ "false");
							log4jParamters_tDBOutput_3.append(" | ");
							log4jParamters_tDBOutput_3.append("DRIVER" + " = "
									+ "JTDS");
							log4jParamters_tDBOutput_3.append(" | ");
							log4jParamters_tDBOutput_3.append("HOST" + " = "
									+ "\"NSMWQNXINFDB02\"");
							log4jParamters_tDBOutput_3.append(" | ");
							log4jParamters_tDBOutput_3.append("PORT" + " = "
									+ "\"1433\"");
							log4jParamters_tDBOutput_3.append(" | ");
							log4jParamters_tDBOutput_3.append("DB_SCHEMA"
									+ " = " + "\"\"");
							log4jParamters_tDBOutput_3.append(" | ");
							log4jParamters_tDBOutput_3.append("DBNAME" + " = "
									+ "\"Talend_Certify\"");
							log4jParamters_tDBOutput_3.append(" | ");
							log4jParamters_tDBOutput_3.append("USER" + " = "
									+ "\"sa\"");
							log4jParamters_tDBOutput_3.append(" | ");
							log4jParamters_tDBOutput_3.append("PASS"
									+ " = "
									+ String.valueOf(
											"bf98e2c82ba433c4880df380fbb39fe9")
											.substring(0, 4) + "...");
							log4jParamters_tDBOutput_3.append(" | ");
							log4jParamters_tDBOutput_3.append("TABLE" + " = "
									+ "\"CSI_9030_IN\"");
							log4jParamters_tDBOutput_3.append(" | ");
							log4jParamters_tDBOutput_3.append("TABLE_ACTION"
									+ " = " + "NONE");
							log4jParamters_tDBOutput_3.append(" | ");
							log4jParamters_tDBOutput_3.append("IDENTITY_INSERT"
									+ " = " + "false");
							log4jParamters_tDBOutput_3.append(" | ");
							log4jParamters_tDBOutput_3.append("DATA_ACTION"
									+ " = " + "INSERT");
							log4jParamters_tDBOutput_3.append(" | ");
							log4jParamters_tDBOutput_3
									.append("SPECIFY_DATASOURCE_ALIAS" + " = "
											+ "false");
							log4jParamters_tDBOutput_3.append(" | ");
							log4jParamters_tDBOutput_3.append("DIE_ON_ERROR"
									+ " = " + "false");
							log4jParamters_tDBOutput_3.append(" | ");
							log4jParamters_tDBOutput_3.append("PROPERTIES"
									+ " = " + "\"\"");
							log4jParamters_tDBOutput_3.append(" | ");
							log4jParamters_tDBOutput_3.append("COMMIT_EVERY"
									+ " = " + "10000");
							log4jParamters_tDBOutput_3.append(" | ");
							log4jParamters_tDBOutput_3.append("ADD_COLS"
									+ " = " + "[]");
							log4jParamters_tDBOutput_3.append(" | ");
							log4jParamters_tDBOutput_3
									.append("USE_FIELD_OPTIONS" + " = "
											+ "false");
							log4jParamters_tDBOutput_3.append(" | ");
							log4jParamters_tDBOutput_3
									.append("IGNORE_DATE_OUTOF_RANGE" + " = "
											+ "false");
							log4jParamters_tDBOutput_3.append(" | ");
							log4jParamters_tDBOutput_3
									.append("ENABLE_DEBUG_MODE" + " = "
											+ "false");
							log4jParamters_tDBOutput_3.append(" | ");
							log4jParamters_tDBOutput_3
									.append("SUPPORT_NULL_WHERE" + " = "
											+ "false");
							log4jParamters_tDBOutput_3.append(" | ");
							log4jParamters_tDBOutput_3.append("USE_BATCH_SIZE"
									+ " = " + "true");
							log4jParamters_tDBOutput_3.append(" | ");
							log4jParamters_tDBOutput_3.append("BATCH_SIZE"
									+ " = " + "10000");
							log4jParamters_tDBOutput_3.append(" | ");
							log4jParamters_tDBOutput_3
									.append("UNIFIED_COMPONENTS" + " = "
											+ "tMSSqlOutput");
							log4jParamters_tDBOutput_3.append(" | ");
							if (log.isDebugEnabled())
								log.debug("tDBOutput_3 - "
										+ (log4jParamters_tDBOutput_3));
						}
					}
					new BytesLimit65535_tDBOutput_3().limitLog4jByte();
				}

				int nb_line_tDBOutput_3 = 0;
				int nb_line_update_tDBOutput_3 = 0;
				int nb_line_inserted_tDBOutput_3 = 0;
				int nb_line_deleted_tDBOutput_3 = 0;
				int nb_line_rejected_tDBOutput_3 = 0;

				int deletedCount_tDBOutput_3 = 0;
				int updatedCount_tDBOutput_3 = 0;
				int insertedCount_tDBOutput_3 = 0;
				int rejectedCount_tDBOutput_3 = 0;
				String dbschema_tDBOutput_3 = null;
				String tableName_tDBOutput_3 = null;
				boolean whetherReject_tDBOutput_3 = false;

				java.util.Calendar calendar_tDBOutput_3 = java.util.Calendar
						.getInstance();
				long year1_tDBOutput_3 = TalendDate.parseDate("yyyy-MM-dd",
						"0001-01-01").getTime();
				long year2_tDBOutput_3 = TalendDate.parseDate("yyyy-MM-dd",
						"1753-01-01").getTime();
				long year10000_tDBOutput_3 = TalendDate.parseDate(
						"yyyy-MM-dd HH:mm:ss", "9999-12-31 24:00:00").getTime();
				long date_tDBOutput_3;

				java.util.Calendar calendar_datetimeoffset_tDBOutput_3 = java.util.Calendar
						.getInstance(java.util.TimeZone.getTimeZone("UTC"));

				java.sql.Connection conn_tDBOutput_3 = null;
				String dbUser_tDBOutput_3 = null;
				dbschema_tDBOutput_3 = "";
				String driverClass_tDBOutput_3 = "net.sourceforge.jtds.jdbc.Driver";

				if (log.isDebugEnabled())
					log.debug("tDBOutput_3 - " + ("Driver ClassName: ")
							+ (driverClass_tDBOutput_3) + ("."));
				java.lang.Class.forName(driverClass_tDBOutput_3);
				String port_tDBOutput_3 = "1433";
				String dbname_tDBOutput_3 = "Talend_Certify";
				String url_tDBOutput_3 = "jdbc:jtds:sqlserver://"
						+ "NSMWQNXINFDB02";
				if (!"".equals(port_tDBOutput_3)) {
					url_tDBOutput_3 += ":" + "1433";
				}
				if (!"".equals(dbname_tDBOutput_3)) {
					url_tDBOutput_3 += "//" + "Talend_Certify";

				}
				url_tDBOutput_3 += ";appName=" + projectName + ";" + "";
				dbUser_tDBOutput_3 = "sa";

				final String decryptedPassword_tDBOutput_3 = routines.system.PasswordEncryptUtil
						.decryptPassword("bf98e2c82ba433c4880df380fbb39fe9");

				String dbPwd_tDBOutput_3 = decryptedPassword_tDBOutput_3;
				if (log.isDebugEnabled())
					log.debug("tDBOutput_3 - " + ("Connection attempts to '")
							+ (url_tDBOutput_3) + ("' with the username '")
							+ (dbUser_tDBOutput_3) + ("'."));
				conn_tDBOutput_3 = java.sql.DriverManager.getConnection(
						url_tDBOutput_3, dbUser_tDBOutput_3, dbPwd_tDBOutput_3);
				if (log.isDebugEnabled())
					log.debug("tDBOutput_3 - " + ("Connection to '")
							+ (url_tDBOutput_3) + ("' has succeeded."));

				resourceMap.put("conn_tDBOutput_3", conn_tDBOutput_3);

				conn_tDBOutput_3.setAutoCommit(false);
				int commitEvery_tDBOutput_3 = 10000;
				int commitCounter_tDBOutput_3 = 0;

				if (log.isDebugEnabled())
					log.debug("tDBOutput_3 - "
							+ ("Connection is set auto commit to '")
							+ (conn_tDBOutput_3.getAutoCommit()) + ("'."));
				int batchSize_tDBOutput_3 = 10000;
				int batchSizeCounter_tDBOutput_3 = 0;

				if (dbschema_tDBOutput_3 == null
						|| dbschema_tDBOutput_3.trim().length() == 0) {
					tableName_tDBOutput_3 = "CSI_9030_IN";
				} else {
					tableName_tDBOutput_3 = dbschema_tDBOutput_3 + "].["
							+ "CSI_9030_IN";
				}
				int count_tDBOutput_3 = 0;

				String insert_tDBOutput_3 = "INSERT INTO ["
						+ tableName_tDBOutput_3
						+ "] ([Employee_ID],[Start_Date],[End_Date],[Process_Date],[Approval_Code],[Expense_Date],[GL_Codes],[Dept_Code],[PSID],[Amount],[Voucher]) VALUES (?,?,?,?,?,?,?,?,?,?,?)";
				java.sql.PreparedStatement pstmt_tDBOutput_3 = conn_tDBOutput_3
						.prepareStatement(insert_tDBOutput_3);
				resourceMap.put("pstmt_tDBOutput_3", pstmt_tDBOutput_3);

				/**
				 * [tDBOutput_3 begin ] stop
				 */

				/**
				 * [tMap_3 begin ] start
				 */

				ok_Hash.put("tMap_3", false);
				start_Hash.put("tMap_3", System.currentTimeMillis());

				currentComponent = "tMap_3";

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null) {

						runStat.updateStatOnConnection(
								"FileImport" + iterateId, 0, 0);

					}
				}

				int tos_count_tMap_3 = 0;

				if (log.isDebugEnabled())
					log.debug("tMap_3 - " + ("Start to work."));
				if (log.isDebugEnabled()) {
					class BytesLimit65535_tMap_3 {
						public void limitLog4jByte() throws Exception {
							StringBuilder log4jParamters_tMap_3 = new StringBuilder();
							log4jParamters_tMap_3.append("Parameters:");
							log4jParamters_tMap_3.append("LINK_STYLE" + " = "
									+ "AUTO");
							log4jParamters_tMap_3.append(" | ");
							log4jParamters_tMap_3
									.append("TEMPORARY_DATA_DIRECTORY" + " = "
											+ "");
							log4jParamters_tMap_3.append(" | ");
							log4jParamters_tMap_3.append("ROWS_BUFFER_SIZE"
									+ " = " + "2000000");
							log4jParamters_tMap_3.append(" | ");
							log4jParamters_tMap_3
									.append("CHANGE_HASH_AND_EQUALS_FOR_BIGDECIMAL"
											+ " = " + "true");
							log4jParamters_tMap_3.append(" | ");
							if (log.isDebugEnabled())
								log.debug("tMap_3 - " + (log4jParamters_tMap_3));
						}
					}
					new BytesLimit65535_tMap_3().limitLog4jByte();
				}

				// ###############################
				// # Lookup's keys initialization
				int count_FileImport_tMap_3 = 0;

				int count_row1_tMap_3 = 0;

				org.talend.designer.components.lookup.memory.AdvancedMemoryLookup<row1Struct> tHash_Lookup_row1 = (org.talend.designer.components.lookup.memory.AdvancedMemoryLookup<row1Struct>) ((org.talend.designer.components.lookup.memory.AdvancedMemoryLookup<row1Struct>) globalMap
						.get("tHash_Lookup_row1"));

				tHash_Lookup_row1.initGet();

				row1Struct row1HashKey = new row1Struct();
				row1Struct row1Default = new row1Struct();
				// ###############################

				// ###############################
				// # Vars initialization
				class Var__tMap_3__Struct {
				}
				Var__tMap_3__Struct Var__tMap_3 = new Var__tMap_3__Struct();
				// ###############################

				// ###############################
				// # Outputs initialization
				int count_StageDb_tMap_3 = 0;

				StageDbStruct StageDb_tmp = new StageDbStruct();
				// ###############################

				/**
				 * [tMap_3 begin ] stop
				 */

				/**
				 * [tFileInputDelimited_1 begin ] start
				 */

				ok_Hash.put("tFileInputDelimited_1", false);
				start_Hash.put("tFileInputDelimited_1",
						System.currentTimeMillis());

				currentComponent = "tFileInputDelimited_1";

				int tos_count_tFileInputDelimited_1 = 0;

				if (log.isDebugEnabled())
					log.debug("tFileInputDelimited_1 - " + ("Start to work."));
				if (log.isDebugEnabled()) {
					class BytesLimit65535_tFileInputDelimited_1 {
						public void limitLog4jByte() throws Exception {
							StringBuilder log4jParamters_tFileInputDelimited_1 = new StringBuilder();
							log4jParamters_tFileInputDelimited_1
									.append("Parameters:");
							log4jParamters_tFileInputDelimited_1
									.append("FILENAME"
											+ " = "
											+ "\"C:/Users/jbennett/Downloads/StudentFiles/APImport_01032018.csv\"");
							log4jParamters_tFileInputDelimited_1.append(" | ");
							log4jParamters_tFileInputDelimited_1
									.append("CSV_OPTION" + " = " + "false");
							log4jParamters_tFileInputDelimited_1.append(" | ");
							log4jParamters_tFileInputDelimited_1
									.append("ROWSEPARATOR" + " = " + "\"\\n\"");
							log4jParamters_tFileInputDelimited_1.append(" | ");
							log4jParamters_tFileInputDelimited_1
									.append("FIELDSEPARATOR" + " = " + "\",\"");
							log4jParamters_tFileInputDelimited_1.append(" | ");
							log4jParamters_tFileInputDelimited_1
									.append("HEADER" + " = " + "1");
							log4jParamters_tFileInputDelimited_1.append(" | ");
							log4jParamters_tFileInputDelimited_1
									.append("FOOTER" + " = " + "0");
							log4jParamters_tFileInputDelimited_1.append(" | ");
							log4jParamters_tFileInputDelimited_1.append("LIMIT"
									+ " = " + "");
							log4jParamters_tFileInputDelimited_1.append(" | ");
							log4jParamters_tFileInputDelimited_1
									.append("REMOVE_EMPTY_ROW" + " = "
											+ "false");
							log4jParamters_tFileInputDelimited_1.append(" | ");
							log4jParamters_tFileInputDelimited_1
									.append("UNCOMPRESS" + " = " + "false");
							log4jParamters_tFileInputDelimited_1.append(" | ");
							log4jParamters_tFileInputDelimited_1
									.append("DIE_ON_ERROR" + " = " + "false");
							log4jParamters_tFileInputDelimited_1.append(" | ");
							log4jParamters_tFileInputDelimited_1
									.append("ADVANCED_SEPARATOR" + " = "
											+ "false");
							log4jParamters_tFileInputDelimited_1.append(" | ");
							log4jParamters_tFileInputDelimited_1
									.append("RANDOM" + " = " + "false");
							log4jParamters_tFileInputDelimited_1.append(" | ");
							log4jParamters_tFileInputDelimited_1
									.append("TRIMALL" + " = " + "true");
							log4jParamters_tFileInputDelimited_1.append(" | ");
							log4jParamters_tFileInputDelimited_1
									.append("CHECK_FIELDS_NUM" + " = "
											+ "false");
							log4jParamters_tFileInputDelimited_1.append(" | ");
							log4jParamters_tFileInputDelimited_1
									.append("CHECK_DATE" + " = " + "false");
							log4jParamters_tFileInputDelimited_1.append(" | ");
							log4jParamters_tFileInputDelimited_1
									.append("ENCODING" + " = " + "\"US-ASCII\"");
							log4jParamters_tFileInputDelimited_1.append(" | ");
							log4jParamters_tFileInputDelimited_1
									.append("SPLITRECORD" + " = " + "false");
							log4jParamters_tFileInputDelimited_1.append(" | ");
							log4jParamters_tFileInputDelimited_1
									.append("ENABLE_DECODE" + " = " + "false");
							log4jParamters_tFileInputDelimited_1.append(" | ");
							if (log.isDebugEnabled())
								log.debug("tFileInputDelimited_1 - "
										+ (log4jParamters_tFileInputDelimited_1));
						}
					}
					new BytesLimit65535_tFileInputDelimited_1()
							.limitLog4jByte();
				}

				final routines.system.RowState rowstate_tFileInputDelimited_1 = new routines.system.RowState();

				int nb_line_tFileInputDelimited_1 = 0;
				org.talend.fileprocess.FileInputDelimited fid_tFileInputDelimited_1 = null;
				try {

					Object filename_tFileInputDelimited_1 = "C:/Users/jbennett/Downloads/StudentFiles/APImport_01032018.csv";
					if (filename_tFileInputDelimited_1 instanceof java.io.InputStream) {

						int footer_value_tFileInputDelimited_1 = 0, random_value_tFileInputDelimited_1 = -1;
						if (footer_value_tFileInputDelimited_1 > 0
								|| random_value_tFileInputDelimited_1 > 0) {
							throw new java.lang.Exception(
									"When the input source is a stream,footer and random shouldn't be bigger than 0.");
						}

					}
					try {
						fid_tFileInputDelimited_1 = new org.talend.fileprocess.FileInputDelimited(
								"C:/Users/jbennett/Downloads/StudentFiles/APImport_01032018.csv",
								"US-ASCII", ",", "\n", false, 1, 0, -1, -1,
								false);
					} catch (java.lang.Exception e) {

						log.error("tFileInputDelimited_1 - " + e.getMessage());

						System.err.println(e.getMessage());

					}

					log.info("tFileInputDelimited_1 - Retrieving records from the datasource.");

					while (fid_tFileInputDelimited_1 != null
							&& fid_tFileInputDelimited_1.nextRecord()) {
						rowstate_tFileInputDelimited_1.reset();

						FileImport = null;

						boolean whetherReject_tFileInputDelimited_1 = false;
						FileImport = new FileImportStruct();
						try {

							int columnIndexWithD_tFileInputDelimited_1 = 0;

							String temp = "";

							columnIndexWithD_tFileInputDelimited_1 = 0;

							FileImport.Employee_ID = fid_tFileInputDelimited_1
									.get(columnIndexWithD_tFileInputDelimited_1)
									.trim();

							columnIndexWithD_tFileInputDelimited_1 = 1;

							temp = fid_tFileInputDelimited_1.get(
									columnIndexWithD_tFileInputDelimited_1)
									.trim();
							if (temp.length() > 0) {

								try {

									FileImport.Start_Date = ParserUtils
											.parseTo_Date(temp, "mm/dd/yyyy");

								} catch (java.lang.Exception ex_tFileInputDelimited_1) {
									rowstate_tFileInputDelimited_1
											.setException(new RuntimeException(
													String.format(
															"Couldn't parse value for column '%s' in '%s', value is '%s'. Details: %s",
															"Start_Date",
															"FileImport", temp,
															ex_tFileInputDelimited_1),
													ex_tFileInputDelimited_1));
								}

							} else {

								FileImport.Start_Date = null;

							}

							columnIndexWithD_tFileInputDelimited_1 = 2;

							temp = fid_tFileInputDelimited_1.get(
									columnIndexWithD_tFileInputDelimited_1)
									.trim();
							if (temp.length() > 0) {

								try {

									FileImport.End_Date = ParserUtils
											.parseTo_Date(temp, "mm/dd/yyyy");

								} catch (java.lang.Exception ex_tFileInputDelimited_1) {
									rowstate_tFileInputDelimited_1
											.setException(new RuntimeException(
													String.format(
															"Couldn't parse value for column '%s' in '%s', value is '%s'. Details: %s",
															"End_Date",
															"FileImport", temp,
															ex_tFileInputDelimited_1),
													ex_tFileInputDelimited_1));
								}

							} else {

								FileImport.End_Date = null;

							}

							columnIndexWithD_tFileInputDelimited_1 = 3;

							temp = fid_tFileInputDelimited_1.get(
									columnIndexWithD_tFileInputDelimited_1)
									.trim();
							if (temp.length() > 0) {

								try {

									FileImport.Process_Date = ParserUtils
											.parseTo_Date(temp, "mm/dd/yyyy");

								} catch (java.lang.Exception ex_tFileInputDelimited_1) {
									rowstate_tFileInputDelimited_1
											.setException(new RuntimeException(
													String.format(
															"Couldn't parse value for column '%s' in '%s', value is '%s'. Details: %s",
															"Process_Date",
															"FileImport", temp,
															ex_tFileInputDelimited_1),
													ex_tFileInputDelimited_1));
								}

							} else {

								FileImport.Process_Date = null;

							}

							columnIndexWithD_tFileInputDelimited_1 = 4;

							FileImport.Approval_Code = fid_tFileInputDelimited_1
									.get(columnIndexWithD_tFileInputDelimited_1)
									.trim();

							columnIndexWithD_tFileInputDelimited_1 = 5;

							temp = fid_tFileInputDelimited_1.get(
									columnIndexWithD_tFileInputDelimited_1)
									.trim();
							if (temp.length() > 0) {

								try {

									FileImport.Expense_Date = ParserUtils
											.parseTo_Date(temp, "mm/dd/yyyy");

								} catch (java.lang.Exception ex_tFileInputDelimited_1) {
									rowstate_tFileInputDelimited_1
											.setException(new RuntimeException(
													String.format(
															"Couldn't parse value for column '%s' in '%s', value is '%s'. Details: %s",
															"Expense_Date",
															"FileImport", temp,
															ex_tFileInputDelimited_1),
													ex_tFileInputDelimited_1));
								}

							} else {

								FileImport.Expense_Date = null;

							}

							columnIndexWithD_tFileInputDelimited_1 = 6;

							FileImport.GL_Codes = fid_tFileInputDelimited_1
									.get(columnIndexWithD_tFileInputDelimited_1)
									.trim();

							columnIndexWithD_tFileInputDelimited_1 = 7;

							FileImport.Dept_Code = fid_tFileInputDelimited_1
									.get(columnIndexWithD_tFileInputDelimited_1)
									.trim();

							columnIndexWithD_tFileInputDelimited_1 = 8;

							FileImport.PSID = fid_tFileInputDelimited_1.get(
									columnIndexWithD_tFileInputDelimited_1)
									.trim();

							columnIndexWithD_tFileInputDelimited_1 = 9;

							temp = fid_tFileInputDelimited_1.get(
									columnIndexWithD_tFileInputDelimited_1)
									.trim();
							if (temp.length() > 0) {

								try {

									FileImport.Amount = ParserUtils
											.parseTo_Float(temp);

								} catch (java.lang.Exception ex_tFileInputDelimited_1) {
									rowstate_tFileInputDelimited_1
											.setException(new RuntimeException(
													String.format(
															"Couldn't parse value for column '%s' in '%s', value is '%s'. Details: %s",
															"Amount",
															"FileImport", temp,
															ex_tFileInputDelimited_1),
													ex_tFileInputDelimited_1));
								}

							} else {

								FileImport.Amount = null;

							}

							if (rowstate_tFileInputDelimited_1.getException() != null) {
								throw rowstate_tFileInputDelimited_1
										.getException();
							}

						} catch (java.lang.Exception e) {
							whetherReject_tFileInputDelimited_1 = true;

							log.error("tFileInputDelimited_1 - "
									+ e.getMessage());

							System.err.println(e.getMessage());
							FileImport = null;

						}

						log.debug("tFileInputDelimited_1 - Retrieving the record "
								+ fid_tFileInputDelimited_1.getRowNumber()
								+ ".");

						/**
						 * [tFileInputDelimited_1 begin ] stop
						 */

						/**
						 * [tFileInputDelimited_1 main ] start
						 */

						currentComponent = "tFileInputDelimited_1";

						tos_count_tFileInputDelimited_1++;

						/**
						 * [tFileInputDelimited_1 main ] stop
						 */

						/**
						 * [tFileInputDelimited_1 process_data_begin ] start
						 */

						currentComponent = "tFileInputDelimited_1";

						/**
						 * [tFileInputDelimited_1 process_data_begin ] stop
						 */
						// Start of branch "FileImport"
						if (FileImport != null) {

							/**
							 * [tMap_3 main ] start
							 */

							currentComponent = "tMap_3";

							// FileImport
							// FileImport

							if (execStat) {
								runStat.updateStatOnConnection("FileImport"
										+ iterateId, 1, 1);
							}

							if (log.isTraceEnabled()) {
								log.trace("FileImport - "
										+ (FileImport == null ? "" : FileImport
												.toLogString()));
							}

							boolean hasCasePrimitiveKeyWithNull_tMap_3 = false;

							// ###############################
							// # Input tables (lookups)
							boolean rejectedInnerJoin_tMap_3 = false;
							boolean mainRowRejected_tMap_3 = false;

							// /////////////////////////////////////////////
							// Starting Lookup Table "row1"
							// /////////////////////////////////////////////

							boolean forceLooprow1 = false;

							row1Struct row1ObjectFromLookup = null;

							if (!rejectedInnerJoin_tMap_3) { // G_TM_M_020

								tHash_Lookup_row1.lookup(row1HashKey);

								if (!tHash_Lookup_row1.hasNext()) { // G_TM_M_090

									forceLooprow1 = true;

								} // G_TM_M_090

							} // G_TM_M_020

							else { // G 20 - G 21
								forceLooprow1 = true;
							} // G 21

							row1Struct row1 = null;

							while ((tHash_Lookup_row1 != null && tHash_Lookup_row1
									.hasNext()) || forceLooprow1) { // G_TM_M_043

								// CALL close loop of lookup 'row1'

								row1Struct fromLookup_row1 = null;
								row1 = row1Default;

								if (!forceLooprow1) { // G 46

									fromLookup_row1 = tHash_Lookup_row1.next();

									if (fromLookup_row1 != null) {
										row1 = fromLookup_row1;
									}

								} // G 46

								forceLooprow1 = false;

								// ###############################
								{ // start of Var scope

									// ###############################
									// # Vars tables

									Var__tMap_3__Struct Var = Var__tMap_3;// ###############################
									// ###############################
									// # Output tables

									StageDb = null;

									// # Output table : 'StageDb'
									count_StageDb_tMap_3++;

									StageDb_tmp.Employee_ID = FileImport.Employee_ID;
									StageDb_tmp.Start_Date = FileImport.Start_Date;
									StageDb_tmp.End_Date = FileImport.End_Date;
									StageDb_tmp.Process_Date = FileImport.Process_Date;
									StageDb_tmp.Approval_Code = FileImport.Approval_Code;
									StageDb_tmp.Expense_Date = FileImport.Expense_Date;
									StageDb_tmp.GL_Codes = FileImport.GL_Codes;
									StageDb_tmp.Dept_Code = FileImport.Dept_Code;
									StageDb_tmp.PSID = FileImport.PSID;
									StageDb_tmp.Amount = FileImport.Amount;
									StageDb_tmp.Voucher = row1.maxvoucher;
									StageDb = StageDb_tmp;
									log.debug("tMap_3 - Outputting the record "
											+ count_StageDb_tMap_3
											+ " of the output table 'StageDb'.");

									// ###############################

								} // end of Var scope

								rejectedInnerJoin_tMap_3 = false;

								tos_count_tMap_3++;

								/**
								 * [tMap_3 main ] stop
								 */

								/**
								 * [tMap_3 process_data_begin ] start
								 */

								currentComponent = "tMap_3";

								/**
								 * [tMap_3 process_data_begin ] stop
								 */
								// Start of branch "StageDb"
								if (StageDb != null) {

									/**
									 * [tDBOutput_3 main ] start
									 */

									currentComponent = "tDBOutput_3";

									// StageDb
									// StageDb

									if (execStat) {
										runStat.updateStatOnConnection(
												"StageDb" + iterateId, 1, 1);
									}

									if (log.isTraceEnabled()) {
										log.trace("StageDb - "
												+ (StageDb == null ? ""
														: StageDb.toLogString()));
									}

									whetherReject_tDBOutput_3 = false;
									if (StageDb.Employee_ID == null) {
										pstmt_tDBOutput_3.setNull(1,
												java.sql.Types.VARCHAR);
									} else {
										pstmt_tDBOutput_3.setString(1,
												StageDb.Employee_ID);
									}

									if (StageDb.Start_Date != null) {
										pstmt_tDBOutput_3.setTimestamp(
												2,
												new java.sql.Timestamp(
														StageDb.Start_Date
																.getTime()));
									} else {
										pstmt_tDBOutput_3.setNull(2,
												java.sql.Types.DATE);
									}

									if (StageDb.End_Date != null) {
										pstmt_tDBOutput_3.setTimestamp(
												3,
												new java.sql.Timestamp(
														StageDb.End_Date
																.getTime()));
									} else {
										pstmt_tDBOutput_3.setNull(3,
												java.sql.Types.DATE);
									}

									if (StageDb.Process_Date != null) {
										pstmt_tDBOutput_3.setTimestamp(
												4,
												new java.sql.Timestamp(
														StageDb.Process_Date
																.getTime()));
									} else {
										pstmt_tDBOutput_3.setNull(4,
												java.sql.Types.DATE);
									}

									if (StageDb.Approval_Code == null) {
										pstmt_tDBOutput_3.setNull(5,
												java.sql.Types.VARCHAR);
									} else {
										pstmt_tDBOutput_3.setString(5,
												StageDb.Approval_Code);
									}

									if (StageDb.Expense_Date != null) {
										pstmt_tDBOutput_3.setTimestamp(
												6,
												new java.sql.Timestamp(
														StageDb.Expense_Date
																.getTime()));
									} else {
										pstmt_tDBOutput_3.setNull(6,
												java.sql.Types.DATE);
									}

									if (StageDb.GL_Codes == null) {
										pstmt_tDBOutput_3.setNull(7,
												java.sql.Types.VARCHAR);
									} else {
										pstmt_tDBOutput_3.setString(7,
												StageDb.GL_Codes);
									}

									if (StageDb.Dept_Code == null) {
										pstmt_tDBOutput_3.setNull(8,
												java.sql.Types.VARCHAR);
									} else {
										pstmt_tDBOutput_3.setString(8,
												StageDb.Dept_Code);
									}

									if (StageDb.PSID == null) {
										pstmt_tDBOutput_3.setNull(9,
												java.sql.Types.VARCHAR);
									} else {
										pstmt_tDBOutput_3.setString(9,
												StageDb.PSID);
									}

									if (StageDb.Amount == null) {
										pstmt_tDBOutput_3.setNull(10,
												java.sql.Types.FLOAT);
									} else {
										pstmt_tDBOutput_3.setFloat(10,
												StageDb.Amount);
									}

									if (StageDb.Voucher == null) {
										pstmt_tDBOutput_3.setNull(11,
												java.sql.Types.INTEGER);
									} else {
										pstmt_tDBOutput_3.setInt(11,
												StageDb.Voucher);
									}

									pstmt_tDBOutput_3.addBatch();
									nb_line_tDBOutput_3++;

									if (log.isDebugEnabled())
										log.debug("tDBOutput_3 - "
												+ ("Adding the record ")
												+ (nb_line_tDBOutput_3)
												+ (" to the ") + ("INSERT")
												+ (" batch."));
									batchSizeCounter_tDBOutput_3++;

									if (!whetherReject_tDBOutput_3) {
									}
									// ////////batch execute by batch
									// size///////
									class LimitBytesHelper_tDBOutput_3 {
										public int limitBytePart1(
												int counter,
												java.sql.PreparedStatement pstmt_tDBOutput_3)
												throws Exception {
											try {

												if (log.isDebugEnabled())
													log.debug("tDBOutput_3 - "
															+ ("Executing the ")
															+ ("INSERT")
															+ (" batch."));
												for (int countEach_tDBOutput_3 : pstmt_tDBOutput_3
														.executeBatch()) {
													if (countEach_tDBOutput_3 == -2
															|| countEach_tDBOutput_3 == -3) {
														break;
													}
													counter += countEach_tDBOutput_3;
												}

												if (log.isDebugEnabled())
													log.debug("tDBOutput_3 - "
															+ ("The ")
															+ ("INSERT")
															+ (" batch execution has succeeded."));
											} catch (java.sql.BatchUpdateException e) {

												int countSum_tDBOutput_3 = 0;
												for (int countEach_tDBOutput_3 : e
														.getUpdateCounts()) {
													counter += (countEach_tDBOutput_3 < 0 ? 0
															: countEach_tDBOutput_3);
												}

												log.error("tDBOutput_3 - "
														+ (e.getMessage()));
												System.err.println(e
														.getMessage());

											}
											return counter;
										}

										public int limitBytePart2(
												int counter,
												java.sql.PreparedStatement pstmt_tDBOutput_3)
												throws Exception {
											try {

												if (log.isDebugEnabled())
													log.debug("tDBOutput_3 - "
															+ ("Executing the ")
															+ ("INSERT")
															+ (" batch."));
												for (int countEach_tDBOutput_3 : pstmt_tDBOutput_3
														.executeBatch()) {
													if (countEach_tDBOutput_3 == -2
															|| countEach_tDBOutput_3 == -3) {
														break;
													}
													counter += countEach_tDBOutput_3;
												}

												if (log.isDebugEnabled())
													log.debug("tDBOutput_3 - "
															+ ("The ")
															+ ("INSERT")
															+ (" batch execution has succeeded."));
											} catch (java.sql.BatchUpdateException e) {

												for (int countEach_tDBOutput_3 : e
														.getUpdateCounts()) {
													counter += (countEach_tDBOutput_3 < 0 ? 0
															: countEach_tDBOutput_3);
												}

												log.error("tDBOutput_3 - "
														+ (e.getMessage()));
												System.err.println(e
														.getMessage());

											}
											return counter;
										}
									}
									if ((batchSize_tDBOutput_3 > 0)
											&& (batchSize_tDBOutput_3 <= batchSizeCounter_tDBOutput_3)) {

										insertedCount_tDBOutput_3 = new LimitBytesHelper_tDBOutput_3()
												.limitBytePart1(
														insertedCount_tDBOutput_3,
														pstmt_tDBOutput_3);

										batchSizeCounter_tDBOutput_3 = 0;
									}

									// //////////commit every////////////

									commitCounter_tDBOutput_3++;
									if (commitEvery_tDBOutput_3 <= commitCounter_tDBOutput_3) {
										if ((batchSize_tDBOutput_3 > 0)
												&& (batchSizeCounter_tDBOutput_3 > 0)) {

											insertedCount_tDBOutput_3 = new LimitBytesHelper_tDBOutput_3()
													.limitBytePart1(
															insertedCount_tDBOutput_3,
															pstmt_tDBOutput_3);

											batchSizeCounter_tDBOutput_3 = 0;
										}

										if (log.isDebugEnabled())
											log.debug("tDBOutput_3 - "
													+ ("Connection starting to commit ")
													+ (commitCounter_tDBOutput_3)
													+ (" record(s)."));
										conn_tDBOutput_3.commit();

										if (log.isDebugEnabled())
											log.debug("tDBOutput_3 - "
													+ ("Connection commit has succeeded."));
										commitCounter_tDBOutput_3 = 0;
									}

									tos_count_tDBOutput_3++;

									/**
									 * [tDBOutput_3 main ] stop
									 */

									/**
									 * [tDBOutput_3 process_data_begin ] start
									 */

									currentComponent = "tDBOutput_3";

									/**
									 * [tDBOutput_3 process_data_begin ] stop
									 */

									/**
									 * [tDBOutput_3 process_data_end ] start
									 */

									currentComponent = "tDBOutput_3";

									/**
									 * [tDBOutput_3 process_data_end ] stop
									 */

								} // End of branch "StageDb"

							} // close loop of lookup 'row1' // G_TM_M_043

							/**
							 * [tMap_3 process_data_end ] start
							 */

							currentComponent = "tMap_3";

							/**
							 * [tMap_3 process_data_end ] stop
							 */

						} // End of branch "FileImport"

						/**
						 * [tFileInputDelimited_1 process_data_end ] start
						 */

						currentComponent = "tFileInputDelimited_1";

						/**
						 * [tFileInputDelimited_1 process_data_end ] stop
						 */

						/**
						 * [tFileInputDelimited_1 end ] start
						 */

						currentComponent = "tFileInputDelimited_1";

					}
				} finally {
					if (!((Object) ("C:/Users/jbennett/Downloads/StudentFiles/APImport_01032018.csv") instanceof java.io.InputStream)) {
						if (fid_tFileInputDelimited_1 != null) {
							fid_tFileInputDelimited_1.close();
						}
					}
					if (fid_tFileInputDelimited_1 != null) {
						globalMap.put("tFileInputDelimited_1_NB_LINE",
								fid_tFileInputDelimited_1.getRowNumber());

						log.info("tFileInputDelimited_1 - Retrieved records count: "
								+ fid_tFileInputDelimited_1.getRowNumber()
								+ ".");

					}
				}

				if (log.isDebugEnabled())
					log.debug("tFileInputDelimited_1 - " + ("Done."));

				ok_Hash.put("tFileInputDelimited_1", true);
				end_Hash.put("tFileInputDelimited_1",
						System.currentTimeMillis());

				/**
				 * [tFileInputDelimited_1 end ] stop
				 */

				/**
				 * [tMap_3 end ] start
				 */

				currentComponent = "tMap_3";

				// ###############################
				// # Lookup hashes releasing
				if (tHash_Lookup_row1 != null) {
					tHash_Lookup_row1.endGet();
				}
				globalMap.remove("tHash_Lookup_row1");

				// ###############################
				log.debug("tMap_3 - Written records count in the table 'StageDb': "
						+ count_StageDb_tMap_3 + ".");

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null
							|| !((Boolean) resourceMap.get("inIterateVComp"))) {
						runStat.updateStatOnConnection(
								"FileImport" + iterateId, 2, 0);
					}
				}

				if (log.isDebugEnabled())
					log.debug("tMap_3 - " + ("Done."));

				ok_Hash.put("tMap_3", true);
				end_Hash.put("tMap_3", System.currentTimeMillis());

				/**
				 * [tMap_3 end ] stop
				 */

				/**
				 * [tDBOutput_3 end ] start
				 */

				currentComponent = "tDBOutput_3";

				try {
					int countSum_tDBOutput_3 = 0;
					if (pstmt_tDBOutput_3 != null
							&& batchSizeCounter_tDBOutput_3 > 0) {

						if (log.isDebugEnabled())
							log.debug("tDBOutput_3 - " + ("Executing the ")
									+ ("INSERT") + (" batch."));
						for (int countEach_tDBOutput_3 : pstmt_tDBOutput_3
								.executeBatch()) {
							if (countEach_tDBOutput_3 == -2
									|| countEach_tDBOutput_3 == -3) {
								break;
							}
							countSum_tDBOutput_3 += countEach_tDBOutput_3;
						}

						if (log.isDebugEnabled())
							log.debug("tDBOutput_3 - " + ("The ") + ("INSERT")
									+ (" batch execution has succeeded."));
					}

					insertedCount_tDBOutput_3 += countSum_tDBOutput_3;

				} catch (java.sql.BatchUpdateException e) {

					int countSum_tDBOutput_3 = 0;
					for (int countEach_tDBOutput_3 : e.getUpdateCounts()) {
						countSum_tDBOutput_3 += (countEach_tDBOutput_3 < 0 ? 0
								: countEach_tDBOutput_3);
					}

					insertedCount_tDBOutput_3 += countSum_tDBOutput_3;

					log.error("tDBOutput_3 - " + (e.getMessage()));
					System.err.println(e.getMessage());

				}
				if (pstmt_tDBOutput_3 != null) {

					pstmt_tDBOutput_3.close();
					resourceMap.remove("pstmt_tDBOutput_3");

				}
				resourceMap.put("statementClosed_tDBOutput_3", true);
				if (log.isDebugEnabled())
					log.debug("tDBOutput_3 - "
							+ ("Connection starting to commit ")
							+ (commitCounter_tDBOutput_3) + (" record(s)."));
				conn_tDBOutput_3.commit();

				if (log.isDebugEnabled())
					log.debug("tDBOutput_3 - "
							+ ("Connection commit has succeeded."));
				if (log.isDebugEnabled())
					log.debug("tDBOutput_3 - "
							+ ("Closing the connection to the database."));
				conn_tDBOutput_3.close();
				if (log.isDebugEnabled())
					log.debug("tDBOutput_3 - "
							+ ("Connection to the database has closed."));
				resourceMap.put("finish_tDBOutput_3", true);

				nb_line_deleted_tDBOutput_3 = nb_line_deleted_tDBOutput_3
						+ deletedCount_tDBOutput_3;
				nb_line_update_tDBOutput_3 = nb_line_update_tDBOutput_3
						+ updatedCount_tDBOutput_3;
				nb_line_inserted_tDBOutput_3 = nb_line_inserted_tDBOutput_3
						+ insertedCount_tDBOutput_3;
				nb_line_rejected_tDBOutput_3 = nb_line_rejected_tDBOutput_3
						+ rejectedCount_tDBOutput_3;

				globalMap.put("tDBOutput_3_NB_LINE", nb_line_tDBOutput_3);
				globalMap.put("tDBOutput_3_NB_LINE_UPDATED",
						nb_line_update_tDBOutput_3);
				globalMap.put("tDBOutput_3_NB_LINE_INSERTED",
						nb_line_inserted_tDBOutput_3);
				globalMap.put("tDBOutput_3_NB_LINE_DELETED",
						nb_line_deleted_tDBOutput_3);
				globalMap.put("tDBOutput_3_NB_LINE_REJECTED",
						nb_line_rejected_tDBOutput_3);

				if (log.isDebugEnabled())
					log.debug("tDBOutput_3 - " + ("Has ") + ("inserted")
							+ (" ") + (nb_line_inserted_tDBOutput_3)
							+ (" record(s)."));

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null
							|| !((Boolean) resourceMap.get("inIterateVComp"))) {
						runStat.updateStatOnConnection("StageDb" + iterateId,
								2, 0);
					}
				}

				if (log.isDebugEnabled())
					log.debug("tDBOutput_3 - " + ("Done."));

				ok_Hash.put("tDBOutput_3", true);
				end_Hash.put("tDBOutput_3", System.currentTimeMillis());

				if (execStat) {
					runStat.updateStatOnConnection("OnComponentOk1", 0, "ok");
				}
				tDBSP_1Process(globalMap);

				/**
				 * [tDBOutput_3 end ] stop
				 */

			}// end the resume

		} catch (java.lang.Exception e) {

			if (!(e instanceof TalendException)) {
				log.fatal(currentComponent + " " + e.getMessage(), e);
			}

			TalendException te = new TalendException(e, currentComponent,
					globalMap);

			throw te;
		} catch (java.lang.Error error) {

			runStat.stopThreadStat();

			throw error;
		} finally {

			// free memory for "tMap_3"
			globalMap.remove("tHash_Lookup_row1");

			try {

				/**
				 * [tFileInputDelimited_1 finally ] start
				 */

				currentComponent = "tFileInputDelimited_1";

				/**
				 * [tFileInputDelimited_1 finally ] stop
				 */

				/**
				 * [tMap_3 finally ] start
				 */

				currentComponent = "tMap_3";

				/**
				 * [tMap_3 finally ] stop
				 */

				/**
				 * [tDBOutput_3 finally ] start
				 */

				currentComponent = "tDBOutput_3";

				try {
					if (resourceMap.get("statementClosed_tDBOutput_3") == null) {
						java.sql.PreparedStatement pstmtToClose_tDBOutput_3 = null;
						if ((pstmtToClose_tDBOutput_3 = (java.sql.PreparedStatement) resourceMap
								.remove("pstmt_tDBOutput_3")) != null) {
							pstmtToClose_tDBOutput_3.close();
						}
					}
				} finally {
					if (resourceMap.get("finish_tDBOutput_3") == null) {
						java.sql.Connection ctn_tDBOutput_3 = null;
						if ((ctn_tDBOutput_3 = (java.sql.Connection) resourceMap
								.get("conn_tDBOutput_3")) != null) {
							try {
								if (log.isDebugEnabled())
									log.debug("tDBOutput_3 - "
											+ ("Closing the connection to the database."));
								ctn_tDBOutput_3.close();
								if (log.isDebugEnabled())
									log.debug("tDBOutput_3 - "
											+ ("Connection to the database has closed."));
							} catch (java.sql.SQLException sqlEx_tDBOutput_3) {
								String errorMessage_tDBOutput_3 = "failed to close the connection in tDBOutput_3 :"
										+ sqlEx_tDBOutput_3.getMessage();
								log.error("tDBOutput_3 - "
										+ (errorMessage_tDBOutput_3));
								System.err.println(errorMessage_tDBOutput_3);
							}
						}
					}
				}

				/**
				 * [tDBOutput_3 finally ] stop
				 */

			} catch (java.lang.Exception e) {
				// ignore
			} catch (java.lang.Error error) {
				// ignore
			}
			resourceMap = null;
		}

		globalMap.put("tFileInputDelimited_1_SUBPROCESS_STATE", 1);
	}

	public void tDBSP_1Process(final java.util.Map<String, Object> globalMap)
			throws TalendException {
		globalMap.put("tDBSP_1_SUBPROCESS_STATE", 0);

		final boolean execStat = this.execStat;

		String iterateId = "";

		String currentComponent = "";
		java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

		try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception()
						.getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { // start the resume
				globalResumeTicket = true;

				/**
				 * [tDBSP_1 begin ] start
				 */

				ok_Hash.put("tDBSP_1", false);
				start_Hash.put("tDBSP_1", System.currentTimeMillis());

				currentComponent = "tDBSP_1";

				int tos_count_tDBSP_1 = 0;

				String dbschema_tDBSP_1 = "";
				java.sql.Connection conn_tDBSP_1 = null;
				String dbUser_tDBSP_1 = null;
				dbschema_tDBSP_1 = "dbo";
				String driverClass_tDBSP_1 = "net.sourceforge.jtds.jdbc.Driver";

				java.lang.Class.forName(driverClass_tDBSP_1);
				String port_tDBSP_1 = "1433";
				String dbname_tDBSP_1 = "Talend_Certify";
				String url_tDBSP_1 = "jdbc:jtds:sqlserver://"
						+ "NSMWQNXINFDB02";
				if (!"".equals(port_tDBSP_1)) {
					url_tDBSP_1 += ":" + "1433";
				}
				if (!"".equals(dbname_tDBSP_1)) {
					url_tDBSP_1 += "//" + "Talend_Certify";

				}
				url_tDBSP_1 += ";appName=" + projectName + ";" + "";
				dbUser_tDBSP_1 = "sa";

				final String decryptedPassword_tDBSP_1 = routines.system.PasswordEncryptUtil
						.decryptPassword("bf98e2c82ba433c4880df380fbb39fe9");

				String dbPwd_tDBSP_1 = decryptedPassword_tDBSP_1;
				conn_tDBSP_1 = java.sql.DriverManager.getConnection(
						url_tDBSP_1, dbUser_tDBSP_1, dbPwd_tDBSP_1);

				// java.sql.Statement stmt_tDBSP_1 =
				// conn_tDBSP_1.createStatement();

				// stmt_tDBSP_1.execute("SET NOCOUNT ON");
				String spSchema_tDBSP_1 = "";
				if (dbschema_tDBSP_1 != null
						&& !dbschema_tDBSP_1.trim().isEmpty()) {
					spSchema_tDBSP_1 = "[" + dbschema_tDBSP_1 + "].";
				}
				java.sql.CallableStatement statement_tDBSP_1 = conn_tDBSP_1
						.prepareCall("{call " + spSchema_tDBSP_1
								+ "QBP_HO_APP_Transform" + "()}");

				java.sql.Timestamp tmpDate_tDBSP_1;
				String tmpString_tDBSP_1;

				/**
				 * [tDBSP_1 begin ] stop
				 */

				/**
				 * [tDBSP_1 main ] start
				 */

				currentComponent = "tDBSP_1";

				statement_tDBSP_1.execute();
				while (statement_tDBSP_1.getMoreResults()
						|| (statement_tDBSP_1.getUpdateCount() != -1)) {
					// Do nothing. "getMoreResults()" would call method do error
					// check.
				}

				tos_count_tDBSP_1++;

				/**
				 * [tDBSP_1 main ] stop
				 */

				/**
				 * [tDBSP_1 process_data_begin ] start
				 */

				currentComponent = "tDBSP_1";

				/**
				 * [tDBSP_1 process_data_begin ] stop
				 */

				/**
				 * [tDBSP_1 process_data_end ] start
				 */

				currentComponent = "tDBSP_1";

				/**
				 * [tDBSP_1 process_data_end ] stop
				 */

				/**
				 * [tDBSP_1 end ] start
				 */

				currentComponent = "tDBSP_1";

				// stmt_tDBSP_1.execute("SET NOCOUNT OFF");

				// stmt_tDBSP_1.close();

				statement_tDBSP_1.close();
				conn_tDBSP_1.close();

				ok_Hash.put("tDBSP_1", true);
				end_Hash.put("tDBSP_1", System.currentTimeMillis());

				/**
				 * [tDBSP_1 end ] stop
				 */
			}// end the resume

			if (resumeEntryMethodName == null || globalResumeTicket) {
				resumeUtil.addLog("CHECKPOINT",
						"CONNECTION:SUBJOB_OK:tDBSP_1:OnSubjobOk1", "", Thread
								.currentThread().getId() + "", "", "", "", "",
						"");
			}

			if (execStat) {
				runStat.updateStatOnConnection("OnSubjobOk1", 0, "ok");
			}

			tDBInput_1Process(globalMap);

			if (resumeEntryMethodName == null || globalResumeTicket) {
				resumeUtil.addLog("CHECKPOINT",
						"CONNECTION:SUBJOB_OK:tDBSP_1:OnSubjobOk2", "", Thread
								.currentThread().getId() + "", "", "", "", "",
						"");
			}

			if (execStat) {
				runStat.updateStatOnConnection("OnSubjobOk2", 0, "ok");
			}

			tDBInput_4Process(globalMap);

		} catch (java.lang.Exception e) {

			if (!(e instanceof TalendException)) {
				log.fatal(currentComponent + " " + e.getMessage(), e);
			}

			TalendException te = new TalendException(e, currentComponent,
					globalMap);

			throw te;
		} catch (java.lang.Error error) {

			runStat.stopThreadStat();

			throw error;
		} finally {

			try {

				/**
				 * [tDBSP_1 finally ] start
				 */

				currentComponent = "tDBSP_1";

				/**
				 * [tDBSP_1 finally ] stop
				 */
			} catch (java.lang.Exception e) {
				// ignore
			} catch (java.lang.Error error) {
				// ignore
			}
			resourceMap = null;
		}

		globalMap.put("tDBSP_1_SUBPROCESS_STATE", 1);
	}

	public static class voucherStruct implements
			routines.system.IPersistableRow<voucherStruct> {
		final static byte[] commonByteArrayLock_DEV_AP_Transactions = new byte[0];
		static byte[] commonByteArray_DEV_AP_Transactions = new byte[0];
		protected static final int DEFAULT_HASHCODE = 1;
		protected static final int PRIME = 31;
		protected int hashCode = DEFAULT_HASHCODE;
		public boolean hashCodeDirty = true;

		public String loopKey;

		public String site_ref;

		public String getSite_ref() {
			return this.site_ref;
		}

		public String vend_num;

		public String getVend_num() {
			return this.vend_num;
		}

		public int voucher;

		public int getVoucher() {
			return this.voucher;
		}

		public String type;

		public String getType() {
			return this.type;
		}

		public java.util.Date dist_date;

		public java.util.Date getDist_date() {
			return this.dist_date;
		}

		public String po_num;

		public String getPo_num() {
			return this.po_num;
		}

		public String inv_num;

		public String getInv_num() {
			return this.inv_num;
		}

		public java.util.Date inv_date;

		public java.util.Date getInv_date() {
			return this.inv_date;
		}

		public Float inv_amt;

		public Float getInv_amt() {
			return this.inv_amt;
		}

		public Float non_disc_amt;

		public Float getNon_disc_amt() {
			return this.non_disc_amt;
		}

		public Integer due_days;

		public Integer getDue_days() {
			return this.due_days;
		}

		public java.util.Date due_date;

		public java.util.Date getDue_date() {
			return this.due_date;
		}

		public Integer disc_days;

		public Integer getDisc_days() {
			return this.disc_days;
		}

		public java.util.Date disc_date;

		public java.util.Date getDisc_date() {
			return this.disc_date;
		}

		public Integer disc_pct;

		public Integer getDisc_pct() {
			return this.disc_pct;
		}

		public Float disc_amt;

		public Float getDisc_amt() {
			return this.disc_amt;
		}

		public String ap_acct;

		public String getAp_acct() {
			return this.ap_acct;
		}

		public String ref;

		public String getRef() {
			return this.ref;
		}

		public Byte post_from_po;

		public Byte getPost_from_po() {
			return this.post_from_po;
		}

		public String txt;

		public String getTxt() {
			return this.txt;
		}

		public Integer prox_day;

		public Integer getProx_day() {
			return this.prox_day;
		}

		public Float exch_rate;

		public Float getExch_rate() {
			return this.exch_rate;
		}

		public Integer includes_tax;

		public Integer getIncludes_tax() {
			return this.includes_tax;
		}

		public Float purch_amt;

		public Float getPurch_amt() {
			return this.purch_amt;
		}

		public Float misc_charges;

		public Float getMisc_charges() {
			return this.misc_charges;
		}

		public Float sales_tax;

		public Float getSales_tax() {
			return this.sales_tax;
		}

		public Float sales_tax_2;

		public Float getSales_tax_2() {
			return this.sales_tax_2;
		}

		public Float freight;

		public Float getFreight() {
			return this.freight;
		}

		public Float duty_amt;

		public Float getDuty_amt() {
			return this.duty_amt;
		}

		public Float brokerage_amt;

		public Float getBrokerage_amt() {
			return this.brokerage_amt;
		}

		public String tax_code1;

		public String getTax_code1() {
			return this.tax_code1;
		}

		public String tax_code2;

		public String getTax_code2() {
			return this.tax_code2;
		}

		public String ap_acct_unit1;

		public String getAp_acct_unit1() {
			return this.ap_acct_unit1;
		}

		public String ap_acct_unit2;

		public String getAp_acct_unit2() {
			return this.ap_acct_unit2;
		}

		public String ap_acct_unit3;

		public String getAp_acct_unit3() {
			return this.ap_acct_unit3;
		}

		public String ap_acct_unit4;

		public String getAp_acct_unit4() {
			return this.ap_acct_unit4;
		}

		public String auth_status;

		public String getAuth_status() {
			return this.auth_status;
		}

		public Short fixed_rate;

		public Short getFixed_rate() {
			return this.fixed_rate;
		}

		public Short prox_code;

		public Short getProx_code() {
			return this.prox_code;
		}

		public String grn_num;

		public String getGrn_num() {
			return this.grn_num;
		}

		public Integer pre_register;

		public Integer getPre_register() {
			return this.pre_register;
		}

		public String authorizer;

		public String getAuthorizer() {
			return this.authorizer;
		}

		public Float insurance_amt;

		public Float getInsurance_amt() {
			return this.insurance_amt;
		}

		public Float loc_frt_amt;

		public Float getLoc_frt_amt() {
			return this.loc_frt_amt;
		}

		public Byte include_tax_in_cost;

		public Byte getInclude_tax_in_cost() {
			return this.include_tax_in_cost;
		}

		public java.util.Date tax_date;

		public java.util.Date getTax_date() {
			return this.tax_date;
		}

		public String builder_po_orig_site;

		public String getBuilder_po_orig_site() {
			return this.builder_po_orig_site;
		}

		public String builder_po_num;

		public String getBuilder_po_num() {
			return this.builder_po_num;
		}

		public String builder_voucher_orig_site;

		public String getBuilder_voucher_orig_site() {
			return this.builder_voucher_orig_site;
		}

		public String builder_voucher;

		public String getBuilder_voucher() {
			return this.builder_voucher;
		}

		public Byte auto_vouchered;

		public Byte getAuto_vouchered() {
			return this.auto_vouchered;
		}

		public Byte cancellation;

		public Byte getCancellation() {
			return this.cancellation;
		}

		public String fiscal_rpt_system_type;

		public String getFiscal_rpt_system_type() {
			return this.fiscal_rpt_system_type;
		}

		@Override
		public int hashCode() {
			if (this.hashCodeDirty) {
				final int prime = PRIME;
				int result = DEFAULT_HASHCODE;

				result = prime
						* result
						+ ((this.site_ref == null) ? 0 : this.site_ref
								.hashCode());

				result = prime
						* result
						+ ((this.vend_num == null) ? 0 : this.vend_num
								.hashCode());

				result = prime * result + (int) this.voucher;

				this.hashCode = result;
				this.hashCodeDirty = false;
			}
			return this.hashCode;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			final voucherStruct other = (voucherStruct) obj;

			if (this.site_ref == null) {
				if (other.site_ref != null)
					return false;

			} else if (!this.site_ref.equals(other.site_ref))

				return false;

			if (this.vend_num == null) {
				if (other.vend_num != null)
					return false;

			} else if (!this.vend_num.equals(other.vend_num))

				return false;

			if (this.voucher != other.voucher)
				return false;

			return true;
		}

		public void copyDataTo(voucherStruct other) {

			other.site_ref = this.site_ref;
			other.vend_num = this.vend_num;
			other.voucher = this.voucher;
			other.type = this.type;
			other.dist_date = this.dist_date;
			other.po_num = this.po_num;
			other.inv_num = this.inv_num;
			other.inv_date = this.inv_date;
			other.inv_amt = this.inv_amt;
			other.non_disc_amt = this.non_disc_amt;
			other.due_days = this.due_days;
			other.due_date = this.due_date;
			other.disc_days = this.disc_days;
			other.disc_date = this.disc_date;
			other.disc_pct = this.disc_pct;
			other.disc_amt = this.disc_amt;
			other.ap_acct = this.ap_acct;
			other.ref = this.ref;
			other.post_from_po = this.post_from_po;
			other.txt = this.txt;
			other.prox_day = this.prox_day;
			other.exch_rate = this.exch_rate;
			other.includes_tax = this.includes_tax;
			other.purch_amt = this.purch_amt;
			other.misc_charges = this.misc_charges;
			other.sales_tax = this.sales_tax;
			other.sales_tax_2 = this.sales_tax_2;
			other.freight = this.freight;
			other.duty_amt = this.duty_amt;
			other.brokerage_amt = this.brokerage_amt;
			other.tax_code1 = this.tax_code1;
			other.tax_code2 = this.tax_code2;
			other.ap_acct_unit1 = this.ap_acct_unit1;
			other.ap_acct_unit2 = this.ap_acct_unit2;
			other.ap_acct_unit3 = this.ap_acct_unit3;
			other.ap_acct_unit4 = this.ap_acct_unit4;
			other.auth_status = this.auth_status;
			other.fixed_rate = this.fixed_rate;
			other.prox_code = this.prox_code;
			other.grn_num = this.grn_num;
			other.pre_register = this.pre_register;
			other.authorizer = this.authorizer;
			other.insurance_amt = this.insurance_amt;
			other.loc_frt_amt = this.loc_frt_amt;
			other.include_tax_in_cost = this.include_tax_in_cost;
			other.tax_date = this.tax_date;
			other.builder_po_orig_site = this.builder_po_orig_site;
			other.builder_po_num = this.builder_po_num;
			other.builder_voucher_orig_site = this.builder_voucher_orig_site;
			other.builder_voucher = this.builder_voucher;
			other.auto_vouchered = this.auto_vouchered;
			other.cancellation = this.cancellation;
			other.fiscal_rpt_system_type = this.fiscal_rpt_system_type;

		}

		public void copyKeysDataTo(voucherStruct other) {

			other.site_ref = this.site_ref;
			other.vend_num = this.vend_num;
			other.voucher = this.voucher;

		}

		private String readString(ObjectInputStream dis) throws IOException {
			String strReturn = null;
			int length = 0;
			length = dis.readInt();
			if (length == -1) {
				strReturn = null;
			} else {
				if (length > commonByteArray_DEV_AP_Transactions.length) {
					if (length < 1024
							&& commonByteArray_DEV_AP_Transactions.length == 0) {
						commonByteArray_DEV_AP_Transactions = new byte[1024];
					} else {
						commonByteArray_DEV_AP_Transactions = new byte[2 * length];
					}
				}
				dis.readFully(commonByteArray_DEV_AP_Transactions, 0, length);
				strReturn = new String(commonByteArray_DEV_AP_Transactions, 0,
						length, utf8Charset);
			}
			return strReturn;
		}

		private void writeString(String str, ObjectOutputStream dos)
				throws IOException {
			if (str == null) {
				dos.writeInt(-1);
			} else {
				byte[] byteArray = str.getBytes(utf8Charset);
				dos.writeInt(byteArray.length);
				dos.write(byteArray);
			}
		}

		private java.util.Date readDate(ObjectInputStream dis)
				throws IOException {
			java.util.Date dateReturn = null;
			int length = 0;
			length = dis.readByte();
			if (length == -1) {
				dateReturn = null;
			} else {
				dateReturn = new Date(dis.readLong());
			}
			return dateReturn;
		}

		private void writeDate(java.util.Date date1, ObjectOutputStream dos)
				throws IOException {
			if (date1 == null) {
				dos.writeByte(-1);
			} else {
				dos.writeByte(0);
				dos.writeLong(date1.getTime());
			}
		}

		private Integer readInteger(ObjectInputStream dis) throws IOException {
			Integer intReturn;
			int length = 0;
			length = dis.readByte();
			if (length == -1) {
				intReturn = null;
			} else {
				intReturn = dis.readInt();
			}
			return intReturn;
		}

		private void writeInteger(Integer intNum, ObjectOutputStream dos)
				throws IOException {
			if (intNum == null) {
				dos.writeByte(-1);
			} else {
				dos.writeByte(0);
				dos.writeInt(intNum);
			}
		}

		public void readData(ObjectInputStream dis) {

			synchronized (commonByteArrayLock_DEV_AP_Transactions) {

				try {

					int length = 0;

					this.site_ref = readString(dis);

					this.vend_num = readString(dis);

					this.voucher = dis.readInt();

					this.type = readString(dis);

					this.dist_date = readDate(dis);

					this.po_num = readString(dis);

					this.inv_num = readString(dis);

					this.inv_date = readDate(dis);

					length = dis.readByte();
					if (length == -1) {
						this.inv_amt = null;
					} else {
						this.inv_amt = dis.readFloat();
					}

					length = dis.readByte();
					if (length == -1) {
						this.non_disc_amt = null;
					} else {
						this.non_disc_amt = dis.readFloat();
					}

					this.due_days = readInteger(dis);

					this.due_date = readDate(dis);

					this.disc_days = readInteger(dis);

					this.disc_date = readDate(dis);

					this.disc_pct = readInteger(dis);

					length = dis.readByte();
					if (length == -1) {
						this.disc_amt = null;
					} else {
						this.disc_amt = dis.readFloat();
					}

					this.ap_acct = readString(dis);

					this.ref = readString(dis);

					length = dis.readByte();
					if (length == -1) {
						this.post_from_po = null;
					} else {
						this.post_from_po = dis.readByte();
					}

					this.txt = readString(dis);

					this.prox_day = readInteger(dis);

					length = dis.readByte();
					if (length == -1) {
						this.exch_rate = null;
					} else {
						this.exch_rate = dis.readFloat();
					}

					this.includes_tax = readInteger(dis);

					length = dis.readByte();
					if (length == -1) {
						this.purch_amt = null;
					} else {
						this.purch_amt = dis.readFloat();
					}

					length = dis.readByte();
					if (length == -1) {
						this.misc_charges = null;
					} else {
						this.misc_charges = dis.readFloat();
					}

					length = dis.readByte();
					if (length == -1) {
						this.sales_tax = null;
					} else {
						this.sales_tax = dis.readFloat();
					}

					length = dis.readByte();
					if (length == -1) {
						this.sales_tax_2 = null;
					} else {
						this.sales_tax_2 = dis.readFloat();
					}

					length = dis.readByte();
					if (length == -1) {
						this.freight = null;
					} else {
						this.freight = dis.readFloat();
					}

					length = dis.readByte();
					if (length == -1) {
						this.duty_amt = null;
					} else {
						this.duty_amt = dis.readFloat();
					}

					length = dis.readByte();
					if (length == -1) {
						this.brokerage_amt = null;
					} else {
						this.brokerage_amt = dis.readFloat();
					}

					this.tax_code1 = readString(dis);

					this.tax_code2 = readString(dis);

					this.ap_acct_unit1 = readString(dis);

					this.ap_acct_unit2 = readString(dis);

					this.ap_acct_unit3 = readString(dis);

					this.ap_acct_unit4 = readString(dis);

					this.auth_status = readString(dis);

					length = dis.readByte();
					if (length == -1) {
						this.fixed_rate = null;
					} else {
						this.fixed_rate = dis.readShort();
					}

					length = dis.readByte();
					if (length == -1) {
						this.prox_code = null;
					} else {
						this.prox_code = dis.readShort();
					}

					this.grn_num = readString(dis);

					this.pre_register = readInteger(dis);

					this.authorizer = readString(dis);

					length = dis.readByte();
					if (length == -1) {
						this.insurance_amt = null;
					} else {
						this.insurance_amt = dis.readFloat();
					}

					length = dis.readByte();
					if (length == -1) {
						this.loc_frt_amt = null;
					} else {
						this.loc_frt_amt = dis.readFloat();
					}

					length = dis.readByte();
					if (length == -1) {
						this.include_tax_in_cost = null;
					} else {
						this.include_tax_in_cost = dis.readByte();
					}

					this.tax_date = readDate(dis);

					this.builder_po_orig_site = readString(dis);

					this.builder_po_num = readString(dis);

					this.builder_voucher_orig_site = readString(dis);

					this.builder_voucher = readString(dis);

					length = dis.readByte();
					if (length == -1) {
						this.auto_vouchered = null;
					} else {
						this.auto_vouchered = dis.readByte();
					}

					length = dis.readByte();
					if (length == -1) {
						this.cancellation = null;
					} else {
						this.cancellation = dis.readByte();
					}

					this.fiscal_rpt_system_type = readString(dis);

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void writeData(ObjectOutputStream dos) {
			try {

				// String

				writeString(this.site_ref, dos);

				// String

				writeString(this.vend_num, dos);

				// int

				dos.writeInt(this.voucher);

				// String

				writeString(this.type, dos);

				// java.util.Date

				writeDate(this.dist_date, dos);

				// String

				writeString(this.po_num, dos);

				// String

				writeString(this.inv_num, dos);

				// java.util.Date

				writeDate(this.inv_date, dos);

				// Float

				if (this.inv_amt == null) {
					dos.writeByte(-1);
				} else {
					dos.writeByte(0);
					dos.writeFloat(this.inv_amt);
				}

				// Float

				if (this.non_disc_amt == null) {
					dos.writeByte(-1);
				} else {
					dos.writeByte(0);
					dos.writeFloat(this.non_disc_amt);
				}

				// Integer

				writeInteger(this.due_days, dos);

				// java.util.Date

				writeDate(this.due_date, dos);

				// Integer

				writeInteger(this.disc_days, dos);

				// java.util.Date

				writeDate(this.disc_date, dos);

				// Integer

				writeInteger(this.disc_pct, dos);

				// Float

				if (this.disc_amt == null) {
					dos.writeByte(-1);
				} else {
					dos.writeByte(0);
					dos.writeFloat(this.disc_amt);
				}

				// String

				writeString(this.ap_acct, dos);

				// String

				writeString(this.ref, dos);

				// Byte

				if (this.post_from_po == null) {
					dos.writeByte(-1);
				} else {
					dos.writeByte(0);
					dos.writeByte(this.post_from_po);
				}

				// String

				writeString(this.txt, dos);

				// Integer

				writeInteger(this.prox_day, dos);

				// Float

				if (this.exch_rate == null) {
					dos.writeByte(-1);
				} else {
					dos.writeByte(0);
					dos.writeFloat(this.exch_rate);
				}

				// Integer

				writeInteger(this.includes_tax, dos);

				// Float

				if (this.purch_amt == null) {
					dos.writeByte(-1);
				} else {
					dos.writeByte(0);
					dos.writeFloat(this.purch_amt);
				}

				// Float

				if (this.misc_charges == null) {
					dos.writeByte(-1);
				} else {
					dos.writeByte(0);
					dos.writeFloat(this.misc_charges);
				}

				// Float

				if (this.sales_tax == null) {
					dos.writeByte(-1);
				} else {
					dos.writeByte(0);
					dos.writeFloat(this.sales_tax);
				}

				// Float

				if (this.sales_tax_2 == null) {
					dos.writeByte(-1);
				} else {
					dos.writeByte(0);
					dos.writeFloat(this.sales_tax_2);
				}

				// Float

				if (this.freight == null) {
					dos.writeByte(-1);
				} else {
					dos.writeByte(0);
					dos.writeFloat(this.freight);
				}

				// Float

				if (this.duty_amt == null) {
					dos.writeByte(-1);
				} else {
					dos.writeByte(0);
					dos.writeFloat(this.duty_amt);
				}

				// Float

				if (this.brokerage_amt == null) {
					dos.writeByte(-1);
				} else {
					dos.writeByte(0);
					dos.writeFloat(this.brokerage_amt);
				}

				// String

				writeString(this.tax_code1, dos);

				// String

				writeString(this.tax_code2, dos);

				// String

				writeString(this.ap_acct_unit1, dos);

				// String

				writeString(this.ap_acct_unit2, dos);

				// String

				writeString(this.ap_acct_unit3, dos);

				// String

				writeString(this.ap_acct_unit4, dos);

				// String

				writeString(this.auth_status, dos);

				// Short

				if (this.fixed_rate == null) {
					dos.writeByte(-1);
				} else {
					dos.writeByte(0);
					dos.writeShort(this.fixed_rate);
				}

				// Short

				if (this.prox_code == null) {
					dos.writeByte(-1);
				} else {
					dos.writeByte(0);
					dos.writeShort(this.prox_code);
				}

				// String

				writeString(this.grn_num, dos);

				// Integer

				writeInteger(this.pre_register, dos);

				// String

				writeString(this.authorizer, dos);

				// Float

				if (this.insurance_amt == null) {
					dos.writeByte(-1);
				} else {
					dos.writeByte(0);
					dos.writeFloat(this.insurance_amt);
				}

				// Float

				if (this.loc_frt_amt == null) {
					dos.writeByte(-1);
				} else {
					dos.writeByte(0);
					dos.writeFloat(this.loc_frt_amt);
				}

				// Byte

				if (this.include_tax_in_cost == null) {
					dos.writeByte(-1);
				} else {
					dos.writeByte(0);
					dos.writeByte(this.include_tax_in_cost);
				}

				// java.util.Date

				writeDate(this.tax_date, dos);

				// String

				writeString(this.builder_po_orig_site, dos);

				// String

				writeString(this.builder_po_num, dos);

				// String

				writeString(this.builder_voucher_orig_site, dos);

				// String

				writeString(this.builder_voucher, dos);

				// Byte

				if (this.auto_vouchered == null) {
					dos.writeByte(-1);
				} else {
					dos.writeByte(0);
					dos.writeByte(this.auto_vouchered);
				}

				// Byte

				if (this.cancellation == null) {
					dos.writeByte(-1);
				} else {
					dos.writeByte(0);
					dos.writeByte(this.cancellation);
				}

				// String

				writeString(this.fiscal_rpt_system_type, dos);

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public String toString() {

			StringBuilder sb = new StringBuilder();
			sb.append(super.toString());
			sb.append("[");
			sb.append("site_ref=" + site_ref);
			sb.append(",vend_num=" + vend_num);
			sb.append(",voucher=" + String.valueOf(voucher));
			sb.append(",type=" + type);
			sb.append(",dist_date=" + String.valueOf(dist_date));
			sb.append(",po_num=" + po_num);
			sb.append(",inv_num=" + inv_num);
			sb.append(",inv_date=" + String.valueOf(inv_date));
			sb.append(",inv_amt=" + String.valueOf(inv_amt));
			sb.append(",non_disc_amt=" + String.valueOf(non_disc_amt));
			sb.append(",due_days=" + String.valueOf(due_days));
			sb.append(",due_date=" + String.valueOf(due_date));
			sb.append(",disc_days=" + String.valueOf(disc_days));
			sb.append(",disc_date=" + String.valueOf(disc_date));
			sb.append(",disc_pct=" + String.valueOf(disc_pct));
			sb.append(",disc_amt=" + String.valueOf(disc_amt));
			sb.append(",ap_acct=" + ap_acct);
			sb.append(",ref=" + ref);
			sb.append(",post_from_po=" + String.valueOf(post_from_po));
			sb.append(",txt=" + txt);
			sb.append(",prox_day=" + String.valueOf(prox_day));
			sb.append(",exch_rate=" + String.valueOf(exch_rate));
			sb.append(",includes_tax=" + String.valueOf(includes_tax));
			sb.append(",purch_amt=" + String.valueOf(purch_amt));
			sb.append(",misc_charges=" + String.valueOf(misc_charges));
			sb.append(",sales_tax=" + String.valueOf(sales_tax));
			sb.append(",sales_tax_2=" + String.valueOf(sales_tax_2));
			sb.append(",freight=" + String.valueOf(freight));
			sb.append(",duty_amt=" + String.valueOf(duty_amt));
			sb.append(",brokerage_amt=" + String.valueOf(brokerage_amt));
			sb.append(",tax_code1=" + tax_code1);
			sb.append(",tax_code2=" + tax_code2);
			sb.append(",ap_acct_unit1=" + ap_acct_unit1);
			sb.append(",ap_acct_unit2=" + ap_acct_unit2);
			sb.append(",ap_acct_unit3=" + ap_acct_unit3);
			sb.append(",ap_acct_unit4=" + ap_acct_unit4);
			sb.append(",auth_status=" + auth_status);
			sb.append(",fixed_rate=" + String.valueOf(fixed_rate));
			sb.append(",prox_code=" + String.valueOf(prox_code));
			sb.append(",grn_num=" + grn_num);
			sb.append(",pre_register=" + String.valueOf(pre_register));
			sb.append(",authorizer=" + authorizer);
			sb.append(",insurance_amt=" + String.valueOf(insurance_amt));
			sb.append(",loc_frt_amt=" + String.valueOf(loc_frt_amt));
			sb.append(",include_tax_in_cost="
					+ String.valueOf(include_tax_in_cost));
			sb.append(",tax_date=" + String.valueOf(tax_date));
			sb.append(",builder_po_orig_site=" + builder_po_orig_site);
			sb.append(",builder_po_num=" + builder_po_num);
			sb.append(",builder_voucher_orig_site=" + builder_voucher_orig_site);
			sb.append(",builder_voucher=" + builder_voucher);
			sb.append(",auto_vouchered=" + String.valueOf(auto_vouchered));
			sb.append(",cancellation=" + String.valueOf(cancellation));
			sb.append(",fiscal_rpt_system_type=" + fiscal_rpt_system_type);
			sb.append("]");

			return sb.toString();
		}

		public String toLogString() {
			StringBuilder sb = new StringBuilder();

			if (site_ref == null) {
				sb.append("<null>");
			} else {
				sb.append(site_ref);
			}

			sb.append("|");

			if (vend_num == null) {
				sb.append("<null>");
			} else {
				sb.append(vend_num);
			}

			sb.append("|");

			sb.append(voucher);

			sb.append("|");

			if (type == null) {
				sb.append("<null>");
			} else {
				sb.append(type);
			}

			sb.append("|");

			if (dist_date == null) {
				sb.append("<null>");
			} else {
				sb.append(dist_date);
			}

			sb.append("|");

			if (po_num == null) {
				sb.append("<null>");
			} else {
				sb.append(po_num);
			}

			sb.append("|");

			if (inv_num == null) {
				sb.append("<null>");
			} else {
				sb.append(inv_num);
			}

			sb.append("|");

			if (inv_date == null) {
				sb.append("<null>");
			} else {
				sb.append(inv_date);
			}

			sb.append("|");

			if (inv_amt == null) {
				sb.append("<null>");
			} else {
				sb.append(inv_amt);
			}

			sb.append("|");

			if (non_disc_amt == null) {
				sb.append("<null>");
			} else {
				sb.append(non_disc_amt);
			}

			sb.append("|");

			if (due_days == null) {
				sb.append("<null>");
			} else {
				sb.append(due_days);
			}

			sb.append("|");

			if (due_date == null) {
				sb.append("<null>");
			} else {
				sb.append(due_date);
			}

			sb.append("|");

			if (disc_days == null) {
				sb.append("<null>");
			} else {
				sb.append(disc_days);
			}

			sb.append("|");

			if (disc_date == null) {
				sb.append("<null>");
			} else {
				sb.append(disc_date);
			}

			sb.append("|");

			if (disc_pct == null) {
				sb.append("<null>");
			} else {
				sb.append(disc_pct);
			}

			sb.append("|");

			if (disc_amt == null) {
				sb.append("<null>");
			} else {
				sb.append(disc_amt);
			}

			sb.append("|");

			if (ap_acct == null) {
				sb.append("<null>");
			} else {
				sb.append(ap_acct);
			}

			sb.append("|");

			if (ref == null) {
				sb.append("<null>");
			} else {
				sb.append(ref);
			}

			sb.append("|");

			if (post_from_po == null) {
				sb.append("<null>");
			} else {
				sb.append(post_from_po);
			}

			sb.append("|");

			if (txt == null) {
				sb.append("<null>");
			} else {
				sb.append(txt);
			}

			sb.append("|");

			if (prox_day == null) {
				sb.append("<null>");
			} else {
				sb.append(prox_day);
			}

			sb.append("|");

			if (exch_rate == null) {
				sb.append("<null>");
			} else {
				sb.append(exch_rate);
			}

			sb.append("|");

			if (includes_tax == null) {
				sb.append("<null>");
			} else {
				sb.append(includes_tax);
			}

			sb.append("|");

			if (purch_amt == null) {
				sb.append("<null>");
			} else {
				sb.append(purch_amt);
			}

			sb.append("|");

			if (misc_charges == null) {
				sb.append("<null>");
			} else {
				sb.append(misc_charges);
			}

			sb.append("|");

			if (sales_tax == null) {
				sb.append("<null>");
			} else {
				sb.append(sales_tax);
			}

			sb.append("|");

			if (sales_tax_2 == null) {
				sb.append("<null>");
			} else {
				sb.append(sales_tax_2);
			}

			sb.append("|");

			if (freight == null) {
				sb.append("<null>");
			} else {
				sb.append(freight);
			}

			sb.append("|");

			if (duty_amt == null) {
				sb.append("<null>");
			} else {
				sb.append(duty_amt);
			}

			sb.append("|");

			if (brokerage_amt == null) {
				sb.append("<null>");
			} else {
				sb.append(brokerage_amt);
			}

			sb.append("|");

			if (tax_code1 == null) {
				sb.append("<null>");
			} else {
				sb.append(tax_code1);
			}

			sb.append("|");

			if (tax_code2 == null) {
				sb.append("<null>");
			} else {
				sb.append(tax_code2);
			}

			sb.append("|");

			if (ap_acct_unit1 == null) {
				sb.append("<null>");
			} else {
				sb.append(ap_acct_unit1);
			}

			sb.append("|");

			if (ap_acct_unit2 == null) {
				sb.append("<null>");
			} else {
				sb.append(ap_acct_unit2);
			}

			sb.append("|");

			if (ap_acct_unit3 == null) {
				sb.append("<null>");
			} else {
				sb.append(ap_acct_unit3);
			}

			sb.append("|");

			if (ap_acct_unit4 == null) {
				sb.append("<null>");
			} else {
				sb.append(ap_acct_unit4);
			}

			sb.append("|");

			if (auth_status == null) {
				sb.append("<null>");
			} else {
				sb.append(auth_status);
			}

			sb.append("|");

			if (fixed_rate == null) {
				sb.append("<null>");
			} else {
				sb.append(fixed_rate);
			}

			sb.append("|");

			if (prox_code == null) {
				sb.append("<null>");
			} else {
				sb.append(prox_code);
			}

			sb.append("|");

			if (grn_num == null) {
				sb.append("<null>");
			} else {
				sb.append(grn_num);
			}

			sb.append("|");

			if (pre_register == null) {
				sb.append("<null>");
			} else {
				sb.append(pre_register);
			}

			sb.append("|");

			if (authorizer == null) {
				sb.append("<null>");
			} else {
				sb.append(authorizer);
			}

			sb.append("|");

			if (insurance_amt == null) {
				sb.append("<null>");
			} else {
				sb.append(insurance_amt);
			}

			sb.append("|");

			if (loc_frt_amt == null) {
				sb.append("<null>");
			} else {
				sb.append(loc_frt_amt);
			}

			sb.append("|");

			if (include_tax_in_cost == null) {
				sb.append("<null>");
			} else {
				sb.append(include_tax_in_cost);
			}

			sb.append("|");

			if (tax_date == null) {
				sb.append("<null>");
			} else {
				sb.append(tax_date);
			}

			sb.append("|");

			if (builder_po_orig_site == null) {
				sb.append("<null>");
			} else {
				sb.append(builder_po_orig_site);
			}

			sb.append("|");

			if (builder_po_num == null) {
				sb.append("<null>");
			} else {
				sb.append(builder_po_num);
			}

			sb.append("|");

			if (builder_voucher_orig_site == null) {
				sb.append("<null>");
			} else {
				sb.append(builder_voucher_orig_site);
			}

			sb.append("|");

			if (builder_voucher == null) {
				sb.append("<null>");
			} else {
				sb.append(builder_voucher);
			}

			sb.append("|");

			if (auto_vouchered == null) {
				sb.append("<null>");
			} else {
				sb.append(auto_vouchered);
			}

			sb.append("|");

			if (cancellation == null) {
				sb.append("<null>");
			} else {
				sb.append(cancellation);
			}

			sb.append("|");

			if (fiscal_rpt_system_type == null) {
				sb.append("<null>");
			} else {
				sb.append(fiscal_rpt_system_type);
			}

			sb.append("|");

			return sb.toString();
		}

		/**
		 * Compare keys
		 */
		public int compareTo(voucherStruct other) {

			int returnValue = -1;

			returnValue = checkNullsAndCompare(this.site_ref, other.site_ref);
			if (returnValue != 0) {
				return returnValue;
			}

			returnValue = checkNullsAndCompare(this.vend_num, other.vend_num);
			if (returnValue != 0) {
				return returnValue;
			}

			returnValue = checkNullsAndCompare(this.voucher, other.voucher);
			if (returnValue != 0) {
				return returnValue;
			}

			return returnValue;
		}

		private int checkNullsAndCompare(Object object1, Object object2) {
			int returnValue = 0;
			if (object1 instanceof Comparable && object2 instanceof Comparable) {
				returnValue = ((Comparable) object1).compareTo(object2);
			} else if (object1 != null && object2 != null) {
				returnValue = compareStrings(object1.toString(),
						object2.toString());
			} else if (object1 == null && object2 != null) {
				returnValue = 1;
			} else if (object1 != null && object2 == null) {
				returnValue = -1;
			} else {
				returnValue = 0;
			}

			return returnValue;
		}

		private int compareStrings(String string1, String string2) {
			return string1.compareTo(string2);
		}

	}

	public static class row3Struct implements
			routines.system.IPersistableRow<row3Struct> {
		final static byte[] commonByteArrayLock_DEV_AP_Transactions = new byte[0];
		static byte[] commonByteArray_DEV_AP_Transactions = new byte[0];

		public String Employee_ID;

		public String getEmployee_ID() {
			return this.Employee_ID;
		}

		public java.util.Date Process_Date;

		public java.util.Date getProcess_Date() {
			return this.Process_Date;
		}

		public String GL_Codes;

		public String getGL_Codes() {
			return this.GL_Codes;
		}

		public String PSID;

		public String getPSID() {
			return this.PSID;
		}

		public Float Amount;

		public Float getAmount() {
			return this.Amount;
		}

		public Integer Voucher;

		public Integer getVoucher() {
			return this.Voucher;
		}

		private String readString(ObjectInputStream dis) throws IOException {
			String strReturn = null;
			int length = 0;
			length = dis.readInt();
			if (length == -1) {
				strReturn = null;
			} else {
				if (length > commonByteArray_DEV_AP_Transactions.length) {
					if (length < 1024
							&& commonByteArray_DEV_AP_Transactions.length == 0) {
						commonByteArray_DEV_AP_Transactions = new byte[1024];
					} else {
						commonByteArray_DEV_AP_Transactions = new byte[2 * length];
					}
				}
				dis.readFully(commonByteArray_DEV_AP_Transactions, 0, length);
				strReturn = new String(commonByteArray_DEV_AP_Transactions, 0,
						length, utf8Charset);
			}
			return strReturn;
		}

		private void writeString(String str, ObjectOutputStream dos)
				throws IOException {
			if (str == null) {
				dos.writeInt(-1);
			} else {
				byte[] byteArray = str.getBytes(utf8Charset);
				dos.writeInt(byteArray.length);
				dos.write(byteArray);
			}
		}

		private java.util.Date readDate(ObjectInputStream dis)
				throws IOException {
			java.util.Date dateReturn = null;
			int length = 0;
			length = dis.readByte();
			if (length == -1) {
				dateReturn = null;
			} else {
				dateReturn = new Date(dis.readLong());
			}
			return dateReturn;
		}

		private void writeDate(java.util.Date date1, ObjectOutputStream dos)
				throws IOException {
			if (date1 == null) {
				dos.writeByte(-1);
			} else {
				dos.writeByte(0);
				dos.writeLong(date1.getTime());
			}
		}

		private Integer readInteger(ObjectInputStream dis) throws IOException {
			Integer intReturn;
			int length = 0;
			length = dis.readByte();
			if (length == -1) {
				intReturn = null;
			} else {
				intReturn = dis.readInt();
			}
			return intReturn;
		}

		private void writeInteger(Integer intNum, ObjectOutputStream dos)
				throws IOException {
			if (intNum == null) {
				dos.writeByte(-1);
			} else {
				dos.writeByte(0);
				dos.writeInt(intNum);
			}
		}

		public void readData(ObjectInputStream dis) {

			synchronized (commonByteArrayLock_DEV_AP_Transactions) {

				try {

					int length = 0;

					this.Employee_ID = readString(dis);

					this.Process_Date = readDate(dis);

					this.GL_Codes = readString(dis);

					this.PSID = readString(dis);

					length = dis.readByte();
					if (length == -1) {
						this.Amount = null;
					} else {
						this.Amount = dis.readFloat();
					}

					this.Voucher = readInteger(dis);

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void writeData(ObjectOutputStream dos) {
			try {

				// String

				writeString(this.Employee_ID, dos);

				// java.util.Date

				writeDate(this.Process_Date, dos);

				// String

				writeString(this.GL_Codes, dos);

				// String

				writeString(this.PSID, dos);

				// Float

				if (this.Amount == null) {
					dos.writeByte(-1);
				} else {
					dos.writeByte(0);
					dos.writeFloat(this.Amount);
				}

				// Integer

				writeInteger(this.Voucher, dos);

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public String toString() {

			StringBuilder sb = new StringBuilder();
			sb.append(super.toString());
			sb.append("[");
			sb.append("Employee_ID=" + Employee_ID);
			sb.append(",Process_Date=" + String.valueOf(Process_Date));
			sb.append(",GL_Codes=" + GL_Codes);
			sb.append(",PSID=" + PSID);
			sb.append(",Amount=" + String.valueOf(Amount));
			sb.append(",Voucher=" + String.valueOf(Voucher));
			sb.append("]");

			return sb.toString();
		}

		public String toLogString() {
			StringBuilder sb = new StringBuilder();

			if (Employee_ID == null) {
				sb.append("<null>");
			} else {
				sb.append(Employee_ID);
			}

			sb.append("|");

			if (Process_Date == null) {
				sb.append("<null>");
			} else {
				sb.append(Process_Date);
			}

			sb.append("|");

			if (GL_Codes == null) {
				sb.append("<null>");
			} else {
				sb.append(GL_Codes);
			}

			sb.append("|");

			if (PSID == null) {
				sb.append("<null>");
			} else {
				sb.append(PSID);
			}

			sb.append("|");

			if (Amount == null) {
				sb.append("<null>");
			} else {
				sb.append(Amount);
			}

			sb.append("|");

			if (Voucher == null) {
				sb.append("<null>");
			} else {
				sb.append(Voucher);
			}

			sb.append("|");

			return sb.toString();
		}

		/**
		 * Compare keys
		 */
		public int compareTo(row3Struct other) {

			int returnValue = -1;

			return returnValue;
		}

		private int checkNullsAndCompare(Object object1, Object object2) {
			int returnValue = 0;
			if (object1 instanceof Comparable && object2 instanceof Comparable) {
				returnValue = ((Comparable) object1).compareTo(object2);
			} else if (object1 != null && object2 != null) {
				returnValue = compareStrings(object1.toString(),
						object2.toString());
			} else if (object1 == null && object2 != null) {
				returnValue = 1;
			} else if (object1 != null && object2 == null) {
				returnValue = -1;
			} else {
				returnValue = 0;
			}

			return returnValue;
		}

		private int compareStrings(String string1, String string2) {
			return string1.compareTo(string2);
		}

	}

	public void tDBInput_1Process(final java.util.Map<String, Object> globalMap)
			throws TalendException {
		globalMap.put("tDBInput_1_SUBPROCESS_STATE", 0);

		final boolean execStat = this.execStat;

		String iterateId = "";

		String currentComponent = "";
		java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

		try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception()
						.getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { // start the resume
				globalResumeTicket = true;

				row3Struct row3 = new row3Struct();
				voucherStruct voucher = new voucherStruct();

				/**
				 * [tDBOutput_4 begin ] start
				 */

				ok_Hash.put("tDBOutput_4", false);
				start_Hash.put("tDBOutput_4", System.currentTimeMillis());

				currentComponent = "tDBOutput_4";

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null) {

						runStat.updateStatOnConnection("voucher" + iterateId,
								0, 0);

					}
				}

				int tos_count_tDBOutput_4 = 0;

				if (log.isDebugEnabled())
					log.debug("tDBOutput_4 - " + ("Start to work."));
				if (log.isDebugEnabled()) {
					class BytesLimit65535_tDBOutput_4 {
						public void limitLog4jByte() throws Exception {
							StringBuilder log4jParamters_tDBOutput_4 = new StringBuilder();
							log4jParamters_tDBOutput_4.append("Parameters:");
							log4jParamters_tDBOutput_4
									.append("USE_EXISTING_CONNECTION" + " = "
											+ "false");
							log4jParamters_tDBOutput_4.append(" | ");
							log4jParamters_tDBOutput_4.append("DRIVER" + " = "
									+ "JTDS");
							log4jParamters_tDBOutput_4.append(" | ");
							log4jParamters_tDBOutput_4.append("HOST" + " = "
									+ "\"NSMWQNXINFDB01\"");
							log4jParamters_tDBOutput_4.append(" | ");
							log4jParamters_tDBOutput_4.append("PORT" + " = "
									+ "\"1433\"");
							log4jParamters_tDBOutput_4.append(" | ");
							log4jParamters_tDBOutput_4.append("DB_SCHEMA"
									+ " = " + "\"dbo\"");
							log4jParamters_tDBOutput_4.append(" | ");
							log4jParamters_tDBOutput_4.append("DBNAME" + " = "
									+ "\"QBP_HO_APP\"");
							log4jParamters_tDBOutput_4.append(" | ");
							log4jParamters_tDBOutput_4.append("USER" + " = "
									+ "\"sa\"");
							log4jParamters_tDBOutput_4.append(" | ");
							log4jParamters_tDBOutput_4.append("PASS"
									+ " = "
									+ String.valueOf(
											"bf98e2c82ba433c4880df380fbb39fe9")
											.substring(0, 4) + "...");
							log4jParamters_tDBOutput_4.append(" | ");
							log4jParamters_tDBOutput_4.append("TABLE" + " = "
									+ "\"aptrx_mst\"");
							log4jParamters_tDBOutput_4.append(" | ");
							log4jParamters_tDBOutput_4.append("TABLE_ACTION"
									+ " = " + "NONE");
							log4jParamters_tDBOutput_4.append(" | ");
							log4jParamters_tDBOutput_4.append("IDENTITY_INSERT"
									+ " = " + "false");
							log4jParamters_tDBOutput_4.append(" | ");
							log4jParamters_tDBOutput_4.append("DATA_ACTION"
									+ " = " + "INSERT");
							log4jParamters_tDBOutput_4.append(" | ");
							log4jParamters_tDBOutput_4
									.append("SPECIFY_DATASOURCE_ALIAS" + " = "
											+ "false");
							log4jParamters_tDBOutput_4.append(" | ");
							log4jParamters_tDBOutput_4.append("DIE_ON_ERROR"
									+ " = " + "false");
							log4jParamters_tDBOutput_4.append(" | ");
							log4jParamters_tDBOutput_4.append("PROPERTIES"
									+ " = " + "\"\"");
							log4jParamters_tDBOutput_4.append(" | ");
							log4jParamters_tDBOutput_4.append("COMMIT_EVERY"
									+ " = " + "10000");
							log4jParamters_tDBOutput_4.append(" | ");
							log4jParamters_tDBOutput_4.append("ADD_COLS"
									+ " = " + "[]");
							log4jParamters_tDBOutput_4.append(" | ");
							log4jParamters_tDBOutput_4
									.append("USE_FIELD_OPTIONS" + " = "
											+ "false");
							log4jParamters_tDBOutput_4.append(" | ");
							log4jParamters_tDBOutput_4
									.append("IGNORE_DATE_OUTOF_RANGE" + " = "
											+ "false");
							log4jParamters_tDBOutput_4.append(" | ");
							log4jParamters_tDBOutput_4
									.append("ENABLE_DEBUG_MODE" + " = "
											+ "false");
							log4jParamters_tDBOutput_4.append(" | ");
							log4jParamters_tDBOutput_4
									.append("SUPPORT_NULL_WHERE" + " = "
											+ "false");
							log4jParamters_tDBOutput_4.append(" | ");
							log4jParamters_tDBOutput_4.append("USE_BATCH_SIZE"
									+ " = " + "true");
							log4jParamters_tDBOutput_4.append(" | ");
							log4jParamters_tDBOutput_4.append("BATCH_SIZE"
									+ " = " + "10000");
							log4jParamters_tDBOutput_4.append(" | ");
							log4jParamters_tDBOutput_4
									.append("UNIFIED_COMPONENTS" + " = "
											+ "tMSSqlOutput");
							log4jParamters_tDBOutput_4.append(" | ");
							if (log.isDebugEnabled())
								log.debug("tDBOutput_4 - "
										+ (log4jParamters_tDBOutput_4));
						}
					}
					new BytesLimit65535_tDBOutput_4().limitLog4jByte();
				}

				int nb_line_tDBOutput_4 = 0;
				int nb_line_update_tDBOutput_4 = 0;
				int nb_line_inserted_tDBOutput_4 = 0;
				int nb_line_deleted_tDBOutput_4 = 0;
				int nb_line_rejected_tDBOutput_4 = 0;

				int deletedCount_tDBOutput_4 = 0;
				int updatedCount_tDBOutput_4 = 0;
				int insertedCount_tDBOutput_4 = 0;
				int rejectedCount_tDBOutput_4 = 0;
				String dbschema_tDBOutput_4 = null;
				String tableName_tDBOutput_4 = null;
				boolean whetherReject_tDBOutput_4 = false;

				java.util.Calendar calendar_tDBOutput_4 = java.util.Calendar
						.getInstance();
				long year1_tDBOutput_4 = TalendDate.parseDate("yyyy-MM-dd",
						"0001-01-01").getTime();
				long year2_tDBOutput_4 = TalendDate.parseDate("yyyy-MM-dd",
						"1753-01-01").getTime();
				long year10000_tDBOutput_4 = TalendDate.parseDate(
						"yyyy-MM-dd HH:mm:ss", "9999-12-31 24:00:00").getTime();
				long date_tDBOutput_4;

				java.util.Calendar calendar_datetimeoffset_tDBOutput_4 = java.util.Calendar
						.getInstance(java.util.TimeZone.getTimeZone("UTC"));

				java.sql.Connection conn_tDBOutput_4 = null;
				String dbUser_tDBOutput_4 = null;
				dbschema_tDBOutput_4 = "dbo";
				String driverClass_tDBOutput_4 = "net.sourceforge.jtds.jdbc.Driver";

				if (log.isDebugEnabled())
					log.debug("tDBOutput_4 - " + ("Driver ClassName: ")
							+ (driverClass_tDBOutput_4) + ("."));
				java.lang.Class.forName(driverClass_tDBOutput_4);
				String port_tDBOutput_4 = "1433";
				String dbname_tDBOutput_4 = "QBP_HO_APP";
				String url_tDBOutput_4 = "jdbc:jtds:sqlserver://"
						+ "NSMWQNXINFDB01";
				if (!"".equals(port_tDBOutput_4)) {
					url_tDBOutput_4 += ":" + "1433";
				}
				if (!"".equals(dbname_tDBOutput_4)) {
					url_tDBOutput_4 += "//" + "QBP_HO_APP";

				}
				url_tDBOutput_4 += ";appName=" + projectName + ";" + "";
				dbUser_tDBOutput_4 = "sa";

				final String decryptedPassword_tDBOutput_4 = routines.system.PasswordEncryptUtil
						.decryptPassword("bf98e2c82ba433c4880df380fbb39fe9");

				String dbPwd_tDBOutput_4 = decryptedPassword_tDBOutput_4;
				if (log.isDebugEnabled())
					log.debug("tDBOutput_4 - " + ("Connection attempts to '")
							+ (url_tDBOutput_4) + ("' with the username '")
							+ (dbUser_tDBOutput_4) + ("'."));
				conn_tDBOutput_4 = java.sql.DriverManager.getConnection(
						url_tDBOutput_4, dbUser_tDBOutput_4, dbPwd_tDBOutput_4);
				if (log.isDebugEnabled())
					log.debug("tDBOutput_4 - " + ("Connection to '")
							+ (url_tDBOutput_4) + ("' has succeeded."));

				resourceMap.put("conn_tDBOutput_4", conn_tDBOutput_4);

				conn_tDBOutput_4.setAutoCommit(false);
				int commitEvery_tDBOutput_4 = 10000;
				int commitCounter_tDBOutput_4 = 0;

				if (log.isDebugEnabled())
					log.debug("tDBOutput_4 - "
							+ ("Connection is set auto commit to '")
							+ (conn_tDBOutput_4.getAutoCommit()) + ("'."));
				int batchSize_tDBOutput_4 = 10000;
				int batchSizeCounter_tDBOutput_4 = 0;

				if (dbschema_tDBOutput_4 == null
						|| dbschema_tDBOutput_4.trim().length() == 0) {
					tableName_tDBOutput_4 = "aptrx_mst";
				} else {
					tableName_tDBOutput_4 = dbschema_tDBOutput_4 + "].["
							+ "aptrx_mst";
				}
				int count_tDBOutput_4 = 0;

				String insert_tDBOutput_4 = "INSERT INTO ["
						+ tableName_tDBOutput_4
						+ "] ([site_ref],[vend_num],[voucher],[type],[dist_date],[po_num],[inv_num],[inv_date],[inv_amt],[non_disc_amt],[due_days],[due_date],[disc_days],[disc_date],[disc_pct],[disc_amt],[ap_acct],[ref],[post_from_po],[txt],[prox_day],[exch_rate],[includes_tax],[purch_amt],[misc_charges],[sales_tax],[sales_tax_2],[freight],[duty_amt],[brokerage_amt],[tax_code1],[tax_code2],[ap_acct_unit1],[ap_acct_unit2],[ap_acct_unit3],[ap_acct_unit4],[auth_status],[fixed_rate],[prox_code],[grn_num],[pre_register],[authorizer],[insurance_amt],[loc_frt_amt],[include_tax_in_cost],[tax_date],[builder_po_orig_site],[builder_po_num],[builder_voucher_orig_site],[builder_voucher],[auto_vouchered],[cancellation],[fiscal_rpt_system_type]) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
				java.sql.PreparedStatement pstmt_tDBOutput_4 = conn_tDBOutput_4
						.prepareStatement(insert_tDBOutput_4);
				resourceMap.put("pstmt_tDBOutput_4", pstmt_tDBOutput_4);

				/**
				 * [tDBOutput_4 begin ] stop
				 */

				/**
				 * [tMap_2 begin ] start
				 */

				ok_Hash.put("tMap_2", false);
				start_Hash.put("tMap_2", System.currentTimeMillis());

				currentComponent = "tMap_2";

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null) {

						runStat.updateStatOnConnection("row3" + iterateId, 0, 0);

					}
				}

				int tos_count_tMap_2 = 0;

				if (log.isDebugEnabled())
					log.debug("tMap_2 - " + ("Start to work."));
				if (log.isDebugEnabled()) {
					class BytesLimit65535_tMap_2 {
						public void limitLog4jByte() throws Exception {
							StringBuilder log4jParamters_tMap_2 = new StringBuilder();
							log4jParamters_tMap_2.append("Parameters:");
							log4jParamters_tMap_2.append("LINK_STYLE" + " = "
									+ "AUTO");
							log4jParamters_tMap_2.append(" | ");
							log4jParamters_tMap_2
									.append("TEMPORARY_DATA_DIRECTORY" + " = "
											+ "");
							log4jParamters_tMap_2.append(" | ");
							log4jParamters_tMap_2.append("ROWS_BUFFER_SIZE"
									+ " = " + "2000000");
							log4jParamters_tMap_2.append(" | ");
							log4jParamters_tMap_2
									.append("CHANGE_HASH_AND_EQUALS_FOR_BIGDECIMAL"
											+ " = " + "true");
							log4jParamters_tMap_2.append(" | ");
							if (log.isDebugEnabled())
								log.debug("tMap_2 - " + (log4jParamters_tMap_2));
						}
					}
					new BytesLimit65535_tMap_2().limitLog4jByte();
				}

				// ###############################
				// # Lookup's keys initialization
				int count_row3_tMap_2 = 0;

				// ###############################

				// ###############################
				// # Vars initialization
				class Var__tMap_2__Struct {
				}
				Var__tMap_2__Struct Var__tMap_2 = new Var__tMap_2__Struct();
				// ###############################

				// ###############################
				// # Outputs initialization
				int count_voucher_tMap_2 = 0;

				voucherStruct voucher_tmp = new voucherStruct();
				// ###############################

				/**
				 * [tMap_2 begin ] stop
				 */

				/**
				 * [tDBInput_1 begin ] start
				 */

				ok_Hash.put("tDBInput_1", false);
				start_Hash.put("tDBInput_1", System.currentTimeMillis());

				currentComponent = "tDBInput_1";

				int tos_count_tDBInput_1 = 0;

				if (log.isDebugEnabled())
					log.debug("tDBInput_1 - " + ("Start to work."));
				if (log.isDebugEnabled()) {
					class BytesLimit65535_tDBInput_1 {
						public void limitLog4jByte() throws Exception {
							StringBuilder log4jParamters_tDBInput_1 = new StringBuilder();
							log4jParamters_tDBInput_1.append("Parameters:");
							log4jParamters_tDBInput_1
									.append("USE_EXISTING_CONNECTION" + " = "
											+ "false");
							log4jParamters_tDBInput_1.append(" | ");
							log4jParamters_tDBInput_1.append("HOST" + " = "
									+ "\"NSMWQNXINFDB02\"");
							log4jParamters_tDBInput_1.append(" | ");
							log4jParamters_tDBInput_1.append("DRIVER" + " = "
									+ "JTDS");
							log4jParamters_tDBInput_1.append(" | ");
							log4jParamters_tDBInput_1.append("PORT" + " = "
									+ "\"1433\"");
							log4jParamters_tDBInput_1.append(" | ");
							log4jParamters_tDBInput_1.append("DB_SCHEMA"
									+ " = " + "\"\"");
							log4jParamters_tDBInput_1.append(" | ");
							log4jParamters_tDBInput_1.append("DBNAME" + " = "
									+ "\"Talend_Certify\"");
							log4jParamters_tDBInput_1.append(" | ");
							log4jParamters_tDBInput_1.append("USER" + " = "
									+ "\"sa\"");
							log4jParamters_tDBInput_1.append(" | ");
							log4jParamters_tDBInput_1.append("PASS"
									+ " = "
									+ String.valueOf(
											"bf98e2c82ba433c4880df380fbb39fe9")
											.substring(0, 4) + "...");
							log4jParamters_tDBInput_1.append(" | ");
							log4jParamters_tDBInput_1.append("TABLE" + " = "
									+ "\"CSI_9030_IN\"");
							log4jParamters_tDBInput_1.append(" | ");
							log4jParamters_tDBInput_1.append("QUERYSTORE"
									+ " = " + "\"\"");
							log4jParamters_tDBInput_1.append(" | ");
							log4jParamters_tDBInput_1
									.append("QUERY"
											+ " = "
											+ "\"SELECT  CSI_9030_IN.Employee_ID,          CSI_9030_IN.Process_Date,         '20000' AS GL_Codes,          CSI_9030_IN.PSID,         SUM( CSI_9030_IN.Amount) AS Amount,          CSI_9030_IN.Voucher  FROM CSI_9030_IN  GROUP BY  CSI_9030_IN.Employee_ID,            CSI_9030_IN.Process_Date,            CSI_9030_IN.PSID,            CSI_9030_IN.Voucher\"");
							log4jParamters_tDBInput_1.append(" | ");
							log4jParamters_tDBInput_1
									.append("SPECIFY_DATASOURCE_ALIAS" + " = "
											+ "false");
							log4jParamters_tDBInput_1.append(" | ");
							log4jParamters_tDBInput_1.append("PROPERTIES"
									+ " = " + "\"\"");
							log4jParamters_tDBInput_1.append(" | ");
							log4jParamters_tDBInput_1.append("TRIM_ALL_COLUMN"
									+ " = " + "false");
							log4jParamters_tDBInput_1.append(" | ");
							log4jParamters_tDBInput_1.append("TRIM_COLUMN"
									+ " = " + "[{TRIM=" + ("false")
									+ ", SCHEMA_COLUMN=" + ("Employee_ID")
									+ "}, {TRIM=" + ("false")
									+ ", SCHEMA_COLUMN=" + ("Process_Date")
									+ "}, {TRIM=" + ("false")
									+ ", SCHEMA_COLUMN=" + ("GL_Codes")
									+ "}, {TRIM=" + ("false")
									+ ", SCHEMA_COLUMN=" + ("PSID")
									+ "}, {TRIM=" + ("false")
									+ ", SCHEMA_COLUMN=" + ("Amount")
									+ "}, {TRIM=" + ("false")
									+ ", SCHEMA_COLUMN=" + ("Voucher") + "}]");
							log4jParamters_tDBInput_1.append(" | ");
							log4jParamters_tDBInput_1
									.append("UNIFIED_COMPONENTS" + " = "
											+ "tMSSqlInput");
							log4jParamters_tDBInput_1.append(" | ");
							if (log.isDebugEnabled())
								log.debug("tDBInput_1 - "
										+ (log4jParamters_tDBInput_1));
						}
					}
					new BytesLimit65535_tDBInput_1().limitLog4jByte();
				}

				org.talend.designer.components.util.mssql.MSSqlGenerateTimestampUtil mssqlGTU_tDBInput_1 = org.talend.designer.components.util.mssql.MSSqlUtilFactory
						.getMSSqlGenerateTimestampUtil();

				java.util.List<String> talendToDBList_tDBInput_1 = new java.util.ArrayList();
				String[] talendToDBArray_tDBInput_1 = new String[] { "FLOAT",
						"NUMERIC", "NUMERIC IDENTITY", "DECIMAL",
						"DECIMAL IDENTITY", "REAL" };
				java.util.Collections.addAll(talendToDBList_tDBInput_1,
						talendToDBArray_tDBInput_1);
				int nb_line_tDBInput_1 = 0;
				java.sql.Connection conn_tDBInput_1 = null;
				String driverClass_tDBInput_1 = "net.sourceforge.jtds.jdbc.Driver";
				java.lang.Class.forName(driverClass_tDBInput_1);
				String dbUser_tDBInput_1 = "sa";

				final String decryptedPassword_tDBInput_1 = routines.system.PasswordEncryptUtil
						.decryptPassword("bf98e2c82ba433c4880df380fbb39fe9");

				String dbPwd_tDBInput_1 = decryptedPassword_tDBInput_1;

				String port_tDBInput_1 = "1433";
				String dbname_tDBInput_1 = "Talend_Certify";
				String url_tDBInput_1 = "jdbc:jtds:sqlserver://"
						+ "NSMWQNXINFDB02";
				if (!"".equals(port_tDBInput_1)) {
					url_tDBInput_1 += ":" + "1433";
				}
				if (!"".equals(dbname_tDBInput_1)) {
					url_tDBInput_1 += "//" + "Talend_Certify";
				}
				url_tDBInput_1 += ";appName=" + projectName + ";" + "";
				String dbschema_tDBInput_1 = "";

				log.debug("tDBInput_1 - Driver ClassName: "
						+ driverClass_tDBInput_1 + ".");

				log.debug("tDBInput_1 - Connection attempt to '"
						+ url_tDBInput_1 + "' with the username '"
						+ dbUser_tDBInput_1 + "'.");

				conn_tDBInput_1 = java.sql.DriverManager.getConnection(
						url_tDBInput_1, dbUser_tDBInput_1, dbPwd_tDBInput_1);
				log.debug("tDBInput_1 - Connection to '" + url_tDBInput_1
						+ "' has succeeded.");

				java.sql.Statement stmt_tDBInput_1 = conn_tDBInput_1
						.createStatement();

				String dbquery_tDBInput_1 = "SELECT  CSI_9030_IN.Employee_ID,\n        CSI_9030_IN.Process_Date,\n       '20000' AS GL_Codes,\n        CSI_9030_IN.P"
						+ "SID,\n       SUM( CSI_9030_IN.Amount) AS Amount,\n        CSI_9030_IN.Voucher\nFROM CSI_9030_IN\nGROUP BY  CSI_9030_IN.E"
						+ "mployee_ID,\n          CSI_9030_IN.Process_Date,\n          CSI_9030_IN.PSID,\n          CSI_9030_IN.Voucher";

				log.debug("tDBInput_1 - Executing the query: '"
						+ dbquery_tDBInput_1 + "'.");

				globalMap.put("tDBInput_1_QUERY", dbquery_tDBInput_1);
				java.sql.ResultSet rs_tDBInput_1 = null;

				try {
					rs_tDBInput_1 = stmt_tDBInput_1
							.executeQuery(dbquery_tDBInput_1);
					java.sql.ResultSetMetaData rsmd_tDBInput_1 = rs_tDBInput_1
							.getMetaData();
					int colQtyInRs_tDBInput_1 = rsmd_tDBInput_1
							.getColumnCount();

					String tmpContent_tDBInput_1 = null;

					log.debug("tDBInput_1 - Retrieving records from the database.");

					while (rs_tDBInput_1.next()) {
						nb_line_tDBInput_1++;

						if (colQtyInRs_tDBInput_1 < 1) {
							row3.Employee_ID = null;
						} else {

							tmpContent_tDBInput_1 = rs_tDBInput_1.getString(1);
							if (tmpContent_tDBInput_1 != null) {
								if (talendToDBList_tDBInput_1
										.contains(rsmd_tDBInput_1
												.getColumnTypeName(1)
												.toUpperCase(
														java.util.Locale.ENGLISH))) {
									row3.Employee_ID = FormatterUtils
											.formatUnwithE(tmpContent_tDBInput_1);
								} else {
									row3.Employee_ID = tmpContent_tDBInput_1;
								}
							} else {
								row3.Employee_ID = null;
							}
						}
						if (colQtyInRs_tDBInput_1 < 2) {
							row3.Process_Date = null;
						} else {

							row3.Process_Date = mssqlGTU_tDBInput_1.getDate(
									rsmd_tDBInput_1, rs_tDBInput_1, 2);

						}
						if (colQtyInRs_tDBInput_1 < 3) {
							row3.GL_Codes = null;
						} else {

							tmpContent_tDBInput_1 = rs_tDBInput_1.getString(3);
							if (tmpContent_tDBInput_1 != null) {
								if (talendToDBList_tDBInput_1
										.contains(rsmd_tDBInput_1
												.getColumnTypeName(3)
												.toUpperCase(
														java.util.Locale.ENGLISH))) {
									row3.GL_Codes = FormatterUtils
											.formatUnwithE(tmpContent_tDBInput_1);
								} else {
									row3.GL_Codes = tmpContent_tDBInput_1;
								}
							} else {
								row3.GL_Codes = null;
							}
						}
						if (colQtyInRs_tDBInput_1 < 4) {
							row3.PSID = null;
						} else {

							tmpContent_tDBInput_1 = rs_tDBInput_1.getString(4);
							if (tmpContent_tDBInput_1 != null) {
								if (talendToDBList_tDBInput_1
										.contains(rsmd_tDBInput_1
												.getColumnTypeName(4)
												.toUpperCase(
														java.util.Locale.ENGLISH))) {
									row3.PSID = FormatterUtils
											.formatUnwithE(tmpContent_tDBInput_1);
								} else {
									row3.PSID = tmpContent_tDBInput_1;
								}
							} else {
								row3.PSID = null;
							}
						}
						if (colQtyInRs_tDBInput_1 < 5) {
							row3.Amount = null;
						} else {

							if (rs_tDBInput_1.getObject(5) != null) {
								row3.Amount = rs_tDBInput_1.getFloat(5);
							} else {
								row3.Amount = null;
							}
						}
						if (colQtyInRs_tDBInput_1 < 6) {
							row3.Voucher = null;
						} else {

							if (rs_tDBInput_1.getObject(6) != null) {
								row3.Voucher = rs_tDBInput_1.getInt(6);
							} else {
								row3.Voucher = null;
							}
						}

						log.debug("tDBInput_1 - Retrieving the record "
								+ nb_line_tDBInput_1 + ".");

						/**
						 * [tDBInput_1 begin ] stop
						 */

						/**
						 * [tDBInput_1 main ] start
						 */

						currentComponent = "tDBInput_1";

						tos_count_tDBInput_1++;

						/**
						 * [tDBInput_1 main ] stop
						 */

						/**
						 * [tDBInput_1 process_data_begin ] start
						 */

						currentComponent = "tDBInput_1";

						/**
						 * [tDBInput_1 process_data_begin ] stop
						 */

						/**
						 * [tMap_2 main ] start
						 */

						currentComponent = "tMap_2";

						// row3
						// row3

						if (execStat) {
							runStat.updateStatOnConnection("row3" + iterateId,
									1, 1);
						}

						if (log.isTraceEnabled()) {
							log.trace("row3 - "
									+ (row3 == null ? "" : row3.toLogString()));
						}

						boolean hasCasePrimitiveKeyWithNull_tMap_2 = false;

						// ###############################
						// # Input tables (lookups)
						boolean rejectedInnerJoin_tMap_2 = false;
						boolean mainRowRejected_tMap_2 = false;

						// ###############################
						{ // start of Var scope

							// ###############################
							// # Vars tables

							Var__tMap_2__Struct Var = Var__tMap_2;// ###############################
							// ###############################
							// # Output tables

							voucher = null;

							// # Output table : 'voucher'
							count_voucher_tMap_2++;

							voucher_tmp.site_ref = "QBPCORP";
							voucher_tmp.vend_num = row3.Employee_ID;
							voucher_tmp.voucher = row3.Voucher;
							voucher_tmp.type = null;
							voucher_tmp.dist_date = (row3.Process_Date == null || row3.Process_Date
									.equals("")) ? TalendDate.getCurrentDate()
									: row3.Process_Date;
							voucher_tmp.po_num = "";
							voucher_tmp.inv_num = row3.PSID;
							voucher_tmp.inv_date = (row3.Process_Date == null || row3.Process_Date
									.equals("")) ? TalendDate.getCurrentDate()
									: row3.Process_Date;
							voucher_tmp.inv_amt = row3.Amount;
							voucher_tmp.non_disc_amt = null;
							voucher_tmp.due_days = null;
							voucher_tmp.due_date = null;
							voucher_tmp.disc_days = 0;
							voucher_tmp.disc_date = (row3.Process_Date == null || row3.Process_Date
									.equals("")) ? TalendDate.getCurrentDate()
									: row3.Process_Date;
							voucher_tmp.disc_pct = 0;
							voucher_tmp.disc_amt = null;
							voucher_tmp.ap_acct = null;
							voucher_tmp.ref = null;
							voucher_tmp.post_from_po = null;
							voucher_tmp.txt = null;
							voucher_tmp.prox_day = null;
							voucher_tmp.exch_rate = null;
							voucher_tmp.includes_tax = null;
							voucher_tmp.purch_amt = null;
							voucher_tmp.misc_charges = null;
							voucher_tmp.sales_tax = null;
							voucher_tmp.sales_tax_2 = null;
							voucher_tmp.freight = null;
							voucher_tmp.duty_amt = null;
							voucher_tmp.brokerage_amt = null;
							voucher_tmp.tax_code1 = null;
							voucher_tmp.tax_code2 = null;
							voucher_tmp.ap_acct_unit1 = null;
							voucher_tmp.ap_acct_unit2 = null;
							voucher_tmp.ap_acct_unit3 = null;
							voucher_tmp.ap_acct_unit4 = null;
							voucher_tmp.auth_status = null;
							voucher_tmp.fixed_rate = null;
							voucher_tmp.prox_code = null;
							voucher_tmp.grn_num = null;
							voucher_tmp.pre_register = null;
							voucher_tmp.authorizer = null;
							voucher_tmp.insurance_amt = null;
							voucher_tmp.loc_frt_amt = null;
							voucher_tmp.include_tax_in_cost = null;
							voucher_tmp.tax_date = null;
							voucher_tmp.builder_po_orig_site = null;
							voucher_tmp.builder_po_num = null;
							voucher_tmp.builder_voucher_orig_site = null;
							voucher_tmp.builder_voucher = null;
							voucher_tmp.auto_vouchered = 0;
							voucher_tmp.cancellation = 0;
							voucher_tmp.fiscal_rpt_system_type = null;
							voucher = voucher_tmp;
							log.debug("tMap_2 - Outputting the record "
									+ count_voucher_tMap_2
									+ " of the output table 'voucher'.");

							// ###############################

						} // end of Var scope

						rejectedInnerJoin_tMap_2 = false;

						tos_count_tMap_2++;

						/**
						 * [tMap_2 main ] stop
						 */

						/**
						 * [tMap_2 process_data_begin ] start
						 */

						currentComponent = "tMap_2";

						/**
						 * [tMap_2 process_data_begin ] stop
						 */
						// Start of branch "voucher"
						if (voucher != null) {

							/**
							 * [tDBOutput_4 main ] start
							 */

							currentComponent = "tDBOutput_4";

							// voucher
							// voucher

							if (execStat) {
								runStat.updateStatOnConnection("voucher"
										+ iterateId, 1, 1);
							}

							if (log.isTraceEnabled()) {
								log.trace("voucher - "
										+ (voucher == null ? "" : voucher
												.toLogString()));
							}

							whetherReject_tDBOutput_4 = false;
							if (voucher.site_ref == null) {
								pstmt_tDBOutput_4.setNull(1,
										java.sql.Types.VARCHAR);
							} else {
								pstmt_tDBOutput_4
										.setString(1, voucher.site_ref);
							}

							if (voucher.vend_num == null) {
								pstmt_tDBOutput_4.setNull(2,
										java.sql.Types.VARCHAR);
							} else {
								pstmt_tDBOutput_4
										.setString(2, voucher.vend_num);
							}

							pstmt_tDBOutput_4.setInt(3, voucher.voucher);

							if (voucher.type == null) {
								pstmt_tDBOutput_4.setNull(4,
										java.sql.Types.VARCHAR);
							} else {
								pstmt_tDBOutput_4.setString(4, voucher.type);
							}

							if (voucher.dist_date != null) {
								pstmt_tDBOutput_4.setTimestamp(5,
										new java.sql.Timestamp(
												voucher.dist_date.getTime()));
							} else {
								pstmt_tDBOutput_4.setNull(5,
										java.sql.Types.DATE);
							}

							if (voucher.po_num == null) {
								pstmt_tDBOutput_4.setNull(6,
										java.sql.Types.VARCHAR);
							} else {
								pstmt_tDBOutput_4.setString(6, voucher.po_num);
							}

							if (voucher.inv_num == null) {
								pstmt_tDBOutput_4.setNull(7,
										java.sql.Types.VARCHAR);
							} else {
								pstmt_tDBOutput_4.setString(7, voucher.inv_num);
							}

							if (voucher.inv_date != null) {
								pstmt_tDBOutput_4.setTimestamp(
										8,
										new java.sql.Timestamp(voucher.inv_date
												.getTime()));
							} else {
								pstmt_tDBOutput_4.setNull(8,
										java.sql.Types.DATE);
							}

							if (voucher.inv_amt == null) {
								pstmt_tDBOutput_4.setNull(9,
										java.sql.Types.FLOAT);
							} else {
								pstmt_tDBOutput_4.setFloat(9, voucher.inv_amt);
							}

							if (voucher.non_disc_amt == null) {
								pstmt_tDBOutput_4.setNull(10,
										java.sql.Types.FLOAT);
							} else {
								pstmt_tDBOutput_4.setFloat(10,
										voucher.non_disc_amt);
							}

							if (voucher.due_days == null) {
								pstmt_tDBOutput_4.setNull(11,
										java.sql.Types.INTEGER);
							} else {
								pstmt_tDBOutput_4.setInt(11, voucher.due_days);
							}

							if (voucher.due_date != null) {
								pstmt_tDBOutput_4.setTimestamp(
										12,
										new java.sql.Timestamp(voucher.due_date
												.getTime()));
							} else {
								pstmt_tDBOutput_4.setNull(12,
										java.sql.Types.DATE);
							}

							if (voucher.disc_days == null) {
								pstmt_tDBOutput_4.setNull(13,
										java.sql.Types.INTEGER);
							} else {
								pstmt_tDBOutput_4.setInt(13, voucher.disc_days);
							}

							if (voucher.disc_date != null) {
								pstmt_tDBOutput_4.setTimestamp(14,
										new java.sql.Timestamp(
												voucher.disc_date.getTime()));
							} else {
								pstmt_tDBOutput_4.setNull(14,
										java.sql.Types.DATE);
							}

							if (voucher.disc_pct == null) {
								pstmt_tDBOutput_4.setNull(15,
										java.sql.Types.INTEGER);
							} else {
								pstmt_tDBOutput_4.setInt(15, voucher.disc_pct);
							}

							if (voucher.disc_amt == null) {
								pstmt_tDBOutput_4.setNull(16,
										java.sql.Types.FLOAT);
							} else {
								pstmt_tDBOutput_4
										.setFloat(16, voucher.disc_amt);
							}

							if (voucher.ap_acct == null) {
								pstmt_tDBOutput_4.setNull(17,
										java.sql.Types.VARCHAR);
							} else {
								pstmt_tDBOutput_4
										.setString(17, voucher.ap_acct);
							}

							if (voucher.ref == null) {
								pstmt_tDBOutput_4.setNull(18,
										java.sql.Types.VARCHAR);
							} else {
								pstmt_tDBOutput_4.setString(18, voucher.ref);
							}

							if (voucher.post_from_po == null) {
								pstmt_tDBOutput_4.setNull(19,
										java.sql.Types.INTEGER);
							} else {
								pstmt_tDBOutput_4.setByte(19,
										voucher.post_from_po);
							}

							if (voucher.txt == null) {
								pstmt_tDBOutput_4.setNull(20,
										java.sql.Types.VARCHAR);
							} else {
								pstmt_tDBOutput_4.setString(20, voucher.txt);
							}

							if (voucher.prox_day == null) {
								pstmt_tDBOutput_4.setNull(21,
										java.sql.Types.INTEGER);
							} else {
								pstmt_tDBOutput_4.setInt(21, voucher.prox_day);
							}

							if (voucher.exch_rate == null) {
								pstmt_tDBOutput_4.setNull(22,
										java.sql.Types.FLOAT);
							} else {
								pstmt_tDBOutput_4.setFloat(22,
										voucher.exch_rate);
							}

							if (voucher.includes_tax == null) {
								pstmt_tDBOutput_4.setNull(23,
										java.sql.Types.INTEGER);
							} else {
								pstmt_tDBOutput_4.setInt(23,
										voucher.includes_tax);
							}

							if (voucher.purch_amt == null) {
								pstmt_tDBOutput_4.setNull(24,
										java.sql.Types.FLOAT);
							} else {
								pstmt_tDBOutput_4.setFloat(24,
										voucher.purch_amt);
							}

							if (voucher.misc_charges == null) {
								pstmt_tDBOutput_4.setNull(25,
										java.sql.Types.FLOAT);
							} else {
								pstmt_tDBOutput_4.setFloat(25,
										voucher.misc_charges);
							}

							if (voucher.sales_tax == null) {
								pstmt_tDBOutput_4.setNull(26,
										java.sql.Types.FLOAT);
							} else {
								pstmt_tDBOutput_4.setFloat(26,
										voucher.sales_tax);
							}

							if (voucher.sales_tax_2 == null) {
								pstmt_tDBOutput_4.setNull(27,
										java.sql.Types.FLOAT);
							} else {
								pstmt_tDBOutput_4.setFloat(27,
										voucher.sales_tax_2);
							}

							if (voucher.freight == null) {
								pstmt_tDBOutput_4.setNull(28,
										java.sql.Types.FLOAT);
							} else {
								pstmt_tDBOutput_4.setFloat(28, voucher.freight);
							}

							if (voucher.duty_amt == null) {
								pstmt_tDBOutput_4.setNull(29,
										java.sql.Types.FLOAT);
							} else {
								pstmt_tDBOutput_4
										.setFloat(29, voucher.duty_amt);
							}

							if (voucher.brokerage_amt == null) {
								pstmt_tDBOutput_4.setNull(30,
										java.sql.Types.FLOAT);
							} else {
								pstmt_tDBOutput_4.setFloat(30,
										voucher.brokerage_amt);
							}

							if (voucher.tax_code1 == null) {
								pstmt_tDBOutput_4.setNull(31,
										java.sql.Types.VARCHAR);
							} else {
								pstmt_tDBOutput_4.setString(31,
										voucher.tax_code1);
							}

							if (voucher.tax_code2 == null) {
								pstmt_tDBOutput_4.setNull(32,
										java.sql.Types.VARCHAR);
							} else {
								pstmt_tDBOutput_4.setString(32,
										voucher.tax_code2);
							}

							if (voucher.ap_acct_unit1 == null) {
								pstmt_tDBOutput_4.setNull(33,
										java.sql.Types.VARCHAR);
							} else {
								pstmt_tDBOutput_4.setString(33,
										voucher.ap_acct_unit1);
							}

							if (voucher.ap_acct_unit2 == null) {
								pstmt_tDBOutput_4.setNull(34,
										java.sql.Types.VARCHAR);
							} else {
								pstmt_tDBOutput_4.setString(34,
										voucher.ap_acct_unit2);
							}

							if (voucher.ap_acct_unit3 == null) {
								pstmt_tDBOutput_4.setNull(35,
										java.sql.Types.VARCHAR);
							} else {
								pstmt_tDBOutput_4.setString(35,
										voucher.ap_acct_unit3);
							}

							if (voucher.ap_acct_unit4 == null) {
								pstmt_tDBOutput_4.setNull(36,
										java.sql.Types.VARCHAR);
							} else {
								pstmt_tDBOutput_4.setString(36,
										voucher.ap_acct_unit4);
							}

							if (voucher.auth_status == null) {
								pstmt_tDBOutput_4.setNull(37,
										java.sql.Types.VARCHAR);
							} else {
								pstmt_tDBOutput_4.setString(37,
										voucher.auth_status);
							}

							if (voucher.fixed_rate == null) {
								pstmt_tDBOutput_4.setNull(38,
										java.sql.Types.INTEGER);
							} else {
								pstmt_tDBOutput_4.setShort(38,
										voucher.fixed_rate);
							}

							if (voucher.prox_code == null) {
								pstmt_tDBOutput_4.setNull(39,
										java.sql.Types.INTEGER);
							} else {
								pstmt_tDBOutput_4.setShort(39,
										voucher.prox_code);
							}

							if (voucher.grn_num == null) {
								pstmt_tDBOutput_4.setNull(40,
										java.sql.Types.VARCHAR);
							} else {
								pstmt_tDBOutput_4
										.setString(40, voucher.grn_num);
							}

							if (voucher.pre_register == null) {
								pstmt_tDBOutput_4.setNull(41,
										java.sql.Types.INTEGER);
							} else {
								pstmt_tDBOutput_4.setInt(41,
										voucher.pre_register);
							}

							if (voucher.authorizer == null) {
								pstmt_tDBOutput_4.setNull(42,
										java.sql.Types.VARCHAR);
							} else {
								pstmt_tDBOutput_4.setString(42,
										voucher.authorizer);
							}

							if (voucher.insurance_amt == null) {
								pstmt_tDBOutput_4.setNull(43,
										java.sql.Types.FLOAT);
							} else {
								pstmt_tDBOutput_4.setFloat(43,
										voucher.insurance_amt);
							}

							if (voucher.loc_frt_amt == null) {
								pstmt_tDBOutput_4.setNull(44,
										java.sql.Types.FLOAT);
							} else {
								pstmt_tDBOutput_4.setFloat(44,
										voucher.loc_frt_amt);
							}

							if (voucher.include_tax_in_cost == null) {
								pstmt_tDBOutput_4.setNull(45,
										java.sql.Types.INTEGER);
							} else {
								pstmt_tDBOutput_4.setByte(45,
										voucher.include_tax_in_cost);
							}

							if (voucher.tax_date != null) {
								pstmt_tDBOutput_4.setTimestamp(
										46,
										new java.sql.Timestamp(voucher.tax_date
												.getTime()));
							} else {
								pstmt_tDBOutput_4.setNull(46,
										java.sql.Types.DATE);
							}

							if (voucher.builder_po_orig_site == null) {
								pstmt_tDBOutput_4.setNull(47,
										java.sql.Types.VARCHAR);
							} else {
								pstmt_tDBOutput_4.setString(47,
										voucher.builder_po_orig_site);
							}

							if (voucher.builder_po_num == null) {
								pstmt_tDBOutput_4.setNull(48,
										java.sql.Types.VARCHAR);
							} else {
								pstmt_tDBOutput_4.setString(48,
										voucher.builder_po_num);
							}

							if (voucher.builder_voucher_orig_site == null) {
								pstmt_tDBOutput_4.setNull(49,
										java.sql.Types.VARCHAR);
							} else {
								pstmt_tDBOutput_4.setString(49,
										voucher.builder_voucher_orig_site);
							}

							if (voucher.builder_voucher == null) {
								pstmt_tDBOutput_4.setNull(50,
										java.sql.Types.VARCHAR);
							} else {
								pstmt_tDBOutput_4.setString(50,
										voucher.builder_voucher);
							}

							if (voucher.auto_vouchered == null) {
								pstmt_tDBOutput_4.setNull(51,
										java.sql.Types.INTEGER);
							} else {
								pstmt_tDBOutput_4.setByte(51,
										voucher.auto_vouchered);
							}

							if (voucher.cancellation == null) {
								pstmt_tDBOutput_4.setNull(52,
										java.sql.Types.INTEGER);
							} else {
								pstmt_tDBOutput_4.setByte(52,
										voucher.cancellation);
							}

							if (voucher.fiscal_rpt_system_type == null) {
								pstmt_tDBOutput_4.setNull(53,
										java.sql.Types.VARCHAR);
							} else {
								pstmt_tDBOutput_4.setString(53,
										voucher.fiscal_rpt_system_type);
							}

							pstmt_tDBOutput_4.addBatch();
							nb_line_tDBOutput_4++;

							if (log.isDebugEnabled())
								log.debug("tDBOutput_4 - "
										+ ("Adding the record ")
										+ (nb_line_tDBOutput_4) + (" to the ")
										+ ("INSERT") + (" batch."));
							batchSizeCounter_tDBOutput_4++;

							// ////////batch execute by batch size///////
							class LimitBytesHelper_tDBOutput_4 {
								public int limitBytePart1(
										int counter,
										java.sql.PreparedStatement pstmt_tDBOutput_4)
										throws Exception {
									try {

										if (log.isDebugEnabled())
											log.debug("tDBOutput_4 - "
													+ ("Executing the ")
													+ ("INSERT") + (" batch."));
										for (int countEach_tDBOutput_4 : pstmt_tDBOutput_4
												.executeBatch()) {
											if (countEach_tDBOutput_4 == -2
													|| countEach_tDBOutput_4 == -3) {
												break;
											}
											counter += countEach_tDBOutput_4;
										}

										if (log.isDebugEnabled())
											log.debug("tDBOutput_4 - "
													+ ("The ")
													+ ("INSERT")
													+ (" batch execution has succeeded."));
									} catch (java.sql.BatchUpdateException e) {

										int countSum_tDBOutput_4 = 0;
										for (int countEach_tDBOutput_4 : e
												.getUpdateCounts()) {
											counter += (countEach_tDBOutput_4 < 0 ? 0
													: countEach_tDBOutput_4);
										}

										log.error("tDBOutput_4 - "
												+ (e.getMessage()));
										System.err.println(e.getMessage());

									}
									return counter;
								}

								public int limitBytePart2(
										int counter,
										java.sql.PreparedStatement pstmt_tDBOutput_4)
										throws Exception {
									try {

										if (log.isDebugEnabled())
											log.debug("tDBOutput_4 - "
													+ ("Executing the ")
													+ ("INSERT") + (" batch."));
										for (int countEach_tDBOutput_4 : pstmt_tDBOutput_4
												.executeBatch()) {
											if (countEach_tDBOutput_4 == -2
													|| countEach_tDBOutput_4 == -3) {
												break;
											}
											counter += countEach_tDBOutput_4;
										}

										if (log.isDebugEnabled())
											log.debug("tDBOutput_4 - "
													+ ("The ")
													+ ("INSERT")
													+ (" batch execution has succeeded."));
									} catch (java.sql.BatchUpdateException e) {

										for (int countEach_tDBOutput_4 : e
												.getUpdateCounts()) {
											counter += (countEach_tDBOutput_4 < 0 ? 0
													: countEach_tDBOutput_4);
										}

										log.error("tDBOutput_4 - "
												+ (e.getMessage()));
										System.err.println(e.getMessage());

									}
									return counter;
								}
							}
							if ((batchSize_tDBOutput_4 > 0)
									&& (batchSize_tDBOutput_4 <= batchSizeCounter_tDBOutput_4)) {

								insertedCount_tDBOutput_4 = new LimitBytesHelper_tDBOutput_4()
										.limitBytePart1(
												insertedCount_tDBOutput_4,
												pstmt_tDBOutput_4);

								batchSizeCounter_tDBOutput_4 = 0;
							}

							// //////////commit every////////////

							commitCounter_tDBOutput_4++;
							if (commitEvery_tDBOutput_4 <= commitCounter_tDBOutput_4) {
								if ((batchSize_tDBOutput_4 > 0)
										&& (batchSizeCounter_tDBOutput_4 > 0)) {

									insertedCount_tDBOutput_4 = new LimitBytesHelper_tDBOutput_4()
											.limitBytePart1(
													insertedCount_tDBOutput_4,
													pstmt_tDBOutput_4);

									batchSizeCounter_tDBOutput_4 = 0;
								}

								if (log.isDebugEnabled())
									log.debug("tDBOutput_4 - "
											+ ("Connection starting to commit ")
											+ (commitCounter_tDBOutput_4)
											+ (" record(s)."));
								conn_tDBOutput_4.commit();

								if (log.isDebugEnabled())
									log.debug("tDBOutput_4 - "
											+ ("Connection commit has succeeded."));
								commitCounter_tDBOutput_4 = 0;
							}

							tos_count_tDBOutput_4++;

							/**
							 * [tDBOutput_4 main ] stop
							 */

							/**
							 * [tDBOutput_4 process_data_begin ] start
							 */

							currentComponent = "tDBOutput_4";

							/**
							 * [tDBOutput_4 process_data_begin ] stop
							 */

							/**
							 * [tDBOutput_4 process_data_end ] start
							 */

							currentComponent = "tDBOutput_4";

							/**
							 * [tDBOutput_4 process_data_end ] stop
							 */

						} // End of branch "voucher"

						/**
						 * [tMap_2 process_data_end ] start
						 */

						currentComponent = "tMap_2";

						/**
						 * [tMap_2 process_data_end ] stop
						 */

						/**
						 * [tDBInput_1 process_data_end ] start
						 */

						currentComponent = "tDBInput_1";

						/**
						 * [tDBInput_1 process_data_end ] stop
						 */

						/**
						 * [tDBInput_1 end ] start
						 */

						currentComponent = "tDBInput_1";

					}
				} finally {
					if (rs_tDBInput_1 != null) {
						rs_tDBInput_1.close();
					}
					if (stmt_tDBInput_1 != null) {
						stmt_tDBInput_1.close();
					}
					if (conn_tDBInput_1 != null && !conn_tDBInput_1.isClosed()) {

						log.debug("tDBInput_1 - Closing the connection to the database.");

						conn_tDBInput_1.close();

						log.debug("tDBInput_1 - Connection to the database closed.");

					}
				}
				globalMap.put("tDBInput_1_NB_LINE", nb_line_tDBInput_1);
				log.debug("tDBInput_1 - Retrieved records count: "
						+ nb_line_tDBInput_1 + " .");

				if (log.isDebugEnabled())
					log.debug("tDBInput_1 - " + ("Done."));

				ok_Hash.put("tDBInput_1", true);
				end_Hash.put("tDBInput_1", System.currentTimeMillis());

				/**
				 * [tDBInput_1 end ] stop
				 */

				/**
				 * [tMap_2 end ] start
				 */

				currentComponent = "tMap_2";

				// ###############################
				// # Lookup hashes releasing
				// ###############################
				log.debug("tMap_2 - Written records count in the table 'voucher': "
						+ count_voucher_tMap_2 + ".");

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null
							|| !((Boolean) resourceMap.get("inIterateVComp"))) {
						runStat.updateStatOnConnection("row3" + iterateId, 2, 0);
					}
				}

				if (log.isDebugEnabled())
					log.debug("tMap_2 - " + ("Done."));

				ok_Hash.put("tMap_2", true);
				end_Hash.put("tMap_2", System.currentTimeMillis());

				/**
				 * [tMap_2 end ] stop
				 */

				/**
				 * [tDBOutput_4 end ] start
				 */

				currentComponent = "tDBOutput_4";

				try {
					int countSum_tDBOutput_4 = 0;
					if (pstmt_tDBOutput_4 != null
							&& batchSizeCounter_tDBOutput_4 > 0) {

						if (log.isDebugEnabled())
							log.debug("tDBOutput_4 - " + ("Executing the ")
									+ ("INSERT") + (" batch."));
						for (int countEach_tDBOutput_4 : pstmt_tDBOutput_4
								.executeBatch()) {
							if (countEach_tDBOutput_4 == -2
									|| countEach_tDBOutput_4 == -3) {
								break;
							}
							countSum_tDBOutput_4 += countEach_tDBOutput_4;
						}

						if (log.isDebugEnabled())
							log.debug("tDBOutput_4 - " + ("The ") + ("INSERT")
									+ (" batch execution has succeeded."));
					}

					insertedCount_tDBOutput_4 += countSum_tDBOutput_4;

				} catch (java.sql.BatchUpdateException e) {

					int countSum_tDBOutput_4 = 0;
					for (int countEach_tDBOutput_4 : e.getUpdateCounts()) {
						countSum_tDBOutput_4 += (countEach_tDBOutput_4 < 0 ? 0
								: countEach_tDBOutput_4);
					}

					insertedCount_tDBOutput_4 += countSum_tDBOutput_4;

					log.error("tDBOutput_4 - " + (e.getMessage()));
					System.err.println(e.getMessage());

				}
				if (pstmt_tDBOutput_4 != null) {

					pstmt_tDBOutput_4.close();
					resourceMap.remove("pstmt_tDBOutput_4");

				}
				resourceMap.put("statementClosed_tDBOutput_4", true);
				if (log.isDebugEnabled())
					log.debug("tDBOutput_4 - "
							+ ("Connection starting to commit ")
							+ (commitCounter_tDBOutput_4) + (" record(s)."));
				conn_tDBOutput_4.commit();

				if (log.isDebugEnabled())
					log.debug("tDBOutput_4 - "
							+ ("Connection commit has succeeded."));
				if (log.isDebugEnabled())
					log.debug("tDBOutput_4 - "
							+ ("Closing the connection to the database."));
				conn_tDBOutput_4.close();
				if (log.isDebugEnabled())
					log.debug("tDBOutput_4 - "
							+ ("Connection to the database has closed."));
				resourceMap.put("finish_tDBOutput_4", true);

				nb_line_deleted_tDBOutput_4 = nb_line_deleted_tDBOutput_4
						+ deletedCount_tDBOutput_4;
				nb_line_update_tDBOutput_4 = nb_line_update_tDBOutput_4
						+ updatedCount_tDBOutput_4;
				nb_line_inserted_tDBOutput_4 = nb_line_inserted_tDBOutput_4
						+ insertedCount_tDBOutput_4;
				nb_line_rejected_tDBOutput_4 = nb_line_rejected_tDBOutput_4
						+ rejectedCount_tDBOutput_4;

				globalMap.put("tDBOutput_4_NB_LINE", nb_line_tDBOutput_4);
				globalMap.put("tDBOutput_4_NB_LINE_UPDATED",
						nb_line_update_tDBOutput_4);
				globalMap.put("tDBOutput_4_NB_LINE_INSERTED",
						nb_line_inserted_tDBOutput_4);
				globalMap.put("tDBOutput_4_NB_LINE_DELETED",
						nb_line_deleted_tDBOutput_4);
				globalMap.put("tDBOutput_4_NB_LINE_REJECTED",
						nb_line_rejected_tDBOutput_4);

				if (log.isDebugEnabled())
					log.debug("tDBOutput_4 - " + ("Has ") + ("inserted")
							+ (" ") + (nb_line_inserted_tDBOutput_4)
							+ (" record(s)."));

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null
							|| !((Boolean) resourceMap.get("inIterateVComp"))) {
						runStat.updateStatOnConnection("voucher" + iterateId,
								2, 0);
					}
				}

				if (log.isDebugEnabled())
					log.debug("tDBOutput_4 - " + ("Done."));

				ok_Hash.put("tDBOutput_4", true);
				end_Hash.put("tDBOutput_4", System.currentTimeMillis());

				/**
				 * [tDBOutput_4 end ] stop
				 */

			}// end the resume

		} catch (java.lang.Exception e) {

			if (!(e instanceof TalendException)) {
				log.fatal(currentComponent + " " + e.getMessage(), e);
			}

			TalendException te = new TalendException(e, currentComponent,
					globalMap);

			throw te;
		} catch (java.lang.Error error) {

			runStat.stopThreadStat();

			throw error;
		} finally {

			try {

				/**
				 * [tDBInput_1 finally ] start
				 */

				currentComponent = "tDBInput_1";

				/**
				 * [tDBInput_1 finally ] stop
				 */

				/**
				 * [tMap_2 finally ] start
				 */

				currentComponent = "tMap_2";

				/**
				 * [tMap_2 finally ] stop
				 */

				/**
				 * [tDBOutput_4 finally ] start
				 */

				currentComponent = "tDBOutput_4";

				try {
					if (resourceMap.get("statementClosed_tDBOutput_4") == null) {
						java.sql.PreparedStatement pstmtToClose_tDBOutput_4 = null;
						if ((pstmtToClose_tDBOutput_4 = (java.sql.PreparedStatement) resourceMap
								.remove("pstmt_tDBOutput_4")) != null) {
							pstmtToClose_tDBOutput_4.close();
						}
					}
				} finally {
					if (resourceMap.get("finish_tDBOutput_4") == null) {
						java.sql.Connection ctn_tDBOutput_4 = null;
						if ((ctn_tDBOutput_4 = (java.sql.Connection) resourceMap
								.get("conn_tDBOutput_4")) != null) {
							try {
								if (log.isDebugEnabled())
									log.debug("tDBOutput_4 - "
											+ ("Closing the connection to the database."));
								ctn_tDBOutput_4.close();
								if (log.isDebugEnabled())
									log.debug("tDBOutput_4 - "
											+ ("Connection to the database has closed."));
							} catch (java.sql.SQLException sqlEx_tDBOutput_4) {
								String errorMessage_tDBOutput_4 = "failed to close the connection in tDBOutput_4 :"
										+ sqlEx_tDBOutput_4.getMessage();
								log.error("tDBOutput_4 - "
										+ (errorMessage_tDBOutput_4));
								System.err.println(errorMessage_tDBOutput_4);
							}
						}
					}
				}

				/**
				 * [tDBOutput_4 finally ] stop
				 */

			} catch (java.lang.Exception e) {
				// ignore
			} catch (java.lang.Error error) {
				// ignore
			}
			resourceMap = null;
		}

		globalMap.put("tDBInput_1_SUBPROCESS_STATE", 1);
	}

	public static class distributionStruct implements
			routines.system.IPersistableRow<distributionStruct> {
		final static byte[] commonByteArrayLock_DEV_AP_Transactions = new byte[0];
		static byte[] commonByteArray_DEV_AP_Transactions = new byte[0];
		protected static final int DEFAULT_HASHCODE = 1;
		protected static final int PRIME = 31;
		protected int hashCode = DEFAULT_HASHCODE;
		public boolean hashCodeDirty = true;

		public String loopKey;

		public String site_ref;

		public String getSite_ref() {
			return this.site_ref;
		}

		public String vend_num;

		public String getVend_num() {
			return this.vend_num;
		}

		public int voucher;

		public int getVoucher() {
			return this.voucher;
		}

		public int dist_seq;

		public int getDist_seq() {
			return this.dist_seq;
		}

		public String acct;

		public String getAcct() {
			return this.acct;
		}

		public Float amount;

		public Float getAmount() {
			return this.amount;
		}

		public String inv_num;

		public String getInv_num() {
			return this.inv_num;
		}

		public String tax_code;

		public String getTax_code() {
			return this.tax_code;
		}

		public String tax_basis;

		public String getTax_basis() {
			return this.tax_basis;
		}

		public String tax_system;

		public String getTax_system() {
			return this.tax_system;
		}

		public String tax_code_e;

		public String getTax_code_e() {
			return this.tax_code_e;
		}

		public String acct_unit1;

		public String getAcct_unit1() {
			return this.acct_unit1;
		}

		public String acct_unit2;

		public String getAcct_unit2() {
			return this.acct_unit2;
		}

		public String acct_unit3;

		public String getAcct_unit3() {
			return this.acct_unit3;
		}

		public String acct_unit4;

		public String getAcct_unit4() {
			return this.acct_unit4;
		}

		public String proj_num;

		public String getProj_num() {
			return this.proj_num;
		}

		public String task_num;

		public String getTask_num() {
			return this.task_num;
		}

		public String cost_code;

		public String getCost_code() {
			return this.cost_code;
		}

		public String MX_iso_country_code;

		public String getMX_iso_country_code() {
			return this.MX_iso_country_code;
		}

		public String MX_ietu_deduction_pct;

		public String getMX_ietu_deduction_pct() {
			return this.MX_ietu_deduction_pct;
		}

		public String MX_diot_trans_type;

		public String getMX_diot_trans_type() {
			return this.MX_diot_trans_type;
		}

		public String MX_foreign_tax_reg_num;

		public String getMX_foreign_tax_reg_num() {
			return this.MX_foreign_tax_reg_num;
		}

		public String MX_tax_reg_num;

		public String getMX_tax_reg_num() {
			return this.MX_tax_reg_num;
		}

		public String MX_tax_reg_num_type;

		public String getMX_tax_reg_num_type() {
			return this.MX_tax_reg_num_type;
		}

		public String MX_vendor_name;

		public String getMX_vendor_name() {
			return this.MX_vendor_name;
		}

		@Override
		public int hashCode() {
			if (this.hashCodeDirty) {
				final int prime = PRIME;
				int result = DEFAULT_HASHCODE;

				result = prime
						* result
						+ ((this.site_ref == null) ? 0 : this.site_ref
								.hashCode());

				result = prime
						* result
						+ ((this.vend_num == null) ? 0 : this.vend_num
								.hashCode());

				result = prime * result + (int) this.voucher;

				result = prime * result + (int) this.dist_seq;

				this.hashCode = result;
				this.hashCodeDirty = false;
			}
			return this.hashCode;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			final distributionStruct other = (distributionStruct) obj;

			if (this.site_ref == null) {
				if (other.site_ref != null)
					return false;

			} else if (!this.site_ref.equals(other.site_ref))

				return false;

			if (this.vend_num == null) {
				if (other.vend_num != null)
					return false;

			} else if (!this.vend_num.equals(other.vend_num))

				return false;

			if (this.voucher != other.voucher)
				return false;

			if (this.dist_seq != other.dist_seq)
				return false;

			return true;
		}

		public void copyDataTo(distributionStruct other) {

			other.site_ref = this.site_ref;
			other.vend_num = this.vend_num;
			other.voucher = this.voucher;
			other.dist_seq = this.dist_seq;
			other.acct = this.acct;
			other.amount = this.amount;
			other.inv_num = this.inv_num;
			other.tax_code = this.tax_code;
			other.tax_basis = this.tax_basis;
			other.tax_system = this.tax_system;
			other.tax_code_e = this.tax_code_e;
			other.acct_unit1 = this.acct_unit1;
			other.acct_unit2 = this.acct_unit2;
			other.acct_unit3 = this.acct_unit3;
			other.acct_unit4 = this.acct_unit4;
			other.proj_num = this.proj_num;
			other.task_num = this.task_num;
			other.cost_code = this.cost_code;
			other.MX_iso_country_code = this.MX_iso_country_code;
			other.MX_ietu_deduction_pct = this.MX_ietu_deduction_pct;
			other.MX_diot_trans_type = this.MX_diot_trans_type;
			other.MX_foreign_tax_reg_num = this.MX_foreign_tax_reg_num;
			other.MX_tax_reg_num = this.MX_tax_reg_num;
			other.MX_tax_reg_num_type = this.MX_tax_reg_num_type;
			other.MX_vendor_name = this.MX_vendor_name;

		}

		public void copyKeysDataTo(distributionStruct other) {

			other.site_ref = this.site_ref;
			other.vend_num = this.vend_num;
			other.voucher = this.voucher;
			other.dist_seq = this.dist_seq;

		}

		private String readString(ObjectInputStream dis) throws IOException {
			String strReturn = null;
			int length = 0;
			length = dis.readInt();
			if (length == -1) {
				strReturn = null;
			} else {
				if (length > commonByteArray_DEV_AP_Transactions.length) {
					if (length < 1024
							&& commonByteArray_DEV_AP_Transactions.length == 0) {
						commonByteArray_DEV_AP_Transactions = new byte[1024];
					} else {
						commonByteArray_DEV_AP_Transactions = new byte[2 * length];
					}
				}
				dis.readFully(commonByteArray_DEV_AP_Transactions, 0, length);
				strReturn = new String(commonByteArray_DEV_AP_Transactions, 0,
						length, utf8Charset);
			}
			return strReturn;
		}

		private void writeString(String str, ObjectOutputStream dos)
				throws IOException {
			if (str == null) {
				dos.writeInt(-1);
			} else {
				byte[] byteArray = str.getBytes(utf8Charset);
				dos.writeInt(byteArray.length);
				dos.write(byteArray);
			}
		}

		public void readData(ObjectInputStream dis) {

			synchronized (commonByteArrayLock_DEV_AP_Transactions) {

				try {

					int length = 0;

					this.site_ref = readString(dis);

					this.vend_num = readString(dis);

					this.voucher = dis.readInt();

					this.dist_seq = dis.readInt();

					this.acct = readString(dis);

					length = dis.readByte();
					if (length == -1) {
						this.amount = null;
					} else {
						this.amount = dis.readFloat();
					}

					this.inv_num = readString(dis);

					this.tax_code = readString(dis);

					this.tax_basis = readString(dis);

					this.tax_system = readString(dis);

					this.tax_code_e = readString(dis);

					this.acct_unit1 = readString(dis);

					this.acct_unit2 = readString(dis);

					this.acct_unit3 = readString(dis);

					this.acct_unit4 = readString(dis);

					this.proj_num = readString(dis);

					this.task_num = readString(dis);

					this.cost_code = readString(dis);

					this.MX_iso_country_code = readString(dis);

					this.MX_ietu_deduction_pct = readString(dis);

					this.MX_diot_trans_type = readString(dis);

					this.MX_foreign_tax_reg_num = readString(dis);

					this.MX_tax_reg_num = readString(dis);

					this.MX_tax_reg_num_type = readString(dis);

					this.MX_vendor_name = readString(dis);

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void writeData(ObjectOutputStream dos) {
			try {

				// String

				writeString(this.site_ref, dos);

				// String

				writeString(this.vend_num, dos);

				// int

				dos.writeInt(this.voucher);

				// int

				dos.writeInt(this.dist_seq);

				// String

				writeString(this.acct, dos);

				// Float

				if (this.amount == null) {
					dos.writeByte(-1);
				} else {
					dos.writeByte(0);
					dos.writeFloat(this.amount);
				}

				// String

				writeString(this.inv_num, dos);

				// String

				writeString(this.tax_code, dos);

				// String

				writeString(this.tax_basis, dos);

				// String

				writeString(this.tax_system, dos);

				// String

				writeString(this.tax_code_e, dos);

				// String

				writeString(this.acct_unit1, dos);

				// String

				writeString(this.acct_unit2, dos);

				// String

				writeString(this.acct_unit3, dos);

				// String

				writeString(this.acct_unit4, dos);

				// String

				writeString(this.proj_num, dos);

				// String

				writeString(this.task_num, dos);

				// String

				writeString(this.cost_code, dos);

				// String

				writeString(this.MX_iso_country_code, dos);

				// String

				writeString(this.MX_ietu_deduction_pct, dos);

				// String

				writeString(this.MX_diot_trans_type, dos);

				// String

				writeString(this.MX_foreign_tax_reg_num, dos);

				// String

				writeString(this.MX_tax_reg_num, dos);

				// String

				writeString(this.MX_tax_reg_num_type, dos);

				// String

				writeString(this.MX_vendor_name, dos);

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public String toString() {

			StringBuilder sb = new StringBuilder();
			sb.append(super.toString());
			sb.append("[");
			sb.append("site_ref=" + site_ref);
			sb.append(",vend_num=" + vend_num);
			sb.append(",voucher=" + String.valueOf(voucher));
			sb.append(",dist_seq=" + String.valueOf(dist_seq));
			sb.append(",acct=" + acct);
			sb.append(",amount=" + String.valueOf(amount));
			sb.append(",inv_num=" + inv_num);
			sb.append(",tax_code=" + tax_code);
			sb.append(",tax_basis=" + tax_basis);
			sb.append(",tax_system=" + tax_system);
			sb.append(",tax_code_e=" + tax_code_e);
			sb.append(",acct_unit1=" + acct_unit1);
			sb.append(",acct_unit2=" + acct_unit2);
			sb.append(",acct_unit3=" + acct_unit3);
			sb.append(",acct_unit4=" + acct_unit4);
			sb.append(",proj_num=" + proj_num);
			sb.append(",task_num=" + task_num);
			sb.append(",cost_code=" + cost_code);
			sb.append(",MX_iso_country_code=" + MX_iso_country_code);
			sb.append(",MX_ietu_deduction_pct=" + MX_ietu_deduction_pct);
			sb.append(",MX_diot_trans_type=" + MX_diot_trans_type);
			sb.append(",MX_foreign_tax_reg_num=" + MX_foreign_tax_reg_num);
			sb.append(",MX_tax_reg_num=" + MX_tax_reg_num);
			sb.append(",MX_tax_reg_num_type=" + MX_tax_reg_num_type);
			sb.append(",MX_vendor_name=" + MX_vendor_name);
			sb.append("]");

			return sb.toString();
		}

		public String toLogString() {
			StringBuilder sb = new StringBuilder();

			if (site_ref == null) {
				sb.append("<null>");
			} else {
				sb.append(site_ref);
			}

			sb.append("|");

			if (vend_num == null) {
				sb.append("<null>");
			} else {
				sb.append(vend_num);
			}

			sb.append("|");

			sb.append(voucher);

			sb.append("|");

			sb.append(dist_seq);

			sb.append("|");

			if (acct == null) {
				sb.append("<null>");
			} else {
				sb.append(acct);
			}

			sb.append("|");

			if (amount == null) {
				sb.append("<null>");
			} else {
				sb.append(amount);
			}

			sb.append("|");

			if (inv_num == null) {
				sb.append("<null>");
			} else {
				sb.append(inv_num);
			}

			sb.append("|");

			if (tax_code == null) {
				sb.append("<null>");
			} else {
				sb.append(tax_code);
			}

			sb.append("|");

			if (tax_basis == null) {
				sb.append("<null>");
			} else {
				sb.append(tax_basis);
			}

			sb.append("|");

			if (tax_system == null) {
				sb.append("<null>");
			} else {
				sb.append(tax_system);
			}

			sb.append("|");

			if (tax_code_e == null) {
				sb.append("<null>");
			} else {
				sb.append(tax_code_e);
			}

			sb.append("|");

			if (acct_unit1 == null) {
				sb.append("<null>");
			} else {
				sb.append(acct_unit1);
			}

			sb.append("|");

			if (acct_unit2 == null) {
				sb.append("<null>");
			} else {
				sb.append(acct_unit2);
			}

			sb.append("|");

			if (acct_unit3 == null) {
				sb.append("<null>");
			} else {
				sb.append(acct_unit3);
			}

			sb.append("|");

			if (acct_unit4 == null) {
				sb.append("<null>");
			} else {
				sb.append(acct_unit4);
			}

			sb.append("|");

			if (proj_num == null) {
				sb.append("<null>");
			} else {
				sb.append(proj_num);
			}

			sb.append("|");

			if (task_num == null) {
				sb.append("<null>");
			} else {
				sb.append(task_num);
			}

			sb.append("|");

			if (cost_code == null) {
				sb.append("<null>");
			} else {
				sb.append(cost_code);
			}

			sb.append("|");

			if (MX_iso_country_code == null) {
				sb.append("<null>");
			} else {
				sb.append(MX_iso_country_code);
			}

			sb.append("|");

			if (MX_ietu_deduction_pct == null) {
				sb.append("<null>");
			} else {
				sb.append(MX_ietu_deduction_pct);
			}

			sb.append("|");

			if (MX_diot_trans_type == null) {
				sb.append("<null>");
			} else {
				sb.append(MX_diot_trans_type);
			}

			sb.append("|");

			if (MX_foreign_tax_reg_num == null) {
				sb.append("<null>");
			} else {
				sb.append(MX_foreign_tax_reg_num);
			}

			sb.append("|");

			if (MX_tax_reg_num == null) {
				sb.append("<null>");
			} else {
				sb.append(MX_tax_reg_num);
			}

			sb.append("|");

			if (MX_tax_reg_num_type == null) {
				sb.append("<null>");
			} else {
				sb.append(MX_tax_reg_num_type);
			}

			sb.append("|");

			if (MX_vendor_name == null) {
				sb.append("<null>");
			} else {
				sb.append(MX_vendor_name);
			}

			sb.append("|");

			return sb.toString();
		}

		/**
		 * Compare keys
		 */
		public int compareTo(distributionStruct other) {

			int returnValue = -1;

			returnValue = checkNullsAndCompare(this.site_ref, other.site_ref);
			if (returnValue != 0) {
				return returnValue;
			}

			returnValue = checkNullsAndCompare(this.vend_num, other.vend_num);
			if (returnValue != 0) {
				return returnValue;
			}

			returnValue = checkNullsAndCompare(this.voucher, other.voucher);
			if (returnValue != 0) {
				return returnValue;
			}

			returnValue = checkNullsAndCompare(this.dist_seq, other.dist_seq);
			if (returnValue != 0) {
				return returnValue;
			}

			return returnValue;
		}

		private int checkNullsAndCompare(Object object1, Object object2) {
			int returnValue = 0;
			if (object1 instanceof Comparable && object2 instanceof Comparable) {
				returnValue = ((Comparable) object1).compareTo(object2);
			} else if (object1 != null && object2 != null) {
				returnValue = compareStrings(object1.toString(),
						object2.toString());
			} else if (object1 == null && object2 != null) {
				returnValue = 1;
			} else if (object1 != null && object2 == null) {
				returnValue = -1;
			} else {
				returnValue = 0;
			}

			return returnValue;
		}

		private int compareStrings(String string1, String string2) {
			return string1.compareTo(string2);
		}

	}

	public static class row2Struct implements
			routines.system.IPersistableRow<row2Struct> {
		final static byte[] commonByteArrayLock_DEV_AP_Transactions = new byte[0];
		static byte[] commonByteArray_DEV_AP_Transactions = new byte[0];

		public String Employee_ID;

		public String getEmployee_ID() {
			return this.Employee_ID;
		}

		public java.util.Date Start_Date;

		public java.util.Date getStart_Date() {
			return this.Start_Date;
		}

		public java.util.Date End_Date;

		public java.util.Date getEnd_Date() {
			return this.End_Date;
		}

		public java.util.Date Process_Date;

		public java.util.Date getProcess_Date() {
			return this.Process_Date;
		}

		public String Approval_Code;

		public String getApproval_Code() {
			return this.Approval_Code;
		}

		public java.util.Date Expense_Date;

		public java.util.Date getExpense_Date() {
			return this.Expense_Date;
		}

		public String GL_Codes;

		public String getGL_Codes() {
			return this.GL_Codes;
		}

		public String Dept_Code;

		public String getDept_Code() {
			return this.Dept_Code;
		}

		public String Trad_Part;

		public String getTrad_Part() {
			return this.Trad_Part;
		}

		public String PSID;

		public String getPSID() {
			return this.PSID;
		}

		public Float Amount;

		public Float getAmount() {
			return this.Amount;
		}

		public Integer Voucher;

		public Integer getVoucher() {
			return this.Voucher;
		}

		public Integer Sequence;

		public Integer getSequence() {
			return this.Sequence;
		}

		private String readString(ObjectInputStream dis) throws IOException {
			String strReturn = null;
			int length = 0;
			length = dis.readInt();
			if (length == -1) {
				strReturn = null;
			} else {
				if (length > commonByteArray_DEV_AP_Transactions.length) {
					if (length < 1024
							&& commonByteArray_DEV_AP_Transactions.length == 0) {
						commonByteArray_DEV_AP_Transactions = new byte[1024];
					} else {
						commonByteArray_DEV_AP_Transactions = new byte[2 * length];
					}
				}
				dis.readFully(commonByteArray_DEV_AP_Transactions, 0, length);
				strReturn = new String(commonByteArray_DEV_AP_Transactions, 0,
						length, utf8Charset);
			}
			return strReturn;
		}

		private void writeString(String str, ObjectOutputStream dos)
				throws IOException {
			if (str == null) {
				dos.writeInt(-1);
			} else {
				byte[] byteArray = str.getBytes(utf8Charset);
				dos.writeInt(byteArray.length);
				dos.write(byteArray);
			}
		}

		private java.util.Date readDate(ObjectInputStream dis)
				throws IOException {
			java.util.Date dateReturn = null;
			int length = 0;
			length = dis.readByte();
			if (length == -1) {
				dateReturn = null;
			} else {
				dateReturn = new Date(dis.readLong());
			}
			return dateReturn;
		}

		private void writeDate(java.util.Date date1, ObjectOutputStream dos)
				throws IOException {
			if (date1 == null) {
				dos.writeByte(-1);
			} else {
				dos.writeByte(0);
				dos.writeLong(date1.getTime());
			}
		}

		private Integer readInteger(ObjectInputStream dis) throws IOException {
			Integer intReturn;
			int length = 0;
			length = dis.readByte();
			if (length == -1) {
				intReturn = null;
			} else {
				intReturn = dis.readInt();
			}
			return intReturn;
		}

		private void writeInteger(Integer intNum, ObjectOutputStream dos)
				throws IOException {
			if (intNum == null) {
				dos.writeByte(-1);
			} else {
				dos.writeByte(0);
				dos.writeInt(intNum);
			}
		}

		public void readData(ObjectInputStream dis) {

			synchronized (commonByteArrayLock_DEV_AP_Transactions) {

				try {

					int length = 0;

					this.Employee_ID = readString(dis);

					this.Start_Date = readDate(dis);

					this.End_Date = readDate(dis);

					this.Process_Date = readDate(dis);

					this.Approval_Code = readString(dis);

					this.Expense_Date = readDate(dis);

					this.GL_Codes = readString(dis);

					this.Dept_Code = readString(dis);

					this.Trad_Part = readString(dis);

					this.PSID = readString(dis);

					length = dis.readByte();
					if (length == -1) {
						this.Amount = null;
					} else {
						this.Amount = dis.readFloat();
					}

					this.Voucher = readInteger(dis);

					this.Sequence = readInteger(dis);

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void writeData(ObjectOutputStream dos) {
			try {

				// String

				writeString(this.Employee_ID, dos);

				// java.util.Date

				writeDate(this.Start_Date, dos);

				// java.util.Date

				writeDate(this.End_Date, dos);

				// java.util.Date

				writeDate(this.Process_Date, dos);

				// String

				writeString(this.Approval_Code, dos);

				// java.util.Date

				writeDate(this.Expense_Date, dos);

				// String

				writeString(this.GL_Codes, dos);

				// String

				writeString(this.Dept_Code, dos);

				// String

				writeString(this.Trad_Part, dos);

				// String

				writeString(this.PSID, dos);

				// Float

				if (this.Amount == null) {
					dos.writeByte(-1);
				} else {
					dos.writeByte(0);
					dos.writeFloat(this.Amount);
				}

				// Integer

				writeInteger(this.Voucher, dos);

				// Integer

				writeInteger(this.Sequence, dos);

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public String toString() {

			StringBuilder sb = new StringBuilder();
			sb.append(super.toString());
			sb.append("[");
			sb.append("Employee_ID=" + Employee_ID);
			sb.append(",Start_Date=" + String.valueOf(Start_Date));
			sb.append(",End_Date=" + String.valueOf(End_Date));
			sb.append(",Process_Date=" + String.valueOf(Process_Date));
			sb.append(",Approval_Code=" + Approval_Code);
			sb.append(",Expense_Date=" + String.valueOf(Expense_Date));
			sb.append(",GL_Codes=" + GL_Codes);
			sb.append(",Dept_Code=" + Dept_Code);
			sb.append(",Trad_Part=" + Trad_Part);
			sb.append(",PSID=" + PSID);
			sb.append(",Amount=" + String.valueOf(Amount));
			sb.append(",Voucher=" + String.valueOf(Voucher));
			sb.append(",Sequence=" + String.valueOf(Sequence));
			sb.append("]");

			return sb.toString();
		}

		public String toLogString() {
			StringBuilder sb = new StringBuilder();

			if (Employee_ID == null) {
				sb.append("<null>");
			} else {
				sb.append(Employee_ID);
			}

			sb.append("|");

			if (Start_Date == null) {
				sb.append("<null>");
			} else {
				sb.append(Start_Date);
			}

			sb.append("|");

			if (End_Date == null) {
				sb.append("<null>");
			} else {
				sb.append(End_Date);
			}

			sb.append("|");

			if (Process_Date == null) {
				sb.append("<null>");
			} else {
				sb.append(Process_Date);
			}

			sb.append("|");

			if (Approval_Code == null) {
				sb.append("<null>");
			} else {
				sb.append(Approval_Code);
			}

			sb.append("|");

			if (Expense_Date == null) {
				sb.append("<null>");
			} else {
				sb.append(Expense_Date);
			}

			sb.append("|");

			if (GL_Codes == null) {
				sb.append("<null>");
			} else {
				sb.append(GL_Codes);
			}

			sb.append("|");

			if (Dept_Code == null) {
				sb.append("<null>");
			} else {
				sb.append(Dept_Code);
			}

			sb.append("|");

			if (Trad_Part == null) {
				sb.append("<null>");
			} else {
				sb.append(Trad_Part);
			}

			sb.append("|");

			if (PSID == null) {
				sb.append("<null>");
			} else {
				sb.append(PSID);
			}

			sb.append("|");

			if (Amount == null) {
				sb.append("<null>");
			} else {
				sb.append(Amount);
			}

			sb.append("|");

			if (Voucher == null) {
				sb.append("<null>");
			} else {
				sb.append(Voucher);
			}

			sb.append("|");

			if (Sequence == null) {
				sb.append("<null>");
			} else {
				sb.append(Sequence);
			}

			sb.append("|");

			return sb.toString();
		}

		/**
		 * Compare keys
		 */
		public int compareTo(row2Struct other) {

			int returnValue = -1;

			return returnValue;
		}

		private int checkNullsAndCompare(Object object1, Object object2) {
			int returnValue = 0;
			if (object1 instanceof Comparable && object2 instanceof Comparable) {
				returnValue = ((Comparable) object1).compareTo(object2);
			} else if (object1 != null && object2 != null) {
				returnValue = compareStrings(object1.toString(),
						object2.toString());
			} else if (object1 == null && object2 != null) {
				returnValue = 1;
			} else if (object1 != null && object2 == null) {
				returnValue = -1;
			} else {
				returnValue = 0;
			}

			return returnValue;
		}

		private int compareStrings(String string1, String string2) {
			return string1.compareTo(string2);
		}

	}

	public void tDBInput_4Process(final java.util.Map<String, Object> globalMap)
			throws TalendException {
		globalMap.put("tDBInput_4_SUBPROCESS_STATE", 0);

		final boolean execStat = this.execStat;

		String iterateId = "";

		String currentComponent = "";
		java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

		try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception()
						.getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { // start the resume
				globalResumeTicket = true;

				row2Struct row2 = new row2Struct();
				distributionStruct distribution = new distributionStruct();

				/**
				 * [tDBOutput_2 begin ] start
				 */

				ok_Hash.put("tDBOutput_2", false);
				start_Hash.put("tDBOutput_2", System.currentTimeMillis());

				currentComponent = "tDBOutput_2";

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null) {

						runStat.updateStatOnConnection("distribution"
								+ iterateId, 0, 0);

					}
				}

				int tos_count_tDBOutput_2 = 0;

				if (log.isDebugEnabled())
					log.debug("tDBOutput_2 - " + ("Start to work."));
				if (log.isDebugEnabled()) {
					class BytesLimit65535_tDBOutput_2 {
						public void limitLog4jByte() throws Exception {
							StringBuilder log4jParamters_tDBOutput_2 = new StringBuilder();
							log4jParamters_tDBOutput_2.append("Parameters:");
							log4jParamters_tDBOutput_2
									.append("USE_EXISTING_CONNECTION" + " = "
											+ "false");
							log4jParamters_tDBOutput_2.append(" | ");
							log4jParamters_tDBOutput_2.append("DRIVER" + " = "
									+ "JTDS");
							log4jParamters_tDBOutput_2.append(" | ");
							log4jParamters_tDBOutput_2.append("HOST" + " = "
									+ "\"NSMWQNXINFDB01\"");
							log4jParamters_tDBOutput_2.append(" | ");
							log4jParamters_tDBOutput_2.append("PORT" + " = "
									+ "\"1433\"");
							log4jParamters_tDBOutput_2.append(" | ");
							log4jParamters_tDBOutput_2.append("DB_SCHEMA"
									+ " = " + "\"dbo\"");
							log4jParamters_tDBOutput_2.append(" | ");
							log4jParamters_tDBOutput_2.append("DBNAME" + " = "
									+ "\"QBP_HO_APP\"");
							log4jParamters_tDBOutput_2.append(" | ");
							log4jParamters_tDBOutput_2.append("USER" + " = "
									+ "\"sa\"");
							log4jParamters_tDBOutput_2.append(" | ");
							log4jParamters_tDBOutput_2.append("PASS"
									+ " = "
									+ String.valueOf(
											"bf98e2c82ba433c4880df380fbb39fe9")
											.substring(0, 4) + "...");
							log4jParamters_tDBOutput_2.append(" | ");
							log4jParamters_tDBOutput_2.append("TABLE" + " = "
									+ "\"aptrxd_mst\"");
							log4jParamters_tDBOutput_2.append(" | ");
							log4jParamters_tDBOutput_2.append("TABLE_ACTION"
									+ " = " + "NONE");
							log4jParamters_tDBOutput_2.append(" | ");
							log4jParamters_tDBOutput_2.append("IDENTITY_INSERT"
									+ " = " + "false");
							log4jParamters_tDBOutput_2.append(" | ");
							log4jParamters_tDBOutput_2.append("DATA_ACTION"
									+ " = " + "INSERT");
							log4jParamters_tDBOutput_2.append(" | ");
							log4jParamters_tDBOutput_2
									.append("SPECIFY_DATASOURCE_ALIAS" + " = "
											+ "false");
							log4jParamters_tDBOutput_2.append(" | ");
							log4jParamters_tDBOutput_2.append("DIE_ON_ERROR"
									+ " = " + "false");
							log4jParamters_tDBOutput_2.append(" | ");
							log4jParamters_tDBOutput_2.append("PROPERTIES"
									+ " = " + "\"\"");
							log4jParamters_tDBOutput_2.append(" | ");
							log4jParamters_tDBOutput_2.append("COMMIT_EVERY"
									+ " = " + "10000");
							log4jParamters_tDBOutput_2.append(" | ");
							log4jParamters_tDBOutput_2.append("ADD_COLS"
									+ " = " + "[]");
							log4jParamters_tDBOutput_2.append(" | ");
							log4jParamters_tDBOutput_2
									.append("USE_FIELD_OPTIONS" + " = "
											+ "false");
							log4jParamters_tDBOutput_2.append(" | ");
							log4jParamters_tDBOutput_2
									.append("IGNORE_DATE_OUTOF_RANGE" + " = "
											+ "false");
							log4jParamters_tDBOutput_2.append(" | ");
							log4jParamters_tDBOutput_2
									.append("ENABLE_DEBUG_MODE" + " = "
											+ "false");
							log4jParamters_tDBOutput_2.append(" | ");
							log4jParamters_tDBOutput_2
									.append("SUPPORT_NULL_WHERE" + " = "
											+ "false");
							log4jParamters_tDBOutput_2.append(" | ");
							log4jParamters_tDBOutput_2.append("USE_BATCH_SIZE"
									+ " = " + "true");
							log4jParamters_tDBOutput_2.append(" | ");
							log4jParamters_tDBOutput_2.append("BATCH_SIZE"
									+ " = " + "10000");
							log4jParamters_tDBOutput_2.append(" | ");
							log4jParamters_tDBOutput_2
									.append("UNIFIED_COMPONENTS" + " = "
											+ "tMSSqlOutput");
							log4jParamters_tDBOutput_2.append(" | ");
							if (log.isDebugEnabled())
								log.debug("tDBOutput_2 - "
										+ (log4jParamters_tDBOutput_2));
						}
					}
					new BytesLimit65535_tDBOutput_2().limitLog4jByte();
				}

				int nb_line_tDBOutput_2 = 0;
				int nb_line_update_tDBOutput_2 = 0;
				int nb_line_inserted_tDBOutput_2 = 0;
				int nb_line_deleted_tDBOutput_2 = 0;
				int nb_line_rejected_tDBOutput_2 = 0;

				int deletedCount_tDBOutput_2 = 0;
				int updatedCount_tDBOutput_2 = 0;
				int insertedCount_tDBOutput_2 = 0;
				int rejectedCount_tDBOutput_2 = 0;
				String dbschema_tDBOutput_2 = null;
				String tableName_tDBOutput_2 = null;
				boolean whetherReject_tDBOutput_2 = false;

				java.util.Calendar calendar_tDBOutput_2 = java.util.Calendar
						.getInstance();
				long year1_tDBOutput_2 = TalendDate.parseDate("yyyy-MM-dd",
						"0001-01-01").getTime();
				long year2_tDBOutput_2 = TalendDate.parseDate("yyyy-MM-dd",
						"1753-01-01").getTime();
				long year10000_tDBOutput_2 = TalendDate.parseDate(
						"yyyy-MM-dd HH:mm:ss", "9999-12-31 24:00:00").getTime();
				long date_tDBOutput_2;

				java.util.Calendar calendar_datetimeoffset_tDBOutput_2 = java.util.Calendar
						.getInstance(java.util.TimeZone.getTimeZone("UTC"));

				java.sql.Connection conn_tDBOutput_2 = null;
				String dbUser_tDBOutput_2 = null;
				dbschema_tDBOutput_2 = "dbo";
				String driverClass_tDBOutput_2 = "net.sourceforge.jtds.jdbc.Driver";

				if (log.isDebugEnabled())
					log.debug("tDBOutput_2 - " + ("Driver ClassName: ")
							+ (driverClass_tDBOutput_2) + ("."));
				java.lang.Class.forName(driverClass_tDBOutput_2);
				String port_tDBOutput_2 = "1433";
				String dbname_tDBOutput_2 = "QBP_HO_APP";
				String url_tDBOutput_2 = "jdbc:jtds:sqlserver://"
						+ "NSMWQNXINFDB01";
				if (!"".equals(port_tDBOutput_2)) {
					url_tDBOutput_2 += ":" + "1433";
				}
				if (!"".equals(dbname_tDBOutput_2)) {
					url_tDBOutput_2 += "//" + "QBP_HO_APP";

				}
				url_tDBOutput_2 += ";appName=" + projectName + ";" + "";
				dbUser_tDBOutput_2 = "sa";

				final String decryptedPassword_tDBOutput_2 = routines.system.PasswordEncryptUtil
						.decryptPassword("bf98e2c82ba433c4880df380fbb39fe9");

				String dbPwd_tDBOutput_2 = decryptedPassword_tDBOutput_2;
				if (log.isDebugEnabled())
					log.debug("tDBOutput_2 - " + ("Connection attempts to '")
							+ (url_tDBOutput_2) + ("' with the username '")
							+ (dbUser_tDBOutput_2) + ("'."));
				conn_tDBOutput_2 = java.sql.DriverManager.getConnection(
						url_tDBOutput_2, dbUser_tDBOutput_2, dbPwd_tDBOutput_2);
				if (log.isDebugEnabled())
					log.debug("tDBOutput_2 - " + ("Connection to '")
							+ (url_tDBOutput_2) + ("' has succeeded."));

				resourceMap.put("conn_tDBOutput_2", conn_tDBOutput_2);

				conn_tDBOutput_2.setAutoCommit(false);
				int commitEvery_tDBOutput_2 = 10000;
				int commitCounter_tDBOutput_2 = 0;

				if (log.isDebugEnabled())
					log.debug("tDBOutput_2 - "
							+ ("Connection is set auto commit to '")
							+ (conn_tDBOutput_2.getAutoCommit()) + ("'."));
				int batchSize_tDBOutput_2 = 10000;
				int batchSizeCounter_tDBOutput_2 = 0;

				if (dbschema_tDBOutput_2 == null
						|| dbschema_tDBOutput_2.trim().length() == 0) {
					tableName_tDBOutput_2 = "aptrxd_mst";
				} else {
					tableName_tDBOutput_2 = dbschema_tDBOutput_2 + "].["
							+ "aptrxd_mst";
				}
				int count_tDBOutput_2 = 0;

				String insert_tDBOutput_2 = "INSERT INTO ["
						+ tableName_tDBOutput_2
						+ "] ([site_ref],[vend_num],[voucher],[dist_seq],[acct],[amount],[inv_num],[tax_code],[tax_basis],[tax_system],[tax_code_e],[acct_unit1],[acct_unit2],[acct_unit3],[acct_unit4],[proj_num],[task_num],[cost_code],[MX_iso_country_code],[MX_ietu_deduction_pct],[MX_diot_trans_type],[MX_foreign_tax_reg_num],[MX_tax_reg_num],[MX_tax_reg_num_type],[MX_vendor_name]) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
				java.sql.PreparedStatement pstmt_tDBOutput_2 = conn_tDBOutput_2
						.prepareStatement(insert_tDBOutput_2);
				resourceMap.put("pstmt_tDBOutput_2", pstmt_tDBOutput_2);

				/**
				 * [tDBOutput_2 begin ] stop
				 */

				/**
				 * [tMap_1 begin ] start
				 */

				ok_Hash.put("tMap_1", false);
				start_Hash.put("tMap_1", System.currentTimeMillis());

				currentComponent = "tMap_1";

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null) {

						runStat.updateStatOnConnection("row2" + iterateId, 0, 0);

					}
				}

				int tos_count_tMap_1 = 0;

				if (log.isDebugEnabled())
					log.debug("tMap_1 - " + ("Start to work."));
				if (log.isDebugEnabled()) {
					class BytesLimit65535_tMap_1 {
						public void limitLog4jByte() throws Exception {
							StringBuilder log4jParamters_tMap_1 = new StringBuilder();
							log4jParamters_tMap_1.append("Parameters:");
							log4jParamters_tMap_1.append("LINK_STYLE" + " = "
									+ "AUTO");
							log4jParamters_tMap_1.append(" | ");
							log4jParamters_tMap_1
									.append("TEMPORARY_DATA_DIRECTORY" + " = "
											+ "");
							log4jParamters_tMap_1.append(" | ");
							log4jParamters_tMap_1.append("ROWS_BUFFER_SIZE"
									+ " = " + "2000000");
							log4jParamters_tMap_1.append(" | ");
							log4jParamters_tMap_1
									.append("CHANGE_HASH_AND_EQUALS_FOR_BIGDECIMAL"
											+ " = " + "true");
							log4jParamters_tMap_1.append(" | ");
							if (log.isDebugEnabled())
								log.debug("tMap_1 - " + (log4jParamters_tMap_1));
						}
					}
					new BytesLimit65535_tMap_1().limitLog4jByte();
				}

				// ###############################
				// # Lookup's keys initialization
				int count_row2_tMap_1 = 0;

				// ###############################

				// ###############################
				// # Vars initialization
				class Var__tMap_1__Struct {
				}
				Var__tMap_1__Struct Var__tMap_1 = new Var__tMap_1__Struct();
				// ###############################

				// ###############################
				// # Outputs initialization
				int count_distribution_tMap_1 = 0;

				distributionStruct distribution_tmp = new distributionStruct();
				// ###############################

				/**
				 * [tMap_1 begin ] stop
				 */

				/**
				 * [tDBInput_4 begin ] start
				 */

				ok_Hash.put("tDBInput_4", false);
				start_Hash.put("tDBInput_4", System.currentTimeMillis());

				currentComponent = "tDBInput_4";

				int tos_count_tDBInput_4 = 0;

				if (log.isDebugEnabled())
					log.debug("tDBInput_4 - " + ("Start to work."));
				if (log.isDebugEnabled()) {
					class BytesLimit65535_tDBInput_4 {
						public void limitLog4jByte() throws Exception {
							StringBuilder log4jParamters_tDBInput_4 = new StringBuilder();
							log4jParamters_tDBInput_4.append("Parameters:");
							log4jParamters_tDBInput_4
									.append("USE_EXISTING_CONNECTION" + " = "
											+ "false");
							log4jParamters_tDBInput_4.append(" | ");
							log4jParamters_tDBInput_4.append("HOST" + " = "
									+ "\"NSMWQNXINFDB02\"");
							log4jParamters_tDBInput_4.append(" | ");
							log4jParamters_tDBInput_4.append("DRIVER" + " = "
									+ "JTDS");
							log4jParamters_tDBInput_4.append(" | ");
							log4jParamters_tDBInput_4.append("PORT" + " = "
									+ "\"1433\"");
							log4jParamters_tDBInput_4.append(" | ");
							log4jParamters_tDBInput_4.append("DB_SCHEMA"
									+ " = " + "\"\"");
							log4jParamters_tDBInput_4.append(" | ");
							log4jParamters_tDBInput_4.append("DBNAME" + " = "
									+ "\"Talend_Certify\"");
							log4jParamters_tDBInput_4.append(" | ");
							log4jParamters_tDBInput_4.append("USER" + " = "
									+ "\"sa\"");
							log4jParamters_tDBInput_4.append(" | ");
							log4jParamters_tDBInput_4.append("PASS"
									+ " = "
									+ String.valueOf(
											"bf98e2c82ba433c4880df380fbb39fe9")
											.substring(0, 4) + "...");
							log4jParamters_tDBInput_4.append(" | ");
							log4jParamters_tDBInput_4.append("TABLE" + " = "
									+ "\"QBP_HO_APP_DistributionList\"");
							log4jParamters_tDBInput_4.append(" | ");
							log4jParamters_tDBInput_4.append("QUERYSTORE"
									+ " = " + "\"\"");
							log4jParamters_tDBInput_4.append(" | ");
							log4jParamters_tDBInput_4
									.append("QUERY"
											+ " = "
											+ "\"SELECT  QBP_HO_APP_DistributionList.Employee_ID  		,QBP_HO_APP_DistributionList.Start_Date  		,QBP_HO_APP_DistributionList.End_Date  		,QBP_HO_APP_DistributionList.Process_Date  		,QBP_HO_APP_DistributionList.Approval_Code  		,QBP_HO_APP_DistributionList.Expense_Date  		,QBP_HO_APP_DistributionList.GL_Codes  		,QBP_HO_APP_DistributionList.Dept_Code  		,QBP_HO_APP_DistributionList.Trad_Part  		,QBP_HO_APP_DistributionList.PSID  		,QBP_HO_APP_DistributionList.Amount  		,QBP_HO_APP_DistributionList.Voucher  	    ,QBP_HO_APP_DistributionList.Sequence  FROM	QBP_HO_APP_DistributionList\"");
							log4jParamters_tDBInput_4.append(" | ");
							log4jParamters_tDBInput_4
									.append("SPECIFY_DATASOURCE_ALIAS" + " = "
											+ "false");
							log4jParamters_tDBInput_4.append(" | ");
							log4jParamters_tDBInput_4.append("PROPERTIES"
									+ " = " + "\"\"");
							log4jParamters_tDBInput_4.append(" | ");
							log4jParamters_tDBInput_4.append("TRIM_ALL_COLUMN"
									+ " = " + "false");
							log4jParamters_tDBInput_4.append(" | ");
							log4jParamters_tDBInput_4.append("TRIM_COLUMN"
									+ " = " + "[{TRIM=" + ("false")
									+ ", SCHEMA_COLUMN=" + ("Employee_ID")
									+ "}, {TRIM=" + ("false")
									+ ", SCHEMA_COLUMN=" + ("Start_Date")
									+ "}, {TRIM=" + ("false")
									+ ", SCHEMA_COLUMN=" + ("End_Date")
									+ "}, {TRIM=" + ("false")
									+ ", SCHEMA_COLUMN=" + ("Process_Date")
									+ "}, {TRIM=" + ("false")
									+ ", SCHEMA_COLUMN=" + ("Approval_Code")
									+ "}, {TRIM=" + ("false")
									+ ", SCHEMA_COLUMN=" + ("Expense_Date")
									+ "}, {TRIM=" + ("false")
									+ ", SCHEMA_COLUMN=" + ("GL_Codes")
									+ "}, {TRIM=" + ("false")
									+ ", SCHEMA_COLUMN=" + ("Dept_Code")
									+ "}, {TRIM=" + ("false")
									+ ", SCHEMA_COLUMN=" + ("Trad_Part")
									+ "}, {TRIM=" + ("false")
									+ ", SCHEMA_COLUMN=" + ("PSID")
									+ "}, {TRIM=" + ("false")
									+ ", SCHEMA_COLUMN=" + ("Amount")
									+ "}, {TRIM=" + ("false")
									+ ", SCHEMA_COLUMN=" + ("Voucher")
									+ "}, {TRIM=" + ("false")
									+ ", SCHEMA_COLUMN=" + ("Sequence") + "}]");
							log4jParamters_tDBInput_4.append(" | ");
							log4jParamters_tDBInput_4
									.append("UNIFIED_COMPONENTS" + " = "
											+ "tMSSqlInput");
							log4jParamters_tDBInput_4.append(" | ");
							if (log.isDebugEnabled())
								log.debug("tDBInput_4 - "
										+ (log4jParamters_tDBInput_4));
						}
					}
					new BytesLimit65535_tDBInput_4().limitLog4jByte();
				}

				org.talend.designer.components.util.mssql.MSSqlGenerateTimestampUtil mssqlGTU_tDBInput_4 = org.talend.designer.components.util.mssql.MSSqlUtilFactory
						.getMSSqlGenerateTimestampUtil();

				java.util.List<String> talendToDBList_tDBInput_4 = new java.util.ArrayList();
				String[] talendToDBArray_tDBInput_4 = new String[] { "FLOAT",
						"NUMERIC", "NUMERIC IDENTITY", "DECIMAL",
						"DECIMAL IDENTITY", "REAL" };
				java.util.Collections.addAll(talendToDBList_tDBInput_4,
						talendToDBArray_tDBInput_4);
				int nb_line_tDBInput_4 = 0;
				java.sql.Connection conn_tDBInput_4 = null;
				String driverClass_tDBInput_4 = "net.sourceforge.jtds.jdbc.Driver";
				java.lang.Class.forName(driverClass_tDBInput_4);
				String dbUser_tDBInput_4 = "sa";

				final String decryptedPassword_tDBInput_4 = routines.system.PasswordEncryptUtil
						.decryptPassword("bf98e2c82ba433c4880df380fbb39fe9");

				String dbPwd_tDBInput_4 = decryptedPassword_tDBInput_4;

				String port_tDBInput_4 = "1433";
				String dbname_tDBInput_4 = "Talend_Certify";
				String url_tDBInput_4 = "jdbc:jtds:sqlserver://"
						+ "NSMWQNXINFDB02";
				if (!"".equals(port_tDBInput_4)) {
					url_tDBInput_4 += ":" + "1433";
				}
				if (!"".equals(dbname_tDBInput_4)) {
					url_tDBInput_4 += "//" + "Talend_Certify";
				}
				url_tDBInput_4 += ";appName=" + projectName + ";" + "";
				String dbschema_tDBInput_4 = "";

				log.debug("tDBInput_4 - Driver ClassName: "
						+ driverClass_tDBInput_4 + ".");

				log.debug("tDBInput_4 - Connection attempt to '"
						+ url_tDBInput_4 + "' with the username '"
						+ dbUser_tDBInput_4 + "'.");

				conn_tDBInput_4 = java.sql.DriverManager.getConnection(
						url_tDBInput_4, dbUser_tDBInput_4, dbPwd_tDBInput_4);
				log.debug("tDBInput_4 - Connection to '" + url_tDBInput_4
						+ "' has succeeded.");

				java.sql.Statement stmt_tDBInput_4 = conn_tDBInput_4
						.createStatement();

				String dbquery_tDBInput_4 = "SELECT  QBP_HO_APP_DistributionList.Employee_ID\n		,QBP_HO_APP_DistributionList.Start_Date\n		,QBP_HO_APP_DistributionL"
						+ "ist.End_Date\n		,QBP_HO_APP_DistributionList.Process_Date\n		,QBP_HO_APP_DistributionList.Approval_Code\n		,QBP_HO_APP_D"
						+ "istributionList.Expense_Date\n		,QBP_HO_APP_DistributionList.GL_Codes\n		,QBP_HO_APP_DistributionList.Dept_Code\n		,QBP_"
						+ "HO_APP_DistributionList.Trad_Part\n		,QBP_HO_APP_DistributionList.PSID\n		,QBP_HO_APP_DistributionList.Amount\n		,QBP_HO"
						+ "_APP_DistributionList.Voucher\n	    ,QBP_HO_APP_DistributionList.Sequence\nFROM	QBP_HO_APP_DistributionList";

				log.debug("tDBInput_4 - Executing the query: '"
						+ dbquery_tDBInput_4 + "'.");

				globalMap.put("tDBInput_4_QUERY", dbquery_tDBInput_4);
				java.sql.ResultSet rs_tDBInput_4 = null;

				try {
					rs_tDBInput_4 = stmt_tDBInput_4
							.executeQuery(dbquery_tDBInput_4);
					java.sql.ResultSetMetaData rsmd_tDBInput_4 = rs_tDBInput_4
							.getMetaData();
					int colQtyInRs_tDBInput_4 = rsmd_tDBInput_4
							.getColumnCount();

					String tmpContent_tDBInput_4 = null;

					log.debug("tDBInput_4 - Retrieving records from the database.");

					while (rs_tDBInput_4.next()) {
						nb_line_tDBInput_4++;

						if (colQtyInRs_tDBInput_4 < 1) {
							row2.Employee_ID = null;
						} else {

							tmpContent_tDBInput_4 = rs_tDBInput_4.getString(1);
							if (tmpContent_tDBInput_4 != null) {
								if (talendToDBList_tDBInput_4
										.contains(rsmd_tDBInput_4
												.getColumnTypeName(1)
												.toUpperCase(
														java.util.Locale.ENGLISH))) {
									row2.Employee_ID = FormatterUtils
											.formatUnwithE(tmpContent_tDBInput_4);
								} else {
									row2.Employee_ID = tmpContent_tDBInput_4;
								}
							} else {
								row2.Employee_ID = null;
							}
						}
						if (colQtyInRs_tDBInput_4 < 2) {
							row2.Start_Date = null;
						} else {

							row2.Start_Date = mssqlGTU_tDBInput_4.getDate(
									rsmd_tDBInput_4, rs_tDBInput_4, 2);

						}
						if (colQtyInRs_tDBInput_4 < 3) {
							row2.End_Date = null;
						} else {

							row2.End_Date = mssqlGTU_tDBInput_4.getDate(
									rsmd_tDBInput_4, rs_tDBInput_4, 3);

						}
						if (colQtyInRs_tDBInput_4 < 4) {
							row2.Process_Date = null;
						} else {

							row2.Process_Date = mssqlGTU_tDBInput_4.getDate(
									rsmd_tDBInput_4, rs_tDBInput_4, 4);

						}
						if (colQtyInRs_tDBInput_4 < 5) {
							row2.Approval_Code = null;
						} else {

							tmpContent_tDBInput_4 = rs_tDBInput_4.getString(5);
							if (tmpContent_tDBInput_4 != null) {
								if (talendToDBList_tDBInput_4
										.contains(rsmd_tDBInput_4
												.getColumnTypeName(5)
												.toUpperCase(
														java.util.Locale.ENGLISH))) {
									row2.Approval_Code = FormatterUtils
											.formatUnwithE(tmpContent_tDBInput_4);
								} else {
									row2.Approval_Code = tmpContent_tDBInput_4;
								}
							} else {
								row2.Approval_Code = null;
							}
						}
						if (colQtyInRs_tDBInput_4 < 6) {
							row2.Expense_Date = null;
						} else {

							row2.Expense_Date = mssqlGTU_tDBInput_4.getDate(
									rsmd_tDBInput_4, rs_tDBInput_4, 6);

						}
						if (colQtyInRs_tDBInput_4 < 7) {
							row2.GL_Codes = null;
						} else {

							tmpContent_tDBInput_4 = rs_tDBInput_4.getString(7);
							if (tmpContent_tDBInput_4 != null) {
								if (talendToDBList_tDBInput_4
										.contains(rsmd_tDBInput_4
												.getColumnTypeName(7)
												.toUpperCase(
														java.util.Locale.ENGLISH))) {
									row2.GL_Codes = FormatterUtils
											.formatUnwithE(tmpContent_tDBInput_4);
								} else {
									row2.GL_Codes = tmpContent_tDBInput_4;
								}
							} else {
								row2.GL_Codes = null;
							}
						}
						if (colQtyInRs_tDBInput_4 < 8) {
							row2.Dept_Code = null;
						} else {

							tmpContent_tDBInput_4 = rs_tDBInput_4.getString(8);
							if (tmpContent_tDBInput_4 != null) {
								if (talendToDBList_tDBInput_4
										.contains(rsmd_tDBInput_4
												.getColumnTypeName(8)
												.toUpperCase(
														java.util.Locale.ENGLISH))) {
									row2.Dept_Code = FormatterUtils
											.formatUnwithE(tmpContent_tDBInput_4);
								} else {
									row2.Dept_Code = tmpContent_tDBInput_4;
								}
							} else {
								row2.Dept_Code = null;
							}
						}
						if (colQtyInRs_tDBInput_4 < 9) {
							row2.Trad_Part = null;
						} else {

							tmpContent_tDBInput_4 = rs_tDBInput_4.getString(9);
							if (tmpContent_tDBInput_4 != null) {
								if (talendToDBList_tDBInput_4
										.contains(rsmd_tDBInput_4
												.getColumnTypeName(9)
												.toUpperCase(
														java.util.Locale.ENGLISH))) {
									row2.Trad_Part = FormatterUtils
											.formatUnwithE(tmpContent_tDBInput_4);
								} else {
									row2.Trad_Part = tmpContent_tDBInput_4;
								}
							} else {
								row2.Trad_Part = null;
							}
						}
						if (colQtyInRs_tDBInput_4 < 10) {
							row2.PSID = null;
						} else {

							tmpContent_tDBInput_4 = rs_tDBInput_4.getString(10);
							if (tmpContent_tDBInput_4 != null) {
								if (talendToDBList_tDBInput_4
										.contains(rsmd_tDBInput_4
												.getColumnTypeName(10)
												.toUpperCase(
														java.util.Locale.ENGLISH))) {
									row2.PSID = FormatterUtils
											.formatUnwithE(tmpContent_tDBInput_4);
								} else {
									row2.PSID = tmpContent_tDBInput_4;
								}
							} else {
								row2.PSID = null;
							}
						}
						if (colQtyInRs_tDBInput_4 < 11) {
							row2.Amount = null;
						} else {

							if (rs_tDBInput_4.getObject(11) != null) {
								row2.Amount = rs_tDBInput_4.getFloat(11);
							} else {
								row2.Amount = null;
							}
						}
						if (colQtyInRs_tDBInput_4 < 12) {
							row2.Voucher = null;
						} else {

							if (rs_tDBInput_4.getObject(12) != null) {
								row2.Voucher = rs_tDBInput_4.getInt(12);
							} else {
								row2.Voucher = null;
							}
						}
						if (colQtyInRs_tDBInput_4 < 13) {
							row2.Sequence = null;
						} else {

							if (rs_tDBInput_4.getObject(13) != null) {
								row2.Sequence = rs_tDBInput_4.getInt(13);
							} else {
								row2.Sequence = null;
							}
						}

						log.debug("tDBInput_4 - Retrieving the record "
								+ nb_line_tDBInput_4 + ".");

						/**
						 * [tDBInput_4 begin ] stop
						 */

						/**
						 * [tDBInput_4 main ] start
						 */

						currentComponent = "tDBInput_4";

						tos_count_tDBInput_4++;

						/**
						 * [tDBInput_4 main ] stop
						 */

						/**
						 * [tDBInput_4 process_data_begin ] start
						 */

						currentComponent = "tDBInput_4";

						/**
						 * [tDBInput_4 process_data_begin ] stop
						 */

						/**
						 * [tMap_1 main ] start
						 */

						currentComponent = "tMap_1";

						// row2
						// row2

						if (execStat) {
							runStat.updateStatOnConnection("row2" + iterateId,
									1, 1);
						}

						if (log.isTraceEnabled()) {
							log.trace("row2 - "
									+ (row2 == null ? "" : row2.toLogString()));
						}

						boolean hasCasePrimitiveKeyWithNull_tMap_1 = false;

						// ###############################
						// # Input tables (lookups)
						boolean rejectedInnerJoin_tMap_1 = false;
						boolean mainRowRejected_tMap_1 = false;

						// ###############################
						{ // start of Var scope

							// ###############################
							// # Vars tables

							Var__tMap_1__Struct Var = Var__tMap_1;// ###############################
							// ###############################
							// # Output tables

							distribution = null;

							// # Output table : 'distribution'
							count_distribution_tMap_1++;

							distribution_tmp.site_ref = "QBPCORP";
							distribution_tmp.vend_num = row2.Employee_ID;
							distribution_tmp.voucher = row2.Voucher;
							distribution_tmp.dist_seq = row2.Sequence;
							distribution_tmp.acct = row2.GL_Codes;
							distribution_tmp.amount = row2.Amount;
							distribution_tmp.inv_num = null;
							distribution_tmp.tax_code = null;
							distribution_tmp.tax_basis = null;
							distribution_tmp.tax_system = null;
							distribution_tmp.tax_code_e = null;
							distribution_tmp.acct_unit1 = null;
							distribution_tmp.acct_unit2 = row2.Dept_Code;
							distribution_tmp.acct_unit3 = row2.Trad_Part;
							distribution_tmp.acct_unit4 = null;
							distribution_tmp.proj_num = null;
							distribution_tmp.task_num = null;
							distribution_tmp.cost_code = null;
							distribution_tmp.MX_iso_country_code = null;
							distribution_tmp.MX_ietu_deduction_pct = null;
							distribution_tmp.MX_diot_trans_type = null;
							distribution_tmp.MX_foreign_tax_reg_num = null;
							distribution_tmp.MX_tax_reg_num = null;
							distribution_tmp.MX_tax_reg_num_type = null;
							distribution_tmp.MX_vendor_name = null;
							distribution = distribution_tmp;
							log.debug("tMap_1 - Outputting the record "
									+ count_distribution_tMap_1
									+ " of the output table 'distribution'.");

							// ###############################

						} // end of Var scope

						rejectedInnerJoin_tMap_1 = false;

						tos_count_tMap_1++;

						/**
						 * [tMap_1 main ] stop
						 */

						/**
						 * [tMap_1 process_data_begin ] start
						 */

						currentComponent = "tMap_1";

						/**
						 * [tMap_1 process_data_begin ] stop
						 */
						// Start of branch "distribution"
						if (distribution != null) {

							/**
							 * [tDBOutput_2 main ] start
							 */

							currentComponent = "tDBOutput_2";

							// distribution
							// distribution

							if (execStat) {
								runStat.updateStatOnConnection("distribution"
										+ iterateId, 1, 1);
							}

							if (log.isTraceEnabled()) {
								log.trace("distribution - "
										+ (distribution == null ? ""
												: distribution.toLogString()));
							}

							whetherReject_tDBOutput_2 = false;
							if (distribution.site_ref == null) {
								pstmt_tDBOutput_2.setNull(1,
										java.sql.Types.VARCHAR);
							} else {
								pstmt_tDBOutput_2.setString(1,
										distribution.site_ref);
							}

							if (distribution.vend_num == null) {
								pstmt_tDBOutput_2.setNull(2,
										java.sql.Types.VARCHAR);
							} else {
								pstmt_tDBOutput_2.setString(2,
										distribution.vend_num);
							}

							pstmt_tDBOutput_2.setInt(3, distribution.voucher);

							pstmt_tDBOutput_2.setInt(4, distribution.dist_seq);

							if (distribution.acct == null) {
								pstmt_tDBOutput_2.setNull(5,
										java.sql.Types.VARCHAR);
							} else {
								pstmt_tDBOutput_2.setString(5,
										distribution.acct);
							}

							if (distribution.amount == null) {
								pstmt_tDBOutput_2.setNull(6,
										java.sql.Types.FLOAT);
							} else {
								pstmt_tDBOutput_2.setFloat(6,
										distribution.amount);
							}

							if (distribution.inv_num == null) {
								pstmt_tDBOutput_2.setNull(7,
										java.sql.Types.VARCHAR);
							} else {
								pstmt_tDBOutput_2.setString(7,
										distribution.inv_num);
							}

							if (distribution.tax_code == null) {
								pstmt_tDBOutput_2.setNull(8,
										java.sql.Types.VARCHAR);
							} else {
								pstmt_tDBOutput_2.setString(8,
										distribution.tax_code);
							}

							if (distribution.tax_basis == null) {
								pstmt_tDBOutput_2.setNull(9,
										java.sql.Types.VARCHAR);
							} else {
								pstmt_tDBOutput_2.setString(9,
										distribution.tax_basis);
							}

							if (distribution.tax_system == null) {
								pstmt_tDBOutput_2.setNull(10,
										java.sql.Types.VARCHAR);
							} else {
								pstmt_tDBOutput_2.setString(10,
										distribution.tax_system);
							}

							if (distribution.tax_code_e == null) {
								pstmt_tDBOutput_2.setNull(11,
										java.sql.Types.VARCHAR);
							} else {
								pstmt_tDBOutput_2.setString(11,
										distribution.tax_code_e);
							}

							if (distribution.acct_unit1 == null) {
								pstmt_tDBOutput_2.setNull(12,
										java.sql.Types.VARCHAR);
							} else {
								pstmt_tDBOutput_2.setString(12,
										distribution.acct_unit1);
							}

							if (distribution.acct_unit2 == null) {
								pstmt_tDBOutput_2.setNull(13,
										java.sql.Types.VARCHAR);
							} else {
								pstmt_tDBOutput_2.setString(13,
										distribution.acct_unit2);
							}

							if (distribution.acct_unit3 == null) {
								pstmt_tDBOutput_2.setNull(14,
										java.sql.Types.VARCHAR);
							} else {
								pstmt_tDBOutput_2.setString(14,
										distribution.acct_unit3);
							}

							if (distribution.acct_unit4 == null) {
								pstmt_tDBOutput_2.setNull(15,
										java.sql.Types.VARCHAR);
							} else {
								pstmt_tDBOutput_2.setString(15,
										distribution.acct_unit4);
							}

							if (distribution.proj_num == null) {
								pstmt_tDBOutput_2.setNull(16,
										java.sql.Types.VARCHAR);
							} else {
								pstmt_tDBOutput_2.setString(16,
										distribution.proj_num);
							}

							if (distribution.task_num == null) {
								pstmt_tDBOutput_2.setNull(17,
										java.sql.Types.VARCHAR);
							} else {
								pstmt_tDBOutput_2.setString(17,
										distribution.task_num);
							}

							if (distribution.cost_code == null) {
								pstmt_tDBOutput_2.setNull(18,
										java.sql.Types.VARCHAR);
							} else {
								pstmt_tDBOutput_2.setString(18,
										distribution.cost_code);
							}

							if (distribution.MX_iso_country_code == null) {
								pstmt_tDBOutput_2.setNull(19,
										java.sql.Types.VARCHAR);
							} else {
								pstmt_tDBOutput_2.setString(19,
										distribution.MX_iso_country_code);
							}

							if (distribution.MX_ietu_deduction_pct == null) {
								pstmt_tDBOutput_2.setNull(20,
										java.sql.Types.VARCHAR);
							} else {
								pstmt_tDBOutput_2.setString(20,
										distribution.MX_ietu_deduction_pct);
							}

							if (distribution.MX_diot_trans_type == null) {
								pstmt_tDBOutput_2.setNull(21,
										java.sql.Types.VARCHAR);
							} else {
								pstmt_tDBOutput_2.setString(21,
										distribution.MX_diot_trans_type);
							}

							if (distribution.MX_foreign_tax_reg_num == null) {
								pstmt_tDBOutput_2.setNull(22,
										java.sql.Types.VARCHAR);
							} else {
								pstmt_tDBOutput_2.setString(22,
										distribution.MX_foreign_tax_reg_num);
							}

							if (distribution.MX_tax_reg_num == null) {
								pstmt_tDBOutput_2.setNull(23,
										java.sql.Types.VARCHAR);
							} else {
								pstmt_tDBOutput_2.setString(23,
										distribution.MX_tax_reg_num);
							}

							if (distribution.MX_tax_reg_num_type == null) {
								pstmt_tDBOutput_2.setNull(24,
										java.sql.Types.VARCHAR);
							} else {
								pstmt_tDBOutput_2.setString(24,
										distribution.MX_tax_reg_num_type);
							}

							if (distribution.MX_vendor_name == null) {
								pstmt_tDBOutput_2.setNull(25,
										java.sql.Types.VARCHAR);
							} else {
								pstmt_tDBOutput_2.setString(25,
										distribution.MX_vendor_name);
							}

							pstmt_tDBOutput_2.addBatch();
							nb_line_tDBOutput_2++;

							if (log.isDebugEnabled())
								log.debug("tDBOutput_2 - "
										+ ("Adding the record ")
										+ (nb_line_tDBOutput_2) + (" to the ")
										+ ("INSERT") + (" batch."));
							batchSizeCounter_tDBOutput_2++;

							// ////////batch execute by batch size///////
							class LimitBytesHelper_tDBOutput_2 {
								public int limitBytePart1(
										int counter,
										java.sql.PreparedStatement pstmt_tDBOutput_2)
										throws Exception {
									try {

										if (log.isDebugEnabled())
											log.debug("tDBOutput_2 - "
													+ ("Executing the ")
													+ ("INSERT") + (" batch."));
										for (int countEach_tDBOutput_2 : pstmt_tDBOutput_2
												.executeBatch()) {
											if (countEach_tDBOutput_2 == -2
													|| countEach_tDBOutput_2 == -3) {
												break;
											}
											counter += countEach_tDBOutput_2;
										}

										if (log.isDebugEnabled())
											log.debug("tDBOutput_2 - "
													+ ("The ")
													+ ("INSERT")
													+ (" batch execution has succeeded."));
									} catch (java.sql.BatchUpdateException e) {

										int countSum_tDBOutput_2 = 0;
										for (int countEach_tDBOutput_2 : e
												.getUpdateCounts()) {
											counter += (countEach_tDBOutput_2 < 0 ? 0
													: countEach_tDBOutput_2);
										}

										log.error("tDBOutput_2 - "
												+ (e.getMessage()));
										System.err.println(e.getMessage());

									}
									return counter;
								}

								public int limitBytePart2(
										int counter,
										java.sql.PreparedStatement pstmt_tDBOutput_2)
										throws Exception {
									try {

										if (log.isDebugEnabled())
											log.debug("tDBOutput_2 - "
													+ ("Executing the ")
													+ ("INSERT") + (" batch."));
										for (int countEach_tDBOutput_2 : pstmt_tDBOutput_2
												.executeBatch()) {
											if (countEach_tDBOutput_2 == -2
													|| countEach_tDBOutput_2 == -3) {
												break;
											}
											counter += countEach_tDBOutput_2;
										}

										if (log.isDebugEnabled())
											log.debug("tDBOutput_2 - "
													+ ("The ")
													+ ("INSERT")
													+ (" batch execution has succeeded."));
									} catch (java.sql.BatchUpdateException e) {

										for (int countEach_tDBOutput_2 : e
												.getUpdateCounts()) {
											counter += (countEach_tDBOutput_2 < 0 ? 0
													: countEach_tDBOutput_2);
										}

										log.error("tDBOutput_2 - "
												+ (e.getMessage()));
										System.err.println(e.getMessage());

									}
									return counter;
								}
							}
							if ((batchSize_tDBOutput_2 > 0)
									&& (batchSize_tDBOutput_2 <= batchSizeCounter_tDBOutput_2)) {

								insertedCount_tDBOutput_2 = new LimitBytesHelper_tDBOutput_2()
										.limitBytePart1(
												insertedCount_tDBOutput_2,
												pstmt_tDBOutput_2);

								batchSizeCounter_tDBOutput_2 = 0;
							}

							// //////////commit every////////////

							commitCounter_tDBOutput_2++;
							if (commitEvery_tDBOutput_2 <= commitCounter_tDBOutput_2) {
								if ((batchSize_tDBOutput_2 > 0)
										&& (batchSizeCounter_tDBOutput_2 > 0)) {

									insertedCount_tDBOutput_2 = new LimitBytesHelper_tDBOutput_2()
											.limitBytePart1(
													insertedCount_tDBOutput_2,
													pstmt_tDBOutput_2);

									batchSizeCounter_tDBOutput_2 = 0;
								}

								if (log.isDebugEnabled())
									log.debug("tDBOutput_2 - "
											+ ("Connection starting to commit ")
											+ (commitCounter_tDBOutput_2)
											+ (" record(s)."));
								conn_tDBOutput_2.commit();

								if (log.isDebugEnabled())
									log.debug("tDBOutput_2 - "
											+ ("Connection commit has succeeded."));
								commitCounter_tDBOutput_2 = 0;
							}

							tos_count_tDBOutput_2++;

							/**
							 * [tDBOutput_2 main ] stop
							 */

							/**
							 * [tDBOutput_2 process_data_begin ] start
							 */

							currentComponent = "tDBOutput_2";

							/**
							 * [tDBOutput_2 process_data_begin ] stop
							 */

							/**
							 * [tDBOutput_2 process_data_end ] start
							 */

							currentComponent = "tDBOutput_2";

							/**
							 * [tDBOutput_2 process_data_end ] stop
							 */

						} // End of branch "distribution"

						/**
						 * [tMap_1 process_data_end ] start
						 */

						currentComponent = "tMap_1";

						/**
						 * [tMap_1 process_data_end ] stop
						 */

						/**
						 * [tDBInput_4 process_data_end ] start
						 */

						currentComponent = "tDBInput_4";

						/**
						 * [tDBInput_4 process_data_end ] stop
						 */

						/**
						 * [tDBInput_4 end ] start
						 */

						currentComponent = "tDBInput_4";

					}
				} finally {
					if (rs_tDBInput_4 != null) {
						rs_tDBInput_4.close();
					}
					if (stmt_tDBInput_4 != null) {
						stmt_tDBInput_4.close();
					}
					if (conn_tDBInput_4 != null && !conn_tDBInput_4.isClosed()) {

						log.debug("tDBInput_4 - Closing the connection to the database.");

						conn_tDBInput_4.close();

						log.debug("tDBInput_4 - Connection to the database closed.");

					}
				}
				globalMap.put("tDBInput_4_NB_LINE", nb_line_tDBInput_4);
				log.debug("tDBInput_4 - Retrieved records count: "
						+ nb_line_tDBInput_4 + " .");

				if (log.isDebugEnabled())
					log.debug("tDBInput_4 - " + ("Done."));

				ok_Hash.put("tDBInput_4", true);
				end_Hash.put("tDBInput_4", System.currentTimeMillis());

				/**
				 * [tDBInput_4 end ] stop
				 */

				/**
				 * [tMap_1 end ] start
				 */

				currentComponent = "tMap_1";

				// ###############################
				// # Lookup hashes releasing
				// ###############################
				log.debug("tMap_1 - Written records count in the table 'distribution': "
						+ count_distribution_tMap_1 + ".");

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null
							|| !((Boolean) resourceMap.get("inIterateVComp"))) {
						runStat.updateStatOnConnection("row2" + iterateId, 2, 0);
					}
				}

				if (log.isDebugEnabled())
					log.debug("tMap_1 - " + ("Done."));

				ok_Hash.put("tMap_1", true);
				end_Hash.put("tMap_1", System.currentTimeMillis());

				/**
				 * [tMap_1 end ] stop
				 */

				/**
				 * [tDBOutput_2 end ] start
				 */

				currentComponent = "tDBOutput_2";

				try {
					int countSum_tDBOutput_2 = 0;
					if (pstmt_tDBOutput_2 != null
							&& batchSizeCounter_tDBOutput_2 > 0) {

						if (log.isDebugEnabled())
							log.debug("tDBOutput_2 - " + ("Executing the ")
									+ ("INSERT") + (" batch."));
						for (int countEach_tDBOutput_2 : pstmt_tDBOutput_2
								.executeBatch()) {
							if (countEach_tDBOutput_2 == -2
									|| countEach_tDBOutput_2 == -3) {
								break;
							}
							countSum_tDBOutput_2 += countEach_tDBOutput_2;
						}

						if (log.isDebugEnabled())
							log.debug("tDBOutput_2 - " + ("The ") + ("INSERT")
									+ (" batch execution has succeeded."));
					}

					insertedCount_tDBOutput_2 += countSum_tDBOutput_2;

				} catch (java.sql.BatchUpdateException e) {

					int countSum_tDBOutput_2 = 0;
					for (int countEach_tDBOutput_2 : e.getUpdateCounts()) {
						countSum_tDBOutput_2 += (countEach_tDBOutput_2 < 0 ? 0
								: countEach_tDBOutput_2);
					}

					insertedCount_tDBOutput_2 += countSum_tDBOutput_2;

					log.error("tDBOutput_2 - " + (e.getMessage()));
					System.err.println(e.getMessage());

				}
				if (pstmt_tDBOutput_2 != null) {

					pstmt_tDBOutput_2.close();
					resourceMap.remove("pstmt_tDBOutput_2");

				}
				resourceMap.put("statementClosed_tDBOutput_2", true);
				if (log.isDebugEnabled())
					log.debug("tDBOutput_2 - "
							+ ("Connection starting to commit ")
							+ (commitCounter_tDBOutput_2) + (" record(s)."));
				conn_tDBOutput_2.commit();

				if (log.isDebugEnabled())
					log.debug("tDBOutput_2 - "
							+ ("Connection commit has succeeded."));
				if (log.isDebugEnabled())
					log.debug("tDBOutput_2 - "
							+ ("Closing the connection to the database."));
				conn_tDBOutput_2.close();
				if (log.isDebugEnabled())
					log.debug("tDBOutput_2 - "
							+ ("Connection to the database has closed."));
				resourceMap.put("finish_tDBOutput_2", true);

				nb_line_deleted_tDBOutput_2 = nb_line_deleted_tDBOutput_2
						+ deletedCount_tDBOutput_2;
				nb_line_update_tDBOutput_2 = nb_line_update_tDBOutput_2
						+ updatedCount_tDBOutput_2;
				nb_line_inserted_tDBOutput_2 = nb_line_inserted_tDBOutput_2
						+ insertedCount_tDBOutput_2;
				nb_line_rejected_tDBOutput_2 = nb_line_rejected_tDBOutput_2
						+ rejectedCount_tDBOutput_2;

				globalMap.put("tDBOutput_2_NB_LINE", nb_line_tDBOutput_2);
				globalMap.put("tDBOutput_2_NB_LINE_UPDATED",
						nb_line_update_tDBOutput_2);
				globalMap.put("tDBOutput_2_NB_LINE_INSERTED",
						nb_line_inserted_tDBOutput_2);
				globalMap.put("tDBOutput_2_NB_LINE_DELETED",
						nb_line_deleted_tDBOutput_2);
				globalMap.put("tDBOutput_2_NB_LINE_REJECTED",
						nb_line_rejected_tDBOutput_2);

				if (log.isDebugEnabled())
					log.debug("tDBOutput_2 - " + ("Has ") + ("inserted")
							+ (" ") + (nb_line_inserted_tDBOutput_2)
							+ (" record(s)."));

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null
							|| !((Boolean) resourceMap.get("inIterateVComp"))) {
						runStat.updateStatOnConnection("distribution"
								+ iterateId, 2, 0);
					}
				}

				if (log.isDebugEnabled())
					log.debug("tDBOutput_2 - " + ("Done."));

				ok_Hash.put("tDBOutput_2", true);
				end_Hash.put("tDBOutput_2", System.currentTimeMillis());

				/**
				 * [tDBOutput_2 end ] stop
				 */

			}// end the resume

		} catch (java.lang.Exception e) {

			if (!(e instanceof TalendException)) {
				log.fatal(currentComponent + " " + e.getMessage(), e);
			}

			TalendException te = new TalendException(e, currentComponent,
					globalMap);

			throw te;
		} catch (java.lang.Error error) {

			runStat.stopThreadStat();

			throw error;
		} finally {

			try {

				/**
				 * [tDBInput_4 finally ] start
				 */

				currentComponent = "tDBInput_4";

				/**
				 * [tDBInput_4 finally ] stop
				 */

				/**
				 * [tMap_1 finally ] start
				 */

				currentComponent = "tMap_1";

				/**
				 * [tMap_1 finally ] stop
				 */

				/**
				 * [tDBOutput_2 finally ] start
				 */

				currentComponent = "tDBOutput_2";

				try {
					if (resourceMap.get("statementClosed_tDBOutput_2") == null) {
						java.sql.PreparedStatement pstmtToClose_tDBOutput_2 = null;
						if ((pstmtToClose_tDBOutput_2 = (java.sql.PreparedStatement) resourceMap
								.remove("pstmt_tDBOutput_2")) != null) {
							pstmtToClose_tDBOutput_2.close();
						}
					}
				} finally {
					if (resourceMap.get("finish_tDBOutput_2") == null) {
						java.sql.Connection ctn_tDBOutput_2 = null;
						if ((ctn_tDBOutput_2 = (java.sql.Connection) resourceMap
								.get("conn_tDBOutput_2")) != null) {
							try {
								if (log.isDebugEnabled())
									log.debug("tDBOutput_2 - "
											+ ("Closing the connection to the database."));
								ctn_tDBOutput_2.close();
								if (log.isDebugEnabled())
									log.debug("tDBOutput_2 - "
											+ ("Connection to the database has closed."));
							} catch (java.sql.SQLException sqlEx_tDBOutput_2) {
								String errorMessage_tDBOutput_2 = "failed to close the connection in tDBOutput_2 :"
										+ sqlEx_tDBOutput_2.getMessage();
								log.error("tDBOutput_2 - "
										+ (errorMessage_tDBOutput_2));
								System.err.println(errorMessage_tDBOutput_2);
							}
						}
					}
				}

				/**
				 * [tDBOutput_2 finally ] stop
				 */

			} catch (java.lang.Exception e) {
				// ignore
			} catch (java.lang.Error error) {
				// ignore
			}
			resourceMap = null;
		}

		globalMap.put("tDBInput_4_SUBPROCESS_STATE", 1);
	}

	public static class row1Struct implements
			routines.system.IPersistableRow<row1Struct> {
		final static byte[] commonByteArrayLock_DEV_AP_Transactions = new byte[0];
		static byte[] commonByteArray_DEV_AP_Transactions = new byte[0];

		public Integer maxvoucher;

		public Integer getMaxvoucher() {
			return this.maxvoucher;
		}

		private Integer readInteger(ObjectInputStream dis) throws IOException {
			Integer intReturn;
			int length = 0;
			length = dis.readByte();
			if (length == -1) {
				intReturn = null;
			} else {
				intReturn = dis.readInt();
			}
			return intReturn;
		}

		private void writeInteger(Integer intNum, ObjectOutputStream dos)
				throws IOException {
			if (intNum == null) {
				dos.writeByte(-1);
			} else {
				dos.writeByte(0);
				dos.writeInt(intNum);
			}
		}

		public void readData(ObjectInputStream dis) {

			synchronized (commonByteArrayLock_DEV_AP_Transactions) {

				try {

					int length = 0;

					this.maxvoucher = readInteger(dis);

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void writeData(ObjectOutputStream dos) {
			try {

				// Integer

				writeInteger(this.maxvoucher, dos);

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public String toString() {

			StringBuilder sb = new StringBuilder();
			sb.append(super.toString());
			sb.append("[");
			sb.append("maxvoucher=" + String.valueOf(maxvoucher));
			sb.append("]");

			return sb.toString();
		}

		public String toLogString() {
			StringBuilder sb = new StringBuilder();

			if (maxvoucher == null) {
				sb.append("<null>");
			} else {
				sb.append(maxvoucher);
			}

			sb.append("|");

			return sb.toString();
		}

		/**
		 * Compare keys
		 */
		public int compareTo(row1Struct other) {

			int returnValue = -1;

			return returnValue;
		}

		private int checkNullsAndCompare(Object object1, Object object2) {
			int returnValue = 0;
			if (object1 instanceof Comparable && object2 instanceof Comparable) {
				returnValue = ((Comparable) object1).compareTo(object2);
			} else if (object1 != null && object2 != null) {
				returnValue = compareStrings(object1.toString(),
						object2.toString());
			} else if (object1 == null && object2 != null) {
				returnValue = 1;
			} else if (object1 != null && object2 == null) {
				returnValue = -1;
			} else {
				returnValue = 0;
			}

			return returnValue;
		}

		private int compareStrings(String string1, String string2) {
			return string1.compareTo(string2);
		}

	}

	public void tDBInput_3Process(final java.util.Map<String, Object> globalMap)
			throws TalendException {
		globalMap.put("tDBInput_3_SUBPROCESS_STATE", 0);

		final boolean execStat = this.execStat;

		String iterateId = "";

		String currentComponent = "";
		java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

		try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception()
						.getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { // start the resume
				globalResumeTicket = true;

				row1Struct row1 = new row1Struct();

				/**
				 * [tAdvancedHash_row1 begin ] start
				 */

				ok_Hash.put("tAdvancedHash_row1", false);
				start_Hash
						.put("tAdvancedHash_row1", System.currentTimeMillis());

				currentComponent = "tAdvancedHash_row1";

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null) {

						runStat.updateStatOnConnection("row1" + iterateId, 0, 0);

					}
				}

				int tos_count_tAdvancedHash_row1 = 0;

				// connection name:row1
				// source node:tDBInput_3 - inputs:(after_tFileInputDelimited_1)
				// outputs:(row1,row1) | target node:tAdvancedHash_row1 -
				// inputs:(row1) outputs:()
				// linked node: tMap_3 - inputs:(FileImport,row1)
				// outputs:(StageDb)

				org.talend.designer.components.lookup.common.ICommonLookup.MATCHING_MODE matchingModeEnum_row1 = org.talend.designer.components.lookup.common.ICommonLookup.MATCHING_MODE.ALL_ROWS;

				org.talend.designer.components.lookup.memory.AdvancedMemoryLookup<row1Struct> tHash_Lookup_row1 = org.talend.designer.components.lookup.memory.AdvancedMemoryLookup
						.<row1Struct> getLookup(matchingModeEnum_row1);

				globalMap.put("tHash_Lookup_row1", tHash_Lookup_row1);

				/**
				 * [tAdvancedHash_row1 begin ] stop
				 */

				/**
				 * [tDBInput_3 begin ] start
				 */

				ok_Hash.put("tDBInput_3", false);
				start_Hash.put("tDBInput_3", System.currentTimeMillis());

				currentComponent = "tDBInput_3";

				int tos_count_tDBInput_3 = 0;

				if (log.isDebugEnabled())
					log.debug("tDBInput_3 - " + ("Start to work."));
				if (log.isDebugEnabled()) {
					class BytesLimit65535_tDBInput_3 {
						public void limitLog4jByte() throws Exception {
							StringBuilder log4jParamters_tDBInput_3 = new StringBuilder();
							log4jParamters_tDBInput_3.append("Parameters:");
							log4jParamters_tDBInput_3
									.append("USE_EXISTING_CONNECTION" + " = "
											+ "false");
							log4jParamters_tDBInput_3.append(" | ");
							log4jParamters_tDBInput_3.append("HOST" + " = "
									+ "\"NSMWQNXINFDB01\"");
							log4jParamters_tDBInput_3.append(" | ");
							log4jParamters_tDBInput_3.append("DRIVER" + " = "
									+ "JTDS");
							log4jParamters_tDBInput_3.append(" | ");
							log4jParamters_tDBInput_3.append("PORT" + " = "
									+ "\"1433\"");
							log4jParamters_tDBInput_3.append(" | ");
							log4jParamters_tDBInput_3.append("DB_SCHEMA"
									+ " = " + "\"dbo\"");
							log4jParamters_tDBInput_3.append(" | ");
							log4jParamters_tDBInput_3.append("DBNAME" + " = "
									+ "\"QBP_HO_APP\"");
							log4jParamters_tDBInput_3.append(" | ");
							log4jParamters_tDBInput_3.append("USER" + " = "
									+ "\"sa\"");
							log4jParamters_tDBInput_3.append(" | ");
							log4jParamters_tDBInput_3.append("PASS"
									+ " = "
									+ String.valueOf(
											"bf98e2c82ba433c4880df380fbb39fe9")
											.substring(0, 4) + "...");
							log4jParamters_tDBInput_3.append(" | ");
							log4jParamters_tDBInput_3.append("TABLE" + " = "
									+ "\"\"");
							log4jParamters_tDBInput_3.append(" | ");
							log4jParamters_tDBInput_3.append("QUERYSTORE"
									+ " = " + "\"\"");
							log4jParamters_tDBInput_3.append(" | ");
							log4jParamters_tDBInput_3
									.append("QUERY"
											+ " = "
											+ "\"select max(a.maxvoucher) as maxvoucher from (select max(voucher) as maxvoucher from aptrx_mst  union  select max(voucher) as maxvoucher from aptrxp_mst) a \"");
							log4jParamters_tDBInput_3.append(" | ");
							log4jParamters_tDBInput_3
									.append("SPECIFY_DATASOURCE_ALIAS" + " = "
											+ "false");
							log4jParamters_tDBInput_3.append(" | ");
							log4jParamters_tDBInput_3.append("PROPERTIES"
									+ " = " + "\"\"");
							log4jParamters_tDBInput_3.append(" | ");
							log4jParamters_tDBInput_3.append("TRIM_ALL_COLUMN"
									+ " = " + "false");
							log4jParamters_tDBInput_3.append(" | ");
							log4jParamters_tDBInput_3.append("TRIM_COLUMN"
									+ " = " + "[{TRIM=" + ("false")
									+ ", SCHEMA_COLUMN=" + ("maxvoucher")
									+ "}]");
							log4jParamters_tDBInput_3.append(" | ");
							log4jParamters_tDBInput_3
									.append("UNIFIED_COMPONENTS" + " = "
											+ "tMSSqlInput");
							log4jParamters_tDBInput_3.append(" | ");
							if (log.isDebugEnabled())
								log.debug("tDBInput_3 - "
										+ (log4jParamters_tDBInput_3));
						}
					}
					new BytesLimit65535_tDBInput_3().limitLog4jByte();
				}

				org.talend.designer.components.util.mssql.MSSqlGenerateTimestampUtil mssqlGTU_tDBInput_3 = org.talend.designer.components.util.mssql.MSSqlUtilFactory
						.getMSSqlGenerateTimestampUtil();

				java.util.List<String> talendToDBList_tDBInput_3 = new java.util.ArrayList();
				String[] talendToDBArray_tDBInput_3 = new String[] { "FLOAT",
						"NUMERIC", "NUMERIC IDENTITY", "DECIMAL",
						"DECIMAL IDENTITY", "REAL" };
				java.util.Collections.addAll(talendToDBList_tDBInput_3,
						talendToDBArray_tDBInput_3);
				int nb_line_tDBInput_3 = 0;
				java.sql.Connection conn_tDBInput_3 = null;
				String driverClass_tDBInput_3 = "net.sourceforge.jtds.jdbc.Driver";
				java.lang.Class.forName(driverClass_tDBInput_3);
				String dbUser_tDBInput_3 = "sa";

				final String decryptedPassword_tDBInput_3 = routines.system.PasswordEncryptUtil
						.decryptPassword("bf98e2c82ba433c4880df380fbb39fe9");

				String dbPwd_tDBInput_3 = decryptedPassword_tDBInput_3;

				String port_tDBInput_3 = "1433";
				String dbname_tDBInput_3 = "QBP_HO_APP";
				String url_tDBInput_3 = "jdbc:jtds:sqlserver://"
						+ "NSMWQNXINFDB01";
				if (!"".equals(port_tDBInput_3)) {
					url_tDBInput_3 += ":" + "1433";
				}
				if (!"".equals(dbname_tDBInput_3)) {
					url_tDBInput_3 += "//" + "QBP_HO_APP";
				}
				url_tDBInput_3 += ";appName=" + projectName + ";" + "";
				String dbschema_tDBInput_3 = "dbo";

				log.debug("tDBInput_3 - Driver ClassName: "
						+ driverClass_tDBInput_3 + ".");

				log.debug("tDBInput_3 - Connection attempt to '"
						+ url_tDBInput_3 + "' with the username '"
						+ dbUser_tDBInput_3 + "'.");

				conn_tDBInput_3 = java.sql.DriverManager.getConnection(
						url_tDBInput_3, dbUser_tDBInput_3, dbPwd_tDBInput_3);
				log.debug("tDBInput_3 - Connection to '" + url_tDBInput_3
						+ "' has succeeded.");

				java.sql.Statement stmt_tDBInput_3 = conn_tDBInput_3
						.createStatement();

				String dbquery_tDBInput_3 = "select max(a.maxvoucher) as maxvoucher from (select max(voucher) as maxvoucher from aptrx_mst\nunion\nselect max(vouche"
						+ "r) as maxvoucher from aptrxp_mst) a ";

				log.debug("tDBInput_3 - Executing the query: '"
						+ dbquery_tDBInput_3 + "'.");

				globalMap.put("tDBInput_3_QUERY", dbquery_tDBInput_3);
				java.sql.ResultSet rs_tDBInput_3 = null;

				try {
					rs_tDBInput_3 = stmt_tDBInput_3
							.executeQuery(dbquery_tDBInput_3);
					java.sql.ResultSetMetaData rsmd_tDBInput_3 = rs_tDBInput_3
							.getMetaData();
					int colQtyInRs_tDBInput_3 = rsmd_tDBInput_3
							.getColumnCount();

					String tmpContent_tDBInput_3 = null;

					log.debug("tDBInput_3 - Retrieving records from the database.");

					while (rs_tDBInput_3.next()) {
						nb_line_tDBInput_3++;

						if (colQtyInRs_tDBInput_3 < 1) {
							row1.maxvoucher = null;
						} else {

							if (rs_tDBInput_3.getObject(1) != null) {
								row1.maxvoucher = rs_tDBInput_3.getInt(1);
							} else {
								row1.maxvoucher = null;
							}
						}

						log.debug("tDBInput_3 - Retrieving the record "
								+ nb_line_tDBInput_3 + ".");

						/**
						 * [tDBInput_3 begin ] stop
						 */

						/**
						 * [tDBInput_3 main ] start
						 */

						currentComponent = "tDBInput_3";

						tos_count_tDBInput_3++;

						/**
						 * [tDBInput_3 main ] stop
						 */

						/**
						 * [tDBInput_3 process_data_begin ] start
						 */

						currentComponent = "tDBInput_3";

						/**
						 * [tDBInput_3 process_data_begin ] stop
						 */

						/**
						 * [tAdvancedHash_row1 main ] start
						 */

						currentComponent = "tAdvancedHash_row1";

						// row1
						// row1

						if (execStat) {
							runStat.updateStatOnConnection("row1" + iterateId,
									1, 1);
						}

						if (log.isTraceEnabled()) {
							log.trace("row1 - "
									+ (row1 == null ? "" : row1.toLogString()));
						}

						row1Struct row1_HashRow = new row1Struct();

						row1_HashRow.maxvoucher = row1.maxvoucher;

						tHash_Lookup_row1.put(row1_HashRow);

						tos_count_tAdvancedHash_row1++;

						/**
						 * [tAdvancedHash_row1 main ] stop
						 */

						/**
						 * [tAdvancedHash_row1 process_data_begin ] start
						 */

						currentComponent = "tAdvancedHash_row1";

						/**
						 * [tAdvancedHash_row1 process_data_begin ] stop
						 */

						/**
						 * [tAdvancedHash_row1 process_data_end ] start
						 */

						currentComponent = "tAdvancedHash_row1";

						/**
						 * [tAdvancedHash_row1 process_data_end ] stop
						 */

						/**
						 * [tDBInput_3 process_data_end ] start
						 */

						currentComponent = "tDBInput_3";

						/**
						 * [tDBInput_3 process_data_end ] stop
						 */

						/**
						 * [tDBInput_3 end ] start
						 */

						currentComponent = "tDBInput_3";

					}
				} finally {
					if (rs_tDBInput_3 != null) {
						rs_tDBInput_3.close();
					}
					if (stmt_tDBInput_3 != null) {
						stmt_tDBInput_3.close();
					}
					if (conn_tDBInput_3 != null && !conn_tDBInput_3.isClosed()) {

						log.debug("tDBInput_3 - Closing the connection to the database.");

						conn_tDBInput_3.close();

						log.debug("tDBInput_3 - Connection to the database closed.");

					}
				}
				globalMap.put("tDBInput_3_NB_LINE", nb_line_tDBInput_3);
				log.debug("tDBInput_3 - Retrieved records count: "
						+ nb_line_tDBInput_3 + " .");

				if (log.isDebugEnabled())
					log.debug("tDBInput_3 - " + ("Done."));

				ok_Hash.put("tDBInput_3", true);
				end_Hash.put("tDBInput_3", System.currentTimeMillis());

				/**
				 * [tDBInput_3 end ] stop
				 */

				/**
				 * [tAdvancedHash_row1 end ] start
				 */

				currentComponent = "tAdvancedHash_row1";

				tHash_Lookup_row1.endPut();

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null
							|| !((Boolean) resourceMap.get("inIterateVComp"))) {
						runStat.updateStatOnConnection("row1" + iterateId, 2, 0);
					}
				}

				ok_Hash.put("tAdvancedHash_row1", true);
				end_Hash.put("tAdvancedHash_row1", System.currentTimeMillis());

				/**
				 * [tAdvancedHash_row1 end ] stop
				 */

			}// end the resume

		} catch (java.lang.Exception e) {

			if (!(e instanceof TalendException)) {
				log.fatal(currentComponent + " " + e.getMessage(), e);
			}

			TalendException te = new TalendException(e, currentComponent,
					globalMap);

			throw te;
		} catch (java.lang.Error error) {

			runStat.stopThreadStat();

			throw error;
		} finally {

			try {

				/**
				 * [tDBInput_3 finally ] start
				 */

				currentComponent = "tDBInput_3";

				/**
				 * [tDBInput_3 finally ] stop
				 */

				/**
				 * [tAdvancedHash_row1 finally ] start
				 */

				currentComponent = "tAdvancedHash_row1";

				/**
				 * [tAdvancedHash_row1 finally ] stop
				 */

			} catch (java.lang.Exception e) {
				// ignore
			} catch (java.lang.Error error) {
				// ignore
			}
			resourceMap = null;
		}

		globalMap.put("tDBInput_3_SUBPROCESS_STATE", 1);
	}

	public String resuming_logs_dir_path = null;
	public String resuming_checkpoint_path = null;
	public String parent_part_launcher = null;
	private String resumeEntryMethodName = null;
	private boolean globalResumeTicket = false;

	public boolean watch = false;
	// portStats is null, it means don't execute the statistics
	public Integer portStats = null;
	public int portTraces = 4334;
	public String clientHost;
	public String defaultClientHost = "localhost";
	public String contextStr = "Default";
	public boolean isDefaultContext = true;
	public String pid = "0";
	public String rootPid = null;
	public String fatherPid = null;
	public String fatherNode = null;
	public long startTime = 0;
	public boolean isChildJob = false;
	public String log4jLevel = "";

	private boolean execStat = true;

	private ThreadLocal<java.util.Map<String, String>> threadLocal = new ThreadLocal<java.util.Map<String, String>>() {
		protected java.util.Map<String, String> initialValue() {
			java.util.Map<String, String> threadRunResultMap = new java.util.HashMap<String, String>();
			threadRunResultMap.put("errorCode", null);
			threadRunResultMap.put("status", "");
			return threadRunResultMap;
		};
	};

	private PropertiesWithType context_param = new PropertiesWithType();
	public java.util.Map<String, Object> parentContextMap = new java.util.HashMap<String, Object>();

	public String status = "";

	public static void main(String[] args) {
		final AP_Transactions AP_TransactionsClass = new AP_Transactions();

		int exitCode = AP_TransactionsClass.runJobInTOS(args);
		if (exitCode == 0) {
			log.info("TalendJob: 'AP_Transactions' - Done.");
		}

		System.exit(exitCode);
	}

	public String[][] runJob(String[] args) {

		int exitCode = runJobInTOS(args);
		String[][] bufferValue = new String[][] { { Integer.toString(exitCode) } };

		return bufferValue;
	}

	public boolean hastBufferOutputComponent() {
		boolean hastBufferOutput = false;

		return hastBufferOutput;
	}

	public int runJobInTOS(String[] args) {
		// reset status
		status = "";

		String lastStr = "";
		for (String arg : args) {
			if (arg.equalsIgnoreCase("--context_param")) {
				lastStr = arg;
			} else if (lastStr.equals("")) {
				evalParam(arg);
			} else {
				evalParam(lastStr + " " + arg);
				lastStr = "";
			}
		}

		if (!"".equals(log4jLevel)) {
			if ("trace".equalsIgnoreCase(log4jLevel)) {
				log.setLevel(org.apache.log4j.Level.TRACE);
			} else if ("debug".equalsIgnoreCase(log4jLevel)) {
				log.setLevel(org.apache.log4j.Level.DEBUG);
			} else if ("info".equalsIgnoreCase(log4jLevel)) {
				log.setLevel(org.apache.log4j.Level.INFO);
			} else if ("warn".equalsIgnoreCase(log4jLevel)) {
				log.setLevel(org.apache.log4j.Level.WARN);
			} else if ("error".equalsIgnoreCase(log4jLevel)) {
				log.setLevel(org.apache.log4j.Level.ERROR);
			} else if ("fatal".equalsIgnoreCase(log4jLevel)) {
				log.setLevel(org.apache.log4j.Level.FATAL);
			} else if ("off".equalsIgnoreCase(log4jLevel)) {
				log.setLevel(org.apache.log4j.Level.OFF);
			}
			org.apache.log4j.Logger.getRootLogger().setLevel(log.getLevel());
		}
		log.info("TalendJob: 'AP_Transactions' - Start.");

		if (clientHost == null) {
			clientHost = defaultClientHost;
		}

		if (pid == null || "0".equals(pid)) {
			pid = TalendString.getAsciiRandomString(6);
		}

		if (rootPid == null) {
			rootPid = pid;
		}
		if (fatherPid == null) {
			fatherPid = pid;
		} else {
			isChildJob = true;
		}

		if (portStats != null) {
			// portStats = -1; //for testing
			if (portStats < 0 || portStats > 65535) {
				// issue:10869, the portStats is invalid, so this client socket
				// can't open
				System.err.println("The statistics socket port " + portStats
						+ " is invalid.");
				execStat = false;
			}
		} else {
			execStat = false;
		}

		try {
			// call job/subjob with an existing context, like:
			// --context=production. if without this parameter, there will use
			// the default context instead.
			java.io.InputStream inContext = AP_Transactions.class
					.getClassLoader().getResourceAsStream(
							"dev/ap_transactions_0_1/contexts/" + contextStr
									+ ".properties");
			if (inContext == null) {
				inContext = AP_Transactions.class
						.getClassLoader()
						.getResourceAsStream(
								"config/contexts/" + contextStr + ".properties");
			}
			if (inContext != null && context != null && context.isEmpty()) {
				// defaultProps is in order to keep the original context value
				defaultProps.load(inContext);
				inContext.close();
				context = new ContextProperties(defaultProps);
			} else if (!isDefaultContext) {
				// print info and job continue to run, for case: context_param
				// is not empty.
				System.err.println("Could not find the context " + contextStr);
			}

			if (!context_param.isEmpty()) {
				context.putAll(context_param);
				// set types for params from parentJobs
				for (Object key : context_param.keySet()) {
					String context_key = key.toString();
					String context_type = context_param
							.getContextType(context_key);
					context.setContextType(context_key, context_type);

				}
			}
		} catch (java.io.IOException ie) {
			System.err.println("Could not load context " + contextStr);
			ie.printStackTrace();
		}

		// get context value from parent directly
		if (parentContextMap != null && !parentContextMap.isEmpty()) {
		}

		// Resume: init the resumeUtil
		resumeEntryMethodName = ResumeUtil
				.getResumeEntryMethodName(resuming_checkpoint_path);
		resumeUtil = new ResumeUtil(resuming_logs_dir_path, isChildJob, rootPid);
		resumeUtil.initCommonInfo(pid, rootPid, fatherPid, projectName,
				jobName, contextStr, jobVersion);

		List<String> parametersToEncrypt = new java.util.ArrayList<String>();
		// Resume: jobStart
		resumeUtil.addLog("JOB_STARTED", "JOB:" + jobName,
				parent_part_launcher, Thread.currentThread().getId() + "", "",
				"", "", "",
				resumeUtil.convertToJsonText(context, parametersToEncrypt));

		if (execStat) {
			try {
				runStat.openSocket(!isChildJob);
				runStat.setAllPID(rootPid, fatherPid, pid, jobName);
				runStat.startThreadStat(clientHost, portStats);
				runStat.updateStatOnJob(RunStat.JOBSTART, fatherNode);
			} catch (java.io.IOException ioException) {
				ioException.printStackTrace();
			}
		}

		java.util.concurrent.ConcurrentHashMap<Object, Object> concurrentHashMap = new java.util.concurrent.ConcurrentHashMap<Object, Object>();
		globalMap.put("concurrentHashMap", concurrentHashMap);

		long startUsedMemory = Runtime.getRuntime().totalMemory()
				- Runtime.getRuntime().freeMemory();
		long endUsedMemory = 0;
		long end = 0;

		startTime = System.currentTimeMillis();

		this.globalResumeTicket = true;// to run tPreJob

		this.globalResumeTicket = false;// to run others jobs

		try {
			errorCode = null;
			tFileInputDelimited_1Process(globalMap);
			if (!"failure".equals(status)) {
				status = "end";
			}
		} catch (TalendException e_tFileInputDelimited_1) {
			globalMap.put("tFileInputDelimited_1_SUBPROCESS_STATE", -1);

			e_tFileInputDelimited_1.printStackTrace();

		}

		this.globalResumeTicket = true;// to run tPostJob

		end = System.currentTimeMillis();

		if (watch) {
			System.out.println((end - startTime) + " milliseconds");
		}

		endUsedMemory = Runtime.getRuntime().totalMemory()
				- Runtime.getRuntime().freeMemory();
		if (false) {
			System.out.println((endUsedMemory - startUsedMemory)
					+ " bytes memory increase when running : AP_Transactions");
		}

		if (execStat) {
			runStat.updateStatOnJob(RunStat.JOBEND, fatherNode);
			runStat.stopThreadStat();
		}
		int returnCode = 0;
		if (errorCode == null) {
			returnCode = status != null && status.equals("failure") ? 1 : 0;
		} else {
			returnCode = errorCode.intValue();
		}
		resumeUtil.addLog("JOB_ENDED", "JOB:" + jobName, parent_part_launcher,
				Thread.currentThread().getId() + "", "", "" + returnCode, "",
				"", "");

		return returnCode;

	}

	// only for OSGi env
	public void destroy() {

	}

	private java.util.Map<String, Object> getSharedConnections4REST() {
		java.util.Map<String, Object> connections = new java.util.HashMap<String, Object>();

		return connections;
	}

	private void evalParam(String arg) {
		if (arg.startsWith("--resuming_logs_dir_path")) {
			resuming_logs_dir_path = arg.substring(25);
		} else if (arg.startsWith("--resuming_checkpoint_path")) {
			resuming_checkpoint_path = arg.substring(27);
		} else if (arg.startsWith("--parent_part_launcher")) {
			parent_part_launcher = arg.substring(23);
		} else if (arg.startsWith("--watch")) {
			watch = true;
		} else if (arg.startsWith("--stat_port=")) {
			String portStatsStr = arg.substring(12);
			if (portStatsStr != null && !portStatsStr.equals("null")) {
				portStats = Integer.parseInt(portStatsStr);
			}
		} else if (arg.startsWith("--trace_port=")) {
			portTraces = Integer.parseInt(arg.substring(13));
		} else if (arg.startsWith("--client_host=")) {
			clientHost = arg.substring(14);
		} else if (arg.startsWith("--context=")) {
			contextStr = arg.substring(10);
			isDefaultContext = false;
		} else if (arg.startsWith("--father_pid=")) {
			fatherPid = arg.substring(13);
		} else if (arg.startsWith("--root_pid=")) {
			rootPid = arg.substring(11);
		} else if (arg.startsWith("--father_node=")) {
			fatherNode = arg.substring(14);
		} else if (arg.startsWith("--pid=")) {
			pid = arg.substring(6);
		} else if (arg.startsWith("--context_type")) {
			String keyValue = arg.substring(15);
			int index = -1;
			if (keyValue != null && (index = keyValue.indexOf('=')) > -1) {
				if (fatherPid == null) {
					context_param.setContextType(keyValue.substring(0, index),
							replaceEscapeChars(keyValue.substring(index + 1)));
				} else { // the subjob won't escape the especial chars
					context_param.setContextType(keyValue.substring(0, index),
							keyValue.substring(index + 1));
				}

			}

		} else if (arg.startsWith("--context_param")) {
			String keyValue = arg.substring(16);
			int index = -1;
			if (keyValue != null && (index = keyValue.indexOf('=')) > -1) {
				if (fatherPid == null) {
					context_param.put(keyValue.substring(0, index),
							replaceEscapeChars(keyValue.substring(index + 1)));
				} else { // the subjob won't escape the especial chars
					context_param.put(keyValue.substring(0, index),
							keyValue.substring(index + 1));
				}
			}
		} else if (arg.startsWith("--log4jLevel=")) {
			log4jLevel = arg.substring(13);
		}

	}

	private static final String NULL_VALUE_EXPRESSION_IN_COMMAND_STRING_FOR_CHILD_JOB_ONLY = "<TALEND_NULL>";

	private final String[][] escapeChars = { { "\\\\", "\\" }, { "\\n", "\n" },
			{ "\\'", "\'" }, { "\\r", "\r" }, { "\\f", "\f" }, { "\\b", "\b" },
			{ "\\t", "\t" } };

	private String replaceEscapeChars(String keyValue) {

		if (keyValue == null || ("").equals(keyValue.trim())) {
			return keyValue;
		}

		StringBuilder result = new StringBuilder();
		int currIndex = 0;
		while (currIndex < keyValue.length()) {
			int index = -1;
			// judege if the left string includes escape chars
			for (String[] strArray : escapeChars) {
				index = keyValue.indexOf(strArray[0], currIndex);
				if (index >= 0) {

					result.append(keyValue.substring(currIndex,
							index + strArray[0].length()).replace(strArray[0],
							strArray[1]));
					currIndex = index + strArray[0].length();
					break;
				}
			}
			// if the left string doesn't include escape chars, append the left
			// into the result
			if (index < 0) {
				result.append(keyValue.substring(currIndex));
				currIndex = currIndex + keyValue.length();
			}
		}

		return result.toString();
	}

	public Integer getErrorCode() {
		return errorCode;
	}

	public String getStatus() {
		return status;
	}

	ResumeUtil resumeUtil = null;
}
/************************************************************************************************
 * 295934 characters generated by Talend Cloud Data Management Platform on the
 * February 19, 2019 3:15:55 PM CST
 ************************************************************************************************/
