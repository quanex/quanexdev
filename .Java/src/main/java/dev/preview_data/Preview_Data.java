package dev.preview_data;

import routines.DataOperation;
import routines.TalendDataGenerator;
import routines.DataQuality;
import routines.Relational;
import routines.DataQualityDependencies;
import routines.Mathematical;
import routines.SQLike;
import routines.Numeric;
import routines.TalendStringUtil;
import routines.TalendString;
import routines.DQTechnical;
import routines.StringHandling;
import routines.DataMasking;
import routines.TalendDate;
import routines.DqStringHandling;
import routines.system.*;
import routines.system.api.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.math.BigDecimal;
import java.io.ByteArrayOutputStream;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;
import java.io.IOException;
import java.util.Comparator;

@SuppressWarnings("unused")
/**
 * Job: Preview_Data Purpose: <br>
 * Description:  <br>
 * @author 
 * @version 7.1.1.20181026_1147
 * @status 
 */
public class Preview_Data implements TalendJob {

	protected static void logIgnoredError(String message, Throwable cause) {
		System.err.println(message);
		if (cause != null) {
			cause.printStackTrace();
		}

	}

	public final Object obj = new Object();

	// for transmiting parameters purpose
	private Object valueObject = null;

	public Object getValueObject() {
		return this.valueObject;
	}

	public void setValueObject(Object valueObject) {
		this.valueObject = valueObject;
	}

	private final static String defaultCharset = java.nio.charset.Charset
			.defaultCharset().name();

	private final static String utf8Charset = "UTF-8";

	// contains type for every context property
	public class PropertiesWithType extends java.util.Properties {
		private static final long serialVersionUID = 1L;
		private java.util.Map<String, String> propertyTypes = new java.util.HashMap<>();

		public PropertiesWithType(java.util.Properties properties) {
			super(properties);
		}

		public PropertiesWithType() {
			super();
		}

		public void setContextType(String key, String type) {
			propertyTypes.put(key, type);
		}

		public String getContextType(String key) {
			return propertyTypes.get(key);
		}
	}

	// create and load default properties
	private java.util.Properties defaultProps = new java.util.Properties();

	// create application properties with default
	public class ContextProperties extends PropertiesWithType {

		private static final long serialVersionUID = 1L;

		public ContextProperties(java.util.Properties properties) {
			super(properties);
		}

		public ContextProperties() {
			super();
		}

		public void synchronizeContext() {

		}

	}

	protected ContextProperties context = new ContextProperties(); // will be
																	// instanciated
																	// by MS.

	public ContextProperties getContext() {
		return this.context;
	}

	private final String jobVersion = "null";
	private final String jobName = "Preview_Data";
	private final String projectName = "DEV";
	public Integer errorCode = null;
	private String currentComponent = "";

	private final java.util.Map<String, Object> globalMap = new java.util.HashMap<String, Object>();
	private final static java.util.Map<String, Object> junitGlobalMap = new java.util.HashMap<String, Object>();

	private final java.util.Map<String, Long> start_Hash = new java.util.HashMap<String, Long>();
	private final java.util.Map<String, Long> end_Hash = new java.util.HashMap<String, Long>();
	private final java.util.Map<String, Boolean> ok_Hash = new java.util.HashMap<String, Boolean>();
	public final java.util.List<String[]> globalBuffer = new java.util.ArrayList<String[]>();

	// OSGi DataSource
	private final static String KEY_DB_DATASOURCES = "KEY_DB_DATASOURCES";

	private final static String KEY_DB_DATASOURCES_RAW = "KEY_DB_DATASOURCES_RAW";

	public void setDataSources(
			java.util.Map<String, javax.sql.DataSource> dataSources) {
		java.util.Map<String, routines.system.TalendDataSource> talendDataSources = new java.util.HashMap<String, routines.system.TalendDataSource>();
		for (java.util.Map.Entry<String, javax.sql.DataSource> dataSourceEntry : dataSources
				.entrySet()) {
			talendDataSources.put(
					dataSourceEntry.getKey(),
					new routines.system.TalendDataSource(dataSourceEntry
							.getValue()));
		}
		globalMap.put(KEY_DB_DATASOURCES, talendDataSources);
		globalMap
				.put(KEY_DB_DATASOURCES_RAW,
						new java.util.HashMap<String, javax.sql.DataSource>(
								dataSources));
	}

	private final java.io.ByteArrayOutputStream baos = new java.io.ByteArrayOutputStream();
	private final java.io.PrintStream errorMessagePS = new java.io.PrintStream(
			new java.io.BufferedOutputStream(baos));

	public String getExceptionStackTrace() {
		if ("failure".equals(this.getStatus())) {
			errorMessagePS.flush();
			return baos.toString();
		}
		return null;
	}

	private Exception exception;

	public Exception getException() {
		if ("failure".equals(this.getStatus())) {
			return this.exception;
		}
		return null;
	}

	private class TalendException extends Exception {

		private static final long serialVersionUID = 1L;

		private java.util.Map<String, Object> globalMap = null;
		private Exception e = null;
		private String currentComponent = null;
		private String virtualComponentName = null;

		public void setVirtualComponentName(String virtualComponentName) {
			this.virtualComponentName = virtualComponentName;
		}

		private TalendException(Exception e, String errorComponent,
				final java.util.Map<String, Object> globalMap) {
			this.currentComponent = errorComponent;
			this.globalMap = globalMap;
			this.e = e;
		}

		public Exception getException() {
			return this.e;
		}

		public String getCurrentComponent() {
			return this.currentComponent;
		}

		public String getExceptionCauseMessage(Exception e) {
			Throwable cause = e;
			String message = null;
			int i = 10;
			while (null != cause && 0 < i--) {
				message = cause.getMessage();
				if (null == message) {
					cause = cause.getCause();
				} else {
					break;
				}
			}
			if (null == message) {
				message = e.getClass().getName();
			}
			return message;
		}

		@Override
		public void printStackTrace() {
			if (!(e instanceof TalendException || e instanceof TDieException)) {
				if (virtualComponentName != null
						&& currentComponent.indexOf(virtualComponentName + "_") == 0) {
					globalMap.put(virtualComponentName + "_ERROR_MESSAGE",
							getExceptionCauseMessage(e));
				}
				globalMap.put(currentComponent + "_ERROR_MESSAGE",
						getExceptionCauseMessage(e));
				System.err.println("Exception in component " + currentComponent
						+ " (" + jobName + ")");
			}
			if (!(e instanceof TDieException)) {
				if (e instanceof TalendException) {
					e.printStackTrace();
				} else {
					e.printStackTrace();
					e.printStackTrace(errorMessagePS);
					Preview_Data.this.exception = e;
				}
			}
			if (!(e instanceof TalendException)) {
				try {
					for (java.lang.reflect.Method m : this.getClass()
							.getEnclosingClass().getMethods()) {
						if (m.getName().compareTo(currentComponent + "_error") == 0) {
							m.invoke(Preview_Data.this, new Object[] { e,
									currentComponent, globalMap });
							break;
						}
					}

					if (!(e instanceof TDieException)) {
					}
				} catch (Exception e) {
					this.e.printStackTrace();
				}
			}
		}
	}

	public void tDBInput_4_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tDBInput_4_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tJavaRow_gen_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tDBInput_4_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tSocketOutput_gen_error(Exception exception,
			String errorComponent, final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tDBInput_4_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tDBInput_4_onSubJobError(Exception exception,
			String errorComponent, final java.util.Map<String, Object> globalMap)
			throws TalendException {

		resumeUtil.addLog("SYSTEM_LOG", "NODE:" + errorComponent, "", Thread
				.currentThread().getId() + "", "FATAL", "",
				exception.getMessage(),
				ResumeUtil.getExceptionStackTrace(exception), "");

	}

	public static class rowDataFromtJavaRow_genStruct implements
			routines.system.IPersistableRow<rowDataFromtJavaRow_genStruct> {
		final static byte[] commonByteArrayLock_DEV_Preview_Data = new byte[0];
		static byte[] commonByteArray_DEV_Preview_Data = new byte[0];

		public String Employee_ID;

		public String getEmployee_ID() {
			return this.Employee_ID;
		}

		public java.util.Date Start_Date;

		public java.util.Date getStart_Date() {
			return this.Start_Date;
		}

		public java.util.Date End_Date;

		public java.util.Date getEnd_Date() {
			return this.End_Date;
		}

		public java.util.Date Process_Date;

		public java.util.Date getProcess_Date() {
			return this.Process_Date;
		}

		public String Approval_Code;

		public String getApproval_Code() {
			return this.Approval_Code;
		}

		public java.util.Date Expense_Date;

		public java.util.Date getExpense_Date() {
			return this.Expense_Date;
		}

		public String GL_Codes;

		public String getGL_Codes() {
			return this.GL_Codes;
		}

		public String Dept_Code;

		public String getDept_Code() {
			return this.Dept_Code;
		}

		public String Trad_Part;

		public String getTrad_Part() {
			return this.Trad_Part;
		}

		public String PSID;

		public String getPSID() {
			return this.PSID;
		}

		public Float Amount;

		public Float getAmount() {
			return this.Amount;
		}

		public Integer Voucher;

		public Integer getVoucher() {
			return this.Voucher;
		}

		public Integer Sequence;

		public Integer getSequence() {
			return this.Sequence;
		}

		private String readString(ObjectInputStream dis) throws IOException {
			String strReturn = null;
			int length = 0;
			length = dis.readInt();
			if (length == -1) {
				strReturn = null;
			} else {
				if (length > commonByteArray_DEV_Preview_Data.length) {
					if (length < 1024
							&& commonByteArray_DEV_Preview_Data.length == 0) {
						commonByteArray_DEV_Preview_Data = new byte[1024];
					} else {
						commonByteArray_DEV_Preview_Data = new byte[2 * length];
					}
				}
				dis.readFully(commonByteArray_DEV_Preview_Data, 0, length);
				strReturn = new String(commonByteArray_DEV_Preview_Data, 0,
						length, utf8Charset);
			}
			return strReturn;
		}

		private void writeString(String str, ObjectOutputStream dos)
				throws IOException {
			if (str == null) {
				dos.writeInt(-1);
			} else {
				byte[] byteArray = str.getBytes(utf8Charset);
				dos.writeInt(byteArray.length);
				dos.write(byteArray);
			}
		}

		private java.util.Date readDate(ObjectInputStream dis)
				throws IOException {
			java.util.Date dateReturn = null;
			int length = 0;
			length = dis.readByte();
			if (length == -1) {
				dateReturn = null;
			} else {
				dateReturn = new Date(dis.readLong());
			}
			return dateReturn;
		}

		private void writeDate(java.util.Date date1, ObjectOutputStream dos)
				throws IOException {
			if (date1 == null) {
				dos.writeByte(-1);
			} else {
				dos.writeByte(0);
				dos.writeLong(date1.getTime());
			}
		}

		private Integer readInteger(ObjectInputStream dis) throws IOException {
			Integer intReturn;
			int length = 0;
			length = dis.readByte();
			if (length == -1) {
				intReturn = null;
			} else {
				intReturn = dis.readInt();
			}
			return intReturn;
		}

		private void writeInteger(Integer intNum, ObjectOutputStream dos)
				throws IOException {
			if (intNum == null) {
				dos.writeByte(-1);
			} else {
				dos.writeByte(0);
				dos.writeInt(intNum);
			}
		}

		public void readData(ObjectInputStream dis) {

			synchronized (commonByteArrayLock_DEV_Preview_Data) {

				try {

					int length = 0;

					this.Employee_ID = readString(dis);

					this.Start_Date = readDate(dis);

					this.End_Date = readDate(dis);

					this.Process_Date = readDate(dis);

					this.Approval_Code = readString(dis);

					this.Expense_Date = readDate(dis);

					this.GL_Codes = readString(dis);

					this.Dept_Code = readString(dis);

					this.Trad_Part = readString(dis);

					this.PSID = readString(dis);

					length = dis.readByte();
					if (length == -1) {
						this.Amount = null;
					} else {
						this.Amount = dis.readFloat();
					}

					this.Voucher = readInteger(dis);

					this.Sequence = readInteger(dis);

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void writeData(ObjectOutputStream dos) {
			try {

				// String

				writeString(this.Employee_ID, dos);

				// java.util.Date

				writeDate(this.Start_Date, dos);

				// java.util.Date

				writeDate(this.End_Date, dos);

				// java.util.Date

				writeDate(this.Process_Date, dos);

				// String

				writeString(this.Approval_Code, dos);

				// java.util.Date

				writeDate(this.Expense_Date, dos);

				// String

				writeString(this.GL_Codes, dos);

				// String

				writeString(this.Dept_Code, dos);

				// String

				writeString(this.Trad_Part, dos);

				// String

				writeString(this.PSID, dos);

				// Float

				if (this.Amount == null) {
					dos.writeByte(-1);
				} else {
					dos.writeByte(0);
					dos.writeFloat(this.Amount);
				}

				// Integer

				writeInteger(this.Voucher, dos);

				// Integer

				writeInteger(this.Sequence, dos);

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public String toString() {

			StringBuilder sb = new StringBuilder();
			sb.append(super.toString());
			sb.append("[");
			sb.append("Employee_ID=" + Employee_ID);
			sb.append(",Start_Date=" + String.valueOf(Start_Date));
			sb.append(",End_Date=" + String.valueOf(End_Date));
			sb.append(",Process_Date=" + String.valueOf(Process_Date));
			sb.append(",Approval_Code=" + Approval_Code);
			sb.append(",Expense_Date=" + String.valueOf(Expense_Date));
			sb.append(",GL_Codes=" + GL_Codes);
			sb.append(",Dept_Code=" + Dept_Code);
			sb.append(",Trad_Part=" + Trad_Part);
			sb.append(",PSID=" + PSID);
			sb.append(",Amount=" + String.valueOf(Amount));
			sb.append(",Voucher=" + String.valueOf(Voucher));
			sb.append(",Sequence=" + String.valueOf(Sequence));
			sb.append("]");

			return sb.toString();
		}

		/**
		 * Compare keys
		 */
		public int compareTo(rowDataFromtJavaRow_genStruct other) {

			int returnValue = -1;

			return returnValue;
		}

		private int checkNullsAndCompare(Object object1, Object object2) {
			int returnValue = 0;
			if (object1 instanceof Comparable && object2 instanceof Comparable) {
				returnValue = ((Comparable) object1).compareTo(object2);
			} else if (object1 != null && object2 != null) {
				returnValue = compareStrings(object1.toString(),
						object2.toString());
			} else if (object1 == null && object2 != null) {
				returnValue = 1;
			} else if (object1 != null && object2 == null) {
				returnValue = -1;
			} else {
				returnValue = 0;
			}

			return returnValue;
		}

		private int compareStrings(String string1, String string2) {
			return string1.compareTo(string2);
		}

	}

	public static class rowDataFromtDBInput_4Struct implements
			routines.system.IPersistableRow<rowDataFromtDBInput_4Struct> {
		final static byte[] commonByteArrayLock_DEV_Preview_Data = new byte[0];
		static byte[] commonByteArray_DEV_Preview_Data = new byte[0];

		public String Employee_ID;

		public String getEmployee_ID() {
			return this.Employee_ID;
		}

		public java.util.Date Start_Date;

		public java.util.Date getStart_Date() {
			return this.Start_Date;
		}

		public java.util.Date End_Date;

		public java.util.Date getEnd_Date() {
			return this.End_Date;
		}

		public java.util.Date Process_Date;

		public java.util.Date getProcess_Date() {
			return this.Process_Date;
		}

		public String Approval_Code;

		public String getApproval_Code() {
			return this.Approval_Code;
		}

		public java.util.Date Expense_Date;

		public java.util.Date getExpense_Date() {
			return this.Expense_Date;
		}

		public String GL_Codes;

		public String getGL_Codes() {
			return this.GL_Codes;
		}

		public String Dept_Code;

		public String getDept_Code() {
			return this.Dept_Code;
		}

		public String Trad_Part;

		public String getTrad_Part() {
			return this.Trad_Part;
		}

		public String PSID;

		public String getPSID() {
			return this.PSID;
		}

		public Float Amount;

		public Float getAmount() {
			return this.Amount;
		}

		public Integer Voucher;

		public Integer getVoucher() {
			return this.Voucher;
		}

		public Integer Sequence;

		public Integer getSequence() {
			return this.Sequence;
		}

		private String readString(ObjectInputStream dis) throws IOException {
			String strReturn = null;
			int length = 0;
			length = dis.readInt();
			if (length == -1) {
				strReturn = null;
			} else {
				if (length > commonByteArray_DEV_Preview_Data.length) {
					if (length < 1024
							&& commonByteArray_DEV_Preview_Data.length == 0) {
						commonByteArray_DEV_Preview_Data = new byte[1024];
					} else {
						commonByteArray_DEV_Preview_Data = new byte[2 * length];
					}
				}
				dis.readFully(commonByteArray_DEV_Preview_Data, 0, length);
				strReturn = new String(commonByteArray_DEV_Preview_Data, 0,
						length, utf8Charset);
			}
			return strReturn;
		}

		private void writeString(String str, ObjectOutputStream dos)
				throws IOException {
			if (str == null) {
				dos.writeInt(-1);
			} else {
				byte[] byteArray = str.getBytes(utf8Charset);
				dos.writeInt(byteArray.length);
				dos.write(byteArray);
			}
		}

		private java.util.Date readDate(ObjectInputStream dis)
				throws IOException {
			java.util.Date dateReturn = null;
			int length = 0;
			length = dis.readByte();
			if (length == -1) {
				dateReturn = null;
			} else {
				dateReturn = new Date(dis.readLong());
			}
			return dateReturn;
		}

		private void writeDate(java.util.Date date1, ObjectOutputStream dos)
				throws IOException {
			if (date1 == null) {
				dos.writeByte(-1);
			} else {
				dos.writeByte(0);
				dos.writeLong(date1.getTime());
			}
		}

		private Integer readInteger(ObjectInputStream dis) throws IOException {
			Integer intReturn;
			int length = 0;
			length = dis.readByte();
			if (length == -1) {
				intReturn = null;
			} else {
				intReturn = dis.readInt();
			}
			return intReturn;
		}

		private void writeInteger(Integer intNum, ObjectOutputStream dos)
				throws IOException {
			if (intNum == null) {
				dos.writeByte(-1);
			} else {
				dos.writeByte(0);
				dos.writeInt(intNum);
			}
		}

		public void readData(ObjectInputStream dis) {

			synchronized (commonByteArrayLock_DEV_Preview_Data) {

				try {

					int length = 0;

					this.Employee_ID = readString(dis);

					this.Start_Date = readDate(dis);

					this.End_Date = readDate(dis);

					this.Process_Date = readDate(dis);

					this.Approval_Code = readString(dis);

					this.Expense_Date = readDate(dis);

					this.GL_Codes = readString(dis);

					this.Dept_Code = readString(dis);

					this.Trad_Part = readString(dis);

					this.PSID = readString(dis);

					length = dis.readByte();
					if (length == -1) {
						this.Amount = null;
					} else {
						this.Amount = dis.readFloat();
					}

					this.Voucher = readInteger(dis);

					this.Sequence = readInteger(dis);

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void writeData(ObjectOutputStream dos) {
			try {

				// String

				writeString(this.Employee_ID, dos);

				// java.util.Date

				writeDate(this.Start_Date, dos);

				// java.util.Date

				writeDate(this.End_Date, dos);

				// java.util.Date

				writeDate(this.Process_Date, dos);

				// String

				writeString(this.Approval_Code, dos);

				// java.util.Date

				writeDate(this.Expense_Date, dos);

				// String

				writeString(this.GL_Codes, dos);

				// String

				writeString(this.Dept_Code, dos);

				// String

				writeString(this.Trad_Part, dos);

				// String

				writeString(this.PSID, dos);

				// Float

				if (this.Amount == null) {
					dos.writeByte(-1);
				} else {
					dos.writeByte(0);
					dos.writeFloat(this.Amount);
				}

				// Integer

				writeInteger(this.Voucher, dos);

				// Integer

				writeInteger(this.Sequence, dos);

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public String toString() {

			StringBuilder sb = new StringBuilder();
			sb.append(super.toString());
			sb.append("[");
			sb.append("Employee_ID=" + Employee_ID);
			sb.append(",Start_Date=" + String.valueOf(Start_Date));
			sb.append(",End_Date=" + String.valueOf(End_Date));
			sb.append(",Process_Date=" + String.valueOf(Process_Date));
			sb.append(",Approval_Code=" + Approval_Code);
			sb.append(",Expense_Date=" + String.valueOf(Expense_Date));
			sb.append(",GL_Codes=" + GL_Codes);
			sb.append(",Dept_Code=" + Dept_Code);
			sb.append(",Trad_Part=" + Trad_Part);
			sb.append(",PSID=" + PSID);
			sb.append(",Amount=" + String.valueOf(Amount));
			sb.append(",Voucher=" + String.valueOf(Voucher));
			sb.append(",Sequence=" + String.valueOf(Sequence));
			sb.append("]");

			return sb.toString();
		}

		/**
		 * Compare keys
		 */
		public int compareTo(rowDataFromtDBInput_4Struct other) {

			int returnValue = -1;

			return returnValue;
		}

		private int checkNullsAndCompare(Object object1, Object object2) {
			int returnValue = 0;
			if (object1 instanceof Comparable && object2 instanceof Comparable) {
				returnValue = ((Comparable) object1).compareTo(object2);
			} else if (object1 != null && object2 != null) {
				returnValue = compareStrings(object1.toString(),
						object2.toString());
			} else if (object1 == null && object2 != null) {
				returnValue = 1;
			} else if (object1 != null && object2 == null) {
				returnValue = -1;
			} else {
				returnValue = 0;
			}

			return returnValue;
		}

		private int compareStrings(String string1, String string2) {
			return string1.compareTo(string2);
		}

	}

	public void tDBInput_4Process(final java.util.Map<String, Object> globalMap)
			throws TalendException {
		globalMap.put("tDBInput_4_SUBPROCESS_STATE", 0);

		final boolean execStat = this.execStat;

		String iterateId = "";

		String currentComponent = "";
		java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

		try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception()
						.getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { // start the resume
				globalResumeTicket = true;

				rowDataFromtDBInput_4Struct rowDataFromtDBInput_4 = new rowDataFromtDBInput_4Struct();
				rowDataFromtJavaRow_genStruct rowDataFromtJavaRow_gen = new rowDataFromtJavaRow_genStruct();

				/**
				 * [tSocketOutput_gen begin ] start
				 */

				ok_Hash.put("tSocketOutput_gen", false);
				start_Hash.put("tSocketOutput_gen", System.currentTimeMillis());

				currentComponent = "tSocketOutput_gen";

				int tos_count_tSocketOutput_gen = 0;

				java.net.Socket sockettSocketOutput_gen = null;
				int nb_line_tSocketOutput_gen = 0;
				boolean retrytSocketOutput_gen = true;
				java.net.ConnectException exception_tSocketOutput_gen = null;

				for (int i = 0; i < 10 - 1; i++) {
					if (retrytSocketOutput_gen) {
						try {
							sockettSocketOutput_gen = new java.net.Socket(
									"127.0.0.1", 40934);
							retrytSocketOutput_gen = false;
						} catch (java.net.ConnectException etSocketOutput_gen) {
							exception_tSocketOutput_gen = etSocketOutput_gen;
							Thread.sleep(1000);
						}
					}
				}

				if (retrytSocketOutput_gen
						&& (exception_tSocketOutput_gen != null)) {
					throw exception_tSocketOutput_gen;
				}

				com.talend.csv.CSVWriter CsvWritertSocketOutput_gen = new com.talend.csv.CSVWriter(
						new java.io.BufferedWriter(
								new java.io.OutputStreamWriter(
										sockettSocketOutput_gen
												.getOutputStream(), "UTF-8")));
				CsvWritertSocketOutput_gen.setSeparator(';');

				/**
				 * [tSocketOutput_gen begin ] stop
				 */

				/**
				 * [tJavaRow_gen begin ] start
				 */

				ok_Hash.put("tJavaRow_gen", false);
				start_Hash.put("tJavaRow_gen", System.currentTimeMillis());

				currentComponent = "tJavaRow_gen";

				int tos_count_tJavaRow_gen = 0;

				int nb_line_tJavaRow_gen = 0;

				/**
				 * [tJavaRow_gen begin ] stop
				 */

				/**
				 * [tDBInput_4 begin ] start
				 */

				ok_Hash.put("tDBInput_4", false);
				start_Hash.put("tDBInput_4", System.currentTimeMillis());

				currentComponent = "tDBInput_4";

				int tos_count_tDBInput_4 = 0;

				org.talend.designer.components.util.mssql.MSSqlGenerateTimestampUtil mssqlGTU_tDBInput_4 = org.talend.designer.components.util.mssql.MSSqlUtilFactory
						.getMSSqlGenerateTimestampUtil();

				java.util.List<String> talendToDBList_tDBInput_4 = new java.util.ArrayList();
				String[] talendToDBArray_tDBInput_4 = new String[] { "FLOAT",
						"NUMERIC", "NUMERIC IDENTITY", "DECIMAL",
						"DECIMAL IDENTITY", "REAL" };
				java.util.Collections.addAll(talendToDBList_tDBInput_4,
						talendToDBArray_tDBInput_4);
				int nb_line_tDBInput_4 = 0;
				java.sql.Connection conn_tDBInput_4 = null;
				String driverClass_tDBInput_4 = "net.sourceforge.jtds.jdbc.Driver";
				java.lang.Class.forName(driverClass_tDBInput_4);
				String dbUser_tDBInput_4 = "sa";

				final String decryptedPassword_tDBInput_4 = routines.system.PasswordEncryptUtil
						.decryptPassword("bf98e2c82ba433c4880df380fbb39fe9");

				String dbPwd_tDBInput_4 = decryptedPassword_tDBInput_4;

				String port_tDBInput_4 = "1433";
				String dbname_tDBInput_4 = "Talend_Certify";
				String url_tDBInput_4 = "jdbc:jtds:sqlserver://"
						+ "NSMWQNXINFDB02";
				if (!"".equals(port_tDBInput_4)) {
					url_tDBInput_4 += ":" + "1433";
				}
				if (!"".equals(dbname_tDBInput_4)) {
					url_tDBInput_4 += "//" + "Talend_Certify";
				}
				url_tDBInput_4 += ";appName=" + projectName + ";" + "";
				String dbschema_tDBInput_4 = "";

				conn_tDBInput_4 = java.sql.DriverManager.getConnection(
						url_tDBInput_4, dbUser_tDBInput_4, dbPwd_tDBInput_4);

				java.sql.Statement stmt_tDBInput_4 = conn_tDBInput_4
						.createStatement();

				String dbquery_tDBInput_4 = "SELECT  QBP_HO_APP_DistributionList.Employee_ID\n		,QBP_HO_APP_DistributionList.Start_Date\n		,QBP_HO_APP_DistributionL"
						+ "ist.End_Date\n		,QBP_HO_APP_DistributionList.Process_Date\n		,QBP_HO_APP_DistributionList.Approval_Code\n		,QBP_HO_APP_D"
						+ "istributionList.Expense_Date\n		,QBP_HO_APP_DistributionList.GL_Codes\n		,QBP_HO_APP_DistributionList.Dept_Code\n		,QBP_"
						+ "HO_APP_DistributionList.Trad_Part\n		,QBP_HO_APP_DistributionList.PSID\n		,QBP_HO_APP_DistributionList.Amount\n		,QBP_HO"
						+ "_APP_DistributionList.Voucher\n	    ,QBP_HO_APP_DistributionList.Sequence\nFROM	QBP_HO_APP_DistributionList";

				globalMap.put("tDBInput_4_QUERY", dbquery_tDBInput_4);
				java.sql.ResultSet rs_tDBInput_4 = null;

				try {
					rs_tDBInput_4 = stmt_tDBInput_4
							.executeQuery(dbquery_tDBInput_4);
					java.sql.ResultSetMetaData rsmd_tDBInput_4 = rs_tDBInput_4
							.getMetaData();
					int colQtyInRs_tDBInput_4 = rsmd_tDBInput_4
							.getColumnCount();

					String tmpContent_tDBInput_4 = null;

					while (rs_tDBInput_4.next()) {
						nb_line_tDBInput_4++;

						if (colQtyInRs_tDBInput_4 < 1) {
							rowDataFromtDBInput_4.Employee_ID = null;
						} else {

							tmpContent_tDBInput_4 = rs_tDBInput_4.getString(1);
							if (tmpContent_tDBInput_4 != null) {
								if (talendToDBList_tDBInput_4
										.contains(rsmd_tDBInput_4
												.getColumnTypeName(1)
												.toUpperCase(
														java.util.Locale.ENGLISH))) {
									rowDataFromtDBInput_4.Employee_ID = FormatterUtils
											.formatUnwithE(tmpContent_tDBInput_4);
								} else {
									rowDataFromtDBInput_4.Employee_ID = tmpContent_tDBInput_4;
								}
							} else {
								rowDataFromtDBInput_4.Employee_ID = null;
							}
						}
						if (colQtyInRs_tDBInput_4 < 2) {
							rowDataFromtDBInput_4.Start_Date = null;
						} else {

							rowDataFromtDBInput_4.Start_Date = mssqlGTU_tDBInput_4
									.getDate(rsmd_tDBInput_4, rs_tDBInput_4, 2);

						}
						if (colQtyInRs_tDBInput_4 < 3) {
							rowDataFromtDBInput_4.End_Date = null;
						} else {

							rowDataFromtDBInput_4.End_Date = mssqlGTU_tDBInput_4
									.getDate(rsmd_tDBInput_4, rs_tDBInput_4, 3);

						}
						if (colQtyInRs_tDBInput_4 < 4) {
							rowDataFromtDBInput_4.Process_Date = null;
						} else {

							rowDataFromtDBInput_4.Process_Date = mssqlGTU_tDBInput_4
									.getDate(rsmd_tDBInput_4, rs_tDBInput_4, 4);

						}
						if (colQtyInRs_tDBInput_4 < 5) {
							rowDataFromtDBInput_4.Approval_Code = null;
						} else {

							tmpContent_tDBInput_4 = rs_tDBInput_4.getString(5);
							if (tmpContent_tDBInput_4 != null) {
								if (talendToDBList_tDBInput_4
										.contains(rsmd_tDBInput_4
												.getColumnTypeName(5)
												.toUpperCase(
														java.util.Locale.ENGLISH))) {
									rowDataFromtDBInput_4.Approval_Code = FormatterUtils
											.formatUnwithE(tmpContent_tDBInput_4);
								} else {
									rowDataFromtDBInput_4.Approval_Code = tmpContent_tDBInput_4;
								}
							} else {
								rowDataFromtDBInput_4.Approval_Code = null;
							}
						}
						if (colQtyInRs_tDBInput_4 < 6) {
							rowDataFromtDBInput_4.Expense_Date = null;
						} else {

							rowDataFromtDBInput_4.Expense_Date = mssqlGTU_tDBInput_4
									.getDate(rsmd_tDBInput_4, rs_tDBInput_4, 6);

						}
						if (colQtyInRs_tDBInput_4 < 7) {
							rowDataFromtDBInput_4.GL_Codes = null;
						} else {

							tmpContent_tDBInput_4 = rs_tDBInput_4.getString(7);
							if (tmpContent_tDBInput_4 != null) {
								if (talendToDBList_tDBInput_4
										.contains(rsmd_tDBInput_4
												.getColumnTypeName(7)
												.toUpperCase(
														java.util.Locale.ENGLISH))) {
									rowDataFromtDBInput_4.GL_Codes = FormatterUtils
											.formatUnwithE(tmpContent_tDBInput_4);
								} else {
									rowDataFromtDBInput_4.GL_Codes = tmpContent_tDBInput_4;
								}
							} else {
								rowDataFromtDBInput_4.GL_Codes = null;
							}
						}
						if (colQtyInRs_tDBInput_4 < 8) {
							rowDataFromtDBInput_4.Dept_Code = null;
						} else {

							tmpContent_tDBInput_4 = rs_tDBInput_4.getString(8);
							if (tmpContent_tDBInput_4 != null) {
								if (talendToDBList_tDBInput_4
										.contains(rsmd_tDBInput_4
												.getColumnTypeName(8)
												.toUpperCase(
														java.util.Locale.ENGLISH))) {
									rowDataFromtDBInput_4.Dept_Code = FormatterUtils
											.formatUnwithE(tmpContent_tDBInput_4);
								} else {
									rowDataFromtDBInput_4.Dept_Code = tmpContent_tDBInput_4;
								}
							} else {
								rowDataFromtDBInput_4.Dept_Code = null;
							}
						}
						if (colQtyInRs_tDBInput_4 < 9) {
							rowDataFromtDBInput_4.Trad_Part = null;
						} else {

							tmpContent_tDBInput_4 = rs_tDBInput_4.getString(9);
							if (tmpContent_tDBInput_4 != null) {
								if (talendToDBList_tDBInput_4
										.contains(rsmd_tDBInput_4
												.getColumnTypeName(9)
												.toUpperCase(
														java.util.Locale.ENGLISH))) {
									rowDataFromtDBInput_4.Trad_Part = FormatterUtils
											.formatUnwithE(tmpContent_tDBInput_4);
								} else {
									rowDataFromtDBInput_4.Trad_Part = tmpContent_tDBInput_4;
								}
							} else {
								rowDataFromtDBInput_4.Trad_Part = null;
							}
						}
						if (colQtyInRs_tDBInput_4 < 10) {
							rowDataFromtDBInput_4.PSID = null;
						} else {

							tmpContent_tDBInput_4 = rs_tDBInput_4.getString(10);
							if (tmpContent_tDBInput_4 != null) {
								if (talendToDBList_tDBInput_4
										.contains(rsmd_tDBInput_4
												.getColumnTypeName(10)
												.toUpperCase(
														java.util.Locale.ENGLISH))) {
									rowDataFromtDBInput_4.PSID = FormatterUtils
											.formatUnwithE(tmpContent_tDBInput_4);
								} else {
									rowDataFromtDBInput_4.PSID = tmpContent_tDBInput_4;
								}
							} else {
								rowDataFromtDBInput_4.PSID = null;
							}
						}
						if (colQtyInRs_tDBInput_4 < 11) {
							rowDataFromtDBInput_4.Amount = null;
						} else {

							if (rs_tDBInput_4.getObject(11) != null) {
								rowDataFromtDBInput_4.Amount = rs_tDBInput_4
										.getFloat(11);
							} else {
								rowDataFromtDBInput_4.Amount = null;
							}
						}
						if (colQtyInRs_tDBInput_4 < 12) {
							rowDataFromtDBInput_4.Voucher = null;
						} else {

							if (rs_tDBInput_4.getObject(12) != null) {
								rowDataFromtDBInput_4.Voucher = rs_tDBInput_4
										.getInt(12);
							} else {
								rowDataFromtDBInput_4.Voucher = null;
							}
						}
						if (colQtyInRs_tDBInput_4 < 13) {
							rowDataFromtDBInput_4.Sequence = null;
						} else {

							if (rs_tDBInput_4.getObject(13) != null) {
								rowDataFromtDBInput_4.Sequence = rs_tDBInput_4
										.getInt(13);
							} else {
								rowDataFromtDBInput_4.Sequence = null;
							}
						}

						/**
						 * [tDBInput_4 begin ] stop
						 */

						/**
						 * [tDBInput_4 main ] start
						 */

						currentComponent = "tDBInput_4";

						tos_count_tDBInput_4++;

						/**
						 * [tDBInput_4 main ] stop
						 */

						/**
						 * [tDBInput_4 process_data_begin ] start
						 */

						currentComponent = "tDBInput_4";

						/**
						 * [tDBInput_4 process_data_begin ] stop
						 */

						/**
						 * [tJavaRow_gen main ] start
						 */

						currentComponent = "tJavaRow_gen";

						rowDataFromtJavaRow_gen.Employee_ID = rowDataFromtDBInput_4.Employee_ID;
						rowDataFromtJavaRow_gen.Start_Date = rowDataFromtDBInput_4.Start_Date;
						rowDataFromtJavaRow_gen.End_Date = rowDataFromtDBInput_4.End_Date;
						rowDataFromtJavaRow_gen.Process_Date = rowDataFromtDBInput_4.Process_Date;
						rowDataFromtJavaRow_gen.Approval_Code = rowDataFromtDBInput_4.Approval_Code;
						rowDataFromtJavaRow_gen.Expense_Date = rowDataFromtDBInput_4.Expense_Date;
						rowDataFromtJavaRow_gen.GL_Codes = rowDataFromtDBInput_4.GL_Codes;
						rowDataFromtJavaRow_gen.Dept_Code = rowDataFromtDBInput_4.Dept_Code;
						rowDataFromtJavaRow_gen.Trad_Part = rowDataFromtDBInput_4.Trad_Part;
						rowDataFromtJavaRow_gen.PSID = rowDataFromtDBInput_4.PSID;
						rowDataFromtJavaRow_gen.Amount = rowDataFromtDBInput_4.Amount;
						rowDataFromtJavaRow_gen.Voucher = rowDataFromtDBInput_4.Voucher;
						rowDataFromtJavaRow_gen.Sequence = rowDataFromtDBInput_4.Sequence;

						if (nb_line_tJavaRow_gen >= 1000) {
							break;
						}

						nb_line_tJavaRow_gen++;

						tos_count_tJavaRow_gen++;

						/**
						 * [tJavaRow_gen main ] stop
						 */

						/**
						 * [tJavaRow_gen process_data_begin ] start
						 */

						currentComponent = "tJavaRow_gen";

						/**
						 * [tJavaRow_gen process_data_begin ] stop
						 */

						/**
						 * [tSocketOutput_gen main ] start
						 */

						currentComponent = "tSocketOutput_gen";

						CsvWritertSocketOutput_gen.setEscapeChar('\\');

						CsvWritertSocketOutput_gen.setQuoteChar('"');
						CsvWritertSocketOutput_gen
								.setQuoteStatus(com.talend.csv.CSVWriter.QuoteStatus.FORCE);
						String[] rowtSocketOutput_gen = new String[13];
						if (rowDataFromtJavaRow_gen.Employee_ID == null) {
							rowtSocketOutput_gen[0] = "";
						} else {
							rowtSocketOutput_gen[0] = rowDataFromtJavaRow_gen.Employee_ID;
						}
						if (rowDataFromtJavaRow_gen.Start_Date == null) {
							rowtSocketOutput_gen[1] = "";
						} else {
							rowtSocketOutput_gen[1] = FormatterUtils
									.format_Date(
											rowDataFromtJavaRow_gen.Start_Date,
											"dd-MM-yyyy");
						}
						if (rowDataFromtJavaRow_gen.End_Date == null) {
							rowtSocketOutput_gen[2] = "";
						} else {
							rowtSocketOutput_gen[2] = FormatterUtils
									.format_Date(
											rowDataFromtJavaRow_gen.End_Date,
											"dd-MM-yyyy");
						}
						if (rowDataFromtJavaRow_gen.Process_Date == null) {
							rowtSocketOutput_gen[3] = "";
						} else {
							rowtSocketOutput_gen[3] = FormatterUtils
									.format_Date(
											rowDataFromtJavaRow_gen.Process_Date,
											"dd-MM-yyyy");
						}
						if (rowDataFromtJavaRow_gen.Approval_Code == null) {
							rowtSocketOutput_gen[4] = "";
						} else {
							rowtSocketOutput_gen[4] = rowDataFromtJavaRow_gen.Approval_Code;
						}
						if (rowDataFromtJavaRow_gen.Expense_Date == null) {
							rowtSocketOutput_gen[5] = "";
						} else {
							rowtSocketOutput_gen[5] = FormatterUtils
									.format_Date(
											rowDataFromtJavaRow_gen.Expense_Date,
											"dd-MM-yyyy");
						}
						if (rowDataFromtJavaRow_gen.GL_Codes == null) {
							rowtSocketOutput_gen[6] = "";
						} else {
							rowtSocketOutput_gen[6] = rowDataFromtJavaRow_gen.GL_Codes;
						}
						if (rowDataFromtJavaRow_gen.Dept_Code == null) {
							rowtSocketOutput_gen[7] = "";
						} else {
							rowtSocketOutput_gen[7] = rowDataFromtJavaRow_gen.Dept_Code;
						}
						if (rowDataFromtJavaRow_gen.Trad_Part == null) {
							rowtSocketOutput_gen[8] = "";
						} else {
							rowtSocketOutput_gen[8] = rowDataFromtJavaRow_gen.Trad_Part;
						}
						if (rowDataFromtJavaRow_gen.PSID == null) {
							rowtSocketOutput_gen[9] = "";
						} else {
							rowtSocketOutput_gen[9] = rowDataFromtJavaRow_gen.PSID;
						}
						if (rowDataFromtJavaRow_gen.Amount == null) {
							rowtSocketOutput_gen[10] = "";
						} else {
							rowtSocketOutput_gen[10] = String
									.valueOf(rowDataFromtJavaRow_gen.Amount);
						}
						if (rowDataFromtJavaRow_gen.Voucher == null) {
							rowtSocketOutput_gen[11] = "";
						} else {
							rowtSocketOutput_gen[11] = String
									.valueOf(rowDataFromtJavaRow_gen.Voucher);
						}
						if (rowDataFromtJavaRow_gen.Sequence == null) {
							rowtSocketOutput_gen[12] = "";
						} else {
							rowtSocketOutput_gen[12] = String
									.valueOf(rowDataFromtJavaRow_gen.Sequence);
						}
						CsvWritertSocketOutput_gen
								.writeNext(rowtSocketOutput_gen);
						CsvWritertSocketOutput_gen.flush();

						nb_line_tSocketOutput_gen++;

						tos_count_tSocketOutput_gen++;

						/**
						 * [tSocketOutput_gen main ] stop
						 */

						/**
						 * [tSocketOutput_gen process_data_begin ] start
						 */

						currentComponent = "tSocketOutput_gen";

						/**
						 * [tSocketOutput_gen process_data_begin ] stop
						 */

						/**
						 * [tSocketOutput_gen process_data_end ] start
						 */

						currentComponent = "tSocketOutput_gen";

						/**
						 * [tSocketOutput_gen process_data_end ] stop
						 */

						/**
						 * [tJavaRow_gen process_data_end ] start
						 */

						currentComponent = "tJavaRow_gen";

						/**
						 * [tJavaRow_gen process_data_end ] stop
						 */

						/**
						 * [tDBInput_4 process_data_end ] start
						 */

						currentComponent = "tDBInput_4";

						/**
						 * [tDBInput_4 process_data_end ] stop
						 */

						/**
						 * [tDBInput_4 end ] start
						 */

						currentComponent = "tDBInput_4";

					}
				} finally {
					if (rs_tDBInput_4 != null) {
						rs_tDBInput_4.close();
					}
					if (stmt_tDBInput_4 != null) {
						stmt_tDBInput_4.close();
					}
					if (conn_tDBInput_4 != null && !conn_tDBInput_4.isClosed()) {

						conn_tDBInput_4.close();

					}
				}
				globalMap.put("tDBInput_4_NB_LINE", nb_line_tDBInput_4);

				ok_Hash.put("tDBInput_4", true);
				end_Hash.put("tDBInput_4", System.currentTimeMillis());

				/**
				 * [tDBInput_4 end ] stop
				 */

				/**
				 * [tJavaRow_gen end ] start
				 */

				currentComponent = "tJavaRow_gen";

				globalMap.put("tJavaRow_gen_NB_LINE", nb_line_tJavaRow_gen);

				ok_Hash.put("tJavaRow_gen", true);
				end_Hash.put("tJavaRow_gen", System.currentTimeMillis());

				/**
				 * [tJavaRow_gen end ] stop
				 */

				/**
				 * [tSocketOutput_gen end ] start
				 */

				currentComponent = "tSocketOutput_gen";

				if (sockettSocketOutput_gen != null) {
					sockettSocketOutput_gen.close();
				}

				if (CsvWritertSocketOutput_gen != null) {
					CsvWritertSocketOutput_gen.close();
				}

				globalMap.put("tSocketOutput_gen_NB_LINE",
						nb_line_tSocketOutput_gen);

				ok_Hash.put("tSocketOutput_gen", true);
				end_Hash.put("tSocketOutput_gen", System.currentTimeMillis());

				/**
				 * [tSocketOutput_gen end ] stop
				 */

			}// end the resume

		} catch (java.lang.Exception e) {

			TalendException te = new TalendException(e, currentComponent,
					globalMap);

			throw te;
		} catch (java.lang.Error error) {

			throw error;
		} finally {

			try {

				/**
				 * [tDBInput_4 finally ] start
				 */

				currentComponent = "tDBInput_4";

				/**
				 * [tDBInput_4 finally ] stop
				 */

				/**
				 * [tJavaRow_gen finally ] start
				 */

				currentComponent = "tJavaRow_gen";

				/**
				 * [tJavaRow_gen finally ] stop
				 */

				/**
				 * [tSocketOutput_gen finally ] start
				 */

				currentComponent = "tSocketOutput_gen";

				/**
				 * [tSocketOutput_gen finally ] stop
				 */

			} catch (java.lang.Exception e) {
				// ignore
			} catch (java.lang.Error error) {
				// ignore
			}
			resourceMap = null;
		}

		globalMap.put("tDBInput_4_SUBPROCESS_STATE", 1);
	}

	public String resuming_logs_dir_path = null;
	public String resuming_checkpoint_path = null;
	public String parent_part_launcher = null;
	private String resumeEntryMethodName = null;
	private boolean globalResumeTicket = false;

	public boolean watch = false;
	// portStats is null, it means don't execute the statistics
	public Integer portStats = null;
	public int portTraces = 4334;
	public String clientHost;
	public String defaultClientHost = "localhost";
	public String contextStr = "Default";
	public boolean isDefaultContext = true;
	public String pid = "0";
	public String rootPid = null;
	public String fatherPid = null;
	public String fatherNode = null;
	public long startTime = 0;
	public boolean isChildJob = false;
	public String log4jLevel = "";

	private boolean execStat = true;

	private ThreadLocal<java.util.Map<String, String>> threadLocal = new ThreadLocal<java.util.Map<String, String>>() {
		protected java.util.Map<String, String> initialValue() {
			java.util.Map<String, String> threadRunResultMap = new java.util.HashMap<String, String>();
			threadRunResultMap.put("errorCode", null);
			threadRunResultMap.put("status", "");
			return threadRunResultMap;
		};
	};

	private PropertiesWithType context_param = new PropertiesWithType();
	public java.util.Map<String, Object> parentContextMap = new java.util.HashMap<String, Object>();

	public String status = "";

	public static void main(String[] args) {
		final Preview_Data Preview_DataClass = new Preview_Data();

		int exitCode = Preview_DataClass.runJobInTOS(args);

		System.exit(exitCode);
	}

	public String[][] runJob(String[] args) {

		int exitCode = runJobInTOS(args);
		String[][] bufferValue = new String[][] { { Integer.toString(exitCode) } };

		return bufferValue;
	}

	public boolean hastBufferOutputComponent() {
		boolean hastBufferOutput = false;

		return hastBufferOutput;
	}

	public int runJobInTOS(String[] args) {
		// reset status
		status = "";

		String lastStr = "";
		for (String arg : args) {
			if (arg.equalsIgnoreCase("--context_param")) {
				lastStr = arg;
			} else if (lastStr.equals("")) {
				evalParam(arg);
			} else {
				evalParam(lastStr + " " + arg);
				lastStr = "";
			}
		}

		if (clientHost == null) {
			clientHost = defaultClientHost;
		}

		if (pid == null || "0".equals(pid)) {
			pid = TalendString.getAsciiRandomString(6);
		}

		if (rootPid == null) {
			rootPid = pid;
		}
		if (fatherPid == null) {
			fatherPid = pid;
		} else {
			isChildJob = true;
		}

		try {
			// call job/subjob with an existing context, like:
			// --context=production. if without this parameter, there will use
			// the default context instead.
			java.io.InputStream inContext = Preview_Data.class.getClassLoader()
					.getResourceAsStream(
							"dev/preview_data/contexts/" + contextStr
									+ ".properties");
			if (inContext == null) {
				inContext = Preview_Data.class
						.getClassLoader()
						.getResourceAsStream(
								"config/contexts/" + contextStr + ".properties");
			}
			if (inContext != null && context != null && context.isEmpty()) {
				// defaultProps is in order to keep the original context value
				defaultProps.load(inContext);
				inContext.close();
				context = new ContextProperties(defaultProps);
			} else if (!isDefaultContext) {
				// print info and job continue to run, for case: context_param
				// is not empty.
				System.err.println("Could not find the context " + contextStr);
			}

			if (!context_param.isEmpty()) {
				context.putAll(context_param);
				// set types for params from parentJobs
				for (Object key : context_param.keySet()) {
					String context_key = key.toString();
					String context_type = context_param
							.getContextType(context_key);
					context.setContextType(context_key, context_type);

				}
			}
		} catch (java.io.IOException ie) {
			System.err.println("Could not load context " + contextStr);
			ie.printStackTrace();
		}

		// get context value from parent directly
		if (parentContextMap != null && !parentContextMap.isEmpty()) {
		}

		// Resume: init the resumeUtil
		resumeEntryMethodName = ResumeUtil
				.getResumeEntryMethodName(resuming_checkpoint_path);
		resumeUtil = new ResumeUtil(resuming_logs_dir_path, isChildJob, rootPid);
		resumeUtil.initCommonInfo(pid, rootPid, fatherPid, projectName,
				jobName, contextStr, jobVersion);

		List<String> parametersToEncrypt = new java.util.ArrayList<String>();
		// Resume: jobStart
		resumeUtil.addLog("JOB_STARTED", "JOB:" + jobName,
				parent_part_launcher, Thread.currentThread().getId() + "", "",
				"", "", "",
				resumeUtil.convertToJsonText(context, parametersToEncrypt));

		java.util.concurrent.ConcurrentHashMap<Object, Object> concurrentHashMap = new java.util.concurrent.ConcurrentHashMap<Object, Object>();
		globalMap.put("concurrentHashMap", concurrentHashMap);

		long startUsedMemory = Runtime.getRuntime().totalMemory()
				- Runtime.getRuntime().freeMemory();
		long endUsedMemory = 0;
		long end = 0;

		startTime = System.currentTimeMillis();

		this.globalResumeTicket = true;// to run tPreJob

		this.globalResumeTicket = false;// to run others jobs

		try {
			errorCode = null;
			tDBInput_4Process(globalMap);
			if (!"failure".equals(status)) {
				status = "end";
			}
		} catch (TalendException e_tDBInput_4) {
			globalMap.put("tDBInput_4_SUBPROCESS_STATE", -1);

			e_tDBInput_4.printStackTrace();

		}

		this.globalResumeTicket = true;// to run tPostJob

		end = System.currentTimeMillis();

		if (watch) {
			System.out.println((end - startTime) + " milliseconds");
		}

		endUsedMemory = Runtime.getRuntime().totalMemory()
				- Runtime.getRuntime().freeMemory();
		if (false) {
			System.out.println((endUsedMemory - startUsedMemory)
					+ " bytes memory increase when running : Preview_Data");
		}

		int returnCode = 0;
		if (errorCode == null) {
			returnCode = status != null && status.equals("failure") ? 1 : 0;
		} else {
			returnCode = errorCode.intValue();
		}
		resumeUtil.addLog("JOB_ENDED", "JOB:" + jobName, parent_part_launcher,
				Thread.currentThread().getId() + "", "", "" + returnCode, "",
				"", "");

		return returnCode;

	}

	// only for OSGi env
	public void destroy() {

	}

	private java.util.Map<String, Object> getSharedConnections4REST() {
		java.util.Map<String, Object> connections = new java.util.HashMap<String, Object>();

		return connections;
	}

	private void evalParam(String arg) {
		if (arg.startsWith("--resuming_logs_dir_path")) {
			resuming_logs_dir_path = arg.substring(25);
		} else if (arg.startsWith("--resuming_checkpoint_path")) {
			resuming_checkpoint_path = arg.substring(27);
		} else if (arg.startsWith("--parent_part_launcher")) {
			parent_part_launcher = arg.substring(23);
		} else if (arg.startsWith("--watch")) {
			watch = true;
		} else if (arg.startsWith("--stat_port=")) {
			String portStatsStr = arg.substring(12);
			if (portStatsStr != null && !portStatsStr.equals("null")) {
				portStats = Integer.parseInt(portStatsStr);
			}
		} else if (arg.startsWith("--trace_port=")) {
			portTraces = Integer.parseInt(arg.substring(13));
		} else if (arg.startsWith("--client_host=")) {
			clientHost = arg.substring(14);
		} else if (arg.startsWith("--context=")) {
			contextStr = arg.substring(10);
			isDefaultContext = false;
		} else if (arg.startsWith("--father_pid=")) {
			fatherPid = arg.substring(13);
		} else if (arg.startsWith("--root_pid=")) {
			rootPid = arg.substring(11);
		} else if (arg.startsWith("--father_node=")) {
			fatherNode = arg.substring(14);
		} else if (arg.startsWith("--pid=")) {
			pid = arg.substring(6);
		} else if (arg.startsWith("--context_type")) {
			String keyValue = arg.substring(15);
			int index = -1;
			if (keyValue != null && (index = keyValue.indexOf('=')) > -1) {
				if (fatherPid == null) {
					context_param.setContextType(keyValue.substring(0, index),
							replaceEscapeChars(keyValue.substring(index + 1)));
				} else { // the subjob won't escape the especial chars
					context_param.setContextType(keyValue.substring(0, index),
							keyValue.substring(index + 1));
				}

			}

		} else if (arg.startsWith("--context_param")) {
			String keyValue = arg.substring(16);
			int index = -1;
			if (keyValue != null && (index = keyValue.indexOf('=')) > -1) {
				if (fatherPid == null) {
					context_param.put(keyValue.substring(0, index),
							replaceEscapeChars(keyValue.substring(index + 1)));
				} else { // the subjob won't escape the especial chars
					context_param.put(keyValue.substring(0, index),
							keyValue.substring(index + 1));
				}
			}
		} else if (arg.startsWith("--log4jLevel=")) {
			log4jLevel = arg.substring(13);
		}

	}

	private static final String NULL_VALUE_EXPRESSION_IN_COMMAND_STRING_FOR_CHILD_JOB_ONLY = "<TALEND_NULL>";

	private final String[][] escapeChars = { { "\\\\", "\\" }, { "\\n", "\n" },
			{ "\\'", "\'" }, { "\\r", "\r" }, { "\\f", "\f" }, { "\\b", "\b" },
			{ "\\t", "\t" } };

	private String replaceEscapeChars(String keyValue) {

		if (keyValue == null || ("").equals(keyValue.trim())) {
			return keyValue;
		}

		StringBuilder result = new StringBuilder();
		int currIndex = 0;
		while (currIndex < keyValue.length()) {
			int index = -1;
			// judege if the left string includes escape chars
			for (String[] strArray : escapeChars) {
				index = keyValue.indexOf(strArray[0], currIndex);
				if (index >= 0) {

					result.append(keyValue.substring(currIndex,
							index + strArray[0].length()).replace(strArray[0],
							strArray[1]));
					currIndex = index + strArray[0].length();
					break;
				}
			}
			// if the left string doesn't include escape chars, append the left
			// into the result
			if (index < 0) {
				result.append(keyValue.substring(currIndex));
				currIndex = currIndex + keyValue.length();
			}
		}

		return result.toString();
	}

	public Integer getErrorCode() {
		return errorCode;
	}

	public String getStatus() {
		return status;
	}

	ResumeUtil resumeUtil = null;
}
/************************************************************************************************
 * 58306 characters generated by Talend Cloud Data Management Platform on the
 * February 19, 2019 2:44:31 PM CST
 ************************************************************************************************/
