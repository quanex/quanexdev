package org.talend.designer.codegen.translators.elt.map.sap;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.talend.core.model.metadata.IMetadataColumn;
import org.talend.core.model.metadata.IMetadataTable;
import org.talend.core.model.metadata.types.JavaType;
import org.talend.core.model.metadata.types.JavaTypesManager;
import org.talend.core.model.process.ElementParameterParser;
import org.talend.core.model.process.IConnection;
import org.talend.core.model.process.IConnectionCategory;
import org.talend.core.model.process.INode;
import org.talend.designer.codegen.config.CodeGeneratorArgument;
import org.talend.core.model.utils.TalendTextUtils;
import org.talend.designer.sapelt.SAPMapComponent;
import org.talend.designer.sapelt.model.emf.sapelt.SapEltData;
import org.talend.designer.sapelt.model.emf.sapelt.InputTable;
import org.talend.designer.sapelt.model.emf.sapelt.OutputTable;
import org.talend.designer.sapelt.language.ScriptGenerationManager;
import org.talend.designer.sapelt.model.emf.sapelt.TableEntry;
import org.talend.designer.sapelt.expressionutils.DataMapExpressionParser;
import org.talend.designer.sapelt.language.SapLanguage;
import org.talend.designer.sapelt.model.tableentry.TableEntryLocation;

public class TELTSAPMapFinallyJava
{
  protected static String nl;
  public static synchronized TELTSAPMapFinallyJava create(String lineSeparator)
  {
    nl = lineSeparator;
    TELTSAPMapFinallyJava result = new TELTSAPMapFinallyJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "";
  protected final String TEXT_2 = NL + NL + "//wait for the producer thread over as it need to release the generated files when exception which may come from other components appear" + NL + "Object data_";
  protected final String TEXT_3 = " = resourceMap.get(\"data_";
  protected final String TEXT_4 = "\");" + NL + "if(data_";
  protected final String TEXT_5 = "!=null) {" + NL + "\t((org.talend.sap.model.table.ISAPBatchData)data_";
  protected final String TEXT_6 = ").waitForResourceRelease();" + NL + "}";

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    stringBuffer.append(TEXT_1);
    
	CodeGeneratorArgument codeGenArgument = (CodeGeneratorArgument) argument;
	
	SAPMapComponent node = (SAPMapComponent) codeGenArgument.getArgument();
	SapEltData sapdata = node.getExternalEmfData();
	List<InputTable> inputTables = sapdata.getInputTables();
	List<OutputTable> outputTables = sapdata.getOutputTables();
	
	ScriptGenerationManager gm = node.getGenerationManager();
	
	String cid = node.getUniqueName();
	
	List<? extends IConnection> outputConnections = node.getOutgoingSortedConnections();
	if((outputConnections == null) || (outputConnections.size() == 0)) {
		return "";
	}
	IConnection outputConnection = outputConnections.get(0);
	
	if(!outputConnection.getLineStyle().hasConnectionCategory(IConnectionCategory.DATA)) {
		return "";
	}
	
	OutputTable outputTable = null;
	
	if(outputTables==null || outputTables.isEmpty()) {
		return "";
	}
	
	for(OutputTable ot : outputTables) {
		if(ot.getName().equals(outputConnection.getUniqueName())) {
			outputTable = ot;
		}
	}
	
	if(outputTable == null) {
		return "";
	}
	
	List<TableEntry> tableEntries = outputTable.getTableEntries();
	
	if(tableEntries == null || tableEntries.isEmpty()) {
		return "";
	}
	
	Map<String, TableEntry> entryMap = new HashMap<String, TableEntry>();
	for(TableEntry entry : tableEntries) {
		entryMap.put(entry.getName(), entry);
	}
	
	IMetadataTable outputMetadata = outputConnection.getMetadataTable();
	if(outputMetadata == null) {
		return "";
	}
	
	List<IMetadataColumn> columnList = outputMetadata.getListColumns();
	
	if(columnList == null || columnList.isEmpty()) {
		return "";
	}

    stringBuffer.append(TEXT_2);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_3);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_4);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_5);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_6);
    return stringBuffer.toString();
  }
}
