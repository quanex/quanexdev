package org.talend.designer.codegen.translators.databases.hive;

import java.util.List;
import org.talend.core.model.metadata.IMetadataColumn;
import org.talend.core.model.metadata.IMetadataTable;
import org.talend.core.model.metadata.types.JavaType;
import org.talend.core.model.metadata.types.JavaTypesManager;
import org.talend.core.model.process.ElementParameterParser;
import org.talend.core.model.process.IConnection;
import org.talend.core.model.process.INode;
import org.talend.designer.common.BigDataCodeGeneratorArgument;
import org.talend.designer.common.tsqlrow.TSqlRowUtil;
import java.util.Map;
import java.util.List;
import java.util.StringJoiner;

public class THiveOutputSparkconfigJava
{
  protected static String nl;
  public static synchronized THiveOutputSparkconfigJava create(String lineSeparator)
  {
    nl = lineSeparator;
    THiveOutputSparkconfigJava result = new THiveOutputSparkconfigJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "";
  protected final String TEXT_2 = NL + "    System.setProperty(\"hive.metastore.uris\", \"thrift://\" + ";
  protected final String TEXT_3 = " + \":\" + ";
  protected final String TEXT_4 = " + \"/\");";
  protected final String TEXT_5 = NL + "    System.setProperty(\"hive.metastore.uris\", ";
  protected final String TEXT_6 = ");";
  protected final String TEXT_7 = NL;
  protected final String TEXT_8 = NL + "            System.setProperty(\"hive.metastore.sasl.enabled\", \"true\");" + NL + "            System.setProperty(\"hive.security.authorization.enabled\", \"false\");" + NL + "            System.setProperty(\"hive.metastore.kerberos.principal\", ";
  protected final String TEXT_9 = ");" + NL + "            System.setProperty(\"hive.metastore.execute.setugi\", \"true\");";
  protected final String TEXT_10 = NL + "            System.setProperty(\"hive.metastore.sasl.enabled\", \"true\");" + NL + "            System.setProperty(\"hive.metastore.execute.setugi\", \"true\");";
  protected final String TEXT_11 = NL + "            System.setProperty(\"fs.defaultFS\", ";
  protected final String TEXT_12 = ");";
  protected final String TEXT_13 = NL + NL + "org.apache.spark.sql.hive.HiveContext hiveContext_";
  protected final String TEXT_14 = " = new org.apache.spark.sql.hive.HiveContext(ctx.sc());" + NL;
  protected final String TEXT_15 = NL + "                org.apache.spark.api.java.JavaRDD<";
  protected final String TEXT_16 = "> ";
  protected final String TEXT_17 = " = ";
  protected final String TEXT_18 = ".map(new ";
  protected final String TEXT_19 = "_From";
  protected final String TEXT_20 = "To";
  protected final String TEXT_21 = "());";
  protected final String TEXT_22 = NL + "                ";
  protected final String TEXT_23 = " df_";
  protected final String TEXT_24 = "_";
  protected final String TEXT_25 = " = hiveContext_";
  protected final String TEXT_26 = ".createDataFrame(";
  protected final String TEXT_27 = ", ";
  protected final String TEXT_28 = ".class);";
  protected final String TEXT_29 = NL + "                " + NL + "                org.apache.spark.sql.types.StructType st_";
  protected final String TEXT_30 = " = (org.apache.spark.sql.types.StructType) org.apache.spark.sql.catalyst.JavaTypeInference.inferDataType(";
  protected final String TEXT_31 = ".class)._1();" + NL + "                org.apache.spark.sql.types.StructField[] fields_";
  protected final String TEXT_32 = " = st_";
  protected final String TEXT_33 = ".fields();";
  protected final String TEXT_34 = NL + "                            Integer precision_";
  protected final String TEXT_35 = "_";
  protected final String TEXT_36 = " = ";
  protected final String TEXT_37 = ";" + NL + "                            if(precision_";
  protected final String TEXT_38 = "_";
  protected final String TEXT_39 = " > org.apache.spark.sql.types.DecimalType.MAX_PRECISION()) {";
  protected final String TEXT_40 = NL + "                                log.warn(\"Decimal precision (\" + precision_";
  protected final String TEXT_41 = "_";
  protected final String TEXT_42 = " + \") cannot be greater than MAX_PRECISION (\" + org.apache.spark.sql.types.DecimalType.MAX_PRECISION() + \"). Decimal precision is set to (\" + org.apache.spark.sql.types.DecimalType.MAX_PRECISION() + \").\");";
  protected final String TEXT_43 = " " + NL + "                                precision_";
  protected final String TEXT_44 = "_";
  protected final String TEXT_45 = " = org.apache.spark.sql.types.DecimalType.MAX_PRECISION();" + NL + "                            }" + NL + "                            Integer scale_";
  protected final String TEXT_46 = "_";
  protected final String TEXT_47 = " = ";
  protected final String TEXT_48 = ";" + NL + "                            if(scale_";
  protected final String TEXT_49 = "_";
  protected final String TEXT_50 = " != null) {" + NL + "                                if(scale_";
  protected final String TEXT_51 = "_";
  protected final String TEXT_52 = " > precision_";
  protected final String TEXT_53 = "_";
  protected final String TEXT_54 = ") {";
  protected final String TEXT_55 = NL + "                                    log.warn(\"Decimal scale (\" + scale_";
  protected final String TEXT_56 = "_";
  protected final String TEXT_57 = " + \") cannot be greater than precision (\" + precision_";
  protected final String TEXT_58 = "_";
  protected final String TEXT_59 = " + \"). Decimal scale is set to (\" + precision_";
  protected final String TEXT_60 = "_";
  protected final String TEXT_61 = " + \").\");";
  protected final String TEXT_62 = " " + NL + "                                    scale_";
  protected final String TEXT_63 = "_";
  protected final String TEXT_64 = " = precision_";
  protected final String TEXT_65 = "_";
  protected final String TEXT_66 = ";" + NL + "                                }" + NL + "                                fields_";
  protected final String TEXT_67 = "[";
  protected final String TEXT_68 = "] = org.apache.spark.sql.types.DataTypes.createStructField(fields_";
  protected final String TEXT_69 = "[";
  protected final String TEXT_70 = "].name(), org.apache.spark.sql.types.DataTypes.createDecimalType(precision_";
  protected final String TEXT_71 = "_";
  protected final String TEXT_72 = ", scale_";
  protected final String TEXT_73 = "_";
  protected final String TEXT_74 = "), fields_";
  protected final String TEXT_75 = "[";
  protected final String TEXT_76 = "].nullable());" + NL + "                            } else {" + NL + "                                scale_";
  protected final String TEXT_77 = "_";
  protected final String TEXT_78 = " = (";
  protected final String TEXT_79 = " > precision_";
  protected final String TEXT_80 = "_";
  protected final String TEXT_81 = ") ? precision_";
  protected final String TEXT_82 = "_";
  protected final String TEXT_83 = " : ";
  protected final String TEXT_84 = ";";
  protected final String TEXT_85 = NL + "                                log.warn(\"Decimal scale is set to (\" + scale_";
  protected final String TEXT_86 = "_";
  protected final String TEXT_87 = " + \").\");";
  protected final String TEXT_88 = " " + NL + "                                fields_";
  protected final String TEXT_89 = "[";
  protected final String TEXT_90 = "] = org.apache.spark.sql.types.DataTypes.createStructField(fields_";
  protected final String TEXT_91 = "[";
  protected final String TEXT_92 = "].name(), org.apache.spark.sql.types.DataTypes.createDecimalType(precision_";
  protected final String TEXT_93 = "_";
  protected final String TEXT_94 = ", scale_";
  protected final String TEXT_95 = "_";
  protected final String TEXT_96 = "), fields_";
  protected final String TEXT_97 = "[";
  protected final String TEXT_98 = "].nullable());" + NL + "                            }";
  protected final String TEXT_99 = NL + "                            Integer scale_";
  protected final String TEXT_100 = "_";
  protected final String TEXT_101 = " = ";
  protected final String TEXT_102 = ";" + NL + "                            if(scale_";
  protected final String TEXT_103 = "_";
  protected final String TEXT_104 = " != null) {" + NL + "                                if(scale_";
  protected final String TEXT_105 = "_";
  protected final String TEXT_106 = " > org.apache.spark.sql.types.DecimalType.MAX_SCALE()) {";
  protected final String TEXT_107 = NL + "                                    log.warn(\"Decimal scale (\" + scale_";
  protected final String TEXT_108 = "_";
  protected final String TEXT_109 = " + \") cannot be greater than MAX_SCALE (\" + org.apache.spark.sql.types.DecimalType.MAX_SCALE() + \"). Decimal scale is set to (\" + org.apache.spark.sql.types.DecimalType.MAX_SCALE() + \").\");";
  protected final String TEXT_110 = " " + NL + "                                    scale_";
  protected final String TEXT_111 = "_";
  protected final String TEXT_112 = " = org.apache.spark.sql.types.DecimalType.MAX_SCALE();" + NL + "                                }" + NL + "                                fields_";
  protected final String TEXT_113 = "[";
  protected final String TEXT_114 = "] = org.apache.spark.sql.types.DataTypes.createStructField(fields_";
  protected final String TEXT_115 = "[";
  protected final String TEXT_116 = "].name(), org.apache.spark.sql.types.DataTypes.createDecimalType(org.apache.spark.sql.types.DecimalType.MAX_PRECISION(), scale_";
  protected final String TEXT_117 = "_";
  protected final String TEXT_118 = "), fields_";
  protected final String TEXT_119 = "[";
  protected final String TEXT_120 = "].nullable());" + NL + "                            } else {";
  protected final String TEXT_121 = NL + "                                log.warn(\"Decimal precision is set to (\" + org.apache.spark.sql.types.DecimalType.MAX_PRECISION() + \") and scale is set to (\" + ";
  protected final String TEXT_122 = " + \").\");";
  protected final String TEXT_123 = " " + NL + "                                fields_";
  protected final String TEXT_124 = "[";
  protected final String TEXT_125 = "] = org.apache.spark.sql.types.DataTypes.createStructField(fields_";
  protected final String TEXT_126 = "[";
  protected final String TEXT_127 = "].name(), org.apache.spark.sql.types.DataTypes.createDecimalType(org.apache.spark.sql.types.DecimalType.MAX_PRECISION(), ";
  protected final String TEXT_128 = "), fields_";
  protected final String TEXT_129 = "[";
  protected final String TEXT_130 = "].nullable());" + NL + "                            }";
  protected final String TEXT_131 = NL + "                org.apache.spark.api.java.JavaRDD<org.apache.spark.sql.Row> rddRow_";
  protected final String TEXT_132 = " = ";
  protected final String TEXT_133 = ".map(new ";
  protected final String TEXT_134 = "_";
  protected final String TEXT_135 = "ToRow());";
  protected final String TEXT_136 = NL + "                ";
  protected final String TEXT_137 = " df_";
  protected final String TEXT_138 = "_";
  protected final String TEXT_139 = " = hiveContext_";
  protected final String TEXT_140 = ".createDataFrame(rddRow_";
  protected final String TEXT_141 = ", org.apache.spark.sql.types.DataTypes.createStructType(fields_";
  protected final String TEXT_142 = "));";
  protected final String TEXT_143 = NL + "                // Ensure that the columns order is consistent with the component schema" + NL + "                java.util.List<org.apache.spark.sql.Column> columns_";
  protected final String TEXT_144 = " = new java.util.ArrayList<org.apache.spark.sql.Column>();";
  protected final String TEXT_145 = NL + "                    columns_";
  protected final String TEXT_146 = ".add(df_";
  protected final String TEXT_147 = "_";
  protected final String TEXT_148 = ".col(\"";
  protected final String TEXT_149 = "\"));";
  protected final String TEXT_150 = NL + "                df_";
  protected final String TEXT_151 = "_";
  protected final String TEXT_152 = " = df_";
  protected final String TEXT_153 = "_";
  protected final String TEXT_154 = ".select(scala.collection.JavaConversions.asScalaBuffer(columns_";
  protected final String TEXT_155 = ").seq());";
  protected final String TEXT_156 = NL + "            \torg.apache.spark.sql.SparkSession sparkSession_";
  protected final String TEXT_157 = " = org.apache.spark.sql.SparkSession.builder().sparkContext(ctx.sc()).getOrCreate();" + NL + "\t\t\t\torg.apache.spark.sql.catalog.Catalog catalog_";
  protected final String TEXT_158 = " = sparkSession_";
  protected final String TEXT_159 = ".catalog();" + NL + "            \tString databaseUri_";
  protected final String TEXT_160 = " = catalog_";
  protected final String TEXT_161 = ".getDatabase(";
  protected final String TEXT_162 = ").locationUri();";
  protected final String TEXT_163 = NL + "            hiveContext_";
  protected final String TEXT_164 = ".sql(\"USE \" + ";
  protected final String TEXT_165 = ");";
  protected final String TEXT_166 = NL + "                df_";
  protected final String TEXT_167 = "_";
  protected final String TEXT_168 = ".saveAsTable(";
  protected final String TEXT_169 = ", \"";
  protected final String TEXT_170 = "\", org.apache.spark.sql.SaveMode.";
  protected final String TEXT_171 = ");";
  protected final String TEXT_172 = NL + "                   hiveContext_";
  protected final String TEXT_173 = ".setConf(\"hive.exec.dynamic.partition\", \"true\");" + NL + "                   hiveContext_";
  protected final String TEXT_174 = ".setConf(\"hive.exec.dynamic.partition.mode\", \"nonstrict\");";
  protected final String TEXT_175 = NL + "                df_";
  protected final String TEXT_176 = "_";
  protected final String TEXT_177 = ".write()" + NL + "                        .mode(org.apache.spark.sql.SaveMode.";
  protected final String TEXT_178 = ")";
  protected final String TEXT_179 = NL + "                        .format(\"";
  protected final String TEXT_180 = "\")";
  protected final String TEXT_181 = NL + "                        .option(\"path\", databaseUri_";
  protected final String TEXT_182 = " + \"/\" + ";
  protected final String TEXT_183 = ")";
  protected final String TEXT_184 = NL + "                        .partitionBy(";
  protected final String TEXT_185 = ")";
  protected final String TEXT_186 = NL + "                        .insertInto(";
  protected final String TEXT_187 = ");";
  protected final String TEXT_188 = NL + "                        .saveAsTable(";
  protected final String TEXT_189 = ");";
  protected final String TEXT_190 = NL + "            df_";
  protected final String TEXT_191 = "_";
  protected final String TEXT_192 = ".write().format(\"orc\").mode(org.apache.spark.sql.SaveMode.";
  protected final String TEXT_193 = ")" + NL + "            .save(";
  protected final String TEXT_194 = "  + ";
  protected final String TEXT_195 = ");";

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    
// Parse the inputs to this javajet generator.
final BigDataCodeGeneratorArgument codeGenArgument = (BigDataCodeGeneratorArgument) argument;
final INode node = (INode)codeGenArgument.getArgument();
final String cid = node.getUniqueName();

final String dataframeClass = org.talend.hadoop.distribution.ESparkVersion.SPARK_2_0.compareTo(codeGenArgument.getSparkVersion()) > 0
    ? "org.apache.spark.sql.DataFrame"
    : "org.apache.spark.sql.Dataset<org.apache.spark.sql.Row>";

    
List<IMetadataTable> metadatas = node.getMetadataList();

// Find the tSparkConfiguration and define which Spark version is currently used. It will define the generation mode: RDD with InputFormat or native Dataframe API.
final java.util.List<? extends INode> sparkConfigs = node.getProcess().getNodesOfType("tSparkConfiguration");
INode sparkConfig = null;
if(sparkConfigs != null && sparkConfigs.size() > 0) {
    sparkConfig = sparkConfigs.get(0);
}

boolean useLocalMode = false;
org.talend.hadoop.distribution.component.SparkBatchComponent sparkBatchDistrib = null;
if(sparkConfig != null) {
    String sparkDistribution = ElementParameterParser.getValue(sparkConfig, "__DISTRIBUTION__");
    String sparkVersion = ElementParameterParser.getValue(sparkConfig, "__SPARK_VERSION__");

    useLocalMode = "true".equals(ElementParameterParser.getValue(sparkConfig, "__SPARK_LOCAL_MODE__"));
    if(!useLocalMode) {
        try {
            sparkBatchDistrib = (org.talend.hadoop.distribution.component.SparkBatchComponent) org.talend.hadoop.distribution.DistributionFactory.buildDistribution(sparkDistribution, sparkVersion);
        } catch (java.lang.Exception e) {
            e.printStackTrace();
            return "";
        }
    }
}

TSqlRowUtil tSqlRowUtil = new TSqlRowUtil(node);
String validateError = tSqlRowUtil.validate(true, false);
if (validateError != null) {
    // Cause the job compilation to explicitly fail if there is a problem.
    return "throw new JobConfigurationError(\"" + validateError +"\");";
}

if(metadatas != null && metadatas.size() > 0) {
    IMetadataTable metadata = metadatas.get(0);
    if(metadata != null) {
    
        IConnection componentIncomingConnection = tSqlRowUtil.getIncomingConnections().get(0);
        String saveMode = ElementParameterParser.getValue(node, "__SAVEMODE__");
        
        String saveFormat = ElementParameterParser.getValue(node, "__TABLEFORMAT__");
        Boolean isHiveSchema = ElementParameterParser.getBooleanValue(node, "__RETRIEVE_HIVE_TABLE_SCHEMA__");
        if (isHiveSchema) saveFormat = "hive";
        
        String outputSource = ElementParameterParser.getValue(node, "__OUTPUT_SOURCE__");
        
        // When true, lets Dataframe re-order the columns by alphabetical order.
        // This has been added for retrocompatibility with previous Studio versions.
        boolean sortColumnsAlphabetically = ElementParameterParser.getBooleanValue(node, "__SORT_COLUMNS_ALPHABETICALLY__");
        
        // Get Hive & HDFS configurations
        String hiveConfiguration = ElementParameterParser.getValue(node, "__HIVE_STORAGE_CONFIGURATION__");
        String hdfsConfiguration = ElementParameterParser.getValue(node, "__HDFS_STORAGE_CONFIGURATION__");

        Boolean hivePartitionsEnabled = ElementParameterParser.getBooleanValue(node, "__HIVE_PARTITIONS_ENABLED__");
        List<Map<String, String>> hivePartitionsKeys = null;
        StringJoiner hivePartitionsKeysSJ = new StringJoiner(",");
        
        if (hivePartitionsEnabled) {
            hivePartitionsKeys = (List<Map<String,String>>) ElementParameterParser.getObjectValue(node, "__HIVE_PARTITIONS_KEYS__");
            for (Map<String, String> parameterRow: hivePartitionsKeys) {
                hivePartitionsKeysSJ.add(String.format("\"%1$s\"",parameterRow.get("KEY_COLUMN")));
            }
        }
        
        INode hiveConfigurationNode = null;
        boolean isHiveMetastoreHaEnabled = false;
        String hiveThriftMetaStoreHost = null;
        String hiveThriftMetaStorePort = null;
        String hiveMetastoreUris = null;
        INode hdfsConfigurationNode = null;
        String hdfsNamenodeURI = null;
        boolean useKrb = false;
        boolean useMaprTicket = false;
        String hivePrincipal = null;
        
        for (INode pNode1 : node.getProcess().getNodesOfType("tHiveConfiguration")) {
            if(hiveConfiguration!=null && hiveConfiguration.equals(pNode1.getUniqueName())) {
                hiveConfigurationNode = pNode1;
                isHiveMetastoreHaEnabled = "true".equals(ElementParameterParser.getValue(hiveConfigurationNode, "__ENABLE_HIVE_HA__"));
                if(!isHiveMetastoreHaEnabled) {
                    hiveThriftMetaStoreHost = ElementParameterParser.getValue(hiveConfigurationNode, "__HOST__");
                    hiveThriftMetaStorePort = ElementParameterParser.getValue(hiveConfigurationNode, "__PORT__");
                } else {
                    hiveMetastoreUris = ElementParameterParser.getValue(hiveConfigurationNode, "__HIVE_METASTORE_URIS__");
                }
                useKrb = "true".equals(ElementParameterParser.getValue(hiveConfigurationNode, "__USE_KRB__"));
                hivePrincipal = ElementParameterParser.getValue(hiveConfigurationNode, "__HIVE_PRINCIPAL__");
                useMaprTicket = "true".equals(ElementParameterParser.getValue(hiveConfigurationNode, "__USE_MAPRTICKET__"));
                break;
            }
        }

        if (useLocalMode || (sparkBatchDistrib != null && !sparkBatchDistrib.useCloudLauncher())) {
            for (INode pNode2 : node.getProcess().getNodesOfType("tHDFSConfiguration")) {
                if(hdfsConfiguration!=null && hdfsConfiguration.equals(pNode2.getUniqueName())) {
                    hdfsConfigurationNode = pNode2;
                    hdfsNamenodeURI = ElementParameterParser.getValue(hdfsConfigurationNode, "__FS_DEFAULT_NAME__");
                    break;
                }
            }
        }

    stringBuffer.append(TEXT_1);
    
if(!isHiveMetastoreHaEnabled) {

    stringBuffer.append(TEXT_2);
    stringBuffer.append(hiveThriftMetaStoreHost);
    stringBuffer.append(TEXT_3);
    stringBuffer.append(hiveThriftMetaStorePort);
    stringBuffer.append(TEXT_4);
    
} else {

    stringBuffer.append(TEXT_5);
    stringBuffer.append(hiveMetastoreUris);
    stringBuffer.append(TEXT_6);
    
}

    stringBuffer.append(TEXT_7);
    
        if(useKrb){
            
    stringBuffer.append(TEXT_8);
    stringBuffer.append(hivePrincipal);
    stringBuffer.append(TEXT_9);
    
        }
    
        if(useMaprTicket){
            
    stringBuffer.append(TEXT_10);
    
        }

    
        if(hdfsNamenodeURI != null) {
        
    stringBuffer.append(TEXT_11);
    stringBuffer.append(hdfsNamenodeURI);
    stringBuffer.append(TEXT_12);
    
        }

    stringBuffer.append(TEXT_13);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_14);
    
		org.talend.designer.bigdata.avro.AvroRecordStructGenerator avroRecordStructGenerator = (org.talend.designer.bigdata.avro.AvroRecordStructGenerator) codeGenArgument.getRecordStructGenerator();
        // DATAFRAME DATE CONVERSION
        for(IConnection incomingConnection : tSqlRowUtil.getIncomingConnections()) {
            String inStructName = codeGenArgument.getRecordStructName(incomingConnection);
            String inRddName = "rdd_"+incomingConnection.getName();
            String rddName, structName;
        
            if(tSqlRowUtil.containsDateFields(incomingConnection)) {
                // Additional map to convert from java.util.Date to java.sql.Date or java.sql.Timestamp
                String newRddName = "tmp_rdd_"+incomingConnection.getName();
                String newStructName = avroRecordStructGenerator.generateRecordStructForDataFrame("DF_"+inStructName, inStructName);
                
    stringBuffer.append(TEXT_15);
    stringBuffer.append(newStructName);
    stringBuffer.append(TEXT_16);
    stringBuffer.append(newRddName);
    stringBuffer.append(TEXT_17);
    stringBuffer.append(inRddName);
    stringBuffer.append(TEXT_18);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_19);
    stringBuffer.append(inStructName);
    stringBuffer.append(TEXT_20);
    stringBuffer.append(newStructName);
    stringBuffer.append(TEXT_21);
    
                rddName = newRddName;
                structName = newStructName;
            }else{
                // No need for additional map
                rddName = inRddName;
                structName = inStructName;
            }
            // Convert the RDD into a DF
            if(org.talend.hadoop.distribution.ESparkVersion.SPARK_1_3.compareTo(codeGenArgument.getSparkVersion()) >= 0){
                
    stringBuffer.append(TEXT_22);
    stringBuffer.append(dataframeClass);
    stringBuffer.append(TEXT_23);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_24);
    stringBuffer.append(incomingConnection.getName());
    stringBuffer.append(TEXT_25);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_26);
    stringBuffer.append(rddName);
    stringBuffer.append(TEXT_27);
    stringBuffer.append(structName);
    stringBuffer.append(TEXT_28);
    
            } 
            // Spark > 1.3 uses the Java JavaTypeInference to change Big Decimal precision 
            else {
                
    stringBuffer.append(TEXT_29);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_30);
    stringBuffer.append(structName);
    stringBuffer.append(TEXT_31);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_32);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_33);
    
                List<IMetadataColumn> columnsCopy = new java.util.ArrayList<IMetadataColumn>(metadata.getListColumns());
                java.util.Collections.sort(columnsCopy, new java.util.Comparator<IMetadataColumn>() {
                    @Override
                    public int compare(IMetadataColumn c1, IMetadataColumn c2) {
                        return c1.getLabel().compareTo(c2.getLabel());
                    }
                });
                int nbColumns = columnsCopy.size();
                for(int i=0; i<nbColumns; i++) {
                    IMetadataColumn column = columnsCopy.get(i);
                    String columnName = column.getLabel();
                    String typeToGenerate = JavaTypesManager.getTypeToGenerate(column.getTalendType(), column.isNullable());
                    JavaType javaType = JavaTypesManager.getJavaTypeFromId(column.getTalendType());
                    if(javaType == JavaTypesManager.BIGDECIMAL) {
                        Integer precision = column.getLength();
                        Integer scale = column.getPrecision();
                        final int defaultScale = 18;
                        
                        boolean isLog4jEnabled = ("true").equals(ElementParameterParser.getValue(node.getProcess(), "__LOG4J_ACTIVATE__"));
                        
                        if(precision != null) {
                        
    stringBuffer.append(TEXT_34);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_35);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_36);
    stringBuffer.append(precision);
    stringBuffer.append(TEXT_37);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_38);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_39);
     if(isLog4jEnabled) { 
    stringBuffer.append(TEXT_40);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_41);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_42);
     } 
    stringBuffer.append(TEXT_43);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_44);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_45);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_46);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_47);
    stringBuffer.append(scale);
    stringBuffer.append(TEXT_48);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_49);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_50);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_51);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_52);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_53);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_54);
     if(isLog4jEnabled) { 
    stringBuffer.append(TEXT_55);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_56);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_57);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_58);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_59);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_60);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_61);
     } 
    stringBuffer.append(TEXT_62);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_63);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_64);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_65);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_66);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_67);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_68);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_69);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_70);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_71);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_72);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_73);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_74);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_75);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_76);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_77);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_78);
    stringBuffer.append(defaultScale);
    stringBuffer.append(TEXT_79);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_80);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_81);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_82);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_83);
    stringBuffer.append(defaultScale);
    stringBuffer.append(TEXT_84);
     if(isLog4jEnabled) { 
    stringBuffer.append(TEXT_85);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_86);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_87);
     } 
    stringBuffer.append(TEXT_88);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_89);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_90);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_91);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_92);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_93);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_94);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_95);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_96);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_97);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_98);
    
                        } else {
                        
    stringBuffer.append(TEXT_99);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_100);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_101);
    stringBuffer.append(scale);
    stringBuffer.append(TEXT_102);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_103);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_104);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_105);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_106);
     if(isLog4jEnabled) { 
    stringBuffer.append(TEXT_107);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_108);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_109);
     } 
    stringBuffer.append(TEXT_110);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_111);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_112);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_113);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_114);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_115);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_116);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_117);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_118);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_119);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_120);
     if(isLog4jEnabled) { 
    stringBuffer.append(TEXT_121);
    stringBuffer.append(defaultScale);
    stringBuffer.append(TEXT_122);
     } 
    stringBuffer.append(TEXT_123);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_124);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_125);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_126);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_127);
    stringBuffer.append(defaultScale);
    stringBuffer.append(TEXT_128);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_129);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_130);
    
                        }
                    }
                }
    
    stringBuffer.append(TEXT_131);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_132);
    stringBuffer.append(rddName);
    stringBuffer.append(TEXT_133);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_134);
    stringBuffer.append(inStructName);
    stringBuffer.append(TEXT_135);
    stringBuffer.append(TEXT_136);
    stringBuffer.append(dataframeClass);
    stringBuffer.append(TEXT_137);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_138);
    stringBuffer.append(incomingConnection.getName());
    stringBuffer.append(TEXT_139);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_140);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_141);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_142);
    
            }
            if(!sortColumnsAlphabetically) {

    stringBuffer.append(TEXT_143);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_144);
    
                for(org.talend.core.model.metadata.IMetadataColumn column : tSqlRowUtil.getColumns(incomingConnection)) {
                
    stringBuffer.append(TEXT_145);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_146);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_147);
    stringBuffer.append(incomingConnection.getName());
    stringBuffer.append(TEXT_148);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_149);
    
                }
                
    stringBuffer.append(TEXT_150);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_151);
    stringBuffer.append(incomingConnection.getName());
    stringBuffer.append(TEXT_152);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_153);
    stringBuffer.append(incomingConnection.getName());
    stringBuffer.append(TEXT_154);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_155);
    
            }
        }

        // Choose the correct output source
        if(outputSource.equals("HIVE_TABLE")){
            String hiveDatabaseName = ElementParameterParser.getValue(node, "__HIVE_DATABASE_NAME__");
            String hiveTableName = ElementParameterParser.getValue(node, "__HIVE_TABLE_NAME__");
            boolean needDatabaseLocationUri = org.talend.hadoop.distribution.ESparkVersion.SPARK_2_2.compareTo(codeGenArgument.getSparkVersion()) <= 0;
            if (needDatabaseLocationUri){
            
    stringBuffer.append(TEXT_156);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_157);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_158);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_159);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_160);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_161);
    stringBuffer.append(hiveDatabaseName);
    stringBuffer.append(TEXT_162);
    
            }
            
    stringBuffer.append(TEXT_163);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_164);
    stringBuffer.append(hiveDatabaseName);
    stringBuffer.append(TEXT_165);
    
            // Versions < 1.5 - no write() method or experimental - no partitioning
            if (org.talend.hadoop.distribution.ESparkVersion.SPARK_1_5.compareTo(codeGenArgument.getSparkVersion()) > 0) {
                
    stringBuffer.append(TEXT_166);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_167);
    stringBuffer.append(componentIncomingConnection.getName());
    stringBuffer.append(TEXT_168);
    stringBuffer.append(hiveTableName);
    stringBuffer.append(TEXT_169);
    stringBuffer.append(saveFormat);
    stringBuffer.append(TEXT_170);
    stringBuffer.append(saveMode);
    stringBuffer.append(TEXT_171);
    
            } else {
                // Versions >= 1.5 - write() method available - partitioning
                
                // Manually enable nonstrict partition mode when appending data
                if (hivePartitionsEnabled && "Append".equals(saveMode)) {
                
    stringBuffer.append(TEXT_172);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_173);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_174);
    
                }
                
    stringBuffer.append(TEXT_175);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_176);
    stringBuffer.append(componentIncomingConnection.getName());
    stringBuffer.append(TEXT_177);
    stringBuffer.append(saveMode);
    stringBuffer.append(TEXT_178);
     if(org.talend.hadoop.distribution.ESparkVersion.SPARK_2_1.compareTo(codeGenArgument.getSparkVersion()) < 0 || !"Append".equals(saveMode)) { 
    stringBuffer.append(TEXT_179);
    stringBuffer.append(saveFormat);
    stringBuffer.append(TEXT_180);
     if (needDatabaseLocationUri && (org.talend.hadoop.distribution.ESparkVersion.SPARK_2_1.compareTo(codeGenArgument.getSparkVersion()) < 0 || !"Append".equals(saveMode))) {
    stringBuffer.append(TEXT_181);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_182);
    stringBuffer.append(hiveTableName);
    stringBuffer.append(TEXT_183);
     }} 
     if (hivePartitionsEnabled) { 
    stringBuffer.append(TEXT_184);
    stringBuffer.append(hivePartitionsKeysSJ.toString());
    stringBuffer.append(TEXT_185);
     } 
     if(org.talend.hadoop.distribution.ESparkVersion.SPARK_2_1.compareTo(codeGenArgument.getSparkVersion()) >= 0 && "Append".equals(saveMode)) { 
    stringBuffer.append(TEXT_186);
    stringBuffer.append(hiveTableName);
    stringBuffer.append(TEXT_187);
     } else { 
    stringBuffer.append(TEXT_188);
    stringBuffer.append(hiveTableName);
    stringBuffer.append(TEXT_189);
     } 
    
            }
        } else if (outputSource.equals("ORC_FILE")){
            String outputFolder = ElementParameterParser.getValue(node, "__OUTPUT_FOLDER__");
            
    stringBuffer.append(TEXT_190);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_191);
    stringBuffer.append(componentIncomingConnection.getName());
    stringBuffer.append(TEXT_192);
    stringBuffer.append(saveMode);
    stringBuffer.append(TEXT_193);
    stringBuffer.append((hdfsNamenodeURI != null) ? hdfsNamenodeURI : "\"\"");
    stringBuffer.append(TEXT_194);
    stringBuffer.append(outputFolder);
    stringBuffer.append(TEXT_195);
    
        }
    } // end if(metadata != null)
} // end if(metadatas != null && metadatas.size() > 0)

    return stringBuffer.toString();
  }
}
