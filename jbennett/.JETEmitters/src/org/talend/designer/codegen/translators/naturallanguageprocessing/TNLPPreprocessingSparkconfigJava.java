package org.talend.designer.codegen.translators.naturallanguageprocessing;

import org.talend.core.model.process.INode;
import org.talend.core.model.process.ElementParameterParser;
import org.talend.core.model.metadata.IMetadataTable;
import org.talend.core.model.metadata.IMetadataColumn;
import org.talend.core.model.process.IConnection;
import org.talend.core.model.process.EConnectionType;
import org.talend.designer.common.BigDataCodeGeneratorArgument;
import org.talend.core.model.metadata.types.JavaTypesManager;
import org.talend.core.model.process.IConnectionCategory;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.HashSet;

public class TNLPPreprocessingSparkconfigJava
{
  protected static String nl;
  public static synchronized TNLPPreprocessingSparkconfigJava create(String lineSeparator)
  {
    nl = lineSeparator;
    TNLPPreprocessingSparkconfigJava result = new TNLPPreprocessingSparkconfigJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "final org.talend.dataquality.nlp.toolkit.AbstractToolkit ";
  protected final String TEXT_2 = "nlp = org.talend.dataquality.nlp.toolkit.ToolkitFactory.create(org.talend.dataquality.nlp.toolkit.ToolkitType.";
  protected final String TEXT_3 = ");" + NL + "org.apache.spark.api.java.JavaRDD<";
  protected final String TEXT_4 = "> rdd_";
  protected final String TEXT_5 = " = rdd_";
  protected final String TEXT_6 = ".map(new ";
  protected final String TEXT_7 = "Preprocessing(\"";
  protected final String TEXT_8 = "\",";
  protected final String TEXT_9 = "nlp, ";
  protected final String TEXT_10 = "));";

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
     
final BigDataCodeGeneratorArgument codeGenArgument = (BigDataCodeGeneratorArgument) argument;
final INode node = (INode)codeGenArgument.getArgument();
final String cid = node.getUniqueName();
List<? extends IConnection> inConns = node.getIncomingConnections();
List< ? extends IConnection> outConns = node.getOutgoingConnections();
IMetadataTable inputMetadataTable = null;
IMetadataTable outputMetadataTable = null;
String inConnName = "";
String inConnTypeName = "";
String outConnName = "";
String outConnTypeName = "";
List<IMetadataColumn> outputColumns = null;
List<IMetadataColumn> inputColumns = null;
StringBuilder pipeline = new StringBuilder("");
if (inConns.size() == 1){
    inConnName = inConns.get(0).getUniqueName();
    inputMetadataTable = inConns.get(0).getMetadataTable();
    inConnTypeName = codeGenArgument.getRecordStructName(inConns.get(0));
    inputColumns = inputMetadataTable.getListColumns();
}
if (outConns.size() == 1){
    outConnName = outConns.get(0).getName();
    outputMetadataTable = outConns.get(0).getMetadataTable();
    outConnTypeName = codeGenArgument.getRecordStructName(outConns.get(0));
    outputColumns = outputMetadataTable.getListColumns();
}
String col = (String) ElementParameterParser.getObjectValue(node, "__PREPROCESSING__");
Boolean cleanTags = (Boolean) ElementParameterParser.getObjectValue(node, "__CLEANTAG__");

    stringBuffer.append(TEXT_1);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_2);
    stringBuffer.append((String)ElementParameterParser.getObjectValue(node, "LIBTYPE"));
    stringBuffer.append(TEXT_3);
    stringBuffer.append(outConnTypeName);
    stringBuffer.append(TEXT_4);
    stringBuffer.append(outConnName);
    stringBuffer.append(TEXT_5);
    stringBuffer.append(inConnName);
    stringBuffer.append(TEXT_6);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_7);
    stringBuffer.append(col);
    stringBuffer.append(TEXT_8);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_9);
    stringBuffer.append(cleanTags);
    stringBuffer.append(TEXT_10);
    return stringBuffer.toString();
  }
}
