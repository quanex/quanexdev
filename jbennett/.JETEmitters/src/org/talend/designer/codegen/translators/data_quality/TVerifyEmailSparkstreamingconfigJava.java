package org.talend.designer.codegen.translators.data_quality;

import java.util.List;
import org.talend.core.model.metadata.IMetadataColumn;
import org.talend.core.model.metadata.IMetadataTable;
import org.talend.core.model.metadata.types.JavaType;
import org.talend.core.model.metadata.types.JavaTypesManager;
import org.talend.core.model.process.EConnectionType;
import org.talend.core.model.process.ElementParameterParser;
import org.talend.core.model.process.IBigDataNode;
import org.talend.core.model.process.IConnection;
import org.talend.core.model.process.IConnectionCategory;
import org.talend.core.model.process.INode;
import org.talend.designer.common.BigDataCodeGeneratorArgument;

public class TVerifyEmailSparkstreamingconfigJava
{
  protected static String nl;
  public static synchronized TVerifyEmailSparkstreamingconfigJava create(String lineSeparator)
  {
    nl = lineSeparator;
    TVerifyEmailSparkstreamingconfigJava result = new TVerifyEmailSparkstreamingconfigJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "    ";
  protected final String TEXT_2 = NL + "    ";
  protected final String TEXT_3 = NL + "            public static class ";
  protected final String TEXT_4 = " implements ";
  protected final String TEXT_5 = " {" + NL + "                private ContextProperties context = new ContextProperties();";
  protected final String TEXT_6 = NL + NL + "                //private ContextProperties context = null;" + NL + "" + NL + "                public ";
  protected final String TEXT_7 = "(JobConf job) {" + NL + "                    this.context = new ContextProperties(job);";
  protected final String TEXT_8 = NL + "                }" + NL + "" + NL + "\t            public ";
  protected final String TEXT_9 = " ";
  protected final String TEXT_10 = "(";
  protected final String TEXT_11 = ") ";
  protected final String TEXT_12 = " {" + NL + "\t            \t";
  protected final String TEXT_13 = NL + "\t            \t";
  protected final String TEXT_14 = NL + "\t                ";
  protected final String TEXT_15 = NL + "\t                return ";
  protected final String TEXT_16 = ";" + NL + "\t            }" + NL + "\t        }" + NL + "\t\t";
  protected final String TEXT_17 = NL + "            public static class ";
  protected final String TEXT_18 = " implements ";
  protected final String TEXT_19 = " {";
  protected final String TEXT_20 = NL + NL + "                private ContextProperties context = null;" + NL + "" + NL + "                public ";
  protected final String TEXT_21 = "(JobConf job) {" + NL + "                    this.context = new ContextProperties(job);" + NL + "                }" + NL + "" + NL + "                public ";
  protected final String TEXT_22 = " ";
  protected final String TEXT_23 = "(";
  protected final String TEXT_24 = ") ";
  protected final String TEXT_25 = " {" + NL + "                \t";
  protected final String TEXT_26 = NL + "\t                 \treturn ";
  protected final String TEXT_27 = ";";
  protected final String TEXT_28 = NL + "                }" + NL + "            }";
  protected final String TEXT_29 = NL + "            public static class ";
  protected final String TEXT_30 = " implements ";
  protected final String TEXT_31 = " {";
  protected final String TEXT_32 = NL + NL + "                private ContextProperties context = null;" + NL + "                private java.util.function.Function<org.apache.avro.generic.IndexedRecord, org.apache.avro.generic.IndexedRecord> function = null;" + NL + "" + NL + "                public ";
  protected final String TEXT_33 = "(JobConf job, java.util.function.Function<org.apache.avro.generic.IndexedRecord, org.apache.avro.generic.IndexedRecord> function) {" + NL + "                    this.context = new ContextProperties(job);" + NL + "                    this.function = function;" + NL + "                }" + NL + "" + NL + "                public ";
  protected final String TEXT_34 = " ";
  protected final String TEXT_35 = "(";
  protected final String TEXT_36 = ") ";
  protected final String TEXT_37 = " {";
  protected final String TEXT_38 = NL + "                    ";
  protected final String TEXT_39 = NL + "                    ";
  protected final String TEXT_40 = NL + "                    return ";
  protected final String TEXT_41 = ";" + NL + "                }" + NL + "            }";
  protected final String TEXT_42 = NL;
  protected final String TEXT_43 = NL + "            // No sparkcode generated for unnecessary ";
  protected final String TEXT_44 = NL;
  protected final String TEXT_45 = NL + "            public static class ";
  protected final String TEXT_46 = "TrueFilter implements org.apache.spark.api.java.function.Function<scala.Tuple2<Boolean, org.apache.avro.specific.SpecificRecordBase>, Boolean> {" + NL + "" + NL + "                public Boolean call(scala.Tuple2<Boolean, org.apache.avro.specific.SpecificRecordBase> arg0)" + NL + "                        throws Exception {" + NL + "                    return arg0._1;" + NL + "                }" + NL + "            }" + NL + "" + NL + "            public static class ";
  protected final String TEXT_47 = "FalseFilter implements org.apache.spark.api.java.function.Function<scala.Tuple2<Boolean, org.apache.avro.specific.SpecificRecordBase>, Boolean> {" + NL + "" + NL + "                public Boolean call(scala.Tuple2<Boolean, org.apache.avro.specific.SpecificRecordBase> arg0)" + NL + "                        throws Exception {" + NL + "                    return !arg0._1;" + NL + "                }" + NL + "            }" + NL + "" + NL + "            public static class ";
  protected final String TEXT_48 = "ToNullWritableMain implements ";
  protected final String TEXT_49 = " {" + NL + "" + NL + "                private ContextProperties context = null;" + NL + "" + NL + "                public ";
  protected final String TEXT_50 = "ToNullWritableMain(JobConf job) {" + NL + "                    this.context = new ContextProperties(job);" + NL + "                }" + NL + "" + NL + "                public ";
  protected final String TEXT_51 = " call(" + NL + "                        scala.Tuple2<Boolean, org.apache.avro.specific.SpecificRecordBase> data){";
  protected final String TEXT_52 = NL + "                    ";
  protected final String TEXT_53 = " ";
  protected final String TEXT_54 = " = (";
  protected final String TEXT_55 = ")data._2;" + NL + "                    return ";
  protected final String TEXT_56 = ";" + NL + "                }" + NL + "            }" + NL + "" + NL + "            public static class ";
  protected final String TEXT_57 = "ToNullWritableReject implements ";
  protected final String TEXT_58 = " {" + NL + "" + NL + "                private ContextProperties context = null;" + NL + "" + NL + "                public ";
  protected final String TEXT_59 = "ToNullWritableReject(JobConf job) {" + NL + "                    this.context = new ContextProperties(job);" + NL + "                }" + NL + "" + NL + "                public ";
  protected final String TEXT_60 = " call(" + NL + "                        scala.Tuple2<Boolean, org.apache.avro.specific.SpecificRecordBase> data){";
  protected final String TEXT_61 = NL + "                        ";
  protected final String TEXT_62 = " ";
  protected final String TEXT_63 = " = (";
  protected final String TEXT_64 = ")data._2;" + NL + "                    return ";
  protected final String TEXT_65 = ";" + NL + "                }" + NL + "            }";
  protected final String TEXT_66 = NL + "            // No sparkconfig generated for unnecessary ";
  protected final String TEXT_67 = NL;
  protected final String TEXT_68 = NL + "            // Extract data." + NL;
  protected final String TEXT_69 = NL + "            ";
  protected final String TEXT_70 = "<Boolean, org.apache.avro.specific.SpecificRecordBase> temporary_rdd_";
  protected final String TEXT_71 = " = rdd_";
  protected final String TEXT_72 = ".";
  protected final String TEXT_73 = "(new ";
  protected final String TEXT_74 = "(job));" + NL + "" + NL + "            // Main flow" + NL;
  protected final String TEXT_75 = NL + "            ";
  protected final String TEXT_76 = " rdd_";
  protected final String TEXT_77 = " = temporary_rdd_";
  protected final String TEXT_78 = NL + "                  .filter(new ";
  protected final String TEXT_79 = "TrueFilter())" + NL + "                    .";
  protected final String TEXT_80 = "(new ";
  protected final String TEXT_81 = "ToNullWritableMain(job));" + NL + "" + NL + "            // Reject flow";
  protected final String TEXT_82 = NL + "            ";
  protected final String TEXT_83 = " rdd_";
  protected final String TEXT_84 = " = temporary_rdd_";
  protected final String TEXT_85 = NL + "                    .filter(new ";
  protected final String TEXT_86 = "FalseFilter())" + NL + "                    .";
  protected final String TEXT_87 = "(new ";
  protected final String TEXT_88 = "ToNullWritableReject(job));";
  protected final String TEXT_89 = NL + "            ";
  protected final String TEXT_90 = " rdd_";
  protected final String TEXT_91 = " = rdd_";
  protected final String TEXT_92 = ".";
  protected final String TEXT_93 = "(new ";
  protected final String TEXT_94 = "(job));";
  protected final String TEXT_95 = NL;
  protected final String TEXT_96 = NL + "        org.talend.dataquality.email.api.EmailVerify emailVerify_";
  protected final String TEXT_97 = "=null;";
  protected final String TEXT_98 = NL + NL + "        //init an EmailVerify instance. " + NL + "        if(emailVerify_";
  protected final String TEXT_99 = "==null){" + NL + "            emailVerify_";
  protected final String TEXT_100 = " = new org.talend.dataquality.email.api.EmailVerify();" + NL + "            java.util.List<String> tldLs_";
  protected final String TEXT_101 = "=new java.util.ArrayList();" + NL + "            java.util.List domainLs_";
  protected final String TEXT_102 = "=new java.util.ArrayList();" + NL;
  protected final String TEXT_103 = NL + "                      tldLs_";
  protected final String TEXT_104 = ".add(";
  protected final String TEXT_105 = ");";
  protected final String TEXT_106 = NL + "                       domainLs_";
  protected final String TEXT_107 = ".add(";
  protected final String TEXT_108 = ");";
  protected final String TEXT_109 = NL + "            " + NL + "            //add some checkers at here" + NL + "            String localPart_";
  protected final String TEXT_110 = " = ";
  protected final String TEXT_111 = ";" + NL + "            if(";
  protected final String TEXT_112 = "){" + NL + "                emailVerify_";
  protected final String TEXT_113 = " =emailVerify_";
  protected final String TEXT_114 = ".addRegularRegexChecker(";
  protected final String TEXT_115 = ",";
  protected final String TEXT_116 = ")" + NL + "                          .addListDomainsChecker(";
  protected final String TEXT_117 = ", domainLs_";
  protected final String TEXT_118 = ")" + NL + "                          .addTLDsChecker(";
  protected final String TEXT_119 = ", tldLs_";
  protected final String TEXT_120 = ", ";
  protected final String TEXT_121 = ")" + NL + "                          .addLocalPartColumnContentChecker(true,";
  protected final String TEXT_122 = ",\"";
  protected final String TEXT_123 = "\", ";
  protected final String TEXT_124 = ", ";
  protected final String TEXT_125 = ", ";
  protected final String TEXT_126 = ", ";
  protected final String TEXT_127 = ", ";
  protected final String TEXT_128 = ")" + NL + "                          .addCallbackMailServerChecker(";
  protected final String TEXT_129 = ");" + NL + "              }else{" + NL + "              emailVerify_";
  protected final String TEXT_130 = " =emailVerify_";
  protected final String TEXT_131 = ".addRegularRegexChecker(";
  protected final String TEXT_132 = ",";
  protected final String TEXT_133 = ")" + NL + "                          .addLocalPartRegexChecker(localPart_";
  protected final String TEXT_134 = ", ";
  protected final String TEXT_135 = ", ";
  protected final String TEXT_136 = ")" + NL + "                          .addListDomainsChecker(";
  protected final String TEXT_137 = ", domainLs_";
  protected final String TEXT_138 = ")" + NL + "                          .addTLDsChecker(";
  protected final String TEXT_139 = ", tldLs_";
  protected final String TEXT_140 = ", ";
  protected final String TEXT_141 = ")" + NL + "                          .addCallbackMailServerChecker(";
  protected final String TEXT_142 = ");" + NL + "              }" + NL + "        }" + NL + "        ";
  protected final String TEXT_143 = NL + "          ";
  protected final String TEXT_144 = " = new ";
  protected final String TEXT_145 = "();" + NL + "          String email_";
  protected final String TEXT_146 = "  =";
  protected final String TEXT_147 = ".";
  protected final String TEXT_148 = ";" + NL + "          ";
  protected final String TEXT_149 = NL + "                 String firstName__";
  protected final String TEXT_150 = "  = ";
  protected final String TEXT_151 = ".";
  protected final String TEXT_152 = ";" + NL + "                 String lastName__";
  protected final String TEXT_153 = "  = ";
  protected final String TEXT_154 = ".";
  protected final String TEXT_155 = ";";
  protected final String TEXT_156 = NL + "                 ";
  protected final String TEXT_157 = ".VerificationLevel = emailVerify_";
  protected final String TEXT_158 = ".checkEmail(email_";
  protected final String TEXT_159 = ",new org.talend.dataquality.email.api.CheckerParams(firstName__";
  protected final String TEXT_160 = ",lastName__";
  protected final String TEXT_161 = ")).getResultValue();";
  protected final String TEXT_162 = NL + "                 ";
  protected final String TEXT_163 = ".SuggestedEmail = emailVerify_";
  protected final String TEXT_164 = ".getSuggestedEmail();";
  protected final String TEXT_165 = NL + "                 ";
  protected final String TEXT_166 = ".VerificationLevel = this.emailVerify_";
  protected final String TEXT_167 = ".checkEmail(email_";
  protected final String TEXT_168 = ").getResultValue();";
  protected final String TEXT_169 = NL + "          ";
  protected final String TEXT_170 = NL + "          ";
  protected final String TEXT_171 = NL + "          " + NL + "          // Emit the parsed structure on the main output.";
  protected final String TEXT_172 = NL + "          ";
  protected final String TEXT_173 = NL + "                  ";
  protected final String TEXT_174 = NL + "          ";
  protected final String TEXT_175 = NL + "        ";
  protected final String TEXT_176 = NL + "        ";

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    stringBuffer.append(TEXT_1);
    stringBuffer.append(TEXT_2);
    
//Parse the inputs to this javajet generator.
final BigDataCodeGeneratorArgument codeGenArgument = (BigDataCodeGeneratorArgument) argument;
final INode node = (INode) codeGenArgument.getArgument();
final IBigDataNode bigDataNode = (IBigDataNode) codeGenArgument.getArgument();
final String cid = node.getUniqueName();

    
	/**
	 * Code generated for component with single output
 	 */
    class SOFunctionGenerator extends org.talend.designer.spark.generator.FunctionGenerator{

    	SOFunctionGenerator(org.talend.designer.common.TransformerBase transformer) {
            super(transformer);
        }

    	SOFunctionGenerator(org.talend.designer.common.TransformerBase transformer, org.talend.designer.spark.generator.SparkFunction sparkFunction) {
    		super(transformer, sparkFunction);
    	}

    	@Override
        public void generate(){
		
    stringBuffer.append(TEXT_3);
    stringBuffer.append(this.sparkFunction.getClassName(cid));
    stringBuffer.append(TEXT_4);
    stringBuffer.append(this.sparkFunction.getCodeFunctionImplementation(getOutValueClass(), getInValueClass()));
    stringBuffer.append(TEXT_5);
    
                this.transformer.generateHelperClasses(true);
                this.transformer.generateTransformContextDeclaration();
                
    stringBuffer.append(TEXT_6);
    stringBuffer.append(this.sparkFunction.getClassName(cid));
    stringBuffer.append(TEXT_7);
    
                    this.transformer.generateTransformContextDeclarationInConstructor();
                    
    stringBuffer.append(TEXT_8);
    stringBuffer.append(this.sparkFunction.getCodeFunctionReturnedType(this.outValueClass.toString()));
    stringBuffer.append(TEXT_9);
    stringBuffer.append(this.sparkFunction.getCodeImplementedMethod());
    stringBuffer.append(TEXT_10);
    stringBuffer.append(this.sparkFunction.getCodeFunctionArgument(getInValueClass()));
    stringBuffer.append(TEXT_11);
    stringBuffer.append(this.sparkFunction.getCodeThrowException());
    stringBuffer.append(TEXT_12);
    stringBuffer.append(this.sparkFunction.getMethodHeader(this.outValueClass, this.outValue, this.inValueClass, this.inValue));
    stringBuffer.append(TEXT_13);
    stringBuffer.append(this.sparkFunction.getCodeKeyMapping(getInValue()));
    stringBuffer.append(TEXT_14);
    
	                this.transformer.generateTransformContextInitialization();
	                this.transformer.generateTransform(true);
	                
    stringBuffer.append(TEXT_15);
    stringBuffer.append(this.sparkFunction.getCodeFunctionReturn(this.getOutValue(), this.getOutValueClass()));
    stringBuffer.append(TEXT_16);
    
        }
    }

	/**
	 * Code generated for component with multiple outputs
 	 */
    class MOFunctionGenerator extends org.talend.designer.spark.generator.FunctionGenerator{

        /** The single connection to pass along the output chain of Mappers
         *  instead of sending to a dedicated collector via MultipleOutputs. */
        String connectionToChain = null;

        MOFunctionGenerator(org.talend.designer.common.TransformerBase transformer) {
            super(transformer);
            defaultOutKeyClass = "Boolean";
        }

    	MOFunctionGenerator(org.talend.designer.common.TransformerBase transformer, org.talend.designer.spark.generator.SparkFunction sparkFunction) {
    		super(transformer, sparkFunction);
            defaultOutKeyClass = "Boolean";
    	}

    	@Override
        public void generate(){
        
    stringBuffer.append(TEXT_17);
    stringBuffer.append(this.sparkFunction.getClassName(cid));
    stringBuffer.append(TEXT_18);
    stringBuffer.append(this.sparkFunction.getCodeFunctionImplementationOutputFixedType(getInValueClass(), "Boolean", "org.apache.avro.specific.SpecificRecordBase"));
    stringBuffer.append(TEXT_19);
    
                this.transformer.generateHelperClasses(true);
                this.transformer.generateTransformContextDeclaration();
                
    stringBuffer.append(TEXT_20);
    stringBuffer.append(this.sparkFunction.getClassName(cid));
    stringBuffer.append(TEXT_21);
    stringBuffer.append(this.sparkFunction.getCodeFunctionReturnedTypeFixedType((String)this.outKeyClass, "org.apache.avro.specific.SpecificRecordBase"));
    stringBuffer.append(TEXT_22);
    stringBuffer.append(this.sparkFunction.getCodeImplementedMethod());
    stringBuffer.append(TEXT_23);
    stringBuffer.append(this.sparkFunction.getCodeFunctionArgument(getInValueClass()));
    stringBuffer.append(TEXT_24);
    stringBuffer.append(this.sparkFunction.getCodeThrowException());
    stringBuffer.append(TEXT_25);
    stringBuffer.append(this.sparkFunction.getMethodHeader(this.outValueClass, this.outValue, this.inValueClass, this.inValue));
    
                    this.transformer.generateTransformContextInitialization();
                    this.transformer.generateTransform(true);
	                if(this.sparkFunction.getCodeFunctionReturn()!=null) {
                
    stringBuffer.append(TEXT_26);
    stringBuffer.append(this.sparkFunction.getCodeFunctionReturn());
    stringBuffer.append(TEXT_27);
    
	            	}
                
    stringBuffer.append(TEXT_28);
    
        }
    }
    
    /**
     * Code generated for tDataprepRun component
     */
    class DataprepFunctionGenerator extends org.talend.designer.spark.generator.FunctionGenerator {

        DataprepFunctionGenerator(org.talend.designer.common.TransformerBase transformer) {
            super(transformer);
        }

        DataprepFunctionGenerator(org.talend.designer.common.TransformerBase transformer, org.talend.designer.spark.generator.SparkFunction sparkFunction) {
            super(transformer, sparkFunction);
        }

        @Override
        public void generate(){
        
    stringBuffer.append(TEXT_29);
    stringBuffer.append(this.sparkFunction.getClassName(cid));
    stringBuffer.append(TEXT_30);
    stringBuffer.append(this.sparkFunction.getCodeFunctionImplementation(getOutValueClass(), getInValueClass()));
    stringBuffer.append(TEXT_31);
    
                this.transformer.generateHelperClasses(true);
                this.transformer.generateTransformContextDeclaration();
                
    stringBuffer.append(TEXT_32);
    stringBuffer.append(this.sparkFunction.getClassName(cid));
    stringBuffer.append(TEXT_33);
    stringBuffer.append(this.sparkFunction.getCodeFunctionReturnedType(this.outValueClass.toString()));
    stringBuffer.append(TEXT_34);
    stringBuffer.append(this.sparkFunction.getCodeImplementedMethod());
    stringBuffer.append(TEXT_35);
    stringBuffer.append(this.sparkFunction.getCodeFunctionArgument(getInValueClass()));
    stringBuffer.append(TEXT_36);
    stringBuffer.append(this.sparkFunction.getCodeThrowException());
    stringBuffer.append(TEXT_37);
    stringBuffer.append(TEXT_38);
    stringBuffer.append(this.sparkFunction.getMethodHeader(this.outValueClass, this.outValue, this.inValueClass, this.inValue));
    stringBuffer.append(TEXT_39);
    stringBuffer.append(this.sparkFunction.getCodeKeyMapping(getInValue()));
    
                        this.transformer.generateTransformContextInitialization();
                        this.transformer.generateTransform(true);
                     
    stringBuffer.append(TEXT_40);
    stringBuffer.append(this.sparkFunction.getCodeFunctionReturn(this.getOutValue(), this.getOutValueClass()));
    stringBuffer.append(TEXT_41);
    
        }
    }

    stringBuffer.append(TEXT_42);
    

/**
 * A common, reusable utility that generates code in the context of a spark
 * component, for reading and writing to a spark RDD.
 */
class SparkRowTransformUtil extends org.talend.designer.common.CommonRowTransformUtil {

    private boolean isMultiOutput = false;

    private org.talend.designer.spark.generator.SparkFunction sparkFunction = null;

    private org.talend.designer.spark.generator.FunctionGenerator functionGenerator = null;

    public SparkRowTransformUtil() {

    }
    
    public SparkRowTransformUtil(org.talend.designer.spark.generator.SparkFunction sparkFunction) {
        this.sparkFunction = sparkFunction;
    }
    
    public void setFunctionGenerator(org.talend.designer.spark.generator.FunctionGenerator functionGenerator) {
        this.functionGenerator = functionGenerator;
    }

    public void setMultiOutput(boolean multiOutput) {
        isMultiOutput = multiOutput;
    }

    public String getCodeToGetInField(String columnName) {
        return functionGenerator.getInValue() + "." + columnName;
    }

    public String getInValue() {
        return functionGenerator.getInValue();
    }

    public String getOutValue() {
        return functionGenerator.getOutValue();
    }

    public String getInValueClass() {
        return functionGenerator.getInValueClass();
    }

    public String getOutValueClass() {
        return functionGenerator.getOutValueClass();
    }

    public String getCodeToGetOutField(boolean isReject, String columnName) {
        return functionGenerator.getOutValue(isReject ? "reject" : "main") + "." + columnName;
    }

    public String getCodeToInitOut(boolean isReject, Iterable<IMetadataColumn> columns) {
        if(!isReject && this.sparkFunction!=null && !isMultiOutput) {
            return this.sparkFunction.getCodeToInitOut(functionGenerator.getOutValue("main"), functionGenerator.getOutValueClass("main"));
        } else {
            return "";
        }
    }

    // Method to avoid using getCodeToInitOut that calls sparkFunction.getCodeToInitOut which creates unnecessary objects
    // Check getCodeToAddToOutput in SparkFunction and its implementation in FlatMapToPairFunction
    public String getCodeToAddToOutput(boolean isReject, Iterable<IMetadataColumn> columns) {
        if(this.sparkFunction!=null && !isMultiOutput) {
            return this.sparkFunction.getCodeToAddToOutput(false, false, functionGenerator.getOutValue(isReject ? "reject" : "main"), functionGenerator.getOutValueClass(isReject ? "reject" : "main"));
        }else if(this.sparkFunction!=null && isMultiOutput){
            if(isReject){
                return this.sparkFunction.getCodeToAddToOutput(true, false, functionGenerator.getOutValue("reject"), functionGenerator.getOutValueClass("reject"));
            }else{
                return this.sparkFunction.getCodeToAddToOutput(true, true, functionGenerator.getOutValue("main"), functionGenerator.getOutValueClass("main"));
            }
        }else {
            return "";
        }
    }

    public String getCodeToSetOutField(boolean isReject, String columnName, String codeValue) {
        return functionGenerator.getOutValue(isReject ? "reject" : "main") + "." + columnName + " = " + codeValue + ";";
    }

    public String getCodeToSetOutField(boolean isReject, String columnName, String codeValue, String operator) {
        return functionGenerator.getOutValue(isReject ? "reject" : "main") + "." + columnName + " " + operator + " " + codeValue + ";";
    }

    public String getCodeToEmit(boolean isReject) {
        if (this.sparkFunction != null && isMultiOutput) {
            if (isReject) {
                return this.sparkFunction.getCodeToEmit(false, functionGenerator.getOutValue("reject"), functionGenerator.getOutValueClass("reject"));
            } else {
                return this.sparkFunction.getCodeToEmit(true, functionGenerator.getOutValue("main"), functionGenerator.getOutValueClass("main"));
            }
        } else {
            if (isReject) {
                return this.sparkFunction.getCodeToInitOut(functionGenerator.getOutValue("reject"), functionGenerator.getOutValueClass("reject"));
            } else {
                return "";
            }
        }
    }

    public void generateSparkCode(final org.talend.designer.common.TransformerBase transformer, final org.talend.designer.spark.generator.SparkFunction sparkFunction) {
        if (transformer.isMultiOutput()) {
            setMultiOutput(true);
        }
        if (transformer.isUnnecessary()) {
            
    stringBuffer.append(TEXT_43);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_44);
    
            return;
        }

        if (transformer.isMultiOutput()) {
            org.talend.designer.spark.generator.SparkFunction localSparkFunction = null;
            if ((sparkFunction instanceof org.talend.designer.spark.generator.FlatMapFunction)
                    || (sparkFunction instanceof org.talend.designer.spark.generator.FlatMapToPairFunction)) {
                localSparkFunction = new org.talend.designer.spark.generator.FlatMapToPairFunction(
                        sparkFunction.isInputPair(),
                        codeGenArgument.getSparkVersion(),
                        sparkFunction.getKeyList());
            } else {
                localSparkFunction = new org.talend.designer.spark.generator.MapToPairFunction(
                        sparkFunction.isInputPair(),
                        sparkFunction.getKeyList());
            }

            org.talend.designer.spark.generator.SparkFunction extractSparkFunction = null;
            if ((sparkFunction instanceof org.talend.designer.spark.generator.FlatMapFunction)
                    || (sparkFunction instanceof org.talend.designer.spark.generator.MapFunction)) {
                extractSparkFunction = new org.talend.designer.spark.generator.MapFunction(
                        sparkFunction.isInputPair(),
                        sparkFunction.getKeyList());
            } else {
                extractSparkFunction = new org.talend.designer.spark.generator.MapToPairFunction(
                        sparkFunction.isInputPair(),
                        sparkFunction.getKeyList());
            }
            this.sparkFunction = localSparkFunction;

            // The multi-output condition is slightly different, and the
            // MapperHelper is configured with internal names for the
            // connections.
            java.util.HashMap<String, String> names = new java.util.HashMap<String, String>();
            names.put("main", transformer.getOutConnMainName());
            names.put("reject", transformer.getOutConnRejectName());

            // Refactoring FunctionGenerator to java so we have to instaniate a MO or SO here
            functionGenerator = new MOFunctionGenerator(transformer, localSparkFunction);
            functionGenerator.init(node, cid, null, transformer.getInConnName(), null, names);

            
    stringBuffer.append(TEXT_45);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_46);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_47);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_48);
    stringBuffer.append(extractSparkFunction.getCodeFunctionImplementationInputFixedType(transformer.getOutConnMainTypeName(), "Boolean", "org.apache.avro.specific.SpecificRecordBase"));
    stringBuffer.append(TEXT_49);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_50);
    stringBuffer.append(extractSparkFunction.getCodeFunctionReturnedType(transformer.getOutConnMainTypeName()));
    stringBuffer.append(TEXT_51);
    stringBuffer.append(TEXT_52);
    stringBuffer.append(transformer.getOutConnMainTypeName());
    stringBuffer.append(TEXT_53);
    stringBuffer.append(transformer.getOutConnMainName());
    stringBuffer.append(TEXT_54);
    stringBuffer.append(transformer.getOutConnMainTypeName());
    stringBuffer.append(TEXT_55);
    stringBuffer.append(extractSparkFunction.getCodeFunctionReturn(transformer.getOutConnMainName(), transformer.getOutConnMainTypeName()));
    stringBuffer.append(TEXT_56);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_57);
    stringBuffer.append(extractSparkFunction.getCodeFunctionImplementationInputFixedType(transformer.getOutConnRejectTypeName(), "Boolean", "org.apache.avro.specific.SpecificRecordBase"));
    stringBuffer.append(TEXT_58);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_59);
    stringBuffer.append(extractSparkFunction.getCodeFunctionReturnedType(transformer.getOutConnRejectTypeName()));
    stringBuffer.append(TEXT_60);
    stringBuffer.append(TEXT_61);
    stringBuffer.append(transformer.getOutConnRejectTypeName());
    stringBuffer.append(TEXT_62);
    stringBuffer.append(transformer.getOutConnRejectName());
    stringBuffer.append(TEXT_63);
    stringBuffer.append(transformer.getOutConnRejectTypeName());
    stringBuffer.append(TEXT_64);
    stringBuffer.append(extractSparkFunction.getCodeFunctionReturn(transformer.getOutConnRejectName(), transformer.getOutConnRejectTypeName()));
    stringBuffer.append(TEXT_65);
    
        } else {
            // Refactoring FunctionGenerator to java so we have to instaniate a MO or SO here
            functionGenerator = new SOFunctionGenerator(transformer, sparkFunction);

            functionGenerator.init(node, cid, null, transformer.getInConnName(), null,
                    transformer.getOutConnMainName() != null
                        ? transformer.getOutConnMainName()
                                : transformer.getOutConnRejectName());
        }
        functionGenerator.generate();
    }

    public void generateSparkConfig(final org.talend.designer.common.TransformerBase transformer, final org.talend.designer.spark.generator.SparkFunction sparkFunction) {
        if (transformer.isUnnecessary()) {
            
    stringBuffer.append(TEXT_66);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_67);
    
            return;
        }

        if (transformer.isMultiOutput()) {
            String localFunctionType = "mapToPair";
            if ((sparkFunction instanceof org.talend.designer.spark.generator.FlatMapFunction)
                    || (sparkFunction instanceof org.talend.designer.spark.generator.FlatMapToPairFunction)) {
                localFunctionType = "flatMapToPair";
            }

            String extractFunctionType = "mapToPair";
            if ((sparkFunction instanceof org.talend.designer.spark.generator.FlatMapFunction)
                    || (sparkFunction instanceof org.talend.designer.spark.generator.MapFunction)) {
                extractFunctionType = "map";
            }
            
    stringBuffer.append(TEXT_68);
    stringBuffer.append(TEXT_69);
    stringBuffer.append(sparkFunction.isStreaming() ?"org.apache.spark.streaming.api.java.JavaPairDStream":"org.apache.spark.api.java.JavaPairRDD");
    stringBuffer.append(TEXT_70);
    stringBuffer.append(transformer.getOutConnMainName());
    stringBuffer.append(TEXT_71);
    stringBuffer.append(transformer.getInConnName());
    stringBuffer.append(TEXT_72);
    stringBuffer.append(localFunctionType);
    stringBuffer.append(TEXT_73);
    stringBuffer.append(sparkFunction.getClassName(cid));
    stringBuffer.append(TEXT_74);
    stringBuffer.append(TEXT_75);
    stringBuffer.append(sparkFunction.getConfigReturnedType(transformer.getOutConnMainTypeName()));
    stringBuffer.append(TEXT_76);
    stringBuffer.append(transformer.getOutConnMainName());
    stringBuffer.append(TEXT_77);
    stringBuffer.append(transformer.getOutConnMainName());
    stringBuffer.append(TEXT_78);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_79);
    stringBuffer.append(extractFunctionType);
    stringBuffer.append(TEXT_80);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_81);
    stringBuffer.append(TEXT_82);
    stringBuffer.append(sparkFunction.getConfigReturnedType(transformer.getOutConnRejectTypeName()));
    stringBuffer.append(TEXT_83);
    stringBuffer.append(transformer.getOutConnRejectName());
    stringBuffer.append(TEXT_84);
    stringBuffer.append(transformer.getOutConnMainName());
    stringBuffer.append(TEXT_85);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_86);
    stringBuffer.append(extractFunctionType);
    stringBuffer.append(TEXT_87);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_88);
    
        } else {
            functionGenerator = new SOFunctionGenerator(transformer, sparkFunction);

            functionGenerator.init(node, cid, null, transformer.getInConnName(), null,
                    transformer.getOutConnMainName() != null
                        ? transformer.getOutConnMainName()
                                : transformer.getOutConnRejectName());
            
    stringBuffer.append(TEXT_89);
    stringBuffer.append(sparkFunction.getConfigReturnedType(transformer.getOutConnMainName() != null ? transformer.getOutConnMainTypeName() : transformer.getOutConnRejectTypeName()));
    stringBuffer.append(TEXT_90);
    stringBuffer.append(transformer.getOutConnMainName() != null ? transformer.getOutConnMainName() : transformer.getOutConnRejectName());
    stringBuffer.append(TEXT_91);
    stringBuffer.append(transformer.getInConnName());
    stringBuffer.append(TEXT_92);
    stringBuffer.append(sparkFunction.getConfigTransformation());
    stringBuffer.append(TEXT_93);
    stringBuffer.append(sparkFunction.getClassName(cid));
    stringBuffer.append(TEXT_94);
    
        }
    }
}


    stringBuffer.append(TEXT_95);
    

/**
 * Contains common processing for tVerifyEmail code generation.  This is
 * used in MapReduce and Storm components.
 *
 * The following imports must occur before importing this file:
 * @ include file="@{org.talend.designer.components.mrprovider}/resources/utils/common_codegen_util.javajet"
 */
class TVerifyEmailUtil extends org.talend.designer.common.TransformerBase {

    boolean isUseRegularRegex = ("true").equals(ElementParameterParser.getValue(node, "__USE_REGULAR_EXPRESSION__"));
    String userDefinedRegex =  ElementParameterParser.getValue(node, "__COMPLETE_REGEX__");
    boolean isCaseSensitive = ("true").equals(ElementParameterParser.getValue(node, "__CASE_SENSITIVE__"));

    boolean isLocalPartShort = ("true").equals(ElementParameterParser.getValue(node, "__USE_SHORT__"));
    String localPartShortExpress= ElementParameterParser.getValue(node, "__LOCAL_SHORT_PART__");

    boolean isLocalPartUseRegex = ("true").equals(ElementParameterParser.getValue(node, "__USE_REGEX__"));
    String localPartRegexExpress= ElementParameterParser.getValue(node, "__LOCAL_REGEX_PART__");

    String nFirstOfFirst = ElementParameterParser.getValue(node, "__COLUMN_NFIRSTCHAR_FIRSTNAME__");
    String nLastOfFirst = ElementParameterParser.getValue(node, "__COLUMN_NLASTCHAR_FIRSTNAME__");
    String nFirstOfLast = ElementParameterParser.getValue(node, "__COLUMN_NFIRSTCHAR_LASTNAME__");
    String nLastOfLast = ElementParameterParser.getValue(node, "__COLUMN_NLASTCHAR_LASTNAME__");
    String separator = ElementParameterParser.getValue(node, "__COLUMN_SEPARATOR__");

    String useCharCase=ElementParameterParser.getValue(node, "__COLUMN_CASE_TYPE__");

    boolean isValidTLDs = ("true").equals(ElementParameterParser.getValue(node, "__CHECK_TLDS__"));
    boolean isBlackListDomains = ("true").equals(ElementParameterParser.getValue(node, "__CHECK_BLACK_DOMAINS__"));
    boolean isCallbackMailServer = ("true").equals(ElementParameterParser.getValue(node, "__CALL_BACK__"));

    //Additional TLDS,white or black domain List 
    java.util.List<java.util.Map<String, String>> tldTable = (java.util.List<java.util.Map<String, String>>)ElementParameterParser.getObjectValue(node, "__ADDITIONAL_TLDS__");
    java.util.List<java.util.Map<String, String>> domianTable = (java.util.List<java.util.Map<String, String>>)ElementParameterParser.getObjectValue(node, "__DOMAINS_LIST__");
    
    private String columnSource = ElementParameterParser.getValue(node, "__EMAIL_COLUMN__");
    boolean isLocalPartUseColumn = ("true").equals(ElementParameterParser.getValue(node, "__USE_COLUMN__"));
    private String columnFirstName = ElementParameterParser.getValue(node, "__COLUMN_FIRSTNAME__");
    private String columnLastName = ElementParameterParser.getValue(node, "__COLUMN_LASTNAME__");
    
    java.util.List<? extends IConnection> outConns = node.getOutgoingSortedConnections();

    /** The list of columns that should be copied directly from the input to
     *  the output schema (where they have the same column names). */
    final private Iterable<IMetadataColumn> copiedInColumns;

    /** Columns in the output schema that are not copied directly from the
     *  input schema (excluding reject fields). */
    final private java.util.List<IMetadataColumn> newOutColumns;

    public TVerifyEmailUtil(INode node,
            org.talend.designer.common.BigDataCodeGeneratorArgument argument,
            org.talend.designer.common.CommonRowTransformUtil rowTransformUtil) {
        super(node, argument, rowTransformUtil, "FLOW", "REJECT");

        if (null != getInConn() && null != getOutConnMain()) {
            copiedInColumns = getColumnsUnion(getInColumns(), getOutColumnsMain());
            newOutColumns = getColumnsDiff(getOutColumnsMain(), getInColumns());
        } else {
            copiedInColumns = null;
            newOutColumns = null;
        }
    }

    public void generateTransformContextDeclaration() {
        
    stringBuffer.append(TEXT_96);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_97);
    
    }

    public void generateTransformContextInitialization(){
        
    stringBuffer.append(TEXT_98);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_99);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_100);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_101);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_102);
    
            if(tldTable!=null){
               for (java.util.Map<String, String> tldMap : tldTable) {
                   String tld = tldMap.get("TLD_NAME");
                   if(tld!=null){
             
    stringBuffer.append(TEXT_103);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_104);
    stringBuffer.append(tld.toUpperCase());
    stringBuffer.append(TEXT_105);
              
                   }
               }
            }

            if(domianTable!=null){
               for (java.util.Map<String, String> domainMap : domianTable) {
                   String domain = domainMap.get("DOMAIN_NAME");
                   if(domain!=null){
             
    stringBuffer.append(TEXT_106);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_107);
    stringBuffer.append(domain);
    stringBuffer.append(TEXT_108);
    
                   }
               }
            }
            
    stringBuffer.append(TEXT_109);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_110);
    stringBuffer.append(isLocalPartShort ? localPartShortExpress : localPartRegexExpress);
    stringBuffer.append(TEXT_111);
    stringBuffer.append(isLocalPartUseColumn);
    stringBuffer.append(TEXT_112);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_113);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_114);
    stringBuffer.append(isUseRegularRegex);
    stringBuffer.append(TEXT_115);
    stringBuffer.append(userDefinedRegex);
    stringBuffer.append(TEXT_116);
    stringBuffer.append(isBlackListDomains);
    stringBuffer.append(TEXT_117);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_118);
    stringBuffer.append(isValidTLDs);
    stringBuffer.append(TEXT_119);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_120);
    stringBuffer.append(isBlackListDomains);
    stringBuffer.append(TEXT_121);
    stringBuffer.append(isCaseSensitive);
    stringBuffer.append(TEXT_122);
    stringBuffer.append(useCharCase);
    stringBuffer.append(TEXT_123);
    stringBuffer.append(nFirstOfFirst);
    stringBuffer.append(TEXT_124);
    stringBuffer.append(nLastOfFirst);
    stringBuffer.append(TEXT_125);
    stringBuffer.append(nFirstOfLast);
    stringBuffer.append(TEXT_126);
    stringBuffer.append(nLastOfLast);
    stringBuffer.append(TEXT_127);
    stringBuffer.append(separator);
    stringBuffer.append(TEXT_128);
    stringBuffer.append(isCallbackMailServer);
    stringBuffer.append(TEXT_129);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_130);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_131);
    stringBuffer.append(isUseRegularRegex);
    stringBuffer.append(TEXT_132);
    stringBuffer.append(userDefinedRegex);
    stringBuffer.append(TEXT_133);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_134);
    stringBuffer.append(isCaseSensitive);
    stringBuffer.append(TEXT_135);
    stringBuffer.append(isLocalPartShort);
    stringBuffer.append(TEXT_136);
    stringBuffer.append(isBlackListDomains);
    stringBuffer.append(TEXT_137);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_138);
    stringBuffer.append(isValidTLDs);
    stringBuffer.append(TEXT_139);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_140);
    stringBuffer.append(isBlackListDomains);
    stringBuffer.append(TEXT_141);
    stringBuffer.append(isCallbackMailServer);
    stringBuffer.append(TEXT_142);
    
    }

    public void generateTransform() {
        generateTransform(false);
    }

    /**
     * Generates code that performs the tranformation from one input string
     * to many output strings, or to a reject flow.
     */
    public void generateTransform(boolean hasAReturnedType) {
          
    stringBuffer.append(TEXT_143);
    stringBuffer.append(getOutConnMainName());
    stringBuffer.append(TEXT_144);
    stringBuffer.append(getOutConnMainTypeName());
    stringBuffer.append(TEXT_145);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_146);
    stringBuffer.append(getInConnName());
    stringBuffer.append(TEXT_147);
    stringBuffer.append(columnSource );
    stringBuffer.append(TEXT_148);
    if(isLocalPartUseColumn){ //use column content (first and last name)
              
    stringBuffer.append(TEXT_149);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_150);
    stringBuffer.append(getInConnName());
    stringBuffer.append(TEXT_151);
    stringBuffer.append(columnFirstName );
    stringBuffer.append(TEXT_152);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_153);
    stringBuffer.append(getInConnName());
    stringBuffer.append(TEXT_154);
    stringBuffer.append(columnLastName );
    stringBuffer.append(TEXT_155);
    stringBuffer.append(TEXT_156);
    stringBuffer.append(getOutConnMainName());
    stringBuffer.append(TEXT_157);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_158);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_159);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_160);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_161);
    stringBuffer.append(TEXT_162);
    stringBuffer.append(getOutConnMainName());
    stringBuffer.append(TEXT_163);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_164);
    }else{
    stringBuffer.append(TEXT_165);
    stringBuffer.append(getOutConnMainName());
    stringBuffer.append(TEXT_166);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_167);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_168);
    }
    stringBuffer.append(TEXT_169);
    stringBuffer.append(TEXT_170);
    stringBuffer.append(getRowTransform().getCodeToCopyFields(false, copiedInColumns));
    stringBuffer.append(TEXT_171);
    stringBuffer.append(TEXT_172);
    stringBuffer.append(getRowTransform().getCodeToEmit(false));
    stringBuffer.append(TEXT_173);
    stringBuffer.append(TEXT_174);
    stringBuffer.append(getRowTransform().getCodeToInitOut(null == getOutConnMain(), newOutColumns));
    
    }
}

    
org.talend.designer.spark.generator.SparkFunction sparkFunction = null;
String requiredInputType = bigDataNode.getRequiredInputType();
String requiredOutputType = bigDataNode.getRequiredOutputType();
String incomingType = bigDataNode.getIncomingType();
String outgoingType = bigDataNode.getOutgoingType();
boolean inputIsPair = requiredInputType != null ? "KEYVALUE".equals(requiredInputType) : "KEYVALUE".equals(incomingType);

String type = requiredOutputType == null ? outgoingType : requiredOutputType;
if("KEYVALUE".equals(type)) {
 sparkFunction = new org.talend.designer.spark.generator.FlatMapToPairFunction(inputIsPair, codeGenArgument.getSparkVersion());
} else if("VALUE".equals(type)) {
 sparkFunction = new org.talend.designer.spark.generator.FlatMapFunction(inputIsPair, codeGenArgument.getSparkVersion());
}

boolean isNodeInBatchMode = org.talend.designer.common.tmap.LookupUtil.isNodeInBatchMode(node);
if("SPARKSTREAMING".equals(node.getComponent().getType()) && !isNodeInBatchMode) {
 sparkFunction.setStreaming(true);
}

final SparkRowTransformUtil sparkTransformUtil = new SparkRowTransformUtil(sparkFunction);
final TVerifyEmailUtil tVerifyEmailUtil = new TVerifyEmailUtil(
     node, codeGenArgument, sparkTransformUtil);

sparkTransformUtil.generateSparkConfig(tVerifyEmailUtil, sparkFunction);

    stringBuffer.append(TEXT_175);
    stringBuffer.append(TEXT_176);
    return stringBuffer.toString();
  }
}
