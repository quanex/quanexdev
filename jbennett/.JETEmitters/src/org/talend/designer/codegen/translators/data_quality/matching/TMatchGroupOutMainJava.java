package org.talend.designer.codegen.translators.data_quality.matching;

import org.talend.core.model.process.INode;
import org.talend.core.model.process.ElementParameterParser;
import org.talend.core.model.process.EConnectionType;
import org.talend.core.model.metadata.IMetadataTable;
import org.talend.core.model.metadata.IMetadataColumn;
import org.talend.core.model.process.IConnection;
import org.talend.designer.codegen.config.CodeGeneratorArgument;
import java.util.List;
import java.util.Map;

public class TMatchGroupOutMainJava
{
  protected static String nl;
  public static synchronized TMatchGroupOutMainJava create(String lineSeparator)
  {
    nl = lineSeparator;
    TMatchGroupOutMainJava result = new TMatchGroupOutMainJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "  ";
  protected final String TEXT_2 = "Struct ";
  protected final String TEXT_3 = "_HashRow = new ";
  protected final String TEXT_4 = "Struct();";
  protected final String TEXT_5 = NL + "        ";
  protected final String TEXT_6 = NL + "      ";
  protected final String TEXT_7 = "_HashRow.";
  protected final String TEXT_8 = " =  ";
  protected final String TEXT_9 = ".";
  protected final String TEXT_10 = ";";
  protected final String TEXT_11 = NL + "    ";
  protected final String TEXT_12 = "_2Struct ";
  protected final String TEXT_13 = "_2_HashRow = new ";
  protected final String TEXT_14 = "_2Struct();";
  protected final String TEXT_15 = NL + "        ";
  protected final String TEXT_16 = "_2_HashRow.";
  protected final String TEXT_17 = " =  ";
  protected final String TEXT_18 = ".";
  protected final String TEXT_19 = ";";
  protected final String TEXT_20 = NL;
  protected final String TEXT_21 = "        " + NL + "        blockRows_";
  protected final String TEXT_22 = ".add(";
  protected final String TEXT_23 = "_2_HashRow.getArrays());";
  protected final String TEXT_24 = NL + NL + "        blockRows_";
  protected final String TEXT_25 = ".put(";
  protected final String TEXT_26 = "_2_HashRow,null);";
  protected final String TEXT_27 = NL + "     if(";
  protected final String TEXT_28 = ".MASTER){";
  protected final String TEXT_29 = NL + "        ";
  protected final String TEXT_30 = "_master =";
  protected final String TEXT_31 = "_HashRow;" + NL + "     }";
  protected final String TEXT_32 = NL + "   ((org.talend.designer.components.lookup.memory.SwooshMultipassAdvaluedMemoryLookup<";
  protected final String TEXT_33 = "Struct>)tHash_Lookup_";
  protected final String TEXT_34 = ").put(";
  protected final String TEXT_35 = "_master,";
  protected final String TEXT_36 = "_HashRow);";
  protected final String TEXT_37 = NL + "tHash_Lookup_";
  protected final String TEXT_38 = ".put(";
  protected final String TEXT_39 = "_HashRow);";

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
     
CodeGeneratorArgument codeGenArgument = (CodeGeneratorArgument) argument;
INode node = (INode)codeGenArgument.getArgument();
String cid = node.getUniqueName();
cid = cid.replace("_GroupOut", "");

// incoming connection
List<? extends IConnection> inConns = node.getIncomingConnections(EConnectionType.FLOW_MAIN);
IConnection inConn = null;
String connNameMain = null;
 
if (inConns == null || inConns.size() == 0){
  return "";
} else{
  inConn = inConns.get(0);
  connNameMain = inConn.getName();
}
// get previous node name
INode preNode= inConn.getSource();
String preNodeName=preNode.getComponent().getName();

//used only for double click, which will add tJavaRow before any tmatchgroup
List<? extends IConnection> inConns2=preNode.getIncomingConnections();
String ppreNodeName = "";
if(inConns2!=null && inConns2.size()>0){
    INode ppreNode = preNode.getIncomingConnections().get(0).getSource();
    ppreNodeName = ppreNode.getComponent().getName();
}


IMetadataTable table = inConn.getMetadataTable();
boolean bSortOnDisk = "true".equals(ElementParameterParser.getValue(node, "__STORE_ON_DISK__")); 
boolean bMultiPass="true".equals(ElementParameterParser.getValue(node, "__LINK_WITH_PREVIOUS__"))&&(preNodeName.startsWith("tMatchGroup")||(preNodeName.equals("tJavaRow") && ppreNodeName.startsWith("tMatchGroup")));
boolean isSwoosh = !"Simple VSR Matcher".equals(ElementParameterParser.getValue(node, "__MATCHING_ALGORITHM__"));

List<Map<String, String>> listBlocking = (List<Map<String,String>>)ElementParameterParser.getObjectValue(node, "__BLOCKING_DEFINITION__");
//tMatchGroupOut->tMatchGroupIn so first node must have only one outputConnection
List<? extends IConnection> outConns = node.getOutgoingSortedConnections().get(0).getTarget().getOutgoingSortedConnections();
if(outConns==null || outConns.size()==0){
    return "";
}

    stringBuffer.append(TEXT_1);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_2);
    stringBuffer.append(connNameMain);
    stringBuffer.append(TEXT_3);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_4);
     

    for (IMetadataColumn column : table.getListColumns()){
        String sColumnName = column.getLabel();
               
    stringBuffer.append(TEXT_5);
    stringBuffer.append(TEXT_6);
    stringBuffer.append(connNameMain);
    stringBuffer.append(TEXT_7);
    stringBuffer.append(sColumnName);
    stringBuffer.append(TEXT_8);
    stringBuffer.append(connNameMain);
    stringBuffer.append(TEXT_9);
    stringBuffer.append(sColumnName);
    stringBuffer.append(TEXT_10);
    
    }
if (listBlocking != null && listBlocking.size() > 0){
    //MOD 20130325 TDQ-1359 yyin
    
    stringBuffer.append(TEXT_11);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_12);
    stringBuffer.append(connNameMain);
    stringBuffer.append(TEXT_13);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_14);
    
   
    for (Map<String, String> blocking : listBlocking){
        String sColumnName = blocking.get("INPUT_COLUMN");
        
    stringBuffer.append(TEXT_15);
    stringBuffer.append(connNameMain);
    stringBuffer.append(TEXT_16);
    stringBuffer.append(sColumnName);
    stringBuffer.append(TEXT_17);
    stringBuffer.append(connNameMain);
    stringBuffer.append(TEXT_18);
    stringBuffer.append(sColumnName);
    stringBuffer.append(TEXT_19);
    
    }//~20130325

    stringBuffer.append(TEXT_20);
    
    if (bSortOnDisk){

    stringBuffer.append(TEXT_21);
    stringBuffer.append(connNameMain);
    stringBuffer.append(TEXT_22);
    stringBuffer.append(connNameMain);
    stringBuffer.append(TEXT_23);
      }else{
    stringBuffer.append(TEXT_24);
    stringBuffer.append(connNameMain);
    stringBuffer.append(TEXT_25);
    stringBuffer.append(connNameMain);
    stringBuffer.append(TEXT_26);
    
    }
}
   if(isSwoosh && bMultiPass && (preNodeName.startsWith("tMatchGroup")||ppreNodeName.startsWith("tMatchGroup"))){
    stringBuffer.append(TEXT_27);
    stringBuffer.append(connNameMain);
    stringBuffer.append(TEXT_28);
    stringBuffer.append(TEXT_29);
    stringBuffer.append(connNameMain);
    stringBuffer.append(TEXT_30);
    stringBuffer.append(connNameMain);
    stringBuffer.append(TEXT_31);
     }        

if(isSwoosh && bMultiPass && !bSortOnDisk){

    stringBuffer.append(TEXT_32);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_33);
    stringBuffer.append(connNameMain);
    stringBuffer.append(TEXT_34);
    stringBuffer.append(connNameMain);
    stringBuffer.append(TEXT_35);
    stringBuffer.append(connNameMain);
    stringBuffer.append(TEXT_36);
    
}else{

    stringBuffer.append(TEXT_37);
    stringBuffer.append(connNameMain);
    stringBuffer.append(TEXT_38);
    stringBuffer.append(connNameMain);
    stringBuffer.append(TEXT_39);
    
}

    return stringBuffer.toString();
  }
}
