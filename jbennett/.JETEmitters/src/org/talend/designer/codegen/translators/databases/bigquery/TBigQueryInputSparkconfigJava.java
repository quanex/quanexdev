package org.talend.designer.codegen.translators.databases.bigquery;

import org.talend.core.model.process.IConnection;
import org.talend.core.model.process.INode;
import org.talend.designer.common.BigDataCodeGeneratorArgument;
import org.talend.designer.common.tsqlrow.TSqlRowUtil;
import org.talend.core.model.process.ElementParameterParser;
import org.talend.designer.spark.generator.storage.MongoDBSparkStorage;

public class TBigQueryInputSparkconfigJava
{
  protected static String nl;
  public static synchronized TBigQueryInputSparkconfigJava create(String lineSeparator)
  {
    nl = lineSeparator;
    TBigQueryInputSparkconfigJava result = new TBigQueryInputSparkconfigJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "\tif(true) {" + NL + "\t\tthrow new RuntimeException(\"";
  protected final String TEXT_2 = " component needs a tBigQueryConfiguration to be defined within the job.\");\t" + NL + "\t}" + NL + "\torg.apache.spark.api.java.JavaRDD<";
  protected final String TEXT_3 = "> rdd_";
  protected final String TEXT_4 = " = null;\t";
  protected final String TEXT_5 = NL + NL + "\t";
  protected final String TEXT_6 = NL + "    \torg.apache.hadoop.conf.Configuration config_";
  protected final String TEXT_7 = " = ctx.sparkContext().hadoopConfiguration();";
  protected final String TEXT_8 = NL + "    \torg.apache.hadoop.conf.Configuration config_";
  protected final String TEXT_9 = " = ctx.hadoopConfiguration();";
  protected final String TEXT_10 = NL + "    " + NL + "    config_";
  protected final String TEXT_11 = ".set(com.google.cloud.hadoop.io.bigquery.BigQueryConfiguration.TEMP_GCS_PATH_KEY, ";
  protected final String TEXT_12 = " + java.util.UUID.randomUUID());" + NL + "    ";
  protected final String TEXT_13 = NL + "\t    ";
  protected final String TEXT_14 = NL + NL + "class BigQueryUtil_";
  protected final String TEXT_15 = " {" + NL + "\tString projectId;" + NL + "\tcom.google.api.services.bigquery.Bigquery bigqueryclient = null;" + NL + "\tString tokenFile;" + NL + "\tboolean useLargeResult = false;" + NL + "\tString tempDataset;" + NL + "\tString tempTable;" + NL + "\t" + NL + "\tpublic BigQueryUtil_";
  protected final String TEXT_16 = "(String projectId, com.google.api.services.bigquery.Bigquery bigqueryclient, String tokenFile) {" + NL + "\t\tthis.projectId = projectId;" + NL + "\t\tthis.bigqueryclient = bigqueryclient;" + NL + "\t\tthis.tokenFile = tokenFile;" + NL + "\t}" + NL + "\t" + NL + "\tprivate String genTempName(String prefix){" + NL + "\t\treturn \"temp_\" + prefix + java.util.UUID.randomUUID().toString().replaceAll(\"-\", \"\") + \"";
  protected final String TEXT_17 = "\".toLowerCase().replaceAll(\"[^a-z0-9]\", \"0\").replaceAll(\"^[^a-z]\", \"a\") + Integer.toHexString(java.util.concurrent.ThreadLocalRandom.current().nextInt());" + NL + "\t}" + NL + "\t" + NL + "\tpublic void cleanup() throws Exception{" + NL + "\t\tif(useLargeResult){" + NL + "\t\t\tbigqueryclient.tables().delete(projectId, tempDataset, tempTable).execute();" + NL + "\t\t\tbigqueryclient.datasets().delete(projectId, tempDataset).execute();" + NL + "\t\t}" + NL + "\t}" + NL + "\t" + NL + "\tprivate String getLocation(com.google.api.services.bigquery.model.JobConfigurationQuery queryConfig) throws Exception {" + NL + "\t\tString location = null;" + NL + "\t\tcom.google.api.services.bigquery.model.JobConfiguration config = new com.google.api.services.bigquery.model.JobConfiguration();" + NL + "\t\tconfig.setQuery(queryConfig);" + NL + "\t\tconfig.setDryRun(true);" + NL + "\t\tcom.google.api.services.bigquery.model.Job job = new com.google.api.services.bigquery.model.Job();" + NL + "\t\tjob.setConfiguration(config);" + NL + "\t\tList<com.google.api.services.bigquery.model.TableReference> referencedTables = bigqueryclient.jobs().insert(projectId, job).execute().getStatistics().getQuery().getReferencedTables();" + NL + "\t\tif(referencedTables != null && !referencedTables.isEmpty()) {" + NL + "\t\t\tlocation = bigqueryclient.tables().get(projectId, referencedTables.get(0).getDatasetId(), referencedTables.get(0).getTableId()).execute().getLocation();" + NL + "\t\t}" + NL + "\t\treturn location;" + NL + "\t}" + NL + "\t" + NL + "\tprivate void createDataset(String location) throws Exception {" + NL + "\t\tcom.google.api.services.bigquery.model.Dataset dataset = new com.google.api.services.bigquery.model.Dataset().setDatasetReference(new com.google.api.services.bigquery.model.DatasetReference().setProjectId(projectId).setDatasetId(tempDataset));" + NL + "\t\tif(location != null) {" + NL + "\t\t\tdataset.setLocation(location);" + NL + "\t\t}" + NL + "\t\tString description = \"Dataset for BigQuery query job temporary table\";" + NL + "\t\tdataset.setFriendlyName(description);" + NL + "    \tdataset.setDescription(description);" + NL + "    \tbigqueryclient.datasets().insert(projectId, dataset).execute();" + NL + "\t}" + NL + "\t" + NL + "\tpublic com.google.api.services.bigquery.model.Job executeQuery(String query, boolean useLargeResult) throws Exception{" + NL + "\t\treturn executeQuery(query, useLargeResult, true);" + NL + "\t}" + NL + "\t" + NL + "\tpublic com.google.api.services.bigquery.model.Job executeQuery(String query, boolean useLargeResult, boolean useLegacySql) throws Exception{" + NL + "\t\tcom.google.api.services.bigquery.model.JobConfigurationQuery queryConfig = new com.google.api.services.bigquery.model.JobConfigurationQuery();" + NL + "\t\tqueryConfig.setQuery(query);" + NL + "\t\tqueryConfig.setUseLegacySql(useLegacySql);" + NL + "\t\tif(useLargeResult){" + NL + "\t\t\tthis.useLargeResult = true;" + NL + "\t\t\ttempDataset = genTempName(\"dataset\");" + NL + "\t\t\ttempTable = genTempName(\"table\");" + NL + "\t\t\tcreateDataset(getLocation(queryConfig));" + NL + "\t\t\tqueryConfig.setAllowLargeResults(true);" + NL + "\t\t\tqueryConfig.setDestinationTable(new com.google.api.services.bigquery.model.TableReference()" + NL + "\t\t\t\t\t\t\t\t\t\t\t.setProjectId(projectId)" + NL + "\t\t\t\t\t\t\t\t\t\t\t.setDatasetId(tempDataset)" + NL + "\t\t\t\t\t\t\t\t\t\t\t.setTableId(tempTable));" + NL + "\t\t}" + NL + "\t\t" + NL + "\t\tcom.google.api.services.bigquery.model.JobConfiguration config = new com.google.api.services.bigquery.model.JobConfiguration();" + NL + "\t\tconfig.setQuery(queryConfig);" + NL + "\t\t" + NL + "\t\tcom.google.api.services.bigquery.model.Job job = new com.google.api.services.bigquery.model.Job();" + NL + "\t\tjob.setConfiguration(config);" + NL + "\t\t" + NL + "\t\tcom.google.api.services.bigquery.model.Job insert = null;" + NL + "\t\tcom.google.api.services.bigquery.model.JobReference jobId = null;" + NL + "\t\ttry {" + NL + "\t\t\tinsert = bigqueryclient.jobs().insert(projectId, job).execute();" + NL + "\t\t\tjobId = insert.getJobReference();" + NL + "\t\t} catch (com.google.api.client.googleapis.json.GoogleJsonResponseException e) {" + NL + "\t\t\tif(tokenFile != null){" + NL + "\t\t\t\ttry {" + NL + "\t\t\t\t\tjava.io.File f = new java.io.File(tokenFile);" + NL + "\t\t\t\t\tboolean isRemoved = f.delete();" + NL + "\t\t\t\t\tif(isRemoved){" + NL + "\t\t\t\t\t\t";
  protected final String TEXT_18 = NL + "\t\t\t\t\t\t\tlog.error(\"";
  protected final String TEXT_19 = " - Unable to connect. This might come from the token expiration. Execute again the job with an empty authorization code.\");" + NL + "\t\t\t\t\t\t";
  protected final String TEXT_20 = NL + "\t\t\t\t\t\t\tSystem.err.println(\"---> Unable to connect. This might come from the token expiration. Execute again the job with an empty authorization code.\");" + NL + "\t\t\t\t\t\t";
  protected final String TEXT_21 = NL + "\t\t\t\t\t}else{" + NL + "\t\t\t\t\t\tthrow new java.lang.Exception();" + NL + "\t\t\t\t\t}" + NL + "\t\t\t\t} catch (java.lang.Exception ee) {" + NL + "\t\t\t\t\t";
  protected final String TEXT_22 = NL + "\t\t\t\t\t\tlog.error(\"";
  protected final String TEXT_23 = " - Unable to connect. This might come from the token expiration. Remove the file \" + tokenFile + \" Execute again the job with an empty authorization code.\");" + NL + "\t\t\t\t\t";
  protected final String TEXT_24 = NL + "\t\t\t\t\t\tSystem.err.println(\"---> Unable to connect. This might come from the token expiration. Remove the file \" + tokenFile + \" Execute again the job with an empty authorization code.\");" + NL + "\t\t\t\t\t";
  protected final String TEXT_25 = NL + "\t\t\t\t}" + NL + "\t\t\t}" + NL + "\t\t\tthrow e;" + NL + "\t\t}" + NL + "\t\t" + NL + "\t\t";
  protected final String TEXT_26 = NL + "\t\t\tlog.info(\"";
  protected final String TEXT_27 = " - Wait for query execution\");" + NL + "\t\t";
  protected final String TEXT_28 = NL + "\t\t// wait for query execution" + NL + "\t\twhile (true) {" + NL + "\t\t\tcom.google.api.services.bigquery.model.Job pollJob = bigqueryclient.jobs().get(projectId, jobId.getJobId()).execute();" + NL + "\t\t\tcom.google.api.services.bigquery.model.JobStatus status = pollJob.getStatus();" + NL + "\t\t\tif (status.getState().equals(\"DONE\")) {" + NL + "\t\t\t\tcom.google.api.services.bigquery.model.ErrorProto errorProto = status.getErrorResult();" + NL + "\t\t\t\tif(errorProto != null){// job failed, handle it" + NL + "\t\t\t\t\t";
  protected final String TEXT_29 = NL + "\t\t\t\t\t\tif(!useLargeResult && \"responseTooLarge\".equals(errorProto.getReason())){// try with large result flag" + NL + "\t\t\t\t\t\t\t";
  protected final String TEXT_30 = NL + "\t\t\t\t\t\t\t\tlog.info(\"";
  protected final String TEXT_31 = " - Try with allow large results flag\");" + NL + "\t\t\t\t\t\t\t";
  protected final String TEXT_32 = NL + "\t\t\t\t\t\t\treturn executeQuery(query, true);" + NL + "\t\t\t\t\t\t}" + NL + "\t\t\t\t\t";
  protected final String TEXT_33 = NL + "\t\t\t\t\t// Do not throw exception to avoid behavior changed(because it may throw \"duplicate\" exception which do not throw before);" + NL + "        \t\t\t";
  protected final String TEXT_34 = NL + "\t\t\t\t\t\tlog.error(\"";
  protected final String TEXT_35 = " - Reason: \" + errorProto.getReason() + \"\\nMessage: \" + errorProto.getMessage());" + NL + "\t\t\t\t\t";
  protected final String TEXT_36 = NL + "\t\t\t\t\t\tSystem.err.println(\"---> Reason: \" + errorProto.getReason() + \"\\nMessage: \" + errorProto.getMessage());" + NL + "\t\t\t\t\t";
  protected final String TEXT_37 = NL + "\t\t\t\t\t}// else job successful" + NL + "\t\t\t\t\tbreak;" + NL + "\t\t\t\t}" + NL + "\t\t\t\t// Pause execution for one second before polling job status again, to " + NL + "\t\t\t\t// reduce unnecessary calls to the BigQUery API and lower overall" + NL + "\t\t\t\t// application bandwidth." + NL + "\t\t\t\tThread.sleep(1000);" + NL + "\t\t\t}" + NL + "\t\t" + NL + "\t\t\treturn insert;" + NL + "\t\t}" + NL + " \t\t" + NL + "\t}" + NL + "\t    com.google.api.services.bigquery.Bigquery bigqueryclient_";
  protected final String TEXT_38 = " = com.google.cloud.hadoop.io.bigquery.BigQueryFactory.INSTANCE.getBigQuery(config_";
  protected final String TEXT_39 = ");" + NL + "\t    String projectId_";
  protected final String TEXT_40 = " = config_";
  protected final String TEXT_41 = ".get(\"mapred.bq.project.id\");" + NL + "\t    BigQueryUtil_";
  protected final String TEXT_42 = " bigQueryUtil_";
  protected final String TEXT_43 = " = new BigQueryUtil_";
  protected final String TEXT_44 = "(projectId_";
  protected final String TEXT_45 = ", bigqueryclient_";
  protected final String TEXT_46 = ", null);" + NL + "\t    com.google.api.services.bigquery.model.Job queryJob_";
  protected final String TEXT_47 = " = bigQueryUtil_";
  protected final String TEXT_48 = ".executeQuery(";
  protected final String TEXT_49 = ", true, ";
  protected final String TEXT_50 = ");" + NL + "    \tcom.google.cloud.hadoop.io.bigquery.BigQueryConfiguration.configureBigQueryInput(config_";
  protected final String TEXT_51 = ", " + NL + "    \t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tprojectId_";
  protected final String TEXT_52 = ", " + NL + "    \t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tqueryJob_";
  protected final String TEXT_53 = ".getConfiguration().getQuery()" + NL + "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t.getDestinationTable().getDatasetId()," + NL + "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tqueryJob_";
  protected final String TEXT_54 = ".getConfiguration().getQuery()" + NL + "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t.getDestinationTable().getTableId());";
  protected final String TEXT_55 = NL + "    \tcom.google.cloud.hadoop.io.bigquery.BigQueryConfiguration.configureBigQueryInput(config_";
  protected final String TEXT_56 = ", ";
  protected final String TEXT_57 = ", ";
  protected final String TEXT_58 = ", ";
  protected final String TEXT_59 = ");";
  protected final String TEXT_60 = NL + "\t\torg.apache.spark.api.java.JavaPairRDD<org.apache.hadoop.io.LongWritable, com.google.gson.JsonObject> gsonRDD_";
  protected final String TEXT_61 = " = ctx.sparkContext().newAPIHadoopRDD(config_";
  protected final String TEXT_62 = ", com.google.cloud.hadoop.io.bigquery.GsonBigQueryInputFormat.class, org.apache.hadoop.io.LongWritable.class, com.google.gson.JsonObject.class);";
  protected final String TEXT_63 = NL + "    \torg.apache.spark.api.java.JavaPairRDD<org.apache.hadoop.io.LongWritable, com.google.gson.JsonObject> gsonRDD_";
  protected final String TEXT_64 = " = ctx.newAPIHadoopRDD(config_";
  protected final String TEXT_65 = ", com.google.cloud.hadoop.io.bigquery.GsonBigQueryInputFormat.class, org.apache.hadoop.io.LongWritable.class, com.google.gson.JsonObject.class);" + NL + "\t";
  protected final String TEXT_66 = NL + "    org.apache.spark.api.java.JavaRDD<";
  protected final String TEXT_67 = "> rdd_";
  protected final String TEXT_68 = " = gsonRDD_";
  protected final String TEXT_69 = ".values().map(new ";
  protected final String TEXT_70 = "_FromGsonTo";
  protected final String TEXT_71 = "(job));";
  protected final String TEXT_72 = NL;

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    
BigDataCodeGeneratorArgument codeGenArgument = (BigDataCodeGeneratorArgument) argument;
INode node = (INode)codeGenArgument.getArgument();
String cid = node.getUniqueName();

// reuse tSqlRowUtil to get outStructName
TSqlRowUtil tSqlRowUtil = new TSqlRowUtil(node);
String validateError = tSqlRowUtil.validate(false, true);
if (validateError != null) {
    // Cause the job compilation to explicitly fail if there is a problem.
    return "throw new JobConfigurationError(\"" + validateError +"\");";
}

String outStructName = codeGenArgument.getRecordStructName(tSqlRowUtil.getOutgoingConnection());

String sourceType = ElementParameterParser.getValue(node,"__SOURCE_TYPE__");
String query = ElementParameterParser.getValue(node,"__QUERY__");
query = query.replaceAll("\n"," ");
query = query.replaceAll("\r"," ");

String project = ElementParameterParser.getValue(node,"__GCP_PROJECT_ID__");
String dataset = ElementParameterParser.getValue(node,"__BIGQUERY_DATASET__");
String table = ElementParameterParser.getValue(node,"__BIGQUERY_TABLE__");
boolean useLegacySql = ElementParameterParser.getBooleanValue(node,"__USE_LEGACY_SQL__");

java.util.List<? extends INode> configurationNodes = node.getProcess().getNodesOfType("tBigQueryConfiguration");
INode configurationNode = null;
if (configurationNodes != null && configurationNodes.size() > 0) {
    configurationNode = configurationNodes.get(0);
} else {

    stringBuffer.append(TEXT_1);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_2);
    stringBuffer.append(outStructName);
    stringBuffer.append(TEXT_3);
    stringBuffer.append(tSqlRowUtil.getOutgoingConnection().getName());
    stringBuffer.append(TEXT_4);
    
}
String bqGCSTempPath = ElementParameterParser.getValue(configurationNode, "__EXECUTE_GCP_STORAGE_TEMP_PATH__");

    stringBuffer.append(TEXT_5);
    
    if("SPARKSTREAMING".equals(node.getComponent().getType())){
    
    stringBuffer.append(TEXT_6);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_7);
    
    } else {
    
    stringBuffer.append(TEXT_8);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_9);
    
    }
    
    stringBuffer.append(TEXT_10);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_11);
    stringBuffer.append(bqGCSTempPath);
    stringBuffer.append(TEXT_12);
    
    if("QUERY".equals(sourceType)){
	    //start to create connection and execute query
	    boolean isLog4jEnabled = false;
	    String resultSizeType = "";
	    
    stringBuffer.append(TEXT_13);
    stringBuffer.append(TEXT_14);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_15);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_16);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_17);
    
				    	if(isLog4jEnabled){
						
    stringBuffer.append(TEXT_18);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_19);
    
						}else{
				    	
    stringBuffer.append(TEXT_20);
    
						}
						
    stringBuffer.append(TEXT_21);
    
			    	if(isLog4jEnabled){
					
    stringBuffer.append(TEXT_22);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_23);
    
					}else{
			    	
    stringBuffer.append(TEXT_24);
    
					}
					
    stringBuffer.append(TEXT_25);
    
		if(isLog4jEnabled){
		
    stringBuffer.append(TEXT_26);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_27);
    
		}
		
    stringBuffer.append(TEXT_28);
    if("AUTO".equals(resultSizeType)){
    stringBuffer.append(TEXT_29);
    
							if(isLog4jEnabled){
							
    stringBuffer.append(TEXT_30);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_31);
    
							}
							
    stringBuffer.append(TEXT_32);
    }
    stringBuffer.append(TEXT_33);
    
			    	if(isLog4jEnabled){
					
    stringBuffer.append(TEXT_34);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_35);
    
					}else{
			    	
    stringBuffer.append(TEXT_36);
    
					}
					
    stringBuffer.append(TEXT_37);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_38);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_39);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_40);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_41);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_42);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_43);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_44);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_45);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_46);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_47);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_48);
    stringBuffer.append(query);
    stringBuffer.append(TEXT_49);
    stringBuffer.append(useLegacySql);
    stringBuffer.append(TEXT_50);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_51);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_52);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_53);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_54);
    
    //end to create connection and execute query
    }else{ 
    
    stringBuffer.append(TEXT_55);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_56);
    stringBuffer.append(project);
    stringBuffer.append(TEXT_57);
    stringBuffer.append(dataset);
    stringBuffer.append(TEXT_58);
    stringBuffer.append(table);
    stringBuffer.append(TEXT_59);
    
    }
    
    
    if("SPARKSTREAMING".equals(node.getComponent().getType())){
    
    stringBuffer.append(TEXT_60);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_61);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_62);
    
    } else {
    
    stringBuffer.append(TEXT_63);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_64);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_65);
    
	}
	
    stringBuffer.append(TEXT_66);
    stringBuffer.append(outStructName);
    stringBuffer.append(TEXT_67);
    stringBuffer.append(tSqlRowUtil.getOutgoingConnection().getName());
    stringBuffer.append(TEXT_68);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_69);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_70);
    stringBuffer.append(outStructName);
    stringBuffer.append(TEXT_71);
    stringBuffer.append(TEXT_72);
    return stringBuffer.toString();
  }
}
