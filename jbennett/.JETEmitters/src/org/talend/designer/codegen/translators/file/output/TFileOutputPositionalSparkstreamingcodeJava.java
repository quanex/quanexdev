package org.talend.designer.codegen.translators.file.output;

import java.util.List;
import java.util.Map;
import org.talend.core.model.metadata.IMetadataColumn;
import org.talend.core.model.metadata.IMetadataTable;
import org.talend.core.model.metadata.types.JavaType;
import org.talend.core.model.metadata.types.JavaTypesManager;
import org.talend.core.model.process.EConnectionType;
import org.talend.core.model.process.ElementParameterParser;
import org.talend.core.model.process.IConnection;
import org.talend.core.model.process.INode;
import org.talend.designer.common.BigDataCodeGeneratorArgument;

public class TFileOutputPositionalSparkstreamingcodeJava
{
  protected static String nl;
  public static synchronized TFileOutputPositionalSparkstreamingcodeJava create(String lineSeparator)
  {
    nl = lineSeparator;
    TFileOutputPositionalSparkstreamingcodeJava result = new TFileOutputPositionalSparkstreamingcodeJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "";
  protected final String TEXT_2 = NL + NL + "        public static class ";
  protected final String TEXT_3 = "StructOutputFormat extends FileOutputFormat<NullWritable, ";
  protected final String TEXT_4 = "> {" + NL + "" + NL + "            private ContextProperties context;" + NL + "" + NL + "            protected static class HDFSRecordWriter implements RecordWriter<NullWritable, ";
  protected final String TEXT_5 = "> {" + NL + "        \t\tprotected DataOutputStream out;" + NL + "        \t\tprivate ContextProperties context;" + NL + "" + NL + "        \t\tpublic HDFSRecordWriter(DataOutputStream out, JobConf job) {" + NL + "        \t\t\tthis.out = out;" + NL + "        \t\t\tthis.context = new ContextProperties(job);" + NL + "        \t\t}" + NL + "" + NL + "        \t\tprivate void writeObject(";
  protected final String TEXT_6 = " value) throws IOException {" + NL + "        \t\t\tStringBuilder sb_";
  protected final String TEXT_7 = " = new StringBuilder();" + NL + "        \t\t\tString tempStringM";
  protected final String TEXT_8 = ";" + NL + "        \t\t\tint tempLengthM";
  protected final String TEXT_9 = " = 0;" + NL + "        \t\t\t";
  protected final String TEXT_10 = NL + "                            tempStringM";
  protected final String TEXT_11 = " = String.valueOf(value.";
  protected final String TEXT_12 = ");";
  protected final String TEXT_13 = NL + "                            if (value.";
  protected final String TEXT_14 = " != null) {";
  protected final String TEXT_15 = NL + "                                    tempStringM";
  protected final String TEXT_16 = " = FormatterUtils.format_Date(value.";
  protected final String TEXT_17 = ", ";
  protected final String TEXT_18 = ");";
  protected final String TEXT_19 = NL + "                                    tempStringM";
  protected final String TEXT_20 = " = FormatterUtils.format_DateInUTC(value.";
  protected final String TEXT_21 = ", ";
  protected final String TEXT_22 = ");";
  protected final String TEXT_23 = NL + "                                    tempStringM";
  protected final String TEXT_24 = " = java.nio.charset.Charset.defaultCharset().decode(java.nio.ByteBuffer.wrap(value.";
  protected final String TEXT_25 = ")).toString();";
  protected final String TEXT_26 = NL + "                                    tempStringM";
  protected final String TEXT_27 = " = value.";
  protected final String TEXT_28 = ";";
  protected final String TEXT_29 = NL + "                                        tempStringM";
  protected final String TEXT_30 = " = FormatterUtils.format_Number(";
  protected final String TEXT_31 = ".toPlainString(), ";
  protected final String TEXT_32 = ", ";
  protected final String TEXT_33 = ");";
  protected final String TEXT_34 = NL + "                                        tempStringM";
  protected final String TEXT_35 = " = FormatterUtils.format_Number(String.valueOf(value.";
  protected final String TEXT_36 = "), ";
  protected final String TEXT_37 = ", ";
  protected final String TEXT_38 = ");";
  protected final String TEXT_39 = NL + "                                    tempStringM";
  protected final String TEXT_40 = " = ";
  protected final String TEXT_41 = ".toPlainString();";
  protected final String TEXT_42 = NL + "                                    tempStringM";
  protected final String TEXT_43 = " = String.valueOf(value.";
  protected final String TEXT_44 = ");";
  protected final String TEXT_45 = NL + "                            } else {" + NL + "                                tempStringM";
  protected final String TEXT_46 = " = \"\";" + NL + "                            }";
  protected final String TEXT_47 = NL + "                            tempLengthM";
  protected final String TEXT_48 = "=tempStringM";
  protected final String TEXT_49 = ".getBytes(";
  protected final String TEXT_50 = ").length;";
  protected final String TEXT_51 = NL + "                            tempLengthM";
  protected final String TEXT_52 = "=tempStringM";
  protected final String TEXT_53 = ".length();";
  protected final String TEXT_54 = NL + NL + "                        if (tempLengthM";
  protected final String TEXT_55 = " >= ";
  protected final String TEXT_56 = ") {";
  protected final String TEXT_57 = NL + "                                sb_";
  protected final String TEXT_58 = ".append(tempStringM";
  protected final String TEXT_59 = ");";
  protected final String TEXT_60 = NL + "                                     byteArray_";
  protected final String TEXT_61 = "=arrays_";
  protected final String TEXT_62 = ".copyOfRange(tempStringM";
  protected final String TEXT_63 = ".getBytes(";
  protected final String TEXT_64 = "),tempLengthM";
  protected final String TEXT_65 = " - ";
  protected final String TEXT_66 = ",tempLengthM";
  protected final String TEXT_67 = ");" + NL + "                                     sb_";
  protected final String TEXT_68 = ".append(new String(byteArray_";
  protected final String TEXT_69 = ",";
  protected final String TEXT_70 = "));";
  protected final String TEXT_71 = NL + "                                     sb_";
  protected final String TEXT_72 = ".append(tempStringM";
  protected final String TEXT_73 = ".substring(tempLengthM";
  protected final String TEXT_74 = "-";
  protected final String TEXT_75 = "));";
  protected final String TEXT_76 = NL + "                                int begin";
  protected final String TEXT_77 = "=(tempLengthM";
  protected final String TEXT_78 = "-";
  protected final String TEXT_79 = ")/2;";
  protected final String TEXT_80 = NL + "                                    byteArray_";
  protected final String TEXT_81 = "=arrays_";
  protected final String TEXT_82 = ".copyOfRange(tempStringM";
  protected final String TEXT_83 = ".getBytes(";
  protected final String TEXT_84 = "),begin";
  protected final String TEXT_85 = ",begin";
  protected final String TEXT_86 = "+";
  protected final String TEXT_87 = ");" + NL + "                                    sb_";
  protected final String TEXT_88 = ".append(new String(byteArray_";
  protected final String TEXT_89 = ",";
  protected final String TEXT_90 = "));";
  protected final String TEXT_91 = NL + "                                    sb_";
  protected final String TEXT_92 = ".append(tempStringM";
  protected final String TEXT_93 = ".substring(begin";
  protected final String TEXT_94 = ", begin";
  protected final String TEXT_95 = "+";
  protected final String TEXT_96 = "));";
  protected final String TEXT_97 = NL + "                                    byteArray_";
  protected final String TEXT_98 = "=arrays_";
  protected final String TEXT_99 = ".copyOfRange(tempStringM";
  protected final String TEXT_100 = ".getBytes(";
  protected final String TEXT_101 = "),0,";
  protected final String TEXT_102 = ");" + NL + "                                    sb_";
  protected final String TEXT_103 = ".append(new String(byteArray_";
  protected final String TEXT_104 = ",";
  protected final String TEXT_105 = "));";
  protected final String TEXT_106 = NL + "                                    sb_";
  protected final String TEXT_107 = ".append(tempStringM";
  protected final String TEXT_108 = ".substring(0, ";
  protected final String TEXT_109 = "));";
  protected final String TEXT_110 = NL + "                        } else if (tempLengthM";
  protected final String TEXT_111 = "<";
  protected final String TEXT_112 = ") {";
  protected final String TEXT_113 = NL + "                                sb_";
  protected final String TEXT_114 = ".append(tempStringM";
  protected final String TEXT_115 = ");" + NL + "                                for(int i_";
  protected final String TEXT_116 = "=0; i_";
  protected final String TEXT_117 = "< ";
  protected final String TEXT_118 = "-tempLengthM";
  protected final String TEXT_119 = "; i_";
  protected final String TEXT_120 = "++){" + NL + "                                    sb_";
  protected final String TEXT_121 = ".append(";
  protected final String TEXT_122 = ");" + NL + "                                }";
  protected final String TEXT_123 = NL + "                                for(int i_";
  protected final String TEXT_124 = "=0; i_";
  protected final String TEXT_125 = "< ";
  protected final String TEXT_126 = "-tempLengthM";
  protected final String TEXT_127 = "; i_";
  protected final String TEXT_128 = "++){" + NL + "                                    sb_";
  protected final String TEXT_129 = ".append(";
  protected final String TEXT_130 = ");" + NL + "                                }" + NL + "                                sb_";
  protected final String TEXT_131 = ".append(tempStringM";
  protected final String TEXT_132 = ");";
  protected final String TEXT_133 = NL + "                                int temp";
  protected final String TEXT_134 = "= (";
  protected final String TEXT_135 = "-tempLengthM";
  protected final String TEXT_136 = ")/2;" + NL + "                                for(int i_";
  protected final String TEXT_137 = "=0;i_";
  protected final String TEXT_138 = "<temp";
  protected final String TEXT_139 = ";i_";
  protected final String TEXT_140 = "++){" + NL + "                                    sb_";
  protected final String TEXT_141 = ".append(";
  protected final String TEXT_142 = ");" + NL + "                                }" + NL + "                                sb_";
  protected final String TEXT_143 = ".append(tempStringM";
  protected final String TEXT_144 = ");" + NL + "                                for(int i=temp";
  protected final String TEXT_145 = "+tempLengthM";
  protected final String TEXT_146 = ";i<";
  protected final String TEXT_147 = ";i++){" + NL + "                                    sb_";
  protected final String TEXT_148 = ".append(";
  protected final String TEXT_149 = ");" + NL + "                                }";
  protected final String TEXT_150 = NL + "                        }" + NL + "                        //get  and format output String end" + NL;
  protected final String TEXT_151 = NL + "        \t\t\tthis.out.write(sb_";
  protected final String TEXT_152 = ".toString().getBytes(";
  protected final String TEXT_153 = "));" + NL + "        \t\t} // writeObject" + NL + "" + NL + "        \t\tpublic synchronized void write(NullWritable key, ";
  protected final String TEXT_154 = " value)" + NL + "        \t\t\t\tthrows IOException {" + NL + "" + NL + "        \t\t\tboolean nullValue = value == null;" + NL + "        \t\t\tif (nullValue) {" + NL + "        \t\t\t\treturn;" + NL + "        \t\t\t} else {" + NL + "        \t\t\t\twriteObject(value);" + NL + "        \t\t\t}" + NL + "        \t\t\tout.write(";
  protected final String TEXT_155 = ".getBytes(";
  protected final String TEXT_156 = "));" + NL + "        \t\t}" + NL + "" + NL + "        \t\tpublic synchronized void close(Reporter reporter) throws IOException {" + NL + "        \t\t\tout.close();" + NL + "        \t\t}" + NL + "        \t} // HDFS RecordReader" + NL + "" + NL + "        \tpublic RecordWriter<NullWritable, ";
  protected final String TEXT_157 = "> getRecordWriter(" + NL + "        \t\t\tFileSystem ignored, JobConf job, String name, Progressable progress) throws IOException{" + NL + "                this.context = new ContextProperties(job);" + NL + "" + NL + "" + NL + "        \t\t";
  protected final String TEXT_158 = NL + "        \t\t\tPath file = FileOutputFormat.getTaskOutputPath(job, name);" + NL + "        \t\t\tFileSystem fs = file.getFileSystem(job);" + NL + "        \t\t\tDataOutputStream fileOut = fs.create(file, progress);" + NL + "        \t\t\t";
  protected final String TEXT_159 = NL + "        \t\t\t\tCompressionCodec codec = ReflectionUtils.newInstance(org.apache.hadoop.io.compress.GzipCodec.class," + NL + "        \t\t\t\t\t\tjob);" + NL + "        \t\t\t\t";
  protected final String TEXT_160 = NL + "        \t\t\t\tCompressionCodec codec = ReflectionUtils.newInstance(org.apache.hadoop.io.compress.BZip2Codec.class," + NL + "        \t\t\t\t\t\tjob);" + NL + "        \t\t\t\t";
  protected final String TEXT_161 = NL + "        \t\t\t// build the filename including the extension" + NL + "        \t\t\tPath file = FileOutputFormat.getTaskOutputPath(job," + NL + "        \t\t\t\t\tname + codec.getDefaultExtension());" + NL + "        \t\t\tFileSystem fs = file.getFileSystem(job);" + NL + "        \t\t\tDataOutputStream fileOut = new DataOutputStream(" + NL + "        \t\t\t\t\tcodec.createOutputStream(fs.create(file, progress)));" + NL + "        \t\t\t";
  protected final String TEXT_162 = NL + "        \t\t\t" + NL + "        \t\t\tfileOut.write(\"";
  protected final String TEXT_163 = "\".getBytes(";
  protected final String TEXT_164 = "));" + NL + "        \t\t\tfileOut.write(";
  protected final String TEXT_165 = ".getBytes(";
  protected final String TEXT_166 = "));" + NL + "\t\t\t\t    ";
  protected final String TEXT_167 = NL + "        \t\t\tStringBuilder headerBuilder_";
  protected final String TEXT_168 = " = new StringBuilder();" + NL + "        \t\t";
  protected final String TEXT_169 = NL + "        \t\t\t\t\t\theaderBuilder_";
  protected final String TEXT_170 = ".append(\"";
  protected final String TEXT_171 = "\");" + NL + "        \t\t\t\t\t";
  protected final String TEXT_172 = NL + "        \t\t\t\t\t\theaderBuilder_";
  protected final String TEXT_173 = ".append(\"";
  protected final String TEXT_174 = "\");" + NL + "        \t\t\t\t\t";
  protected final String TEXT_175 = NL + "        \t\t\t\t\t\theaderBuilder_";
  protected final String TEXT_176 = ".append(\"";
  protected final String TEXT_177 = "\");" + NL + "        \t\t\t\t\t";
  protected final String TEXT_178 = NL + "        \t\t\t\t\t\theaderBuilder_";
  protected final String TEXT_179 = ".append(\"";
  protected final String TEXT_180 = "\");" + NL + "        \t\t\t\t\t";
  protected final String TEXT_181 = NL + "\t\t\t\t\t\t\t\theaderBuilder_";
  protected final String TEXT_182 = ".append(\"";
  protected final String TEXT_183 = "\");" + NL + "\t\t\t\t\t\t\t";
  protected final String TEXT_184 = NL + "\t\t\t\t\t\t\t\t\theaderBuilder_";
  protected final String TEXT_185 = ".append(";
  protected final String TEXT_186 = ");" + NL + "\t\t\t\t\t\t\t\t";
  protected final String TEXT_187 = NL + "\t\t\t\t\t\t\t\t\theaderBuilder_";
  protected final String TEXT_188 = ".append(";
  protected final String TEXT_189 = ");" + NL + "\t\t\t\t\t\t\t\t";
  protected final String TEXT_190 = NL + "\t\t\t\t\t\t\t\theaderBuilder_";
  protected final String TEXT_191 = ".append(\"";
  protected final String TEXT_192 = "\");" + NL + "\t\t\t\t\t\t\t\t";
  protected final String TEXT_193 = NL + "\t\t\t\t\t\t\t\t\theaderBuilder_";
  protected final String TEXT_194 = ".append(";
  protected final String TEXT_195 = ");" + NL + "\t\t\t\t\t\t\t\t";
  protected final String TEXT_196 = NL + "\t\t\t\t\t\t\t\theaderBuilder_";
  protected final String TEXT_197 = ".append(\"";
  protected final String TEXT_198 = "\");" + NL + "\t\t\t\t\t\t\t\t";
  protected final String TEXT_199 = NL + "\t\t\t\t\t\t\t\t\theaderBuilder_";
  protected final String TEXT_200 = ".append(";
  protected final String TEXT_201 = ");" + NL + "\t\t\t\t\t\t\t\t";
  protected final String TEXT_202 = NL + "        \t\t\t\tfileOut.write(headerBuilder_";
  protected final String TEXT_203 = ".toString().getBytes(";
  protected final String TEXT_204 = "));" + NL + "        \t\t\t\tfileOut.write(";
  protected final String TEXT_205 = ".getBytes(";
  protected final String TEXT_206 = "));" + NL + "        \t\t\t";
  protected final String TEXT_207 = NL + "\t\t\t\treturn new HDFSRecordWriter(fileOut, job);" + NL + "\t\t\t}" + NL + "\t\t}" + NL + "\t    ";
  protected final String TEXT_208 = NL + NL + "    public static class WriteNonEmpty_";
  protected final String TEXT_209 = "_ForeachRDDOutput<KEY, VALUE> implements ";
  protected final String TEXT_210 = " {" + NL + "" + NL + "        /** default serial version UID */" + NL + "        private static final long serialVersionUID = 1L;" + NL + "" + NL + "        private final String prefix;" + NL + "        private final String suffix;" + NL + "        private final Class<KEY> keyClass;" + NL + "        private final Class<VALUE> valueClass;" + NL + "        private final Class<? extends org.apache.hadoop.mapred.OutputFormat<?,?>> outputFormatClass;" + NL + "        private final org.apache.hadoop.mapred.JobConf jobConf;" + NL + "" + NL + "        public WriteNonEmpty_";
  protected final String TEXT_211 = "_ForeachRDDOutput(String prefix, String suffix," + NL + "                Class<KEY> keyClass," + NL + "                Class<VALUE> valueClass," + NL + "                Class<? extends org.apache.hadoop.mapred.OutputFormat<?,?>> outputFormatClass," + NL + "                org.apache.hadoop.mapred.JobConf jobConf) {" + NL + "            this.prefix = prefix;" + NL + "            this.suffix = suffix;" + NL + "            this.keyClass = keyClass;" + NL + "            this.valueClass = valueClass;" + NL + "            this.outputFormatClass = outputFormatClass;" + NL + "            this.jobConf = jobConf;" + NL + "        }" + NL + "" + NL + "        public WriteNonEmpty_";
  protected final String TEXT_212 = "_ForeachRDDOutput(String prefix, String suffix," + NL + "                Class<KEY> keyClass," + NL + "                Class<VALUE> valueClass," + NL + "                Class<? extends org.apache.hadoop.mapred.OutputFormat<?,?>> outputFormatClass) {" + NL + "            this(prefix, suffix, keyClass, valueClass, outputFormatClass, null);" + NL + "        }" + NL + "" + NL + "        @Override" + NL + "        public ";
  protected final String TEXT_213 = " call(";
  protected final String TEXT_214 = " rdd," + NL + "                org.apache.spark.streaming.Time time) throws Exception {" + NL + "            if (!rdd.isEmpty()) {" + NL + "                if (jobConf != null) {" + NL + "                    rdd.saveAsHadoopFile(prefix + \"-\" + time.milliseconds() + suffix," + NL + "                            keyClass, valueClass, outputFormatClass, jobConf);" + NL + "                } else {" + NL + "                    rdd.saveAsHadoopFile(prefix + \"-\" + time.milliseconds() + suffix," + NL + "                            keyClass, valueClass, outputFormatClass);" + NL + "                }" + NL + "            }";
  protected final String TEXT_215 = NL + "            ";
  protected final String TEXT_216 = NL + "        }" + NL + "    }";
  protected final String TEXT_217 = NL;

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    stringBuffer.append(TEXT_1);
    
BigDataCodeGeneratorArgument codeGenArgument = (BigDataCodeGeneratorArgument) argument;
INode node = (INode)codeGenArgument.getArgument();  
String cid = node.getUniqueName();

List<IMetadataTable> metadatas = node.getMetadataList();
if(metadatas != null && metadatas.size() > 0) {
    IMetadataTable metadata = metadatas.get(0);
    if(metadata != null){
        List<Map<String, String>> formats =
                (List<Map<String,String>>)ElementParameterParser.getObjectValue(node, "__FORMATS__");
        String rowSeparator = ElementParameterParser.getValue(node,"__ROWSEPARATOR__");
        String fieldSeparator = ElementParameterParser.getValue(node,"__FIELDSEPARATOR__");
        boolean includeHeader = "true".equals(ElementParameterParser.getValue(node, "__INCLUDEHEADER__"));
        boolean includeSeparator = "true".equals(ElementParameterParser.getValue(node, "__INCLUDESEPARATOR__")) && includeHeader;
        boolean customEncoding = "true".equals(ElementParameterParser.getValue(node, "__CUSTOM_ENCODING__"));
        String encoding = ElementParameterParser.getValue(node,"__ENCODING__");

        String advancedSeparatorStr = ElementParameterParser.getValue(node, "__ADVANCED_SEPARATOR__");
        boolean advancedSeparator = (advancedSeparatorStr!=null&&!("").equals(advancedSeparatorStr))?("true").equals(advancedSeparatorStr):false;
        String thousandsSeparator = ElementParameterParser.getValueWithJavaType(node, "__THOUSANDS_SEPARATOR__", JavaTypesManager.CHARACTER);
        String decimalSeparator = ElementParameterParser.getValueWithJavaType(node, "__DECIMAL_SEPARATOR__", JavaTypesManager.CHARACTER);
        boolean useLocalTimezone = ElementParameterParser.getBooleanValue(node, "__LOCAL_TIMEZONE_DATE_FORMAT__");

        boolean compress = "true".equals(ElementParameterParser.getValue(node, "__COMPRESS__"));
        String compression = ElementParameterParser.getValue(node, "__COMPRESSION__");
        boolean merge = "true".equals(ElementParameterParser.getValue(node, "__MERGE_RESULT__"));

        boolean useByte = ("true").equals(ElementParameterParser.getValue(node, "__USE_BYTE__"));


        List<? extends IConnection> inConns = node.getIncomingConnections(EConnectionType.FLOW_MAIN);
        if(inConns == null || inConns.size() == 0) {
            return "";
        }
        IConnection inConn = inConns.get(0); 
        String connName = inConn.getName();
        String connTypeName = codeGenArgument.getRecordStructName(inConn);

        List<IMetadataColumn> columns = metadata.getListColumns();
        int columnSize = columns.size();
        
    stringBuffer.append(TEXT_2);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_3);
    stringBuffer.append(connTypeName);
    stringBuffer.append(TEXT_4);
    stringBuffer.append(connTypeName);
    stringBuffer.append(TEXT_5);
    stringBuffer.append(connTypeName);
    stringBuffer.append(TEXT_6);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_7);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_8);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_9);
    
        			for (int i = 0; i < columnSize; i++) {
        			    IMetadataColumn column = columns.get(i);
                        Map<String,String> format=formats.get(i);
        			    String columnName = column.getLabel();
        			    JavaType javaType = JavaTypesManager.getJavaTypeFromId(column.getTalendType());
        			    String pattern = column.getPattern() == null || column.getPattern().trim().length() == 0 ? null : column.getPattern();
        			    boolean isPrimitive = JavaTypesManager.isJavaPrimitiveType(javaType, column.isNullable());

        			    //get  and format output String begin

                        if(isPrimitive) {
                            
    stringBuffer.append(TEXT_10);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_11);
    stringBuffer.append(column.getLabel() );
    stringBuffer.append(TEXT_12);
    
                        } else {
                            
    stringBuffer.append(TEXT_13);
    stringBuffer.append(column.getLabel() );
    stringBuffer.append(TEXT_14);
    
                                if(javaType == JavaTypesManager.DATE && pattern!=null){
                                    if (useLocalTimezone) {
    stringBuffer.append(TEXT_15);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_16);
    stringBuffer.append(column.getLabel() );
    stringBuffer.append(TEXT_17);
    stringBuffer.append(pattern );
    stringBuffer.append(TEXT_18);
    
                                    } else {
                                    
    stringBuffer.append(TEXT_19);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_20);
    stringBuffer.append(column.getLabel() );
    stringBuffer.append(TEXT_21);
    stringBuffer.append(pattern );
    stringBuffer.append(TEXT_22);
    
                                    }
                                }else if(javaType == JavaTypesManager.BYTE_ARRAY){
                                    
    stringBuffer.append(TEXT_23);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_24);
    stringBuffer.append(column.getLabel() );
    stringBuffer.append(TEXT_25);
    
                                }else if(javaType == JavaTypesManager.STRING){
                                    
    stringBuffer.append(TEXT_26);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_27);
    stringBuffer.append(column.getLabel() );
    stringBuffer.append(TEXT_28);
    
                                } else if(advancedSeparator && JavaTypesManager.isNumberType(javaType, column.isNullable())) { 
                                    if (javaType == JavaTypesManager.BIGDECIMAL) {
                                        
    stringBuffer.append(TEXT_29);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_30);
    stringBuffer.append(column.getPrecision() == null? "value" + "." + column.getLabel() : "value" + "." + column.getLabel() + ".setScale(" + column.getPrecision() + ", java.math.RoundingMode.HALF_UP)" );
    stringBuffer.append(TEXT_31);
    stringBuffer.append( thousandsSeparator );
    stringBuffer.append(TEXT_32);
    stringBuffer.append( decimalSeparator );
    stringBuffer.append(TEXT_33);
    
                                    } else {
                                        
    stringBuffer.append(TEXT_34);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_35);
    stringBuffer.append(column.getLabel() );
    stringBuffer.append(TEXT_36);
    stringBuffer.append( thousandsSeparator );
    stringBuffer.append(TEXT_37);
    stringBuffer.append( decimalSeparator );
    stringBuffer.append(TEXT_38);
    
                                    }
                                } else if (javaType == JavaTypesManager.BIGDECIMAL) {
                                    
    stringBuffer.append(TEXT_39);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_40);
    stringBuffer.append(column.getPrecision() == null ? "value" + "." + column.getLabel() : "value" + "." + column.getLabel() + ".setScale(" + column.getPrecision() + ", java.math.RoundingMode.HALF_UP)" );
    stringBuffer.append(TEXT_41);
    
                                } else {
                                    
    stringBuffer.append(TEXT_42);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_43);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_44);
    
                                }
                                
    stringBuffer.append(TEXT_45);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_46);
    
                        }

                        if (useByte) {
                            
    stringBuffer.append(TEXT_47);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_48);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_49);
    stringBuffer.append(encoding );
    stringBuffer.append(TEXT_50);
    
                        }else{
                            
    stringBuffer.append(TEXT_51);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_52);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_53);
    
                        }
                        
    stringBuffer.append(TEXT_54);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_55);
    stringBuffer.append(format.get("SIZE"));
    stringBuffer.append(TEXT_56);
    
                            if (("\'A\'").equals(format.get("KEEP"))) {
    stringBuffer.append(TEXT_57);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_58);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_59);
    
                            } else if (("\'R\'").equals(format.get("KEEP"))) {
                                 if(useByte){
                                     
    stringBuffer.append(TEXT_60);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_61);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_62);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_63);
    stringBuffer.append(encoding );
    stringBuffer.append(TEXT_64);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_65);
    stringBuffer.append(format.get("SIZE"));
    stringBuffer.append(TEXT_66);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_67);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_68);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_69);
    stringBuffer.append(encoding );
    stringBuffer.append(TEXT_70);
    
                                 }else{
                                     
    stringBuffer.append(TEXT_71);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_72);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_73);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_74);
    stringBuffer.append(format.get("SIZE"));
    stringBuffer.append(TEXT_75);
    
                                 }
                            } else if (("\'M\'").equals(format.get("KEEP"))) {
                                
    stringBuffer.append(TEXT_76);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_77);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_78);
    stringBuffer.append(format.get("SIZE"));
    stringBuffer.append(TEXT_79);
    
                                if(useByte){
                                    
    stringBuffer.append(TEXT_80);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_81);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_82);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_83);
    stringBuffer.append(encoding );
    stringBuffer.append(TEXT_84);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_85);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_86);
    stringBuffer.append(format.get("SIZE"));
    stringBuffer.append(TEXT_87);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_88);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_89);
    stringBuffer.append(encoding );
    stringBuffer.append(TEXT_90);
    
                                }else{
                                    
    stringBuffer.append(TEXT_91);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_92);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_93);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_94);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_95);
    stringBuffer.append(format.get("SIZE"));
    stringBuffer.append(TEXT_96);
    
                                }
                            } else {
                                if(useByte){
                                    
    stringBuffer.append(TEXT_97);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_98);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_99);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_100);
    stringBuffer.append(encoding );
    stringBuffer.append(TEXT_101);
    stringBuffer.append(format.get("SIZE"));
    stringBuffer.append(TEXT_102);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_103);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_104);
    stringBuffer.append(encoding );
    stringBuffer.append(TEXT_105);
    
                                }else{
                                    
    stringBuffer.append(TEXT_106);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_107);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_108);
    stringBuffer.append(format.get("SIZE"));
    stringBuffer.append(TEXT_109);
    
                                }
                            }
                            
    stringBuffer.append(TEXT_110);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_111);
    stringBuffer.append(format.get("SIZE"));
    stringBuffer.append(TEXT_112);
    
                            if (("\'L\'").equals(format.get("ALIGN"))) {
                                
    stringBuffer.append(TEXT_113);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_114);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_115);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_116);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_117);
    stringBuffer.append(format.get("SIZE"));
    stringBuffer.append(TEXT_118);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_119);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_120);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_121);
    stringBuffer.append(format.get("PADDING_CHAR"));
    stringBuffer.append(TEXT_122);
    
                            } else if (("\'R\'").equals(format.get("ALIGN"))) {
                                
    stringBuffer.append(TEXT_123);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_124);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_125);
    stringBuffer.append(format.get("SIZE"));
    stringBuffer.append(TEXT_126);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_127);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_128);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_129);
    stringBuffer.append(format.get("PADDING_CHAR"));
    stringBuffer.append(TEXT_130);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_131);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_132);
    
                            } else {
                                
    stringBuffer.append(TEXT_133);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_134);
    stringBuffer.append(format.get("SIZE"));
    stringBuffer.append(TEXT_135);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_136);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_137);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_138);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_139);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_140);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_141);
    stringBuffer.append(format.get("PADDING_CHAR"));
    stringBuffer.append(TEXT_142);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_143);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_144);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_145);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_146);
    stringBuffer.append(format.get("SIZE"));
    stringBuffer.append(TEXT_147);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_148);
    stringBuffer.append(format.get("PADDING_CHAR"));
    stringBuffer.append(TEXT_149);
    
                            }
                            
    stringBuffer.append(TEXT_150);
    
        			} // For

        			
    stringBuffer.append(TEXT_151);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_152);
    stringBuffer.append(customEncoding?encoding:"" );
    stringBuffer.append(TEXT_153);
    stringBuffer.append(connTypeName);
    stringBuffer.append(TEXT_154);
    stringBuffer.append(rowSeparator);
    stringBuffer.append(TEXT_155);
    stringBuffer.append(customEncoding?encoding:"" );
    stringBuffer.append(TEXT_156);
    stringBuffer.append(connTypeName);
    stringBuffer.append(TEXT_157);
    
        		if (!compress || merge) {
        		    
    stringBuffer.append(TEXT_158);
    
        		} else {

        			if("GZIP".equals(compression)){
        				
    stringBuffer.append(TEXT_159);
    
        			}else if("BZIP2".equals(compression)){
        				
    stringBuffer.append(TEXT_160);
    
        			}
        			
    stringBuffer.append(TEXT_161);
    
        		}
        		
        		if(includeSeparator) {
        			String header = "";
        			int iterator = 1;
        			for(IMetadataColumn columnForHeader : columns){
        				header += columnForHeader.getLabel() + (iterator!=columns.size() ? fieldSeparator.substring(1, fieldSeparator.length() - 1) : "");
        				iterator++;
        			}
        			
    stringBuffer.append(TEXT_162);
    stringBuffer.append(header);
    stringBuffer.append(TEXT_163);
    stringBuffer.append(customEncoding?encoding:"" );
    stringBuffer.append(TEXT_164);
    stringBuffer.append(rowSeparator);
    stringBuffer.append(TEXT_165);
    stringBuffer.append(customEncoding?encoding:"" );
    stringBuffer.append(TEXT_166);
    
        		} else if (includeHeader) {
        		
    stringBuffer.append(TEXT_167);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_168);
    
        			for(int i=0; i<columns.size(); i++) {
        				String initialColumnName = columns.get(i).getLabel();
        				Map<String,String> format = formats.get(i);
        				int size = Integer.parseInt(format.get("SIZE"));
        				if(initialColumnName.length() >= size) {
        					if (("\'A\'").equals(format.get("KEEP"))) {
        					
    stringBuffer.append(TEXT_169);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_170);
    stringBuffer.append(initialColumnName);
    stringBuffer.append(TEXT_171);
    
        					} else if (("\'R\'").equals(format.get("KEEP"))) {
        					
    stringBuffer.append(TEXT_172);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_173);
    stringBuffer.append(initialColumnName.substring(initialColumnName.length()-size));
    stringBuffer.append(TEXT_174);
    
        					} else if (("\'M\'").equals(format.get("KEEP"))) {
        						int begin = (initialColumnName.length()-size)/2;
        					
    stringBuffer.append(TEXT_175);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_176);
    stringBuffer.append(initialColumnName.substring(begin, begin+size));
    stringBuffer.append(TEXT_177);
    
        					} else {
        					
    stringBuffer.append(TEXT_178);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_179);
    stringBuffer.append(initialColumnName.substring(0, size));
    stringBuffer.append(TEXT_180);
    
        					}
        				} else if(initialColumnName.length() < size) {
        					if (("\'L\'").equals(format.get("ALIGN"))) {
        					
    stringBuffer.append(TEXT_181);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_182);
    stringBuffer.append(initialColumnName);
    stringBuffer.append(TEXT_183);
    
								for(int j=0; j<size-initialColumnName.length(); j++){
								
    stringBuffer.append(TEXT_184);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_185);
    stringBuffer.append(format.get("PADDING_CHAR"));
    stringBuffer.append(TEXT_186);
    
								}
        					} else if (("\'R\'").equals(format.get("ALIGN"))) {
								for(int j=0; j<size-initialColumnName.length(); j++){
								
    stringBuffer.append(TEXT_187);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_188);
    stringBuffer.append(format.get("PADDING_CHAR"));
    stringBuffer.append(TEXT_189);
    
								}
								
    stringBuffer.append(TEXT_190);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_191);
    stringBuffer.append(initialColumnName);
    stringBuffer.append(TEXT_192);
    
        					} else {
								int center = (size-initialColumnName.length())/2;
								for(int j=0; j<center; j++){
								
    stringBuffer.append(TEXT_193);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_194);
    stringBuffer.append(format.get("PADDING_CHAR"));
    stringBuffer.append(TEXT_195);
    
								}
								
    stringBuffer.append(TEXT_196);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_197);
    stringBuffer.append(initialColumnName);
    stringBuffer.append(TEXT_198);
    
								for(int j=center+initialColumnName.length(); j<size ;j++){
								
    stringBuffer.append(TEXT_199);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_200);
    stringBuffer.append(format.get("PADDING_CHAR"));
    stringBuffer.append(TEXT_201);
    
								}	
        					}
        				}
        			} // end for
        			
    stringBuffer.append(TEXT_202);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_203);
    stringBuffer.append(customEncoding?encoding:"" );
    stringBuffer.append(TEXT_204);
    stringBuffer.append(rowSeparator);
    stringBuffer.append(TEXT_205);
    stringBuffer.append(customEncoding?encoding:"" );
    stringBuffer.append(TEXT_206);
    
				}
			    
    stringBuffer.append(TEXT_207);
    
	}
}

    
boolean writeEmptyBatches = "true".equals(ElementParameterParser.getValue(node, "__WRITE_EMPTY_BATCHES__"));
if (!writeEmptyBatches) {
    
    
{ // Start ForeachRDD helper function
    // The signature of foreachRDD has changed in Spark 2.0
    org.talend.designer.spark.generator.utils.ForeachRDDUtil foreachUtil =
            org.talend.designer.spark.generator.utils.ForeachRDDUtil.createFunctionWithTimeJavaPairRDD(
                    codeGenArgument.getSparkVersion(), "KEY", "VALUE");
    
    stringBuffer.append(TEXT_208);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_209);
    stringBuffer.append(foreachUtil.getFunctionInterface());
    stringBuffer.append(TEXT_210);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_211);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_212);
    stringBuffer.append(foreachUtil.getCallReturnType());
    stringBuffer.append(TEXT_213);
    stringBuffer.append(foreachUtil.getCallArgumentType());
    stringBuffer.append(TEXT_214);
    stringBuffer.append(TEXT_215);
    stringBuffer.append(foreachUtil.getCallReturnCode());
    stringBuffer.append(TEXT_216);
    
} // End ForeachRDD helper function

    
}

    stringBuffer.append(TEXT_217);
    return stringBuffer.toString();
  }
}
