package org.talend.designer.codegen.translators.data_quality.survivorship;

import org.talend.core.model.metadata.IMetadataColumn;
import org.talend.core.model.metadata.IMetadataTable;
import org.talend.core.model.metadata.types.JavaType;
import org.talend.core.model.metadata.types.JavaTypesManager;
import org.talend.core.model.process.EConnectionType;
import org.talend.core.model.process.ElementParameterParser;
import org.talend.core.model.process.IBigDataNode;
import org.talend.core.model.process.IConnection;
import org.talend.core.model.process.IConnectionCategory;
import org.talend.core.model.process.INode;
import org.talend.designer.common.BigDataCodeGeneratorArgument;

public class TRuleSurvivorshipSparkcodeJava
{
  protected static String nl;
  public static synchronized TRuleSurvivorshipSparkcodeJava create(String lineSeparator)
  {
    nl = lineSeparator;
    TRuleSurvivorshipSparkcodeJava result = new TRuleSurvivorshipSparkcodeJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = NL + "public static class ";
  protected final String TEXT_2 = "_DummyMapToPairFunction implements org.apache.spark.api.java.function.PairFunction<";
  protected final String TEXT_3 = ", Object, ";
  protected final String TEXT_4 = "> {" + NL + "\tprivate ContextProperties context = null;" + NL + "\t" + NL + "\tpublic ";
  protected final String TEXT_5 = "_DummyMapToPairFunction(JobConf job) {" + NL + "\t\tthis.context = new ContextProperties(job);" + NL + "\t}" + NL + "\t" + NL + "\tpublic scala.Tuple2<Object, ";
  protected final String TEXT_6 = "> call(";
  protected final String TEXT_7 = " row) throws java.lang.Exception {" + NL + "\t\treturn new scala.Tuple2<Object, ";
  protected final String TEXT_8 = ">(row.";
  protected final String TEXT_9 = ", row);" + NL + "\t}" + NL + "}" + NL + "" + NL + "public static class ";
  protected final String TEXT_10 = "_Function implements org.apache.spark.api.java.function.FlatMapFunction<java.util.Iterator<";
  protected final String TEXT_11 = ">, ";
  protected final String TEXT_12 = "> {" + NL + "\tprivate ContextProperties context = null;" + NL + "\tprivate org.talend.survivorship.SurvivorshipManager survivorshipManager = null;" + NL + "\t" + NL + "\tpublic ";
  protected final String TEXT_13 = "_Function(JobConf job) {" + NL + "\t\tthis.context = new ContextProperties(job);" + NL + "\t}" + NL + "\t" + NL + "\t@Override" + NL + "\tpublic ";
  protected final String TEXT_14 = "<";
  protected final String TEXT_15 = "> call(java.util.Iterator<";
  protected final String TEXT_16 = "> inputs) throws Exception {" + NL + "" + NL + "\t\t// TODO: remove init SurvivorshipManager from call() method" + NL + "\t\tif(survivorshipManager == null){" + NL + "\t\t    //transmit the rule path only for useLocalMode,or else the String \"Real_spark_relative_path\" represent a real cluster." + NL + "\t\t    survivorshipManager = new org.talend.survivorship.SurvivorshipManager(\"true\".equals(\"";
  protected final String TEXT_17 = "\")?\"";
  protected final String TEXT_18 = "\":\"Real_spark_relative_path\", ";
  protected final String TEXT_19 = ");" + NL + "\t\t    survivorshipManager.setJobName(\"";
  protected final String TEXT_20 = "\");" + NL + "\t\t    survivorshipManager.setJobVersion(\"";
  protected final String TEXT_21 = "\");" + NL + "\t\t" + NL + "\t\t";
  protected final String TEXT_22 = NL + "\t\t    survivorshipManager.addColumn(\"";
  protected final String TEXT_23 = "\",\"";
  protected final String TEXT_24 = "\");" + NL + "\t\t    " + NL + "\t\t";
  protected final String TEXT_25 = NL + "\t\t    survivorshipManager.addRuleDefinition(" + NL + "\t\t        new org.talend.survivorship.model.RuleDefinition(" + NL + "\t\t            org.talend.survivorship.model.RuleDefinition.Order.";
  protected final String TEXT_26 = NL + "\t\t            ,";
  protected final String TEXT_27 = NL + "\t\t            ,\"";
  protected final String TEXT_28 = "\"                                " + NL + "\t\t            ,";
  protected final String TEXT_29 = "null";
  protected final String TEXT_30 = "org.talend.survivorship.model.RuleDefinition.Function.";
  protected final String TEXT_31 = NL + "\t\t            ,";
  protected final String TEXT_32 = NL + "\t\t            ,\"";
  protected final String TEXT_33 = "\"" + NL + "\t\t            ,";
  protected final String TEXT_34 = NL + "\t\t        )" + NL + "\t\t    );" + NL + "\t\t";
  protected final String TEXT_35 = NL + "\t\t//TDQ-14308,Create a Map and store all drools files InputStream,this Inputstream will be transfered to SurvivorshipManager to load resources." + NL + "\t\tjava.util.Map<org.kie.api.io.ResourceType, java.util.List<java.io.InputStream>> streamsMap=new java.util.HashMap<org.kie.api.io.ResourceType, java.util.List<java.io.InputStream>>();" + NL + "\t\tString rulePath=\"metadata/survivorship/\"+";
  protected final String TEXT_36 = "+\"/\";" + NL + "\t\tjava.util.List<java.io.InputStream> drlStreamLs=new java.util.ArrayList<java.io.InputStream>();" + NL + "\t\tjava.util.List<java.io.InputStream> flowStreamLs=new java.util.ArrayList<java.io.InputStream>();" + NL + "\t\tString version=org.talend.survivorship.SurvivorshipConstants.VERSION_SUFFIX;" + NL + "\t\tString packageExtension=org.talend.survivorship.SurvivorshipConstants.PKG_ITEM_EXTENSION;" + NL + "\t\tString droolsPre=org.talend.survivorship.SurvivorshipConstants.DROOLS;" + NL + "\t\tString droolsExtension=org.talend.survivorship.SurvivorshipConstants.RULE_ITEM_EXTENSION;" + NL + "\t\tString flowExtension=org.talend.survivorship.SurvivorshipConstants.FLOW_ITEM_EXTENSION;" + NL + "\t\tString flowPre=org.talend.survivorship.SurvivorshipConstants.SURVIVOR_FLOW;" + NL + "\t\tjava.io.InputStream streamPakage =this.getClass().getClassLoader().getResourceAsStream(rulePath+droolsPre+version+packageExtension);" + NL + "\t\tif(streamPakage!=null){" + NL + "\t\t     drlStreamLs.add(streamPakage);" + NL + "\t\t}else if(";
  protected final String TEXT_37 = "){" + NL + "\t\t     log.error(rulePath+droolsPre+version+packageExtension+\"  is null!\");" + NL + "\t\t}" + NL + "\t\tjava.util.List<org.talend.survivorship.model.RuleDefinition> ruleDefs=survivorshipManager.getRuleDefinitionList();" + NL + "\t\tfor (org.talend.survivorship.model.RuleDefinition definition : ruleDefs) {" + NL + "\t\t     if (\"SEQ\".equals(definition.getOrder().getLabel())) {" + NL + "\t\t          java.io.InputStream stream=this.getClass().getClassLoader().getResourceAsStream(rulePath+ definition.getRuleName()+version+droolsExtension);" + NL + "\t\t          if(stream!=null){" + NL + "\t\t              drlStreamLs.add(stream);" + NL + "\t\t          }else if(";
  protected final String TEXT_38 = "){" + NL + "\t\t              log.error(rulePath+ definition.getRuleName()+version+droolsExtension+\" is null!\");" + NL + "\t\t          }    " + NL + "\t\t      }" + NL + "\t\t}" + NL + "\t\tjava.io.InputStream streamBpmn=this.getClass().getClassLoader().getResourceAsStream(rulePath+flowPre+version+flowExtension);" + NL + "\t\tif(streamPakage!=null){" + NL + "\t\t     flowStreamLs.add(streamBpmn);" + NL + "\t\t}else if(";
  protected final String TEXT_39 = "){" + NL + "\t\t     log.error(rulePath+droolsPre+version+packageExtension+\"  is null= \"+(streamBpmn==null));" + NL + "\t\t}" + NL + "\t\tstreamsMap.put(org.kie.api.io.ResourceType.DRL,drlStreamLs);" + NL + "\t\tstreamsMap.put(org.kie.api.io.ResourceType.BPMN2,flowStreamLs);" + NL + "\t\t";
  protected final String TEXT_40 = NL + "\t\t    org.talend.survivorship.model.Column column=null;" + NL + "\t\t    org.talend.survivorship.model.ConflictRuleDefinition conflictRule=null;" + NL + "\t\t";
  protected final String TEXT_41 = NL + "\t\t        column=survivorshipManager.getColumnByName(\"";
  protected final String TEXT_42 = "\");" + NL + "\t\t        conflictRule=new org.talend.survivorship.model.ConflictRuleDefinition(" + NL + "\t\t                null" + NL + "\t\t                ,";
  protected final String TEXT_43 = NL + "\t\t                ,\"";
  protected final String TEXT_44 = "\"                                " + NL + "\t\t                ,";
  protected final String TEXT_45 = "null";
  protected final String TEXT_46 = "org.talend.survivorship.model.ConflictRuleDefinition.Function.";
  protected final String TEXT_47 = NL + "\t\t                ,";
  protected final String TEXT_48 = NL + "\t\t                ,\"";
  protected final String TEXT_49 = "\"" + NL + "\t\t                ,";
  protected final String TEXT_50 = NL + "\t\t                ,";
  protected final String TEXT_51 = NL + "\t\t                ,";
  protected final String TEXT_52 = NL + "\t\t            );" + NL + "\t\t        column.getConflictResolveList().add(conflictRule);" + NL + "\t\t ";
  protected final String TEXT_53 = "    " + NL + "\t\t    survivorshipManager.initKnowledgeBase(streamsMap);" + NL + "\t\t " + NL + "\t\t //check if the conflict rules are valid." + NL + "\t\t java.util.Map<String,java.util.List<String>> errorMap=survivorshipManager.checkConflictRuleValid();" + NL + "\t\t if(errorMap.size()>0){" + NL + "\t\t     java.util.Iterator<?> iter = errorMap.entrySet().iterator();" + NL + "\t\t     StringBuilder erroMessages=new StringBuilder();" + NL + "\t\t     while (iter.hasNext()){" + NL + "\t\t        java.util.Map.Entry<String,java.util.List<String>> entry = (java.util.Map.Entry) iter.next();" + NL + "\t\t        erroMessages.append(\"'\"+entry.getKey()+\"':\\n\");" + NL + "\t\t        for(String value:entry.getValue()){" + NL + "\t\t            for(int index=0;index<(\"'\"+entry.getKey()+\"':\").length();index++){" + NL + "\t\t                erroMessages.append(\" \");" + NL + "\t\t            }" + NL + "\t\t            erroMessages.append(value+\"\\n\");" + NL + "\t\t        }" + NL + "\t\t     }" + NL + "\t\t     throw new Exception(erroMessages.toString());" + NL + "\t\t }" + NL + "\t\t";
  protected final String TEXT_54 = NL + "\t\t    survivorshipManager.initKnowledgeBase(streamsMap);" + NL + "\t\t  ";
  protected final String TEXT_55 = NL + "\t\t}" + NL + "\t\t// End of init SurvivorshipManager" + NL + "" + NL + "\t\tjava.util.List<";
  protected final String TEXT_56 = "> outputs = new java.util.ArrayList<";
  protected final String TEXT_57 = ">();" + NL + "\t\tObject currentId, preId = null;" + NL + "\t\tjava.util.List<";
  protected final String TEXT_58 = "> recordsInGroup = new java.util.ArrayList<";
  protected final String TEXT_59 = ">();" + NL + "\t\twhile(inputs.hasNext()){" + NL + "\t\t\t";
  protected final String TEXT_60 = " currentRow = inputs.next();" + NL + "\t\t\tcurrentId = currentRow.";
  protected final String TEXT_61 = ";" + NL + "\t\t\tif(!recordsInGroup.isEmpty() && !currentId.equals(preId)){ // group completed" + NL + "\t\t\t\toutputs.addAll(runRuleSurvivorship(recordsInGroup, preId));" + NL + "\t\t\t\trecordsInGroup.clear();" + NL + "\t\t\t}// end of if(!currentId.equals(preId))" + NL + "\t\t\t\t\t\t" + NL + "\t\t\trecordsInGroup.add(currentRow);" + NL + "\t\t\tpreId = currentId;" + NL + "\t\t}" + NL + "\t\tif(!recordsInGroup.isEmpty()){ // for the last group in a partition" + NL + "\t\t\toutputs.addAll(runRuleSurvivorship(recordsInGroup, preId));" + NL + "\t\t}" + NL + "\t\t";
  protected final String TEXT_62 = NL + "\t\treturn outputs;" + NL + "\t\t";
  protected final String TEXT_63 = NL + "\t\treturn outputs.iterator();" + NL + "\t\t";
  protected final String TEXT_64 = NL + "\t}" + NL + "" + NL + "\tprivate java.util.List<";
  protected final String TEXT_65 = "> runRuleSurvivorship(java.util.List<";
  protected final String TEXT_66 = "> recordsInGroup, Object id){" + NL + "\t\tjava.util.List<";
  protected final String TEXT_67 = "> outputs = new java.util.ArrayList<";
  protected final String TEXT_68 = ">();" + NL + "\t\t\t" + NL + "\t\tObject[][] groupValues = new Object[recordsInGroup.size()][";
  protected final String TEXT_69 = "];" + NL + "\t\t// the method runSession() needs Object[][] as input, so convert List<";
  protected final String TEXT_70 = "> to Object[][]" + NL + "\t\tfor (int i = 0; i < recordsInGroup.size(); i++) {" + NL + "\t\t\t";
  protected final String TEXT_71 = " values = recordsInGroup.get(i);" + NL + "\t\t";
  protected final String TEXT_72 = NL + "\t\t\tgroupValues[i] = new Object[] {";
  protected final String TEXT_73 = "};" + NL + "\t\t}" + NL + "\t\t" + NL + "\t\t// call the function runSession of SurvivorshipManager" + NL + "\t\tsurvivorshipManager.runSession(groupValues);" + NL + "\t\tjava.util.Map<String, Object> survivors = survivorshipManager.getSurvivorMap();" + NL + "\t\tjava.util.List<java.util.HashSet<String>> conflicts = survivorshipManager.getConflictList();" + NL + "\t\t" + NL + "\t\t// add the original records to outputs list" + NL + "\t\tfor (int i = 0; i < recordsInGroup.size(); i++) {" + NL + "\t\t\t";
  protected final String TEXT_74 = " input = recordsInGroup.get(i);" + NL + "\t\t\t";
  protected final String TEXT_75 = " output = new ";
  protected final String TEXT_76 = "();" + NL + "\t\t";
  protected final String TEXT_77 = NL + "\t\t\toutput.";
  protected final String TEXT_78 = " = input.";
  protected final String TEXT_79 = ";" + NL + "\t\t";
  protected final String TEXT_80 = NL + "\t\t\toutput.CONFLICT = conflicts.get(i).toString();" + NL + "\t\t\toutput.SURVIVOR = false;" + NL + "\t\t\toutputs.add(output);" + NL + "\t\t}" + NL + "\t\t" + NL + "\t\t// add the master record to outputs list" + NL + "\t\t";
  protected final String TEXT_81 = " outputMaster = new ";
  protected final String TEXT_82 = "();" + NL + "\t\t";
  protected final String TEXT_83 = NL + "\t\t\toutputMaster.put(\"";
  protected final String TEXT_84 = "\", survivors.get(\"";
  protected final String TEXT_85 = "\"));" + NL + "\t\t";
  protected final String TEXT_86 = NL + "\t\toutputMaster.CONFLICT = survivorshipManager.getConflictsOfSurvivor().toString();" + NL + "\t\toutputMaster.SURVIVOR = true;" + NL + "\t\toutputMaster.";
  protected final String TEXT_87 = " = ParserUtils.parseTo_";
  protected final String TEXT_88 = "(id.toString());" + NL + "\t\toutputs.add(outputMaster);" + NL + "\t\t" + NL + "\t\treturn outputs;" + NL + "\t\t\t" + NL + "\t}" + NL + "" + NL + "}";
  protected final String TEXT_89 = NL;

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    
//Parse the inputs to this javajet generator.
final BigDataCodeGeneratorArgument codeGenArgument = (BigDataCodeGeneratorArgument) argument;
final INode node = (INode) codeGenArgument.getArgument();
final IBigDataNode bigDataNode = (IBigDataNode) codeGenArgument.getArgument();
final String cid = node.getUniqueName();
final IConnection inConn = node.getIncomingConnections().get(0);
final String inConnTypeName = codeGenArgument.getRecordStructName(inConn);
final java.util.List<IMetadataColumn> inputColumns = inConn != null ? inConn.getMetadataTable().getListColumns() : new java.util.ArrayList<IMetadataColumn>();
final IConnection outConn = node.getOutgoingConnections().get(0);
final String outConnTypeName = codeGenArgument.getRecordStructName(outConn);
final String packageName = ElementParameterParser.getValue(node, "__PACKAGE_NAME__");
final String projectDir = ElementParameterParser.getValue(node.getProcess(), "__TDQ_DEFAULT_PROJECT_DIR__"); 
final String grpIDColumnName = ElementParameterParser.getValue(node, "__GRP_ID__");
final String isDefinedConflict = ElementParameterParser.getValue(node, "__DEFINE_CONFLICT__");
final java.util.List<java.util.Map<String, String>> conlictRules = (java.util.List<java.util.Map<String,String>>)ElementParameterParser.getObjectValue(node, "__CONFLICT_TABLE__");
final java.util.List<? extends IConnection> outConns = node.getOutgoingSortedConnections();
final java.util.List<java.util.Map<String, String>> operations = (java.util.List<java.util.Map<String,String>>)ElementParameterParser.getObjectValue(node, "__OPERATIONS__");
final boolean isLog4jEnabled = ("true").equals(ElementParameterParser.getValue(node.getProcess(), "__LOG4J_ACTIVATE__"));
//Find the tSparkConfiguration and define which Spark version is currently used.
final java.util.List<? extends INode> sparkConfigs = node.getProcess().getNodesOfType("tSparkConfiguration");
boolean useLocalMode = false;
INode sparkConfig = null;
if(sparkConfigs != null && sparkConfigs.size() > 0) {
	sparkConfig = sparkConfigs.get(0);
}
if(sparkConfig != null) {
useLocalMode = "true".equals(ElementParameterParser.getValue(sparkConfig, "__SPARK_LOCAL_MODE__"));
}

final boolean isSpark1 = org.talend.hadoop.distribution.ESparkVersion.SPARK_2_0.compareTo(codeGenArgument.getSparkVersion()) > 0;
final String flatMapOutPutType = isSpark1 ? "java.util.List" : "java.util.Iterator";


    stringBuffer.append(TEXT_1);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_2);
    stringBuffer.append(inConnTypeName);
    stringBuffer.append(TEXT_3);
    stringBuffer.append(inConnTypeName);
    stringBuffer.append(TEXT_4);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_5);
    stringBuffer.append(inConnTypeName);
    stringBuffer.append(TEXT_6);
    stringBuffer.append(inConnTypeName);
    stringBuffer.append(TEXT_7);
    stringBuffer.append(inConnTypeName);
    stringBuffer.append(TEXT_8);
    stringBuffer.append(grpIDColumnName);
    stringBuffer.append(TEXT_9);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_10);
    stringBuffer.append(inConnTypeName);
    stringBuffer.append(TEXT_11);
    stringBuffer.append(outConnTypeName);
    stringBuffer.append(TEXT_12);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_13);
    stringBuffer.append(flatMapOutPutType);
    stringBuffer.append(TEXT_14);
    stringBuffer.append(outConnTypeName);
    stringBuffer.append(TEXT_15);
    stringBuffer.append(inConnTypeName);
    stringBuffer.append(TEXT_16);
    stringBuffer.append(useLocalMode);
    stringBuffer.append(TEXT_17);
    stringBuffer.append(projectDir);
    stringBuffer.append(TEXT_18);
    stringBuffer.append(packageName);
    stringBuffer.append(TEXT_19);
    stringBuffer.append(node.getProcess().getName());
    stringBuffer.append(TEXT_20);
    stringBuffer.append(node.getProcess().getVersion());
    stringBuffer.append(TEXT_21);
    
		for(IMetadataColumn column : inputColumns){
		    String typeName = "";
		    typeName = column.getTalendType().substring(column.getTalendType().indexOf("_")+1);
		    if(typeName.equals("Date")){
		        typeName = "java.util.Date";
		    }
		
    stringBuffer.append(TEXT_22);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_23);
    stringBuffer.append(typeName);
    stringBuffer.append(TEXT_24);
    
		}
		
		for(int i=0; i<operations.size(); i++){
		    java.util.Map<String, String> operation = operations.get(i);
		    String relation = operation.get("RELATION");
		    String name = operation.get("RULE_NAME");
		    String reference = operation.get("INPUT_COLUMN");
		    String function = operation.get("FUNCTION");
		    String operationValue = operation.get("OPERATION");
		    String target = operation.get("OUTPUT_COLUMN");//?
		    String ignoreNull = operation.get("IGNORE_NULL");
		    
    stringBuffer.append(TEXT_25);
    stringBuffer.append(relation);
    stringBuffer.append(TEXT_26);
    stringBuffer.append(name);
    stringBuffer.append(TEXT_27);
    stringBuffer.append(reference);
    stringBuffer.append(TEXT_28);
    if("None".equals(function)){
    stringBuffer.append(TEXT_29);
    }else{
    stringBuffer.append(TEXT_30);
    stringBuffer.append(function);
    }
    stringBuffer.append(TEXT_31);
    stringBuffer.append("\"\"".equals(operationValue)?null:operationValue);
    stringBuffer.append(TEXT_32);
    stringBuffer.append(target);
    stringBuffer.append(TEXT_33);
    stringBuffer.append(ignoreNull);
    stringBuffer.append(TEXT_34);
    
		}
		
    stringBuffer.append(TEXT_35);
    stringBuffer.append(packageName);
    stringBuffer.append(TEXT_36);
    stringBuffer.append(isLog4jEnabled);
    stringBuffer.append(TEXT_37);
    stringBuffer.append(isLog4jEnabled);
    stringBuffer.append(TEXT_38);
    stringBuffer.append(isLog4jEnabled);
    stringBuffer.append(TEXT_39);
    
		
	      	//check if the conflict rules are valid
		if("true".equals(isDefinedConflict)){
		
    stringBuffer.append(TEXT_40);
    
		    for(int i=0; i<conlictRules.size(); i++){
		        java.util.Map<String, String> conlictRule = conlictRules.get(i);
		        String name = conlictRule.get("CR_RULE_NAME");
		        String conflictingCol = conlictRule.get("CR_CONFLICTING_COLUMN");
		        String function = conlictRule.get("CR_FUNCTION");
		        String operationValue = conlictRule.get("CR_VALUE");
		        String referenceCol = conlictRule.get("CR_REF_COLUMN");
		        String ignoreNull = conlictRule.get("CR_IGNORE_NULL");
		        String disable = conlictRule.get("CR_DISABLE");
		        String fillColumn = null;
		        String removeDuplicate ="false";
		 
    stringBuffer.append(TEXT_41);
    stringBuffer.append(conflictingCol);
    stringBuffer.append(TEXT_42);
    stringBuffer.append(name);
    stringBuffer.append(TEXT_43);
    stringBuffer.append(referenceCol);
    stringBuffer.append(TEXT_44);
    if("None".equals(function)){
    stringBuffer.append(TEXT_45);
    }else{
    stringBuffer.append(TEXT_46);
    stringBuffer.append(function);
    }
    stringBuffer.append(TEXT_47);
    stringBuffer.append("\"\"".equals(operationValue)?null:operationValue);
    stringBuffer.append(TEXT_48);
    stringBuffer.append(conflictingCol);
    stringBuffer.append(TEXT_49);
    stringBuffer.append(ignoreNull);
    stringBuffer.append(TEXT_50);
    stringBuffer.append(removeDuplicate);
    stringBuffer.append(TEXT_51);
    stringBuffer.append(disable);
    stringBuffer.append(TEXT_52);
    
		    }
		 
    stringBuffer.append(TEXT_53);
    
		}else{
	       	
    stringBuffer.append(TEXT_54);
     
		}
		
    stringBuffer.append(TEXT_55);
    stringBuffer.append(outConnTypeName);
    stringBuffer.append(TEXT_56);
    stringBuffer.append(outConnTypeName);
    stringBuffer.append(TEXT_57);
    stringBuffer.append(inConnTypeName);
    stringBuffer.append(TEXT_58);
    stringBuffer.append(inConnTypeName);
    stringBuffer.append(TEXT_59);
    stringBuffer.append(inConnTypeName);
    stringBuffer.append(TEXT_60);
    stringBuffer.append(grpIDColumnName);
    stringBuffer.append(TEXT_61);
    
		if(isSpark1){
		
    stringBuffer.append(TEXT_62);
    
		} else {
		
    stringBuffer.append(TEXT_63);
    
		}
		
    stringBuffer.append(TEXT_64);
    stringBuffer.append(outConnTypeName);
    stringBuffer.append(TEXT_65);
    stringBuffer.append(inConnTypeName);
    stringBuffer.append(TEXT_66);
    stringBuffer.append(outConnTypeName);
    stringBuffer.append(TEXT_67);
    stringBuffer.append(outConnTypeName);
    stringBuffer.append(TEXT_68);
    stringBuffer.append(inputColumns.size());
    stringBuffer.append(TEXT_69);
    stringBuffer.append(inConnTypeName);
    stringBuffer.append(TEXT_70);
    stringBuffer.append(inConnTypeName);
    stringBuffer.append(TEXT_71);
    
			String colValuesStr = "";
			for( int i = 0; i < inputColumns.size(); i++) {
				IMetadataColumn metadataColumn = inputColumns.get(i);
				String prefix = (i == 0 ) ? "values." : ", values.";
				colValuesStr += prefix + metadataColumn.getLabel();
			}
		
    stringBuffer.append(TEXT_72);
    stringBuffer.append(colValuesStr);
    stringBuffer.append(TEXT_73);
    stringBuffer.append(inConnTypeName);
    stringBuffer.append(TEXT_74);
    stringBuffer.append(outConnTypeName);
    stringBuffer.append(TEXT_75);
    stringBuffer.append(outConnTypeName);
    stringBuffer.append(TEXT_76);
    
		for( int i = 0; i < inputColumns.size(); i++) {
			IMetadataColumn metadataColumn = inputColumns.get(i);
		
    stringBuffer.append(TEXT_77);
    stringBuffer.append(metadataColumn.getLabel());
    stringBuffer.append(TEXT_78);
    stringBuffer.append(metadataColumn.getLabel());
    stringBuffer.append(TEXT_79);
    
		}
		
    stringBuffer.append(TEXT_80);
    stringBuffer.append(outConnTypeName);
    stringBuffer.append(TEXT_81);
    stringBuffer.append(outConnTypeName);
    stringBuffer.append(TEXT_82);
    
		String grpIDColumnType = "";
		for( int i = 0; i < inputColumns.size(); i++) {
			IMetadataColumn metadataColumn = inputColumns.get(i);
			if(grpIDColumnName.equals(metadataColumn.getLabel())){
                   		grpIDColumnType = JavaTypesManager.getTypeToGenerate(metadataColumn.getTalendType(), metadataColumn.isNullable());                            
                 	}
		
    stringBuffer.append(TEXT_83);
    stringBuffer.append(metadataColumn.getLabel());
    stringBuffer.append(TEXT_84);
    stringBuffer.append(metadataColumn.getLabel());
    stringBuffer.append(TEXT_85);
    
		}
		
    stringBuffer.append(TEXT_86);
    stringBuffer.append(grpIDColumnName);
    stringBuffer.append(TEXT_87);
    stringBuffer.append(grpIDColumnType);
    stringBuffer.append(TEXT_88);
    stringBuffer.append(TEXT_89);
    return stringBuffer.toString();
  }
}
