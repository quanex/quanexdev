package org.talend.designer.codegen.translators.business.sap;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.talend.core.model.metadata.IMetadataColumn;
import org.talend.core.model.metadata.IMetadataTable;
import org.talend.core.model.metadata.types.JavaType;
import org.talend.core.model.metadata.types.JavaTypesManager;
import org.talend.core.model.process.ElementParameterParser;
import org.talend.core.model.process.IConnection;
import org.talend.core.model.process.IConnectionCategory;
import org.talend.core.model.process.INode;
import org.talend.designer.codegen.config.CodeGeneratorArgument;
import org.talend.core.model.utils.TalendTextUtils;

public class TSAPODPInputBeginJava
{
  protected static String nl;
  public static synchronized TSAPODPInputBeginJava create(String lineSeparator)
  {
    nl = lineSeparator;
    TSAPODPInputBeginJava result = new TSAPODPInputBeginJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "";
  protected final String TEXT_2 = NL + "\t" + NL + "\t";
  protected final String TEXT_3 = "\t    " + NL + "\t\torg.talend.sap.ISAPConnection connection_";
  protected final String TEXT_4 = " = (org.talend.sap.ISAPConnection)globalMap.get(\"conn_";
  protected final String TEXT_5 = "\");\t";
  protected final String TEXT_6 = NL + "\t\t\t\tif(connection_";
  protected final String TEXT_7 = " == null){" + NL + "\t\t\t\t\tconnection_";
  protected final String TEXT_8 = " = ((org.talend.sap.impl.SAPConnectionFactory)(org.talend.sap.impl.SAPConnectionFactory.getInstance())).createConnection(";
  protected final String TEXT_9 = ");" + NL + "\t\t\t\t}";
  protected final String TEXT_10 = NL + "\t";
  protected final String TEXT_11 = NL + "\t\torg.talend.sap.ISAPConnection connection_";
  protected final String TEXT_12 = " = null;";
  protected final String TEXT_13 = NL + "\t\t\t\tconnection_";
  protected final String TEXT_14 = " = ((org.talend.sap.impl.SAPConnectionFactory)(org.talend.sap.impl.SAPConnectionFactory.getInstance())).createConnection(";
  protected final String TEXT_15 = ");";
  protected final String TEXT_16 = NL + "\t\t\tif (connection_";
  protected final String TEXT_17 = " == null) {//}";
  protected final String TEXT_18 = NL + "\t\t";
  protected final String TEXT_19 = NL + NL + "\tclass PropertyUil_";
  protected final String TEXT_20 = " {" + NL + "\t\t" + NL + "        void validateAndSet(java.util.Properties p, String key, Object value) {" + NL + "        \tif(value==null) {" + NL + "        \t\tSystem.err.println(\"WARN : will ignore the property : \" + key + \" as setting the null value.\"); " + NL + "        \t\treturn;" + NL + "        \t}" + NL + "        \t" + NL + "        \tp.setProperty(key, String.valueOf(value));" + NL + "        }" + NL + "        " + NL + "\t}" + NL + "\t" + NL + "\tPropertyUil_";
  protected final String TEXT_21 = " pu_";
  protected final String TEXT_22 = " = new PropertyUil_";
  protected final String TEXT_23 = "();" + NL + "" + NL + "\t";
  protected final String TEXT_24 = " " + NL + "\tfinal String decryptedPassword_";
  protected final String TEXT_25 = " = routines.system.PasswordEncryptUtil.decryptPassword(";
  protected final String TEXT_26 = ");";
  protected final String TEXT_27 = NL + "\tfinal String decryptedPassword_";
  protected final String TEXT_28 = " = ";
  protected final String TEXT_29 = "; ";
  protected final String TEXT_30 = NL + "\tjava.util.Properties properties_";
  protected final String TEXT_31 = " = new java.util.Properties();" + NL + "    pu_";
  protected final String TEXT_32 = ".validateAndSet(properties_";
  protected final String TEXT_33 = ", org.talend.sap.ISAPConnection.PROP_CLIENT, ";
  protected final String TEXT_34 = ");" + NL + "    pu_";
  protected final String TEXT_35 = ".validateAndSet(properties_";
  protected final String TEXT_36 = ", org.talend.sap.ISAPConnection.PROP_USER, ";
  protected final String TEXT_37 = ");" + NL + "    pu_";
  protected final String TEXT_38 = ".validateAndSet(properties_";
  protected final String TEXT_39 = ", org.talend.sap.ISAPConnection.PROP_PASSWORD, decryptedPassword_";
  protected final String TEXT_40 = ");" + NL + "    pu_";
  protected final String TEXT_41 = ".validateAndSet(properties_";
  protected final String TEXT_42 = ", org.talend.sap.ISAPConnection.PROP_LANGUAGE, ";
  protected final String TEXT_43 = ");" + NL + "    ";
  protected final String TEXT_44 = NL + "    pu_";
  protected final String TEXT_45 = ".validateAndSet(properties_";
  protected final String TEXT_46 = ", org.talend.sap.ISAPConnection.PROP_APPLICATION_SERVER_HOST, ";
  protected final String TEXT_47 = ");" + NL + "    pu_";
  protected final String TEXT_48 = ".validateAndSet(properties_";
  protected final String TEXT_49 = ", org.talend.sap.ISAPConnection.PROP_SYSTEM_NUMBER, ";
  protected final String TEXT_50 = ");";
  protected final String TEXT_51 = NL + "    pu_";
  protected final String TEXT_52 = ".validateAndSet(properties_";
  protected final String TEXT_53 = ", \"jco.client.mshost\", ";
  protected final String TEXT_54 = ");" + NL + "    pu_";
  protected final String TEXT_55 = ".validateAndSet(properties_";
  protected final String TEXT_56 = ", \"jco.client.r3name\", ";
  protected final String TEXT_57 = ");" + NL + "    pu_";
  protected final String TEXT_58 = ".validateAndSet(properties_";
  protected final String TEXT_59 = ", \"jco.client.group\", ";
  protected final String TEXT_60 = ");";
  protected final String TEXT_61 = NL + "    " + NL + "\t";
  protected final String TEXT_62 = NL + "\t\tpu_";
  protected final String TEXT_63 = ".validateAndSet(properties_";
  protected final String TEXT_64 = ", ";
  protected final String TEXT_65 = " ,";
  protected final String TEXT_66 = ");" + NL + "\t\t";
  protected final String TEXT_67 = NL + "        " + NL + "    \tconnection_";
  protected final String TEXT_68 = " = org.talend.sap.impl.SAPConnectionFactory.getInstance().createConnection(properties_";
  protected final String TEXT_69 = ");";
  protected final String TEXT_70 = NL + "\t\t\t//{" + NL + "\t\t\t}";
  protected final String TEXT_71 = NL + "\t\t" + NL + "\torg.talend.sap.service.ISAPODPService dataService_";
  protected final String TEXT_72 = " = connection_";
  protected final String TEXT_73 = ".getODPService();" + NL + "\t" + NL + "  int nb_line_";
  protected final String TEXT_74 = " = 0;" + NL + "  " + NL + "\ttry {" + NL + "\t\t" + NL + "\t\torg.talend.sap.model.odp.ISAPDataServiceRequest request_";
  protected final String TEXT_75 = " = dataService_";
  protected final String TEXT_76 = ".createDataServiceRequest(";
  protected final String TEXT_77 = ");";
  protected final String TEXT_78 = NL + "\t\t\trequest_";
  protected final String TEXT_79 = ".subscriberName(";
  protected final String TEXT_80 = ");" + NL + "\t\t\trequest_";
  protected final String TEXT_81 = ".subscriberProcessName(";
  protected final String TEXT_82 = ");" + NL + "\t\t\trequest_";
  protected final String TEXT_83 = ".resetSubscription();" + NL + "\t\t\t";
  protected final String TEXT_84 = NL + "\t\t\torg.talend.sap.service.ISAPODPService metadataService_";
  protected final String TEXT_85 = " = connection_";
  protected final String TEXT_86 = ".getODPService();" + NL + "\t\t\torg.talend.sap.model.odp.ISAPDataServiceMetadata detail_";
  protected final String TEXT_87 = " = metadataService_";
  protected final String TEXT_88 = ".getDataServiceMetadataByName(";
  protected final String TEXT_89 = ");" + NL + "\t\t\t" + NL + "\t\t\tjava.util.List<org.talend.sap.model.odp.ISAPDataServiceField> fields_";
  protected final String TEXT_90 = " = detail_";
  protected final String TEXT_91 = ".getFields();" + NL + "\t\t\t" + NL + "\t\t\troutines.system.Dynamic dcg_";
  protected final String TEXT_92 = " =  new routines.system.Dynamic();" + NL + "\t\t\tjava.util.List<String> listSchema_";
  protected final String TEXT_93 = " = new java.util.ArrayList<String>();";
  protected final String TEXT_94 = NL + "    \t\t\t\tlistSchema_";
  protected final String TEXT_95 = ".add(\"";
  protected final String TEXT_96 = "\");";
  protected final String TEXT_97 = "\t\t\t" + NL + "\t\t\t//prepare the dynamic metadata" + NL + "\t\t\tfor(org.talend.sap.model.odp.ISAPDataServiceField field_";
  protected final String TEXT_98 = " : fields_";
  protected final String TEXT_99 = ") {" + NL + "\t\t\t\tif(listSchema_";
  protected final String TEXT_100 = ".contains(field_";
  protected final String TEXT_101 = ".getName())) {" + NL + "\t\t\t\t\t//do nothing" + NL + "\t\t\t\t} else {" + NL + "\t\t\t\t\t//Component doesnt support Dynamic Schema values" + NL + "\t\t\t\t\t/*" + NL + "\t\t\t\t\t//dynamic columns" + NL + "\t\t\t\t\troutines.system.DynamicMetadata dcm_";
  protected final String TEXT_102 = " = new routines.system.DynamicMetadata();" + NL + "\t\t\t\t\t" + NL + "\t\t\t\t\tdcm_";
  protected final String TEXT_103 = ".setName(field_";
  protected final String TEXT_104 = ".getNameForTalend());" + NL + "                \tdcm_";
  protected final String TEXT_105 = ".setDbName(field_";
  protected final String TEXT_106 = ".getName());" + NL + "                \tdcm_";
  protected final String TEXT_107 = ".setType(routines.system.Dynamic.getTalendTypeFromDBType(\"sap_id\", field_";
  protected final String TEXT_108 = ".getType().name(), field_";
  protected final String TEXT_109 = ".getLength(), field_";
  protected final String TEXT_110 = ".getScale()));" + NL + "                \tdcm_";
  protected final String TEXT_111 = ".setDbType(field_";
  protected final String TEXT_112 = ".getType().name());" + NL + "                \t*/" + NL + "                \t";
  protected final String TEXT_113 = NL + "                \t//dcm_";
  protected final String TEXT_114 = ".setFormat(";
  protected final String TEXT_115 = ");" + NL + "                \t";
  protected final String TEXT_116 = NL + "\t\t\t\t\t/*" + NL + "                \tdcm_";
  protected final String TEXT_117 = ".setLength(field_";
  protected final String TEXT_118 = ".getLength() == null ? 100 : field_";
  protected final String TEXT_119 = ".getLength());" + NL + "                \tdcm_";
  protected final String TEXT_120 = ".setPrecision(field_";
  protected final String TEXT_121 = ".getScale() == null ? 0 : field_";
  protected final String TEXT_122 = ".getScale());" + NL + "                \tdcm_";
  protected final String TEXT_123 = ".setNullable(true);" + NL + "                \tdcm_";
  protected final String TEXT_124 = ".setKey(field_";
  protected final String TEXT_125 = ".isKey());" + NL + "                \tdcm_";
  protected final String TEXT_126 = ".setSourceType(DynamicMetadata.sourceTypes.sap);" + NL + "                \t" + NL + "                \tdcg_";
  protected final String TEXT_127 = ".metadatas.add(dcm_";
  protected final String TEXT_128 = ");" + NL + "\t\t\t\t\t*/" + NL + "\t\t\t\t}" + NL + "\t\t\t}" + NL + "\t\t\t";
  protected final String TEXT_129 = NL + "\t\t\trequest_";
  protected final String TEXT_130 = ".addSelection(\"";
  protected final String TEXT_131 = "\".trim(), org.talend.sap.model.SAPSign.";
  protected final String TEXT_132 = ", org.talend.sap.model.SAPComparisonOperator.";
  protected final String TEXT_133 = ", ";
  protected final String TEXT_134 = ", ";
  protected final String TEXT_135 = "null";
  protected final String TEXT_136 = ");" + NL + "\t\t\t";
  protected final String TEXT_137 = "\t\t" + NL + "\t\tint fetchSize_";
  protected final String TEXT_138 = " = ";
  protected final String TEXT_139 = ";" + NL + "\t\t" + NL + "\t\tif(fetchSize_";
  protected final String TEXT_140 = " > 0) {" + NL + "\t\t\trequest_";
  protected final String TEXT_141 = ".fetchSize(fetchSize_";
  protected final String TEXT_142 = ");" + NL + "\t\t}" + NL + "\t\t" + NL + "\t\trequest_";
  protected final String TEXT_143 = ".subscriberName(";
  protected final String TEXT_144 = ");" + NL + "\t\trequest_";
  protected final String TEXT_145 = ".subscriberProcessName(";
  protected final String TEXT_146 = ");";
  protected final String TEXT_147 = " " + NL + "final String decrypted_ftp_pwd_";
  protected final String TEXT_148 = " = routines.system.PasswordEncryptUtil.decryptPassword(";
  protected final String TEXT_149 = ");";
  protected final String TEXT_150 = NL + "final String decrypted_ftp_pwd_";
  protected final String TEXT_151 = " = ";
  protected final String TEXT_152 = "; ";
  protected final String TEXT_153 = NL + "request_";
  protected final String TEXT_154 = ".ftp(org.talend.sap.model.FTP_MODE.";
  protected final String TEXT_155 = ", ";
  protected final String TEXT_156 = ", ";
  protected final String TEXT_157 = ", ";
  protected final String TEXT_158 = ", decrypted_ftp_pwd_";
  protected final String TEXT_159 = ");";
  protected final String TEXT_160 = "org.talend.sap.model.table.ISAPBatchData data_";
  protected final String TEXT_161 = " = request_";
  protected final String TEXT_162 = ".fullExtraction(";
  protected final String TEXT_163 = ", ";
  protected final String TEXT_164 = ");";
  protected final String TEXT_165 = "org.talend.sap.IGenericDataAccess        data_";
  protected final String TEXT_166 = " = request_";
  protected final String TEXT_167 = ".fullExtraction();";
  protected final String TEXT_168 = "org.talend.sap.model.table.ISAPBatchData data_";
  protected final String TEXT_169 = " = request_";
  protected final String TEXT_170 = ".deltaExtraction(";
  protected final String TEXT_171 = ", ";
  protected final String TEXT_172 = ");";
  protected final String TEXT_173 = "org.talend.sap.IGenericDataAccess        data_";
  protected final String TEXT_174 = " = request_";
  protected final String TEXT_175 = ".deltaExtraction();";
  protected final String TEXT_176 = NL + "    resourceMap.put(\"data_";
  protected final String TEXT_177 = "\", data_";
  protected final String TEXT_178 = ");" + NL + "    " + NL + "\t  while (data_";
  protected final String TEXT_179 = ".nextRow()) {";
  protected final String TEXT_180 = NL + "    while (data_";
  protected final String TEXT_181 = ".hasNextRow()) {";
  protected final String TEXT_182 = NL + "\t\t\t/*" + NL + "\t\t\t";
  protected final String TEXT_183 = ".";
  protected final String TEXT_184 = " = dcg_";
  protected final String TEXT_185 = ";" + NL + "" + NL + "\t\t\tdcg_";
  protected final String TEXT_186 = ".clearColumnValues();" + NL + "\t\t\tfor (int i_";
  protected final String TEXT_187 = " = 0; i_";
  protected final String TEXT_188 = " < dcg_";
  protected final String TEXT_189 = ".getColumnCount(); i_";
  protected final String TEXT_190 = "++) {" + NL + "                routines.system.DynamicMetadata dcm_";
  protected final String TEXT_191 = " = dcg_";
  protected final String TEXT_192 = ".getColumnMetadata(i_";
  protected final String TEXT_193 = ");" + NL + "    \t\t\tObject value_";
  protected final String TEXT_194 = " = null;" + NL + "    \t\t\tint currentIndex_";
  protected final String TEXT_195 = " = ";
  protected final String TEXT_196 = " + i_";
  protected final String TEXT_197 = ";" + NL + "    \t\t\t" + NL + "                if (\"id_String\".equals(dcm_";
  protected final String TEXT_198 = ".getType())) {" + NL + "                    value_";
  protected final String TEXT_199 = " = data_";
  protected final String TEXT_200 = ".getString(currentIndex_";
  protected final String TEXT_201 = ");" + NL + "                } else if (\"id_Integer\".equals(dcm_";
  protected final String TEXT_202 = ".getType())) {" + NL + "            \t\tvalue_";
  protected final String TEXT_203 = " = data_";
  protected final String TEXT_204 = ".getInteger(currentIndex_";
  protected final String TEXT_205 = ");" + NL + "                } else if (\"id_Long\".equals(dcm_";
  protected final String TEXT_206 = ".getType())) {" + NL + "            \t\tvalue_";
  protected final String TEXT_207 = " = data_";
  protected final String TEXT_208 = ".getLong(currentIndex_";
  protected final String TEXT_209 = ");" + NL + "                } else if (\"id_Short\".equals(dcm_";
  protected final String TEXT_210 = ".getType())) {" + NL + "            \t\tvalue_";
  protected final String TEXT_211 = " = data_";
  protected final String TEXT_212 = ".getShort(currentIndex_";
  protected final String TEXT_213 = ");" + NL + "                } else if (\"id_Date\".equals(dcm_";
  protected final String TEXT_214 = ".getType())) {" + NL + "            \t\tvalue_";
  protected final String TEXT_215 = " = data_";
  protected final String TEXT_216 = ".getDate(currentIndex_";
  protected final String TEXT_217 = ");" + NL + "                } else if (\"id_Byte\".equals(dcm_";
  protected final String TEXT_218 = ".getType())) {" + NL + "            \t\tvalue_";
  protected final String TEXT_219 = " = data_";
  protected final String TEXT_220 = ".getByte(currentIndex_";
  protected final String TEXT_221 = ");" + NL + "                } else if (\"id_byte[]\".equals(dcm_";
  protected final String TEXT_222 = ".getType())) {" + NL + "            \t\tvalue_";
  protected final String TEXT_223 = " = data_";
  protected final String TEXT_224 = ".getRaw(currentIndex_";
  protected final String TEXT_225 = ");" + NL + "                } else if (\"id_Double\".equals(dcm_";
  protected final String TEXT_226 = ".getType())) {" + NL + "            \t\tvalue_";
  protected final String TEXT_227 = " = data_";
  protected final String TEXT_228 = ".getDouble(currentIndex_";
  protected final String TEXT_229 = ");" + NL + "                } else if (\"id_Float\".equals(dcm_";
  protected final String TEXT_230 = ".getType())) {" + NL + "            \t\tvalue_";
  protected final String TEXT_231 = " = data_";
  protected final String TEXT_232 = ".getFloat(currentIndex_";
  protected final String TEXT_233 = ");" + NL + "                } else if (\"id_BigDecimal\".equals(dcm_";
  protected final String TEXT_234 = ".getType())) {" + NL + "                \tif(\"BIG_INTEGER\".equals(dcm_";
  protected final String TEXT_235 = ".getDbType())) {" + NL + "                \t\tvalue_";
  protected final String TEXT_236 = " = new java.math.BigDecimal(data_";
  protected final String TEXT_237 = ".getBigInteger(currentIndex_";
  protected final String TEXT_238 = "));" + NL + "                \t} else {" + NL + "                \t\tvalue_";
  protected final String TEXT_239 = " = data_";
  protected final String TEXT_240 = ".getBigDecimal(currentIndex_";
  protected final String TEXT_241 = ");" + NL + "                \t}" + NL + "                } else if (\"id_Object\".equals(dcm_";
  protected final String TEXT_242 = ".getType())) {" + NL + "                \tif(\"BIG_INTEGER\".equals(dcm_";
  protected final String TEXT_243 = ".getDbType())) {" + NL + "                \t\tvalue_";
  protected final String TEXT_244 = " = data_";
  protected final String TEXT_245 = ".getBigInteger(currentIndex_";
  protected final String TEXT_246 = ");" + NL + "                \t} else {" + NL + "                \t\tvalue_";
  protected final String TEXT_247 = " = data_";
  protected final String TEXT_248 = ".getString(currentIndex_";
  protected final String TEXT_249 = ");" + NL + "                \t}" + NL + "                } else {" + NL + "                \t//no other possible as the talend type come from the mapping_sap.xml, so do nothing" + NL + "                }" + NL + "                " + NL + "                dcg_";
  protected final String TEXT_250 = ".addColumnValue(value_";
  protected final String TEXT_251 = ");" + NL + "        \t}" + NL + "\t\t\t*/";
  protected final String TEXT_252 = NL + "\t\t\t";
  protected final String TEXT_253 = ".";
  protected final String TEXT_254 = " = data_";
  protected final String TEXT_255 = ".getString(\"";
  protected final String TEXT_256 = "\");";
  protected final String TEXT_257 = NL + "\t\t\t";
  protected final String TEXT_258 = ".";
  protected final String TEXT_259 = " = data_";
  protected final String TEXT_260 = ".getInteger(\"";
  protected final String TEXT_261 = "\");";
  protected final String TEXT_262 = NL + "\t\t\t";
  protected final String TEXT_263 = ".";
  protected final String TEXT_264 = " = data_";
  protected final String TEXT_265 = ".getLong(\"";
  protected final String TEXT_266 = "\");";
  protected final String TEXT_267 = NL + "\t\t\t";
  protected final String TEXT_268 = ".";
  protected final String TEXT_269 = " = data_";
  protected final String TEXT_270 = ".getShort(\"";
  protected final String TEXT_271 = "\");";
  protected final String TEXT_272 = NL + "\t\t\t";
  protected final String TEXT_273 = ".";
  protected final String TEXT_274 = " = data_";
  protected final String TEXT_275 = ".getDate(\"";
  protected final String TEXT_276 = "\");";
  protected final String TEXT_277 = NL + "\t\t\t";
  protected final String TEXT_278 = ".";
  protected final String TEXT_279 = " = data_";
  protected final String TEXT_280 = ".getByte(\"";
  protected final String TEXT_281 = "\");";
  protected final String TEXT_282 = NL + "\t\t\t";
  protected final String TEXT_283 = ".";
  protected final String TEXT_284 = " = data_";
  protected final String TEXT_285 = ".getRaw(\"";
  protected final String TEXT_286 = "\");";
  protected final String TEXT_287 = NL + "\t\t\t";
  protected final String TEXT_288 = ".";
  protected final String TEXT_289 = " = data_";
  protected final String TEXT_290 = ".getDouble(\"";
  protected final String TEXT_291 = "\");";
  protected final String TEXT_292 = NL + "\t\t\t";
  protected final String TEXT_293 = ".";
  protected final String TEXT_294 = " = data_";
  protected final String TEXT_295 = ".getFloat(\"";
  protected final String TEXT_296 = "\");";
  protected final String TEXT_297 = NL + "\t\t\tjava.math.BigInteger bi_";
  protected final String TEXT_298 = "_";
  protected final String TEXT_299 = " = data_";
  protected final String TEXT_300 = ".getBigInteger(\"";
  protected final String TEXT_301 = "\");" + NL + "\t\t\t";
  protected final String TEXT_302 = ".";
  protected final String TEXT_303 = " = bi_";
  protected final String TEXT_304 = "_";
  protected final String TEXT_305 = "==null ? null : new java.math.BigDecimal(bi_";
  protected final String TEXT_306 = "_";
  protected final String TEXT_307 = ");";
  protected final String TEXT_308 = NL + "\t\t\t";
  protected final String TEXT_309 = ".";
  protected final String TEXT_310 = " = data_";
  protected final String TEXT_311 = ".getBigDecimal(\"";
  protected final String TEXT_312 = "\");";
  protected final String TEXT_313 = NL + "\t\t\t";
  protected final String TEXT_314 = ".";
  protected final String TEXT_315 = " = data_";
  protected final String TEXT_316 = ".getBigInteger(\"";
  protected final String TEXT_317 = "\");\t\t";
  protected final String TEXT_318 = NL + "\t\t\t";
  protected final String TEXT_319 = ".";
  protected final String TEXT_320 = " = data_";
  protected final String TEXT_321 = ".getString(\"";
  protected final String TEXT_322 = "\");";
  protected final String TEXT_323 = NL + "\t\t\t";
  protected final String TEXT_324 = ".";
  protected final String TEXT_325 = " = ParserUtils.parseTo_";
  protected final String TEXT_326 = "(data_";
  protected final String TEXT_327 = ".getString(\"";
  protected final String TEXT_328 = "\"));";
  protected final String TEXT_329 = NL;

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    stringBuffer.append(TEXT_1);
    
	CodeGeneratorArgument codeGenArgument = (CodeGeneratorArgument) argument;
	INode node = (INode)codeGenArgument.getArgument();
	String cid = node.getUniqueName();
	
	List<? extends IConnection> outputConnections = node.getOutgoingSortedConnections();
	if((outputConnections == null) || (outputConnections.size() == 0)) {
		return "";
	}
	IConnection outputConnection = outputConnections.get(0);
	
	if(!outputConnection.getLineStyle().hasConnectionCategory(IConnectionCategory.DATA)) {
		return "";
	}
	
	List<IMetadataTable> metadatas = node.getMetadataList();
	if ((metadatas == null) && (metadatas.size() == 0) || (metadatas.get(0) == null)) {
		return "";
	}
	IMetadataTable metadata = metadatas.get(0);
	
	List<IMetadataColumn> columnList = metadata.getListColumns();
	if((columnList == null) || (columnList.size() == 0)) {
		return "";
	}
	
	String client = ElementParameterParser.getValue(node, "__CLIENT__");
	String userid = ElementParameterParser.getValue(node, "__USERID__");
	String password = ElementParameterParser.getValue(node, "__PASSWORD__");
	String language = ElementParameterParser.getValue(node, "__LANGUAGE__");
	String hostname = ElementParameterParser.getValue(node, "__HOSTNAME__");
	String systemnumber = ElementParameterParser.getValue(node, "__SYSTEMNUMBER__");
	
	String systemId = ElementParameterParser.getValue(node,"__SYSTEMID__");
	String groupName = ElementParameterParser.getValue(node,"__GROUPNAME__");
	
	String serverType = ElementParameterParser.getValue(node,"__SERVERTYPE__");
	
	String serviceName = ElementParameterParser.getValue(node, "__TABLE__");
	
	String fetchSize = ElementParameterParser.getValue(node, "__FETCH_SIZE__");
	
	String file_protocol = ElementParameterParser.getValue(node,"__FILE_PROTOCOL__");
	
	List<Map<String, String>> sapProps = (List<Map<String,String>>)ElementParameterParser.getObjectValue(node, "__SAP_PROPERTIES__");
	
	String passwordFieldName = "__PASSWORD__";
	
    boolean useExistingConn = ("true").equals(ElementParameterParser.getValue(node,"__USE_EXISTING_CONNECTION__"));
	String connection = ElementParameterParser.getValue(node,"__CONNECTION__");

    stringBuffer.append(TEXT_2);
    if(useExistingConn){
    stringBuffer.append(TEXT_3);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_4);
    stringBuffer.append(connection );
    stringBuffer.append(TEXT_5);
    	
		INode connectionNode = null; 
		for (INode processNode : node.getProcess().getGeneratingNodes()) { 
			if(connection.equals(processNode.getUniqueName())) { 
				connectionNode = processNode; 
				break; 
			} 
		} 
		boolean specify_alias = "true".equals(ElementParameterParser.getValue(connectionNode, "__SPECIFY_DATASOURCE_ALIAS__"));
		if(specify_alias){
			String alias = ElementParameterParser.getValue(connectionNode, "__SAP_DATASOURCE_ALIAS__");
			if(null != alias && !("".equals(alias))){

    stringBuffer.append(TEXT_6);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_7);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_8);
    stringBuffer.append(alias);
    stringBuffer.append(TEXT_9);
    
			}
		}

    stringBuffer.append(TEXT_10);
    }else{
    stringBuffer.append(TEXT_11);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_12);
    
		boolean specify_alias = "true".equals(ElementParameterParser.getValue(node, "__SPECIFY_DATASOURCE_ALIAS__"));
		if(specify_alias){
			String alias = ElementParameterParser.getValue(node, "__SAP_DATASOURCE_ALIAS__");
			if(null != alias && !("".equals(alias))){

    stringBuffer.append(TEXT_13);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_14);
    stringBuffer.append(alias);
    stringBuffer.append(TEXT_15);
    
			}

    stringBuffer.append(TEXT_16);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_17);
    
		}

    stringBuffer.append(TEXT_18);
    stringBuffer.append(TEXT_19);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_20);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_21);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_22);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_23);
    if (ElementParameterParser.canEncrypt(node, passwordFieldName)) {
    stringBuffer.append(TEXT_24);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_25);
    stringBuffer.append(ElementParameterParser.getEncryptedValue(node, passwordFieldName));
    stringBuffer.append(TEXT_26);
    } else {
    stringBuffer.append(TEXT_27);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_28);
    stringBuffer.append( ElementParameterParser.getValue(node, passwordFieldName));
    stringBuffer.append(TEXT_29);
    }
    stringBuffer.append(TEXT_30);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_31);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_32);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_33);
    stringBuffer.append(client);
    stringBuffer.append(TEXT_34);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_35);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_36);
    stringBuffer.append(userid);
    stringBuffer.append(TEXT_37);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_38);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_39);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_40);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_41);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_42);
    stringBuffer.append(language);
    stringBuffer.append(TEXT_43);
    if("ApplicationServer".equals(serverType)){
    stringBuffer.append(TEXT_44);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_45);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_46);
    stringBuffer.append(hostname);
    stringBuffer.append(TEXT_47);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_48);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_49);
    stringBuffer.append(systemnumber);
    stringBuffer.append(TEXT_50);
    }else{
    stringBuffer.append(TEXT_51);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_52);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_53);
    stringBuffer.append(hostname);
    stringBuffer.append(TEXT_54);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_55);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_56);
    stringBuffer.append(systemId);
    stringBuffer.append(TEXT_57);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_58);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_59);
    stringBuffer.append(groupName);
    stringBuffer.append(TEXT_60);
    }
    stringBuffer.append(TEXT_61);
    
    if(sapProps!=null) {
		for(Map<String, String> item : sapProps){
		
    stringBuffer.append(TEXT_62);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_63);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_64);
    stringBuffer.append(item.get("PROPERTY") );
    stringBuffer.append(TEXT_65);
    stringBuffer.append(item.get("VALUE") );
    stringBuffer.append(TEXT_66);
     
		}
    }
	
    stringBuffer.append(TEXT_67);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_68);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_69);
    
		if(specify_alias){

    stringBuffer.append(TEXT_70);
    
		}
	}

    stringBuffer.append(TEXT_71);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_72);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_73);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_74);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_75);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_76);
    stringBuffer.append(serviceName);
    stringBuffer.append(TEXT_77);
    
		boolean fullExtraction = "true".equals(ElementParameterParser.getValue(node, "__EXTRACTION_FULL__"));
		boolean resetSubscriber = "true".equals(ElementParameterParser.getValue(node, "__RESET_SUBSCRIBER__"));
		String subscriber = ElementParameterParser.getValue(node, "__SUBSCRIBER_NAME__");
		String subscriber_process = ElementParameterParser.getValue(node, "__SUBSCRIBER_PROCESS_NAME__");
	
		if(resetSubscriber && !fullExtraction){

    stringBuffer.append(TEXT_78);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_79);
    stringBuffer.append(subscriber);
    stringBuffer.append(TEXT_80);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_81);
    stringBuffer.append(subscriber_process);
    stringBuffer.append(TEXT_82);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_83);
    
		}

    	boolean isDynamic = metadata.isDynamicSchema();
    	int dynamicColumnIndex = -1;
    	if(isDynamic) {

    stringBuffer.append(TEXT_84);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_85);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_86);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_87);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_88);
    stringBuffer.append(serviceName);
    stringBuffer.append(TEXT_89);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_90);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_91);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_92);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_93);
    
			IMetadataColumn dynamicColumn = null;
        	for(int i=0;i<columnList.size();i++) {
                IMetadataColumn column = columnList.get(i);
                if("id_Dynamic".equals(column.getTalendType())) {
        			dynamicColumn = column;
        			dynamicColumnIndex = i;
    			} else {
    				String tableField = column.getOriginalDbColumnName();

    stringBuffer.append(TEXT_94);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_95);
    stringBuffer.append(tableField);
    stringBuffer.append(TEXT_96);
    
    			}
    		}

    stringBuffer.append(TEXT_97);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_98);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_99);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_100);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_101);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_102);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_103);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_104);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_105);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_106);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_107);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_108);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_109);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_110);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_111);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_112);
    
                	String datePattern4DynamicColumn = dynamicColumn.getPattern();
                	if(datePattern4DynamicColumn!=null && !"".equals(datePattern4DynamicColumn) && !"\"\"".equals(datePattern4DynamicColumn)) {
                	
    stringBuffer.append(TEXT_113);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_114);
    stringBuffer.append(datePattern4DynamicColumn);
    stringBuffer.append(TEXT_115);
    
                	}
                	
    stringBuffer.append(TEXT_116);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_117);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_118);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_119);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_120);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_121);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_122);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_123);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_124);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_125);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_126);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_127);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_128);
    
		}
		
		List<Map<String, String>> selection = (List<Map<String,String>>)ElementParameterParser.getObjectValue(node,  "__SELECTION_TABLE__");
		if(selection == null) {
			selection = new ArrayList<Map<String, String>>();
		}
		for (Map<String, String> element : selection) {
			String field = element.get("SELECTION_COLUMN");
			String sign = element.get("SIGN");
			String operator = element.get("OPERATOR");
			String lowValue = element.get("LOWVALUE");
			String highValue = element.get("HIGHVALUE");
			
    stringBuffer.append(TEXT_129);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_130);
    stringBuffer.append(field);
    stringBuffer.append(TEXT_131);
    stringBuffer.append(sign);
    stringBuffer.append(TEXT_132);
    stringBuffer.append(operator);
    stringBuffer.append(TEXT_133);
    stringBuffer.append(lowValue);
    stringBuffer.append(TEXT_134);
    if(operator!=null && operator.contains("BETWEEN")){
    stringBuffer.append(highValue);
    } else {
    stringBuffer.append(TEXT_135);
    }
    stringBuffer.append(TEXT_136);
    
		}

    stringBuffer.append(TEXT_137);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_138);
    stringBuffer.append(fetchSize);
    stringBuffer.append(TEXT_139);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_140);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_141);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_142);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_143);
    stringBuffer.append(subscriber);
    stringBuffer.append(TEXT_144);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_145);
    stringBuffer.append(subscriber_process);
    stringBuffer.append(TEXT_146);
    

  // FTP support
  boolean useFtp = "true".equals(ElementParameterParser.getValue(node,"__USE_FTP_BATCH__"));
  String ftp_host = ElementParameterParser.getValue(node, "__FTP_HOST__");
  String ftp_port = ElementParameterParser.getValue(node, "__FTP_PORT__");
  String ftp_user = ElementParameterParser.getValue(node, "__FTP_USER__");
  
  String ftp_dir = ElementParameterParser.getValue(node, "__FTP_DIR__");
  String filename_prefix = ElementParameterParser.getValue(node, "__GENERATED_FILENAME_PREFIX__");
  
  String ftp_pass_field_name = "__FTP_PASS__";
  
  if (useFtp) {
		if (ElementParameterParser.canEncrypt(node, ftp_pass_field_name)) {

    stringBuffer.append(TEXT_147);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_148);
    stringBuffer.append(ElementParameterParser.getEncryptedValue(node, ftp_pass_field_name));
    stringBuffer.append(TEXT_149);
    	} else { 
    stringBuffer.append(TEXT_150);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_151);
    stringBuffer.append( ElementParameterParser.getValue(node, ftp_pass_field_name));
    stringBuffer.append(TEXT_152);
     	}

    stringBuffer.append(TEXT_153);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_154);
    stringBuffer.append(file_protocol);
    stringBuffer.append(TEXT_155);
    stringBuffer.append(ftp_host);
    stringBuffer.append(TEXT_156);
    stringBuffer.append(ftp_port);
    stringBuffer.append(TEXT_157);
    stringBuffer.append(ftp_user);
    stringBuffer.append(TEXT_158);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_159);
    
  }
  
  if (useFtp && fullExtraction) { 
    stringBuffer.append(TEXT_160);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_161);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_162);
    stringBuffer.append(ftp_dir);
    stringBuffer.append(TEXT_163);
    stringBuffer.append(filename_prefix);
    stringBuffer.append(TEXT_164);
    
  } else if (fullExtraction) {    
    stringBuffer.append(TEXT_165);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_166);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_167);
    
  } else if (useFtp) {            
    stringBuffer.append(TEXT_168);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_169);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_170);
    stringBuffer.append(ftp_dir);
    stringBuffer.append(TEXT_171);
    stringBuffer.append(filename_prefix);
    stringBuffer.append(TEXT_172);
    
	} else {                        
    stringBuffer.append(TEXT_173);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_174);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_175);
    
  }
  
  if (useFtp) {

    stringBuffer.append(TEXT_176);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_177);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_178);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_179);
     } else { 
    stringBuffer.append(TEXT_180);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_181);
    
  }


	for(int i=0;i<columnList.size();i++) {
		IMetadataColumn column = columnList.get(i);
	    String typeToGenerate = JavaTypesManager.getTypeToGenerate(column.getTalendType(),column.isNullable());
	    JavaType javaType = JavaTypesManager.getJavaTypeFromId(column.getTalendType());
	    String dbType = column.getType();
		String tableField = column.getOriginalDbColumnName();
	    
		//Component doesnt support Dynamic Schema values
	    if(dynamicColumnIndex == i) {//dynamic column
	    	int startIndex4DynamicColumn = columnList.size() - 1;

    stringBuffer.append(TEXT_182);
    stringBuffer.append(outputConnection.getName());
    stringBuffer.append(TEXT_183);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_184);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_185);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_186);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_187);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_188);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_189);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_190);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_191);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_192);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_193);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_194);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_195);
    stringBuffer.append(startIndex4DynamicColumn);
    stringBuffer.append(TEXT_196);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_197);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_198);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_199);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_200);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_201);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_202);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_203);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_204);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_205);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_206);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_207);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_208);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_209);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_210);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_211);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_212);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_213);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_214);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_215);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_216);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_217);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_218);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_219);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_220);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_221);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_222);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_223);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_224);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_225);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_226);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_227);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_228);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_229);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_230);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_231);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_232);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_233);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_234);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_235);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_236);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_237);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_238);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_239);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_240);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_241);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_242);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_243);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_244);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_245);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_246);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_247);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_248);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_249);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_250);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_251);
    
	    	continue;
	    }
	    
		if(javaType == JavaTypesManager.STRING) {

    stringBuffer.append(TEXT_252);
    stringBuffer.append(outputConnection.getName() );
    stringBuffer.append(TEXT_253);
    stringBuffer.append(column.getLabel() );
    stringBuffer.append(TEXT_254);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_255);
    stringBuffer.append(tableField);
    stringBuffer.append(TEXT_256);
    
		} else if(javaType == JavaTypesManager.INTEGER) {

    stringBuffer.append(TEXT_257);
    stringBuffer.append(outputConnection.getName() );
    stringBuffer.append(TEXT_258);
    stringBuffer.append(column.getLabel() );
    stringBuffer.append(TEXT_259);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_260);
    stringBuffer.append(tableField);
    stringBuffer.append(TEXT_261);
    
		} else if(javaType == JavaTypesManager.LONG) {

    stringBuffer.append(TEXT_262);
    stringBuffer.append(outputConnection.getName() );
    stringBuffer.append(TEXT_263);
    stringBuffer.append(column.getLabel() );
    stringBuffer.append(TEXT_264);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_265);
    stringBuffer.append(tableField);
    stringBuffer.append(TEXT_266);
    
		} else if(javaType == JavaTypesManager.SHORT) {

    stringBuffer.append(TEXT_267);
    stringBuffer.append(outputConnection.getName() );
    stringBuffer.append(TEXT_268);
    stringBuffer.append(column.getLabel() );
    stringBuffer.append(TEXT_269);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_270);
    stringBuffer.append(tableField);
    stringBuffer.append(TEXT_271);
    
		} else if(javaType == JavaTypesManager.DATE) {

    stringBuffer.append(TEXT_272);
    stringBuffer.append(outputConnection.getName() );
    stringBuffer.append(TEXT_273);
    stringBuffer.append(column.getLabel() );
    stringBuffer.append(TEXT_274);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_275);
    stringBuffer.append(tableField);
    stringBuffer.append(TEXT_276);
    
		} else if(javaType == JavaTypesManager.BYTE) {

    stringBuffer.append(TEXT_277);
    stringBuffer.append(outputConnection.getName() );
    stringBuffer.append(TEXT_278);
    stringBuffer.append(column.getLabel() );
    stringBuffer.append(TEXT_279);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_280);
    stringBuffer.append(tableField);
    stringBuffer.append(TEXT_281);
    
		} else if(javaType == JavaTypesManager.BYTE_ARRAY) {

    stringBuffer.append(TEXT_282);
    stringBuffer.append(outputConnection.getName() );
    stringBuffer.append(TEXT_283);
    stringBuffer.append(column.getLabel() );
    stringBuffer.append(TEXT_284);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_285);
    stringBuffer.append(tableField);
    stringBuffer.append(TEXT_286);
    
		} else if(javaType == JavaTypesManager.DOUBLE) {

    stringBuffer.append(TEXT_287);
    stringBuffer.append(outputConnection.getName() );
    stringBuffer.append(TEXT_288);
    stringBuffer.append(column.getLabel() );
    stringBuffer.append(TEXT_289);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_290);
    stringBuffer.append(tableField);
    stringBuffer.append(TEXT_291);
    
		} else if(javaType == JavaTypesManager.FLOAT) {

    stringBuffer.append(TEXT_292);
    stringBuffer.append(outputConnection.getName() );
    stringBuffer.append(TEXT_293);
    stringBuffer.append(column.getLabel() );
    stringBuffer.append(TEXT_294);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_295);
    stringBuffer.append(tableField);
    stringBuffer.append(TEXT_296);
    
		} else if(javaType == JavaTypesManager.BIGDECIMAL) {
			if("BIG_INTEGER".equals(dbType)) {

    stringBuffer.append(TEXT_297);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_298);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_299);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_300);
    stringBuffer.append(tableField);
    stringBuffer.append(TEXT_301);
    stringBuffer.append(outputConnection.getName() );
    stringBuffer.append(TEXT_302);
    stringBuffer.append(column.getLabel() );
    stringBuffer.append(TEXT_303);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_304);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_305);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_306);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_307);
    
			} else {

    stringBuffer.append(TEXT_308);
    stringBuffer.append(outputConnection.getName() );
    stringBuffer.append(TEXT_309);
    stringBuffer.append(column.getLabel() );
    stringBuffer.append(TEXT_310);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_311);
    stringBuffer.append(tableField);
    stringBuffer.append(TEXT_312);
    
			}
		} else if(javaType == JavaTypesManager.OBJECT && "BIG_INTEGER".equals(dbType)) {

    stringBuffer.append(TEXT_313);
    stringBuffer.append(outputConnection.getName() );
    stringBuffer.append(TEXT_314);
    stringBuffer.append(column.getLabel() );
    stringBuffer.append(TEXT_315);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_316);
    stringBuffer.append(tableField);
    stringBuffer.append(TEXT_317);
    
		} else if(javaType == JavaTypesManager.OBJECT) {

    stringBuffer.append(TEXT_318);
    stringBuffer.append(outputConnection.getName() );
    stringBuffer.append(TEXT_319);
    stringBuffer.append(column.getLabel() );
    stringBuffer.append(TEXT_320);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_321);
    stringBuffer.append(tableField);
    stringBuffer.append(TEXT_322);
    
		} else {

    stringBuffer.append(TEXT_323);
    stringBuffer.append(outputConnection.getName() );
    stringBuffer.append(TEXT_324);
    stringBuffer.append(column.getLabel() );
    stringBuffer.append(TEXT_325);
    stringBuffer.append(typeToGenerate);
    stringBuffer.append(TEXT_326);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_327);
    stringBuffer.append(tableField);
    stringBuffer.append(TEXT_328);
    
		}
	}

    stringBuffer.append(TEXT_329);
    return stringBuffer.toString();
  }
}
