package org.talend.designer.codegen.translators.data_quality.address.melissadata;

import org.talend.core.model.process.INode;
import org.talend.designer.codegen.config.CodeGeneratorArgument;
import org.talend.core.model.process.ElementParameterParser;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;

public class TMelissaDataAddressBeginJava
{
  protected static String nl;
  public static synchronized TMelissaDataAddressBeginJava create(String lineSeparator)
  {
    nl = lineSeparator;
    TMelissaDataAddressBeginJava result = new TMelissaDataAddressBeginJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "      " + NL + "\tcom.melissadata.mdAddr ao_";
  protected final String TEXT_2 = " = new com.melissadata.mdAddr();" + NL + "\t//test licensce" + NL + "\tboolean testao_";
  protected final String TEXT_3 = " = ao_";
  protected final String TEXT_4 = ".SetLicenseString(";
  protected final String TEXT_5 = ");" + NL + "\t" + NL + "\tcom.melissadata.mdGeo geocoder_";
  protected final String TEXT_6 = "=null;" + NL + "\tboolean testgeo_";
  protected final String TEXT_7 = "=false;" + NL + "\tif(";
  protected final String TEXT_8 = "|| ";
  protected final String TEXT_9 = "){" + NL + "\t   geocoder_";
  protected final String TEXT_10 = " =new com.melissadata.mdGeo();" + NL + "\t   testgeo_";
  protected final String TEXT_11 = " = geocoder_";
  protected final String TEXT_12 = ".SetLicenseString(";
  protected final String TEXT_13 = ");" + NL + "\t}" + NL + "\t" + NL + "\tcom.melissadata.mdRightFielder rightfielder_";
  protected final String TEXT_14 = "=null;" + NL + "\tboolean testrfielder_";
  protected final String TEXT_15 = "=false;" + NL + "\tif(";
  protected final String TEXT_16 = "){" + NL + "\t   rightfielder_";
  protected final String TEXT_17 = " = new com.melissadata.mdRightFielder();" + NL + "\t   testrfielder_";
  protected final String TEXT_18 = " = rightfielder_";
  protected final String TEXT_19 = ".SetLicenseString(";
  protected final String TEXT_20 = ");" + NL + "\t}   " + NL + "" + NL + "\t//set path to files" + NL + "\tao_";
  protected final String TEXT_21 = ".SetPathToUSFiles(";
  protected final String TEXT_22 = ");" + NL + "    ao_";
  protected final String TEXT_23 = ".SetPathToCanadaFiles(";
  protected final String TEXT_24 = ");" + NL + "    ao_";
  protected final String TEXT_25 = ".SetPathToDPVDataFiles(";
  protected final String TEXT_26 = ");" + NL + "    ao_";
  protected final String TEXT_27 = ".SetPathToLACSLinkDataFiles(";
  protected final String TEXT_28 = ");" + NL + "\tao_";
  protected final String TEXT_29 = ".SetPathToSuiteFinderDataFiles(";
  protected final String TEXT_30 = ");" + NL + "\tao_";
  protected final String TEXT_31 = ".SetPathToSuiteLinkDataFiles(";
  protected final String TEXT_32 = ");" + NL + "\tao_";
  protected final String TEXT_33 = ".SetPathToRBDIFiles(";
  protected final String TEXT_34 = ");" + NL + "" + NL + "\tif(";
  protected final String TEXT_35 = "|| ";
  protected final String TEXT_36 = "){" + NL + "\t  geocoder_";
  protected final String TEXT_37 = ".SetPathToGeoCanadaDataFiles(";
  protected final String TEXT_38 = ");" + NL + "\t  geocoder_";
  protected final String TEXT_39 = ".SetPathToGeoCodeDataFiles(";
  protected final String TEXT_40 = ");" + NL + "\t  geocoder_";
  protected final String TEXT_41 = ".SetPathToGeoPointDataFiles(";
  protected final String TEXT_42 = ");" + NL + "\t}" + NL + "" + NL + "    if(";
  protected final String TEXT_43 = "){" + NL + "      rightfielder_";
  protected final String TEXT_44 = ".SetPathToRightFielderFiles(";
  protected final String TEXT_45 = ");" + NL + "    }" + NL + "\t" + NL + "\t//Initialize Data Files" + NL + "\tcom.melissadata.mdAddr.ProgramStatus result_mdAddr = ao_";
  protected final String TEXT_46 = ".InitializeDataFiles();" + NL + "    if (result_mdAddr != com.melissadata.mdAddr.ProgramStatus.ErrorNone) {" + NL + "      System.err.println(\"Address Object Initialization Error: \" + ao_";
  protected final String TEXT_47 = ".GetInitializeErrorString());" + NL + "      return;" + NL + "    }" + NL + "    " + NL + "    " + NL + "    if(";
  protected final String TEXT_48 = "|| ";
  protected final String TEXT_49 = "){" + NL + "      com.melissadata.mdGeo.ProgramStatus result_mdGeo = geocoder_";
  protected final String TEXT_50 = ".InitializeDataFiles();" + NL + "      if (result_mdGeo != com.melissadata.mdGeo.ProgramStatus.ErrorNone) {" + NL + "        System.err.println(\"GeoCoder Initialization Error: \" + geocoder_";
  protected final String TEXT_51 = ".GetInitializeErrorString());" + NL + "      }" + NL + "    }" + NL + "    " + NL + "    if(";
  protected final String TEXT_52 = "){" + NL + "      com.melissadata.mdRightFielder.ProgramStatus result_mdRightFielder = rightfielder_";
  protected final String TEXT_53 = ".InitializeDataFiles();" + NL + "      if (result_mdRightFielder != com.melissadata.mdRightFielder.ProgramStatus.NoError) {" + NL + "        System.err.println(\"Right Fielder Object Initialization Error: \" + rightfielder_";
  protected final String TEXT_54 = ".GetInitializeErrorString());" + NL + "        return;" + NL + "      }" + NL + "    }" + NL + "    " + NL + "\tboolean useRightfielder";
  protected final String TEXT_55 = "=";
  protected final String TEXT_56 = ";" + NL + "\tboolean geopoint_owned__";
  protected final String TEXT_57 = "=";
  protected final String TEXT_58 = ";" + NL + "\tboolean geocode_owned__";
  protected final String TEXT_59 = "=";
  protected final String TEXT_60 = ";" + NL + "\tboolean geocode_no__";
  protected final String TEXT_61 = "=";
  protected final String TEXT_62 = ";" + NL + "\t" + NL + "\tboolean geocoder_exists_";
  protected final String TEXT_63 = "=false;" + NL + "\tif(";
  protected final String TEXT_64 = "|| ";
  protected final String TEXT_65 = "){" + NL + "\t   geocoder_exists_";
  protected final String TEXT_66 = "=geocoder_";
  protected final String TEXT_67 = ".GetInitializeErrorString().contains(\"No error\");" + NL + "\t}" + NL + "\tboolean rightfielder_exists_";
  protected final String TEXT_68 = "=false;" + NL + "\tif(";
  protected final String TEXT_69 = "){" + NL + "\t  rightfielder_exists_";
  protected final String TEXT_70 = "=rightfielder_";
  protected final String TEXT_71 = ".GetInitializeErrorString().contains(\"No Error\");" + NL + "\t}  ";

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    
	CodeGeneratorArgument codeGenArgument = (CodeGeneratorArgument) argument;
	INode node = (INode)codeGenArgument.getArgument();
	String cid = node.getUniqueName();
    
	String path = ElementParameterParser.getValue(node, "__PATH__");
	String license = ElementParameterParser.getValue(node, "__LICENSE__");
	String dataFileDir = ElementParameterParser.getValue(node, "__DATAFILE_DIR__");

    List<Map<String, String>> input_address = (List<Map<String, String>>)ElementParameterParser.getObjectValue(node, "__INPUT_ADDRESS__");
    List<Map<String, String>> output_address = (List<Map<String, String>>)ElementParameterParser.getObjectValue(node, "__OUTPUT_ADDRESS__");	
    
    // when the user selects only the single field: "Address", in this case, you must use RightFielder
    boolean useRightFielder = input_address!=null && input_address.size() == 1 && input_address.get(0).get("ADDRESS_FIELD").startsWith("Address");

	boolean GeoPointLicense = "true".equals(ElementParameterParser.getValue(node, "__GEOPOINT__"));
	boolean GeoCodeLicense = "true".equals(ElementParameterParser.getValue(node, "__GEOCODE__"));
	boolean NoGeoCodeLicense = "true".equals(ElementParameterParser.getValue(node, "__NOGEOCODE__"));

    stringBuffer.append(TEXT_1);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_2);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_3);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_4);
    stringBuffer.append( license );
    stringBuffer.append(TEXT_5);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_6);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_7);
    stringBuffer.append( GeoPointLicense );
    stringBuffer.append(TEXT_8);
    stringBuffer.append( GeoCodeLicense );
    stringBuffer.append(TEXT_9);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_10);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_11);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_12);
    stringBuffer.append( license );
    stringBuffer.append(TEXT_13);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_14);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_15);
    stringBuffer.append( useRightFielder );
    stringBuffer.append(TEXT_16);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_17);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_18);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_19);
    stringBuffer.append( license );
    stringBuffer.append(TEXT_20);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_21);
    stringBuffer.append( dataFileDir );
    stringBuffer.append(TEXT_22);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_23);
    stringBuffer.append( dataFileDir );
    stringBuffer.append(TEXT_24);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_25);
    stringBuffer.append( dataFileDir );
    stringBuffer.append(TEXT_26);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_27);
    stringBuffer.append( dataFileDir );
    stringBuffer.append(TEXT_28);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_29);
    stringBuffer.append( dataFileDir );
    stringBuffer.append(TEXT_30);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_31);
    stringBuffer.append( dataFileDir );
    stringBuffer.append(TEXT_32);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_33);
    stringBuffer.append( dataFileDir );
    stringBuffer.append(TEXT_34);
    stringBuffer.append( GeoPointLicense );
    stringBuffer.append(TEXT_35);
    stringBuffer.append( GeoCodeLicense );
    stringBuffer.append(TEXT_36);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_37);
    stringBuffer.append( dataFileDir );
    stringBuffer.append(TEXT_38);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_39);
    stringBuffer.append( dataFileDir );
    stringBuffer.append(TEXT_40);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_41);
    stringBuffer.append( dataFileDir );
    stringBuffer.append(TEXT_42);
    stringBuffer.append( useRightFielder );
    stringBuffer.append(TEXT_43);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_44);
    stringBuffer.append( dataFileDir );
    stringBuffer.append(TEXT_45);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_46);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_47);
    stringBuffer.append( GeoPointLicense );
    stringBuffer.append(TEXT_48);
    stringBuffer.append( GeoCodeLicense );
    stringBuffer.append(TEXT_49);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_50);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_51);
    stringBuffer.append( useRightFielder );
    stringBuffer.append(TEXT_52);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_53);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_54);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_55);
    stringBuffer.append( useRightFielder );
    stringBuffer.append(TEXT_56);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_57);
    stringBuffer.append( GeoPointLicense );
    stringBuffer.append(TEXT_58);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_59);
    stringBuffer.append( GeoCodeLicense );
    stringBuffer.append(TEXT_60);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_61);
    stringBuffer.append( NoGeoCodeLicense );
    stringBuffer.append(TEXT_62);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_63);
    stringBuffer.append( GeoPointLicense );
    stringBuffer.append(TEXT_64);
    stringBuffer.append( GeoCodeLicense );
    stringBuffer.append(TEXT_65);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_66);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_67);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_68);
    stringBuffer.append( useRightFielder );
    stringBuffer.append(TEXT_69);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_70);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_71);
    return stringBuffer.toString();
  }
}
