package org.talend.designer.codegen.translators.naturallanguageprocessing;

import org.talend.core.model.process.INode;
import org.talend.core.model.process.ElementParameterParser;
import org.talend.core.model.metadata.IMetadataTable;
import org.talend.core.model.metadata.IMetadataColumn;
import org.talend.core.model.process.IConnection;
import org.talend.core.model.process.EConnectionType;
import org.talend.designer.common.BigDataCodeGeneratorArgument;
import org.talend.core.model.metadata.types.JavaTypesManager;
import org.talend.core.model.process.IConnectionCategory;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.HashSet;

public class TNLPModelSparkconfigJava
{
  protected static String nl;
  public static synchronized TNLPModelSparkconfigJava create(String lineSeparator)
  {
    nl = lineSeparator;
    TNLPModelSparkconfigJava result = new TNLPModelSparkconfigJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "List<String> cols_";
  protected final String TEXT_2 = " = new java.util.ArrayList<String>();";
  protected final String TEXT_3 = NL + "    cols_";
  protected final String TEXT_4 = ".add(\"";
  protected final String TEXT_5 = "\");";
  protected final String TEXT_6 = NL + "String[] template_";
  protected final String TEXT_7 = " = \"";
  protected final String TEXT_8 = "\".split(\"\\\\|\");" + NL + "List<";
  protected final String TEXT_9 = "> temp_";
  protected final String TEXT_10 = " = rdd_";
  protected final String TEXT_11 = ".collect();" + NL + "List<List<";
  protected final String TEXT_12 = ">> temp_separated_";
  protected final String TEXT_13 = " = new java.util.ArrayList<List<";
  protected final String TEXT_14 = ">>();" + NL + "List<";
  protected final String TEXT_15 = "> t_";
  protected final String TEXT_16 = " = new java.util.ArrayList<";
  protected final String TEXT_17 = ">();" + NL + "for(";
  protected final String TEXT_18 = " row_";
  protected final String TEXT_19 = " : temp_";
  protected final String TEXT_20 = "){" + NL + "    if(row_";
  protected final String TEXT_21 = ".get(\"";
  protected final String TEXT_22 = "\") == null){" + NL + "        temp_separated_";
  protected final String TEXT_23 = ".add(t_";
  protected final String TEXT_24 = ");" + NL + "        t_";
  protected final String TEXT_25 = " = new java.util.ArrayList<";
  protected final String TEXT_26 = ">();" + NL + "    }else{" + NL + "        t_";
  protected final String TEXT_27 = ".add(row_";
  protected final String TEXT_28 = ");" + NL + "    }" + NL + "}" + NL + "//TDQ-13832 if the last data segment without CRLF,the List \"t_";
  protected final String TEXT_29 = "\" will not be empty.add it at here." + NL + "if(!t_";
  protected final String TEXT_30 = ".isEmpty()){" + NL + "    temp_separated_";
  protected final String TEXT_31 = ".add(t_";
  protected final String TEXT_32 = ");" + NL + "}" + NL + "org.apache.spark.api.java.JavaRDD<List<";
  protected final String TEXT_33 = ">> rdds_";
  protected final String TEXT_34 = " = ctx.parallelize(temp_separated_";
  protected final String TEXT_35 = ");" + NL + "org.apache.spark.api.java.JavaPairRDD<List<String>, List<String>> tokenLabelPairRDD_";
  protected final String TEXT_36 = " = rdds_";
  protected final String TEXT_37 = ".mapToPair(new ";
  protected final String TEXT_38 = "getTokenLabelFunction());" + NL + "org.apache.spark.api.java.JavaRDD<List<String>> otherFeatureRDD_";
  protected final String TEXT_39 = " = rdds_";
  protected final String TEXT_40 = ".map(new ";
  protected final String TEXT_41 = "getFeatureFunction(cols_";
  protected final String TEXT_42 = "));" + NL + "org.talend.dataquality.nlp.FeatureConstructor fc_";
  protected final String TEXT_43 = " = new org.talend.dataquality.nlp.FeatureConstructor(org.talend.dataquality.nlp.toolkit.ToolkitType.";
  protected final String TEXT_44 = ", \"";
  protected final String TEXT_45 = "\")" + NL + ".setPredecessorOccurenceThredhold(100)" + NL + ".setPositionRelativeCompensation(10)" + NL + ".setMostFrequentPercent(0.05);" + NL + "org.talend.dataquality.nlp.NLPProcessing nlp_";
  protected final String TEXT_46 = " = new org.talend.dataquality.nlp.NLPProcessing(fc_";
  protected final String TEXT_47 = ", tokenLabelPairRDD_";
  protected final String TEXT_48 = ".keys(), otherFeatureRDD_";
  protected final String TEXT_49 = ", tokenLabelPairRDD_";
  protected final String TEXT_50 = ".values(), template_";
  protected final String TEXT_51 = ");" + NL + "org.talend.dataquality.nlp.NLPProcessing.Model model_";
  protected final String TEXT_52 = " = null;" + NL + "if(";
  protected final String TEXT_53 = "!=true){" + NL + "    model_";
  protected final String TEXT_54 = " = nlp_";
  protected final String TEXT_55 = ".train();" + NL + "}else{" + NL + "    final Integer FOLD_";
  protected final String TEXT_56 = "=";
  protected final String TEXT_57 = ";" + NL + "    if(temp_separated_";
  protected final String TEXT_58 = ".size()<FOLD_";
  protected final String TEXT_59 = "){" + NL + "        String errorMsg_";
  protected final String TEXT_60 = "=\"Number of folds= \"+FOLD_";
  protected final String TEXT_61 = "+\", dataset size= \" + temp_separated_";
  protected final String TEXT_62 = ".size() +\". The number of folds in cross validation must smaller than input dataset size !\";" + NL + "        throw new IllegalArgumentException (errorMsg_";
  protected final String TEXT_63 = ");" + NL + "    }" + NL + "    model_";
  protected final String TEXT_64 = " = nlp_";
  protected final String TEXT_65 = ".evaluateCV(FOLD_";
  protected final String TEXT_66 = ");" + NL + "}" + NL + "if(model_";
  protected final String TEXT_67 = " != null){" + NL + "    com.esotericsoftware.kryo.Kryo kryo_";
  protected final String TEXT_68 = " = new com.esotericsoftware.kryo.Kryo();" + NL + "    kryo_";
  protected final String TEXT_69 = ".setInstantiatorStrategy(new org.objenesis.strategy.StdInstantiatorStrategy());" + NL + "    //if store in Folder ( not select store in file), need to add the default file name" + NL + "    String path_";
  protected final String TEXT_70 = " = ";
  protected final String TEXT_71 = ";" + NL + "\tif(!";
  protected final String TEXT_72 = "){" + NL + "    \tpath_";
  protected final String TEXT_73 = " = path_";
  protected final String TEXT_74 = " + \"/model\";" + NL + "\t}" + NL + "    com.esotericsoftware.kryo.io.Output modelOutput_";
  protected final String TEXT_75 = " = new com.esotericsoftware.kryo.io.Output(" + NL + "            fs.create(new org.apache.hadoop.fs.Path(path_";
  protected final String TEXT_76 = ")));" + NL + "    kryo_";
  protected final String TEXT_77 = ".writeObject(modelOutput_";
  protected final String TEXT_78 = ",model_";
  protected final String TEXT_79 = ");" + NL + "    modelOutput_";
  protected final String TEXT_80 = ".close();" + NL + "}";

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
     
final BigDataCodeGeneratorArgument codeGenArgument = (BigDataCodeGeneratorArgument) argument;
final INode node = (INode)codeGenArgument.getArgument();
final String cid = node.getUniqueName();

/*********** Model saving ************/
// True if save model on disk(file system), otherwise false
Boolean saveOnDisk = ElementParameterParser.getBooleanValue(node, "__SAVE_ON_DISK__");
// HDFS folder to save in
String hdfsFolder = ElementParameterParser.getValue(node,"__HDFS_FOLDER__");
boolean useConfigurationComponent = "true".equals(ElementParameterParser.getValue(node, "__DEFINE_STORAGE_CONFIGURATION__"));
boolean storeModelInFile = "true".equals(ElementParameterParser.getValue(node, "__STORE_IN_FILE__"));
if(useConfigurationComponent) {
    String uriPrefix = org.talend.designer.spark.generator.storage.SparkStorageUtils.getURIPrefix(node);
    hdfsFolder = uriPrefix + " + " + hdfsFolder;
}
List<? extends IConnection> inConns = node.getIncomingConnections(EConnectionType.FLOW_MAIN);
IMetadataTable inputMetadataTable = null;
String inConnName = "";
String inConnTypeName = "";
List<IMetadataColumn> inputColumns = null;
StringBuilder pipeline = new StringBuilder("");
if (inConns.size() == 1){
    inConnName = inConns.get(0).getName();
    inputMetadataTable = inConns.get(0).getMetadataTable();
    inConnTypeName = codeGenArgument.getRecordStructName(inConns.get(0));
    inputColumns = inputMetadataTable.getListColumns();
}
// Names used in parameter interface, names used in API
LinkedHashMap<String, String> featureRef = new LinkedHashMap<String, String>();
featureRef.put("FEATURE_1", "postag");
featureRef.put("FEATURE_2", "ner");
featureRef.put("FEATURE_3", "tokenization");
featureRef.put("FEATURE_4", "lemmatization");
featureRef.put("FEATURE_5", "stemming");
featureRef.put("FEATURE_6", "lowerwords");
featureRef.put("FEATURE_7", "tokenisnumeric");
featureRef.put("FEATURE_8", "tokenispunct");
featureRef.put("FEATURE_9", "tokeninwordnet");
featureRef.put("FEATURE_10", "tokeninstopwords");
featureRef.put("FEATURE_11", "tokeninfirstname");
featureRef.put("FEATURE_12", "tokeninlastname");
featureRef.put("FEATURE_13", "tokensuffixprefix");
featureRef.put("FEATURE_14", "tokenismostfrequent");
featureRef.put("FEATURE_15", "tokenpositionrelative");
featureRef.put("FEATURE_16", "tokeniscapitalized");
featureRef.put("FEATURE_17", "tokenisupper");
featureRef.put("FEATURE_18", "numtokeninline");
featureRef.put("FEATURE_19", "tokenmostfrequentpredecessor");
featureRef.put("FEATURE_20", "tokeninacronym");
featureRef.put("FEATURE_21", "tokeningeonames");
List<Map<String, String>> featureSchema = (ArrayList<Map<String, String>>)ElementParameterParser.getObjectValue(node, "__FEATURE_SCHEMA__");
List<Map<String, String>> otherFeatureSchema = (Boolean)ElementParameterParser.getObjectValue(node, "__IF_OTHER_FEATURE__") ? (ArrayList<Map<String, String>>)ElementParameterParser.getObjectValue(node, "__OTHER_FEATURE_SCHEMA__") : new ArrayList<Map<String, String>>();
List<Map<String, String>> mixedSchema = new ArrayList<Map<String, String>>();
mixedSchema.addAll(featureSchema);
mixedSchema.addAll(otherFeatureSchema);
List<String> pl = new ArrayList<String>();
for(String key : featureRef.keySet()){
    for(Map<String, String> f : featureSchema){
        if(f.get("FEATURE_COLUMN").equals(key) && !pl.contains(key)){
            pl.add(key);
        }
    }
}
for(String step : pl){
    pipeline.append(featureRef.get(step)+";");
}

List<String> template = new ArrayList<String>();
List<String> order = new ArrayList<String>();
for(String key : featureRef.keySet()){
    for(Map<String, String> f : mixedSchema){
        if(f.get("FEATURE_COLUMN")!=null && f.get("FEATURE_COLUMN").equals(key) && !order.contains(key)){
            order.add(key);
        }
    }
}
for(int i=0;i<inputColumns.size();i++){
    if(i!=0 && i!=inputColumns.size()-1){
        order.add(inputColumns.get(i).getLabel());
    }
}
int numFeature = 0;
for(int i=0;i<mixedSchema.size();i++){
    String pos = mixedSchema.get(i).get("RELATIVE_POSITION")!=null ? mixedSchema.get(i).get("RELATIVE_POSITION") : mixedSchema.get(i).get("OTHER_FEATURE_RELATIVE_POSITION");
    String name = mixedSchema.get(i).get("FEATURE_COLUMN")!=null ? mixedSchema.get(i).get("FEATURE_COLUMN") : mixedSchema.get(i).get("OTHER_FEATURE_COLUMN");
    if(order.contains(name)){
        int index = order.indexOf(name);
        String[] parts = pos.replace(" ","").split(",");
        for(int j=0;j<parts.length;j++){
            StringBuilder temp = new StringBuilder("");
            temp.append(String.format("U%s:%%x[%s,%s]", Integer.toString(numFeature), parts[j], Integer.toString(index)));
            template.add(temp.toString());
            numFeature++;
        }
    }
}
template.add("B");
StringBuilder schema = new StringBuilder("");
for(String t : template){
    schema.append(t+"|");
}

    stringBuffer.append(TEXT_1);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_2);
    
for(int i=1;i<inputColumns.size()-1;i++){
    String lab = inputColumns.get(i).getLabel();

    stringBuffer.append(TEXT_3);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_4);
    stringBuffer.append(lab);
    stringBuffer.append(TEXT_5);
    
}

    stringBuffer.append(TEXT_6);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_7);
    stringBuffer.append(schema.toString());
    stringBuffer.append(TEXT_8);
    stringBuffer.append(inConnTypeName);
    stringBuffer.append(TEXT_9);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_10);
    stringBuffer.append(inConnName);
    stringBuffer.append(TEXT_11);
    stringBuffer.append(inConnTypeName);
    stringBuffer.append(TEXT_12);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_13);
    stringBuffer.append(inConnTypeName);
    stringBuffer.append(TEXT_14);
    stringBuffer.append(inConnTypeName);
    stringBuffer.append(TEXT_15);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_16);
    stringBuffer.append(inConnTypeName);
    stringBuffer.append(TEXT_17);
    stringBuffer.append(inConnTypeName);
    stringBuffer.append(TEXT_18);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_19);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_20);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_21);
    stringBuffer.append(inputColumns.get(0).getLabel());
    stringBuffer.append(TEXT_22);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_23);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_24);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_25);
    stringBuffer.append(inConnTypeName);
    stringBuffer.append(TEXT_26);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_27);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_28);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_29);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_30);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_31);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_32);
    stringBuffer.append(inConnTypeName);
    stringBuffer.append(TEXT_33);
    stringBuffer.append(inConnName);
    stringBuffer.append(TEXT_34);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_35);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_36);
    stringBuffer.append(inConnName);
    stringBuffer.append(TEXT_37);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_38);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_39);
    stringBuffer.append(inConnName);
    stringBuffer.append(TEXT_40);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_41);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_42);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_43);
    stringBuffer.append((String)ElementParameterParser.getObjectValue(node, "LIBTYPE"));
    stringBuffer.append(TEXT_44);
    stringBuffer.append(pipeline);
    stringBuffer.append(TEXT_45);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_46);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_47);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_48);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_49);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_50);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_51);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_52);
    stringBuffer.append(ElementParameterParser.getObjectValue(node, "__CROSS_VALIDATION__"));
    stringBuffer.append(TEXT_53);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_54);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_55);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_56);
    stringBuffer.append(ElementParameterParser.getObjectValue(node, "__FOLD__"));
    stringBuffer.append(TEXT_57);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_58);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_59);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_60);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_61);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_62);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_63);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_64);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_65);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_66);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_67);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_68);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_69);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_70);
    stringBuffer.append(hdfsFolder);
    stringBuffer.append(TEXT_71);
    stringBuffer.append(storeModelInFile);
    stringBuffer.append(TEXT_72);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_73);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_74);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_75);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_76);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_77);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_78);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_79);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_80);
    return stringBuffer.toString();
  }
}
