package org.talend.designer.codegen.translators.messaging.pubsub;

import java.util.Map.Entry;
import org.talend.core.model.process.ElementParameterParser;
import org.talend.core.model.process.IConnection;
import org.talend.core.model.process.INode;
import org.talend.designer.common.BigDataCodeGeneratorArgument;
import org.talend.designer.common.tgooglecloudconfiguration.TGoogleCloudConfigurationUtil;

public class TPubSubAbstractInputAvroSparkstreamingconfigJava
{
  protected static String nl;
  public static synchronized TPubSubAbstractInputAvroSparkstreamingconfigJava create(String lineSeparator)
  {
    nl = lineSeparator;
    TPubSubAbstractInputAvroSparkstreamingconfigJava result = new TPubSubAbstractInputAvroSparkstreamingconfigJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "";
  protected final String TEXT_2 = NL + "    String avroSchema_";
  protected final String TEXT_3 = " = org.apache.commons.io.FileUtils.readFileToString(new java.io.File(";
  protected final String TEXT_4 = "));";
  protected final String TEXT_5 = NL + NL + "org.apache.spark.streaming.pubsub.SparkGCPCredentials.Builder builder =" + NL + "              new org.apache.spark.streaming.pubsub.SparkGCPCredentials.Builder();" + NL;
  protected final String TEXT_6 = NL + "    builder = builder.p12ServiceAccount(";
  protected final String TEXT_7 = ",";
  protected final String TEXT_8 = NL + "            ";
  protected final String TEXT_9 = ");";
  protected final String TEXT_10 = NL + "    builder = builder.jsonServiceAccount(";
  protected final String TEXT_11 = ");";
  protected final String TEXT_12 = NL + NL + "org.apache.spark.streaming.api.java.JavaReceiverInputDStream<org.apache.spark.streaming.pubsub.SparkPubsubMessage> dStream_";
  protected final String TEXT_13 = " = null;" + NL;
  protected final String TEXT_14 = "\t" + NL + "             dStream_";
  protected final String TEXT_15 = " =  org.apache.spark.streaming.pubsub.PubsubUtils.createStream(" + NL + "                ctx, ";
  protected final String TEXT_16 = ", ";
  protected final String TEXT_17 = ", ";
  protected final String TEXT_18 = "," + NL + "                builder.build()," + NL + "                org.apache.spark.storage.StorageLevel.";
  protected final String TEXT_19 = "());" + NL + "\t";
  protected final String TEXT_20 = NL + "\t\t// to be replaced by constructor that uses topic" + NL + "\t   dStream_";
  protected final String TEXT_21 = " =  org.apache.spark.streaming.pubsub.PubsubUtils.createStream(" + NL + "                ctx, ";
  protected final String TEXT_22 = ", ";
  protected final String TEXT_23 = "," + NL + "                builder.build()," + NL + "                org.apache.spark.storage.StorageLevel.";
  protected final String TEXT_24 = "());" + NL + "\t";
  protected final String TEXT_25 = NL + NL + "org.apache.spark.streaming.api.java.JavaPairDStream<NullWritable, ";
  protected final String TEXT_26 = "> rdd_";
  protected final String TEXT_27 = " = null;" + NL;
  protected final String TEXT_28 = NL + "\trdd_";
  protected final String TEXT_29 = " = dStream_";
  protected final String TEXT_30 = ".mapToPair(new ";
  protected final String TEXT_31 = "_MapToOutputStruct());";
  protected final String TEXT_32 = NL + "\trdd_";
  protected final String TEXT_33 = " = dStream_";
  protected final String TEXT_34 = ".mapToPair(new ";
  protected final String TEXT_35 = "_MapToOutputStruct(avroSchema_";
  protected final String TEXT_36 = "));";
  protected final String TEXT_37 = NL + " ";

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    stringBuffer.append(TEXT_1);
    
BigDataCodeGeneratorArgument codeGenArgument = (BigDataCodeGeneratorArgument) argument;
INode node = (INode)codeGenArgument.getArgument();
final TGoogleCloudConfigurationUtil gcpConfigUtil = new TGoogleCloudConfigurationUtil(node);
if (gcpConfigUtil.getValidateError() != null) {
    // Cause the job compilation to explicitly fail if there is a problem.
    return "throw new JobConfigurationError(\"" + gcpConfigUtil.getValidateError() +"\");";
}

String cid = node.getUniqueName();
IConnection conn = node.getOutgoingConnections().get(0);
String connectionTypeName = codeGenArgument.getRecordStructName(conn);
String connName = conn.getName();

String topicName = ElementParameterParser.getValue(node, "__PUBSUB_TOPIC__");
String subscriptionName = ElementParameterParser.getValue(node, "__PUBSUB_SUBSCRIPTION_NAME__");

String storageLevel = ElementParameterParser.getValue(node, "__STORAGELEVEL__");

boolean createSubscriptionFromTopic = true;
if (topicName.replaceAll("\"", "").length() == 0) {
		createSubscriptionFromTopic = false;
}
if (subscriptionName.replaceAll("\"", "").length() == 0) {
	  	subscriptionName = "\"subscription_" + java.util.UUID.randomUUID()+"\"";
}

boolean useHierarchical = "true".equals(ElementParameterParser.getValue(node, "__USE_HIERARCHICAL__"));

String mapToOutputStructArgument = "";
if (useHierarchical) {
    String schemaFileName = ElementParameterParser.getValue(node, "__SCHEMA_FILENAME__");
    mapToOutputStructArgument = "avroSchema_" + cid;
    
    stringBuffer.append(TEXT_2);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_3);
    stringBuffer.append(schemaFileName);
    stringBuffer.append(TEXT_4);
    
}

    stringBuffer.append(TEXT_5);
    
if (gcpConfigUtil.isSpecifyingCredentialsP12()) {
    
    stringBuffer.append(TEXT_6);
    stringBuffer.append(gcpConfigUtil.getGcpAuthenticationPath());
    stringBuffer.append(TEXT_7);
    stringBuffer.append(TEXT_8);
    stringBuffer.append(gcpConfigUtil.getGcpServiceAccountId());
    stringBuffer.append(TEXT_9);
    
} else if (gcpConfigUtil.isSpecifyingCredentialsJson()) {
    
    stringBuffer.append(TEXT_10);
    stringBuffer.append(gcpConfigUtil.getGcpAuthenticationPath());
    stringBuffer.append(TEXT_11);
    
}

    stringBuffer.append(TEXT_12);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_13);
    
	if (createSubscriptionFromTopic) {
	
    stringBuffer.append(TEXT_14);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_15);
    stringBuffer.append(gcpConfigUtil.getProjectName());
    stringBuffer.append(TEXT_16);
    stringBuffer.append(topicName);
    stringBuffer.append(TEXT_17);
    stringBuffer.append(subscriptionName);
    stringBuffer.append(TEXT_18);
    stringBuffer.append(storageLevel);
    stringBuffer.append(TEXT_19);
    
 } else {
	
    stringBuffer.append(TEXT_20);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_21);
    stringBuffer.append(gcpConfigUtil.getProjectName());
    stringBuffer.append(TEXT_22);
    stringBuffer.append(subscriptionName);
    stringBuffer.append(TEXT_23);
    stringBuffer.append(storageLevel);
    stringBuffer.append(TEXT_24);
     
}

    stringBuffer.append(TEXT_25);
    stringBuffer.append(connectionTypeName);
    stringBuffer.append(TEXT_26);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_27);
     if (!useHierarchical) {

    stringBuffer.append(TEXT_28);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_29);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_30);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_31);
    
  } else {

    stringBuffer.append(TEXT_32);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_33);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_34);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_35);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_36);
     
}

    stringBuffer.append(TEXT_37);
    return stringBuffer.toString();
  }
}
