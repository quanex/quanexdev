package org.talend.designer.codegen.translators.dataquality.matching;

import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import org.talend.core.model.metadata.IMetadataColumn;
import org.talend.core.model.metadata.IMetadataTable;
import org.talend.core.model.metadata.types.JavaType;
import org.talend.core.model.metadata.types.JavaTypesManager;
import org.talend.core.model.process.ElementParameterParser;
import org.talend.core.model.process.IBigDataNode;
import org.talend.core.model.process.IConnection;
import org.talend.core.model.process.IConnectionCategory;
import org.talend.core.model.process.INode;
import org.talend.core.model.utils.NodeUtil;
import org.talend.designer.common.BigDataCodeGeneratorArgument;
import org.talend.designer.common.tmodelencoder.TModelEncoderUtil;
import org.talend.designer.common.tsqlrow.TSqlRowUtil;

public class TMatchPairingSparkconfigJava
{
  protected static String nl;
  public static synchronized TMatchPairingSparkconfigJava create(String lineSeparator)
  {
    nl = lineSeparator;
    TMatchPairingSparkconfigJava result = new TMatchPairingSparkconfigJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "";
  protected final String TEXT_2 = NL + "                if(log.is";
  protected final String TEXT_3 = "Enabled())";
  protected final String TEXT_4 = NL + "            log.";
  protected final String TEXT_5 = "(\"";
  protected final String TEXT_6 = " - \" ";
  protected final String TEXT_7 = " + (";
  protected final String TEXT_8 = ") ";
  protected final String TEXT_9 = ");";
  protected final String TEXT_10 = NL + "            if (log.isDebugEnabled()) {" + NL + "                class BytesLimit65535_";
  protected final String TEXT_11 = "{" + NL + "                    public void limitLog4jByte() throws Exception{" + NL + "                    StringBuilder ";
  protected final String TEXT_12 = " = new StringBuilder();";
  protected final String TEXT_13 = NL + "                    ";
  protected final String TEXT_14 = ".append(\"Parameters:\");";
  protected final String TEXT_15 = NL + "                            ";
  protected final String TEXT_16 = ".append(\"";
  protected final String TEXT_17 = "\" + \" = \" + String.valueOf(";
  protected final String TEXT_18 = ").substring(0, 4) + \"...\");     ";
  protected final String TEXT_19 = NL + "                            ";
  protected final String TEXT_20 = ".append(\"";
  protected final String TEXT_21 = "\" + \" = \" + ";
  protected final String TEXT_22 = ");";
  protected final String TEXT_23 = NL + "                        ";
  protected final String TEXT_24 = ".append(\" | \");";
  protected final String TEXT_25 = NL + "                    } " + NL + "                } " + NL + "            new BytesLimit65535_";
  protected final String TEXT_26 = "().limitLog4jByte();" + NL + "            }";
  protected final String TEXT_27 = NL + "            StringBuilder ";
  protected final String TEXT_28 = " = new StringBuilder();    ";
  protected final String TEXT_29 = NL + "                    ";
  protected final String TEXT_30 = ".append(";
  protected final String TEXT_31 = ".";
  protected final String TEXT_32 = ");";
  protected final String TEXT_33 = NL + "                    if(";
  protected final String TEXT_34 = ".";
  protected final String TEXT_35 = " == null){";
  protected final String TEXT_36 = NL + "                        ";
  protected final String TEXT_37 = ".append(\"<null>\");" + NL + "                    }else{";
  protected final String TEXT_38 = NL + "                        ";
  protected final String TEXT_39 = ".append(";
  protected final String TEXT_40 = ".";
  protected final String TEXT_41 = ");" + NL + "                    }   ";
  protected final String TEXT_42 = NL + "                ";
  protected final String TEXT_43 = ".append(\"|\");";
  protected final String TEXT_44 = NL + "            // SparkSQL will use the default TimeZone to handle dates (see org.apache.spark.sql.catalyst.util.DateTimeUtils)." + NL + "            // To avoid inconsistencies with others components in the Studio, ensure that UTC is set as the default TimeZone." + NL + "            java.util.TimeZone.setDefault(java.util.TimeZone.getTimeZone(\"UTC\"));" + NL + "            " + NL + "            // java.util.Date -> java.sql.Timestamp conversions to be compliant with Spark SQL before creating our DataFrame" + NL + "            org.apache.spark.api.java.JavaRDD<";
  protected final String TEXT_45 = "> ";
  protected final String TEXT_46 = " = rdd_";
  protected final String TEXT_47 = ".map(new ";
  protected final String TEXT_48 = "_From";
  protected final String TEXT_49 = "To";
  protected final String TEXT_50 = "());";
  protected final String TEXT_51 = NL + "\t// Convert incoming RDD to DataFrame" + NL + "\torg.apache.spark.sql.SQLContext sqlContext_";
  protected final String TEXT_52 = " = new org.apache.spark.sql.SQLContext(ctx);" + NL + "\t";
  protected final String TEXT_53 = " ";
  protected final String TEXT_54 = " =" + NL + "\t        sqlContext_";
  protected final String TEXT_55 = ".createDataFrame(";
  protected final String TEXT_56 = ", ";
  protected final String TEXT_57 = ".class);" + NL + "" + NL + "\t// Create holder for all transformations to perform" + NL + "\tjava.util.List<org.apache.spark.ml.PipelineStage> ";
  protected final String TEXT_58 = " = new java.util.ArrayList<org.apache.spark.ml.PipelineStage>();";
  protected final String TEXT_59 = NL + NL + NL + "\t// List of columnNames for constructing bkv" + NL + "\tjava.util.List<String> ";
  protected final String TEXT_60 = " = new java.util.ArrayList<String>();" + NL + "\t";
  protected final String TEXT_61 = NL + "\t          ";
  protected final String TEXT_62 = ".add(\"";
  protected final String TEXT_63 = "\");" + NL + "\t";
  protected final String TEXT_64 = NL + NL + "    " + NL + "    org.talend.dataquality.reconciliation.api.MatchPairing.Config config_";
  protected final String TEXT_65 = " =" + NL + "        new org.talend.dataquality.reconciliation.api.MatchPairing.Config(";
  protected final String TEXT_66 = NL + "            ";
  protected final String TEXT_67 = ".toArray(new String[";
  protected final String TEXT_68 = ".size()]),";
  protected final String TEXT_69 = NL + "            ";
  protected final String TEXT_70 = ",";
  protected final String TEXT_71 = NL + "            ";
  protected final String TEXT_72 = ",";
  protected final String TEXT_73 = NL + "            ";
  protected final String TEXT_74 = " || ";
  protected final String TEXT_75 = ",";
  protected final String TEXT_76 = NL + "            ";
  protected final String TEXT_77 = ",";
  protected final String TEXT_78 = NL + "            ";
  protected final String TEXT_79 = ",";
  protected final String TEXT_80 = NL + "            ";
  protected final String TEXT_81 = ",";
  protected final String TEXT_82 = NL + "            ";
  protected final String TEXT_83 = ",";
  protected final String TEXT_84 = NL + "            ";
  protected final String TEXT_85 = ");" + NL + "        " + NL + "    scala.Tuple4< org.apache.spark.ml.PipelineModel,  org.apache.spark.ml.PipelineModel, org.talend.dataquality.reconciliation.util.MatchResultSplitter.MatchResults,scala.Option<";
  protected final String TEXT_86 = ">> res_";
  protected final String TEXT_87 = " = " + NL + "            org.talend.dataquality.reconciliation.api.MatchPairing.run(";
  protected final String TEXT_88 = NL + "                    ";
  protected final String TEXT_89 = ",";
  protected final String TEXT_90 = NL + "                    ";
  protected final String TEXT_91 = ".toArray(new org.apache.spark.ml.PipelineStage[";
  protected final String TEXT_92 = ".size()])," + NL + "                    config_";
  protected final String TEXT_93 = ");" + NL + "    " + NL + "    org.talend.dataquality.reconciliation.util.MatchResultSplitter.MatchResults matchPairs_";
  protected final String TEXT_94 = " = res_";
  protected final String TEXT_95 = "._3();" + NL + "" + NL + "\t";
  protected final String TEXT_96 = NL + "\t// Convert suspect outgoing DataFrame to RDD" + NL + "    org.apache.spark.api.java.JavaRDD<";
  protected final String TEXT_97 = "> rdd_";
  protected final String TEXT_98 = " =" + NL + "            matchPairs_";
  protected final String TEXT_99 = ".suspectOut().toJavaRDD().map(new ";
  protected final String TEXT_100 = "_FromRowTo";
  protected final String TEXT_101 = "());" + NL + "\t";
  protected final String TEXT_102 = NL;
  protected final String TEXT_103 = NL + "    // Convert exact outgoing DataFrame to RDD" + NL + "    org.apache.spark.api.java.JavaRDD<";
  protected final String TEXT_104 = "> rdd_";
  protected final String TEXT_105 = " =" + NL + "            matchPairs_";
  protected final String TEXT_106 = ".exactOut().get().toJavaRDD().map(new ";
  protected final String TEXT_107 = "_FromRowTo";
  protected final String TEXT_108 = "());";
  protected final String TEXT_109 = NL + NL + "\t";
  protected final String TEXT_110 = NL + "\t// Convert unique outgoing DataFrame to RDD" + NL + "\torg.apache.spark.api.java.JavaRDD<";
  protected final String TEXT_111 = "> rdd_";
  protected final String TEXT_112 = " =" + NL + "            matchPairs_";
  protected final String TEXT_113 = ".uniqueOut().get().toJavaRDD().map(new ";
  protected final String TEXT_114 = "_FromRowTo";
  protected final String TEXT_115 = "());" + NL + "\t";
  protected final String TEXT_116 = NL + "\t" + NL + "\t";
  protected final String TEXT_117 = NL + "\t// Convert SuspectSampling outgoing DataFrame to RDD" + NL + "\torg.apache.spark.api.java.JavaRDD<";
  protected final String TEXT_118 = "> rdd_";
  protected final String TEXT_119 = " =" + NL + "            res_";
  protected final String TEXT_120 = "._4().get().toJavaRDD().map(new ";
  protected final String TEXT_121 = "_FromRowTo";
  protected final String TEXT_122 = "());" + NL + "" + NL + "\t";
  protected final String TEXT_123 = NL + NL + "\t";
  protected final String TEXT_124 = NL + "        String tdsUrl = !";
  protected final String TEXT_125 = ".endsWith(\"/\") ? ";
  protected final String TEXT_126 = " + \"/\" : ";
  protected final String TEXT_127 = ";" + NL + "        String loginUrl = tdsUrl+\"login?username=\"+java.net.URLEncoder.encode(";
  protected final String TEXT_128 = ", ";
  protected final String TEXT_129 = ".CHARSET_UTF8)" + NL + "                        +\"&password=\"+java.net.URLEncoder.encode(";
  protected final String TEXT_130 = ", ";
  protected final String TEXT_131 = ".CHARSET_UTF8)+\"&client-app=STUDIO\";";
  protected final String TEXT_132 = NL + "        ";
  protected final String TEXT_133 = " loginRequest = ";
  protected final String TEXT_134 = ".post(loginUrl);" + NL + "        int statusCode = loginRequest.code();" + NL + "        if(statusCode==403){";
  protected final String TEXT_135 = NL + "        } else if (statusCode==404){";
  protected final String TEXT_136 = NL + "        } else if(statusCode>=300){";
  protected final String TEXT_137 = NL + "        }" + NL + "    " + NL + "        com.talend.fasterxml.jackson.databind.ObjectMapper MAPPER = new com.talend.fasterxml.jackson.databind.ObjectMapper();" + NL + "    " + NL + "        //Get suspect pair sampling data frame";
  protected final String TEXT_138 = NL + "        ";
  protected final String TEXT_139 = " dataFrameForTDS_";
  protected final String TEXT_140 = " = res_";
  protected final String TEXT_141 = "._4().get();" + NL + "        org.apache.spark.sql.Row[] suspectSamplingRecords_";
  protected final String TEXT_142 = " = ";
  protected final String TEXT_143 = "  " + NL + "                    dataFrameForTDS_";
  protected final String TEXT_144 = ".collect();";
  protected final String TEXT_145 = " " + NL + "                    (org.apache.spark.sql.Row[]) dataFrameForTDS_";
  protected final String TEXT_146 = ".collect();";
  protected final String TEXT_147 = NL + "                " + NL + "        java.util.List<Map<String, Object>> taskObjs = new java.util.ArrayList<>();" + NL + "        java.util.List<String> tasksToCreate = new java.util.ArrayList<>();" + NL + "        String pairId = \"\";" + NL + "        org.apache.spark.sql.types.StructType structType_";
  protected final String TEXT_148 = " = dataFrameForTDS_";
  protected final String TEXT_149 = ".schema();" + NL + "" + NL + "        for (org.apache.spark.sql.Row record : suspectSamplingRecords_";
  protected final String TEXT_150 = "){" + NL + "    " + NL + "            java.util.Map<String, Object> taskObj = new java.util.HashMap<String, Object>();" + NL + "            taskObj.put(\"type\", \"GROUPING\");" + NL + "            if(!";
  protected final String TEXT_151 = ".isEmpty()){" + NL + "            \ttaskObj.put(\"assignee\", ";
  protected final String TEXT_152 = ");" + NL + "            }" + NL + "            java.util.List<Map<String, Object>> sourceRecords = new java.util.ArrayList<>();" + NL + "" + NL + "            Double score = (Double) record.get(structType_";
  protected final String TEXT_153 = ".fieldIndex(\"SCORE\"));" + NL + "" + NL + "            if (pairId.equals(record.get(structType_";
  protected final String TEXT_154 = ".fieldIndex(\"PAIR_ID\")))) {" + NL + "                // get the last taskObj" + NL + "                taskObj = (java.util.Map<String, Object>) taskObjs.get(taskObjs.size() - 1);" + NL + "                taskObjs.remove(taskObj);" + NL + "                sourceRecords = (java.util.List<java.util.Map<String, Object>>) taskObj.get(\"sourceRecords\");" + NL + "                if (score != null && (taskObj.get(\"matchScore\") == null || !score.equals(taskObj.get(\"matchScore\")))) {" + NL + "                    taskObj.put(\"matchScore\", score);" + NL + "                }" + NL + "            } else {" + NL + "                if(taskObjs.size()==";
  protected final String TEXT_155 = "){" + NL + "                    tasksToCreate.add(MAPPER.writeValueAsString(taskObjs));" + NL + "                    taskObjs.clear();" + NL + "                }" + NL + "                // a new task" + NL + "                pairId = (String) record.get(structType_";
  protected final String TEXT_156 = ".fieldIndex(\"PAIR_ID\"));" + NL + "" + NL + "                if (score != null) {" + NL + "                    taskObj.put(\"matchScore\", score);" + NL + "                }" + NL + "            }" + NL + "" + NL + "            // add source record" + NL + "            java.util.Map<String, Object> sourceRecordObj = new java.util.HashMap<String, Object>();" + NL + "" + NL + "            java.util.Map<String, Object> recordObj = new java.util.HashMap<String, Object>();" + NL + "" + NL + "            for (String fieldName : structType_";
  protected final String TEXT_157 = ".fieldNames()) {" + NL + "                if(!(fieldName.equals(\"PAIR_ID\") || fieldName.equals(\"SCORE\") || fieldName.equals(\"LABEL\")) ){" + NL + "                \tObject value = record.get(structType_";
  protected final String TEXT_158 = ".fieldIndex(fieldName));" + NL + "                \tif(value instanceof java.util.Date){" + NL + "                \t\t//TDS server need number of days from epoch time" + NL + "                \t\trecordObj.put(fieldName, ((Date) value).getTime() / (1000 * 3600 * 24));" + NL + "                \t}else{" + NL + "                \t\trecordObj.put(fieldName, value);" + NL + "                \t}" + NL + "                }" + NL + "            }" + NL + "            sourceRecordObj.put(\"record\", recordObj);" + NL + "            sourceRecords.add(sourceRecordObj);" + NL + "            taskObj.put(\"sourceRecords\", sourceRecords);" + NL + "            taskObjs.add(taskObj);" + NL + "        }" + NL + "" + NL + "        if(taskObjs.size()>0){" + NL + "            tasksToCreate.add(MAPPER.writeValueAsString(taskObjs));" + NL + "        }" + NL + "" + NL + "        String authorization = loginRequest.header(\"AUTHORIZATION\");" + NL + "        String resourceToCreateTask = tdsUrl + \"api/v1/campaigns/owned/\" + ";
  protected final String TEXT_159 = " + \"/tasks\";" + NL + "        " + NL + "        for(String body : tasksToCreate){";
  protected final String TEXT_160 = NL + "            ";
  protected final String TEXT_161 = " request = ";
  protected final String TEXT_162 = ".post(resourceToCreateTask).authorization(authorization)" + NL + "                    .header(";
  protected final String TEXT_163 = ".HEADER_CONTENT_TYPE, ";
  protected final String TEXT_164 = ".CONTENT_TYPE_JSON)" + NL + "                    .send(body);" + NL + "\t\t    " + NL + "            statusCode = request.code();" + NL + "            if(statusCode >= 300){";
  protected final String TEXT_165 = "                " + NL + "            }else{" + NL + "                String newAuthorization = request.header(\"AUTHORIZATION\");" + NL + "                if (newAuthorization != null) {" + NL + "                    authorization = newAuthorization;" + NL + "                }" + NL + "                java.lang.StringBuilder sb = new java.lang.StringBuilder();" + NL + "                if (request.noContent()) {";
  protected final String TEXT_166 = "                " + NL + "                } else {" + NL + "                    java.io.BufferedReader reader = request.bufferedReader();\t\t\t\t\t" + NL + "                    String line;" + NL + "                    while ((line = reader.readLine()) != null) {" + NL + "                        sb.append(line);" + NL + "                    }" + NL + "                }" + NL + "                List<Map<String, Object>> tasks = MAPPER.readValue(sb.toString(), List.class);" + NL + "                Map<String, Object> rawJson = null;" + NL + "                int rejectCount = 0;" + NL + "                for (Object obj : tasks) {" + NL + "                    rawJson = (Map<String, Object>) obj;" + NL + "                    if(java.util.Objects.equals(\"TASK_NOT_ASSIGNABLE_TO_USER\", rawJson.get(\"error\"))) {";
  protected final String TEXT_167 = NL + "                        break;" + NL + "                    }" + NL + "                    if (rawJson.get(\"result\") == null) {" + NL + "                        rejectCount++;" + NL + "                    }" + NL + "                }" + NL + "                if(rejectCount>0){";
  protected final String TEXT_168 = NL + "                }" + NL + "            }" + NL + "        }" + NL + "        //logout";
  protected final String TEXT_169 = NL + "        ";
  protected final String TEXT_170 = ".post(tdsUrl + \"logout?client-app=STUDIO\").header(authorization);\t\t";
  protected final String TEXT_171 = NL + "            java.net.URI currentURI_";
  protected final String TEXT_172 = "_config = FileSystem.getDefaultUri(ctx.hadoopConfiguration());" + NL + "            FileSystem.setDefaultUri(ctx.hadoopConfiguration(), new java.net.URI(";
  protected final String TEXT_173 = "));" + NL + "            fs = FileSystem.get(ctx.hadoopConfiguration());";
  protected final String TEXT_174 = NL + "\tPath pathToDelete_";
  protected final String TEXT_175 = " = new Path(";
  protected final String TEXT_176 = ");" + NL + "\tif (fs.exists(pathToDelete_";
  protected final String TEXT_177 = ")) {" + NL + "\t      fs.delete(pathToDelete_";
  protected final String TEXT_178 = ", true);" + NL + "\t}" + NL + "" + NL + "\t// Serialize the model" + NL + "\tcom.esotericsoftware.kryo.Kryo kryo_";
  protected final String TEXT_179 = " = new com.esotericsoftware.kryo.Kryo();" + NL + "\t" + NL + "\tcom.esotericsoftware.kryo.io.Output blockingModelOutput_";
  protected final String TEXT_180 = " = new com.esotericsoftware.kryo.io.Output(" + NL + "         fs.create(new org.apache.hadoop.fs.Path( ";
  protected final String TEXT_181 = " + \"/blocking\")));" + NL + "\tkryo_";
  protected final String TEXT_182 = ".writeObject(blockingModelOutput_";
  protected final String TEXT_183 = "," + NL + "\t        new TalendPipelineModel(res_";
  protected final String TEXT_184 = "._1())," + NL + "\t        new TalendPipelineModelSerializer());" + NL + "\tblockingModelOutput_";
  protected final String TEXT_185 = ".close();" + NL + "\t" + NL + "\tcom.esotericsoftware.kryo.io.Output filteringModelOutput_";
  protected final String TEXT_186 = " = new com.esotericsoftware.kryo.io.Output(" + NL + "\t      fs.create(new org.apache.hadoop.fs.Path( ";
  protected final String TEXT_187 = " + \"/filtering\")));" + NL + "\tkryo_";
  protected final String TEXT_188 = ".writeObject(filteringModelOutput_";
  protected final String TEXT_189 = "," + NL + "\t        new TalendPipelineModel(res_";
  protected final String TEXT_190 = "._2())," + NL + "\t        new TalendPipelineModelSerializer());" + NL + "\tfilteringModelOutput_";
  protected final String TEXT_191 = ".close();";

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    
//Parse the inputs to this javajet generator.
final BigDataCodeGeneratorArgument codeGenArgument = (BigDataCodeGeneratorArgument) argument;
final INode node = (INode)codeGenArgument.getArgument();
final String cid = node.getUniqueName();
final boolean isLog4jEnabled = ("true").equals(ElementParameterParser.getValue(node.getProcess(), "__LOG4J_ACTIVATE__"));

final boolean isSpark1 = org.talend.hadoop.distribution.ESparkVersion.SPARK_2_0.compareTo(codeGenArgument.getSparkVersion()) > 0;

final String dataframeClass = isSpark1
    ? "org.apache.spark.sql.DataFrame"
    : "org.apache.spark.sql.Dataset<org.apache.spark.sql.Row>";

TSqlRowUtil tSqlRowUtil = new TSqlRowUtil(node);
String validateError = tSqlRowUtil.validate(true, true);
if (validateError != null) {
    // Cause the job compilation to explicitly fail if there is a problem.
    return "throw new JobConfigurationError(\"" + validateError +"\");";
}

    
//IO Connections
IConnection incomingConnection = null;
if (node.getIncomingConnections() != null) {
 for (IConnection in : node.getIncomingConnections()) {
     if (in.getLineStyle().hasConnectionCategory(IConnectionCategory.DATA)) {
         incomingConnection = in;
         break;
     }
 }
}
String incomingConnectionName = incomingConnection.getName();
String incomingStructName = codeGenArgument.getRecordStructName(incomingConnection);

IConnection suspectConnection = null;
boolean hasSuspectConnection = false;
List<? extends IConnection> outgoingSuspectConnections = node.getOutgoingConnections("SUSPECT");
if (outgoingSuspectConnections.size() > 0) {
    suspectConnection = outgoingSuspectConnections.get(0);
}
if(suspectConnection != null){
    hasSuspectConnection = true;
}

IConnection exactConnection = null;
boolean hasExactConnection = false;
List<? extends IConnection> outgoingExactConnections = node.getOutgoingConnections("EXACT");
if (outgoingExactConnections.size() > 0) {
    exactConnection = outgoingExactConnections.get(0);
}
if(exactConnection != null){
    hasExactConnection = true;
}

IConnection uniqueConnection = null;
boolean hasUniqueConnection = false;
List<? extends IConnection> outgoingUniqueConnections = node.getOutgoingConnections("UNIQUE");
if (outgoingUniqueConnections.size() > 0) {
    uniqueConnection = outgoingUniqueConnections.get(0);
}
if(uniqueConnection != null){
    hasUniqueConnection = true;
}

IConnection suspectSamplingConnection = null;
boolean hasSuspectSamplingConnection = false;
List<? extends IConnection> outgoingSuspectSamplingConnections = node.getOutgoingConnections("SUSPECT_SAMPLING");
if (outgoingSuspectSamplingConnections.size() > 0) {
    suspectSamplingConnection = outgoingSuspectSamplingConnections.get(0);
}
if(suspectSamplingConnection != null){
    hasSuspectSamplingConnection = true;
}


    stringBuffer.append(TEXT_1);
    
class BasicLogUtil{
    protected String cid  = "";
    protected org.talend.core.model.process.INode node = null;
    protected boolean log4jEnabled = false;
    private String logID = "";
    
    private BasicLogUtil(){}
    
    public BasicLogUtil(org.talend.core.model.process.INode node){
        this.node = node;
        String cidx = this.node.getUniqueName();
        if(cidx.matches("^.*?tAmazonAuroraOutput_\\d+_out$") || cidx.matches("^.*?tDBOutput_\\d+_out$")){
             cidx = cidx.substring(0,cidx.length()-4);// 4 ==> "_out".length();
        }
        this.cid = cidx;
        this.log4jEnabled = ("true").equals(org.talend.core.model.process.ElementParameterParser.getValue(this.node.getProcess(), "__LOG4J_ACTIVATE__"));
        this.log4jEnabled = this.log4jEnabled &&
                            this.node.getComponent().isLog4JEnabled() && !"JOBLET".equals(node.getComponent().getComponentType().toString());
        this.logID = this.cid;
    }
    
    public String var(String varName){
        return varName + "_" + this.cid;
    }
    public String str(String content){
        return "\"" + content + "\"";
    }
    
    public void info(String... message){
        log4j("info", message);
    }
    
    public void debug(String... message){
        log4j("debug", message);
    }
    
    public void warn(String... message){
        log4j("warn", message);
    }
    
    public void error(String... message){
        log4j("error", message);
    }
    
    public void fatal(String... message){
        log4j("fatal", message);
    }
    
    public void trace(String... message){
        log4j("trace", message);
    }
    java.util.List<String> checkableList = java.util.Arrays.asList(new String[]{"info", "debug", "trace"});     
    public void log4j(String level, String... messages){
        if(this.log4jEnabled){
            if(checkableList.contains(level)){
            
    stringBuffer.append(TEXT_2);
    stringBuffer.append(level.substring(0, 1).toUpperCase() + level.substring(1));
    stringBuffer.append(TEXT_3);
    
            }
            
    stringBuffer.append(TEXT_4);
    stringBuffer.append(level);
    stringBuffer.append(TEXT_5);
    stringBuffer.append(logID);
    stringBuffer.append(TEXT_6);
    for(String message : messages){
    stringBuffer.append(TEXT_7);
    stringBuffer.append(message);
    stringBuffer.append(TEXT_8);
    }
    stringBuffer.append(TEXT_9);
    
        }
    }
    
    public boolean isActive(){
        return this.log4jEnabled;
    }
}

class LogUtil extends BasicLogUtil{
    
    private LogUtil(){
    }
    
    public LogUtil(org.talend.core.model.process.INode node){
        super(node);
    }
    
    public void startWork(){
        debug(str("Start to work."));
    }
    
    public void endWork(){
        debug(str("Done."));
    }
    
    public void logIgnoredException(String exception){
        warn(exception);
    }
    
    public void logPrintedException(String exception){
        error(exception);
    }
    
    public void logException(String exception){
        fatal(exception);
    }
    
    public void logCompSetting(){
    
    
        if(log4jEnabled){
        
    stringBuffer.append(TEXT_10);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_11);
    stringBuffer.append(var("log4jParamters"));
    stringBuffer.append(TEXT_12);
    stringBuffer.append(TEXT_13);
    stringBuffer.append(var("log4jParamters"));
    stringBuffer.append(TEXT_14);
    
                    java.util.Set<org.talend.core.model.process.EParameterFieldType> ignoredParamsTypes = new java.util.HashSet<org.talend.core.model.process.EParameterFieldType>(); 
                    ignoredParamsTypes.addAll(
                        java.util.Arrays.asList(
                            org.talend.core.model.process.EParameterFieldType.SCHEMA_TYPE,
                            org.talend.core.model.process.EParameterFieldType.LABEL,
                            org.talend.core.model.process.EParameterFieldType.EXTERNAL,
                            org.talend.core.model.process.EParameterFieldType.MAPPING_TYPE,
                            org.talend.core.model.process.EParameterFieldType.IMAGE,
                            org.talend.core.model.process.EParameterFieldType.TNS_EDITOR,
                            org.talend.core.model.process.EParameterFieldType.WSDL2JAVA,
                            org.talend.core.model.process.EParameterFieldType.GENERATEGRAMMARCONTROLLER,
                            org.talend.core.model.process.EParameterFieldType.GENERATE_SURVIVORSHIP_RULES_CONTROLLER,
                            org.talend.core.model.process.EParameterFieldType.REFRESH_REPORTS,
                            org.talend.core.model.process.EParameterFieldType.BROWSE_REPORTS,
                            org.talend.core.model.process.EParameterFieldType.PALO_DIM_SELECTION,
                            org.talend.core.model.process.EParameterFieldType.GUESS_SCHEMA,
                            org.talend.core.model.process.EParameterFieldType.MATCH_RULE_IMEX_CONTROLLER,
                            org.talend.core.model.process.EParameterFieldType.MEMO_PERL,
                            org.talend.core.model.process.EParameterFieldType.DBTYPE_LIST,
                            org.talend.core.model.process.EParameterFieldType.VERSION,
                            org.talend.core.model.process.EParameterFieldType.TECHNICAL,
                            org.talend.core.model.process.EParameterFieldType.ICON_SELECTION,
                            org.talend.core.model.process.EParameterFieldType.JAVA_COMMAND,
                            org.talend.core.model.process.EParameterFieldType.TREE_TABLE,
                            org.talend.core.model.process.EParameterFieldType.VALIDATION_RULE_TYPE,
                            org.talend.core.model.process.EParameterFieldType.DCSCHEMA,
                            org.talend.core.model.process.EParameterFieldType.SURVIVOR_RELATION,
                            org.talend.core.model.process.EParameterFieldType.REST_RESPONSE_SCHEMA_TYPE
                            )
                    );
                    for(org.talend.core.model.process.IElementParameter ep : org.talend.core.model.utils.NodeUtil.getDisplayedParameters(node)){
                        if(!ep.isLog4JEnabled() || ignoredParamsTypes.contains(ep.getFieldType())){
                            continue;
                        }
                        String name = ep.getName();
                        if(org.talend.core.model.process.EParameterFieldType.PASSWORD.equals(ep.getFieldType())){
                            String epName = "__" + name + "__";
                            String password = "";
                            if(org.talend.core.model.process.ElementParameterParser.canEncrypt(node, epName)){
                                password = org.talend.core.model.process.ElementParameterParser.getEncryptedValue(node, epName);
                            }else{
                                String passwordValue = org.talend.core.model.process.ElementParameterParser.getValue(node, epName);
                                if (passwordValue == null || "".equals(passwordValue.trim())) {// for the value which empty
                                    passwordValue = "\"\"";
                                } 
                                password = "routines.system.PasswordEncryptUtil.encryptPassword(" + passwordValue + ")";
                            } 
                            
    stringBuffer.append(TEXT_15);
    stringBuffer.append(var("log4jParamters"));
    stringBuffer.append(TEXT_16);
    stringBuffer.append(name);
    stringBuffer.append(TEXT_17);
    stringBuffer.append(password);
    stringBuffer.append(TEXT_18);
    
                        }else{
                            String value = org.talend.core.model.utils.NodeUtil.getNormalizeParameterValue(node, ep);
                            if(value.length()>10000){
                            value = org.talend.core.model.utils.NodeUtil.replaceCRLFInMEMO_SQL(value);
                            }
                            
    stringBuffer.append(TEXT_19);
    stringBuffer.append(var("log4jParamters"));
    stringBuffer.append(TEXT_20);
    stringBuffer.append(name);
    stringBuffer.append(TEXT_21);
    stringBuffer.append(value);
    stringBuffer.append(TEXT_22);
    
                        }   
                        
    stringBuffer.append(TEXT_23);
    stringBuffer.append(var("log4jParamters"));
    stringBuffer.append(TEXT_24);
    
                    }
                    debug(var("log4jParamters")); 
                    
    stringBuffer.append(TEXT_25);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_26);
    
        } 
        
    
    } 
    
    //no use for now, because we log the data by rowStruct
    public void traceData(String rowStruct, java.util.List<org.talend.core.model.metadata.IMetadataColumn> columnList, String nbline){
        if(log4jEnabled){
        
    stringBuffer.append(TEXT_27);
    stringBuffer.append(var("log4jSb"));
    stringBuffer.append(TEXT_28);
    
            for(org.talend.core.model.metadata.IMetadataColumn column : columnList){
                org.talend.core.model.metadata.types.JavaType javaType = org.talend.core.model.metadata.types.JavaTypesManager.getJavaTypeFromId(column.getTalendType());
                String columnName = column.getLabel();
                boolean isPrimit = org.talend.core.model.metadata.types.JavaTypesManager.isJavaPrimitiveType(column.getTalendType(), column.isNullable());
                if(isPrimit){
                
    stringBuffer.append(TEXT_29);
    stringBuffer.append(var("log4jSb"));
    stringBuffer.append(TEXT_30);
    stringBuffer.append(rowStruct);
    stringBuffer.append(TEXT_31);
    stringBuffer.append(columnName);
    stringBuffer.append(TEXT_32);
    
                }else{
                
    stringBuffer.append(TEXT_33);
    stringBuffer.append(rowStruct);
    stringBuffer.append(TEXT_34);
    stringBuffer.append(columnName);
    stringBuffer.append(TEXT_35);
    stringBuffer.append(TEXT_36);
    stringBuffer.append(var("log4jSb"));
    stringBuffer.append(TEXT_37);
    stringBuffer.append(TEXT_38);
    stringBuffer.append(var("log4jSb"));
    stringBuffer.append(TEXT_39);
    stringBuffer.append(rowStruct);
    stringBuffer.append(TEXT_40);
    stringBuffer.append(columnName);
    stringBuffer.append(TEXT_41);
    
                }
                
    stringBuffer.append(TEXT_42);
    stringBuffer.append(var("log4jSb"));
    stringBuffer.append(TEXT_43);
    
            }
        }
        trace(str("Content of the record "), nbline, str(": "), var("log4jSb"));
        
    
    }
}

class LogHelper{
    
    java.util.Map<String, String> pastDict = null;
    
    public LogHelper(){
        pastDict = new java.util.HashMap<String, String>();
        pastDict.put("insert", "inserted");
        pastDict.put("update", "updated");
        pastDict.put("delete", "deleted");
        pastDict.put("upsert", "upserted");
    }   
    
    public String upperFirstChar(String data){ 
        return data.substring(0, 1).toUpperCase() + data.substring(1);
    }
    
    public String toPastTense(String data){
        return pastDict.get(data);
    }
}
LogHelper logHelper = new LogHelper();

LogUtil log = null;

    
LogUtil logUtil = new LogUtil(node);
//Component params
String lms = ElementParameterParser.getValue(node, "__MIN_SUFFIX_LENGTH__");
String lmbs = ElementParameterParser.getValue(node, "__MAX_BLOCK_SIZE__");
String filteringThreshold = ElementParameterParser.getValue(node, "__FILTERING_THRESHOLD__");
String suspectSamplingSize = ElementParameterParser.getValue(node, "__SUSPECT_SAMPLING_SIZE__");

Boolean hasSeed = ElementParameterParser.getBooleanValue(node, "__HASSEED__");
String seed = hasSeed? "new scala.Some<Object>("+ ElementParameterParser.getValue(node,"__SEED__") + "L)" : "scala.Option.apply(null)";


List<Map<String, String>> bkvCols = (List<Map<String,String>>)ElementParameterParser.getObjectValue(node, "__BKV_TABLE__");
String bkvColNames = "bkvCols_" + cid;

//HDFS folder to save in
String hdfsFolder = ElementParameterParser.getValue(node,"__HDFS_FOLDER__");
boolean useConfigurationComponent = "true".equals(ElementParameterParser.getValue(node, "__DEFINE_STORAGE_CONFIGURATION__"));
String uriPrefix = "\"\"";
if(useConfigurationComponent) {
    uriPrefix = org.talend.designer.spark.generator.storage.SparkStorageUtils.getURIPrefix(node);
    hdfsFolder = uriPrefix + " + " + hdfsFolder;
}

boolean isUsingTDS = "true".equals(ElementParameterParser.getValue(node, "__IS_USING_TDS__")); 
String tdsUrl = ElementParameterParser.getValue(node, "__TDS_URL__");
String tdsUsername = ElementParameterParser.getValue(node, "__TDS_USERNAME__");
String tdsPassword = ElementParameterParser.getValue(node, "__TDS_PASSWORD__");
String tdsCampaign = ElementParameterParser.getValue(node, "__TDS_CAMPAIGN__");
String tdsAssignee = ElementParameterParser.getValue(node, "__TDS_ASSIGNEE__");
String tdsBatchSize = ElementParameterParser.getValue(node, "__TDS_BATCH_SIZE__");

TModelEncoderUtil tModelEncoderUtil = new TModelEncoderUtil(node);
String stagesName = "stages_" + tModelEncoderUtil.getFirstModelEncoderName();
String trainingName = "training_" + tModelEncoderUtil.getFirstModelEncoderName();

if(tModelEncoderUtil.isFirstModelEncoder()) {
    // We're on the first tModelEncoder, so we need to convert the incoming RDD to DataFrame
    String rddName, structName;
    if(tSqlRowUtil.containsDateFields(incomingConnection)) {
        // Additional map to convert from java.util.Date to java.sql.Timestamp
            String newRddName = "tmp_rdd_"+incomingConnectionName;
            String newStructName = "DF_"+incomingStructName+"AvroRecord";

    stringBuffer.append(TEXT_44);
    stringBuffer.append(newStructName);
    stringBuffer.append(TEXT_45);
    stringBuffer.append(newRddName);
    stringBuffer.append(TEXT_46);
    stringBuffer.append(incomingConnectionName);
    stringBuffer.append(TEXT_47);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_48);
    stringBuffer.append(incomingStructName);
    stringBuffer.append(TEXT_49);
    stringBuffer.append(newStructName);
    stringBuffer.append(TEXT_50);
    
            rddName = newRddName;
            structName = newStructName;
    } else {
            // No need for additional map
            rddName = "rdd_"+incomingConnectionName;
            structName = incomingStructName;
    }

    stringBuffer.append(TEXT_51);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_52);
    stringBuffer.append(dataframeClass);
    stringBuffer.append(TEXT_53);
    stringBuffer.append(trainingName);
    stringBuffer.append(TEXT_54);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_55);
    stringBuffer.append(rddName);
    stringBuffer.append(TEXT_56);
    stringBuffer.append(structName);
    stringBuffer.append(TEXT_57);
    stringBuffer.append(stagesName);
    stringBuffer.append(TEXT_58);
    
} // end if(tModelEncoderUtil.isFirstModelEncoder())

    stringBuffer.append(TEXT_59);
    stringBuffer.append(bkvColNames);
    stringBuffer.append(TEXT_60);
    
	   if((bkvCols!=null)&&(bkvCols.size() > 0)){
	      for(Map<String,String> bkvColMap: bkvCols){
	
    stringBuffer.append(TEXT_61);
    stringBuffer.append(bkvColNames);
    stringBuffer.append(TEXT_62);
    stringBuffer.append(bkvColMap.get("INPUT_COLUMN"));
    stringBuffer.append(TEXT_63);
    
	      }
	   }
	
    stringBuffer.append(TEXT_64);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_65);
    stringBuffer.append(TEXT_66);
    stringBuffer.append(bkvColNames);
    stringBuffer.append(TEXT_67);
    stringBuffer.append(bkvColNames);
    stringBuffer.append(TEXT_68);
    stringBuffer.append(TEXT_69);
    stringBuffer.append(hasExactConnection);
    stringBuffer.append(TEXT_70);
    stringBuffer.append(TEXT_71);
    stringBuffer.append(hasUniqueConnection);
    stringBuffer.append(TEXT_72);
    stringBuffer.append(TEXT_73);
    stringBuffer.append(hasSuspectSamplingConnection);
    stringBuffer.append(TEXT_74);
    stringBuffer.append(isUsingTDS);
    stringBuffer.append(TEXT_75);
    stringBuffer.append(TEXT_76);
    stringBuffer.append(lms);
    stringBuffer.append(TEXT_77);
    stringBuffer.append(TEXT_78);
    stringBuffer.append(lmbs);
    stringBuffer.append(TEXT_79);
    stringBuffer.append(TEXT_80);
    stringBuffer.append(filteringThreshold);
    stringBuffer.append(TEXT_81);
    stringBuffer.append(TEXT_82);
    stringBuffer.append(suspectSamplingSize);
    stringBuffer.append(TEXT_83);
    stringBuffer.append(TEXT_84);
    stringBuffer.append(seed);
    stringBuffer.append(TEXT_85);
    stringBuffer.append(dataframeClass);
    stringBuffer.append(TEXT_86);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_87);
    stringBuffer.append(TEXT_88);
    stringBuffer.append(trainingName);
    stringBuffer.append(TEXT_89);
    stringBuffer.append(TEXT_90);
    stringBuffer.append(stagesName);
    stringBuffer.append(TEXT_91);
    stringBuffer.append(stagesName);
    stringBuffer.append(TEXT_92);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_93);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_94);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_95);
    
	if(hasSuspectConnection){
	    String suspectOutConnectionName = suspectConnection.getName();
	    String suspectOutStructName = codeGenArgument.getRecordStructName(suspectConnection);
	
    stringBuffer.append(TEXT_96);
    stringBuffer.append(suspectOutStructName);
    stringBuffer.append(TEXT_97);
    stringBuffer.append(suspectOutConnectionName);
    stringBuffer.append(TEXT_98);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_99);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_100);
    stringBuffer.append(suspectOutStructName);
    stringBuffer.append(TEXT_101);
    
	}
	
    stringBuffer.append(TEXT_102);
    
    if(hasExactConnection){
        String exactOutConnectionName = exactConnection.getName();
        String exactOutStructName = codeGenArgument.getRecordStructName(exactConnection);
    
    stringBuffer.append(TEXT_103);
    stringBuffer.append(exactOutStructName);
    stringBuffer.append(TEXT_104);
    stringBuffer.append(exactOutConnectionName);
    stringBuffer.append(TEXT_105);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_106);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_107);
    stringBuffer.append(exactOutStructName);
    stringBuffer.append(TEXT_108);
    
    }
    
    stringBuffer.append(TEXT_109);
    
	if(hasUniqueConnection){
	    String uniqueOutConnectionName = uniqueConnection.getName();
	    String uniqueOutStructName = codeGenArgument.getRecordStructName(uniqueConnection);
	
    stringBuffer.append(TEXT_110);
    stringBuffer.append(uniqueOutStructName);
    stringBuffer.append(TEXT_111);
    stringBuffer.append(uniqueOutConnectionName);
    stringBuffer.append(TEXT_112);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_113);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_114);
    stringBuffer.append(uniqueOutStructName);
    stringBuffer.append(TEXT_115);
    
	}
	
    stringBuffer.append(TEXT_116);
    
	if(hasSuspectSamplingConnection){
	    String suspectSamplingOutConnectionName = suspectSamplingConnection.getName();
	    String suspectSamplingOutStructName = codeGenArgument.getRecordStructName(suspectSamplingConnection);
	
    stringBuffer.append(TEXT_117);
    stringBuffer.append(suspectSamplingOutStructName);
    stringBuffer.append(TEXT_118);
    stringBuffer.append(suspectSamplingOutConnectionName);
    stringBuffer.append(TEXT_119);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_120);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_121);
    stringBuffer.append(suspectSamplingOutStructName);
    stringBuffer.append(TEXT_122);
    
	}
	
    stringBuffer.append(TEXT_123);
    
    if(isUsingTDS){
        String httpRequestClass = "com.github.kevinsawicki.http.HttpRequest";
    
    stringBuffer.append(TEXT_124);
    stringBuffer.append(tdsUrl);
    stringBuffer.append(TEXT_125);
    stringBuffer.append(tdsUrl);
    stringBuffer.append(TEXT_126);
    stringBuffer.append(tdsUrl);
    stringBuffer.append(TEXT_127);
    stringBuffer.append(tdsUsername);
    stringBuffer.append(TEXT_128);
    stringBuffer.append(httpRequestClass);
    stringBuffer.append(TEXT_129);
    stringBuffer.append(tdsPassword);
    stringBuffer.append(TEXT_130);
    stringBuffer.append(httpRequestClass);
    stringBuffer.append(TEXT_131);
     logUtil.debug("\"TDS Login URL: \"+ loginUrl");
    stringBuffer.append(TEXT_132);
    stringBuffer.append(httpRequestClass);
    stringBuffer.append(TEXT_133);
    stringBuffer.append(httpRequestClass);
    stringBuffer.append(TEXT_134);
     logUtil.error("\"Data Stewardship username or password is incorrect.\"");
    stringBuffer.append(TEXT_135);
     logUtil.error("\"Data Stewardship URL not found.\"");
    stringBuffer.append(TEXT_136);
     logUtil.error("\"Login Data Stewardship failed.\"");
    stringBuffer.append(TEXT_137);
    stringBuffer.append(TEXT_138);
    stringBuffer.append(dataframeClass);
    stringBuffer.append(TEXT_139);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_140);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_141);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_142);
     if(isSpark1) { 
    stringBuffer.append(TEXT_143);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_144);
     } else { 
    stringBuffer.append(TEXT_145);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_146);
     } 
    stringBuffer.append(TEXT_147);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_148);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_149);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_150);
    stringBuffer.append(tdsAssignee);
    stringBuffer.append(TEXT_151);
    stringBuffer.append(tdsAssignee);
    stringBuffer.append(TEXT_152);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_153);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_154);
    stringBuffer.append(tdsBatchSize);
    stringBuffer.append(TEXT_155);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_156);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_157);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_158);
    stringBuffer.append(tdsCampaign);
    stringBuffer.append(TEXT_159);
    stringBuffer.append(TEXT_160);
    stringBuffer.append(httpRequestClass);
    stringBuffer.append(TEXT_161);
    stringBuffer.append(httpRequestClass);
    stringBuffer.append(TEXT_162);
    stringBuffer.append(httpRequestClass);
    stringBuffer.append(TEXT_163);
    stringBuffer.append(httpRequestClass);
    stringBuffer.append(TEXT_164);
     logUtil.error("\"Create Data Stewardship tasks failed: \"+statusCode");
    stringBuffer.append(TEXT_165);
     logUtil.warn("\"Data Stewardship response contains no content \"");
    stringBuffer.append(TEXT_166);
     logUtil.error("\"The task is not assignable to the user: \" + "+ tdsAssignee);
    stringBuffer.append(TEXT_167);
     logUtil.error("\"Some Data Stewardship records are invalid. Reject count :\"+rejectCount");
    stringBuffer.append(TEXT_168);
    stringBuffer.append(TEXT_169);
    stringBuffer.append(httpRequestClass);
    stringBuffer.append(TEXT_170);
    
    }
    
    
        if(!"\"\"".equals(uriPrefix)) {
            
    stringBuffer.append(TEXT_171);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_172);
    stringBuffer.append(uriPrefix);
    stringBuffer.append(TEXT_173);
    
        }
    
    stringBuffer.append(TEXT_174);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_175);
    stringBuffer.append(hdfsFolder);
    stringBuffer.append(TEXT_176);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_177);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_178);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_179);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_180);
    stringBuffer.append(hdfsFolder);
    stringBuffer.append(TEXT_181);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_182);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_183);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_184);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_185);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_186);
    stringBuffer.append(hdfsFolder);
    stringBuffer.append(TEXT_187);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_188);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_189);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_190);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_191);
    return stringBuffer.toString();
  }
}
