package org.talend.designer.codegen.translators.machinelearning.classification;

import java.util.List;
import org.talend.core.model.metadata.IMetadataColumn;
import org.talend.core.model.metadata.IMetadataTable;
import org.talend.core.model.metadata.types.JavaType;
import org.talend.core.model.metadata.types.JavaTypesManager;
import org.talend.core.model.process.ElementParameterParser;
import org.talend.core.model.process.EConnectionType;
import org.talend.core.model.process.IConnection;
import org.talend.core.model.process.IConnectionCategory;
import org.talend.core.model.process.INode;
import org.talend.core.model.process.IBigDataNode;
import org.talend.designer.common.tsqlrow.TSqlRowUtil;
import org.talend.designer.common.BigDataCodeGeneratorArgument;

public class TPredictSparkconfigJava
{
  protected static String nl;
  public static synchronized TPredictSparkconfigJava create(String lineSeparator)
  {
    nl = lineSeparator;
    TPredictSparkconfigJava result = new TPredictSparkconfigJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = NL + "// 1. Model Loading" + NL + "// Common processing for both batch & streaming version of tPredict" + NL + "final org.apache.spark.mllib.classification.talend.NaiveBayesRegularRecordsModel model_";
  protected final String TEXT_2 = " =";
  protected final String TEXT_3 = NL + "        (org.apache.spark.mllib.classification.talend.NaiveBayesRegularRecordsModel) org.talend.datascience.mllib.pmml.imports.ModelImporter" + NL + "            .fromPMML(";
  protected final String TEXT_4 = ", ";
  protected final String TEXT_5 = ");";
  protected final String TEXT_6 = NL + "        (org.apache.spark.mllib.classification.talend.NaiveBayesRegularRecordsModel) org.talend.datascience.mllib.pmml.imports.ModelImporter" + NL + "                .fromPMML(";
  protected final String TEXT_7 = ");";
  protected final String TEXT_8 = NL;
  protected final String TEXT_9 = NL + NL + "\t//Streaming version of tPredict" + NL + "\torg.apache.spark.streaming.api.java.JavaDStream<";
  protected final String TEXT_10 = ">" + NL + "\t\trdd_";
  protected final String TEXT_11 = " = rdd_";
  protected final String TEXT_12 = ".transform(" + NL + "" + NL + "\t\t    new org.apache.spark.api.java.function.Function<org.apache.spark.api.java.JavaRDD<";
  protected final String TEXT_13 = ">, org.apache.spark.api.java.JavaRDD<";
  protected final String TEXT_14 = ">>() {" + NL + "\t\t\t\t@Override" + NL + "\t\t\t\tpublic org.apache.spark.api.java.JavaRDD<";
  protected final String TEXT_15 = "> call(org.apache.spark.api.java.JavaRDD<";
  protected final String TEXT_16 = "> rdd) {" + NL + "" + NL + "\t\t\t\t\t// 2. Create dataFrame" + NL + "\t\t\t\t\torg.apache.spark.sql.SQLContext sqlContext_";
  protected final String TEXT_17 = " = org.talend.datascience.util.SQLUtil.getSQLContextSingleton(rdd.context());" + NL + "\t\t\t\t    org.apache.spark.sql.DataFrame df_";
  protected final String TEXT_18 = " =" + NL + "\t\t\t\t    \tsqlContext_";
  protected final String TEXT_19 = ".createDataFrame(rdd, ";
  protected final String TEXT_20 = ".class);" + NL + "" + NL + "\t\t\t\t    // 3. Call predictor" + NL + "\t\t\t\t    return (org.talend.datascience.mllib.classification.NaiveBayes.predictor(" + NL + "\t\t\t\t    \t\t\tdf_";
  protected final String TEXT_21 = ".rdd(), // rdd<Row>" + NL + "\t\t\t\t    \t\t\tdf_";
  protected final String TEXT_22 = ".schema(), // schema" + NL + "\t\t\t\t    \t\t\tmodel_";
  protected final String TEXT_23 = " // model" + NL + "\t\t\t\t    \t\t).toJavaRDD()).map(new ";
  protected final String TEXT_24 = "_FromRowTo";
  protected final String TEXT_25 = "());" + NL + "\t\t\t\t}" + NL + "\t\t\t}" + NL + "\t\t);" + NL;
  protected final String TEXT_26 = NL + "\t// Batch version of tPredict" + NL + "\t// 2. Create dataFrame from incoming rdd & rowStruct" + NL + "\torg.apache.spark.sql.SQLContext sqlContext_";
  protected final String TEXT_27 = " = new org.apache.spark.sql.SQLContext(";
  protected final String TEXT_28 = ");" + NL + "\torg.apache.spark.sql.DataFrame df_";
  protected final String TEXT_29 = "_";
  protected final String TEXT_30 = " =" + NL + "\t\tsqlContext_";
  protected final String TEXT_31 = ".createDataFrame(rdd_";
  protected final String TEXT_32 = ", ";
  protected final String TEXT_33 = ".class);" + NL + "" + NL + "\t// 3. Call predictor" + NL + "\torg.apache.spark.api.java.JavaRDD<";
  protected final String TEXT_34 = "> rdd_";
  protected final String TEXT_35 = " =" + NL + "\t\t(org.talend.datascience.mllib.classification.NaiveBayes.predictor(" + NL + "\t\t\tdf_";
  protected final String TEXT_36 = "_";
  protected final String TEXT_37 = ".rdd(), // rdd<Row>" + NL + "\t\t\tdf_";
  protected final String TEXT_38 = "_";
  protected final String TEXT_39 = ".schema(), // schema" + NL + "\t\t\tmodel_";
  protected final String TEXT_40 = " // model" + NL + "\t\t).toJavaRDD()).map(new ";
  protected final String TEXT_41 = "_FromRowTo";
  protected final String TEXT_42 = "());";
  protected final String TEXT_43 = NL + "org.apache.spark.api.java.JavaRDD<";
  protected final String TEXT_44 = "> rdd_";
  protected final String TEXT_45 = ";" + NL + "{";
  protected final String TEXT_46 = NL + "            java.net.URI currentURI_";
  protected final String TEXT_47 = "_config = FileSystem.getDefaultUri(ctx.hadoopConfiguration());" + NL + "            FileSystem.setDefaultUri(ctx.hadoopConfiguration(), new java.net.URI(";
  protected final String TEXT_48 = "));" + NL + "            fs = FileSystem.get(ctx.hadoopConfiguration());";
  protected final String TEXT_49 = NL + "        com.esotericsoftware.kryo.Kryo kryo_";
  protected final String TEXT_50 = " = new com.esotericsoftware.kryo.Kryo();" + NL + "        com.esotericsoftware.kryo.io.Input featuresInput_";
  protected final String TEXT_51 = " = new com.esotericsoftware.kryo.io.Input(fs.open(new org.apache.hadoop.fs.Path(";
  protected final String TEXT_52 = " + \"/features\")));" + NL + "        TalendPipelineModel featuresTalendPipelineModel_";
  protected final String TEXT_53 = " = kryo_";
  protected final String TEXT_54 = ".readObject(featuresInput_";
  protected final String TEXT_55 = ", TalendPipelineModel.class, new TalendPipelineModelSerializer());" + NL + "" + NL + "        org.apache.spark.mllib.classification.NaiveBayesModel currentModel_";
  protected final String TEXT_56 = " = org.apache.spark.mllib.classification.NaiveBayesModel.load(ctx.sc(), ";
  protected final String TEXT_57 = " + \"/model\");";
  protected final String TEXT_58 = NL + "            FileSystem.setDefaultUri(ctx.hadoopConfiguration(), currentURI_";
  protected final String TEXT_59 = "_config);" + NL + "            fs = FileSystem.get(ctx.hadoopConfiguration());";
  protected final String TEXT_60 = NL + "        TalendPipelineModel featuresTalendPipelineModel_";
  protected final String TEXT_61 = " = (TalendPipelineModel)globalMap.get(\"";
  protected final String TEXT_62 = "_PIPELINE\");" + NL + "" + NL + "        Object temporaryModel_";
  protected final String TEXT_63 = " = globalMap.get(\"";
  protected final String TEXT_64 = "_MODEL\");" + NL + "        if (temporaryModel_";
  protected final String TEXT_65 = " == null) {" + NL + "            throw new RuntimeException(\"The selected model does not exist\");" + NL + "        }else if (!(temporaryModel_";
  protected final String TEXT_66 = " instanceof org.apache.spark.mllib.classification.NaiveBayesModel)) {" + NL + "            throw new RuntimeException(\"The selected model is of type \" + temporaryModel_";
  protected final String TEXT_67 = ".getClass() + \" is should be of type org.apache.spark.mllib.classification.NaiveBayesModel\");" + NL + "        }" + NL + "        org.apache.spark.mllib.classification.NaiveBayesModel currentModel_";
  protected final String TEXT_68 = "= (org.apache.spark.mllib.classification.NaiveBayesModel) temporaryModel_";
  protected final String TEXT_69 = ";" + NL;
  protected final String TEXT_70 = NL + "    java.util.Map<String, String> featuresParamsMap_";
  protected final String TEXT_71 = " = featuresTalendPipelineModel_";
  protected final String TEXT_72 = ".getParams();" + NL + "    org.apache.spark.ml.PipelineModel featuresTransformationsModel_";
  protected final String TEXT_73 = " = featuresTalendPipelineModel_";
  protected final String TEXT_74 = ".getPipelineModel();" + NL + "    String vectorName_";
  protected final String TEXT_75 = " = featuresParamsMap_";
  protected final String TEXT_76 = ".get(\"VECTOR_NAME\");" + NL + "" + NL + "    // Pipeline" + NL + "    org.apache.spark.sql.SQLContext sqlContext_";
  protected final String TEXT_77 = " = new org.apache.spark.sql.SQLContext(ctx);";
  protected final String TEXT_78 = NL + "    ";
  protected final String TEXT_79 = " inputDataFrame";
  protected final String TEXT_80 = " = sqlContext_";
  protected final String TEXT_81 = ".createDataFrame(rdd_";
  protected final String TEXT_82 = ", ";
  protected final String TEXT_83 = ".class);";
  protected final String TEXT_84 = NL + "    ";
  protected final String TEXT_85 = " transformedInputDataFrame_";
  protected final String TEXT_86 = " = featuresTransformationsModel_";
  protected final String TEXT_87 = ".transform(inputDataFrame";
  protected final String TEXT_88 = ");" + NL;
  protected final String TEXT_89 = NL + "        // output row,output the double lable to String." + NL + "        org.apache.spark.ml.feature.StringIndexerModel sim = (org.apache.spark.ml.feature.StringIndexerModel) featuresTransformationsModel_";
  protected final String TEXT_90 = ".stages()[1];";
  protected final String TEXT_91 = NL + "            org.apache.spark.rdd.RDD<";
  protected final String TEXT_92 = "> rdd = transformedInputDataFrame_";
  protected final String TEXT_93 = ".map(" + NL + "                    new StringIndexerInverseFunction_";
  protected final String TEXT_94 = "(sim,vectorName_";
  protected final String TEXT_95 = ",currentModel_";
  protected final String TEXT_96 = ")," + NL + "                    scala.reflect.ClassManifestFactory.fromClass(";
  protected final String TEXT_97 = ".class));" + NL + "            rdd_";
  protected final String TEXT_98 = " = org.apache.spark.api.java.JavaRDD.fromRDD(rdd, rdd.org$apache$spark$rdd$RDD$$evidence$1);";
  protected final String TEXT_99 = NL + "            rdd_";
  protected final String TEXT_100 = " = transformedInputDataFrame_";
  protected final String TEXT_101 = ".map(new StringIndexerInverseFunction_";
  protected final String TEXT_102 = "(sim,vectorName_";
  protected final String TEXT_103 = ",currentModel_";
  protected final String TEXT_104 = ")," + NL + "                    org.apache.spark.sql.Encoders.bean(";
  protected final String TEXT_105 = ".class)).toJavaRDD();";
  protected final String TEXT_106 = NL + "        // TODO";
  protected final String TEXT_107 = NL + "}";
  protected final String TEXT_108 = NL + "org.apache.spark.api.java.JavaRDD<";
  protected final String TEXT_109 = "> rdd_";
  protected final String TEXT_110 = ";" + NL + "{";
  protected final String TEXT_111 = NL + "            java.net.URI currentURI_";
  protected final String TEXT_112 = "_config = FileSystem.getDefaultUri(ctx.hadoopConfiguration());" + NL + "            FileSystem.setDefaultUri(ctx.hadoopConfiguration(), new java.net.URI(";
  protected final String TEXT_113 = "));" + NL + "            fs = FileSystem.get(ctx.hadoopConfiguration());";
  protected final String TEXT_114 = NL + "        com.esotericsoftware.kryo.Kryo kryo_";
  protected final String TEXT_115 = " = new com.esotericsoftware.kryo.Kryo();" + NL + "        com.esotericsoftware.kryo.io.Input featuresInput_";
  protected final String TEXT_116 = " = new com.esotericsoftware.kryo.io.Input(fs.open(new org.apache.hadoop.fs.Path(";
  protected final String TEXT_117 = " + \"/model/\")));" + NL + "        TalendPipelineModelSerializer talendPipelineModelSerializer_";
  protected final String TEXT_118 = " = new TalendPipelineModelSerializer();" + NL + "        TalendPipelineModel featuresTalendPipelineModel_";
  protected final String TEXT_119 = " = kryo_";
  protected final String TEXT_120 = ".readObject(featuresInput_";
  protected final String TEXT_121 = ", TalendPipelineModel.class, talendPipelineModelSerializer_";
  protected final String TEXT_122 = ");" + NL + "" + NL + "        java.util.Map<String, String> featuresParamsMap_";
  protected final String TEXT_123 = " = featuresTalendPipelineModel_";
  protected final String TEXT_124 = ".getParams();" + NL + "" + NL + "        org.apache.spark.ml.PipelineModel pipelineModel_";
  protected final String TEXT_125 = " = featuresTalendPipelineModel_";
  protected final String TEXT_126 = ".getPipelineModel();";
  protected final String TEXT_127 = NL + "            FileSystem.setDefaultUri(ctx.hadoopConfiguration(), currentURI_";
  protected final String TEXT_128 = "_config);" + NL + "            fs = FileSystem.get(ctx.hadoopConfiguration());";
  protected final String TEXT_129 = NL + "        org.apache.spark.ml.PipelineModel pipelineModel_";
  protected final String TEXT_130 = ";" + NL + "        {" + NL + "            Object model = globalMap.get(\"";
  protected final String TEXT_131 = "_MODEL\");" + NL + "            if (model instanceof org.apache.spark.ml.PipelineModel) {" + NL + "                pipelineModel_";
  protected final String TEXT_132 = " = (org.apache.spark.ml.PipelineModel) model;" + NL + "            } else {" + NL + "                throw new Exception(\"Unsupported model type: \" + model.getClass());" + NL + "            }" + NL + "        }";
  protected final String TEXT_133 = NL + NL + "    // Convert incoming RDD to DataFrame" + NL + "    org.apache.spark.sql.SQLContext sqlContext_";
  protected final String TEXT_134 = " = new org.apache.spark.sql.SQLContext(ctx);";
  protected final String TEXT_135 = NL + "    ";
  protected final String TEXT_136 = " df_";
  protected final String TEXT_137 = " = sqlContext_";
  protected final String TEXT_138 = ".createDataFrame(rdd_";
  protected final String TEXT_139 = ", ";
  protected final String TEXT_140 = ".class);";
  protected final String TEXT_141 = NL + "    ";
  protected final String TEXT_142 = " results_";
  protected final String TEXT_143 = " = pipelineModel_";
  protected final String TEXT_144 = ".transform(df_";
  protected final String TEXT_145 = ");" + NL + "" + NL + "    // Convert DataFrame back to RDD" + NL + "    rdd_";
  protected final String TEXT_146 = " = results_";
  protected final String TEXT_147 = ".toJavaRDD().map(new ";
  protected final String TEXT_148 = "_FromRowTo";
  protected final String TEXT_149 = "());" + NL + "}";
  protected final String TEXT_150 = NL + "org.apache.spark.api.java.JavaRDD<";
  protected final String TEXT_151 = "> rdd_";
  protected final String TEXT_152 = ";" + NL + "{";
  protected final String TEXT_153 = NL + "            java.net.URI currentURI_";
  protected final String TEXT_154 = "_config = FileSystem.getDefaultUri(ctx.hadoopConfiguration());" + NL + "            FileSystem.setDefaultUri(ctx.hadoopConfiguration(), new java.net.URI(";
  protected final String TEXT_155 = "));" + NL + "            fs = FileSystem.get(ctx.hadoopConfiguration());";
  protected final String TEXT_156 = NL + "        com.esotericsoftware.kryo.Kryo kryo_";
  protected final String TEXT_157 = " = new com.esotericsoftware.kryo.Kryo();";
  protected final String TEXT_158 = NL + "            org.apache.hadoop.fs.FSDataInputStream fsdis_";
  protected final String TEXT_159 = " = fs.open(new org.apache.hadoop.fs.Path(";
  protected final String TEXT_160 = " + \"/model/\"));" + NL + "            byte[] ba_";
  protected final String TEXT_161 = " = org.apache.commons.io.IOUtils.toByteArray(fsdis_";
  protected final String TEXT_162 = " );" + NL + "            java.io.ByteArrayInputStream bais_";
  protected final String TEXT_163 = " = new java.io.ByteArrayInputStream(ba_";
  protected final String TEXT_164 = ");" + NL + "            com.esotericsoftware.kryo.io.Input featuresInput_";
  protected final String TEXT_165 = " = new com.esotericsoftware.kryo.io.Input(bais_";
  protected final String TEXT_166 = ");";
  protected final String TEXT_167 = NL + "            com.esotericsoftware.kryo.io.Input featuresInput_";
  protected final String TEXT_168 = " = new com.esotericsoftware.kryo.io.Input(fs.open(new org.apache.hadoop.fs.Path(";
  protected final String TEXT_169 = " + \"/model/\")));";
  protected final String TEXT_170 = NL + "        TalendPipelineModelSerializer talendPipelineModelSerializer_";
  protected final String TEXT_171 = " = new TalendPipelineModelSerializer();" + NL + "        TalendPipelineModel featuresTalendPipelineModel_";
  protected final String TEXT_172 = " = kryo_";
  protected final String TEXT_173 = ".readObject(featuresInput_";
  protected final String TEXT_174 = ", TalendPipelineModel.class, talendPipelineModelSerializer_";
  protected final String TEXT_175 = ");" + NL + "" + NL + "        java.util.Map<String, String> featuresParamsMap_";
  protected final String TEXT_176 = " = featuresTalendPipelineModel_";
  protected final String TEXT_177 = ".getParams();" + NL + "" + NL + "        org.apache.spark.ml.PipelineModel pipelineModel_";
  protected final String TEXT_178 = " = featuresTalendPipelineModel_";
  protected final String TEXT_179 = ".getPipelineModel();";
  protected final String TEXT_180 = NL + "            FileSystem.setDefaultUri(ctx.hadoopConfiguration(), currentURI_";
  protected final String TEXT_181 = "_config);" + NL + "            fs = FileSystem.get(ctx.hadoopConfiguration());";
  protected final String TEXT_182 = NL + "        org.apache.spark.ml.PipelineModel pipelineModel_";
  protected final String TEXT_183 = ";" + NL + "        {" + NL + "            Object model = globalMap.get(\"";
  protected final String TEXT_184 = "_MODEL\");" + NL + "            if (model instanceof org.apache.spark.ml.PipelineModel) {" + NL + "                pipelineModel_";
  protected final String TEXT_185 = " = (org.apache.spark.ml.PipelineModel) model;" + NL + "            } else {" + NL + "                throw new Exception(\"Unsupported model type: \" + model.getClass());" + NL + "            }" + NL + "        }";
  protected final String TEXT_186 = NL + NL + "    // Convert incoming RDD to DataFrame" + NL + "    org.apache.spark.sql.SQLContext sqlContext_";
  protected final String TEXT_187 = " = new org.apache.spark.sql.SQLContext(ctx);";
  protected final String TEXT_188 = NL + "    ";
  protected final String TEXT_189 = " df_";
  protected final String TEXT_190 = " = sqlContext_";
  protected final String TEXT_191 = ".createDataFrame(rdd_";
  protected final String TEXT_192 = ", ";
  protected final String TEXT_193 = ".class);";
  protected final String TEXT_194 = NL + "    ";
  protected final String TEXT_195 = " results = pipelineModel_";
  protected final String TEXT_196 = ".transform(df_";
  protected final String TEXT_197 = ");" + NL;
  protected final String TEXT_198 = NL + "        org.apache.spark.ml.feature.StringIndexerModel sim = (org.apache.spark.ml.feature.StringIndexerModel) pipelineModel_";
  protected final String TEXT_199 = ".stages()[1];";
  protected final String TEXT_200 = NL + "            org.apache.spark.rdd.RDD<";
  protected final String TEXT_201 = "> rdd = results.map(" + NL + "                    new StringIndexerInverseFunction_";
  protected final String TEXT_202 = "(sim)," + NL + "                    scala.reflect.ClassManifestFactory.fromClass(";
  protected final String TEXT_203 = ".class));" + NL + "            rdd_";
  protected final String TEXT_204 = " = org.apache.spark.api.java.JavaRDD.fromRDD(rdd, rdd.org$apache$spark$rdd$RDD$$evidence$1);";
  protected final String TEXT_205 = NL + "            rdd_";
  protected final String TEXT_206 = " = results.map(new StringIndexerInverseFunction_";
  protected final String TEXT_207 = "(sim)," + NL + "                    org.apache.spark.sql.Encoders.bean(";
  protected final String TEXT_208 = ".class)).toJavaRDD();";
  protected final String TEXT_209 = NL + "        // TODO";
  protected final String TEXT_210 = NL + "}";
  protected final String TEXT_211 = NL + "org.apache.spark.api.java.JavaRDD<";
  protected final String TEXT_212 = "> rdd_";
  protected final String TEXT_213 = ";" + NL + "{";
  protected final String TEXT_214 = NL + "            java.net.URI currentURI_";
  protected final String TEXT_215 = "_config = FileSystem.getDefaultUri(ctx.hadoopConfiguration());" + NL + "            FileSystem.setDefaultUri(ctx.hadoopConfiguration(), new java.net.URI(";
  protected final String TEXT_216 = "));" + NL + "            fs = FileSystem.get(ctx.hadoopConfiguration());";
  protected final String TEXT_217 = NL + "        com.esotericsoftware.kryo.Kryo kryo_";
  protected final String TEXT_218 = " = new com.esotericsoftware.kryo.Kryo();";
  protected final String TEXT_219 = NL + "            org.apache.hadoop.fs.FSDataInputStream fsdis_";
  protected final String TEXT_220 = " = fs.open(new org.apache.hadoop.fs.Path(";
  protected final String TEXT_221 = " + \"/model/\"));" + NL + "            byte[] ba_";
  protected final String TEXT_222 = " = org.apache.commons.io.IOUtils.toByteArray(fsdis_";
  protected final String TEXT_223 = " );" + NL + "            java.io.ByteArrayInputStream bais_";
  protected final String TEXT_224 = " = new java.io.ByteArrayInputStream(ba_";
  protected final String TEXT_225 = ");" + NL + "            com.esotericsoftware.kryo.io.Input featuresInput_";
  protected final String TEXT_226 = " = new com.esotericsoftware.kryo.io.Input(bais_";
  protected final String TEXT_227 = ");";
  protected final String TEXT_228 = NL + "            com.esotericsoftware.kryo.io.Input featuresInput_";
  protected final String TEXT_229 = " = new com.esotericsoftware.kryo.io.Input(fs.open(new org.apache.hadoop.fs.Path(";
  protected final String TEXT_230 = " + \"/model/\")));";
  protected final String TEXT_231 = NL + "        TalendPipelineModelSerializer talendPipelineModelSerializer_";
  protected final String TEXT_232 = " = new TalendPipelineModelSerializer();" + NL + "        TalendPipelineModel featuresTalendPipelineModel_";
  protected final String TEXT_233 = " = kryo_";
  protected final String TEXT_234 = ".readObject(featuresInput_";
  protected final String TEXT_235 = ", TalendPipelineModel.class, talendPipelineModelSerializer_";
  protected final String TEXT_236 = ");" + NL + "" + NL + "        java.util.Map<String, String> featuresParamsMap_";
  protected final String TEXT_237 = " = featuresTalendPipelineModel_";
  protected final String TEXT_238 = ".getParams();" + NL + "" + NL + "        org.apache.spark.ml.PipelineModel pipelineModel_";
  protected final String TEXT_239 = " = featuresTalendPipelineModel_";
  protected final String TEXT_240 = ".getPipelineModel();";
  protected final String TEXT_241 = NL + "            FileSystem.setDefaultUri(ctx.hadoopConfiguration(), currentURI_";
  protected final String TEXT_242 = "_config);" + NL + "            fs = FileSystem.get(ctx.hadoopConfiguration());";
  protected final String TEXT_243 = NL + "        org.apache.spark.ml.PipelineModel pipelineModel_";
  protected final String TEXT_244 = ";" + NL + "        {" + NL + "            Object model = globalMap.get(\"";
  protected final String TEXT_245 = "_MODEL\");" + NL + "            if (model instanceof org.apache.spark.ml.PipelineModel) {" + NL + "                pipelineModel_";
  protected final String TEXT_246 = " = (org.apache.spark.ml.PipelineModel) model;" + NL + "            } else {" + NL + "                throw new Exception(\"Unsupported model type: \" + model.getClass());" + NL + "            }" + NL + "        }";
  protected final String TEXT_247 = NL + NL + "    // Convert incoming RDD to DataFrame" + NL + "    org.apache.spark.sql.SQLContext sqlContext_";
  protected final String TEXT_248 = " = new org.apache.spark.sql.SQLContext(ctx);";
  protected final String TEXT_249 = NL + "    ";
  protected final String TEXT_250 = " df_";
  protected final String TEXT_251 = " = sqlContext_";
  protected final String TEXT_252 = ".createDataFrame(rdd_";
  protected final String TEXT_253 = ", ";
  protected final String TEXT_254 = ".class);";
  protected final String TEXT_255 = NL + "    ";
  protected final String TEXT_256 = " results = pipelineModel_";
  protected final String TEXT_257 = ".transform(df_";
  protected final String TEXT_258 = ");" + NL;
  protected final String TEXT_259 = NL + "        org.apache.spark.ml.feature.StringIndexerModel sim = (org.apache.spark.ml.feature.StringIndexerModel) pipelineModel_";
  protected final String TEXT_260 = ".stages()[1];";
  protected final String TEXT_261 = NL + "            org.apache.spark.rdd.RDD<";
  protected final String TEXT_262 = "> rdd = results.map(" + NL + "                    new StringIndexerInverseFunction_";
  protected final String TEXT_263 = "(sim)," + NL + "                    scala.reflect.ClassManifestFactory.fromClass(";
  protected final String TEXT_264 = ".class));" + NL + "            rdd_";
  protected final String TEXT_265 = " = org.apache.spark.api.java.JavaRDD.fromRDD(rdd, rdd.org$apache$spark$rdd$RDD$$evidence$1);";
  protected final String TEXT_266 = NL + "            rdd_";
  protected final String TEXT_267 = " = results.map(new StringIndexerInverseFunction_";
  protected final String TEXT_268 = "(sim)," + NL + "                    org.apache.spark.sql.Encoders.bean(";
  protected final String TEXT_269 = ".class)).toJavaRDD();";
  protected final String TEXT_270 = NL + "        // TODO";
  protected final String TEXT_271 = NL + "}";
  protected final String TEXT_272 = NL + "org.apache.spark.api.java.JavaRDD<";
  protected final String TEXT_273 = "> rdd_";
  protected final String TEXT_274 = ";" + NL + "{";
  protected final String TEXT_275 = NL + "            java.net.URI currentURI_";
  protected final String TEXT_276 = "_config = FileSystem.getDefaultUri(ctx.hadoopConfiguration());" + NL + "            FileSystem.setDefaultUri(ctx.hadoopConfiguration(), new java.net.URI(";
  protected final String TEXT_277 = "));" + NL + "            fs = FileSystem.get(ctx.hadoopConfiguration());";
  protected final String TEXT_278 = NL + "        com.esotericsoftware.kryo.Kryo kryo_";
  protected final String TEXT_279 = " = new com.esotericsoftware.kryo.Kryo();";
  protected final String TEXT_280 = NL + "            org.apache.hadoop.fs.FSDataInputStream fsdis_";
  protected final String TEXT_281 = " = fs.open(new org.apache.hadoop.fs.Path(";
  protected final String TEXT_282 = " + \"/model/\"));" + NL + "            byte[] ba_";
  protected final String TEXT_283 = " = org.apache.commons.io.IOUtils.toByteArray(fsdis_";
  protected final String TEXT_284 = " );" + NL + "            java.io.ByteArrayInputStream bais_";
  protected final String TEXT_285 = " = new java.io.ByteArrayInputStream(ba_";
  protected final String TEXT_286 = ");" + NL + "            com.esotericsoftware.kryo.io.Input featuresInput_";
  protected final String TEXT_287 = " = new com.esotericsoftware.kryo.io.Input(bais_";
  protected final String TEXT_288 = ");";
  protected final String TEXT_289 = NL + "            com.esotericsoftware.kryo.io.Input featuresInput_";
  protected final String TEXT_290 = " = new com.esotericsoftware.kryo.io.Input(fs.open(new org.apache.hadoop.fs.Path(";
  protected final String TEXT_291 = " + \"/model/\")));";
  protected final String TEXT_292 = NL + "        TalendPipelineModelSerializer talendPipelineModelSerializer_";
  protected final String TEXT_293 = " = new TalendPipelineModelSerializer();" + NL + "        TalendPipelineModel featuresTalendPipelineModel_";
  protected final String TEXT_294 = " = kryo_";
  protected final String TEXT_295 = ".readObject(featuresInput_";
  protected final String TEXT_296 = ", TalendPipelineModel.class, talendPipelineModelSerializer_";
  protected final String TEXT_297 = ");" + NL + "" + NL + "        java.util.Map<String, String> featuresParamsMap_";
  protected final String TEXT_298 = " = featuresTalendPipelineModel_";
  protected final String TEXT_299 = ".getParams();" + NL + "" + NL + "        org.apache.spark.ml.PipelineModel pipelineModel_";
  protected final String TEXT_300 = " = featuresTalendPipelineModel_";
  protected final String TEXT_301 = ".getPipelineModel();";
  protected final String TEXT_302 = NL + "            FileSystem.setDefaultUri(ctx.hadoopConfiguration(), currentURI_";
  protected final String TEXT_303 = "_config);" + NL + "            fs = FileSystem.get(ctx.hadoopConfiguration());";
  protected final String TEXT_304 = NL + "        org.apache.spark.ml.PipelineModel pipelineModel_";
  protected final String TEXT_305 = ";" + NL + "        {" + NL + "            Object model = globalMap.get(\"";
  protected final String TEXT_306 = "_MODEL\");" + NL + "            if (model instanceof org.apache.spark.ml.PipelineModel) {" + NL + "                pipelineModel_";
  protected final String TEXT_307 = " = (org.apache.spark.ml.PipelineModel) model;" + NL + "            } else {" + NL + "                throw new Exception(\"Unsupported model type: \" + model.getClass());" + NL + "            }" + NL + "        }";
  protected final String TEXT_308 = NL + NL + "    // Convert incoming RDD to DataFrame" + NL + "    org.apache.spark.sql.SQLContext sqlContext_";
  protected final String TEXT_309 = " = new org.apache.spark.sql.SQLContext(ctx);";
  protected final String TEXT_310 = NL + "    ";
  protected final String TEXT_311 = " df_";
  protected final String TEXT_312 = " = sqlContext_";
  protected final String TEXT_313 = ".createDataFrame(rdd_";
  protected final String TEXT_314 = ", ";
  protected final String TEXT_315 = ".class);";
  protected final String TEXT_316 = NL + "    ";
  protected final String TEXT_317 = " results = pipelineModel_";
  protected final String TEXT_318 = ".transform(df_";
  protected final String TEXT_319 = ");" + NL;
  protected final String TEXT_320 = NL + "        org.apache.spark.ml.feature.StringIndexerModel sim = (org.apache.spark.ml.feature.StringIndexerModel) pipelineModel_";
  protected final String TEXT_321 = ".stages()[1];";
  protected final String TEXT_322 = NL + "            org.apache.spark.rdd.RDD<";
  protected final String TEXT_323 = "> rdd = results.map(" + NL + "                    new StringIndexerInverseFunction_";
  protected final String TEXT_324 = "(sim)," + NL + "                    scala.reflect.ClassManifestFactory.fromClass(";
  protected final String TEXT_325 = ".class));" + NL + "            rdd_";
  protected final String TEXT_326 = " = org.apache.spark.api.java.JavaRDD.fromRDD(rdd, rdd.org$apache$spark$rdd$RDD$$evidence$1);";
  protected final String TEXT_327 = NL + "            rdd_";
  protected final String TEXT_328 = " = results.map(new StringIndexerInverseFunction_";
  protected final String TEXT_329 = "(sim)," + NL + "                    org.apache.spark.sql.Encoders.bean(";
  protected final String TEXT_330 = ".class)).toJavaRDD();";
  protected final String TEXT_331 = NL + "        // TODO";
  protected final String TEXT_332 = NL + "}";
  protected final String TEXT_333 = NL + "org.apache.spark.api.java.JavaRDD<";
  protected final String TEXT_334 = "> rdd_";
  protected final String TEXT_335 = ";" + NL + "{";
  protected final String TEXT_336 = NL + "            java.net.URI currentURI_";
  protected final String TEXT_337 = "_config = FileSystem.getDefaultUri(ctx.hadoopConfiguration());" + NL + "            FileSystem.setDefaultUri(ctx.hadoopConfiguration(), new java.net.URI(";
  protected final String TEXT_338 = "));" + NL + "            fs = FileSystem.get(ctx.hadoopConfiguration());";
  protected final String TEXT_339 = NL + "        com.esotericsoftware.kryo.Kryo kryo_";
  protected final String TEXT_340 = " = new com.esotericsoftware.kryo.Kryo();";
  protected final String TEXT_341 = NL + "            org.apache.hadoop.fs.FSDataInputStream fsdis_";
  protected final String TEXT_342 = " = fs.open(new org.apache.hadoop.fs.Path(";
  protected final String TEXT_343 = " + \"/model/\"));" + NL + "            byte[] ba_";
  protected final String TEXT_344 = " = org.apache.commons.io.IOUtils.toByteArray(fsdis_";
  protected final String TEXT_345 = " );" + NL + "            java.io.ByteArrayInputStream bais_";
  protected final String TEXT_346 = " = new java.io.ByteArrayInputStream(ba_";
  protected final String TEXT_347 = ");" + NL + "            com.esotericsoftware.kryo.io.Input featuresInput_";
  protected final String TEXT_348 = " = new com.esotericsoftware.kryo.io.Input(bais_";
  protected final String TEXT_349 = ");";
  protected final String TEXT_350 = NL + "            com.esotericsoftware.kryo.io.Input featuresInput_";
  protected final String TEXT_351 = " = new com.esotericsoftware.kryo.io.Input(fs.open(new org.apache.hadoop.fs.Path(";
  protected final String TEXT_352 = " + \"/model/\")));";
  protected final String TEXT_353 = NL + "        TalendPipelineModelSerializer talendPipelineModelSerializer_";
  protected final String TEXT_354 = " = new TalendPipelineModelSerializer();" + NL + "        TalendPipelineModel featuresTalendPipelineModel_";
  protected final String TEXT_355 = " = kryo_";
  protected final String TEXT_356 = ".readObject(featuresInput_";
  protected final String TEXT_357 = ", TalendPipelineModel.class, talendPipelineModelSerializer_";
  protected final String TEXT_358 = ");" + NL + "" + NL + "        java.util.Map<String, String> featuresParamsMap_";
  protected final String TEXT_359 = " = featuresTalendPipelineModel_";
  protected final String TEXT_360 = ".getParams();" + NL + "" + NL + "        org.apache.spark.ml.PipelineModel pipelineModel_";
  protected final String TEXT_361 = " = featuresTalendPipelineModel_";
  protected final String TEXT_362 = ".getPipelineModel();";
  protected final String TEXT_363 = NL + "            FileSystem.setDefaultUri(ctx.hadoopConfiguration(), currentURI_";
  protected final String TEXT_364 = "_config);" + NL + "            fs = FileSystem.get(ctx.hadoopConfiguration());";
  protected final String TEXT_365 = NL + "        org.apache.spark.ml.PipelineModel pipelineModel_";
  protected final String TEXT_366 = ";" + NL + "        {" + NL + "            Object model = globalMap.get(\"";
  protected final String TEXT_367 = "_MODEL\");" + NL + "            if (model instanceof org.apache.spark.ml.PipelineModel) {" + NL + "                pipelineModel_";
  protected final String TEXT_368 = " = (org.apache.spark.ml.PipelineModel) model;" + NL + "            } else {" + NL + "                throw new Exception(\"Unsupported model type: \" + model.getClass());" + NL + "            }" + NL + "        }";
  protected final String TEXT_369 = NL + NL + "    // Convert incoming RDD to DataFrame" + NL + "    org.apache.spark.sql.SQLContext sqlContext_";
  protected final String TEXT_370 = " = new org.apache.spark.sql.SQLContext(ctx);";
  protected final String TEXT_371 = NL + "    ";
  protected final String TEXT_372 = " df_";
  protected final String TEXT_373 = " = sqlContext_";
  protected final String TEXT_374 = ".createDataFrame(rdd_";
  protected final String TEXT_375 = ", ";
  protected final String TEXT_376 = ".class);";
  protected final String TEXT_377 = NL + "    ";
  protected final String TEXT_378 = " results = pipelineModel_";
  protected final String TEXT_379 = ".transform(df_";
  protected final String TEXT_380 = ");" + NL;
  protected final String TEXT_381 = NL + "        org.apache.spark.ml.feature.StringIndexerModel sim = (org.apache.spark.ml.feature.StringIndexerModel) pipelineModel_";
  protected final String TEXT_382 = ".stages()[1];";
  protected final String TEXT_383 = NL + "            org.apache.spark.rdd.RDD<";
  protected final String TEXT_384 = "> rdd = results.map(" + NL + "                    new StringIndexerInverseFunction_";
  protected final String TEXT_385 = "(sim)," + NL + "                    scala.reflect.ClassManifestFactory.fromClass(";
  protected final String TEXT_386 = ".class));" + NL + "            rdd_";
  protected final String TEXT_387 = " = org.apache.spark.api.java.JavaRDD.fromRDD(rdd, rdd.org$apache$spark$rdd$RDD$$evidence$1);";
  protected final String TEXT_388 = NL + "            rdd_";
  protected final String TEXT_389 = " = results.map(new StringIndexerInverseFunction_";
  protected final String TEXT_390 = "(sim)," + NL + "                    org.apache.spark.sql.Encoders.bean(";
  protected final String TEXT_391 = ".class)).toJavaRDD();";
  protected final String TEXT_392 = NL + "        // TODO";
  protected final String TEXT_393 = NL + "}";
  protected final String TEXT_394 = NL;
  protected final String TEXT_395 = NL + "        java.net.URI currentURI_";
  protected final String TEXT_396 = "_config = FileSystem.getDefaultUri(ctx.hadoopConfiguration());" + NL + "        FileSystem.setDefaultUri(ctx.hadoopConfiguration(), new java.net.URI(";
  protected final String TEXT_397 = "));" + NL + "        fs = FileSystem.get(ctx.hadoopConfiguration());";
  protected final String TEXT_398 = NL + "    com.esotericsoftware.kryo.Kryo kryo_";
  protected final String TEXT_399 = " = new com.esotericsoftware.kryo.Kryo();" + NL + "    com.esotericsoftware.kryo.io.Input featuresInput_";
  protected final String TEXT_400 = " = new com.esotericsoftware.kryo.io.Input(fs.open(new org.apache.hadoop.fs.Path(";
  protected final String TEXT_401 = " + \"/features\")));" + NL + "    TalendPipelineModel featuresTalendPipelineModel_";
  protected final String TEXT_402 = " = kryo_";
  protected final String TEXT_403 = ".readObject(featuresInput_";
  protected final String TEXT_404 = ", TalendPipelineModel.class, new TalendPipelineModelSerializer());" + NL + "" + NL + "    org.apache.spark.mllib.classification.SVMModel currentModel_";
  protected final String TEXT_405 = " = org.apache.spark.mllib.classification.SVMModel.load(ctx.sc(), ";
  protected final String TEXT_406 = " + \"/model\");";
  protected final String TEXT_407 = NL + "        FileSystem.setDefaultUri(ctx.hadoopConfiguration(), currentURI_";
  protected final String TEXT_408 = "_config);" + NL + "        fs = FileSystem.get(ctx.hadoopConfiguration());";
  protected final String TEXT_409 = NL + "    TalendPipelineModel featuresTalendPipelineModel_";
  protected final String TEXT_410 = " = (TalendPipelineModel)globalMap.get(\"";
  protected final String TEXT_411 = "_PIPELINE\");" + NL + "" + NL + "    Object temporaryModel_";
  protected final String TEXT_412 = " = globalMap.get(\"";
  protected final String TEXT_413 = "_MODEL\");" + NL + "    if (temporaryModel_";
  protected final String TEXT_414 = " == null) {" + NL + "        throw new RuntimeException(\"The selected model does not exist\");" + NL + "    }else if (!(temporaryModel_";
  protected final String TEXT_415 = " instanceof org.apache.spark.mllib.classification.SVMModel)) {" + NL + "        throw new RuntimeException(\"The selected model is of type \" + temporaryModel_";
  protected final String TEXT_416 = ".getClass() + \" is should be of type org.apache.spark.mllib.classification.SVMModel\");" + NL + "    }" + NL + "    org.apache.spark.mllib.classification.SVMModel currentModel_";
  protected final String TEXT_417 = " = (org.apache.spark.mllib.classification.SVMModel) temporaryModel_";
  protected final String TEXT_418 = ";" + NL;
  protected final String TEXT_419 = NL + "java.util.Map<String, String> featuresParamsMap_";
  protected final String TEXT_420 = " = featuresTalendPipelineModel_";
  protected final String TEXT_421 = ".getParams();" + NL + "org.apache.spark.ml.PipelineModel featuresTransformationsModel_";
  protected final String TEXT_422 = " = featuresTalendPipelineModel_";
  protected final String TEXT_423 = ".getPipelineModel();" + NL + "String vectorName_";
  protected final String TEXT_424 = " = featuresParamsMap_";
  protected final String TEXT_425 = ".get(\"VECTOR_NAME\");" + NL + "" + NL + "// Pipeline" + NL + "org.apache.spark.sql.SQLContext sqlContext_";
  protected final String TEXT_426 = " = new org.apache.spark.sql.SQLContext(ctx);";
  protected final String TEXT_427 = NL;
  protected final String TEXT_428 = " inputDataFrame";
  protected final String TEXT_429 = " = sqlContext_";
  protected final String TEXT_430 = ".createDataFrame(rdd_";
  protected final String TEXT_431 = ", ";
  protected final String TEXT_432 = ".class);";
  protected final String TEXT_433 = NL;
  protected final String TEXT_434 = " transformedInputDataFrame_";
  protected final String TEXT_435 = " = featuresTransformationsModel_";
  protected final String TEXT_436 = ".transform(inputDataFrame";
  protected final String TEXT_437 = ");" + NL + "" + NL + "// Model" + NL + "org.apache.spark.api.java.JavaRDD<";
  protected final String TEXT_438 = "> temporaryrdd_";
  protected final String TEXT_439 = " = transformedInputDataFrame_";
  protected final String TEXT_440 = ".toJavaRDD().map(new GetEncodedStruct_";
  protected final String TEXT_441 = "(vectorName_";
  protected final String TEXT_442 = "));" + NL + "org.apache.spark.api.java.JavaRDD<";
  protected final String TEXT_443 = "> rdd_";
  protected final String TEXT_444 = " = temporaryrdd_";
  protected final String TEXT_445 = ".map(new GetPrediction_";
  protected final String TEXT_446 = "(currentModel_";
  protected final String TEXT_447 = "));" + NL;
  protected final String TEXT_448 = NL;
  protected final String TEXT_449 = NL + "        java.net.URI currentURI_";
  protected final String TEXT_450 = "_config = FileSystem.getDefaultUri(ctx.hadoopConfiguration());" + NL + "        FileSystem.setDefaultUri(ctx.hadoopConfiguration(), new java.net.URI(";
  protected final String TEXT_451 = "));" + NL + "        fs = FileSystem.get(ctx.hadoopConfiguration());";
  protected final String TEXT_452 = NL + "    com.esotericsoftware.kryo.Kryo kryo_";
  protected final String TEXT_453 = " = new com.esotericsoftware.kryo.Kryo();" + NL + "    com.esotericsoftware.kryo.io.Input featuresInput_";
  protected final String TEXT_454 = " = new com.esotericsoftware.kryo.io.Input(fs.open(new org.apache.hadoop.fs.Path(";
  protected final String TEXT_455 = " + \"/features\")));" + NL + "    TalendPipelineModel featuresTalendPipelineModel_";
  protected final String TEXT_456 = " = kryo_";
  protected final String TEXT_457 = ".readObject(featuresInput_";
  protected final String TEXT_458 = ", TalendPipelineModel.class, new TalendPipelineModelSerializer());" + NL + "" + NL + "    org.apache.spark.mllib.clustering.KMeansModel currentModel_";
  protected final String TEXT_459 = " = org.apache.spark.mllib.clustering.KMeansModel.load(ctx.sc(), ";
  protected final String TEXT_460 = " + \"/model\");";
  protected final String TEXT_461 = NL + "        FileSystem.setDefaultUri(ctx.hadoopConfiguration(), currentURI_";
  protected final String TEXT_462 = "_config);" + NL + "        fs = FileSystem.get(ctx.hadoopConfiguration());";
  protected final String TEXT_463 = NL + "    TalendPipelineModel featuresTalendPipelineModel_";
  protected final String TEXT_464 = " = (TalendPipelineModel)globalMap.get(\"";
  protected final String TEXT_465 = "_PIPELINE\");" + NL + "" + NL + "    Object temporaryModel_";
  protected final String TEXT_466 = " = globalMap.get(\"";
  protected final String TEXT_467 = "_MODEL\");" + NL + "    if (temporaryModel_";
  protected final String TEXT_468 = " == null) {" + NL + "        throw new RuntimeException(\"The selected model does not exist\");" + NL + "    }else if (!(temporaryModel_";
  protected final String TEXT_469 = " instanceof org.apache.spark.mllib.clustering.KMeansModel)) {" + NL + "        throw new RuntimeException(\"The selected model is of type \" + temporaryModel_";
  protected final String TEXT_470 = ".getClass() + \" is should be of type org.apache.spark.mllib.clustering.KMeansModel\");" + NL + "    }" + NL + "    org.apache.spark.mllib.clustering.KMeansModel currentModel_";
  protected final String TEXT_471 = " = (org.apache.spark.mllib.clustering.KMeansModel) temporaryModel_";
  protected final String TEXT_472 = ";" + NL;
  protected final String TEXT_473 = NL + "java.util.Map<String, String> featuresParamsMap_";
  protected final String TEXT_474 = " = featuresTalendPipelineModel_";
  protected final String TEXT_475 = ".getParams();" + NL + "org.apache.spark.ml.PipelineModel featuresTransformationsModel_";
  protected final String TEXT_476 = " = featuresTalendPipelineModel_";
  protected final String TEXT_477 = ".getPipelineModel();" + NL + "String vectorName_";
  protected final String TEXT_478 = " = featuresParamsMap_";
  protected final String TEXT_479 = ".get(\"VECTOR_NAME\");" + NL + "" + NL + "// Pipeline" + NL + "org.apache.spark.sql.SQLContext sqlContext_";
  protected final String TEXT_480 = " = new org.apache.spark.sql.SQLContext(ctx);";
  protected final String TEXT_481 = NL;
  protected final String TEXT_482 = " inputDataFrame";
  protected final String TEXT_483 = " = sqlContext_";
  protected final String TEXT_484 = ".createDataFrame(rdd_";
  protected final String TEXT_485 = ", ";
  protected final String TEXT_486 = ".class);";
  protected final String TEXT_487 = NL;
  protected final String TEXT_488 = " transformedInputDataFrame_";
  protected final String TEXT_489 = " = featuresTransformationsModel_";
  protected final String TEXT_490 = ".transform(inputDataFrame";
  protected final String TEXT_491 = ");" + NL + "" + NL + "// Model" + NL + "org.apache.spark.api.java.JavaRDD<";
  protected final String TEXT_492 = "> temporaryrdd_";
  protected final String TEXT_493 = " = transformedInputDataFrame_";
  protected final String TEXT_494 = ".toJavaRDD().map(new GetEncodedStruct_";
  protected final String TEXT_495 = "(vectorName_";
  protected final String TEXT_496 = "));" + NL + "org.apache.spark.api.java.JavaRDD<";
  protected final String TEXT_497 = "> rdd_";
  protected final String TEXT_498 = " = temporaryrdd_";
  protected final String TEXT_499 = ".map(new GetPrediction_";
  protected final String TEXT_500 = "(currentModel_";
  protected final String TEXT_501 = "));" + NL;

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    
//Parse the inputs to this javajet generator.
final BigDataCodeGeneratorArgument codeGenArgument = (BigDataCodeGeneratorArgument) argument;
final INode node = (INode) codeGenArgument.getArgument();
final String cid = node.getUniqueName();

final String dataframeClass = org.talend.hadoop.distribution.ESparkVersion.SPARK_2_0.compareTo(codeGenArgument.getSparkVersion()) > 0
    ? "org.apache.spark.sql.DataFrame"
    : "org.apache.spark.sql.Dataset<org.apache.spark.sql.Row>";

    
final String modelType = ElementParameterParser.getValue(node, "__MODEL_TYPE__");
final String sparkVersion=ElementParameterParser.getValue((INode) ((BigDataCodeGeneratorArgument) argument).getArgument(), "__NVB_VERSION__");

// NAIVEBAYES sparkconfig
if("NAIVEBAYES".equals(modelType)){
    if(sparkVersion.equals("SPARK_VERSION_1.3")){

    
TSqlRowUtil tSqlRowUtil = new TSqlRowUtil(node);
String validateError = tSqlRowUtil.validate(true, true);
if (validateError != null) {
    // Cause the job compilation to explicitly fail if there is a problem.
    return "throw new JobConfigurationError(\"" + validateError +"\");";
}

IConnection inConn = tSqlRowUtil.getIncomingConnections().get(0);
String pmmlModelPath = ElementParameterParser.getValue(node, "__PMML_MODEL_PATH__");

String inStructName = codeGenArgument.getRecordStructName(inConn);
String outStructName = codeGenArgument.getRecordStructName(tSqlRowUtil.getOutgoingConnection());
boolean useConfigurationComponent = "true".equals(ElementParameterParser.getValue(node, "__DEFINE_STORAGE_CONFIGURATION__"));
String ctx = ("SPARKSTREAMING".equals(node.getComponent().getType())) ? "ctx.sparkContext().sc()" : "ctx.sc()" ;


    stringBuffer.append(TEXT_1);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_2);
    
    if(useConfigurationComponent){//import from dfs


    stringBuffer.append(TEXT_3);
    stringBuffer.append(ctx);
    stringBuffer.append(TEXT_4);
    stringBuffer.append(pmmlModelPath);
    stringBuffer.append(TEXT_5);
    
    }else{//import from local

    stringBuffer.append(TEXT_6);
    stringBuffer.append(pmmlModelPath);
    stringBuffer.append(TEXT_7);
    
    }

    stringBuffer.append(TEXT_8);
    
if("SPARKSTREAMING".equals(node.getComponent().getType())
	&& !org.talend.designer.common.tmap.LookupUtil.isNodeInBatchMode(node)) {

    stringBuffer.append(TEXT_9);
    stringBuffer.append(outStructName);
    stringBuffer.append(TEXT_10);
    stringBuffer.append(tSqlRowUtil.getOutgoingConnection().getName());
    stringBuffer.append(TEXT_11);
    stringBuffer.append(inConn.getName());
    stringBuffer.append(TEXT_12);
    stringBuffer.append(inStructName);
    stringBuffer.append(TEXT_13);
    stringBuffer.append(outStructName);
    stringBuffer.append(TEXT_14);
    stringBuffer.append(outStructName);
    stringBuffer.append(TEXT_15);
    stringBuffer.append(inStructName);
    stringBuffer.append(TEXT_16);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_17);
    stringBuffer.append(inConn.getName());
    stringBuffer.append(TEXT_18);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_19);
    stringBuffer.append(inStructName);
    stringBuffer.append(TEXT_20);
    stringBuffer.append(inConn.getName());
    stringBuffer.append(TEXT_21);
    stringBuffer.append(inConn.getName());
    stringBuffer.append(TEXT_22);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_23);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_24);
    stringBuffer.append(outStructName);
    stringBuffer.append(TEXT_25);
    
}else{

    stringBuffer.append(TEXT_26);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_27);
    stringBuffer.append(ctx);
    stringBuffer.append(TEXT_28);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_29);
    stringBuffer.append(inConn.getName());
    stringBuffer.append(TEXT_30);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_31);
    stringBuffer.append(inConn.getName());
    stringBuffer.append(TEXT_32);
    stringBuffer.append(inStructName);
    stringBuffer.append(TEXT_33);
    stringBuffer.append(outStructName);
    stringBuffer.append(TEXT_34);
    stringBuffer.append(tSqlRowUtil.getOutgoingConnection().getName());
    stringBuffer.append(TEXT_35);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_36);
    stringBuffer.append(inConn.getName());
    stringBuffer.append(TEXT_37);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_38);
    stringBuffer.append(inConn.getName());
    stringBuffer.append(TEXT_39);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_40);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_41);
    stringBuffer.append(outStructName);
    stringBuffer.append(TEXT_42);
    
}

    
    }else{

    
boolean isLog4jEnabled = ("true").equals(ElementParameterParser.getValue(node.getProcess(), "__LOG4J_ACTIVATE__"));
String modelCid = ElementParameterParser.getValue(node, "__NVB_MODEL_LOCATION__");

    
List<IMetadataTable> metadatas = node.getMetadataList();
if ((metadatas == null) || (metadatas.size() == 0))
    return "" ;
IMetadataTable metadata = metadatas.get(0);
if (metadata == null)
    return "";
List<IMetadataColumn> columns = metadata.getListColumns();
if (columns == null)
    return "";
List<? extends IConnection> conns = node.getIncomingConnections();
if (conns == null || conns.size() == 0 || !conns.get(0).getLineStyle().hasConnectionCategory(IConnectionCategory.DATA))
    return "";
IConnection inConn = conns.get(0);
conns = node.getOutgoingConnections();
if (conns == null || conns.size() == 0 || !conns.get(0).getLineStyle().hasConnectionCategory(IConnectionCategory.DATA))
    return "";
IConnection outConn = conns.get(0);

String inRowStruct = codeGenArgument.getRecordStructName(inConn);
String inConnName = inConn.getName();

String outRowStruct = codeGenArgument.getRecordStructName(outConn);
String outConnName = outConn.getName();


// This is set to true if the output label is not double format.
boolean needsLabelIndexer = true;

Boolean savedOnDisk = ElementParameterParser.getBooleanValue(node, "__SAVE_ON_DISK__");
Boolean modelOnJob = ElementParameterParser.getBooleanValue(node, "__NVB_MODEL_COMPUTED__");


    stringBuffer.append(TEXT_43);
    stringBuffer.append(outRowStruct);
    stringBuffer.append(TEXT_44);
    stringBuffer.append(outConnName);
    stringBuffer.append(TEXT_45);
    
    if (savedOnDisk) {

        String modelPath = ElementParameterParser.getValue(node, "__HDFS_FOLDER__");
        boolean useConfigurationComponent = "true".equals(ElementParameterParser.getValue(node, "__DEFINE_STORAGE_CONFIGURATION__"));

        String uriPrefix = "\"\"";
        if(useConfigurationComponent) {
            uriPrefix = org.talend.designer.spark.generator.storage.SparkStorageUtils.getURIPrefix(node);
            modelPath = uriPrefix + " + " + modelPath;
        }

        if(!"\"\"".equals(uriPrefix)) {
            
    stringBuffer.append(TEXT_46);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_47);
    stringBuffer.append(uriPrefix);
    stringBuffer.append(TEXT_48);
    
        }


        
    stringBuffer.append(TEXT_49);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_50);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_51);
    stringBuffer.append(modelPath);
    stringBuffer.append(TEXT_52);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_53);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_54);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_55);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_56);
    stringBuffer.append(modelPath);
    stringBuffer.append(TEXT_57);
    
        if(!"\"\"".equals(uriPrefix)) {
            
    stringBuffer.append(TEXT_58);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_59);
    
        }
    } else if (modelOnJob) {

        //retrieve name of the external component
        String modelLocation = ElementParameterParser.getValue(node, "__NVB_MODEL_LOCATION__");
        
    stringBuffer.append(TEXT_60);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_61);
    stringBuffer.append(modelLocation);
    stringBuffer.append(TEXT_62);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_63);
    stringBuffer.append(modelLocation);
    stringBuffer.append(TEXT_64);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_65);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_66);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_67);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_68);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_69);
    
    }
    
    stringBuffer.append(TEXT_70);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_71);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_72);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_73);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_74);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_75);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_76);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_77);
    stringBuffer.append(TEXT_78);
    stringBuffer.append(dataframeClass);
    stringBuffer.append(TEXT_79);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_80);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_81);
    stringBuffer.append(inConnName);
    stringBuffer.append(TEXT_82);
    stringBuffer.append(inRowStruct);
    stringBuffer.append(TEXT_83);
    stringBuffer.append(TEXT_84);
    stringBuffer.append(dataframeClass);
    stringBuffer.append(TEXT_85);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_86);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_87);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_88);
    
    if (needsLabelIndexer) {
        
    stringBuffer.append(TEXT_89);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_90);
    
        if (org.talend.hadoop.distribution.ESparkVersion.SPARK_2_0.compareTo(codeGenArgument.getSparkVersion()) > 0) {//spark1
            
    stringBuffer.append(TEXT_91);
    stringBuffer.append(outRowStruct);
    stringBuffer.append(TEXT_92);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_93);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_94);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_95);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_96);
    stringBuffer.append(outRowStruct);
    stringBuffer.append(TEXT_97);
    stringBuffer.append(outConnName);
    stringBuffer.append(TEXT_98);
    
        } else {
            
    stringBuffer.append(TEXT_99);
    stringBuffer.append(outConnName);
    stringBuffer.append(TEXT_100);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_101);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_102);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_103);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_104);
    stringBuffer.append(outRowStruct);
    stringBuffer.append(TEXT_105);
    
        }
    } else {
       
    stringBuffer.append(TEXT_106);
    
    }
    
    stringBuffer.append(TEXT_107);
      }
} // LINEAR_REGRESSION sparkconfig
 else if("LINEAR_REGRESSION".equals(modelType)){

    
boolean isLog4jEnabled = ("true").equals(ElementParameterParser.getValue(node.getProcess(), "__LOG4J_ACTIVATE__"));

    
List<IMetadataTable> metadatas = node.getMetadataList();
if ((metadatas == null) || (metadatas.size() == 0))
    return "" ;
IMetadataTable metadata = metadatas.get(0);
if (metadata == null)
    return "";
List<IMetadataColumn> columns = metadata.getListColumns();
if (columns == null)
    return "";
List<? extends IConnection> conns = node.getIncomingConnections();
if (conns == null || conns.size() == 0 || !conns.get(0).getLineStyle().hasConnectionCategory(IConnectionCategory.DATA))
    return "";
IConnection inConn = conns.get(0);
conns = node.getOutgoingConnections();
if (conns == null || conns.size() == 0 || !conns.get(0).getLineStyle().hasConnectionCategory(IConnectionCategory.DATA))
    return "";
IConnection outConn = conns.get(0);

String inRowStruct = codeGenArgument.getRecordStructName(inConn);
String inConnName = inConn.getName();

String outRowStruct = codeGenArgument.getRecordStructName(outConn);
String outConnName = outConn.getName();

Boolean savedOnDisk = ElementParameterParser.getBooleanValue(node, "__SAVE_ON_DISK__");
String hdfsFolder = ElementParameterParser.getValue(node,"__HDFS_FOLDER__");
boolean useConfigurationComponent = "true".equals(ElementParameterParser.getValue(node, "__DEFINE_STORAGE_CONFIGURATION__"));

String uriPrefix = "\"\"";
if(useConfigurationComponent) {
    uriPrefix = org.talend.designer.spark.generator.storage.SparkStorageUtils.getURIPrefix(node);
    hdfsFolder = uriPrefix + " + " + hdfsFolder;
}


    stringBuffer.append(TEXT_108);
    stringBuffer.append(outRowStruct);
    stringBuffer.append(TEXT_109);
    stringBuffer.append(outConnName);
    stringBuffer.append(TEXT_110);
    
    // Get the pipeline model.
    if (savedOnDisk) {
        if(!"\"\"".equals(uriPrefix)) {
            
    stringBuffer.append(TEXT_111);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_112);
    stringBuffer.append(uriPrefix);
    stringBuffer.append(TEXT_113);
    
        }
        
    stringBuffer.append(TEXT_114);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_115);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_116);
    stringBuffer.append(hdfsFolder);
    stringBuffer.append(TEXT_117);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_118);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_119);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_120);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_121);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_122);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_123);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_124);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_125);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_126);
    
        if(!"\"\"".equals(uriPrefix)) {
            
    stringBuffer.append(TEXT_127);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_128);
    
        }
    } else {
        String modelCid = ElementParameterParser.getValue(node, "__LR_MODEL_LOCATION__");
        
    stringBuffer.append(TEXT_129);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_130);
    stringBuffer.append(modelCid);
    stringBuffer.append(TEXT_131);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_132);
    
    }
    
    stringBuffer.append(TEXT_133);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_134);
    stringBuffer.append(TEXT_135);
    stringBuffer.append(dataframeClass);
    stringBuffer.append(TEXT_136);
    stringBuffer.append(inConnName);
    stringBuffer.append(TEXT_137);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_138);
    stringBuffer.append(inConnName);
    stringBuffer.append(TEXT_139);
    stringBuffer.append(inRowStruct);
    stringBuffer.append(TEXT_140);
    stringBuffer.append(TEXT_141);
    stringBuffer.append(dataframeClass);
    stringBuffer.append(TEXT_142);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_143);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_144);
    stringBuffer.append(inConnName);
    stringBuffer.append(TEXT_145);
    stringBuffer.append(outConnName);
    stringBuffer.append(TEXT_146);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_147);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_148);
    stringBuffer.append(outRowStruct);
    stringBuffer.append(TEXT_149);
    

    
}// LOGISTIC_REGRESSION sparkconfig
 else if("LOGISTIC_REGRESSION".equals(modelType)){

    
String modelCid = ElementParameterParser.getValue(node, "__LOR_MODEL_LOCATION__");

    
boolean isLog4jEnabled = ("true").equals(ElementParameterParser.getValue(node.getProcess(), "__LOG4J_ACTIVATE__"));

    
List<IMetadataTable> metadatas = node.getMetadataList();
if ((metadatas == null) || (metadatas.size() == 0))
    return "" ;
IMetadataTable metadata = metadatas.get(0);
if (metadata == null)
    return "";
List<IMetadataColumn> columns = metadata.getListColumns();
if (columns == null)
    return "";
List<? extends IConnection> conns = node.getIncomingConnections();
if (conns == null || conns.size() == 0 || !conns.get(0).getLineStyle().hasConnectionCategory(IConnectionCategory.DATA))
    return "";
IConnection inConn = conns.get(0);
conns = node.getOutgoingConnections();
if (conns == null || conns.size() == 0 || !conns.get(0).getLineStyle().hasConnectionCategory(IConnectionCategory.DATA))
    return "";
IConnection outConn = conns.get(0);

String inRowStruct = codeGenArgument.getRecordStructName(inConn);
String inConnName = inConn.getName();

String outRowStruct = codeGenArgument.getRecordStructName(outConn);
String outConnName = outConn.getName();


// This is set to true if the output label is not double format.
// TODO: check if the labelColumn is double and provide a unique labelColumn.
boolean needsLabelIndexer = true;
String labelColumn = "label";

Boolean savedOnDisk = ElementParameterParser.getBooleanValue(node, "__SAVE_ON_DISK__");
String hdfsFolder = ElementParameterParser.getValue(node,"__HDFS_FOLDER__");
boolean useConfigurationComponent = "true".equals(ElementParameterParser.getValue(node, "__DEFINE_STORAGE_CONFIGURATION__"));
String uriPrefix = "\"\"";
if(useConfigurationComponent) {
    uriPrefix = org.talend.designer.spark.generator.storage.SparkStorageUtils.getURIPrefix(node);
    hdfsFolder = uriPrefix + " + " + hdfsFolder;
}

final List<? extends INode> sparkConfigs = node.getProcess().getNodesOfType("tSparkConfiguration");
INode sparkConfig = null;
if(sparkConfigs != null && sparkConfigs.size() > 0) {
    sparkConfig = sparkConfigs.get(0);
}

String sparkDistribution = ElementParameterParser.getValue(sparkConfig, "__DISTRIBUTION__");
String sparkDistribVersion = ElementParameterParser.getValue(sparkConfig, "__SPARK_VERSION__");

org.talend.hadoop.distribution.component.HadoopComponent hadoopComponent = null;

try {
    hadoopComponent = (org.talend.hadoop.distribution.component.HadoopComponent) org.talend.hadoop.distribution.DistributionFactory.buildDistribution(sparkDistribution, sparkDistribVersion);
} catch (java.lang.Exception e) {
    e.printStackTrace();
    return "";
}

boolean serializeModelInJob = false;
boolean useLocalMode = "true".equals(ElementParameterParser.getValue(sparkConfig, "__SPARK_LOCAL_MODE__"));
if(!useLocalMode){
    boolean isMaprDistribution = hadoopComponent instanceof org.talend.hadoop.distribution.constants.mapr.IMapRDistribution ? true : false;
    if(isMaprDistribution){
        serializeModelInJob = true;
    }
}


    stringBuffer.append(TEXT_150);
    stringBuffer.append(outRowStruct);
    stringBuffer.append(TEXT_151);
    stringBuffer.append(outConnName);
    stringBuffer.append(TEXT_152);
    
    // Get the pipeline model.
    if (savedOnDisk) {

        if(!"\"\"".equals(uriPrefix)) {
            
    stringBuffer.append(TEXT_153);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_154);
    stringBuffer.append(uriPrefix);
    stringBuffer.append(TEXT_155);
    
        }
        
    stringBuffer.append(TEXT_156);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_157);
    
        // Mapr workaround (EOF exception)
        // Kryo doesn't seem to be able to read from MaprFS
        // We read the model contents to a byte array then pass it to Kryo
        if(serializeModelInJob){
            
    stringBuffer.append(TEXT_158);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_159);
    stringBuffer.append(hdfsFolder);
    stringBuffer.append(TEXT_160);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_161);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_162);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_163);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_164);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_165);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_166);
    
        } else {
            
    stringBuffer.append(TEXT_167);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_168);
    stringBuffer.append(hdfsFolder);
    stringBuffer.append(TEXT_169);
    
        }
        
    stringBuffer.append(TEXT_170);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_171);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_172);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_173);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_174);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_175);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_176);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_177);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_178);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_179);
    
        if(!"\"\"".equals(uriPrefix)) {
            
    stringBuffer.append(TEXT_180);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_181);
    
        }

    } else {
        
    stringBuffer.append(TEXT_182);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_183);
    stringBuffer.append(modelCid);
    stringBuffer.append(TEXT_184);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_185);
    
    }
    
    stringBuffer.append(TEXT_186);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_187);
    stringBuffer.append(TEXT_188);
    stringBuffer.append(dataframeClass);
    stringBuffer.append(TEXT_189);
    stringBuffer.append(inConnName);
    stringBuffer.append(TEXT_190);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_191);
    stringBuffer.append(inConnName);
    stringBuffer.append(TEXT_192);
    stringBuffer.append(inRowStruct);
    stringBuffer.append(TEXT_193);
    stringBuffer.append(TEXT_194);
    stringBuffer.append(dataframeClass);
    stringBuffer.append(TEXT_195);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_196);
    stringBuffer.append(inConnName);
    stringBuffer.append(TEXT_197);
    

    if (needsLabelIndexer) {
        
    stringBuffer.append(TEXT_198);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_199);
    
        if (org.talend.hadoop.distribution.ESparkVersion.SPARK_2_0.compareTo(codeGenArgument.getSparkVersion()) > 0) {
            
    stringBuffer.append(TEXT_200);
    stringBuffer.append(outRowStruct);
    stringBuffer.append(TEXT_201);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_202);
    stringBuffer.append(outRowStruct);
    stringBuffer.append(TEXT_203);
    stringBuffer.append(outConnName);
    stringBuffer.append(TEXT_204);
    
        } else {
            
    stringBuffer.append(TEXT_205);
    stringBuffer.append(outConnName);
    stringBuffer.append(TEXT_206);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_207);
    stringBuffer.append(outRowStruct);
    stringBuffer.append(TEXT_208);
    
        }
    } else {
       
    stringBuffer.append(TEXT_209);
    
    }
    
    stringBuffer.append(TEXT_210);
    

    
}// RANDOM_FOREST sparkconfig
 else if("RANDOM_FOREST".equals(modelType)){

    
String modelCid = ElementParameterParser.getValue(node, "__RF_MODEL_LOCATION__");

    
boolean isLog4jEnabled = ("true").equals(ElementParameterParser.getValue(node.getProcess(), "__LOG4J_ACTIVATE__"));

    
List<IMetadataTable> metadatas = node.getMetadataList();
if ((metadatas == null) || (metadatas.size() == 0))
    return "" ;
IMetadataTable metadata = metadatas.get(0);
if (metadata == null)
    return "";
List<IMetadataColumn> columns = metadata.getListColumns();
if (columns == null)
    return "";
List<? extends IConnection> conns = node.getIncomingConnections();
if (conns == null || conns.size() == 0 || !conns.get(0).getLineStyle().hasConnectionCategory(IConnectionCategory.DATA))
    return "";
IConnection inConn = conns.get(0);
conns = node.getOutgoingConnections();
if (conns == null || conns.size() == 0 || !conns.get(0).getLineStyle().hasConnectionCategory(IConnectionCategory.DATA))
    return "";
IConnection outConn = conns.get(0);

String inRowStruct = codeGenArgument.getRecordStructName(inConn);
String inConnName = inConn.getName();

String outRowStruct = codeGenArgument.getRecordStructName(outConn);
String outConnName = outConn.getName();


// This is set to true if the output label is not double format.
// TODO: check if the labelColumn is double and provide a unique labelColumn.
boolean needsLabelIndexer = true;
String labelColumn = "label";

Boolean savedOnDisk = ElementParameterParser.getBooleanValue(node, "__SAVE_ON_DISK__");
String hdfsFolder = ElementParameterParser.getValue(node,"__HDFS_FOLDER__");
boolean useConfigurationComponent = "true".equals(ElementParameterParser.getValue(node, "__DEFINE_STORAGE_CONFIGURATION__"));
String uriPrefix = "\"\"";
if(useConfigurationComponent) {
    uriPrefix = org.talend.designer.spark.generator.storage.SparkStorageUtils.getURIPrefix(node);
    hdfsFolder = uriPrefix + " + " + hdfsFolder;
}

final List<? extends INode> sparkConfigs = node.getProcess().getNodesOfType("tSparkConfiguration");
INode sparkConfig = null;
if(sparkConfigs != null && sparkConfigs.size() > 0) {
    sparkConfig = sparkConfigs.get(0);
}

String sparkDistribution = ElementParameterParser.getValue(sparkConfig, "__DISTRIBUTION__");
String sparkDistribVersion = ElementParameterParser.getValue(sparkConfig, "__SPARK_VERSION__");

org.talend.hadoop.distribution.component.HadoopComponent hadoopComponent = null;

try {
    hadoopComponent = (org.talend.hadoop.distribution.component.HadoopComponent) org.talend.hadoop.distribution.DistributionFactory.buildDistribution(sparkDistribution, sparkDistribVersion);
} catch (java.lang.Exception e) {
    e.printStackTrace();
    return "";
}

boolean serializeModelInJob = false;
boolean useLocalMode = "true".equals(ElementParameterParser.getValue(sparkConfig, "__SPARK_LOCAL_MODE__"));
if(!useLocalMode){
    boolean isMaprDistribution = hadoopComponent instanceof org.talend.hadoop.distribution.constants.mapr.IMapRDistribution ? true : false;
    if(isMaprDistribution){
        serializeModelInJob = true;
    }
}


    stringBuffer.append(TEXT_211);
    stringBuffer.append(outRowStruct);
    stringBuffer.append(TEXT_212);
    stringBuffer.append(outConnName);
    stringBuffer.append(TEXT_213);
    
    // Get the pipeline model.
    if (savedOnDisk) {

        if(!"\"\"".equals(uriPrefix)) {
            
    stringBuffer.append(TEXT_214);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_215);
    stringBuffer.append(uriPrefix);
    stringBuffer.append(TEXT_216);
    
        }
        
    stringBuffer.append(TEXT_217);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_218);
    
        // Mapr workaround (EOF exception)
        // Kryo doesn't seem to be able to read from MaprFS
        // We read the model contents to a byte array then pass it to Kryo
        if(serializeModelInJob){
            
    stringBuffer.append(TEXT_219);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_220);
    stringBuffer.append(hdfsFolder);
    stringBuffer.append(TEXT_221);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_222);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_223);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_224);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_225);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_226);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_227);
    
        } else {
            
    stringBuffer.append(TEXT_228);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_229);
    stringBuffer.append(hdfsFolder);
    stringBuffer.append(TEXT_230);
    
        }
        
    stringBuffer.append(TEXT_231);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_232);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_233);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_234);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_235);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_236);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_237);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_238);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_239);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_240);
    
        if(!"\"\"".equals(uriPrefix)) {
            
    stringBuffer.append(TEXT_241);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_242);
    
        }

    } else {
        
    stringBuffer.append(TEXT_243);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_244);
    stringBuffer.append(modelCid);
    stringBuffer.append(TEXT_245);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_246);
    
    }
    
    stringBuffer.append(TEXT_247);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_248);
    stringBuffer.append(TEXT_249);
    stringBuffer.append(dataframeClass);
    stringBuffer.append(TEXT_250);
    stringBuffer.append(inConnName);
    stringBuffer.append(TEXT_251);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_252);
    stringBuffer.append(inConnName);
    stringBuffer.append(TEXT_253);
    stringBuffer.append(inRowStruct);
    stringBuffer.append(TEXT_254);
    stringBuffer.append(TEXT_255);
    stringBuffer.append(dataframeClass);
    stringBuffer.append(TEXT_256);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_257);
    stringBuffer.append(inConnName);
    stringBuffer.append(TEXT_258);
    

    if (needsLabelIndexer) {
        
    stringBuffer.append(TEXT_259);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_260);
    
        if (org.talend.hadoop.distribution.ESparkVersion.SPARK_2_0.compareTo(codeGenArgument.getSparkVersion()) > 0) {
            
    stringBuffer.append(TEXT_261);
    stringBuffer.append(outRowStruct);
    stringBuffer.append(TEXT_262);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_263);
    stringBuffer.append(outRowStruct);
    stringBuffer.append(TEXT_264);
    stringBuffer.append(outConnName);
    stringBuffer.append(TEXT_265);
    
        } else {
            
    stringBuffer.append(TEXT_266);
    stringBuffer.append(outConnName);
    stringBuffer.append(TEXT_267);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_268);
    stringBuffer.append(outRowStruct);
    stringBuffer.append(TEXT_269);
    
        }
    } else {
       
    stringBuffer.append(TEXT_270);
    
    }
    
    stringBuffer.append(TEXT_271);
    

    
}// DECISION_TREE sparkconfig
 else if("DECISION_TREE".equals(modelType)){

    
String modelCid = ElementParameterParser.getValue(node, "__DT_MODEL_LOCATION__");

    
boolean isLog4jEnabled = ("true").equals(ElementParameterParser.getValue(node.getProcess(), "__LOG4J_ACTIVATE__"));

    
List<IMetadataTable> metadatas = node.getMetadataList();
if ((metadatas == null) || (metadatas.size() == 0))
    return "" ;
IMetadataTable metadata = metadatas.get(0);
if (metadata == null)
    return "";
List<IMetadataColumn> columns = metadata.getListColumns();
if (columns == null)
    return "";
List<? extends IConnection> conns = node.getIncomingConnections();
if (conns == null || conns.size() == 0 || !conns.get(0).getLineStyle().hasConnectionCategory(IConnectionCategory.DATA))
    return "";
IConnection inConn = conns.get(0);
conns = node.getOutgoingConnections();
if (conns == null || conns.size() == 0 || !conns.get(0).getLineStyle().hasConnectionCategory(IConnectionCategory.DATA))
    return "";
IConnection outConn = conns.get(0);

String inRowStruct = codeGenArgument.getRecordStructName(inConn);
String inConnName = inConn.getName();

String outRowStruct = codeGenArgument.getRecordStructName(outConn);
String outConnName = outConn.getName();


// This is set to true if the output label is not double format.
// TODO: check if the labelColumn is double and provide a unique labelColumn.
boolean needsLabelIndexer = true;
String labelColumn = "label";

Boolean savedOnDisk = ElementParameterParser.getBooleanValue(node, "__SAVE_ON_DISK__");
String hdfsFolder = ElementParameterParser.getValue(node,"__HDFS_FOLDER__");
boolean useConfigurationComponent = "true".equals(ElementParameterParser.getValue(node, "__DEFINE_STORAGE_CONFIGURATION__"));
String uriPrefix = "\"\"";
if(useConfigurationComponent) {
    uriPrefix = org.talend.designer.spark.generator.storage.SparkStorageUtils.getURIPrefix(node);
    hdfsFolder = uriPrefix + " + " + hdfsFolder;
}

final List<? extends INode> sparkConfigs = node.getProcess().getNodesOfType("tSparkConfiguration");
INode sparkConfig = null;
if(sparkConfigs != null && sparkConfigs.size() > 0) {
    sparkConfig = sparkConfigs.get(0);
}

String sparkDistribution = ElementParameterParser.getValue(sparkConfig, "__DISTRIBUTION__");
String sparkDistribVersion = ElementParameterParser.getValue(sparkConfig, "__SPARK_VERSION__");

org.talend.hadoop.distribution.component.HadoopComponent hadoopComponent = null;

try {
    hadoopComponent = (org.talend.hadoop.distribution.component.HadoopComponent) org.talend.hadoop.distribution.DistributionFactory.buildDistribution(sparkDistribution, sparkDistribVersion);
} catch (java.lang.Exception e) {
    e.printStackTrace();
    return "";
}

boolean serializeModelInJob = false;
boolean useLocalMode = "true".equals(ElementParameterParser.getValue(sparkConfig, "__SPARK_LOCAL_MODE__"));
if(!useLocalMode){
    boolean isMaprDistribution = hadoopComponent instanceof org.talend.hadoop.distribution.constants.mapr.IMapRDistribution ? true : false;
    if(isMaprDistribution){
        serializeModelInJob = true;
    }
}


    stringBuffer.append(TEXT_272);
    stringBuffer.append(outRowStruct);
    stringBuffer.append(TEXT_273);
    stringBuffer.append(outConnName);
    stringBuffer.append(TEXT_274);
    
    // Get the pipeline model.
    if (savedOnDisk) {

        if(!"\"\"".equals(uriPrefix)) {
            
    stringBuffer.append(TEXT_275);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_276);
    stringBuffer.append(uriPrefix);
    stringBuffer.append(TEXT_277);
    
        }
        
    stringBuffer.append(TEXT_278);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_279);
    
        // Mapr workaround (EOF exception)
        // Kryo doesn't seem to be able to read from MaprFS
        // We read the model contents to a byte array then pass it to Kryo
        if(serializeModelInJob){
            
    stringBuffer.append(TEXT_280);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_281);
    stringBuffer.append(hdfsFolder);
    stringBuffer.append(TEXT_282);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_283);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_284);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_285);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_286);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_287);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_288);
    
        } else {
            
    stringBuffer.append(TEXT_289);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_290);
    stringBuffer.append(hdfsFolder);
    stringBuffer.append(TEXT_291);
    
        }
        
    stringBuffer.append(TEXT_292);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_293);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_294);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_295);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_296);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_297);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_298);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_299);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_300);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_301);
    
        if(!"\"\"".equals(uriPrefix)) {
            
    stringBuffer.append(TEXT_302);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_303);
    
        }

    } else {
        
    stringBuffer.append(TEXT_304);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_305);
    stringBuffer.append(modelCid);
    stringBuffer.append(TEXT_306);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_307);
    
    }
    
    stringBuffer.append(TEXT_308);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_309);
    stringBuffer.append(TEXT_310);
    stringBuffer.append(dataframeClass);
    stringBuffer.append(TEXT_311);
    stringBuffer.append(inConnName);
    stringBuffer.append(TEXT_312);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_313);
    stringBuffer.append(inConnName);
    stringBuffer.append(TEXT_314);
    stringBuffer.append(inRowStruct);
    stringBuffer.append(TEXT_315);
    stringBuffer.append(TEXT_316);
    stringBuffer.append(dataframeClass);
    stringBuffer.append(TEXT_317);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_318);
    stringBuffer.append(inConnName);
    stringBuffer.append(TEXT_319);
    

    if (needsLabelIndexer) {
        
    stringBuffer.append(TEXT_320);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_321);
    
        if (org.talend.hadoop.distribution.ESparkVersion.SPARK_2_0.compareTo(codeGenArgument.getSparkVersion()) > 0) {
            
    stringBuffer.append(TEXT_322);
    stringBuffer.append(outRowStruct);
    stringBuffer.append(TEXT_323);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_324);
    stringBuffer.append(outRowStruct);
    stringBuffer.append(TEXT_325);
    stringBuffer.append(outConnName);
    stringBuffer.append(TEXT_326);
    
        } else {
            
    stringBuffer.append(TEXT_327);
    stringBuffer.append(outConnName);
    stringBuffer.append(TEXT_328);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_329);
    stringBuffer.append(outRowStruct);
    stringBuffer.append(TEXT_330);
    
        }
    } else {
       
    stringBuffer.append(TEXT_331);
    
    }
    
    stringBuffer.append(TEXT_332);
    

    
}// GRADIENT_BOOSTED sparkconfig
 else if("GRADIENT_BOOSTED".equals(modelType)){

    
String modelCid = ElementParameterParser.getValue(node, "__GB_MODEL_LOCATION__");

    
boolean isLog4jEnabled = ("true").equals(ElementParameterParser.getValue(node.getProcess(), "__LOG4J_ACTIVATE__"));

    
List<IMetadataTable> metadatas = node.getMetadataList();
if ((metadatas == null) || (metadatas.size() == 0))
    return "" ;
IMetadataTable metadata = metadatas.get(0);
if (metadata == null)
    return "";
List<IMetadataColumn> columns = metadata.getListColumns();
if (columns == null)
    return "";
List<? extends IConnection> conns = node.getIncomingConnections();
if (conns == null || conns.size() == 0 || !conns.get(0).getLineStyle().hasConnectionCategory(IConnectionCategory.DATA))
    return "";
IConnection inConn = conns.get(0);
conns = node.getOutgoingConnections();
if (conns == null || conns.size() == 0 || !conns.get(0).getLineStyle().hasConnectionCategory(IConnectionCategory.DATA))
    return "";
IConnection outConn = conns.get(0);

String inRowStruct = codeGenArgument.getRecordStructName(inConn);
String inConnName = inConn.getName();

String outRowStruct = codeGenArgument.getRecordStructName(outConn);
String outConnName = outConn.getName();


// This is set to true if the output label is not double format.
// TODO: check if the labelColumn is double and provide a unique labelColumn.
boolean needsLabelIndexer = true;
String labelColumn = "label";

Boolean savedOnDisk = ElementParameterParser.getBooleanValue(node, "__SAVE_ON_DISK__");
String hdfsFolder = ElementParameterParser.getValue(node,"__HDFS_FOLDER__");
boolean useConfigurationComponent = "true".equals(ElementParameterParser.getValue(node, "__DEFINE_STORAGE_CONFIGURATION__"));
String uriPrefix = "\"\"";
if(useConfigurationComponent) {
    uriPrefix = org.talend.designer.spark.generator.storage.SparkStorageUtils.getURIPrefix(node);
    hdfsFolder = uriPrefix + " + " + hdfsFolder;
}

final List<? extends INode> sparkConfigs = node.getProcess().getNodesOfType("tSparkConfiguration");
INode sparkConfig = null;
if(sparkConfigs != null && sparkConfigs.size() > 0) {
    sparkConfig = sparkConfigs.get(0);
}

String sparkDistribution = ElementParameterParser.getValue(sparkConfig, "__DISTRIBUTION__");
String sparkDistribVersion = ElementParameterParser.getValue(sparkConfig, "__SPARK_VERSION__");

org.talend.hadoop.distribution.component.HadoopComponent hadoopComponent = null;

try {
    hadoopComponent = (org.talend.hadoop.distribution.component.HadoopComponent) org.talend.hadoop.distribution.DistributionFactory.buildDistribution(sparkDistribution, sparkDistribVersion);
} catch (java.lang.Exception e) {
    e.printStackTrace();
    return "";
}

boolean serializeModelInJob = false;
boolean useLocalMode = "true".equals(ElementParameterParser.getValue(sparkConfig, "__SPARK_LOCAL_MODE__"));
if(!useLocalMode){
    boolean isMaprDistribution = hadoopComponent instanceof org.talend.hadoop.distribution.constants.mapr.IMapRDistribution ? true : false;
    if(isMaprDistribution){
        serializeModelInJob = true;
    }
}


    stringBuffer.append(TEXT_333);
    stringBuffer.append(outRowStruct);
    stringBuffer.append(TEXT_334);
    stringBuffer.append(outConnName);
    stringBuffer.append(TEXT_335);
    
    // Get the pipeline model.
    if (savedOnDisk) {

        if(!"\"\"".equals(uriPrefix)) {
            
    stringBuffer.append(TEXT_336);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_337);
    stringBuffer.append(uriPrefix);
    stringBuffer.append(TEXT_338);
    
        }
        
    stringBuffer.append(TEXT_339);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_340);
    
        // Mapr workaround (EOF exception)
        // Kryo doesn't seem to be able to read from MaprFS
        // We read the model contents to a byte array then pass it to Kryo
        if(serializeModelInJob){
            
    stringBuffer.append(TEXT_341);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_342);
    stringBuffer.append(hdfsFolder);
    stringBuffer.append(TEXT_343);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_344);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_345);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_346);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_347);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_348);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_349);
    
        } else {
            
    stringBuffer.append(TEXT_350);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_351);
    stringBuffer.append(hdfsFolder);
    stringBuffer.append(TEXT_352);
    
        }
        
    stringBuffer.append(TEXT_353);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_354);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_355);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_356);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_357);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_358);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_359);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_360);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_361);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_362);
    
        if(!"\"\"".equals(uriPrefix)) {
            
    stringBuffer.append(TEXT_363);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_364);
    
        }

    } else {
        
    stringBuffer.append(TEXT_365);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_366);
    stringBuffer.append(modelCid);
    stringBuffer.append(TEXT_367);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_368);
    
    }
    
    stringBuffer.append(TEXT_369);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_370);
    stringBuffer.append(TEXT_371);
    stringBuffer.append(dataframeClass);
    stringBuffer.append(TEXT_372);
    stringBuffer.append(inConnName);
    stringBuffer.append(TEXT_373);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_374);
    stringBuffer.append(inConnName);
    stringBuffer.append(TEXT_375);
    stringBuffer.append(inRowStruct);
    stringBuffer.append(TEXT_376);
    stringBuffer.append(TEXT_377);
    stringBuffer.append(dataframeClass);
    stringBuffer.append(TEXT_378);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_379);
    stringBuffer.append(inConnName);
    stringBuffer.append(TEXT_380);
    

    if (needsLabelIndexer) {
        
    stringBuffer.append(TEXT_381);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_382);
    
        if (org.talend.hadoop.distribution.ESparkVersion.SPARK_2_0.compareTo(codeGenArgument.getSparkVersion()) > 0) {
            
    stringBuffer.append(TEXT_383);
    stringBuffer.append(outRowStruct);
    stringBuffer.append(TEXT_384);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_385);
    stringBuffer.append(outRowStruct);
    stringBuffer.append(TEXT_386);
    stringBuffer.append(outConnName);
    stringBuffer.append(TEXT_387);
    
        } else {
            
    stringBuffer.append(TEXT_388);
    stringBuffer.append(outConnName);
    stringBuffer.append(TEXT_389);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_390);
    stringBuffer.append(outRowStruct);
    stringBuffer.append(TEXT_391);
    
        }
    } else {
       
    stringBuffer.append(TEXT_392);
    
    }
    
    stringBuffer.append(TEXT_393);
    

    
}
// SVM_CLASSIFICATION sparkconfig
 else if("SVM_CLASSIFICATION".equals(modelType)){

    stringBuffer.append(TEXT_394);
    
boolean isLog4jEnabled = ("true").equals(ElementParameterParser.getValue(node.getProcess(), "__LOG4J_ACTIVATE__"));

    
IConnection conn = null;
List<? extends IConnection> conns = node.getIncomingConnections();
if(conns != null && conns.size() > 0 && conns.get(0).getLineStyle().hasConnectionCategory(IConnectionCategory.DATA)){
    conn = conns.get(0);
}

List<? extends IConnection> outConns = node.getOutgoingConnections();
IConnection outConn = null;
if(outConns != null && outConns.size() > 0 && outConns.get(0).getLineStyle().hasConnectionCategory(IConnectionCategory.DATA)){
    outConn = outConns.get(0);
}

if(conn == null || outConn == null){
    return "";
}

String inRowStruct = codeGenArgument.getRecordStructName(conn);
String inRowStructEncoded = conn.getName() + "Encoded";
String inConnName = conn.getName();

String outRowStruct = codeGenArgument.getRecordStructName(outConn);
String outConnName = outConn.getName();

Boolean modelOnFilesystem = ElementParameterParser.getBooleanValue(node, "__SAVE_ON_DISK__");
Boolean modelOnJob = ElementParameterParser.getBooleanValue(node, "__SVM_MODEL_COMPUTED__");

if (modelOnFilesystem) {

    String modelPath = ElementParameterParser.getValue(node, "__HDFS_FOLDER__");
    boolean useConfigurationComponent = "true".equals(ElementParameterParser.getValue(node, "__DEFINE_STORAGE_CONFIGURATION__"));

    String uriPrefix = "\"\"";
    if(useConfigurationComponent) {
        uriPrefix = org.talend.designer.spark.generator.storage.SparkStorageUtils.getURIPrefix(node);
        modelPath = uriPrefix + " + " + modelPath;
    }

    if(!"\"\"".equals(uriPrefix)) {
        
    stringBuffer.append(TEXT_395);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_396);
    stringBuffer.append(uriPrefix);
    stringBuffer.append(TEXT_397);
    
    }
    
    stringBuffer.append(TEXT_398);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_399);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_400);
    stringBuffer.append(modelPath);
    stringBuffer.append(TEXT_401);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_402);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_403);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_404);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_405);
    stringBuffer.append(modelPath);
    stringBuffer.append(TEXT_406);
    
    if(!"\"\"".equals(uriPrefix)) {
        
    stringBuffer.append(TEXT_407);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_408);
    
    }
} else if (modelOnJob) {

    //retrieve name of the external component
    String modelLocation = ElementParameterParser.getValue(node, "__SVM_MODEL_LOCATION__");
    
    stringBuffer.append(TEXT_409);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_410);
    stringBuffer.append(modelLocation);
    stringBuffer.append(TEXT_411);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_412);
    stringBuffer.append(modelLocation);
    stringBuffer.append(TEXT_413);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_414);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_415);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_416);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_417);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_418);
    
}

    stringBuffer.append(TEXT_419);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_420);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_421);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_422);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_423);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_424);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_425);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_426);
    stringBuffer.append(TEXT_427);
    stringBuffer.append(dataframeClass);
    stringBuffer.append(TEXT_428);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_429);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_430);
    stringBuffer.append(inConnName);
    stringBuffer.append(TEXT_431);
    stringBuffer.append(inRowStruct);
    stringBuffer.append(TEXT_432);
    stringBuffer.append(TEXT_433);
    stringBuffer.append(dataframeClass);
    stringBuffer.append(TEXT_434);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_435);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_436);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_437);
    stringBuffer.append(inRowStructEncoded);
    stringBuffer.append(TEXT_438);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_439);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_440);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_441);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_442);
    stringBuffer.append(outRowStruct);
    stringBuffer.append(TEXT_443);
    stringBuffer.append(outConnName);
    stringBuffer.append(TEXT_444);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_445);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_446);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_447);
    
}// KMEANS sparkconfig
 else if("KMEANS".equals(modelType)){

    stringBuffer.append(TEXT_448);
    
boolean isLog4jEnabled = ("true").equals(ElementParameterParser.getValue(node.getProcess(), "__LOG4J_ACTIVATE__"));

    
IConnection conn = null;
List<? extends IConnection> conns = node.getIncomingConnections();
if(conns != null && conns.size() > 0 && conns.get(0).getLineStyle().hasConnectionCategory(IConnectionCategory.DATA)){
    conn = conns.get(0);
}

List<? extends IConnection> outConns = node.getOutgoingConnections();
IConnection outConn = null;
if(outConns != null && outConns.size() > 0 && outConns.get(0).getLineStyle().hasConnectionCategory(IConnectionCategory.DATA)){
    outConn = outConns.get(0);
}

if(conn == null || outConn == null){
    return "";
}

String inRowStruct = codeGenArgument.getRecordStructName(conn);
String inRowStructEncoded = conn.getName() + "Encoded";
String inConnName = conn.getName();

String outRowStruct = codeGenArgument.getRecordStructName(outConn);
String outConnName = outConn.getName();


Boolean modelOnFilesystem = ElementParameterParser.getBooleanValue(node, "__SAVE_ON_DISK__");
Boolean modelOnJob = ElementParameterParser.getBooleanValue(node, "__KMEANS_MODEL_COMPUTED__");

if (modelOnFilesystem) {

    String modelPath = ElementParameterParser.getValue(node, "__HDFS_FOLDER__");
    boolean useConfigurationComponent = "true".equals(ElementParameterParser.getValue(node, "__DEFINE_STORAGE_CONFIGURATION__"));

    String uriPrefix = "\"\"";
    if(useConfigurationComponent) {
        uriPrefix = org.talend.designer.spark.generator.storage.SparkStorageUtils.getURIPrefix(node);
        modelPath = uriPrefix + " + " + modelPath;
    }

    if(!"\"\"".equals(uriPrefix)) {
        
    stringBuffer.append(TEXT_449);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_450);
    stringBuffer.append(uriPrefix);
    stringBuffer.append(TEXT_451);
    
    }
    
    stringBuffer.append(TEXT_452);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_453);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_454);
    stringBuffer.append(modelPath);
    stringBuffer.append(TEXT_455);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_456);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_457);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_458);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_459);
    stringBuffer.append(modelPath);
    stringBuffer.append(TEXT_460);
    
    if(!"\"\"".equals(uriPrefix)) {
        
    stringBuffer.append(TEXT_461);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_462);
    
    }
} else if (modelOnJob) {

    //retrieve name of the external component
    String modelLocation = ElementParameterParser.getValue(node, "__KMEANS_MODEL_LOCATION__");
    
    stringBuffer.append(TEXT_463);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_464);
    stringBuffer.append(modelLocation);
    stringBuffer.append(TEXT_465);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_466);
    stringBuffer.append(modelLocation);
    stringBuffer.append(TEXT_467);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_468);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_469);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_470);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_471);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_472);
    
}

    stringBuffer.append(TEXT_473);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_474);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_475);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_476);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_477);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_478);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_479);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_480);
    stringBuffer.append(TEXT_481);
    stringBuffer.append(dataframeClass);
    stringBuffer.append(TEXT_482);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_483);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_484);
    stringBuffer.append(inConnName);
    stringBuffer.append(TEXT_485);
    stringBuffer.append(inRowStruct);
    stringBuffer.append(TEXT_486);
    stringBuffer.append(TEXT_487);
    stringBuffer.append(dataframeClass);
    stringBuffer.append(TEXT_488);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_489);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_490);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_491);
    stringBuffer.append(inRowStructEncoded);
    stringBuffer.append(TEXT_492);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_493);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_494);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_495);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_496);
    stringBuffer.append(outRowStruct);
    stringBuffer.append(TEXT_497);
    stringBuffer.append(outConnName);
    stringBuffer.append(TEXT_498);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_499);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_500);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_501);
    
}

    return stringBuffer.toString();
  }
}
