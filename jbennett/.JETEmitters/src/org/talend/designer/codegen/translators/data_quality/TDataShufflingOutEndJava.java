package org.talend.designer.codegen.translators.data_quality;

import org.talend.core.model.process.INode;
import org.talend.core.model.process.ElementParameterParser;
import org.talend.designer.codegen.config.CodeGeneratorArgument;
import org.talend.core.model.metadata.IMetadataTable;
import org.talend.core.model.metadata.IMetadataColumn;
import org.talend.core.model.process.IConnection;
import org.talend.core.model.process.IConnectionCategory;
import org.talend.core.model.metadata.types.JavaTypesManager;
import org.talend.core.model.metadata.types.JavaType;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.ArrayList;
import org.talend.core.model.utils.NodeUtil;
import org.talend.core.model.process.EConnectionType;

public class TDataShufflingOutEndJava
{
  protected static String nl;
  public static synchronized TDataShufflingOutEndJava create(String lineSeparator)
  {
    nl = lineSeparator;
    TDataShufflingOutEndJava result = new TDataShufflingOutEndJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "";
  protected final String TEXT_2 = NL + "  \tshufflingService_";
  protected final String TEXT_3 = ".setHasFinished(true);" + NL + "  \tglobalMap.put(\"";
  protected final String TEXT_4 = "_NB_LINE\",nb_line_";
  protected final String TEXT_5 = ");" + NL + "\tglobalMap.put(\"";
  protected final String TEXT_6 = "_FINISH\" + (shufflingQueue_";
  protected final String TEXT_7 = "==null?\"\":shufflingQueue_";
  protected final String TEXT_8 = ".hashCode()), \"true\");" + NL + "  \t" + NL + "  \t";
  protected final String TEXT_9 = NL + "    tso_";
  protected final String TEXT_10 = ".join();" + NL + "    if(tso_";
  protected final String TEXT_11 = ".getLastException()!=null) {" + NL + "        currentComponent = tso_";
  protected final String TEXT_12 = ".getCurrentComponent();" + NL + "        throw tso_";
  protected final String TEXT_13 = ".getLastException();" + NL + "    }";
  protected final String TEXT_14 = NL + "\t" + NL + "\tresourceMap.put(\"finish_";
  protected final String TEXT_15 = "\", true);" + NL + "  " + NL + "\t";

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    stringBuffer.append(TEXT_1);
    
    CodeGeneratorArgument codeGenArgument = (CodeGeneratorArgument) argument;
    INode node = (INode)codeGenArgument.getArgument();
    String cid = ElementParameterParser.getValue(node, "__DESTINATION__");
    String virtualTargetCid = node.getOutgoingConnections(EConnectionType.ON_COMPONENT_OK).get(0).getTarget().getUniqueName();

    String incomingConnName = null;    
	IMetadataTable inputMetadataTable = null;
	java.util.List<IMetadataColumn> inputColumns = null;

    List<? extends IConnection> incomingConnections = node.getIncomingConnections();
	if (incomingConnections != null && !incomingConnections.isEmpty()) {	
		for (IConnection conn : incomingConnections) {
			if (conn.getLineStyle().hasConnectionCategory(IConnectionCategory.DATA)) {
				incomingConnName = conn.getName();				
				inputMetadataTable = conn.getMetadataTable();
				inputColumns = inputMetadataTable.getListColumns();
				break;
			}
		}
	}  
  
    stringBuffer.append(TEXT_2);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_3);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_4);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_5);
    stringBuffer.append(virtualTargetCid);
    stringBuffer.append(TEXT_6);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_7);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_8);
    
    IConnection nextMergeConn = NodeUtil.getNextMergeConnection(node);
    if(nextMergeConn == null || nextMergeConn.getInputId()==1){
    
    stringBuffer.append(TEXT_9);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_10);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_11);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_12);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_13);
    
    }

	
    stringBuffer.append(TEXT_14);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_15);
    return stringBuffer.toString();
  }
}
