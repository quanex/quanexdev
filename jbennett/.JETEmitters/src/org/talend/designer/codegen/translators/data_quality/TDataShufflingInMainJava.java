package org.talend.designer.codegen.translators.data_quality;

import org.talend.core.model.process.INode;
import org.talend.core.model.process.ElementParameterParser;
import org.talend.designer.codegen.config.CodeGeneratorArgument;
import org.talend.core.model.metadata.IMetadataTable;
import org.talend.core.model.metadata.IMetadataColumn;
import org.talend.core.model.process.IConnection;
import org.talend.core.model.process.IConnectionCategory;
import org.talend.core.model.metadata.types.JavaTypesManager;
import org.talend.core.model.metadata.types.JavaType;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import org.talend.core.model.utils.NodeUtil;
import org.talend.core.model.process.EConnectionType;

public class TDataShufflingInMainJava
{
  protected static String nl;
  public static synchronized TDataShufflingInMainJava create(String lineSeparator)
  {
    nl = lineSeparator;
    TDataShufflingInMainJava result = new TDataShufflingInMainJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "";
  protected final String TEXT_2 = NL + "\tList<List<Object>> result_";
  protected final String TEXT_3 = " = (List<List<Object>>) shufflingQueue_";
  protected final String TEXT_4 = ".poll();" + NL + "\tfor (int count = 0; count < result_";
  protected final String TEXT_5 = ".size(); count++){//for1" + NL + "\t\tList<Object> rowTemp_";
  protected final String TEXT_6 = " = result_";
  protected final String TEXT_7 = ".get(count);" + NL + "\t\t" + NL + "\t\t";
  protected final String TEXT_8 = "\t" + NL + "\t\t";
  protected final String TEXT_9 = ".";
  protected final String TEXT_10 = " = (";
  protected final String TEXT_11 = ") rowTemp_";
  protected final String TEXT_12 = ".get(";
  protected final String TEXT_13 = ");" + NL + "\t\t";
  protected final String TEXT_14 = NL + "\t" + NL + "\t\t";
  protected final String TEXT_15 = NL + "\t}//for1" + NL + "\t\t";
  protected final String TEXT_16 = NL + "\t" + NL + "\t" + NL + NL + NL + NL;
  protected final String TEXT_17 = NL;

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    stringBuffer.append(TEXT_1);
    
    CodeGeneratorArgument codeGenArgument = (CodeGeneratorArgument) argument;
    INode node = (INode)codeGenArgument.getArgument();
    String cid = ElementParameterParser.getValue(node, "__ORIGIN__");
    String virtualTargetCid = node.getUniqueName();
    String incomingConnName = null, outgoingConnName = null;    
	IMetadataTable outputMetadataTable = null;
	java.util.List<IMetadataColumn> outputColumns = null;
	
	IMetadataTable inputMetadataTable = null;
	java.util.List<IMetadataColumn> inputColumns = null;

	String searchedComponentName = cid + "_SHUFFLIN";
	List<? extends INode> generatedNodes = node.getProcess().getGeneratingNodes();
	for(INode loopNode : generatedNodes) {
		if(loopNode.getUniqueName().equals(searchedComponentName)) {
			List<IConnection> incomingConnections = (List<IConnection>) loopNode.getIncomingConnections();
			if (incomingConnections != null && !incomingConnections.isEmpty()) {
				for (IConnection conn : incomingConnections) {
					incomingConnName = conn.getName();				
				inputMetadataTable = conn.getMetadataTable();
				inputColumns = inputMetadataTable.getListColumns();
				break;
				}
			}
			break;
		}
	}
	
    List<? extends IConnection> outgoingConnections = node.getOutgoingConnections();
	if (outgoingConnections != null && !outgoingConnections.isEmpty()) {	
		for (IConnection conn : outgoingConnections) {
			if (conn.getLineStyle().hasConnectionCategory(IConnectionCategory.DATA)) {
				outgoingConnName = conn.getName();				
				outputMetadataTable = conn.getMetadataTable();
				outputColumns = outputMetadataTable.getListColumns();
				break;
			}
		}
	}   
  
    
	List<Integer> indexes = new java.util.ArrayList<Integer>();
	List<String> transferInputColumn = new java.util.ArrayList<String>();
	for(IMetadataColumn input : inputColumns){
		transferInputColumn.add(input.toString());
	}
	
	for(int i = 0; i < outputColumns.size(); i++){
		indexes.add(transferInputColumn.indexOf(outputColumns.get(i).toString()));
	}
   
    stringBuffer.append(TEXT_2);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_3);
    stringBuffer.append(virtualTargetCid);
    stringBuffer.append(TEXT_4);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_5);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_6);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_7);
    
		for(int i = 0; i < indexes.size(); i++){//forindex
			IMetadataColumn column = outputColumns.get(i);
			int index = indexes.get(i);
			String type = column.getTalendType().substring(column.getTalendType().lastIndexOf("_") + 1);
			if(type.equals("Date")){
				type = "java.util.Date";
			}
		
    stringBuffer.append(TEXT_8);
    stringBuffer.append(outgoingConnName);
    stringBuffer.append(TEXT_9);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_10);
    stringBuffer.append(type);
    stringBuffer.append(TEXT_11);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_12);
    stringBuffer.append(index);
    stringBuffer.append(TEXT_13);
    
		}//endindex
		
    stringBuffer.append(TEXT_14);
    
		if (node.getOutgoingConnections() == null || node.getOutgoingConnections().size() == 0){
		
    stringBuffer.append(TEXT_15);
    
		}
		
    stringBuffer.append(TEXT_16);
    stringBuffer.append(TEXT_17);
    return stringBuffer.toString();
  }
}
