package org.talend.designer.codegen.translators.messaging.maprstreams;

import java.util.List;
import org.talend.core.model.metadata.IMetadataColumn;
import org.talend.core.model.metadata.IMetadataTable;
import org.talend.core.model.metadata.types.JavaType;
import org.talend.core.model.metadata.types.JavaTypesManager;
import org.talend.core.model.process.EConnectionType;
import org.talend.core.model.process.ElementParameterParser;
import org.talend.core.model.process.IConnection;
import org.talend.core.model.process.IConnectionCategory;
import org.talend.core.model.process.INode;
import org.talend.designer.common.BigDataCodeGeneratorArgument;
import org.talend.designer.common.tmaprstreamsoutput.TMapRStreamsOutputUtil;
import org.talend.designer.spark.generator.storage.MapRStreamsSparkStorage;

public class TMapRStreamsOutputSparkstreamingcodeJava
{
  protected static String nl;
  public static synchronized TMapRStreamsOutputSparkstreamingcodeJava create(String lineSeparator)
  {
    nl = lineSeparator;
    TMapRStreamsOutputSparkstreamingcodeJava result = new TMapRStreamsOutputSparkstreamingcodeJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "";
  protected final String TEXT_2 = NL + "\t\tproperties = new java.util.Properties();" + NL + "\t\tproperties.setProperty(\"bootstrap.servers\", \"ignored by MapR Streams\");" + NL + "\t\tproperties.setProperty(\"compression.type\", \"";
  protected final String TEXT_3 = "\");";
  protected final String TEXT_4 = NL + "\t\t\tproperties.setProperty(";
  protected final String TEXT_5 = ", ";
  protected final String TEXT_6 = ");";
  protected final String TEXT_7 = NL + "\t\ttopics = new java.util.HashSet<String>();";
  protected final String TEXT_8 = NL + "\t\t\tif(true){" + NL + "\t\t\t\tthrow new Exception(\"At least one stream/topic must be provided.\");" + NL + "\t\t\t}";
  protected final String TEXT_9 = NL + "\t\t\t\ttopics.add(";
  protected final String TEXT_10 = ");";
  protected final String TEXT_11 = NL;
  protected final String TEXT_12 = NL;
  protected final String TEXT_13 = NL + NL + "public static class ";
  protected final String TEXT_14 = " {" + NL + "\tprivate static volatile org.apache.commons.pool2.ObjectPool<";
  protected final String TEXT_15 = "> pool;" + NL + "\t" + NL + "\tprivate ";
  protected final String TEXT_16 = "() {" + NL + "\t    // empty" + NL + "\t}" + NL + "" + NL + "\tpublic static org.apache.commons.pool2.ObjectPool<";
  protected final String TEXT_17 = "> get(ContextProperties context) {" + NL + "\t\tif(pool == null) {" + NL + "\t\t\tsynchronized(";
  protected final String TEXT_18 = ".class) {" + NL + "\t\t\t\tif(pool == null) {" + NL + "\t\t\t\t\tpool = createPool(context);" + NL + "\t\t\t\t}" + NL + "\t\t\t}" + NL + "\t\t}" + NL + "\t   return pool;" + NL + "\t}" + NL + "" + NL + "\tprivate static org.apache.commons.pool2.ObjectPool<";
  protected final String TEXT_19 = "> createPool(ContextProperties context) {" + NL + "\t    org.apache.commons.pool2.impl.GenericObjectPoolConfig config = new org.apache.commons.pool2.impl.GenericObjectPoolConfig();" + NL + "\t    config.setMaxTotal(";
  protected final String TEXT_20 = ");" + NL + "\t    config.setMaxWaitMillis(";
  protected final String TEXT_21 = ");" + NL + "\t    config.setMinIdle(";
  protected final String TEXT_22 = ");" + NL + "\t    config.setMaxIdle(";
  protected final String TEXT_23 = ");" + NL + "\t    config.setBlockWhenExhausted(true);";
  protected final String TEXT_24 = NL + "\t\t\tconfig.setTimeBetweenEvictionRunsMillis(";
  protected final String TEXT_25 = ");" + NL + "\t\t\tconfig.setMinEvictableIdleTimeMillis(";
  protected final String TEXT_26 = ");" + NL + "\t\t\tconfig.setSoftMinEvictableIdleTimeMillis(";
  protected final String TEXT_27 = ");";
  protected final String TEXT_28 = NL + "\t    return new org.apache.commons.pool2.impl.GenericObjectPool<";
  protected final String TEXT_29 = ">(new ";
  protected final String TEXT_30 = "(context), config);" + NL + "\t}" + NL + "}" + NL + "" + NL + "public static class ";
  protected final String TEXT_31 = " extends org.apache.commons.pool2.BasePooledObjectFactory<";
  protected final String TEXT_32 = "> {" + NL + "" + NL + "\tprivate ContextProperties context;" + NL + "" + NL + "\tprivate java.util.Properties properties;" + NL + "" + NL + "\tpublic ";
  protected final String TEXT_33 = "(ContextProperties context) {" + NL + "\t\tthis.context = context;";
  protected final String TEXT_34 = NL + "\t}" + NL + "" + NL + "\t@Override" + NL + "\tpublic ";
  protected final String TEXT_35 = " create() throws java.lang.Exception {" + NL + "\t\treturn new ";
  protected final String TEXT_36 = "(this.properties, new org.apache.kafka.common.serialization.ByteArraySerializer(), new org.apache.kafka.common.serialization.ByteArraySerializer());" + NL + "\t}" + NL + "" + NL + "\t@Override" + NL + "\tpublic org.apache.commons.pool2.PooledObject<";
  protected final String TEXT_37 = "> wrap(";
  protected final String TEXT_38 = " producer) {" + NL + "\t    return new org.apache.commons.pool2.impl.DefaultPooledObject<";
  protected final String TEXT_39 = ">(producer);" + NL + "\t}" + NL + "" + NL + "\t@Override" + NL + "\tpublic void destroyObject(org.apache.commons.pool2.PooledObject<";
  protected final String TEXT_40 = "> pooledObject) throws Exception {" + NL + "\t    if (pooledObject != null) {" + NL + "\t        pooledObject.getObject().close();" + NL + "\t    }" + NL + "\t    super.destroyObject(pooledObject);" + NL + "\t}" + NL + "}" + NL + "" + NL + "public static class ";
  protected final String TEXT_41 = "_ForeachRDD<KEY> implements ";
  protected final String TEXT_42 = " {" + NL + "" + NL + "\tprivate ContextProperties context;" + NL + "" + NL + "\tpublic ";
  protected final String TEXT_43 = "_ForeachRDD(JobConf job){" + NL + "\t\tthis.context = new ContextProperties(job);" + NL + "\t}" + NL + "" + NL + "\tpublic ";
  protected final String TEXT_44 = " call(org.apache.spark.api.java.JavaPairRDD<KEY,";
  protected final String TEXT_45 = "> rdd) throws java.lang.Exception {" + NL + "\t\trdd.foreachPartition(new ";
  protected final String TEXT_46 = "_ForeachPartition<KEY>(context));" + NL + "\t\t";
  protected final String TEXT_47 = NL + "\t}" + NL + "}" + NL + "" + NL + "public static class ";
  protected final String TEXT_48 = "_ForeachPartition<KEY> implements org.apache.spark.api.java.function.VoidFunction<java.util.Iterator<scala.Tuple2<KEY,";
  protected final String TEXT_49 = ">>> {" + NL + "" + NL + "\tprivate ContextProperties context;" + NL + "" + NL + "\tprivate java.util.Set<String> topics;" + NL + "" + NL + "\tpublic ";
  protected final String TEXT_50 = "_ForeachPartition(ContextProperties context){" + NL + "\t\tthis.context = context;";
  protected final String TEXT_51 = NL + "\t}" + NL + "" + NL + "\tpublic void call(java.util.Iterator<scala.Tuple2<KEY,";
  protected final String TEXT_52 = ">> iterator) throws java.lang.Exception {" + NL + "\t\t";
  protected final String TEXT_53 = " kafkaProducer = ";
  protected final String TEXT_54 = ".get(context).borrowObject();" + NL + "\t\tscala.Tuple2<KEY,";
  protected final String TEXT_55 = "> tuple;" + NL + "\t\ttry {" + NL + "\t\t\twhile(iterator.hasNext()) {" + NL + "\t\t\t\ttuple = iterator.next();" + NL + "\t\t\t\tif(tuple._2() != null){" + NL + "\t\t\t\t\tfor(String topic : topics){" + NL + "\t\t\t\t\t\tjava.nio.ByteBuffer byteBuffer = tuple._2().";
  protected final String TEXT_56 = ";" + NL + "\t\t\t\t\t\tif(byteBuffer != null){" + NL + "\t\t\t\t\t\t\tbyte[] value = new byte[byteBuffer.remaining()];" + NL + "\t\t\t\t\t\t\tbyteBuffer.get(value);" + NL + "\t\t\t\t\t\t\tkafkaProducer.send(new org.apache.kafka.clients.producer.ProducerRecord<byte[], byte[]>(topic, value));" + NL + "\t\t\t\t\t\t}" + NL + "\t\t\t\t\t}" + NL + "\t\t\t\t}" + NL + "\t\t\t}" + NL + "\t\t} finally {" + NL + "\t\t\tif(kafkaProducer != null) {" + NL + "\t\t\t\t";
  protected final String TEXT_57 = ".get(context).returnObject(kafkaProducer);" + NL + "\t\t\t}" + NL + "\t\t}" + NL + "\t}" + NL + "}" + NL + NL;
  protected final String TEXT_58 = NL;

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    stringBuffer.append(TEXT_1);
    
final class TMapRStreamsOutputHelper {

	private TMapRStreamsOutputUtil tMapRStreamsOutputUtil;
	
	public TMapRStreamsOutputHelper(TMapRStreamsOutputUtil util){
		tMapRStreamsOutputUtil = util;
	}

	public void generateProperties() {

    stringBuffer.append(TEXT_2);
    stringBuffer.append(tMapRStreamsOutputUtil.getCompression());
    stringBuffer.append(TEXT_3);
    
		for(java.util.Map.Entry<String, String> producerProperty : tMapRStreamsOutputUtil.getProducerProperties().entrySet()) {

    stringBuffer.append(TEXT_4);
    stringBuffer.append(producerProperty.getKey());
    stringBuffer.append(TEXT_5);
    stringBuffer.append(producerProperty.getValue());
    stringBuffer.append(TEXT_6);
    
		} // end for
	} // end generateProperties
	
	public void generateTopics() {

    stringBuffer.append(TEXT_7);
    
		if(tMapRStreamsOutputUtil.getTopics().isEmpty()){

    stringBuffer.append(TEXT_8);
    
		}else {
			for(String topic : tMapRStreamsOutputUtil.getTopics()) {

    stringBuffer.append(TEXT_9);
    stringBuffer.append(topic);
    stringBuffer.append(TEXT_10);
    
			} // end for
		} // end else
	} // end generateTopics
	
} // end class TMapRStreamsOutputHelper

    stringBuffer.append(TEXT_11);
    
// Parse the inputs to this javajet generator.
final BigDataCodeGeneratorArgument codeGenArgument = (BigDataCodeGeneratorArgument) argument;
final INode node = (INode)codeGenArgument.getArgument();
final TMapRStreamsOutputUtil tMapRStreamsOutputUtil = new TMapRStreamsOutputUtil(node);
final TMapRStreamsOutputHelper tMapRStreamsOutputHelper = new TMapRStreamsOutputHelper(tMapRStreamsOutputUtil);
final MapRStreamsSparkStorage storage = new MapRStreamsSparkStorage(node);

final String cid = node.getUniqueName();
String inStructName = codeGenArgument.getRecordStructName(tMapRStreamsOutputUtil.getIncomingConnection());

final String foreachClass;
final String foreachReturnClass;
final String foreachReturn;
if (org.talend.hadoop.distribution.ESparkVersion.SPARK_2_0.compareTo(codeGenArgument.getSparkVersion()) > 0) {
    foreachClass = "org.apache.spark.api.java.function.Function<org.apache.spark.api.java.JavaPairRDD<KEY, " + inStructName + ">, Void>";
    foreachReturnClass = "Void";
    foreachReturn = "return null;";
} else {
    foreachClass = "org.apache.spark.api.java.function.VoidFunction<org.apache.spark.api.java.JavaPairRDD<KEY," + inStructName + ">>";
    foreachReturnClass = "void";
    foreachReturn = "";
}

    stringBuffer.append(TEXT_12);
    
	// Connection pool code generation.
	
	// Warning : the object factory class has to be generated elsewhere, since the factory implementation depends of
	// the actual pooling object we want to store in this pool.
	
	// This file should be included in every single Spark and Spark Streaming component sparkcode.javajet
	// which requires to create connections to an external system without relying on a dedicated Spark connector. 

    stringBuffer.append(TEXT_13);
    stringBuffer.append(storage.getPoolClassName());
    stringBuffer.append(TEXT_14);
    stringBuffer.append(storage.getPooledObjectClassName());
    stringBuffer.append(TEXT_15);
    stringBuffer.append(storage.getPoolClassName());
    stringBuffer.append(TEXT_16);
    stringBuffer.append(storage.getPooledObjectClassName());
    stringBuffer.append(TEXT_17);
    stringBuffer.append(storage.getPoolClassName());
    stringBuffer.append(TEXT_18);
    stringBuffer.append(storage.getPooledObjectClassName());
    stringBuffer.append(TEXT_19);
    stringBuffer.append(storage.getPoolMaxTotal());
    stringBuffer.append(TEXT_20);
    stringBuffer.append(storage.getPoolMaxWait());
    stringBuffer.append(TEXT_21);
    stringBuffer.append(storage.getPoolMinIdle());
    stringBuffer.append(TEXT_22);
    stringBuffer.append(storage.getPoolMaxIdle());
    stringBuffer.append(TEXT_23);
    
		if(storage.isPoolUseEviction()) {

    stringBuffer.append(TEXT_24);
    stringBuffer.append(storage.getPoolTimeBetweenEviction());
    stringBuffer.append(TEXT_25);
    stringBuffer.append(storage.getPoolEvictionMinIdleTime());
    stringBuffer.append(TEXT_26);
    stringBuffer.append(storage.getPoolEvictionSoftMinIdleTime());
    stringBuffer.append(TEXT_27);
    
		}

    stringBuffer.append(TEXT_28);
    stringBuffer.append(storage.getPooledObjectClassName());
    stringBuffer.append(TEXT_29);
    stringBuffer.append(storage.getFactoryClassName());
    stringBuffer.append(TEXT_30);
    stringBuffer.append(storage.getFactoryClassName());
    stringBuffer.append(TEXT_31);
    stringBuffer.append(storage.getPooledObjectClassName());
    stringBuffer.append(TEXT_32);
    stringBuffer.append(storage.getFactoryClassName());
    stringBuffer.append(TEXT_33);
    
		tMapRStreamsOutputHelper.generateProperties();

    stringBuffer.append(TEXT_34);
    stringBuffer.append(storage.getPooledObjectClassName());
    stringBuffer.append(TEXT_35);
    stringBuffer.append(storage.getPooledObjectClassName());
    stringBuffer.append(TEXT_36);
    stringBuffer.append(storage.getPooledObjectClassName());
    stringBuffer.append(TEXT_37);
    stringBuffer.append(storage.getPooledObjectClassName());
    stringBuffer.append(TEXT_38);
    stringBuffer.append(storage.getPooledObjectClassName());
    stringBuffer.append(TEXT_39);
    stringBuffer.append(storage.getPooledObjectClassName());
    stringBuffer.append(TEXT_40);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_41);
    stringBuffer.append(foreachClass);
    stringBuffer.append(TEXT_42);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_43);
    stringBuffer.append(foreachReturnClass);
    stringBuffer.append(TEXT_44);
    stringBuffer.append(inStructName);
    stringBuffer.append(TEXT_45);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_46);
    stringBuffer.append(foreachReturn);
    stringBuffer.append(TEXT_47);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_48);
    stringBuffer.append(inStructName);
    stringBuffer.append(TEXT_49);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_50);
    
		tMapRStreamsOutputHelper.generateTopics();

    stringBuffer.append(TEXT_51);
    stringBuffer.append(inStructName);
    stringBuffer.append(TEXT_52);
    stringBuffer.append(storage.getPooledObjectClassName());
    stringBuffer.append(TEXT_53);
    stringBuffer.append(storage.getPoolClassName());
    stringBuffer.append(TEXT_54);
    stringBuffer.append(inStructName);
    stringBuffer.append(TEXT_55);
    stringBuffer.append(tMapRStreamsOutputUtil.getIncomingColumnName());
    stringBuffer.append(TEXT_56);
    stringBuffer.append(storage.getPoolClassName());
    stringBuffer.append(TEXT_57);
    stringBuffer.append(TEXT_58);
    return stringBuffer.toString();
  }
}
