package org.talend.designer.codegen.translators.databases.bigquery;

import org.talend.core.model.process.INode;
import org.talend.designer.common.BigDataCodeGeneratorArgument;
import org.talend.designer.common.tsqlrow.TSqlRowUtil;
import org.talend.core.model.metadata.IMetadataTable;
import org.talend.core.model.metadata.IMetadataColumn;
import org.talend.core.model.metadata.types.JavaTypesManager;
import org.talend.core.model.metadata.types.JavaType;
import org.talend.core.model.process.ElementParameterParser;

public class TBigQueryInputSparkstreamingcodeJava
{
  protected static String nl;
  public static synchronized TBigQueryInputSparkstreamingcodeJava create(String lineSeparator)
  {
    nl = lineSeparator;
    TBigQueryInputSparkstreamingcodeJava result = new TBigQueryInputSparkstreamingcodeJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "        public static class ";
  protected final String TEXT_2 = "_FromGsonTo";
  protected final String TEXT_3 = " implements org.apache.spark.api.java.function.Function<com.google.gson.JsonObject, ";
  protected final String TEXT_4 = "> {" + NL + "" + NL + "            private ContextProperties context = null;" + NL + "" + NL + "            public ";
  protected final String TEXT_5 = "_FromGsonTo";
  protected final String TEXT_6 = "(JobConf job) {" + NL + "                this.context = new ContextProperties(job);" + NL + "            }" + NL + "" + NL + "            public ";
  protected final String TEXT_7 = " call(com.google.gson.JsonObject gson) {";
  protected final String TEXT_8 = NL + "                ";
  protected final String TEXT_9 = " result = new ";
  protected final String TEXT_10 = "();" + NL + "                com.google.gson.JsonElement jsonElement = null;";
  protected final String TEXT_11 = NL + "                    jsonElement = gson.get(\"";
  protected final String TEXT_12 = "\");" + NL + "                    " + NL + "                    if(jsonElement == null || jsonElement.isJsonNull()){" + NL + "                    \tresult.";
  protected final String TEXT_13 = " = ";
  protected final String TEXT_14 = ";" + NL + "                    }else if(jsonElement.isJsonArray()){" + NL + "                    \t";
  protected final String TEXT_15 = NL + "                    \t\tcom.google.gson.JsonArray jsonArray = (com.google.gson.JsonArray)jsonElement;" + NL + "                    \t\tjava.util.Iterator<com.google.gson.JsonElement> iterator = jsonArray.iterator();" + NL + "                    \t\tresult.";
  protected final String TEXT_16 = " = new java.util.ArrayList<>();" + NL + "                    \t\twhile(iterator.hasNext()){" + NL + "                    \t\t\tresult.";
  protected final String TEXT_17 = ".add(iterator.next().toString());" + NL + "                    \t\t}" + NL + "                    \t";
  protected final String TEXT_18 = NL + "                    \t\tresult.";
  protected final String TEXT_19 = " = jsonElement.toString();" + NL + "                    \t";
  protected final String TEXT_20 = NL + "                    \t\tthrow new RuntimeException(\"Do not support convert REPEAT to type excepet LIST/STRING/OBJECT\");" + NL + "                    \t";
  protected final String TEXT_21 = NL + "                    }else if(jsonElement.isJsonObject()){" + NL + "                    \t";
  protected final String TEXT_22 = NL + "                    \t\tresult.";
  protected final String TEXT_23 = " = jsonElement.toString();" + NL + "                    \t";
  protected final String TEXT_24 = NL + "                    \t\tthrow new RuntimeException(\"Do not support convert RECORD/STRUCT to type excepet STRING/OBJECT\");" + NL + "                    \t";
  protected final String TEXT_25 = NL + "                    }else{//jsonElement isPrimitive" + NL + "                    \t";
  protected final String TEXT_26 = NL + "                        \tresult.";
  protected final String TEXT_27 = " = java.nio.ByteBuffer.wrap(com.google.api.client.util.Base64.decodeBase64(jsonElement.getAsString()));";
  protected final String TEXT_28 = NL + "                        \tresult.";
  protected final String TEXT_29 = " = jsonElement.getAsString();";
  protected final String TEXT_30 = NL + "                    \t\tresult.";
  protected final String TEXT_31 = " = BigDataParserUtils.parseTo_";
  protected final String TEXT_32 = "(jsonElement.getAsString());" + NL + "                    \t";
  protected final String TEXT_33 = NL + "                    }";
  protected final String TEXT_34 = NL + "                return result;" + NL + "            }" + NL + "        }";

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    
BigDataCodeGeneratorArgument codeGenArgument = (BigDataCodeGeneratorArgument)argument;
INode node = (INode)codeGenArgument.getArgument();
String cid = node.getUniqueName();

TSqlRowUtil tSqlRowUtil = new TSqlRowUtil(node);
String validateError = tSqlRowUtil.validate(false, true);
if (validateError != null) {
    // Cause the job compilation to explicitly fail if there is a problem.
    return "throw new JobConfigurationError(\"" + validateError +"\");";
}

String outStructName = codeGenArgument.getRecordStructName(tSqlRowUtil.getOutgoingConnection());

java.util.List<IMetadataTable> metadatas = node.getMetadataList();
java.util.List<IMetadataColumn> columnList = null;

if(metadatas != null && metadatas.size() > 0){
    IMetadataTable metadata = metadatas.get(0);
    if(metadata != null){
    
    stringBuffer.append(TEXT_1);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_2);
    stringBuffer.append(outStructName);
    stringBuffer.append(TEXT_3);
    stringBuffer.append(outStructName);
    stringBuffer.append(TEXT_4);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_5);
    stringBuffer.append(outStructName);
    stringBuffer.append(TEXT_6);
    stringBuffer.append(outStructName);
    stringBuffer.append(TEXT_7);
    stringBuffer.append(TEXT_8);
    stringBuffer.append(outStructName);
    stringBuffer.append(TEXT_9);
    stringBuffer.append(outStructName);
    stringBuffer.append(TEXT_10);
    
                // Get input schema columns
                columnList = metadata.getListColumns();
                int sizeColumns = columnList.size();
                for (int i = 0; i < sizeColumns; i++) {
                    IMetadataColumn column = columnList.get(i);
                    String typeToGenerate = JavaTypesManager.getTypeToGenerate(column.getTalendType(), column.isNullable());
                    JavaType javaType = JavaTypesManager.getJavaTypeFromId(column.getTalendType());
                    String patternValue = column.getPattern() == null || column.getPattern().trim().length() == 0 ? null : column.getPattern();
                    
    stringBuffer.append(TEXT_11);
    stringBuffer.append(column.getOriginalDbColumnName());
    stringBuffer.append(TEXT_12);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_13);
    stringBuffer.append(JavaTypesManager.getDefaultValueFromJavaType(typeToGenerate));
    stringBuffer.append(TEXT_14);
    
                    	if(javaType == JavaTypesManager.LIST){
                    	
    stringBuffer.append(TEXT_15);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_16);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_17);
    
                    	}else if(javaType == JavaTypesManager.STRING || javaType == JavaTypesManager.OBJECT){
                    	
    stringBuffer.append(TEXT_18);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_19);
    
                    	}else{
                    	
    stringBuffer.append(TEXT_20);
    
                    	}
                    	
    stringBuffer.append(TEXT_21);
    
                    	if(javaType == JavaTypesManager.STRING || javaType == JavaTypesManager.OBJECT){
                    	
    stringBuffer.append(TEXT_22);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_23);
    
                    	}else{
                    	
    stringBuffer.append(TEXT_24);
    
                    	}
                    	
    stringBuffer.append(TEXT_25);
    
                    	if(javaType == JavaTypesManager.BYTE_ARRAY){
                        
    stringBuffer.append(TEXT_26);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_27);
    
                        }else if(javaType == JavaTypesManager.STRING){
                        
    stringBuffer.append(TEXT_28);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_29);
    
                    	}else{
                    	
    stringBuffer.append(TEXT_30);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_31);
    stringBuffer.append(JavaTypesManager.getShortNameFromJavaType(javaType));
    stringBuffer.append(TEXT_32);
    
                    	}
                    	
    stringBuffer.append(TEXT_33);
    
                }
                
    stringBuffer.append(TEXT_34);
    
    }
// No Schema
} else {

}

    return stringBuffer.toString();
  }
}
