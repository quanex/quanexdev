package org.talend.designer.codegen.translators.databases.kudu;

import java.util.List;
import org.talend.core.model.metadata.IMetadataColumn;
import org.talend.core.model.metadata.IMetadataTable;
import org.talend.core.model.metadata.types.JavaType;
import org.talend.core.model.metadata.types.JavaTypesManager;
import org.talend.core.model.process.EConnectionType;
import org.talend.core.model.process.ElementParameterParser;
import org.talend.core.model.process.IConnection;
import org.talend.core.model.process.IConnectionCategory;
import org.talend.core.model.process.INode;
import org.talend.designer.common.BigDataCodeGeneratorArgument;
import org.talend.designer.common.tkuduinput.TKuduInputUtil;

public class TKuduInputSparkconfigJava
{
  protected static String nl;
  public static synchronized TKuduInputSparkconfigJava create(String lineSeparator)
  {
    nl = lineSeparator;
    TKuduInputSparkconfigJava result = new TKuduInputSparkconfigJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "java.util.StringJoiner stringJoiner = new java.util.StringJoiner(\",\");";
  protected final String TEXT_2 = NL + "\tstringJoiner.add(String.format(\"%s:%s\", ";
  protected final String TEXT_3 = ", ";
  protected final String TEXT_4 = "));";
  protected final String TEXT_5 = NL + "final String masters_";
  protected final String TEXT_6 = " = stringJoiner.toString();" + NL + "org.apache.kudu.spark.kudu.KuduContext kuduCtx_";
  protected final String TEXT_7 = " = new org.apache.kudu.spark.kudu.KuduContext(masters_";
  protected final String TEXT_8 = ", ctx.sc());" + NL + "org.apache.spark.sql.SQLContext sqlCtx_";
  protected final String TEXT_9 = " = new org.apache.spark.sql.SQLContext(ctx);";
  protected final String TEXT_10 = NL + "        \t" + NL + "        \tjava.util.List<String> strKuduColumnsList_";
  protected final String TEXT_11 = " = new java.util.ArrayList<String>();" + NL + "        \t";
  protected final String TEXT_12 = NL + "\t\t\t\tstrKuduColumnsList_";
  protected final String TEXT_13 = ".add(\"";
  protected final String TEXT_14 = "\");" + NL + "\t\t\t\t";
  protected final String TEXT_15 = NL + "\t\t\t\tscala.collection.Seq<String> kuduColumnsSeq = scala.collection.JavaConverters.asScalaBufferConverter(strKuduColumnsList_";
  protected final String TEXT_16 = ").asScala().seq();" + NL + "\t\t\t";
  protected final String TEXT_17 = NL + "\t\t\t\tscala.collection.Seq<String> kuduColumnsSeq = scala.collection.JavaConverters.asScalaIterableConverter(strKuduColumnsList_";
  protected final String TEXT_18 = ").asScala().toSeq();" + NL + "\t\t\t";
  protected final String TEXT_19 = NL + "\t\t\t" + NL + "\t\t\torg.apache.spark.rdd.RDD<org.apache.spark.sql.Row> kuduRDD_";
  protected final String TEXT_20 = " = kuduCtx_";
  protected final String TEXT_21 = ".kuduRDD(ctx.sc(), ";
  protected final String TEXT_22 = ", kuduColumnsSeq)" + NL + "\t\t\t";
  protected final String TEXT_23 = NL + "\t\t\t\t\t\t.filter(new KuduFilter_";
  protected final String TEXT_24 = "_";
  protected final String TEXT_25 = "(job))" + NL + "\t\t\t\t";
  protected final String TEXT_26 = ";" + NL + "\t\t\t";
  protected final String TEXT_27 = NL + "\t\t\t\torg.apache.spark.api.java.JavaPairRDD<NullWritable, ";
  protected final String TEXT_28 = "> rdd_";
  protected final String TEXT_29 = " = ctx.parallelize(java.util.Arrays.asList(((org.apache.spark.sql.Row[]) kuduRDD_";
  protected final String TEXT_30 = ".take(";
  protected final String TEXT_31 = "))))." + NL + "\t\t\t\tmapToPair(new ";
  protected final String TEXT_32 = "_FromRowTo";
  protected final String TEXT_33 = "());" + NL + "\t\t\t";
  protected final String TEXT_34 = NL + "\t\t\t\torg.apache.spark.api.java.JavaPairRDD<NullWritable, ";
  protected final String TEXT_35 = "> rdd_";
  protected final String TEXT_36 = " = kuduRDD_";
  protected final String TEXT_37 = ".toJavaRDD().mapToPair(new ";
  protected final String TEXT_38 = "_FromRowTo";
  protected final String TEXT_39 = "());" + NL + "\t\t\t";
  protected final String TEXT_40 = NL;

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    
BigDataCodeGeneratorArgument codeGenArgument = (BigDataCodeGeneratorArgument) argument;
INode node = (INode)codeGenArgument.getArgument();
String cid = node.getUniqueName();
List<IMetadataTable> metadatas = node.getMetadataList();

    
INode kuduConfigurationNode = null;
String kuduConfigurationName = ElementParameterParser.getValue(node,"__KUDU_CONFIGURATION__");
boolean useExistingConnection = ("true").equals(ElementParameterParser.getValue(node,"__USE_EXISTING_CONNECTION__"));

for (INode pNode : node.getProcess().getNodesOfType("tKuduConfiguration")) {
    if(kuduConfigurationName!=null && kuduConfigurationName.equals(pNode.getUniqueName())) {
        kuduConfigurationNode = pNode;
        break;
    }
}

if(!useExistingConnection) {
	kuduConfigurationNode = node;
}

List<java.util.Map<String, String>> serverProperties = (List<java.util.Map<String, String>>) ElementParameterParser.getObjectValue(kuduConfigurationNode, "__SERVER_PROPERTIES__");

    stringBuffer.append(TEXT_1);
    
for(java.util.Map<String, String> map : serverProperties) {

    stringBuffer.append(TEXT_2);
    stringBuffer.append(map.get("SERVER"));
    stringBuffer.append(TEXT_3);
    stringBuffer.append(map.get("PORT"));
    stringBuffer.append(TEXT_4);
    
}

    stringBuffer.append(TEXT_5);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_6);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_7);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_8);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_9);
    
org.talend.hadoop.distribution.ESparkVersion sparkVersion = codeGenArgument.getSparkVersion();

if(metadatas != null && metadatas.size() > 0) {
    IMetadataTable metadata = metadatas.get(0);
    if(metadata != null){
    	List< ? extends IConnection> connections = node.getOutgoingConnections();
    	if ((connections != null) && (connections.size() > 0)) {
    		IConnection connection = connections.get(0);
    		String connName = connection.getName();
    		String connTypeName = codeGenArgument.getRecordStructName(connection);
        	String tableName = ElementParameterParser.getValue(node, "__TABLE__");
        	String schema = ElementParameterParser.getValue(node, "__SCHEMA__");
        	boolean useQuery = "true".equals(ElementParameterParser.getValue(node, "__USE_QUERY__"));
        	String limit = ElementParameterParser.getValue(node, "__LIMIT__");
        	
    stringBuffer.append(TEXT_10);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_11);
    
        	java.util.List<IMetadataColumn> columnList = metadata.getListColumns();
        	for (int i = 0; i < columnList.size(); i++) {
				IMetadataColumn column = columnList.get(i);
				
    stringBuffer.append(TEXT_12);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_13);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_14);
    
			}
			if(org.talend.hadoop.distribution.ESparkVersion.SPARK_2_0.compareTo(sparkVersion) < 0) {
			
    stringBuffer.append(TEXT_15);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_16);
    
			} else {
			
    stringBuffer.append(TEXT_17);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_18);
    
			}
			
    stringBuffer.append(TEXT_19);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_20);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_21);
    stringBuffer.append(tableName);
    stringBuffer.append(TEXT_22);
    
				if (useQuery) {
					TKuduInputUtil tKuduInputUtil = new TKuduInputUtil(node);
					org.talend.designer.common.tkuduinput.conditions.TKuduInputConditions conditions = tKuduInputUtil.findConditions();
					List<org.talend.designer.common.tkuduinput.condition.TKuduInputCondition> listConditions = conditions.getConditions();
					for(int i=0; i<listConditions.size(); i++) {
				
    stringBuffer.append(TEXT_23);
    stringBuffer.append(listConditions.get(i).getColumn());
    stringBuffer.append(TEXT_24);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_25);
    
					}
				}
			
    stringBuffer.append(TEXT_26);
    
			if(limit != null && !limit.trim().isEmpty()) {
			
    stringBuffer.append(TEXT_27);
    stringBuffer.append(connTypeName);
    stringBuffer.append(TEXT_28);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_29);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_30);
    stringBuffer.append(limit);
    stringBuffer.append(TEXT_31);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_32);
    stringBuffer.append(connTypeName);
    stringBuffer.append(TEXT_33);
     } else {
			
    stringBuffer.append(TEXT_34);
    stringBuffer.append(connTypeName);
    stringBuffer.append(TEXT_35);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_36);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_37);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_38);
    stringBuffer.append(connTypeName);
    stringBuffer.append(TEXT_39);
    
			}
		}
	}
}

    stringBuffer.append(TEXT_40);
    return stringBuffer.toString();
  }
}
