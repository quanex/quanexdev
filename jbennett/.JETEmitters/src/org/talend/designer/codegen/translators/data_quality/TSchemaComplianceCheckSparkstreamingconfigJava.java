package org.talend.designer.codegen.translators.data_quality;

import java.util.List;
import java.util.Map;
import org.talend.core.model.metadata.IMetadataColumn;
import org.talend.core.model.metadata.IMetadataTable;
import org.talend.core.model.metadata.types.JavaType;
import org.talend.core.model.metadata.types.JavaTypesManager;
import org.talend.core.model.process.ElementParameterParser;
import org.talend.core.model.process.IBigDataNode;
import org.talend.core.model.process.IConnection;
import org.talend.core.model.process.IConnectionCategory;
import org.talend.core.model.process.INode;
import org.talend.core.model.utils.NodeUtil;
import org.talend.designer.common.BigDataCodeGeneratorArgument;

public class TSchemaComplianceCheckSparkstreamingconfigJava
{
  protected static String nl;
  public static synchronized TSchemaComplianceCheckSparkstreamingconfigJava create(String lineSeparator)
  {
    nl = lineSeparator;
    TSchemaComplianceCheckSparkstreamingconfigJava result = new TSchemaComplianceCheckSparkstreamingconfigJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "            public static class ";
  protected final String TEXT_2 = " implements ";
  protected final String TEXT_3 = " {" + NL + "                private ContextProperties context = new ContextProperties();";
  protected final String TEXT_4 = NL + NL + "                //private ContextProperties context = null;" + NL + "" + NL + "                public ";
  protected final String TEXT_5 = "(JobConf job) {" + NL + "                    this.context = new ContextProperties(job);";
  protected final String TEXT_6 = NL + "                }" + NL + "" + NL + "\t            public ";
  protected final String TEXT_7 = " ";
  protected final String TEXT_8 = "(";
  protected final String TEXT_9 = ") ";
  protected final String TEXT_10 = " {" + NL + "\t            \t";
  protected final String TEXT_11 = NL + "\t            \t";
  protected final String TEXT_12 = NL + "\t                ";
  protected final String TEXT_13 = NL + "\t                return ";
  protected final String TEXT_14 = ";" + NL + "\t            }" + NL + "\t        }" + NL + "\t\t";
  protected final String TEXT_15 = NL + "            public static class ";
  protected final String TEXT_16 = " implements ";
  protected final String TEXT_17 = " {";
  protected final String TEXT_18 = NL + NL + "                private ContextProperties context = null;" + NL + "" + NL + "                public ";
  protected final String TEXT_19 = "(JobConf job) {" + NL + "                    this.context = new ContextProperties(job);" + NL + "                }" + NL + "" + NL + "                public ";
  protected final String TEXT_20 = " ";
  protected final String TEXT_21 = "(";
  protected final String TEXT_22 = ") ";
  protected final String TEXT_23 = " {" + NL + "                \t";
  protected final String TEXT_24 = NL + "\t                 \treturn ";
  protected final String TEXT_25 = ";";
  protected final String TEXT_26 = NL + "                }" + NL + "            }";
  protected final String TEXT_27 = NL + "            public static class ";
  protected final String TEXT_28 = " implements ";
  protected final String TEXT_29 = " {";
  protected final String TEXT_30 = NL + NL + "                private ContextProperties context = null;" + NL + "                private java.util.function.Function<org.apache.avro.generic.IndexedRecord, org.apache.avro.generic.IndexedRecord> function = null;" + NL + "" + NL + "                public ";
  protected final String TEXT_31 = "(JobConf job, java.util.function.Function<org.apache.avro.generic.IndexedRecord, org.apache.avro.generic.IndexedRecord> function) {" + NL + "                    this.context = new ContextProperties(job);" + NL + "                    this.function = function;" + NL + "                }" + NL + "" + NL + "                public ";
  protected final String TEXT_32 = " ";
  protected final String TEXT_33 = "(";
  protected final String TEXT_34 = ") ";
  protected final String TEXT_35 = " {";
  protected final String TEXT_36 = NL + "                    ";
  protected final String TEXT_37 = NL + "                    ";
  protected final String TEXT_38 = NL + "                    return ";
  protected final String TEXT_39 = ";" + NL + "                }" + NL + "            }";
  protected final String TEXT_40 = NL;
  protected final String TEXT_41 = NL + "            // No sparkcode generated for unnecessary ";
  protected final String TEXT_42 = NL;
  protected final String TEXT_43 = NL + "            public static class ";
  protected final String TEXT_44 = "TrueFilter implements org.apache.spark.api.java.function.Function<scala.Tuple2<Boolean, org.apache.avro.specific.SpecificRecordBase>, Boolean> {" + NL + "" + NL + "                public Boolean call(scala.Tuple2<Boolean, org.apache.avro.specific.SpecificRecordBase> arg0)" + NL + "                        throws Exception {" + NL + "                    return arg0._1;" + NL + "                }" + NL + "            }" + NL + "" + NL + "            public static class ";
  protected final String TEXT_45 = "FalseFilter implements org.apache.spark.api.java.function.Function<scala.Tuple2<Boolean, org.apache.avro.specific.SpecificRecordBase>, Boolean> {" + NL + "" + NL + "                public Boolean call(scala.Tuple2<Boolean, org.apache.avro.specific.SpecificRecordBase> arg0)" + NL + "                        throws Exception {" + NL + "                    return !arg0._1;" + NL + "                }" + NL + "            }" + NL + "" + NL + "            public static class ";
  protected final String TEXT_46 = "ToNullWritableMain implements ";
  protected final String TEXT_47 = " {" + NL + "" + NL + "                private ContextProperties context = null;" + NL + "" + NL + "                public ";
  protected final String TEXT_48 = "ToNullWritableMain(JobConf job) {" + NL + "                    this.context = new ContextProperties(job);" + NL + "                }" + NL + "" + NL + "                public ";
  protected final String TEXT_49 = " call(" + NL + "                        scala.Tuple2<Boolean, org.apache.avro.specific.SpecificRecordBase> data){";
  protected final String TEXT_50 = NL + "                    ";
  protected final String TEXT_51 = " ";
  protected final String TEXT_52 = " = (";
  protected final String TEXT_53 = ")data._2;" + NL + "                    return ";
  protected final String TEXT_54 = ";" + NL + "                }" + NL + "            }" + NL + "" + NL + "            public static class ";
  protected final String TEXT_55 = "ToNullWritableReject implements ";
  protected final String TEXT_56 = " {" + NL + "" + NL + "                private ContextProperties context = null;" + NL + "" + NL + "                public ";
  protected final String TEXT_57 = "ToNullWritableReject(JobConf job) {" + NL + "                    this.context = new ContextProperties(job);" + NL + "                }" + NL + "" + NL + "                public ";
  protected final String TEXT_58 = " call(" + NL + "                        scala.Tuple2<Boolean, org.apache.avro.specific.SpecificRecordBase> data){";
  protected final String TEXT_59 = NL + "                        ";
  protected final String TEXT_60 = " ";
  protected final String TEXT_61 = " = (";
  protected final String TEXT_62 = ")data._2;" + NL + "                    return ";
  protected final String TEXT_63 = ";" + NL + "                }" + NL + "            }";
  protected final String TEXT_64 = NL + "            // No sparkconfig generated for unnecessary ";
  protected final String TEXT_65 = NL;
  protected final String TEXT_66 = NL + "            // Extract data." + NL;
  protected final String TEXT_67 = NL + "            ";
  protected final String TEXT_68 = "<Boolean, org.apache.avro.specific.SpecificRecordBase> temporary_rdd_";
  protected final String TEXT_69 = " = rdd_";
  protected final String TEXT_70 = ".";
  protected final String TEXT_71 = "(new ";
  protected final String TEXT_72 = "(job));" + NL + "" + NL + "            // Main flow" + NL;
  protected final String TEXT_73 = NL + "            ";
  protected final String TEXT_74 = " rdd_";
  protected final String TEXT_75 = " = temporary_rdd_";
  protected final String TEXT_76 = NL + "                  .filter(new ";
  protected final String TEXT_77 = "TrueFilter())" + NL + "                    .";
  protected final String TEXT_78 = "(new ";
  protected final String TEXT_79 = "ToNullWritableMain(job));" + NL + "" + NL + "            // Reject flow";
  protected final String TEXT_80 = NL + "            ";
  protected final String TEXT_81 = " rdd_";
  protected final String TEXT_82 = " = temporary_rdd_";
  protected final String TEXT_83 = NL + "                    .filter(new ";
  protected final String TEXT_84 = "FalseFilter())" + NL + "                    .";
  protected final String TEXT_85 = "(new ";
  protected final String TEXT_86 = "ToNullWritableReject(job));";
  protected final String TEXT_87 = NL + "            ";
  protected final String TEXT_88 = " rdd_";
  protected final String TEXT_89 = " = rdd_";
  protected final String TEXT_90 = ".";
  protected final String TEXT_91 = "(new ";
  protected final String TEXT_92 = "(job));";
  protected final String TEXT_93 = NL + "\t\t\t//number of digits before the decimal point(ignoring frontend zeroes)" + NL + "\t\t\tString bdp_data = ";
  protected final String TEXT_94 = ";" + NL + " \t\t\tint len1 = 0;" + NL + " \t\t\tint len2 = 0;" + NL + " \t\t\tif(bdp_data.startsWith(\"-\")){" + NL + " \t\t\t\tbdp_data = bdp_data.substring(1);" + NL + " \t\t\t}" + NL + " \t\t\tbdp_data = org.apache.commons.lang.StringUtils.stripStart(bdp_data, \"0\");" + NL + "" + NL + " \t\t\tif(bdp_data.indexOf(\".\") >= 0){" + NL + " \t\t\t\tlen1 = bdp_data.indexOf(\".\");" + NL + " \t\t\t\tbdp_data = org.apache.commons.lang.StringUtils.stripEnd(bdp_data, \"0\");" + NL + " \t\t\t\tlen2 = bdp_data.length() - (len1 + 1);" + NL + " \t\t\t}else{" + NL + " \t\t\t\tlen1 = bdp_data.length();" + NL + " \t\t\t}" + NL + "" + NL + " \t\t\tif (";
  protected final String TEXT_95 = " < len2) {" + NL + " \t\t\t\tifPassedThrough = false;" + NL + " \t\t\t\terrorCodeThrough += 8;" + NL + " \t\t\t\terrorMessageThrough += \"|precision Non-matches\";" + NL + " \t\t\t} else if (";
  protected final String TEXT_96 = " < len1 + ";
  protected final String TEXT_97 = ") {" + NL + " \t\t\t\tifPassedThrough = false;" + NL + " \t\t\t\terrorCodeThrough += 8;" + NL + " \t\t\t\terrorMessageThrough += \"|invalid Length setting is unsuitable for Precision\";" + NL + " \t\t\t}";
  protected final String TEXT_98 = NL + "try {" + NL + "\tif(";
  protected final String TEXT_99 = NL + "\t";
  protected final String TEXT_100 = ".";
  protected final String TEXT_101 = " != null";
  protected final String TEXT_102 = NL + "\t&& (!\"\".equals(";
  protected final String TEXT_103 = ".";
  protected final String TEXT_104 = "))";
  protected final String TEXT_105 = NL + "\t";
  protected final String TEXT_106 = ".";
  protected final String TEXT_107 = " != null";
  protected final String TEXT_108 = NL + "\ttrue";
  protected final String TEXT_109 = NL + "\t) {";
  protected final String TEXT_110 = NL + "\t\tif(!(\"true\".equals(";
  protected final String TEXT_111 = ".";
  protected final String TEXT_112 = ") || \"false\".equals(";
  protected final String TEXT_113 = ".";
  protected final String TEXT_114 = "))){" + NL + "\t\t\tthrow new java.lang.Exception(\"Wrong Boolean type!\");" + NL + "\t\t}";
  protected final String TEXT_115 = NL + "\t\tif(";
  protected final String TEXT_116 = ".";
  protected final String TEXT_117 = ".toCharArray().length != 1){" + NL + "\t\t\tthrow new java.lang.Exception(\"Wrong Character type!\");" + NL + "\t\t}";
  protected final String TEXT_118 = NL + "\t\t";
  protected final String TEXT_119 = " tester_";
  protected final String TEXT_120 = " = new ";
  protected final String TEXT_121 = "(";
  protected final String TEXT_122 = ".";
  protected final String TEXT_123 = ");";
  protected final String TEXT_124 = NL + "\t\t";
  protected final String TEXT_125 = " tester_";
  protected final String TEXT_126 = " = new ";
  protected final String TEXT_127 = "();";
  protected final String TEXT_128 = NL + "\t\t";
  protected final String TEXT_129 = " tester_";
  protected final String TEXT_130 = " = ";
  protected final String TEXT_131 = ".valueOf(";
  protected final String TEXT_132 = ".";
  protected final String TEXT_133 = ");";
  protected final String TEXT_134 = NL + "\t}" + NL + "} catch(java.lang.Exception e) {" + NL + "\tifPassedThrough = false;" + NL + "\terrorCodeThrough += 2;" + NL + "\terrorMessageThrough += \"|wrong type\";" + NL + "}";
  protected final String TEXT_135 = NL + "if (";
  protected final String TEXT_136 = ".";
  protected final String TEXT_137 = " != null){";
  protected final String TEXT_138 = NL + "}";
  protected final String TEXT_139 = NL + "if (";
  protected final String TEXT_140 = NL;
  protected final String TEXT_141 = ".";
  protected final String TEXT_142 = " != null ";
  protected final String TEXT_143 = NL + "&& (!\"\".equals(";
  protected final String TEXT_144 = ".";
  protected final String TEXT_145 = "))\t\t\t\t\t";
  protected final String TEXT_146 = " ";
  protected final String TEXT_147 = NL;
  protected final String TEXT_148 = ".";
  protected final String TEXT_149 = " != null";
  protected final String TEXT_150 = " " + NL + "true";
  protected final String TEXT_151 = NL + ") {";
  protected final String TEXT_152 = NL + "\tif( ";
  protected final String TEXT_153 = ".";
  protected final String TEXT_154 = ".length() > ";
  protected final String TEXT_155 = " )" + NL + "\t\t";
  protected final String TEXT_156 = ".";
  protected final String TEXT_157 = " = ";
  protected final String TEXT_158 = ".";
  protected final String TEXT_159 = ".substring(0, ";
  protected final String TEXT_160 = ");";
  protected final String TEXT_161 = NL + "\ttmpContentThrough = String.valueOf(";
  protected final String TEXT_162 = ".";
  protected final String TEXT_163 = ");";
  protected final String TEXT_164 = NL + "\ttmpContentThrough = ";
  protected final String TEXT_165 = ".";
  protected final String TEXT_166 = ".toString();";
  protected final String TEXT_167 = NL + "\tif (tmpContentThrough.length() > ";
  protected final String TEXT_168 = ")" + NL + "\t\t";
  protected final String TEXT_169 = ".";
  protected final String TEXT_170 = " = ";
  protected final String TEXT_171 = ".";
  protected final String TEXT_172 = ".substring(0, ";
  protected final String TEXT_173 = ");";
  protected final String TEXT_174 = NL + "\tif (";
  protected final String TEXT_175 = ".";
  protected final String TEXT_176 = ".length() > ";
  protected final String TEXT_177 = ") {" + NL + "\t\tifPassedThrough = false;" + NL + "\t\terrorCodeThrough += 8;" + NL + "\t\terrorMessageThrough += \"|exceed max length\";" + NL + "\t}";
  protected final String TEXT_178 = NL + "\ttmpContentThrough = String.valueOf(";
  protected final String TEXT_179 = ".";
  protected final String TEXT_180 = ");";
  protected final String TEXT_181 = NL + "\ttmpContentThrough = ";
  protected final String TEXT_182 = ".";
  protected final String TEXT_183 = ".toString();  ";
  protected final String TEXT_184 = NL + NL + "\tif (tmpContentThrough.length() > ";
  protected final String TEXT_185 = ") {" + NL + "\t\tifPassedThrough = false;" + NL + "\t\terrorCodeThrough += 8;" + NL + "\t\terrorMessageThrough += \"|exceed max length\";" + NL + "\t}";
  protected final String TEXT_186 = NL + "}";
  protected final String TEXT_187 = NL + "ifPassedThrough = false;" + NL + "errorCodeThrough += 2;" + NL + "errorMessageThrough += \"|Date format not defined\";";
  protected final String TEXT_188 = NL + "try{" + NL + "\tif (";
  protected final String TEXT_189 = NL + "\t";
  protected final String TEXT_190 = ".";
  protected final String TEXT_191 = " != null ";
  protected final String TEXT_192 = NL + "\t&& (!\"\".equals(";
  protected final String TEXT_193 = ".";
  protected final String TEXT_194 = "))";
  protected final String TEXT_195 = " " + NL + "\t";
  protected final String TEXT_196 = ".";
  protected final String TEXT_197 = " != null";
  protected final String TEXT_198 = " " + NL + "\ttrue";
  protected final String TEXT_199 = NL + "\t){";
  protected final String TEXT_200 = NL + "\t\tif (!TalendDate.isDate((";
  protected final String TEXT_201 = ".";
  protected final String TEXT_202 = ").toString(), ";
  protected final String TEXT_203 = ",";
  protected final String TEXT_204 = "true";
  protected final String TEXT_205 = "false";
  protected final String TEXT_206 = "))" + NL + "\t\t\tthrow new IllegalArgumentException(\"Data format not matches\");";
  protected final String TEXT_207 = NL + "\t\tFastDateParser.getInstance(";
  protected final String TEXT_208 = ", false).parse(";
  protected final String TEXT_209 = ".";
  protected final String TEXT_210 = ");            ";
  protected final String TEXT_211 = NL + "\t}" + NL + "} catch(java.lang.Exception e){" + NL + "\tifPassedThrough = false;" + NL + "\terrorCodeThrough += 2;" + NL + "\terrorMessageThrough += \"|wrong DATE pattern or wrong DATE data\";" + NL + "}";
  protected final String TEXT_212 = NL + "ifPassedThrough = false;" + NL + "errorCodeThrough += 2;" + NL + "errorMessageThrough += \"|wrong DATE pattern or wrong DATE data\";";
  protected final String TEXT_213 = NL + "ifPassedThrough = false;" + NL + "errorCodeThrough += 2;" + NL + "errorMessageThrough += \"|The TYPE of inputting data is error. (one of OBJECT, STRING, DATE)\";";
  protected final String TEXT_214 = NL + "// validate nullable (empty as null)" + NL + "if ((";
  protected final String TEXT_215 = ".";
  protected final String TEXT_216 = " == null) || (\"\".equals(";
  protected final String TEXT_217 = ".";
  protected final String TEXT_218 = "))) {";
  protected final String TEXT_219 = NL + "// validate nullable" + NL + "if (";
  protected final String TEXT_220 = ".";
  protected final String TEXT_221 = " == null) {";
  protected final String TEXT_222 = NL + "// validate nullable (empty as null)" + NL + "if ((";
  protected final String TEXT_223 = ".";
  protected final String TEXT_224 = " == null) || (\"\".equals(";
  protected final String TEXT_225 = ".";
  protected final String TEXT_226 = "))) {";
  protected final String TEXT_227 = NL + "// validate nullable (empty as null)" + NL + "if ((";
  protected final String TEXT_228 = ".";
  protected final String TEXT_229 = " == null) || (\"\".equals(";
  protected final String TEXT_230 = ".";
  protected final String TEXT_231 = "))) {";
  protected final String TEXT_232 = NL + "// validate nullable" + NL + "if (";
  protected final String TEXT_233 = ".";
  protected final String TEXT_234 = " == null) {";
  protected final String TEXT_235 = NL + "\tifPassedThrough = false;" + NL + "\terrorCodeThrough += 4;" + NL + "\terrorMessageThrough += \"|empty or null\";" + NL + "}";
  protected final String TEXT_236 = NL + "\t\terrorMessageThrough = \"\";";
  protected final String TEXT_237 = NL + "\t\tboolean rowHasConversionError = false;" + NL + "\t\tException rowException = null;" + NL + "\t\tboolean ifPassedThrough = true;" + NL + "\t\tint errorCodeThrough = 0;" + NL + "\t\tString rowErrorMessageThrough = \"\";" + NL + "\t\tString errorMessageThrough = \"\";" + NL + "\t\tString tmpContentThrough = \"\";";
  protected final String TEXT_238 = NL + "\t\t\t\t\t";
  protected final String TEXT_239 = NL + "\t\t\t\t\t" + NL + "\t\t\t\t\tif (errorMessageThrough.length() > 0) {" + NL + "\t\t \t\t\t\tif (rowErrorMessageThrough.length() > 0) {" + NL + "\t\t \t\t\t\t\trowErrorMessageThrough += \";\"+ errorMessageThrough.replaceFirst(\"\\\\|\", \"";
  protected final String TEXT_240 = " : \");" + NL + "\t\t \t\t\t\t} else {" + NL + "\t\t \t\t\t\t\trowErrorMessageThrough = errorMessageThrough.replaceFirst(\"\\\\|\", \"";
  protected final String TEXT_241 = " : \");" + NL + "\t\t \t\t\t\t}" + NL + "\t\t \t\t\t}";
  protected final String TEXT_242 = NL + "\t\tif(!ifPassedThrough){" + NL + "\t\t\tString errorCodeThroughStr = String.valueOf(errorCodeThrough);";
  protected final String TEXT_243 = NL + "        }else{";
  protected final String TEXT_244 = NL + "            \t";
  protected final String TEXT_245 = NL + "        }";
  protected final String TEXT_246 = NL + "\t\t";
  protected final String TEXT_247 = NL + "\t\t";
  protected final String TEXT_248 = NL + "\t\t";
  protected final String TEXT_249 = NL + "\t\t";

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    
// Parse the inputs to this javajet generator.
final BigDataCodeGeneratorArgument codeGenArgument = (BigDataCodeGeneratorArgument) argument;
final INode node = (INode) codeGenArgument.getArgument();
final IBigDataNode bigDataNode = (IBigDataNode) codeGenArgument.getArgument();
final String cid = node.getUniqueName();


    
	/**
	 * Code generated for component with single output
 	 */
    class SOFunctionGenerator extends org.talend.designer.spark.generator.FunctionGenerator{

    	SOFunctionGenerator(org.talend.designer.common.TransformerBase transformer) {
            super(transformer);
        }

    	SOFunctionGenerator(org.talend.designer.common.TransformerBase transformer, org.talend.designer.spark.generator.SparkFunction sparkFunction) {
    		super(transformer, sparkFunction);
    	}

    	@Override
        public void generate(){
		
    stringBuffer.append(TEXT_1);
    stringBuffer.append(this.sparkFunction.getClassName(cid));
    stringBuffer.append(TEXT_2);
    stringBuffer.append(this.sparkFunction.getCodeFunctionImplementation(getOutValueClass(), getInValueClass()));
    stringBuffer.append(TEXT_3);
    
                this.transformer.generateHelperClasses(true);
                this.transformer.generateTransformContextDeclaration();
                
    stringBuffer.append(TEXT_4);
    stringBuffer.append(this.sparkFunction.getClassName(cid));
    stringBuffer.append(TEXT_5);
    
                    this.transformer.generateTransformContextDeclarationInConstructor();
                    
    stringBuffer.append(TEXT_6);
    stringBuffer.append(this.sparkFunction.getCodeFunctionReturnedType(this.outValueClass.toString()));
    stringBuffer.append(TEXT_7);
    stringBuffer.append(this.sparkFunction.getCodeImplementedMethod());
    stringBuffer.append(TEXT_8);
    stringBuffer.append(this.sparkFunction.getCodeFunctionArgument(getInValueClass()));
    stringBuffer.append(TEXT_9);
    stringBuffer.append(this.sparkFunction.getCodeThrowException());
    stringBuffer.append(TEXT_10);
    stringBuffer.append(this.sparkFunction.getMethodHeader(this.outValueClass, this.outValue, this.inValueClass, this.inValue));
    stringBuffer.append(TEXT_11);
    stringBuffer.append(this.sparkFunction.getCodeKeyMapping(getInValue()));
    stringBuffer.append(TEXT_12);
    
	                this.transformer.generateTransformContextInitialization();
	                this.transformer.generateTransform(true);
	                
    stringBuffer.append(TEXT_13);
    stringBuffer.append(this.sparkFunction.getCodeFunctionReturn(this.getOutValue(), this.getOutValueClass()));
    stringBuffer.append(TEXT_14);
    
        }
    }

	/**
	 * Code generated for component with multiple outputs
 	 */
    class MOFunctionGenerator extends org.talend.designer.spark.generator.FunctionGenerator{

        /** The single connection to pass along the output chain of Mappers
         *  instead of sending to a dedicated collector via MultipleOutputs. */
        String connectionToChain = null;

        MOFunctionGenerator(org.talend.designer.common.TransformerBase transformer) {
            super(transformer);
            defaultOutKeyClass = "Boolean";
        }

    	MOFunctionGenerator(org.talend.designer.common.TransformerBase transformer, org.talend.designer.spark.generator.SparkFunction sparkFunction) {
    		super(transformer, sparkFunction);
            defaultOutKeyClass = "Boolean";
    	}

    	@Override
        public void generate(){
        
    stringBuffer.append(TEXT_15);
    stringBuffer.append(this.sparkFunction.getClassName(cid));
    stringBuffer.append(TEXT_16);
    stringBuffer.append(this.sparkFunction.getCodeFunctionImplementationOutputFixedType(getInValueClass(), "Boolean", "org.apache.avro.specific.SpecificRecordBase"));
    stringBuffer.append(TEXT_17);
    
                this.transformer.generateHelperClasses(true);
                this.transformer.generateTransformContextDeclaration();
                
    stringBuffer.append(TEXT_18);
    stringBuffer.append(this.sparkFunction.getClassName(cid));
    stringBuffer.append(TEXT_19);
    stringBuffer.append(this.sparkFunction.getCodeFunctionReturnedTypeFixedType((String)this.outKeyClass, "org.apache.avro.specific.SpecificRecordBase"));
    stringBuffer.append(TEXT_20);
    stringBuffer.append(this.sparkFunction.getCodeImplementedMethod());
    stringBuffer.append(TEXT_21);
    stringBuffer.append(this.sparkFunction.getCodeFunctionArgument(getInValueClass()));
    stringBuffer.append(TEXT_22);
    stringBuffer.append(this.sparkFunction.getCodeThrowException());
    stringBuffer.append(TEXT_23);
    stringBuffer.append(this.sparkFunction.getMethodHeader(this.outValueClass, this.outValue, this.inValueClass, this.inValue));
    
                    this.transformer.generateTransformContextInitialization();
                    this.transformer.generateTransform(true);
	                if(this.sparkFunction.getCodeFunctionReturn()!=null) {
                
    stringBuffer.append(TEXT_24);
    stringBuffer.append(this.sparkFunction.getCodeFunctionReturn());
    stringBuffer.append(TEXT_25);
    
	            	}
                
    stringBuffer.append(TEXT_26);
    
        }
    }
    
    /**
     * Code generated for tDataprepRun component
     */
    class DataprepFunctionGenerator extends org.talend.designer.spark.generator.FunctionGenerator {

        DataprepFunctionGenerator(org.talend.designer.common.TransformerBase transformer) {
            super(transformer);
        }

        DataprepFunctionGenerator(org.talend.designer.common.TransformerBase transformer, org.talend.designer.spark.generator.SparkFunction sparkFunction) {
            super(transformer, sparkFunction);
        }

        @Override
        public void generate(){
        
    stringBuffer.append(TEXT_27);
    stringBuffer.append(this.sparkFunction.getClassName(cid));
    stringBuffer.append(TEXT_28);
    stringBuffer.append(this.sparkFunction.getCodeFunctionImplementation(getOutValueClass(), getInValueClass()));
    stringBuffer.append(TEXT_29);
    
                this.transformer.generateHelperClasses(true);
                this.transformer.generateTransformContextDeclaration();
                
    stringBuffer.append(TEXT_30);
    stringBuffer.append(this.sparkFunction.getClassName(cid));
    stringBuffer.append(TEXT_31);
    stringBuffer.append(this.sparkFunction.getCodeFunctionReturnedType(this.outValueClass.toString()));
    stringBuffer.append(TEXT_32);
    stringBuffer.append(this.sparkFunction.getCodeImplementedMethod());
    stringBuffer.append(TEXT_33);
    stringBuffer.append(this.sparkFunction.getCodeFunctionArgument(getInValueClass()));
    stringBuffer.append(TEXT_34);
    stringBuffer.append(this.sparkFunction.getCodeThrowException());
    stringBuffer.append(TEXT_35);
    stringBuffer.append(TEXT_36);
    stringBuffer.append(this.sparkFunction.getMethodHeader(this.outValueClass, this.outValue, this.inValueClass, this.inValue));
    stringBuffer.append(TEXT_37);
    stringBuffer.append(this.sparkFunction.getCodeKeyMapping(getInValue()));
    
                        this.transformer.generateTransformContextInitialization();
                        this.transformer.generateTransform(true);
                     
    stringBuffer.append(TEXT_38);
    stringBuffer.append(this.sparkFunction.getCodeFunctionReturn(this.getOutValue(), this.getOutValueClass()));
    stringBuffer.append(TEXT_39);
    
        }
    }

    stringBuffer.append(TEXT_40);
    

/**
 * A common, reusable utility that generates code in the context of a spark
 * component, for reading and writing to a spark RDD.
 */
class SparkRowTransformUtil extends org.talend.designer.common.CommonRowTransformUtil {

    private boolean isMultiOutput = false;

    private org.talend.designer.spark.generator.SparkFunction sparkFunction = null;

    private org.talend.designer.spark.generator.FunctionGenerator functionGenerator = null;

    public SparkRowTransformUtil() {

    }
    
    public SparkRowTransformUtil(org.talend.designer.spark.generator.SparkFunction sparkFunction) {
        this.sparkFunction = sparkFunction;
    }
    
    public void setFunctionGenerator(org.talend.designer.spark.generator.FunctionGenerator functionGenerator) {
        this.functionGenerator = functionGenerator;
    }

    public void setMultiOutput(boolean multiOutput) {
        isMultiOutput = multiOutput;
    }

    public String getCodeToGetInField(String columnName) {
        return functionGenerator.getInValue() + "." + columnName;
    }

    public String getInValue() {
        return functionGenerator.getInValue();
    }

    public String getOutValue() {
        return functionGenerator.getOutValue();
    }

    public String getInValueClass() {
        return functionGenerator.getInValueClass();
    }

    public String getOutValueClass() {
        return functionGenerator.getOutValueClass();
    }

    public String getCodeToGetOutField(boolean isReject, String columnName) {
        return functionGenerator.getOutValue(isReject ? "reject" : "main") + "." + columnName;
    }

    public String getCodeToInitOut(boolean isReject, Iterable<IMetadataColumn> columns) {
        if(!isReject && this.sparkFunction!=null && !isMultiOutput) {
            return this.sparkFunction.getCodeToInitOut(functionGenerator.getOutValue("main"), functionGenerator.getOutValueClass("main"));
        } else {
            return "";
        }
    }

    // Method to avoid using getCodeToInitOut that calls sparkFunction.getCodeToInitOut which creates unnecessary objects
    // Check getCodeToAddToOutput in SparkFunction and its implementation in FlatMapToPairFunction
    public String getCodeToAddToOutput(boolean isReject, Iterable<IMetadataColumn> columns) {
        if(this.sparkFunction!=null && !isMultiOutput) {
            return this.sparkFunction.getCodeToAddToOutput(false, false, functionGenerator.getOutValue(isReject ? "reject" : "main"), functionGenerator.getOutValueClass(isReject ? "reject" : "main"));
        }else if(this.sparkFunction!=null && isMultiOutput){
            if(isReject){
                return this.sparkFunction.getCodeToAddToOutput(true, false, functionGenerator.getOutValue("reject"), functionGenerator.getOutValueClass("reject"));
            }else{
                return this.sparkFunction.getCodeToAddToOutput(true, true, functionGenerator.getOutValue("main"), functionGenerator.getOutValueClass("main"));
            }
        }else {
            return "";
        }
    }

    public String getCodeToSetOutField(boolean isReject, String columnName, String codeValue) {
        return functionGenerator.getOutValue(isReject ? "reject" : "main") + "." + columnName + " = " + codeValue + ";";
    }

    public String getCodeToSetOutField(boolean isReject, String columnName, String codeValue, String operator) {
        return functionGenerator.getOutValue(isReject ? "reject" : "main") + "." + columnName + " " + operator + " " + codeValue + ";";
    }

    public String getCodeToEmit(boolean isReject) {
        if (this.sparkFunction != null && isMultiOutput) {
            if (isReject) {
                return this.sparkFunction.getCodeToEmit(false, functionGenerator.getOutValue("reject"), functionGenerator.getOutValueClass("reject"));
            } else {
                return this.sparkFunction.getCodeToEmit(true, functionGenerator.getOutValue("main"), functionGenerator.getOutValueClass("main"));
            }
        } else {
            if (isReject) {
                return this.sparkFunction.getCodeToInitOut(functionGenerator.getOutValue("reject"), functionGenerator.getOutValueClass("reject"));
            } else {
                return "";
            }
        }
    }

    public void generateSparkCode(final org.talend.designer.common.TransformerBase transformer, final org.talend.designer.spark.generator.SparkFunction sparkFunction) {
        if (transformer.isMultiOutput()) {
            setMultiOutput(true);
        }
        if (transformer.isUnnecessary()) {
            
    stringBuffer.append(TEXT_41);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_42);
    
            return;
        }

        if (transformer.isMultiOutput()) {
            org.talend.designer.spark.generator.SparkFunction localSparkFunction = null;
            if ((sparkFunction instanceof org.talend.designer.spark.generator.FlatMapFunction)
                    || (sparkFunction instanceof org.talend.designer.spark.generator.FlatMapToPairFunction)) {
                localSparkFunction = new org.talend.designer.spark.generator.FlatMapToPairFunction(
                        sparkFunction.isInputPair(),
                        codeGenArgument.getSparkVersion(),
                        sparkFunction.getKeyList());
            } else {
                localSparkFunction = new org.talend.designer.spark.generator.MapToPairFunction(
                        sparkFunction.isInputPair(),
                        sparkFunction.getKeyList());
            }

            org.talend.designer.spark.generator.SparkFunction extractSparkFunction = null;
            if ((sparkFunction instanceof org.talend.designer.spark.generator.FlatMapFunction)
                    || (sparkFunction instanceof org.talend.designer.spark.generator.MapFunction)) {
                extractSparkFunction = new org.talend.designer.spark.generator.MapFunction(
                        sparkFunction.isInputPair(),
                        sparkFunction.getKeyList());
            } else {
                extractSparkFunction = new org.talend.designer.spark.generator.MapToPairFunction(
                        sparkFunction.isInputPair(),
                        sparkFunction.getKeyList());
            }
            this.sparkFunction = localSparkFunction;

            // The multi-output condition is slightly different, and the
            // MapperHelper is configured with internal names for the
            // connections.
            java.util.HashMap<String, String> names = new java.util.HashMap<String, String>();
            names.put("main", transformer.getOutConnMainName());
            names.put("reject", transformer.getOutConnRejectName());

            // Refactoring FunctionGenerator to java so we have to instaniate a MO or SO here
            functionGenerator = new MOFunctionGenerator(transformer, localSparkFunction);
            functionGenerator.init(node, cid, null, transformer.getInConnName(), null, names);

            
    stringBuffer.append(TEXT_43);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_44);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_45);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_46);
    stringBuffer.append(extractSparkFunction.getCodeFunctionImplementationInputFixedType(transformer.getOutConnMainTypeName(), "Boolean", "org.apache.avro.specific.SpecificRecordBase"));
    stringBuffer.append(TEXT_47);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_48);
    stringBuffer.append(extractSparkFunction.getCodeFunctionReturnedType(transformer.getOutConnMainTypeName()));
    stringBuffer.append(TEXT_49);
    stringBuffer.append(TEXT_50);
    stringBuffer.append(transformer.getOutConnMainTypeName());
    stringBuffer.append(TEXT_51);
    stringBuffer.append(transformer.getOutConnMainName());
    stringBuffer.append(TEXT_52);
    stringBuffer.append(transformer.getOutConnMainTypeName());
    stringBuffer.append(TEXT_53);
    stringBuffer.append(extractSparkFunction.getCodeFunctionReturn(transformer.getOutConnMainName(), transformer.getOutConnMainTypeName()));
    stringBuffer.append(TEXT_54);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_55);
    stringBuffer.append(extractSparkFunction.getCodeFunctionImplementationInputFixedType(transformer.getOutConnRejectTypeName(), "Boolean", "org.apache.avro.specific.SpecificRecordBase"));
    stringBuffer.append(TEXT_56);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_57);
    stringBuffer.append(extractSparkFunction.getCodeFunctionReturnedType(transformer.getOutConnRejectTypeName()));
    stringBuffer.append(TEXT_58);
    stringBuffer.append(TEXT_59);
    stringBuffer.append(transformer.getOutConnRejectTypeName());
    stringBuffer.append(TEXT_60);
    stringBuffer.append(transformer.getOutConnRejectName());
    stringBuffer.append(TEXT_61);
    stringBuffer.append(transformer.getOutConnRejectTypeName());
    stringBuffer.append(TEXT_62);
    stringBuffer.append(extractSparkFunction.getCodeFunctionReturn(transformer.getOutConnRejectName(), transformer.getOutConnRejectTypeName()));
    stringBuffer.append(TEXT_63);
    
        } else {
            // Refactoring FunctionGenerator to java so we have to instaniate a MO or SO here
            functionGenerator = new SOFunctionGenerator(transformer, sparkFunction);

            functionGenerator.init(node, cid, null, transformer.getInConnName(), null,
                    transformer.getOutConnMainName() != null
                        ? transformer.getOutConnMainName()
                                : transformer.getOutConnRejectName());
        }
        functionGenerator.generate();
    }

    public void generateSparkConfig(final org.talend.designer.common.TransformerBase transformer, final org.talend.designer.spark.generator.SparkFunction sparkFunction) {
        if (transformer.isUnnecessary()) {
            
    stringBuffer.append(TEXT_64);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_65);
    
            return;
        }

        if (transformer.isMultiOutput()) {
            String localFunctionType = "mapToPair";
            if ((sparkFunction instanceof org.talend.designer.spark.generator.FlatMapFunction)
                    || (sparkFunction instanceof org.talend.designer.spark.generator.FlatMapToPairFunction)) {
                localFunctionType = "flatMapToPair";
            }

            String extractFunctionType = "mapToPair";
            if ((sparkFunction instanceof org.talend.designer.spark.generator.FlatMapFunction)
                    || (sparkFunction instanceof org.talend.designer.spark.generator.MapFunction)) {
                extractFunctionType = "map";
            }
            
    stringBuffer.append(TEXT_66);
    stringBuffer.append(TEXT_67);
    stringBuffer.append(sparkFunction.isStreaming() ?"org.apache.spark.streaming.api.java.JavaPairDStream":"org.apache.spark.api.java.JavaPairRDD");
    stringBuffer.append(TEXT_68);
    stringBuffer.append(transformer.getOutConnMainName());
    stringBuffer.append(TEXT_69);
    stringBuffer.append(transformer.getInConnName());
    stringBuffer.append(TEXT_70);
    stringBuffer.append(localFunctionType);
    stringBuffer.append(TEXT_71);
    stringBuffer.append(sparkFunction.getClassName(cid));
    stringBuffer.append(TEXT_72);
    stringBuffer.append(TEXT_73);
    stringBuffer.append(sparkFunction.getConfigReturnedType(transformer.getOutConnMainTypeName()));
    stringBuffer.append(TEXT_74);
    stringBuffer.append(transformer.getOutConnMainName());
    stringBuffer.append(TEXT_75);
    stringBuffer.append(transformer.getOutConnMainName());
    stringBuffer.append(TEXT_76);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_77);
    stringBuffer.append(extractFunctionType);
    stringBuffer.append(TEXT_78);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_79);
    stringBuffer.append(TEXT_80);
    stringBuffer.append(sparkFunction.getConfigReturnedType(transformer.getOutConnRejectTypeName()));
    stringBuffer.append(TEXT_81);
    stringBuffer.append(transformer.getOutConnRejectName());
    stringBuffer.append(TEXT_82);
    stringBuffer.append(transformer.getOutConnMainName());
    stringBuffer.append(TEXT_83);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_84);
    stringBuffer.append(extractFunctionType);
    stringBuffer.append(TEXT_85);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_86);
    
        } else {
            functionGenerator = new SOFunctionGenerator(transformer, sparkFunction);

            functionGenerator.init(node, cid, null, transformer.getInConnName(), null,
                    transformer.getOutConnMainName() != null
                        ? transformer.getOutConnMainName()
                                : transformer.getOutConnRejectName());
            
    stringBuffer.append(TEXT_87);
    stringBuffer.append(sparkFunction.getConfigReturnedType(transformer.getOutConnMainName() != null ? transformer.getOutConnMainTypeName() : transformer.getOutConnRejectTypeName()));
    stringBuffer.append(TEXT_88);
    stringBuffer.append(transformer.getOutConnMainName() != null ? transformer.getOutConnMainName() : transformer.getOutConnRejectName());
    stringBuffer.append(TEXT_89);
    stringBuffer.append(transformer.getInConnName());
    stringBuffer.append(TEXT_90);
    stringBuffer.append(sparkFunction.getConfigTransformation());
    stringBuffer.append(TEXT_91);
    stringBuffer.append(sparkFunction.getClassName(cid));
    stringBuffer.append(TEXT_92);
    
        }
    }
}


    
class SchemaChecker implements java.io.Serializable { //CLASS SCHEMACHECKER START
	final boolean bIsTrim = ElementParameterParser.getBooleanValue(node, "__SUB_STRING__");
	final boolean useFasteDateChecker = false;//ElementParameterParser.getBooleanValue(node, "__FAST_DATE_CHECK__");
	final boolean emptyIsNull = ElementParameterParser.getBooleanValue(node, "__EMPTY_IS_NULL__");
	final boolean allEmptyAreNull = ElementParameterParser.getBooleanValue(node, "__ALL_EMPTY_ARE_NULL__");
	
	List<Map<String, String>> list = (List<Map<String, String>>)ElementParameterParser.getObjectValue(node, "__EMPTY_NULL_TABLE__");
	boolean anotherChecked = ElementParameterParser.getBooleanValue(node, "__CHECK_ANOTHER__");
	boolean ignoreTimeZone = ElementParameterParser.getBooleanValue(node, "__IGNORE_TIMEZONE__");
	List<String> listEmptyAsNull = new java.util.ArrayList<String>();
	
	public SchemaChecker(){
	    for(Map<String, String> map : list){
			if("true".equals(map.get("EMPTY_NULL"))){
				listEmptyAsNull.add(map.get("SCHEMA_COLUMN"));
			}
		}
	}
	
	public void testBigDecimalPrecision(String _sInConnName, String colName, int iPrecision, int _maxLength, boolean isBigDecimal){

    stringBuffer.append(TEXT_93);
    stringBuffer.append(isBigDecimal?"("+_sInConnName+"."+colName+").toPlainString()":"String.valueOf("+_sInConnName+"."+colName+")" );
    stringBuffer.append(TEXT_94);
    stringBuffer.append(iPrecision);
    stringBuffer.append(TEXT_95);
    stringBuffer.append(_maxLength);
    stringBuffer.append(TEXT_96);
    stringBuffer.append(iPrecision);
    stringBuffer.append(TEXT_97);
    
	}

	public void  testDataType(boolean _bNullable, String _sInConnName, IMetadataColumn metadataColumn, String typeSelected, String cid) { //METHOD_TESTDATATYPE START
		JavaType javaType = JavaTypesManager.getJavaTypeFromId(metadataColumn.getTalendType());
		boolean isPrimitive = JavaTypesManager.isJavaPrimitiveType( javaType, metadataColumn.isNullable());
		String colName = metadataColumn.getLabel();

		if (javaType == JavaTypesManager.OBJECT || javaType == JavaTypesManager.STRING) { //CONDITION_00100 START

    stringBuffer.append(TEXT_98);
    
			if (_bNullable){ //CONDITION_00110 START

    stringBuffer.append(TEXT_99);
    stringBuffer.append(_sInConnName);
    stringBuffer.append(TEXT_100);
    stringBuffer.append(colName);
    stringBuffer.append(TEXT_101);
    
				if(allEmptyAreNull || listEmptyAsNull.contains(metadataColumn.getLabel())) {

    stringBuffer.append(TEXT_102);
    stringBuffer.append(_sInConnName);
    stringBuffer.append(TEXT_103);
    stringBuffer.append(colName);
    stringBuffer.append(TEXT_104);
    
				}
			}else if(!isPrimitive){ //CONDITION_00110 ELSE IF

    stringBuffer.append(TEXT_105);
    stringBuffer.append(_sInConnName);
    stringBuffer.append(TEXT_106);
    stringBuffer.append(colName);
    stringBuffer.append(TEXT_107);
    
			}else{ //CONDITION_00110 ELSE

    stringBuffer.append(TEXT_108);
    
			} //CONDITION_00110 STOP

    stringBuffer.append(TEXT_109);
    
			if(typeSelected.equals("Boolean") ) { //CONDITION_00120 START

    stringBuffer.append(TEXT_110);
    stringBuffer.append(_sInConnName);
    stringBuffer.append(TEXT_111);
    stringBuffer.append(colName);
    stringBuffer.append(TEXT_112);
    stringBuffer.append(_sInConnName);
    stringBuffer.append(TEXT_113);
    stringBuffer.append(colName);
    stringBuffer.append(TEXT_114);
    
			} else if(typeSelected.equals("Character")) { //CONDITION_00120 ELSE IF

    stringBuffer.append(TEXT_115);
    stringBuffer.append(_sInConnName);
    stringBuffer.append(TEXT_116);
    stringBuffer.append(colName);
    stringBuffer.append(TEXT_117);
    
			} else if(typeSelected.equals("BigDecimal")) { //CONDITION_00120 ELSE IF

    stringBuffer.append(TEXT_118);
    stringBuffer.append(typeSelected);
    stringBuffer.append(TEXT_119);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_120);
    stringBuffer.append(typeSelected);
    stringBuffer.append(TEXT_121);
    stringBuffer.append(_sInConnName);
    stringBuffer.append(TEXT_122);
    stringBuffer.append(colName);
    stringBuffer.append(TEXT_123);
    
			} else if(typeSelected.equals("Object")){ //CONDITION_00120 ELSE IF

    stringBuffer.append(TEXT_124);
    stringBuffer.append(typeSelected);
    stringBuffer.append(TEXT_125);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_126);
    stringBuffer.append(typeSelected);
    stringBuffer.append(TEXT_127);
    
			} else { //CONDITION_00120 ELSE

    stringBuffer.append(TEXT_128);
    stringBuffer.append(typeSelected);
    stringBuffer.append(TEXT_129);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_130);
    stringBuffer.append(typeSelected);
    stringBuffer.append(TEXT_131);
    stringBuffer.append(_sInConnName);
    stringBuffer.append(TEXT_132);
    stringBuffer.append(colName);
    stringBuffer.append(TEXT_133);
    
			} //CONDITION_00120 STOP

    stringBuffer.append(TEXT_134);
    
		} //CONDITION_00100 STOP
	} //METHOD_TESTDATATYPE STOP

	public void testPrecision(int _maxLength, int iPrecision, String _sInConnName, IMetadataColumn metadataColumn, String typeSelected, String cid) { //METHOD_TESTPRECISION START
		JavaType javaType = JavaTypesManager.getJavaTypeFromId(metadataColumn.getTalendType());
		String colName = metadataColumn.getLabel();
		boolean needCheck = false;
		if(anotherChecked) {
			if("BigDecimal".equalsIgnoreCase(typeSelected)) {
				needCheck = true;
			}
		} else if (javaType == JavaTypesManager.BIGDECIMAL) {
			/* NULLable, in case input value is Null, do nothing... 
			Non-NULLable, 
				(1) in case input value is Non-null, go into...; 
				(2) in case input value is Null, do nothing and warning by NULL-CHECKER.
			*/
			/*
				if precision value is not empty or Null, checking "Precision" at first, if passed then checking "Length"
			*/
			needCheck = true;
		}
		if(needCheck) { //CONDITION_00130 START

    stringBuffer.append(TEXT_135);
    stringBuffer.append(_sInConnName);
    stringBuffer.append(TEXT_136);
    stringBuffer.append(colName);
    stringBuffer.append(TEXT_137);
    
			if(javaType == JavaTypesManager.BIGDECIMAL) { //CONDITION_00131 START
				testBigDecimalPrecision(_sInConnName, colName, iPrecision, _maxLength, true);
			} else {  //CONDITION_00131 ELSE
			    testBigDecimalPrecision(_sInConnName, colName, iPrecision, _maxLength, false);
			}  //CONDITION_00131 STOP

    stringBuffer.append(TEXT_138);
    
		} //CONDITION_00130 STOP
	} //METHOD_TESTPRECISION STOP

	public void testDataLength(boolean _bNullable, String _sInConnName,IMetadataColumn inColumn, IMetadataColumn metadataColumn, int maxLength, String cid) { //METHOD_TESTDATALENGTH START
		JavaType javaType = JavaTypesManager.getJavaTypeFromId(metadataColumn.getTalendType());
		boolean isPrimitive = JavaTypesManager.isJavaPrimitiveType(javaType, metadataColumn.isNullable());
		boolean bIsStringType = (javaType == JavaTypesManager.STRING), bIsIntegerType = (javaType == JavaTypesManager.INTEGER);
		String colName = inColumn.getLabel();

		if (maxLength > 0 && ( bIsStringType || bIsIntegerType )){ //CONDITION_00140 START

    stringBuffer.append(TEXT_139);
    
			if (_bNullable){ //CONDITION_00141 START

    stringBuffer.append(TEXT_140);
    stringBuffer.append(_sInConnName);
    stringBuffer.append(TEXT_141);
    stringBuffer.append(colName);
    stringBuffer.append(TEXT_142);
    
				if(allEmptyAreNull || listEmptyAsNull.contains(metadataColumn.getLabel())) {

    stringBuffer.append(TEXT_143);
    stringBuffer.append(_sInConnName);
    stringBuffer.append(TEXT_144);
    stringBuffer.append(colName);
    stringBuffer.append(TEXT_145);
    
				}
			}else if (!isPrimitive){ //CONDITION_00141 ELSE IF

    stringBuffer.append(TEXT_146);
    stringBuffer.append(TEXT_147);
    stringBuffer.append(_sInConnName);
    stringBuffer.append(TEXT_148);
    stringBuffer.append(colName);
    stringBuffer.append(TEXT_149);
    
			}else { //CONDITION_00141 ELSE

    stringBuffer.append(TEXT_150);
    
			} //CONDITION_00141 STOP

    stringBuffer.append(TEXT_151);
    
			if ( bIsTrim ){ //CONDITION_00142 START
				if (bIsStringType) { //CONDITION_001421 START

    stringBuffer.append(TEXT_152);
    stringBuffer.append(_sInConnName);
    stringBuffer.append(TEXT_153);
    stringBuffer.append(colName);
    stringBuffer.append(TEXT_154);
    stringBuffer.append(maxLength);
    stringBuffer.append(TEXT_155);
    stringBuffer.append(_sInConnName);
    stringBuffer.append(TEXT_156);
    stringBuffer.append(colName);
    stringBuffer.append(TEXT_157);
    stringBuffer.append(_sInConnName);
    stringBuffer.append(TEXT_158);
    stringBuffer.append(colName);
    stringBuffer.append(TEXT_159);
    stringBuffer.append(maxLength);
    stringBuffer.append(TEXT_160);
    
				} else if ( bIsIntegerType ){//CONDITION_001421 ELSE IF
					String generatedType = JavaTypesManager.getTypeToGenerate(metadataColumn.getTalendType(), metadataColumn.isNullable());
					if ("int".equals(generatedType)) { //CONDITION_0014211 START

    stringBuffer.append(TEXT_161);
    stringBuffer.append(_sInConnName);
    stringBuffer.append(TEXT_162);
    stringBuffer.append(colName);
    stringBuffer.append(TEXT_163);
    
					} else{ //CONDITION_0014211 ELSE

    stringBuffer.append(TEXT_164);
    stringBuffer.append(_sInConnName);
    stringBuffer.append(TEXT_165);
    stringBuffer.append(colName);
    stringBuffer.append(TEXT_166);
    
					} //CONDITION_0014211 STOP

    stringBuffer.append(TEXT_167);
    stringBuffer.append(maxLength);
    stringBuffer.append(TEXT_168);
    stringBuffer.append(_sInConnName);
    stringBuffer.append(TEXT_169);
    stringBuffer.append(colName);
    stringBuffer.append(TEXT_170);
    stringBuffer.append(_sInConnName);
    stringBuffer.append(TEXT_171);
    stringBuffer.append(colName);
    stringBuffer.append(TEXT_172);
    stringBuffer.append(maxLength);
    stringBuffer.append(TEXT_173);
    
				} //CONDITION_001421 STOP
			} else{ //CONDITION_00142 ELSE
				if (bIsStringType) { //CONDITION_001422 START

    stringBuffer.append(TEXT_174);
    stringBuffer.append(_sInConnName);
    stringBuffer.append(TEXT_175);
    stringBuffer.append(colName);
    stringBuffer.append(TEXT_176);
    stringBuffer.append(maxLength);
    stringBuffer.append(TEXT_177);
    
				} else if (bIsIntegerType) { //CONDITION_001422 ELSE IF
					String generatedType = JavaTypesManager.getTypeToGenerate(metadataColumn.getTalendType(), metadataColumn.isNullable());
					if ("int".equals(generatedType)) { //CONDITION_0014221 START

    stringBuffer.append(TEXT_178);
    stringBuffer.append(_sInConnName);
    stringBuffer.append(TEXT_179);
    stringBuffer.append(colName);
    stringBuffer.append(TEXT_180);
    
					} else { //CONDITION_0014221 ELSE

    stringBuffer.append(TEXT_181);
    stringBuffer.append(_sInConnName);
    stringBuffer.append(TEXT_182);
    stringBuffer.append(colName);
    stringBuffer.append(TEXT_183);
    
					} //CONDITION_0014221 STOP

    stringBuffer.append(TEXT_184);
    stringBuffer.append(maxLength);
    stringBuffer.append(TEXT_185);
    
				}//CONDITION_001422 STOP
			} //CONDITION_00142 STOP

    stringBuffer.append(TEXT_186);
     
		} //CONDITION_00140 STOP
	} //METHOD_TESTDATALENGTH STOP

	public void testDate(boolean _bNullable, String _sInConnName, IMetadataColumn metadataColumn, String pattern, String cid) { //METHOD_TESTDATE START
		JavaType javaType = JavaTypesManager.getJavaTypeFromId(metadataColumn.getTalendType());
		boolean isPrimitive = JavaTypesManager.isJavaPrimitiveType( javaType, metadataColumn.isNullable());
		String colName = metadataColumn.getLabel();

		if ("".equals(pattern)){ //CONDITION_00150 START

    stringBuffer.append(TEXT_187);
    
		} else { //CONDITION_00150 ELSE
			if (javaType == JavaTypesManager.OBJECT || javaType == JavaTypesManager.STRING) { //CONDITION_00151 START

    stringBuffer.append(TEXT_188);
    
				if (_bNullable){ //CONDITION_001511 START

    stringBuffer.append(TEXT_189);
    stringBuffer.append(_sInConnName);
    stringBuffer.append(TEXT_190);
    stringBuffer.append(colName);
    stringBuffer.append(TEXT_191);
    
					if(allEmptyAreNull || listEmptyAsNull.contains(metadataColumn.getLabel())) {

    stringBuffer.append(TEXT_192);
    stringBuffer.append(_sInConnName);
    stringBuffer.append(TEXT_193);
    stringBuffer.append(colName);
    stringBuffer.append(TEXT_194);
    
					}
				}else if (!isPrimitive){ //CONDITION_001511 ELSE IF

    stringBuffer.append(TEXT_195);
    stringBuffer.append(_sInConnName);
    stringBuffer.append(TEXT_196);
    stringBuffer.append(colName);
    stringBuffer.append(TEXT_197);
    
				}else { //CONDITION_001511 ELSE

    stringBuffer.append(TEXT_198);
    
				} //CONDITION_001511 STOP

    stringBuffer.append(TEXT_199);
    
				if (!useFasteDateChecker) { //CONDITION_001512 START

    stringBuffer.append(TEXT_200);
    stringBuffer.append(_sInConnName);
    stringBuffer.append(TEXT_201);
    stringBuffer.append(colName);
    stringBuffer.append(TEXT_202);
    stringBuffer.append(pattern);
    stringBuffer.append(TEXT_203);
    if(ignoreTimeZone){
    stringBuffer.append(TEXT_204);
    }else{
    stringBuffer.append(TEXT_205);
    }
    stringBuffer.append(TEXT_206);
    
				} else { //CONDITION_001512 ELSE

    stringBuffer.append(TEXT_207);
    stringBuffer.append(pattern);
    stringBuffer.append(TEXT_208);
    stringBuffer.append(_sInConnName);
    stringBuffer.append(TEXT_209);
    stringBuffer.append(colName);
    stringBuffer.append(TEXT_210);
    
				} //CONDITION_001512 STOP

    stringBuffer.append(TEXT_211);
    
			// date type need check also (some inputting data not legal, beacause original data is not suite with pattern and has be converted)
			} else if (javaType == JavaTypesManager.DATE){ //CONDITION_00151 ELSE IF
				if (!metadataColumn.getPattern().equals(pattern)){ //CONDITION_001513 START

    stringBuffer.append(TEXT_212);
    
				} //CONDITION_001513 STOP
			} else{ //CONDITION_00151 ELSE

    stringBuffer.append(TEXT_213);
    
			} //CONDITION_00151 STOP
		} //CONDITION_00150 STOP
	} //METHOD_TESTDATE STOP

	public void testNull(String _sInConnName, IMetadataColumn metadataColumn, String cid){ //METHOD_TESTNULL START
		boolean isPrimitive = JavaTypesManager.isJavaPrimitiveType(metadataColumn.getTalendType(), metadataColumn.isNullable());
		if (!isPrimitive){ //CONDITION_00160 START
			if(emptyIsNull && !allEmptyAreNull){ //CONDITION_001601 START - for the migration task
				if(listEmptyAsNull.contains(metadataColumn.getLabel())){ //CONDITION_0016011 START

    stringBuffer.append(TEXT_214);
    stringBuffer.append(_sInConnName);
    stringBuffer.append(TEXT_215);
    stringBuffer.append(metadataColumn.getLabel());
    stringBuffer.append(TEXT_216);
    stringBuffer.append(_sInConnName);
    stringBuffer.append(TEXT_217);
    stringBuffer.append(metadataColumn.getLabel());
    stringBuffer.append(TEXT_218);
    
				}else{ //CONDITION_0016011 ELSE

    stringBuffer.append(TEXT_219);
    stringBuffer.append(_sInConnName);
    stringBuffer.append(TEXT_220);
    stringBuffer.append(metadataColumn.getLabel());
    stringBuffer.append(TEXT_221);
    
				} //CONDITION_0016011 STOP
			}else{ //CONDITION_001601 ELSE
				if(allEmptyAreNull){ //CONDITION_0016012 START

    stringBuffer.append(TEXT_222);
    stringBuffer.append(_sInConnName);
    stringBuffer.append(TEXT_223);
    stringBuffer.append(metadataColumn.getLabel());
    stringBuffer.append(TEXT_224);
    stringBuffer.append(_sInConnName);
    stringBuffer.append(TEXT_225);
    stringBuffer.append(metadataColumn.getLabel());
    stringBuffer.append(TEXT_226);
    
				}else if(listEmptyAsNull.contains(metadataColumn.getLabel())){ //CONDITION_0016012 ELSE IF

    stringBuffer.append(TEXT_227);
    stringBuffer.append(_sInConnName);
    stringBuffer.append(TEXT_228);
    stringBuffer.append(metadataColumn.getLabel());
    stringBuffer.append(TEXT_229);
    stringBuffer.append(_sInConnName);
    stringBuffer.append(TEXT_230);
    stringBuffer.append(metadataColumn.getLabel());
    stringBuffer.append(TEXT_231);
    
				}else{ //CONDITION_0016012 ELSE

    stringBuffer.append(TEXT_232);
    stringBuffer.append(_sInConnName);
    stringBuffer.append(TEXT_233);
    stringBuffer.append(metadataColumn.getLabel());
    stringBuffer.append(TEXT_234);
    
				} //CONDITION_0016012 STOP
			} //CONDITION_001601 STOP

    stringBuffer.append(TEXT_235);
    
		} //CONDITION_00160 STOP
	} //METHOD_TESTNULL STOP
} //CLASS SCHEMACHECKER STOP

    

/**
 *
 * The following imports must occur before importing this file:
 * @ include file="@{org.talend.designer.components.mrprovider}/resources/utils/common_codegen_util.javajet"
 */
class TSchemaComplianceCheckUtil extends org.talend.designer.common.TransformerBase {
     
     final private static String REJECT_MSG = "errorMessage";
     final private static String REJECT_CODE = "errorCode";
     // TODO: these is unused --kept here for consistency with other rejectable
     // transformers
     final private static String REJECT_FIELD = "";
             
     /** The outgoing connection contains the column {@link #REJECT_FIELD}.
      *  If we import a job from a DI job, this column will be missing */
     private Boolean containsRejectField = false;
     
     private INode node;
     
     public TSchemaComplianceCheckUtil(INode node,
             org.talend.designer.common.BigDataCodeGeneratorArgument argument,
             org.talend.designer.common.CommonRowTransformUtil rowTransformUtil) {
         super(node, argument, rowTransformUtil, "FLOW", "REJECT");
         
         // TODO: always false, kept for consistency
         containsRejectField = hasOutputColumn(true, REJECT_FIELD);
         this.node = node;
	}
     
     /**
      * Check for empty values and replace with null if the option is chosen.
      */
    public String generateNullReplacement(boolean emptyToNull, String preLabel, String outLabel, String inType, String outType){

        if (emptyToNull && ("String".equals(inType) || "Object".equals(inType))) {
            return "if (\"\".equals(" + getRowTransform().getCodeToGetInField(preLabel) + ")){\n" + getRowTransform().getCodeToGetInField(preLabel) + "= null;" + "\n}";
        }else{
            return "";
        }
    }
     
    /**
     * Second case when it is custom rules but still on the source row schema
     */
    public void generateComplianceCheck(
            IMetadataColumn inColumn,
            Map<String, String> checkedColumn
    ){
        Object pre_maxLength = inColumn.getLength();
   		int maxLength = (pre_maxLength == null) ? 0 : Integer.parseInt(pre_maxLength.toString());
   		
		generateComplianceCheck(
		        inColumn,
		        checkedColumn.get("SCHEMA_COLUMN"),
		        checkedColumn.get("SELECTED_TYPE"),
		        checkedColumn.get("DATEPATTERN"),
		        "true".equals(checkedColumn.get("NULLABLE")),
		        "true".equals(checkedColumn.get("MAX_LENGTH")),
		        maxLength,
		        null,
		        inColumn
        );
	}
    
    public void generateComplianceCheck(
            IMetadataColumn inColumn,
            String sTestColName,
    		String sTestColType,
    		String sTestColPattern,
    		boolean bNullable,
    		boolean bMaxLenLimited,
    		int maxLength,
    		Object pre_iPrecision,
    		IMetadataColumn schemaColumn
    ){
        List<? extends IConnection> listInConns = node.getIncomingConnections();
		String sInConnName = null;
		IConnection inConn = null;
		
		if (listInConns != null && listInConns.size() > 0) {
			IConnection inConnTemp = listInConns.get(0);
			sInConnName = inConnTemp.getName();
		}
		

    stringBuffer.append(TEXT_236);
    	
		
		SchemaChecker checker = new SchemaChecker();
        
     	// NULL checking
   		if (!bNullable){
   			checker.testNull(sInConnName, inColumn, cid);
   		}

   		// type checking
   		if (sTestColType != null){
   			if (sTestColType.indexOf("Date") >= 0){
   				checker.testDate(bNullable, sInConnName, inColumn, sTestColPattern, cid); 
   			} else{
   				checker.testDataType(bNullable, sInConnName, inColumn, sTestColType, cid);
   			}
   		}
   		

   		// length checking
   		if (bMaxLenLimited){
   			checker.testDataLength(bNullable, sInConnName,inColumn, schemaColumn, maxLength, cid);
   		}

   		
   		// precision checking
   		if (pre_iPrecision != null){
   			checker.testPrecision(maxLength, Integer.parseInt(pre_iPrecision.toString()), sInConnName, inColumn, sTestColType, cid);
   		}
	}
    
    /**
     * First or Last case when the component schema or custom schema is used
     */
    public void generateComplianceCheck(IMetadataColumn inColumn, IMetadataColumn schemaColumn, boolean useless){
   		Object pre_maxLength = inColumn.getLength();
   		int maxLength = (pre_maxLength == null) ? 0 : Integer.parseInt(pre_maxLength.toString());
   		
		generateComplianceCheck(
		        inColumn,
		        schemaColumn.getLabel(),
		        JavaTypesManager.getTypeToGenerate(schemaColumn.getTalendType(), true),
		        schemaColumn.getPattern(),
		        schemaColumn.isNullable(),
		        true,
		        maxLength,
		        schemaColumn.getPrecision(),
		        schemaColumn
        );
	}
     

     /**
      * Generates code that performs the tranformation from one input string
      * to many output strings, or to a reject flow. The boolean parameter is
      * used to define whether the transform method return type is void or something
      * else.
      *
      * DI sample output :
      *    MainOutput no exception : col1|col2|col3...
      *    MainOutput with exception : No Output (Error output with Exception msg)
      *    Reject Output : col1|col2...|colx|Empty Col where exception occured|coly...|Error Code|Error msg
      */
     public void generateTransform(boolean hasAReturnedType) {

    stringBuffer.append(TEXT_237);
    

		Iterable<IMetadataColumn> srcColumns = getInColumns();
		Iterable<IMetadataColumn> outColumns = getOutColumnsMain() != null ? getOutColumnsMain() : getOutColumnsReject();
		boolean checkAll = ElementParameterParser.getBooleanValue(node, "__CHECK_ALL__");
		boolean checkAnother = ElementParameterParser.getBooleanValue(node, "__CHECK_ANOTHER__");

		if(checkAnother){ // Using another custom schema
		    if (node.getMetadataFromConnector("OTHER") != null)
		        srcColumns = node.getMetadataFromConnector("OTHER").getListColumns();
		}

        for (IMetadataColumn col : outColumns){
            String outLabel = col.getLabel();
            String outPattern = col.getPattern();
            String outTypeWhole = JavaTypesManager.getTypeToGenerate(col.getTalendType(), col.isNullable());
            String outType = outTypeWhole.contains(".") ? outTypeWhole.substring(outTypeWhole.lastIndexOf(".") + 1) : outTypeWhole;
            if (("byte[]").equals(outType)){
                outType = "byteArray";
            }
            
            int colIndex = 0;
            for (IMetadataColumn preCol : srcColumns){
                String preLabel = preCol.getLabel();
                if (preLabel.equals(outLabel)){
					if(checkAll || checkAnother){
					    generateComplianceCheck(preCol, node.getMetadataList().get(0).getListColumns().get(colIndex), false);
					} else{
					    List<Map<String, String>> listCheckedColumns = (List<Map<String, String>>)ElementParameterParser.getObjectValue(node, "__CHECKCOLS__");
					    
					    generateComplianceCheck(preCol, listCheckedColumns.get(colIndex));
					}

    stringBuffer.append(TEXT_238);
    stringBuffer.append(getRowTransform().getCodeToSetOutField(outLabel,getRowTransform().getCodeToGetInField(preLabel)));
    stringBuffer.append(TEXT_239);
    stringBuffer.append(preLabel );
    stringBuffer.append(TEXT_240);
    stringBuffer.append(preLabel );
    stringBuffer.append(TEXT_241);
    
				}
                colIndex++;
            }
        }

    stringBuffer.append(TEXT_242);
    
			generateTransformReject(false, "errorCodeThroughStr", "rowErrorMessageThrough");

    stringBuffer.append(TEXT_243);
    
			if(null != getOutConnMain()) {
			    Iterable<IMetadataColumn> empty = new java.util.ArrayList<IMetadataColumn>();

    stringBuffer.append(TEXT_244);
    stringBuffer.append(getRowTransform().getCodeToAddToOutput(null == getOutConnMain(), empty));
    
			}

    stringBuffer.append(TEXT_245);
    
     }
      
	/**
     * Generates code in the transform context to create reject output.
     *
     * @param die if this reject output should kill the job.  Normally, this is
     *    tied to a dieOnError parameter for the component, but can be
     *    explicitly set to false for non-fatal reject output.
     * @param codeException a variable in the transform scope that contains the
     *    variable with an exception.  If null, this will be constructed from
     *    the codeRejectMsg.
     * @param codeRejectMsg the error message to output with the reject output.
     */
    private void generateTransformReject(boolean die, String codeException, String codeRejectMsg) {
		 // If there are multiple outputs, then copy all of the new columns from
		 // the original output to the error output.
		if (isMultiOutput()) {
		    Iterable<IMetadataColumn> empty = new java.util.ArrayList<IMetadataColumn>();

    stringBuffer.append(TEXT_246);
    stringBuffer.append(getRowTransform().getCodeToSetOutField(true, REJECT_MSG, codeRejectMsg) );
    stringBuffer.append(TEXT_247);
    stringBuffer.append(getRowTransform().getCodeToSetOutField(true, REJECT_CODE, codeException) );
    stringBuffer.append(TEXT_248);
    stringBuffer.append(getRowTransform().getCodeToCopyOutMainToReject(getOutColumnsMain()));
    stringBuffer.append(TEXT_249);
    stringBuffer.append(getRowTransform().getCodeToAddToOutput(true, empty));
    
		}
    }
}

    

org.talend.designer.spark.generator.SparkFunction sparkFunction = null;
String requiredInputType = bigDataNode.getRequiredInputType();
String requiredOutputType = bigDataNode.getRequiredOutputType();
String incomingType = bigDataNode.getIncomingType();
String outgoingType = bigDataNode.getOutgoingType();
boolean inputIsPair = requiredInputType != null ? "KEYVALUE".equals(requiredInputType) : "KEYVALUE".equals(incomingType);

String type = requiredOutputType == null ? outgoingType : requiredOutputType;
if("KEYVALUE".equals(type)) {
    sparkFunction = new org.talend.designer.spark.generator.FlatMapToPairFunction(inputIsPair, codeGenArgument.getSparkVersion());
} else if("VALUE".equals(type)) {
    sparkFunction = new org.talend.designer.spark.generator.FlatMapFunction(inputIsPair, codeGenArgument.getSparkVersion());
}
boolean isNodeInBatchMode = org.talend.designer.common.tmap.LookupUtil.isNodeInBatchMode(node);
if("SPARKSTREAMING".equals(node.getComponent().getType()) && !isNodeInBatchMode) {
    sparkFunction.setStreaming(true);
}

final SparkRowTransformUtil sparkTransformUtil = new SparkRowTransformUtil(sparkFunction);
final TSchemaComplianceCheckUtil tSchemaComplianceCheckUtil = new TSchemaComplianceCheckUtil(node, codeGenArgument, sparkTransformUtil);

sparkTransformUtil.generateSparkConfig(tSchemaComplianceCheckUtil, sparkFunction);


    return stringBuffer.toString();
  }
}
