package org.talend.designer.codegen.translators.databases.kudu;

import java.util.List;
import org.talend.core.model.metadata.IMetadataColumn;
import org.talend.core.model.metadata.IMetadataTable;
import org.talend.core.model.metadata.types.JavaType;
import org.talend.core.model.metadata.types.JavaTypesManager;
import org.talend.core.model.process.EConnectionType;
import org.talend.core.model.process.ElementParameterParser;
import org.talend.core.model.process.IConnection;
import org.talend.core.model.process.IConnectionCategory;
import org.talend.core.model.process.INode;
import org.talend.designer.common.BigDataCodeGeneratorArgument;
import org.talend.designer.common.tkuduoutput.TKuduOutputUtil;

public class TKuduOutputSparkconfigJava
{
  protected static String nl;
  public static synchronized TKuduOutputSparkconfigJava create(String lineSeparator)
  {
    nl = lineSeparator;
    TKuduOutputSparkconfigJava result = new TKuduOutputSparkconfigJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "java.util.StringJoiner stringJoiner = new java.util.StringJoiner(\",\");";
  protected final String TEXT_2 = NL + "\tstringJoiner.add(String.format(\"%s:%s\", ";
  protected final String TEXT_3 = ", ";
  protected final String TEXT_4 = "));";
  protected final String TEXT_5 = NL + "final String masters_";
  protected final String TEXT_6 = " = stringJoiner.toString();" + NL + "org.apache.kudu.spark.kudu.KuduContext kuduCtx_";
  protected final String TEXT_7 = " = new org.apache.kudu.spark.kudu.KuduContext(masters_";
  protected final String TEXT_8 = ", ctx.sc());" + NL + "org.apache.spark.sql.SQLContext sqlCtx_";
  protected final String TEXT_9 = " = new org.apache.spark.sql.SQLContext(ctx);";
  protected final String TEXT_10 = NL + "        \t" + NL + "        \tjava.util.List<String> strKuduKeysList_";
  protected final String TEXT_11 = " = new java.util.ArrayList<String>();" + NL + "        \torg.apache.spark.sql.types.StructType types_";
  protected final String TEXT_12 = " = new org.apache.spark.sql.types.StructType();" + NL + "        \t" + NL + "        \torg.apache.spark.sql.types.StructType st_";
  protected final String TEXT_13 = " = (org.apache.spark.sql.types.StructType) org.apache.spark.sql.catalyst.JavaTypeInference.inferDataType(";
  protected final String TEXT_14 = ".class)._1();" + NL + "        \torg.apache.spark.sql.types.StructField[] fields_";
  protected final String TEXT_15 = " = st_";
  protected final String TEXT_16 = ".fields();" + NL + "        \t" + NL + "        \tjava.util.List<org.apache.spark.sql.types.StructField> fieldsList = new java.util.ArrayList<org.apache.spark.sql.types.StructField>();" + NL + "        \torg.apache.spark.sql.types.StructField[] fieldsArray = new org.apache.spark.sql.types.StructField[fieldsList.size()];" + NL + "        \t" + NL + "        \tjava.util.ArrayList<org.apache.kudu.ColumnSchema> columns = new java.util.ArrayList<>();" + NL + "\t\t\t" + NL + "        \t";
  protected final String TEXT_17 = NL + "\t\t\t\t\tstrKuduKeysList_";
  protected final String TEXT_18 = ".add(\"";
  protected final String TEXT_19 = "\");" + NL + "\t\t\t\t";
  protected final String TEXT_20 = NL + "\t\t\t\t\t\tcolumns.add(new org.apache.kudu.ColumnSchema.ColumnSchemaBuilder(\"";
  protected final String TEXT_21 = "\", org.apache.kudu.Type.UNIXTIME_MICROS).key(true).build());" + NL + "\t\t\t\t\t";
  protected final String TEXT_22 = NL + "\t\t\t\t\ttypes_";
  protected final String TEXT_23 = " = types_";
  protected final String TEXT_24 = ".add(\"";
  protected final String TEXT_25 = "\", org.apache.spark.sql.types.DataTypes.TimestampType);" + NL + "\t\t\t\t\tfieldsList.add(getStructField_";
  protected final String TEXT_26 = "(fields_";
  protected final String TEXT_27 = ", \"";
  protected final String TEXT_28 = "\", org.apache.spark.sql.types.DataTypes.TimestampType));" + NL + "\t\t\t\t";
  protected final String TEXT_29 = NL + "\t\t\t\t\t\tcolumns.add(new org.apache.kudu.ColumnSchema.ColumnSchemaBuilder(\"";
  protected final String TEXT_30 = "\", org.apache.kudu.Type.STRING).key(true).build());" + NL + "\t\t\t\t\t";
  protected final String TEXT_31 = NL + "\t\t\t\t\ttypes_";
  protected final String TEXT_32 = " = types_";
  protected final String TEXT_33 = ".add(\"";
  protected final String TEXT_34 = "\", org.apache.spark.sql.types.DataTypes.StringType);" + NL + "\t\t\t\t\tfieldsList.add(getStructField_";
  protected final String TEXT_35 = "(fields_";
  protected final String TEXT_36 = ", \"";
  protected final String TEXT_37 = "\", org.apache.spark.sql.types.DataTypes.StringType));" + NL + "\t\t\t\t";
  protected final String TEXT_38 = NL + "\t\t\t\t\t\tcolumns.add(new org.apache.kudu.ColumnSchema.ColumnSchemaBuilder(\"";
  protected final String TEXT_39 = "\", org.apache.kudu.Type.INT8).key(true).build());" + NL + "\t\t\t        ";
  protected final String TEXT_40 = NL + "\t\t\t\t\ttypes_";
  protected final String TEXT_41 = " = types_";
  protected final String TEXT_42 = ".add(\"";
  protected final String TEXT_43 = "\", org.apache.spark.sql.types.DataTypes.ShortType);" + NL + "\t\t\t\t\tfieldsList.add(getStructField_";
  protected final String TEXT_44 = "(fields_";
  protected final String TEXT_45 = ", \"";
  protected final String TEXT_46 = "\", org.apache.spark.sql.types.DataTypes.ShortType));" + NL + "\t\t\t\t";
  protected final String TEXT_47 = NL + "\t\t\t\t\t\tcolumns.add(new org.apache.kudu.ColumnSchema.ColumnSchemaBuilder(\"";
  protected final String TEXT_48 = "\", org.apache.kudu.Type.DOUBLE).key(true).build());" + NL + "\t\t\t\t\t";
  protected final String TEXT_49 = NL + "\t\t\t\t\ttypes_";
  protected final String TEXT_50 = " = types_";
  protected final String TEXT_51 = ".add(\"";
  protected final String TEXT_52 = "\", org.apache.spark.sql.types.DataTypes.DoubleType);" + NL + "\t\t\t\t\tfieldsList.add(getStructField_";
  protected final String TEXT_53 = "(fields_";
  protected final String TEXT_54 = ", \"";
  protected final String TEXT_55 = "\", org.apache.spark.sql.types.DataTypes.DoubleType));" + NL + "\t\t\t\t";
  protected final String TEXT_56 = NL + "\t\t\t\t\t\tcolumns.add(new org.apache.kudu.ColumnSchema.ColumnSchemaBuilder(\"";
  protected final String TEXT_57 = "\", org.apache.kudu.Type.FLOAT).key(true).build());" + NL + "\t\t\t\t\t";
  protected final String TEXT_58 = NL + "\t\t\t\t\ttypes_";
  protected final String TEXT_59 = " = types_";
  protected final String TEXT_60 = ".add(\"";
  protected final String TEXT_61 = "\", org.apache.spark.sql.types.DataTypes.FloatType);" + NL + "\t\t\t\t\tfieldsList.add(getStructField_";
  protected final String TEXT_62 = "(fields_";
  protected final String TEXT_63 = ", \"";
  protected final String TEXT_64 = "\", org.apache.spark.sql.types.DataTypes.FloatType));" + NL + "\t\t\t\t";
  protected final String TEXT_65 = NL + "\t\t\t\t\t\tcolumns.add(new org.apache.kudu.ColumnSchema.ColumnSchemaBuilder(\"";
  protected final String TEXT_66 = "\", org.apache.kudu.Type.BOOL).key(true).build());" + NL + "\t\t\t\t\t";
  protected final String TEXT_67 = NL + "\t\t\t\t\ttypes_";
  protected final String TEXT_68 = " = types_";
  protected final String TEXT_69 = ".add(\"";
  protected final String TEXT_70 = "\", org.apache.spark.sql.types.DataTypes.BooleanType);" + NL + "\t\t\t\t\tfieldsList.add(getStructField_";
  protected final String TEXT_71 = "(fields_";
  protected final String TEXT_72 = ", \"";
  protected final String TEXT_73 = "\", org.apache.spark.sql.types.DataTypes.BooleanType));" + NL + "\t\t\t\t";
  protected final String TEXT_74 = NL + "\t\t\t\t\t\tcolumns.add(new org.apache.kudu.ColumnSchema.ColumnSchemaBuilder(\"";
  protected final String TEXT_75 = "\", org.apache.kudu.Type.BINARY).key(true).build());" + NL + "\t\t\t\t\t";
  protected final String TEXT_76 = NL + "\t\t\t\t\ttypes_";
  protected final String TEXT_77 = " = types_";
  protected final String TEXT_78 = ".add(\"";
  protected final String TEXT_79 = "\", org.apache.spark.sql.types.DataTypes.ByteType);" + NL + "\t\t\t\t\tfieldsList.add(getStructField_";
  protected final String TEXT_80 = "(fields_";
  protected final String TEXT_81 = ", \"";
  protected final String TEXT_82 = "\", org.apache.spark.sql.types.DataTypes.ByteType));" + NL + "\t\t\t\t";
  protected final String TEXT_83 = NL + "\t\t\t\t\t\tcolumns.add(new org.apache.kudu.ColumnSchema.ColumnSchemaBuilder(\"";
  protected final String TEXT_84 = "\", org.apache.kudu.Type.DOUBLE).key(true).build());" + NL + "\t\t\t\t\t";
  protected final String TEXT_85 = NL + "\t\t\t\t\t\tInteger precision_";
  protected final String TEXT_86 = "_";
  protected final String TEXT_87 = " = ";
  protected final String TEXT_88 = ";" + NL + "\t\t\t\t\t\tif(precision_";
  protected final String TEXT_89 = "_";
  protected final String TEXT_90 = " > org.apache.spark.sql.types.DecimalType.MAX_PRECISION()) {" + NL + "\t\t\t\t\t\t\t";
  protected final String TEXT_91 = NL + "\t\t\t\t\t\t\t\tlog.warn(\"Decimal precision (\" + precision_";
  protected final String TEXT_92 = "_";
  protected final String TEXT_93 = " + \") cannot be greater than MAX_PRECISION (\" + org.apache.spark.sql.types.DecimalType.MAX_PRECISION() + \"). Decimal precision is set to (\" + org.apache.spark.sql.types.DecimalType.MAX_PRECISION() + \").\");" + NL + "\t\t\t\t\t\t\t";
  protected final String TEXT_94 = NL + "\t\t\t\t\t\t\tprecision_";
  protected final String TEXT_95 = "_";
  protected final String TEXT_96 = " = org.apache.spark.sql.types.DecimalType.MAX_PRECISION();" + NL + "\t\t\t\t\t\t}" + NL + "\t\t\t\t\t\tInteger scale_";
  protected final String TEXT_97 = "_";
  protected final String TEXT_98 = " = ";
  protected final String TEXT_99 = ";" + NL + "\t\t\t\t\t\tif(scale_";
  protected final String TEXT_100 = "_";
  protected final String TEXT_101 = " != null) {" + NL + "\t\t\t\t\t\t\tif(scale_";
  protected final String TEXT_102 = "_";
  protected final String TEXT_103 = " > precision_";
  protected final String TEXT_104 = "_";
  protected final String TEXT_105 = ") {" + NL + "\t\t\t\t\t\t\t\t";
  protected final String TEXT_106 = NL + "\t\t\t\t\t\t\t\t\tlog.warn(\"Decimal scale (\" + scale_";
  protected final String TEXT_107 = "_";
  protected final String TEXT_108 = " + \") cannot be greater than precision (\" + precision_";
  protected final String TEXT_109 = "_";
  protected final String TEXT_110 = " + \"). Decimal scale is set to (\" + precision_";
  protected final String TEXT_111 = "_";
  protected final String TEXT_112 = " + \").\");" + NL + "\t\t\t\t\t\t\t\t";
  protected final String TEXT_113 = NL + "\t\t\t\t\t\t\t\tscale_";
  protected final String TEXT_114 = "_";
  protected final String TEXT_115 = " = precision_";
  protected final String TEXT_116 = "_";
  protected final String TEXT_117 = ";" + NL + "\t\t\t\t\t\t\t}" + NL + "\t\t\t\t\t\t\ttypes_";
  protected final String TEXT_118 = " = types_";
  protected final String TEXT_119 = ".add(\"";
  protected final String TEXT_120 = "\", org.apache.spark.sql.types.DataTypes.createDecimalType(precision_";
  protected final String TEXT_121 = "_";
  protected final String TEXT_122 = ", scale_";
  protected final String TEXT_123 = "_";
  protected final String TEXT_124 = "));" + NL + "\t\t\t\t\t\t\tfieldsList.add(getStructField_";
  protected final String TEXT_125 = "(fields_";
  protected final String TEXT_126 = ", \"";
  protected final String TEXT_127 = "\", org.apache.spark.sql.types.DataTypes.createDecimalType(precision_";
  protected final String TEXT_128 = "_";
  protected final String TEXT_129 = ", scale_";
  protected final String TEXT_130 = "_";
  protected final String TEXT_131 = ")));" + NL + "\t\t\t\t\t\t} else {" + NL + "\t\t\t\t\t\t\tscale_";
  protected final String TEXT_132 = "_";
  protected final String TEXT_133 = " = (";
  protected final String TEXT_134 = " > precision_";
  protected final String TEXT_135 = "_";
  protected final String TEXT_136 = ") ? precision_";
  protected final String TEXT_137 = "_";
  protected final String TEXT_138 = " : ";
  protected final String TEXT_139 = ";" + NL + "\t\t\t\t\t\t\t";
  protected final String TEXT_140 = NL + "\t\t\t\t\t\t\t\tlog.warn(\"Decimal scale is set to (\" + scale_";
  protected final String TEXT_141 = "_";
  protected final String TEXT_142 = " + \").\");" + NL + "\t\t\t\t\t\t\t";
  protected final String TEXT_143 = NL + "\t\t\t\t\t\t\ttypes_";
  protected final String TEXT_144 = " = types_";
  protected final String TEXT_145 = ".add(\"";
  protected final String TEXT_146 = "\", org.apache.spark.sql.types.DataTypes.createDecimalType(precision_";
  protected final String TEXT_147 = "_";
  protected final String TEXT_148 = ", scale_";
  protected final String TEXT_149 = "_";
  protected final String TEXT_150 = "));" + NL + "\t\t\t\t\t\t\tfieldsList.add(getStructField_";
  protected final String TEXT_151 = "(fields_";
  protected final String TEXT_152 = ", \"";
  protected final String TEXT_153 = "\", org.apache.spark.sql.types.DataTypes.createDecimalType(precision_";
  protected final String TEXT_154 = "_";
  protected final String TEXT_155 = ", scale_";
  protected final String TEXT_156 = "_";
  protected final String TEXT_157 = ")));" + NL + "\t\t\t\t\t\t}" + NL + "\t\t\t\t\t";
  protected final String TEXT_158 = NL + "\t\t\t\t\t\tInteger scale_";
  protected final String TEXT_159 = "_";
  protected final String TEXT_160 = " = ";
  protected final String TEXT_161 = ";" + NL + "\t\t\t\t\t\tif(scale_";
  protected final String TEXT_162 = "_";
  protected final String TEXT_163 = " != null) {" + NL + "\t\t\t\t\t\t\tif(scale_";
  protected final String TEXT_164 = "_";
  protected final String TEXT_165 = " > org.apache.spark.sql.types.DecimalType.MAX_SCALE()) {" + NL + "\t\t\t\t\t\t\t\t";
  protected final String TEXT_166 = NL + "\t\t\t\t\t\t\t\t\tlog.warn(\"Decimal scale (\" + scale_";
  protected final String TEXT_167 = "_";
  protected final String TEXT_168 = " + \") cannot be greater than MAX_SCALE (\" + org.apache.spark.sql.types.DecimalType.MAX_SCALE() + \"). Decimal scale is set to (\" + org.apache.spark.sql.types.DecimalType.MAX_SCALE() + \").\");" + NL + "\t\t\t\t\t\t\t\t";
  protected final String TEXT_169 = NL + "\t\t\t\t\t\t\t\tscale_";
  protected final String TEXT_170 = "_";
  protected final String TEXT_171 = " = org.apache.spark.sql.types.DecimalType.MAX_SCALE();" + NL + "\t\t\t\t\t\t\t}" + NL + "\t\t\t\t\t\t\ttypes_";
  protected final String TEXT_172 = " = types_";
  protected final String TEXT_173 = ".add(\"";
  protected final String TEXT_174 = "\", org.apache.spark.sql.types.DataTypes.createDecimalType(org.apache.spark.sql.types.DecimalType.MAX_PRECISION(), scale_";
  protected final String TEXT_175 = "_";
  protected final String TEXT_176 = "));" + NL + "\t\t\t\t\t\t\tfieldsList.add(getStructField_";
  protected final String TEXT_177 = "(fields_";
  protected final String TEXT_178 = ", \"";
  protected final String TEXT_179 = "\", org.apache.spark.sql.types.DataTypes.createDecimalType(org.apache.spark.sql.types.DecimalType.MAX_PRECISION(), scale_";
  protected final String TEXT_180 = "_";
  protected final String TEXT_181 = ")));" + NL + "\t\t\t\t\t\t} else {" + NL + "\t\t\t\t\t\t\t";
  protected final String TEXT_182 = NL + "\t\t\t\t\t\t\t\tlog.warn(\"Decimal precision is set to (\" + org.apache.spark.sql.types.DecimalType.MAX_PRECISION() + \") and scale is set to (\" + ";
  protected final String TEXT_183 = " + \").\");" + NL + "\t\t\t\t\t\t\t";
  protected final String TEXT_184 = NL + "\t\t\t\t\t\t\ttypes_";
  protected final String TEXT_185 = " = types_";
  protected final String TEXT_186 = ".add(\"";
  protected final String TEXT_187 = "\", org.apache.spark.sql.types.DataTypes.createDecimalType(org.apache.spark.sql.types.DecimalType.MAX_PRECISION(), ";
  protected final String TEXT_188 = "));" + NL + "\t\t\t\t\t\t\tfieldsList.add(getStructField_";
  protected final String TEXT_189 = "(fields_";
  protected final String TEXT_190 = ", \"";
  protected final String TEXT_191 = "\", org.apache.spark.sql.types.DataTypes.createDecimalType(org.apache.spark.sql.types.DecimalType.MAX_PRECISION(), ";
  protected final String TEXT_192 = ")));" + NL + "\t\t\t\t\t\t}" + NL + "\t\t\t\t\t";
  protected final String TEXT_193 = NL + "\t\t\t\t\t\tcolumns.add(new org.apache.kudu.ColumnSchema.ColumnSchemaBuilder(\"";
  protected final String TEXT_194 = "\", org.apache.kudu.Type.STRING).key(true).build());" + NL + "\t\t\t\t\t";
  protected final String TEXT_195 = NL + "\t\t\t\t\ttypes_";
  protected final String TEXT_196 = " = types_";
  protected final String TEXT_197 = ".add(\"";
  protected final String TEXT_198 = "\", \"";
  protected final String TEXT_199 = "\");" + NL + "\t\t\t\t\tfieldsList.add(getStructField_";
  protected final String TEXT_200 = "(fields_";
  protected final String TEXT_201 = ", \"";
  protected final String TEXT_202 = "\", null));" + NL + "\t\t\t\t";
  protected final String TEXT_203 = NL + "\t\t\t\t\t\tcolumns.add(new org.apache.kudu.ColumnSchema.ColumnSchemaBuilder(\"";
  protected final String TEXT_204 = "\", org.apache.kudu.Type.INT32).key(true).build());" + NL + "\t\t\t\t\t";
  protected final String TEXT_205 = NL + "\t\t\t\t\ttypes_";
  protected final String TEXT_206 = " = types_";
  protected final String TEXT_207 = ".add(\"";
  protected final String TEXT_208 = "\", org.apache.spark.sql.types.DataTypes.IntegerType);" + NL + "\t\t\t\t\tfieldsList.add(getStructField_";
  protected final String TEXT_209 = "(fields_";
  protected final String TEXT_210 = ", \"";
  protected final String TEXT_211 = "\", org.apache.spark.sql.types.DataTypes.IntegerType));" + NL + "\t\t\t\t";
  protected final String TEXT_212 = NL + "\t\t\t\t\t\tcolumns.add(new org.apache.kudu.ColumnSchema.ColumnSchemaBuilder(\"";
  protected final String TEXT_213 = "\", org.apache.kudu.Type.INT64).key(true).build());" + NL + "\t\t\t\t\t";
  protected final String TEXT_214 = NL + "\t\t\t\t\ttypes_";
  protected final String TEXT_215 = " = types_";
  protected final String TEXT_216 = ".add(\"";
  protected final String TEXT_217 = "\", org.apache.spark.sql.types.DataTypes.LongType);" + NL + "\t\t\t\t\tfieldsList.add(getStructField_";
  protected final String TEXT_218 = "(fields_";
  protected final String TEXT_219 = ", \"";
  protected final String TEXT_220 = "\", org.apache.spark.sql.types.DataTypes.LongType));" + NL + "\t\t\t\t";
  protected final String TEXT_221 = NL + "\t\t\t\t\t\tcolumns.add(new org.apache.kudu.ColumnSchema.ColumnSchemaBuilder(\"";
  protected final String TEXT_222 = "\", org.apache.kudu.Type.INT32).nullable(true).key(false).build());" + NL + "\t\t\t\t\t";
  protected final String TEXT_223 = NL + "\t\t\t\t\t\tcolumns.add(new org.apache.kudu.ColumnSchema.ColumnSchemaBuilder(\"";
  protected final String TEXT_224 = "\", org.apache.kudu.Type.STRING).nullable(true).key(false).build());" + NL + "\t\t\t\t\t";
  protected final String TEXT_225 = NL + "\t\t\t\t\t\tcolumns.add(new org.apache.kudu.ColumnSchema.ColumnSchemaBuilder(\"";
  protected final String TEXT_226 = "\", org.apache.kudu.Type.INT64).nullable(true).key(false).build());" + NL + "\t\t\t\t\t";
  protected final String TEXT_227 = NL + "\t\t\t\t\t\tcolumns.add(new org.apache.kudu.ColumnSchema.ColumnSchemaBuilder(\"";
  protected final String TEXT_228 = "\", org.apache.kudu.Type.BINARY).nullable(true).key(false).build());" + NL + "\t\t\t\t\t";
  protected final String TEXT_229 = NL + "\t\t\t\t\t\tcolumns.add(new org.apache.kudu.ColumnSchema.ColumnSchemaBuilder(\"";
  protected final String TEXT_230 = "\", org.apache.kudu.Type.BOOL).nullable(true).key(false).build());" + NL + "\t\t\t\t\t";
  protected final String TEXT_231 = NL + "\t\t\t\t\t\tcolumns.add(new org.apache.kudu.ColumnSchema.ColumnSchemaBuilder(\"";
  protected final String TEXT_232 = "\", org.apache.kudu.Type.FLOAT).nullable(true).key(false).build());" + NL + "\t\t\t\t\t";
  protected final String TEXT_233 = NL + "\t\t\t\t\t\tcolumns.add(new org.apache.kudu.ColumnSchema.ColumnSchemaBuilder(\"";
  protected final String TEXT_234 = "\", org.apache.kudu.Type.DOUBLE).nullable(true).key(false).build());" + NL + "\t\t\t\t\t";
  protected final String TEXT_235 = NL + "\t\t\t\t\t\tcolumns.add(new org.apache.kudu.ColumnSchema.ColumnSchemaBuilder(\"";
  protected final String TEXT_236 = "\", org.apache.kudu.Type.INT8).nullable(true).key(false).build());" + NL + "\t\t\t\t\t";
  protected final String TEXT_237 = NL + "\t\t\t\t\t\tcolumns.add(new org.apache.kudu.ColumnSchema.ColumnSchemaBuilder(\"";
  protected final String TEXT_238 = "\", org.apache.kudu.Type.STRING).nullable(true).key(false).build());" + NL + "\t\t\t\t\t";
  protected final String TEXT_239 = NL + "\t\t\t\t\t\tcolumns.add(new org.apache.kudu.ColumnSchema.ColumnSchemaBuilder(\"";
  protected final String TEXT_240 = "\", org.apache.kudu.Type.UNIXTIME_MICROS).nullable(true).key(false).build());" + NL + "\t\t\t\t\t";
  protected final String TEXT_241 = NL + "\t\t\t\t\t\tcolumns.add(new org.apache.kudu.ColumnSchema.ColumnSchemaBuilder(\"";
  protected final String TEXT_242 = "\", org.apache.kudu.Type.DOUBLE).nullable(true).key(false).build());" + NL + "\t\t\t\t\t";
  protected final String TEXT_243 = NL + "\t\t\t" + NL + "\t\t\tfieldsArray = fieldsList.toArray(fieldsArray);" + NL + "\t\t\torg.apache.kudu.Schema schema = new org.apache.kudu.Schema(columns);" + NL + "\t\t\t" + NL + "\t\t\t";
  protected final String TEXT_244 = NL + "\t\t\t\tscala.collection.Seq<String> keys_";
  protected final String TEXT_245 = " = scala.collection.JavaConverters.asScalaBufferConverter(strKuduKeysList_";
  protected final String TEXT_246 = ").asScala().seq();" + NL + "\t\t\t";
  protected final String TEXT_247 = NL + "\t\t\t\tscala.collection.Seq<String> keys_";
  protected final String TEXT_248 = " = scala.collection.JavaConverters.asScalaIterableConverter(strKuduKeysList_";
  protected final String TEXT_249 = ").asScala().toSeq();" + NL + "\t\t\t";
  protected final String TEXT_250 = NL + "\t\t\ttry {" + NL + "\t\t\t\t";
  protected final String TEXT_251 = NL + "\t\t\t\t\tboolean createKuduTable = true;" + NL + "\t\t\t\t\t";
  protected final String TEXT_252 = NL + "\t\t\t\t\t\tif(kuduCtx_";
  protected final String TEXT_253 = ".tableExists(";
  protected final String TEXT_254 = ")) {" + NL + "\t\t\t\t\t\t\tkuduCtx_";
  protected final String TEXT_255 = ".deleteTable(";
  protected final String TEXT_256 = ");" + NL + "\t\t\t\t\t\t}" + NL + "\t\t\t\t\t";
  protected final String TEXT_257 = NL + "\t\t\t\t\t\tkuduCtx_";
  protected final String TEXT_258 = ".deleteTable(";
  protected final String TEXT_259 = ");" + NL + "\t\t\t\t\t";
  protected final String TEXT_260 = NL + "\t        \t\t\t\tjava.util.List<String> rangePartitionColumns_";
  protected final String TEXT_261 = "_";
  protected final String TEXT_262 = " = new java.util.ArrayList<String>();" + NL + "\t        \t\t\t\torg.apache.kudu.client.PartialRow lower_";
  protected final String TEXT_263 = " = schema.newPartialRow();" + NL + "\t        \t\t\t\torg.apache.kudu.client.PartialRow upper_";
  protected final String TEXT_264 = " = schema.newPartialRow();" + NL + "\t        \t\t\t";
  protected final String TEXT_265 = NL + "\t\t\t\t        \t\t\t\trangePartitionColumns_";
  protected final String TEXT_266 = "_";
  protected final String TEXT_267 = ".add(\"";
  protected final String TEXT_268 = "\");" + NL + "\t\t\t\t        \t\t\t\t";
  protected final String TEXT_269 = NL + "\t\t\t\t\t        \t\t\t\tlower_";
  protected final String TEXT_270 = ".addString(\"";
  protected final String TEXT_271 = "\", ";
  protected final String TEXT_272 = ");" + NL + "\t\t\t\t\t        \t\t\t";
  protected final String TEXT_273 = NL + "\t\t\t\t\t        \t\t\t\tupper_";
  protected final String TEXT_274 = ".addString(\"";
  protected final String TEXT_275 = "\", ";
  protected final String TEXT_276 = ");" + NL + "\t\t\t\t\t        \t\t\t";
  protected final String TEXT_277 = NL + "\t\t\t\t        \t\t\t";
  protected final String TEXT_278 = NL + "\t\t\t\t        \t\t\t\trangePartitionColumns_";
  protected final String TEXT_279 = "_";
  protected final String TEXT_280 = ".add(\"";
  protected final String TEXT_281 = "\");" + NL + "\t\t\t\t        \t\t\t\t";
  protected final String TEXT_282 = NL + "\t\t\t\t\t        \t\t\t\tlower_";
  protected final String TEXT_283 = ".addInt(\"";
  protected final String TEXT_284 = "\", ";
  protected final String TEXT_285 = ");" + NL + "\t\t\t\t\t        \t\t\t";
  protected final String TEXT_286 = NL + "\t\t\t\t\t        \t\t\t\tupper_";
  protected final String TEXT_287 = ".addInt(\"";
  protected final String TEXT_288 = "\", ";
  protected final String TEXT_289 = ");" + NL + "\t\t\t\t\t        \t\t\t";
  protected final String TEXT_290 = NL + "\t\t\t\t        \t\t\t";
  protected final String TEXT_291 = NL + "\t\t\t\t        \t\t\t\trangePartitionColumns_";
  protected final String TEXT_292 = "_";
  protected final String TEXT_293 = ".add(\"";
  protected final String TEXT_294 = "\");" + NL + "\t\t\t\t\t        \t\t\t";
  protected final String TEXT_295 = NL + "\t\t\t\t\t        \t\t\t\tlower_";
  protected final String TEXT_296 = ".addLong(\"";
  protected final String TEXT_297 = "\", ";
  protected final String TEXT_298 = ");" + NL + "\t\t\t\t\t        \t\t\t";
  protected final String TEXT_299 = NL + "\t\t\t\t\t        \t\t\t\tupper_";
  protected final String TEXT_300 = ".addLong(\"";
  protected final String TEXT_301 = "\", ";
  protected final String TEXT_302 = ");" + NL + "\t\t\t\t\t        \t\t\t";
  protected final String TEXT_303 = NL + "\t\t\t\t        \t\t\t";
  protected final String TEXT_304 = NL + "\t\t\t\t        \t\t\t\trangePartitionColumns_";
  protected final String TEXT_305 = "_";
  protected final String TEXT_306 = ".add(\"";
  protected final String TEXT_307 = "\");" + NL + "\t\t\t\t\t        \t\t\t";
  protected final String TEXT_308 = NL + "\t\t\t\t\t        \t\t\t\tlower_";
  protected final String TEXT_309 = ".addLong(\"";
  protected final String TEXT_310 = "\", ";
  protected final String TEXT_311 = ");" + NL + "\t\t\t\t\t        \t\t\t";
  protected final String TEXT_312 = NL + "\t\t\t\t\t        \t\t\t\tupper_";
  protected final String TEXT_313 = ".addLong(\"";
  protected final String TEXT_314 = "\", ";
  protected final String TEXT_315 = ");" + NL + "\t\t\t\t\t        \t\t\t";
  protected final String TEXT_316 = NL + "\t\t\t\t        \t\t\t";
  protected final String TEXT_317 = NL + "\t\t\t\t        \t\t\t\trangePartitionColumns_";
  protected final String TEXT_318 = "_";
  protected final String TEXT_319 = ".add(\"";
  protected final String TEXT_320 = "\");" + NL + "\t\t\t\t\t        \t\t\t";
  protected final String TEXT_321 = NL + "\t\t\t\t\t        \t\t\t\tlower_";
  protected final String TEXT_322 = ".addByte(\"";
  protected final String TEXT_323 = "\", ";
  protected final String TEXT_324 = ");" + NL + "\t\t\t\t\t        \t\t\t";
  protected final String TEXT_325 = NL + "\t\t\t\t\t        \t\t\t\tupper_";
  protected final String TEXT_326 = ".addByte(\"";
  protected final String TEXT_327 = "\", ";
  protected final String TEXT_328 = ");" + NL + "\t\t\t\t\t        \t\t\t";
  protected final String TEXT_329 = NL + "\t\t\t\t        \t\t\t";
  protected final String TEXT_330 = NL + "\t\t\t\t        \t\t\t\trangePartitionColumns_";
  protected final String TEXT_331 = "_";
  protected final String TEXT_332 = ".add(\"";
  protected final String TEXT_333 = "\");" + NL + "\t\t\t\t\t        \t\t\t";
  protected final String TEXT_334 = NL + "\t\t\t\t\t        \t\t\t\tlower_";
  protected final String TEXT_335 = ".addDouble(\"";
  protected final String TEXT_336 = "\", ";
  protected final String TEXT_337 = ");" + NL + "\t\t\t\t\t        \t\t\t";
  protected final String TEXT_338 = NL + "\t\t\t\t\t        \t\t\t\tupper_";
  protected final String TEXT_339 = ".addDouble(\"";
  protected final String TEXT_340 = "\", ";
  protected final String TEXT_341 = ");" + NL + "\t\t\t\t\t        \t\t\t";
  protected final String TEXT_342 = NL + "\t\t\t\t        \t\t\t";
  protected final String TEXT_343 = NL + "\t\t\t\t        \t\t\t\trangePartitionColumns_";
  protected final String TEXT_344 = "_";
  protected final String TEXT_345 = ".add(\"";
  protected final String TEXT_346 = "\");" + NL + "\t\t\t\t\t        \t\t\t";
  protected final String TEXT_347 = NL + "\t\t\t\t\t        \t\t\t\tlower_";
  protected final String TEXT_348 = ".addFloat(\"";
  protected final String TEXT_349 = "\", ";
  protected final String TEXT_350 = ");" + NL + "\t\t\t\t\t        \t\t\t";
  protected final String TEXT_351 = NL + "\t\t\t\t\t        \t\t\t\tupper_";
  protected final String TEXT_352 = ".addFloat(\"";
  protected final String TEXT_353 = "\", ";
  protected final String TEXT_354 = ");" + NL + "\t\t\t\t\t        \t\t\t";
  protected final String TEXT_355 = NL + "\t\t\t\t        \t\t\t";
  protected final String TEXT_356 = NL + "\t\t\t\t        \t\t\t\trangePartitionColumns_";
  protected final String TEXT_357 = "_";
  protected final String TEXT_358 = ".add(\"";
  protected final String TEXT_359 = "\");" + NL + "\t\t\t\t\t        \t\t\t";
  protected final String TEXT_360 = NL + "\t\t\t\t\t        \t\t\t\tlower_";
  protected final String TEXT_361 = ".addBoolean(\"";
  protected final String TEXT_362 = "\", ";
  protected final String TEXT_363 = ");" + NL + "\t\t\t\t\t        \t\t\t";
  protected final String TEXT_364 = NL + "\t\t\t\t\t        \t\t\t\tupper_";
  protected final String TEXT_365 = ".addBoolean(\"";
  protected final String TEXT_366 = "\", ";
  protected final String TEXT_367 = ");" + NL + "\t\t\t\t\t        \t\t\t";
  protected final String TEXT_368 = NL + "\t\t\t\t        \t\t\t";
  protected final String TEXT_369 = NL + "\t\t\t\t        \t\t\t\trangePartitionColumns_";
  protected final String TEXT_370 = "_";
  protected final String TEXT_371 = ".add(\"";
  protected final String TEXT_372 = "\");" + NL + "\t\t\t\t        \t\t\t\t";
  protected final String TEXT_373 = NL + "\t\t\t\t\t        \t\t\t\tjava.util.Date dateLower_";
  protected final String TEXT_374 = " = ";
  protected final String TEXT_375 = ";" + NL + "\t\t\t\t\t        \t\t\t\tlower_";
  protected final String TEXT_376 = ".addLong(\"";
  protected final String TEXT_377 = "\", dateLower_";
  protected final String TEXT_378 = ".getTime() * 1000);" + NL + "\t\t\t\t\t        \t\t\t";
  protected final String TEXT_379 = NL + "\t\t\t\t\t        \t\t\t\tjava.util.Date dateUpper_";
  protected final String TEXT_380 = " = ";
  protected final String TEXT_381 = ";" + NL + "\t\t\t\t\t        \t\t\t\tupper_";
  protected final String TEXT_382 = ".addLong(\"";
  protected final String TEXT_383 = "\", dateUpper_";
  protected final String TEXT_384 = ".getTime() * 1000);" + NL + "\t\t\t\t\t        \t\t\t";
  protected final String TEXT_385 = NL + "\t\t\t\t        \t\t\t";
  protected final String TEXT_386 = NL + "\t\t\t\t        \t\t\t\trangePartitionColumns_";
  protected final String TEXT_387 = "_";
  protected final String TEXT_388 = ".add(\"";
  protected final String TEXT_389 = "\");" + NL + "\t\t\t\t\t        \t\t\t";
  protected final String TEXT_390 = NL + "\t\t\t\t\t        \t\t\t\tlower_";
  protected final String TEXT_391 = ".addDouble(\"";
  protected final String TEXT_392 = "\", ";
  protected final String TEXT_393 = ".doubleValue());" + NL + "\t\t\t\t\t        \t\t\t";
  protected final String TEXT_394 = NL + "\t\t\t\t\t        \t\t\t\tupper_";
  protected final String TEXT_395 = ".addDouble(\"";
  protected final String TEXT_396 = "\", ";
  protected final String TEXT_397 = ".doubleValue());" + NL + "\t\t\t\t\t        \t\t\t";
  protected final String TEXT_398 = NL + "\t\t\t\t        \t\t\t";
  protected final String TEXT_399 = NL + "\t\t\t\t\t\tif(!kuduCtx_";
  protected final String TEXT_400 = ".tableExists(";
  protected final String TEXT_401 = ")) {" + NL + "\t\t\t\t\t\t\tcreateKuduTable = true;" + NL + "\t\t\t\t\t\t} else {" + NL + "\t\t\t\t\t\t\tcreateKuduTable = false;" + NL + "\t\t\t\t\t\t}" + NL + "\t\t\t\t\t";
  protected final String TEXT_402 = NL + "\t\t\t\t\t" + NL + "\t\t\t\t\tif(createKuduTable)" + NL + "\t\t\t\t\tkuduCtx_";
  protected final String TEXT_403 = ".createTable(";
  protected final String TEXT_404 = ", types_";
  protected final String TEXT_405 = ", keys_";
  protected final String TEXT_406 = ", new org.apache.kudu.client.CreateTableOptions()" + NL + "\t\t\t\t\t";
  protected final String TEXT_407 = NL + "\t\t\t\t\t\t\t.addHashPartitions(java.util.Arrays.asList(\"";
  protected final String TEXT_408 = "\"), ";
  protected final String TEXT_409 = ")" + NL + "\t\t\t\t\t\t\t";
  protected final String TEXT_410 = NL + "\t\t\t\t\t\t\t.addRangePartition(lower_";
  protected final String TEXT_411 = ", upper_";
  protected final String TEXT_412 = ")" + NL + "\t\t\t\t\t\t";
  protected final String TEXT_413 = NL + "\t\t\t\t\t\t\t.setRangePartitionColumns(rangePartitionColumns_";
  protected final String TEXT_414 = "_";
  protected final String TEXT_415 = ")" + NL + "\t\t\t\t\t\t";
  protected final String TEXT_416 = NL + "\t\t\t\t\t\t.setRangePartitionColumns(strKuduKeysList_";
  protected final String TEXT_417 = ")" + NL + "\t\t\t\t\t";
  protected final String TEXT_418 = NL + "\t\t\t\t\t.setNumReplicas(";
  protected final String TEXT_419 = "));" + NL + "\t\t\t\t";
  protected final String TEXT_420 = NL + "\t\t\t\t\torg.apache.spark.api.java.JavaRDD<org.apache.spark.sql.Row> rddRow_";
  protected final String TEXT_421 = " = ";
  protected final String TEXT_422 = ".values().map(new ";
  protected final String TEXT_423 = "_";
  protected final String TEXT_424 = "ToRow());" + NL + "\t\t\t\t\t" + NL + "\t\t\t\t\t";
  protected final String TEXT_425 = NL + "\t\t\t\t\t\torg.apache.spark.sql.DataFrame df_";
  protected final String TEXT_426 = " = sqlCtx_";
  protected final String TEXT_427 = ".createDataFrame(rddRow_";
  protected final String TEXT_428 = ", org.apache.spark.sql.types.DataTypes.createStructType(fieldsArray));" + NL + "\t\t\t\t\t\tkuduCtx_";
  protected final String TEXT_429 = ".insertRows(df_";
  protected final String TEXT_430 = ", ";
  protected final String TEXT_431 = ");" + NL + "\t\t\t\t\t";
  protected final String TEXT_432 = NL + "\t\t\t\t\t\torg.apache.spark.sql.Dataset<org.apache.spark.sql.Row> df_";
  protected final String TEXT_433 = " = sqlCtx_";
  protected final String TEXT_434 = ".createDataFrame(rddRow_";
  protected final String TEXT_435 = ", org.apache.spark.sql.types.DataTypes.createStructType(fieldsArray));" + NL + "\t\t\t\t\t\tkuduCtx_";
  protected final String TEXT_436 = ".insertRows(df_";
  protected final String TEXT_437 = ", ";
  protected final String TEXT_438 = ");" + NL + "\t\t\t\t\t";
  protected final String TEXT_439 = NL + "\t\t\t\t\torg.apache.spark.api.java.JavaRDD<org.apache.spark.sql.Row> rddRow_";
  protected final String TEXT_440 = " = ";
  protected final String TEXT_441 = ".values().map(new ";
  protected final String TEXT_442 = "_";
  protected final String TEXT_443 = "ToRow());" + NL + "\t\t\t\t\t" + NL + "\t\t\t\t\t";
  protected final String TEXT_444 = NL + "\t\t\t\t\t\torg.apache.spark.sql.DataFrame df_";
  protected final String TEXT_445 = " = sqlCtx_";
  protected final String TEXT_446 = ".createDataFrame(rddRow_";
  protected final String TEXT_447 = ", org.apache.spark.sql.types.DataTypes.createStructType(fieldsArray));" + NL + "\t\t\t\t\t\tkuduCtx_";
  protected final String TEXT_448 = ".updateRows(df_";
  protected final String TEXT_449 = ", ";
  protected final String TEXT_450 = ");" + NL + "\t\t\t\t\t";
  protected final String TEXT_451 = NL + "\t\t\t\t\t\torg.apache.spark.sql.Dataset<org.apache.spark.sql.Row> df_";
  protected final String TEXT_452 = " = sqlCtx_";
  protected final String TEXT_453 = ".createDataFrame(rddRow_";
  protected final String TEXT_454 = ", org.apache.spark.sql.types.DataTypes.createStructType(fieldsArray));" + NL + "\t\t\t\t\t\tkuduCtx_";
  protected final String TEXT_455 = ".updateRows(df_";
  protected final String TEXT_456 = ", ";
  protected final String TEXT_457 = ");" + NL + "\t\t\t\t\t";
  protected final String TEXT_458 = NL + "\t\t\t\t\torg.apache.spark.api.java.JavaRDD<org.apache.spark.sql.Row> rddRow_";
  protected final String TEXT_459 = " = ";
  protected final String TEXT_460 = ".values().map(new ";
  protected final String TEXT_461 = "_";
  protected final String TEXT_462 = "ToRow());" + NL + "\t\t\t\t\t" + NL + "\t\t\t\t\t";
  protected final String TEXT_463 = NL + "\t\t\t\t\t\torg.apache.spark.sql.DataFrame df_";
  protected final String TEXT_464 = " = sqlCtx_";
  protected final String TEXT_465 = ".createDataFrame(rddRow_";
  protected final String TEXT_466 = ", org.apache.spark.sql.types.DataTypes.createStructType(fieldsArray));" + NL + "\t\t\t\t\t\tkuduCtx_";
  protected final String TEXT_467 = ".upsertRows(df_";
  protected final String TEXT_468 = ", ";
  protected final String TEXT_469 = ");" + NL + "\t\t\t\t\t";
  protected final String TEXT_470 = NL + "\t\t\t\t\t\torg.apache.spark.sql.Dataset<org.apache.spark.sql.Row> df_";
  protected final String TEXT_471 = " = sqlCtx_";
  protected final String TEXT_472 = ".createDataFrame(rddRow_";
  protected final String TEXT_473 = ", org.apache.spark.sql.types.DataTypes.createStructType(fieldsArray));" + NL + "\t\t\t\t\t\tkuduCtx_";
  protected final String TEXT_474 = ".upsertRows(df_";
  protected final String TEXT_475 = ", ";
  protected final String TEXT_476 = ");" + NL + "\t\t\t\t\t";
  protected final String TEXT_477 = NL + "\t\t\t\t\torg.apache.spark.api.java.JavaRDD<org.apache.spark.sql.Row> rddRow_";
  protected final String TEXT_478 = " = ";
  protected final String TEXT_479 = ".values().map(new ";
  protected final String TEXT_480 = "_";
  protected final String TEXT_481 = "ToRow());" + NL + "\t\t\t\t\t" + NL + "\t\t\t\t\tjava.util.List<org.apache.spark.sql.types.StructField> structFieldsKey_";
  protected final String TEXT_482 = " = new java.util.ArrayList<org.apache.spark.sql.types.StructField>();" + NL + "\t\t\t\t\t" + NL + "\t\t\t\t\tfor(int i=0; i<fieldsList.size(); i++) {" + NL + "\t\t\t\t\t\tif(strKuduKeysList_";
  protected final String TEXT_483 = ".contains(fieldsList.get(i).name())) {" + NL + "\t\t\t\t\t\t\tstructFieldsKey_";
  protected final String TEXT_484 = ".add(fieldsList.get(i));" + NL + "\t\t\t\t\t\t}" + NL + "\t\t\t\t\t}" + NL + "\t\t\t\t\t" + NL + "\t\t\t\t\t";
  protected final String TEXT_485 = NL + "\t\t\t\t\t\torg.apache.spark.sql.DataFrame df_";
  protected final String TEXT_486 = " = sqlCtx_";
  protected final String TEXT_487 = ".createDataFrame(rddRow_";
  protected final String TEXT_488 = ", org.apache.spark.sql.types.DataTypes.createStructType(structFieldsKey_";
  protected final String TEXT_489 = "));" + NL + "\t\t\t\t\t\tkuduCtx_";
  protected final String TEXT_490 = ".deleteRows(df_";
  protected final String TEXT_491 = ", ";
  protected final String TEXT_492 = ");" + NL + "\t\t\t\t\t";
  protected final String TEXT_493 = NL + "\t\t\t\t\t\torg.apache.spark.sql.Dataset<org.apache.spark.sql.Row> df_";
  protected final String TEXT_494 = " = sqlCtx_";
  protected final String TEXT_495 = ".createDataFrame(rddRow_";
  protected final String TEXT_496 = ", org.apache.spark.sql.types.DataTypes.createStructType(structFieldsKey_";
  protected final String TEXT_497 = "));" + NL + "\t\t\t\t\t\tkuduCtx_";
  protected final String TEXT_498 = ".deleteRows(df_";
  protected final String TEXT_499 = ", ";
  protected final String TEXT_500 = ");" + NL + "\t\t\t\t\t";
  protected final String TEXT_501 = NL + "\t\t\t} catch(java.lang.Exception e_";
  protected final String TEXT_502 = ") {" + NL + "\t\t\t\t";
  protected final String TEXT_503 = NL + "\t\t\t\t\tthrow(e_";
  protected final String TEXT_504 = ");" + NL + "\t\t\t\t\t";
  protected final String TEXT_505 = NL + "\t\t\t\t\t\tlog.error(\"";
  protected final String TEXT_506 = " - \" + e_";
  protected final String TEXT_507 = ".getMessage());" + NL + "\t\t\t\t\t";
  protected final String TEXT_508 = NL + "\t\t\t\t\tSystem.err.println(e_";
  protected final String TEXT_509 = ".getMessage());" + NL + "\t\t\t\t\t";
  protected final String TEXT_510 = NL + "\t\t\t}" + NL + "\t\t\t";
  protected final String TEXT_511 = NL;

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    
BigDataCodeGeneratorArgument codeGenArgument = (BigDataCodeGeneratorArgument) argument;
INode node = (INode)codeGenArgument.getArgument();
String cid = node.getUniqueName();
List<IMetadataTable> metadatas = node.getMetadataList();

    
INode kuduConfigurationNode = null;
String kuduConfigurationName = ElementParameterParser.getValue(node,"__KUDU_CONFIGURATION__");
boolean useExistingConnection = ("true").equals(ElementParameterParser.getValue(node,"__USE_EXISTING_CONNECTION__"));

for (INode pNode : node.getProcess().getNodesOfType("tKuduConfiguration")) {
    if(kuduConfigurationName!=null && kuduConfigurationName.equals(pNode.getUniqueName())) {
        kuduConfigurationNode = pNode;
        break;
    }
}

if(!useExistingConnection) {
	kuduConfigurationNode = node;
}

List<java.util.Map<String, String>> serverProperties = (List<java.util.Map<String, String>>) ElementParameterParser.getObjectValue(kuduConfigurationNode, "__SERVER_PROPERTIES__");

    stringBuffer.append(TEXT_1);
    
for(java.util.Map<String, String> map : serverProperties) {

    stringBuffer.append(TEXT_2);
    stringBuffer.append(map.get("SERVER"));
    stringBuffer.append(TEXT_3);
    stringBuffer.append(map.get("PORT"));
    stringBuffer.append(TEXT_4);
    
}

    stringBuffer.append(TEXT_5);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_6);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_7);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_8);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_9);
    
final boolean isLog4jEnabled = ("true").equals(ElementParameterParser.getValue(node.getProcess(), "__LOG4J_ACTIVATE__"));

org.talend.hadoop.distribution.ESparkVersion sparkVersion = codeGenArgument.getSparkVersion();

if(metadatas!=null && metadatas.size() > 0) {
    IMetadataTable metadata = metadatas.get(0);
    if(metadata != null){
    	List< ? extends IConnection> connections = node.getIncomingConnections();
    	if ((connections != null) && (connections.size() > 0)) {
    		IConnection connection = connections.get(0);
    		String connName = connection.getName();
    		String inRddName = "rdd_"+connection.getName();
    		String originalStructName = codeGenArgument.getRecordStructName(connection);
    		String tableName = ElementParameterParser.getValue(node, "__TABLE__");
    		String tableAction = ElementParameterParser.getValue(node, "__TABLE_ACTION__");
    		String dataAction = ElementParameterParser.getValue(node, "__DATA_ACTION__");
        	String schema = ElementParameterParser.getValue(node, "__SCHEMA__");
        	String numOfReplicas = ElementParameterParser.getValue(node, "__NUM_REPLICAS__");
        	boolean dieOnError = ("true").equals(ElementParameterParser.getValue(node, "__DIE_ON_ERROR__"));
        	
        	java.util.List<IMetadataColumn> columnList = metadata.getListColumns();
        	
    stringBuffer.append(TEXT_10);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_11);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_12);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_13);
    stringBuffer.append(originalStructName);
    stringBuffer.append(TEXT_14);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_15);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_16);
    
        	for (int i = 0; i < columnList.size(); i++) {
        		IMetadataColumn column = columnList.get(i);
        		String typeToGenerate = JavaTypesManager.getTypeToGenerate(column.getTalendType(), column.isNullable());
        		JavaType javaType = JavaTypesManager.getJavaTypeFromId(column.getTalendType());
        		String patternValue = column.getPattern() == null || column.getPattern().trim().length() == 0 ? null : column.getPattern();
				if(column.isKey()) {
				
    stringBuffer.append(TEXT_17);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_18);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_19);
    
				}
				if(javaType == JavaTypesManager.DATE) {
					if(column.isKey()) {
					
    stringBuffer.append(TEXT_20);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_21);
    
					}
					
    stringBuffer.append(TEXT_22);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_23);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_24);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_25);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_26);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_27);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_28);
    
				} else if(javaType == JavaTypesManager.CHARACTER) {
					if(column.isKey()) {
					
    stringBuffer.append(TEXT_29);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_30);
    
					}
					
    stringBuffer.append(TEXT_31);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_32);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_33);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_34);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_35);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_36);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_37);
    
				} else if(javaType == JavaTypesManager.SHORT) {
					if(column.isKey()) {
					
    stringBuffer.append(TEXT_38);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_39);
    
			        }
			        
    stringBuffer.append(TEXT_40);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_41);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_42);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_43);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_44);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_45);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_46);
    
				} else if(javaType == JavaTypesManager.DOUBLE) {
					if(column.isKey()) {
					
    stringBuffer.append(TEXT_47);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_48);
    
					}
					
    stringBuffer.append(TEXT_49);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_50);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_51);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_52);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_53);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_54);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_55);
    
				} else if(javaType == JavaTypesManager.FLOAT) {
					if(column.isKey()) {
					
    stringBuffer.append(TEXT_56);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_57);
    
					}
					
    stringBuffer.append(TEXT_58);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_59);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_60);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_61);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_62);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_63);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_64);
    
				} else if(javaType == JavaTypesManager.BOOLEAN) {
					if(column.isKey()) {
					
    stringBuffer.append(TEXT_65);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_66);
    
					}
					
    stringBuffer.append(TEXT_67);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_68);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_69);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_70);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_71);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_72);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_73);
    
				} else if(javaType == JavaTypesManager.BYTE) {
					if(column.isKey()) {
					
    stringBuffer.append(TEXT_74);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_75);
    
					}
					
    stringBuffer.append(TEXT_76);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_77);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_78);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_79);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_80);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_81);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_82);
    
				} else if(javaType == JavaTypesManager.BIGDECIMAL) {
					Integer precision = column.getLength();
					Integer scale = column.getPrecision();
					final int defaultScale = 18;
					
					if(column.isKey()) {
					
    stringBuffer.append(TEXT_83);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_84);
    
					}
					
					if(precision != null) {
					
    stringBuffer.append(TEXT_85);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_86);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_87);
    stringBuffer.append(precision);
    stringBuffer.append(TEXT_88);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_89);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_90);
     if(isLog4jEnabled) { 
    stringBuffer.append(TEXT_91);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_92);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_93);
     } 
    stringBuffer.append(TEXT_94);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_95);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_96);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_97);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_98);
    stringBuffer.append(scale);
    stringBuffer.append(TEXT_99);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_100);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_101);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_102);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_103);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_104);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_105);
     if(isLog4jEnabled) { 
    stringBuffer.append(TEXT_106);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_107);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_108);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_109);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_110);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_111);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_112);
     } 
    stringBuffer.append(TEXT_113);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_114);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_115);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_116);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_117);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_118);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_119);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_120);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_121);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_122);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_123);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_124);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_125);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_126);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_127);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_128);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_129);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_130);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_131);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_132);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_133);
    stringBuffer.append(defaultScale);
    stringBuffer.append(TEXT_134);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_135);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_136);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_137);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_138);
    stringBuffer.append(defaultScale);
    stringBuffer.append(TEXT_139);
     if(isLog4jEnabled) { 
    stringBuffer.append(TEXT_140);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_141);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_142);
     } 
    stringBuffer.append(TEXT_143);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_144);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_145);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_146);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_147);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_148);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_149);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_150);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_151);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_152);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_153);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_154);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_155);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_156);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_157);
    
					} else {
					
    stringBuffer.append(TEXT_158);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_159);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_160);
    stringBuffer.append(scale);
    stringBuffer.append(TEXT_161);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_162);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_163);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_164);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_165);
     if(isLog4jEnabled) { 
    stringBuffer.append(TEXT_166);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_167);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_168);
     } 
    stringBuffer.append(TEXT_169);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_170);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_171);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_172);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_173);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_174);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_175);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_176);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_177);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_178);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_179);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_180);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_181);
     if(isLog4jEnabled) { 
    stringBuffer.append(TEXT_182);
    stringBuffer.append(defaultScale);
    stringBuffer.append(TEXT_183);
     } 
    stringBuffer.append(TEXT_184);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_185);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_186);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_187);
    stringBuffer.append(defaultScale);
    stringBuffer.append(TEXT_188);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_189);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_190);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_191);
    stringBuffer.append(defaultScale);
    stringBuffer.append(TEXT_192);
    
					}
				} else if(javaType == JavaTypesManager.STRING) {
					if(column.isKey()) {
					
    stringBuffer.append(TEXT_193);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_194);
    
					}
					
    stringBuffer.append(TEXT_195);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_196);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_197);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_198);
    stringBuffer.append(typeToGenerate);
    stringBuffer.append(TEXT_199);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_200);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_201);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_202);
    
				} else if(javaType == JavaTypesManager.INTEGER) {
					if(column.isKey()) {
					
    stringBuffer.append(TEXT_203);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_204);
    
					}
					
    stringBuffer.append(TEXT_205);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_206);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_207);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_208);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_209);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_210);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_211);
    
				} else if(javaType == JavaTypesManager.LONG) {
					if(column.isKey()) {
					
    stringBuffer.append(TEXT_212);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_213);
    
					}
					
    stringBuffer.append(TEXT_214);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_215);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_216);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_217);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_218);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_219);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_220);
    
				}
			}
			
			for (int i = 0; i < columnList.size(); i++) {
				IMetadataColumn column = columnList.get(i);
				JavaType javaType = JavaTypesManager.getJavaTypeFromId(column.getTalendType());
				
				if(javaType == JavaTypesManager.INTEGER) {
					if(!column.isKey()) {
					
    stringBuffer.append(TEXT_221);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_222);
    
					}
				} else if(javaType == JavaTypesManager.STRING) {
					if(!column.isKey()) {
					
    stringBuffer.append(TEXT_223);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_224);
    
					}
				} else if(javaType == JavaTypesManager.LONG) {
					if(!column.isKey()) {
					
    stringBuffer.append(TEXT_225);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_226);
    
					}
				} else if(javaType == JavaTypesManager.BYTE) {
					if(!column.isKey()) {
					
    stringBuffer.append(TEXT_227);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_228);
    
					}
				} else if(javaType == JavaTypesManager.BOOLEAN) {
					if(!column.isKey()) {
					
    stringBuffer.append(TEXT_229);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_230);
    
					}
				} else if(javaType == JavaTypesManager.FLOAT) {
					if(!column.isKey()) {
					
    stringBuffer.append(TEXT_231);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_232);
    
					}
				} else if(javaType == JavaTypesManager.DOUBLE) {
					if(!column.isKey()) {
					
    stringBuffer.append(TEXT_233);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_234);
    
					}
				} else if(javaType == JavaTypesManager.SHORT) {
					if(!column.isKey()) {
					
    stringBuffer.append(TEXT_235);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_236);
    
					}
				} else if(javaType == JavaTypesManager.CHARACTER) {
					if(!column.isKey()) {
					
    stringBuffer.append(TEXT_237);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_238);
    
					}
				} else if(javaType == JavaTypesManager.DATE) {
					if(!column.isKey()) {
					
    stringBuffer.append(TEXT_239);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_240);
    
					}
				} else if(javaType == JavaTypesManager.BIGDECIMAL) {
					if(!column.isKey()) {
					
    stringBuffer.append(TEXT_241);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_242);
    
					}
				}
			}
			
    stringBuffer.append(TEXT_243);
    
			if(org.talend.hadoop.distribution.ESparkVersion.SPARK_2_0.compareTo(sparkVersion) <= 0) {
			
    stringBuffer.append(TEXT_244);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_245);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_246);
    
			} else {
			
    stringBuffer.append(TEXT_247);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_248);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_249);
    
			}
			
    stringBuffer.append(TEXT_250);
    
				if(!("NONE").equals(tableAction)) {
				
    stringBuffer.append(TEXT_251);
    
					if(("DROP_IF_EXISTS_AND_CREATE").equals(tableAction)) {
					
    stringBuffer.append(TEXT_252);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_253);
    stringBuffer.append(tableName);
    stringBuffer.append(TEXT_254);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_255);
    stringBuffer.append(tableName);
    stringBuffer.append(TEXT_256);
    
					} else if(("DROP_CREATE_TABLE").equals(tableAction)) {
					
    stringBuffer.append(TEXT_257);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_258);
    stringBuffer.append(tableName);
    stringBuffer.append(TEXT_259);
    
					}
					TKuduOutputUtil tKuduOutputUtil = new TKuduOutputUtil(node);
					org.talend.designer.common.tkuduoutput.partitions.TKuduOutputHashPartitions hashPartitions = tKuduOutputUtil.findHashPartitions();
					List<org.talend.designer.common.tkuduoutput.partition.TKuduOutputHashPartition> listHashPartitions = hashPartitions.getHashPartitions();
					
					org.talend.designer.common.tkuduoutput.partitions.TKuduOutputRangePartitions rangePartitions = tKuduOutputUtil.findRangePartitions();
	        		List<org.talend.designer.common.tkuduoutput.partition.TKuduOutputRangePartition> listRangePartitions = rangePartitions.getRangePartitions();
	        		java.util.Map<String, List<org.talend.designer.common.tkuduoutput.partition.TKuduOutputRangePartition>> rangePartitionsGroupByNumPartition = rangePartitions.getRangePartitionsGroupByNumPartition();
	        		
	        		for (java.util.Map.Entry entry : rangePartitionsGroupByNumPartition.entrySet()) {
	        			java.util.List<org.talend.designer.common.tkuduoutput.partition.TKuduOutputRangePartition> rangePartitionGroupByNumPartition = (List<org.talend.designer.common.tkuduoutput.partition.TKuduOutputRangePartition>) entry.getValue();
	        			
    stringBuffer.append(TEXT_260);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_261);
    stringBuffer.append(entry.getKey());
    stringBuffer.append(TEXT_262);
    stringBuffer.append(entry.getKey());
    stringBuffer.append(TEXT_263);
    stringBuffer.append(entry.getKey());
    stringBuffer.append(TEXT_264);
    
	        			for(org.talend.designer.common.tkuduoutput.partition.TKuduOutputRangePartition rangePartition : rangePartitionGroupByNumPartition) {
	        				for (int j = 0; j < columnList.size(); j++) {
				        		IMetadataColumn column = columnList.get(j);
				        		JavaType javaType = JavaTypesManager.getJavaTypeFromId(column.getTalendType());
				        		
				        		if(rangePartition.getColumn().equals(column.getLabel())) {
				        		
				        			if(javaType == JavaTypesManager.STRING) {
				        			
    stringBuffer.append(TEXT_265);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_266);
    stringBuffer.append(rangePartition.getNumPartition());
    stringBuffer.append(TEXT_267);
    stringBuffer.append(rangePartition.getColumn());
    stringBuffer.append(TEXT_268);
     if(rangePartition.getLowerBoundary() != null && !rangePartition.getLowerBoundary().isEmpty()) { 
    stringBuffer.append(TEXT_269);
    stringBuffer.append(rangePartition.getNumPartition());
    stringBuffer.append(TEXT_270);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_271);
    stringBuffer.append(rangePartition.getLowerBoundary());
    stringBuffer.append(TEXT_272);
     } if(rangePartition.getUpperBoundary() != null && !rangePartition.getUpperBoundary().isEmpty()) { 
    stringBuffer.append(TEXT_273);
    stringBuffer.append(rangePartition.getNumPartition());
    stringBuffer.append(TEXT_274);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_275);
    stringBuffer.append(rangePartition.getUpperBoundary());
    stringBuffer.append(TEXT_276);
     } 
    stringBuffer.append(TEXT_277);
    
				        			} else if(javaType == JavaTypesManager.INTEGER) {
				        			
    stringBuffer.append(TEXT_278);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_279);
    stringBuffer.append(rangePartition.getNumPartition());
    stringBuffer.append(TEXT_280);
    stringBuffer.append(rangePartition.getColumn());
    stringBuffer.append(TEXT_281);
     if(rangePartition.getLowerBoundary() != null && !rangePartition.getLowerBoundary().isEmpty()) { 
    stringBuffer.append(TEXT_282);
    stringBuffer.append(rangePartition.getNumPartition());
    stringBuffer.append(TEXT_283);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_284);
    stringBuffer.append(rangePartition.getLowerBoundary());
    stringBuffer.append(TEXT_285);
     } if(rangePartition.getUpperBoundary() != null && !rangePartition.getUpperBoundary().isEmpty()) { 
    stringBuffer.append(TEXT_286);
    stringBuffer.append(rangePartition.getNumPartition());
    stringBuffer.append(TEXT_287);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_288);
    stringBuffer.append(rangePartition.getUpperBoundary());
    stringBuffer.append(TEXT_289);
     } 
    stringBuffer.append(TEXT_290);
    
				        			} else if(javaType == JavaTypesManager.LONG) {
				        			
    stringBuffer.append(TEXT_291);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_292);
    stringBuffer.append(rangePartition.getNumPartition());
    stringBuffer.append(TEXT_293);
    stringBuffer.append(rangePartition.getColumn());
    stringBuffer.append(TEXT_294);
     if(rangePartition.getLowerBoundary() != null && !rangePartition.getLowerBoundary().isEmpty()) { 
    stringBuffer.append(TEXT_295);
    stringBuffer.append(rangePartition.getNumPartition());
    stringBuffer.append(TEXT_296);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_297);
    stringBuffer.append(rangePartition.getLowerBoundary());
    stringBuffer.append(TEXT_298);
     } if(rangePartition.getUpperBoundary() != null && !rangePartition.getUpperBoundary().isEmpty()) { 
    stringBuffer.append(TEXT_299);
    stringBuffer.append(rangePartition.getNumPartition());
    stringBuffer.append(TEXT_300);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_301);
    stringBuffer.append(rangePartition.getUpperBoundary());
    stringBuffer.append(TEXT_302);
     } 
    stringBuffer.append(TEXT_303);
    
				        			} else if(javaType == JavaTypesManager.SHORT) {
				        			
    stringBuffer.append(TEXT_304);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_305);
    stringBuffer.append(rangePartition.getNumPartition());
    stringBuffer.append(TEXT_306);
    stringBuffer.append(rangePartition.getColumn());
    stringBuffer.append(TEXT_307);
     if(rangePartition.getLowerBoundary() != null && !rangePartition.getLowerBoundary().isEmpty()) { 
    stringBuffer.append(TEXT_308);
    stringBuffer.append(rangePartition.getNumPartition());
    stringBuffer.append(TEXT_309);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_310);
    stringBuffer.append(rangePartition.getLowerBoundary());
    stringBuffer.append(TEXT_311);
     } if(rangePartition.getUpperBoundary() != null && !rangePartition.getUpperBoundary().isEmpty()) { 
    stringBuffer.append(TEXT_312);
    stringBuffer.append(rangePartition.getNumPartition());
    stringBuffer.append(TEXT_313);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_314);
    stringBuffer.append(rangePartition.getUpperBoundary());
    stringBuffer.append(TEXT_315);
     } 
    stringBuffer.append(TEXT_316);
    
				        			} else if(javaType == JavaTypesManager.BYTE) {
				        			
    stringBuffer.append(TEXT_317);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_318);
    stringBuffer.append(rangePartition.getNumPartition());
    stringBuffer.append(TEXT_319);
    stringBuffer.append(rangePartition.getColumn());
    stringBuffer.append(TEXT_320);
     if(rangePartition.getLowerBoundary() != null && !rangePartition.getLowerBoundary().isEmpty()) { 
    stringBuffer.append(TEXT_321);
    stringBuffer.append(rangePartition.getNumPartition());
    stringBuffer.append(TEXT_322);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_323);
    stringBuffer.append(rangePartition.getLowerBoundary());
    stringBuffer.append(TEXT_324);
     } if(rangePartition.getUpperBoundary() != null && !rangePartition.getUpperBoundary().isEmpty()) { 
    stringBuffer.append(TEXT_325);
    stringBuffer.append(rangePartition.getNumPartition());
    stringBuffer.append(TEXT_326);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_327);
    stringBuffer.append(rangePartition.getUpperBoundary());
    stringBuffer.append(TEXT_328);
     } 
    stringBuffer.append(TEXT_329);
    
				        			} else if(javaType == JavaTypesManager.DOUBLE) {
				        			
    stringBuffer.append(TEXT_330);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_331);
    stringBuffer.append(rangePartition.getNumPartition());
    stringBuffer.append(TEXT_332);
    stringBuffer.append(rangePartition.getColumn());
    stringBuffer.append(TEXT_333);
     if(rangePartition.getLowerBoundary() != null && !rangePartition.getLowerBoundary().isEmpty()) { 
    stringBuffer.append(TEXT_334);
    stringBuffer.append(rangePartition.getNumPartition());
    stringBuffer.append(TEXT_335);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_336);
    stringBuffer.append(rangePartition.getLowerBoundary());
    stringBuffer.append(TEXT_337);
     } if(rangePartition.getUpperBoundary() != null && !rangePartition.getUpperBoundary().isEmpty()) { 
    stringBuffer.append(TEXT_338);
    stringBuffer.append(rangePartition.getNumPartition());
    stringBuffer.append(TEXT_339);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_340);
    stringBuffer.append(rangePartition.getUpperBoundary());
    stringBuffer.append(TEXT_341);
     } 
    stringBuffer.append(TEXT_342);
    
				        			} else if(javaType == JavaTypesManager.FLOAT) {
				        			
    stringBuffer.append(TEXT_343);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_344);
    stringBuffer.append(rangePartition.getNumPartition());
    stringBuffer.append(TEXT_345);
    stringBuffer.append(rangePartition.getColumn());
    stringBuffer.append(TEXT_346);
     if(rangePartition.getLowerBoundary() != null && !rangePartition.getLowerBoundary().isEmpty()) { 
    stringBuffer.append(TEXT_347);
    stringBuffer.append(rangePartition.getNumPartition());
    stringBuffer.append(TEXT_348);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_349);
    stringBuffer.append(rangePartition.getLowerBoundary());
    stringBuffer.append(TEXT_350);
     } if(rangePartition.getUpperBoundary() != null && !rangePartition.getUpperBoundary().isEmpty()) { 
    stringBuffer.append(TEXT_351);
    stringBuffer.append(rangePartition.getNumPartition());
    stringBuffer.append(TEXT_352);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_353);
    stringBuffer.append(rangePartition.getUpperBoundary());
    stringBuffer.append(TEXT_354);
     } 
    stringBuffer.append(TEXT_355);
    
				        			} else if(javaType == JavaTypesManager.BOOLEAN) {
				        			
    stringBuffer.append(TEXT_356);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_357);
    stringBuffer.append(rangePartition.getNumPartition());
    stringBuffer.append(TEXT_358);
    stringBuffer.append(rangePartition.getColumn());
    stringBuffer.append(TEXT_359);
     if(rangePartition.getLowerBoundary() != null && !rangePartition.getLowerBoundary().isEmpty()) { 
    stringBuffer.append(TEXT_360);
    stringBuffer.append(rangePartition.getNumPartition());
    stringBuffer.append(TEXT_361);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_362);
    stringBuffer.append(rangePartition.getLowerBoundary());
    stringBuffer.append(TEXT_363);
     } if(rangePartition.getUpperBoundary() != null && !rangePartition.getUpperBoundary().isEmpty()) { 
    stringBuffer.append(TEXT_364);
    stringBuffer.append(rangePartition.getNumPartition());
    stringBuffer.append(TEXT_365);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_366);
    stringBuffer.append(rangePartition.getUpperBoundary());
    stringBuffer.append(TEXT_367);
     } 
    stringBuffer.append(TEXT_368);
    
				        			} else if(javaType == JavaTypesManager.DATE) {
				        			
    stringBuffer.append(TEXT_369);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_370);
    stringBuffer.append(rangePartition.getNumPartition());
    stringBuffer.append(TEXT_371);
    stringBuffer.append(rangePartition.getColumn());
    stringBuffer.append(TEXT_372);
     if(rangePartition.getLowerBoundary() != null && !rangePartition.getLowerBoundary().isEmpty()) { 
    stringBuffer.append(TEXT_373);
    stringBuffer.append(rangePartition.getNumPartition());
    stringBuffer.append(TEXT_374);
    stringBuffer.append(rangePartition.getLowerBoundary());
    stringBuffer.append(TEXT_375);
    stringBuffer.append(rangePartition.getNumPartition());
    stringBuffer.append(TEXT_376);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_377);
    stringBuffer.append(rangePartition.getNumPartition());
    stringBuffer.append(TEXT_378);
     } if(rangePartition.getUpperBoundary() != null && !rangePartition.getUpperBoundary().isEmpty()) { 
    stringBuffer.append(TEXT_379);
    stringBuffer.append(rangePartition.getNumPartition());
    stringBuffer.append(TEXT_380);
    stringBuffer.append(rangePartition.getUpperBoundary());
    stringBuffer.append(TEXT_381);
    stringBuffer.append(rangePartition.getNumPartition());
    stringBuffer.append(TEXT_382);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_383);
    stringBuffer.append(rangePartition.getNumPartition());
    stringBuffer.append(TEXT_384);
     } 
    stringBuffer.append(TEXT_385);
    
				        			} else if(javaType == JavaTypesManager.BIGDECIMAL) {
				        			
    stringBuffer.append(TEXT_386);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_387);
    stringBuffer.append(rangePartition.getNumPartition());
    stringBuffer.append(TEXT_388);
    stringBuffer.append(rangePartition.getColumn());
    stringBuffer.append(TEXT_389);
     if(rangePartition.getLowerBoundary() != null && !rangePartition.getLowerBoundary().isEmpty()) { 
    stringBuffer.append(TEXT_390);
    stringBuffer.append(rangePartition.getNumPartition());
    stringBuffer.append(TEXT_391);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_392);
    stringBuffer.append(rangePartition.getLowerBoundary());
    stringBuffer.append(TEXT_393);
     } if(rangePartition.getUpperBoundary() != null && !rangePartition.getUpperBoundary().isEmpty()) { 
    stringBuffer.append(TEXT_394);
    stringBuffer.append(rangePartition.getNumPartition());
    stringBuffer.append(TEXT_395);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_396);
    stringBuffer.append(rangePartition.getUpperBoundary());
    stringBuffer.append(TEXT_397);
     } 
    stringBuffer.append(TEXT_398);
    
				        			}
				        		}
			        		}
	        			}
	        		}
	            	
	        		if(("CREATE_IF_NOT_EXISTS").equals(tableAction)) {
					
    stringBuffer.append(TEXT_399);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_400);
    stringBuffer.append(tableName);
    stringBuffer.append(TEXT_401);
    
					}
					
    stringBuffer.append(TEXT_402);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_403);
    stringBuffer.append(tableName);
    stringBuffer.append(TEXT_404);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_405);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_406);
    
					if(!listHashPartitions.isEmpty() || !listRangePartitions.isEmpty()) {
						for(int i=0; i<listHashPartitions.size(); i++) {
							String column = listHashPartitions.get(i).getColumn();
							
    stringBuffer.append(TEXT_407);
    stringBuffer.append(column);
    stringBuffer.append(TEXT_408);
    stringBuffer.append(listHashPartitions.get(i).getBuckets());
    stringBuffer.append(TEXT_409);
    
						}
						for(java.util.Map.Entry entry : rangePartitionsGroupByNumPartition.entrySet()) {
						
    stringBuffer.append(TEXT_410);
    stringBuffer.append(entry.getKey());
    stringBuffer.append(TEXT_411);
    stringBuffer.append(entry.getKey());
    stringBuffer.append(TEXT_412);
    
						}
						if(!rangePartitionsGroupByNumPartition.entrySet().isEmpty()) {
						
    stringBuffer.append(TEXT_413);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_414);
    stringBuffer.append(rangePartitionsGroupByNumPartition.entrySet().iterator().next().getKey());
    stringBuffer.append(TEXT_415);
    
						}
					} else {
					
    stringBuffer.append(TEXT_416);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_417);
    
					}
					
    stringBuffer.append(TEXT_418);
    stringBuffer.append(numOfReplicas);
    stringBuffer.append(TEXT_419);
    
				}
				
				if(("INSERT").equals(dataAction)) {
				
    stringBuffer.append(TEXT_420);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_421);
    stringBuffer.append(inRddName);
    stringBuffer.append(TEXT_422);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_423);
    stringBuffer.append(originalStructName);
    stringBuffer.append(TEXT_424);
    
					if(org.talend.hadoop.distribution.ESparkVersion.SPARK_2_0.compareTo(sparkVersion) > 0) {
					
    stringBuffer.append(TEXT_425);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_426);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_427);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_428);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_429);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_430);
    stringBuffer.append(tableName);
    stringBuffer.append(TEXT_431);
    
					} else {
					
    stringBuffer.append(TEXT_432);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_433);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_434);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_435);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_436);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_437);
    stringBuffer.append(tableName);
    stringBuffer.append(TEXT_438);
    
					}
				} else if(("UPDATE").equals(dataAction)) {
				
    stringBuffer.append(TEXT_439);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_440);
    stringBuffer.append(inRddName);
    stringBuffer.append(TEXT_441);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_442);
    stringBuffer.append(originalStructName);
    stringBuffer.append(TEXT_443);
    
					if(org.talend.hadoop.distribution.ESparkVersion.SPARK_2_0.compareTo(sparkVersion) > 0) {
					
    stringBuffer.append(TEXT_444);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_445);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_446);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_447);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_448);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_449);
    stringBuffer.append(tableName);
    stringBuffer.append(TEXT_450);
    
					} else {
					
    stringBuffer.append(TEXT_451);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_452);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_453);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_454);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_455);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_456);
    stringBuffer.append(tableName);
    stringBuffer.append(TEXT_457);
    
					}
				} else if(("UPDATE_OR_INSERT").equals(dataAction)) {
				
    stringBuffer.append(TEXT_458);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_459);
    stringBuffer.append(inRddName);
    stringBuffer.append(TEXT_460);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_461);
    stringBuffer.append(originalStructName);
    stringBuffer.append(TEXT_462);
    
					if(org.talend.hadoop.distribution.ESparkVersion.SPARK_2_0.compareTo(sparkVersion) > 0) {
				    
    stringBuffer.append(TEXT_463);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_464);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_465);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_466);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_467);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_468);
    stringBuffer.append(tableName);
    stringBuffer.append(TEXT_469);
    
					} else {
					
    stringBuffer.append(TEXT_470);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_471);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_472);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_473);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_474);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_475);
    stringBuffer.append(tableName);
    stringBuffer.append(TEXT_476);
    
					}
				} else if(("DELETE").equals(dataAction)) {
				
    stringBuffer.append(TEXT_477);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_478);
    stringBuffer.append(inRddName);
    stringBuffer.append(TEXT_479);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_480);
    stringBuffer.append(originalStructName);
    stringBuffer.append(TEXT_481);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_482);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_483);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_484);
    
					if(org.talend.hadoop.distribution.ESparkVersion.SPARK_2_0.compareTo(sparkVersion) > 0) {
					
    stringBuffer.append(TEXT_485);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_486);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_487);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_488);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_489);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_490);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_491);
    stringBuffer.append(tableName);
    stringBuffer.append(TEXT_492);
    
					} else {
					
    stringBuffer.append(TEXT_493);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_494);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_495);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_496);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_497);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_498);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_499);
    stringBuffer.append(tableName);
    stringBuffer.append(TEXT_500);
    
					}
				}
				
    stringBuffer.append(TEXT_501);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_502);
    
				if(dieOnError) {
				
    stringBuffer.append(TEXT_503);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_504);
    
				} else {
					if(isLog4jEnabled){
					
    stringBuffer.append(TEXT_505);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_506);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_507);
    
					}
					
    stringBuffer.append(TEXT_508);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_509);
    
				}
				
    stringBuffer.append(TEXT_510);
    
		}
	}
}

    stringBuffer.append(TEXT_511);
    return stringBuffer.toString();
  }
}
