package org.talend.designer.codegen.translators.common;

import java.util.Vector;
import org.talend.core.model.process.ElementParameterParser;
import org.talend.core.model.process.INode;
import org.talend.core.model.process.IProcess;
import org.talend.designer.codegen.config.CodeGeneratorArgument;
import org.talend.designer.codegen.config.NodesSubTree;

public class Sparkstreaming_checkpoint_subprocess_footerJava
{
  protected static String nl;
  public static synchronized Sparkstreaming_checkpoint_subprocess_footerJava create(String lineSeparator)
  {
    nl = lineSeparator;
    Sparkstreaming_checkpoint_subprocess_footerJava result = new Sparkstreaming_checkpoint_subprocess_footerJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "";
  protected final String TEXT_2 = NL + "\t\treturn ctx;";
  protected final String TEXT_3 = NL + NL + "\t    } catch(java.lang.Exception e) {" + NL + "\t    \tthrow e;" + NL + "\t    }" + NL + "    }";

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    
    CodeGeneratorArgument codeGenArgument = (CodeGeneratorArgument) argument;
    Vector v = (Vector) codeGenArgument.getArgument();
    NodesSubTree subTree = (NodesSubTree)v.get(1);

    stringBuffer.append(TEXT_1);
    
    if(!subTree.getName().startsWith("tHadoopConfManager")) {

    stringBuffer.append(TEXT_2);
    
    }

    stringBuffer.append(TEXT_3);
    return stringBuffer.toString();
  }
}
