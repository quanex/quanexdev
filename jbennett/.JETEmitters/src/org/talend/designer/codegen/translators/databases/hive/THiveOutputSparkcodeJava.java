package org.talend.designer.codegen.translators.databases.hive;

import org.talend.core.model.process.INode;
import org.talend.core.model.process.IConnection;
import org.talend.core.model.metadata.IMetadataColumn;
import org.talend.core.model.process.ElementParameterParser;
import org.talend.designer.common.BigDataCodeGeneratorArgument;
import org.talend.designer.common.tsqlrow.TSqlRowUtil;

public class THiveOutputSparkcodeJava
{
  protected static String nl;
  public static synchronized THiveOutputSparkcodeJava create(String lineSeparator)
  {
    nl = lineSeparator;
    THiveOutputSparkcodeJava result = new THiveOutputSparkcodeJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = NL + "public static class ";
  protected final String TEXT_2 = "_From";
  protected final String TEXT_3 = "To";
  protected final String TEXT_4 = " implements org.apache.spark.api.java.function.Function<";
  protected final String TEXT_5 = ", ";
  protected final String TEXT_6 = "> {" + NL + "" + NL + "\tpublic ";
  protected final String TEXT_7 = " call(";
  protected final String TEXT_8 = " input) {" + NL + "\t\t";
  protected final String TEXT_9 = " result = new ";
  protected final String TEXT_10 = "();";
  protected final String TEXT_11 = NL + "\t\t\t\tif(input.";
  protected final String TEXT_12 = " != null) {" + NL + "\t\t\t\t\tresult.";
  protected final String TEXT_13 = " = new java.sql.";
  protected final String TEXT_14 = "(input.";
  protected final String TEXT_15 = ".getTime());" + NL + "\t\t\t\t} else {" + NL + "\t\t\t\t\tresult.";
  protected final String TEXT_16 = " = null;" + NL + "\t\t\t\t}";
  protected final String TEXT_17 = NL + "\t\tresult.";
  protected final String TEXT_18 = " = input.";
  protected final String TEXT_19 = ";";
  protected final String TEXT_20 = NL + "\t\treturn result;" + NL + "\t}" + NL + "}";
  protected final String TEXT_21 = NL + "\tpublic static class ";
  protected final String TEXT_22 = "_";
  protected final String TEXT_23 = "ToRow implements org.apache.spark.api.java.function.Function<";
  protected final String TEXT_24 = ", org.apache.spark.sql.Row> {" + NL + "    \tpublic org.apache.spark.sql.Row call(";
  protected final String TEXT_25 = " input) {" + NL + "    \t\treturn org.apache.spark.sql.RowFactory.create(";
  protected final String TEXT_26 = ", ";
  protected final String TEXT_27 = NL + "\t\t\t\t\tinput.";
  protected final String TEXT_28 = NL + "\t\t\t);" + NL + "\t\t}" + NL + "\t}";
  protected final String TEXT_29 = NL;

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    
BigDataCodeGeneratorArgument codeGenArgument = (BigDataCodeGeneratorArgument)argument;
INode node = (INode)codeGenArgument.getArgument();
String cid = node.getUniqueName();
TSqlRowUtil tSqlRowUtil = new TSqlRowUtil(node);
String validateError = tSqlRowUtil.validate(true, false);
if (validateError != null) {
    // Cause the job compilation to explicitly fail if there is a problem.
    return "throw new JobConfigurationError(\"" + validateError +"\");";
}

final boolean useTimestampForDatesInDataframes = ElementParameterParser.getBooleanValue(node, "__DATE_TO_TIMESTAMP_DF_TYPE_SUBSTITUTION__");


// If the incoming rowStruct contains a Date field (always typed as java.util.Date),
// we must generate a new structure which replaces these java.util.Date instances by
// java.sql.Date or java.sql.Timestamp instances.

org.talend.designer.bigdata.avro.AvroRecordStructGenerator avroRecordStructGenerator = (org.talend.designer.bigdata.avro.AvroRecordStructGenerator) codeGenArgument.getRecordStructGenerator();

for(IConnection incomingConnection : tSqlRowUtil.getIncomingConnections()) {
	java.util.List<IMetadataColumn> columns = tSqlRowUtil.getColumns(incomingConnection);
	String originalStructName = codeGenArgument.getRecordStructName(incomingConnection);
	String inStructName = originalStructName;
	if(tSqlRowUtil.containsDateFields(incomingConnection)) {
		String suggestedDfStructName = "DF_"+originalStructName;
		String dfStructName = avroRecordStructGenerator.generateRecordStructForDataFrame(suggestedDfStructName, originalStructName, useTimestampForDatesInDataframes);
		inStructName = dfStructName;

    stringBuffer.append(TEXT_1);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_2);
    stringBuffer.append(originalStructName);
    stringBuffer.append(TEXT_3);
    stringBuffer.append(dfStructName);
    stringBuffer.append(TEXT_4);
    stringBuffer.append(originalStructName);
    stringBuffer.append(TEXT_5);
    stringBuffer.append(dfStructName);
    stringBuffer.append(TEXT_6);
    stringBuffer.append(dfStructName);
    stringBuffer.append(TEXT_7);
    stringBuffer.append(originalStructName);
    stringBuffer.append(TEXT_8);
    stringBuffer.append(dfStructName);
    stringBuffer.append(TEXT_9);
    stringBuffer.append(dfStructName);
    stringBuffer.append(TEXT_10);
    
		for(IMetadataColumn column : columns) {
			if(tSqlRowUtil.isDateField(column)) {

    stringBuffer.append(TEXT_11);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_12);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_13);
    stringBuffer.append(useTimestampForDatesInDataframes ? "Timestamp" : "Date");
    stringBuffer.append(TEXT_14);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_15);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_16);
    
			} else {

    stringBuffer.append(TEXT_17);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_18);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_19);
    
			}
		} // end for(IMetadataColumn column : columns)

    stringBuffer.append(TEXT_20);
    
	} // end if(tSqlRowUtil.containsDateFields(incomingConnection))

    stringBuffer.append(TEXT_21);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_22);
    stringBuffer.append(originalStructName);
    stringBuffer.append(TEXT_23);
    stringBuffer.append(inStructName);
    stringBuffer.append(TEXT_24);
    stringBuffer.append(inStructName);
    stringBuffer.append(TEXT_25);
     
				boolean isFirst = true;
				java.util.List<IMetadataColumn> columnsCopy = new java.util.ArrayList<IMetadataColumn>(columns);
				java.util.Collections.sort(columnsCopy, new java.util.Comparator<IMetadataColumn>() {
					@Override
					public int compare(IMetadataColumn c1, IMetadataColumn c2) {
						return c1.getLabel().compareTo(c2.getLabel());
					}
				});
				for(IMetadataColumn column : columnsCopy) {
					if(!isFirst) {
                    	
    stringBuffer.append(TEXT_26);
    
                    }
                    isFirst = false; 

    stringBuffer.append(TEXT_27);
    stringBuffer.append(column.getLabel());
     
				} 

    stringBuffer.append(TEXT_28);
    
} // end for(IConnection incomingConnection : tSqlRowUtil.getIncomingConnections())


    stringBuffer.append(TEXT_29);
    return stringBuffer.toString();
  }
}
