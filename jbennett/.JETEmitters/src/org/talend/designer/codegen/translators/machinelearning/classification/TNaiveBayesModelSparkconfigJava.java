package org.talend.designer.codegen.translators.machinelearning.classification;

import org.talend.core.model.metadata.IMetadataColumn;
import org.talend.core.model.metadata.IMetadataTable;
import org.talend.core.model.metadata.types.JavaType;
import org.talend.core.model.metadata.types.JavaTypesManager;
import org.talend.core.model.process.ElementParameterParser;
import org.talend.core.model.process.EConnectionType;
import org.talend.core.model.process.IConnection;
import org.talend.core.model.process.IConnectionCategory;
import org.talend.core.model.process.INode;
import org.talend.core.model.process.IBigDataNode;
import org.talend.designer.common.BigDataCodeGeneratorArgument;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.talend.designer.common.tmodelencoder.TModelEncoderUtil;

public class TNaiveBayesModelSparkconfigJava
{
  protected static String nl;
  public static synchronized TNaiveBayesModelSparkconfigJava create(String lineSeparator)
  {
    nl = lineSeparator;
    TNaiveBayesModelSparkconfigJava result = new TNaiveBayesModelSparkconfigJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "";
  protected final String TEXT_2 = NL + NL + "Map<String, String> paramsMap_";
  protected final String TEXT_3 = " = new java.util.HashMap<String, String>();" + NL + "paramsMap_";
  protected final String TEXT_4 = ".put(\"VECTOR_NAME\", \"";
  protected final String TEXT_5 = "\");" + NL + "" + NL + "//the StringIndexer is required here. it will be used by LabeledPoint" + NL + "// 1. if it's a string column, this func will convert it to double column," + NL + "// 2. if it's already a double column, this func will transform to a 0-based double column" + NL + "org.apache.spark.ml.feature.StringIndexer s2i_";
  protected final String TEXT_6 = " =" + NL + "new org.apache.spark.ml.feature.StringIndexer()" + NL + "    .setInputCol(\"";
  protected final String TEXT_7 = "\")" + NL + "    .setOutputCol(\"indexedLabel\");";
  protected final String TEXT_8 = NL;
  protected final String TEXT_9 = ".add(s2i_";
  protected final String TEXT_10 = ");" + NL + "" + NL + "// Retrieve and use the transformation pipeline" + NL + "org.apache.spark.ml.Pipeline featuresTransformationsPipeline_";
  protected final String TEXT_11 = " = new org.apache.spark.ml.Pipeline()" + NL + "    .setStages(";
  protected final String TEXT_12 = NL + "    .toArray(new org.apache.spark.ml.PipelineStage[";
  protected final String TEXT_13 = ".size()]));" + NL + "org.apache.spark.ml.PipelineModel featuresTransformationsModel_";
  protected final String TEXT_14 = " = featuresTransformationsPipeline_";
  protected final String TEXT_15 = ".fit(";
  protected final String TEXT_16 = ");";
  protected final String TEXT_17 = NL;
  protected final String TEXT_18 = " transformedFeatures_";
  protected final String TEXT_19 = " = featuresTransformationsModel_";
  protected final String TEXT_20 = ".transform(";
  protected final String TEXT_21 = ");" + NL + "" + NL + "// retrieve the vector RDD" + NL + "org.apache.spark.api.java.JavaRDD<";
  protected final String TEXT_22 = "> temporaryrdd_";
  protected final String TEXT_23 = " = transformedFeatures_";
  protected final String TEXT_24 = ".toJavaRDD().map(new GetLabledPoint_";
  protected final String TEXT_25 = "());" + NL + "temporaryrdd_";
  protected final String TEXT_26 = ".cache();" + NL + "" + NL + "//create the NaiveBayes object" + NL + "org.apache.spark.mllib.classification.NaiveBayes naiveBayes_";
  protected final String TEXT_27 = " = new org.apache.spark.mllib.classification.NaiveBayes();" + NL + "" + NL + "//Launch the training and retreive the model" + NL + "org.apache.spark.mllib.classification.NaiveBayesModel naiveBayesModel_";
  protected final String TEXT_28 = " = naiveBayes_";
  protected final String TEXT_29 = ".run(temporaryrdd_";
  protected final String TEXT_30 = ".rdd());" + NL + "" + NL + "//Saving" + NL + "TalendPipelineModel featuresTalendPipelineModel_";
  protected final String TEXT_31 = " = new TalendPipelineModel(featuresTransformationsModel_";
  protected final String TEXT_32 = ", paramsMap_";
  protected final String TEXT_33 = ");";
  protected final String TEXT_34 = NL + NL + "//save on global map" + NL + "globalMap.put(\"";
  protected final String TEXT_35 = "_MODEL\", naiveBayesModel_";
  protected final String TEXT_36 = ");" + NL + "globalMap.put(\"";
  protected final String TEXT_37 = "_PIPELINE\", featuresTalendPipelineModel_";
  protected final String TEXT_38 = ");" + NL;
  protected final String TEXT_39 = NL + "        java.net.URI currentURI_";
  protected final String TEXT_40 = "_config = FileSystem.getDefaultUri(ctx.hadoopConfiguration());" + NL + "        FileSystem.setDefaultUri(ctx.hadoopConfiguration(), new java.net.URI(";
  protected final String TEXT_41 = "));" + NL + "        fs = FileSystem.get(ctx.hadoopConfiguration());";
  protected final String TEXT_42 = NL + "    Path pathToDelete_";
  protected final String TEXT_43 = " = new Path(";
  protected final String TEXT_44 = ");" + NL + "    if (fs.exists(pathToDelete_";
  protected final String TEXT_45 = ")) {" + NL + "        fs.delete(pathToDelete_";
  protected final String TEXT_46 = ", true);" + NL + "    }" + NL + "" + NL + " // Serialize the model" + NL + "    com.esotericsoftware.kryo.Kryo kryo_";
  protected final String TEXT_47 = " = new com.esotericsoftware.kryo.Kryo();" + NL + "    com.esotericsoftware.kryo.io.Output featuresOutput_";
  protected final String TEXT_48 = " = new com.esotericsoftware.kryo.io.Output(" + NL + "    fs.create(new org.apache.hadoop.fs.Path( ";
  protected final String TEXT_49 = " + \"/features\")));" + NL + "    kryo_";
  protected final String TEXT_50 = ".writeObject(featuresOutput_";
  protected final String TEXT_51 = ", featuresTalendPipelineModel_";
  protected final String TEXT_52 = ", new TalendPipelineModelSerializer());" + NL + "    featuresOutput_";
  protected final String TEXT_53 = ".close();" + NL + "    naiveBayesModel_";
  protected final String TEXT_54 = ".save(ctx.sc(), ";
  protected final String TEXT_55 = " + \"/model\");";
  protected final String TEXT_56 = NL + "        FileSystem.setDefaultUri(ctx.hadoopConfiguration(), currentURI_";
  protected final String TEXT_57 = "_config);" + NL + "        fs = FileSystem.get(ctx.hadoopConfiguration());";
  protected final String TEXT_58 = NL + NL + "org.apache.spark.sql.SQLContext sqlContext_";
  protected final String TEXT_59 = " = new org.apache.spark.sql.SQLContext(ctx);" + NL + "" + NL + "// Create dataFrame from incoming rdd & rowStruct";
  protected final String TEXT_60 = NL;
  protected final String TEXT_61 = " df_";
  protected final String TEXT_62 = "_";
  protected final String TEXT_63 = " =" + NL + "\tsqlContext_";
  protected final String TEXT_64 = ".createDataFrame(rdd_";
  protected final String TEXT_65 = ", ";
  protected final String TEXT_66 = ".class);" + NL + "" + NL + "// Create dictionary" + NL + "List<scala.Tuple3<String, org.talend.datascience.types.DataMiningType, String[]>> dictionary_";
  protected final String TEXT_67 = NL + "\t= new ArrayList<scala.Tuple3<String, org.talend.datascience.types.DataMiningType, String[]>>();";
  protected final String TEXT_68 = NL + "\t\t// add the feature column: ";
  protected final String TEXT_69 = " in dictionary" + NL + "\t\tdictionary_";
  protected final String TEXT_70 = ".add(" + NL + "\t\t\tnew scala.Tuple3<String, org.talend.datascience.types.DataMiningType, String[]>(" + NL + "\t\t\t\t\"";
  protected final String TEXT_71 = "\"," + NL + "\t\t\t\tnew org.talend.datascience.types.";
  protected final String TEXT_72 = "()," + NL + "\t\t\t\t";
  protected final String TEXT_73 = ".split(\";\")" + NL + "\t\t\t)" + NL + "\t\t);";
  protected final String TEXT_74 = NL + "\t// add the label in the end of dictionary" + NL + "\tdictionary_";
  protected final String TEXT_75 = ".add(" + NL + "\t\tnew scala.Tuple3<String, org.talend.datascience.types.DataMiningType, String[]>(" + NL + "\t\t\t\t\"";
  protected final String TEXT_76 = "\"," + NL + "\t\t\t\tnew org.talend.datascience.types.Categorical()," + NL + "\t\t\t\t";
  protected final String TEXT_77 = ".split(\";\")" + NL + "\t\t)" + NL + "\t);" + NL + "" + NL + "\t// Call Naive Bayes modelBuilder for training & evaluate the model" + NL + "\t@SuppressWarnings(\"unchecked\")" + NL + "\torg.apache.spark.mllib.classification.talend.NaiveBayesRegularRecordsModel model_";
  protected final String TEXT_78 = " =" + NL + "\t\torg.talend.datascience.mllib.classification.NaiveBayes.modelBuilder(" + NL + "\t\t\tdf_";
  protected final String TEXT_79 = "_";
  protected final String TEXT_80 = ".rdd(), // rdd<Row>" + NL + "\t\t\tdf_";
  protected final String TEXT_81 = "_";
  protected final String TEXT_82 = ".schema(), // schema" + NL + "\t\t\t";
  protected final String TEXT_83 = ", // training percentage" + NL + "\t\t\tdictionary_";
  protected final String TEXT_84 = "// dictionary" + NL + "\t\t\t\t.toArray((scala.Tuple3<String, org.talend.datascience.types.DataMiningType, String[]>[]) new scala.Tuple3[dictionary_";
  protected final String TEXT_85 = ".size()])" + NL + "\t\t);" + NL + "" + NL + "\t// Export model in PMML format" + NL + "\t";
  protected final String TEXT_86 = NL + "\t        org.talend.datascience.mllib.pmml.exports.ModelExporter.toPMML(ctx.sc(), model_";
  protected final String TEXT_87 = ", ";
  protected final String TEXT_88 = " +\"/\"+ ";
  protected final String TEXT_89 = ");" + NL + "\t";
  protected final String TEXT_90 = NL + "            org.talend.datascience.mllib.pmml.exports.ModelExporter.toPMML(model_";
  protected final String TEXT_91 = ", ";
  protected final String TEXT_92 = " +\"/\"+ ";
  protected final String TEXT_93 = ");" + NL + "\t";
  protected final String TEXT_94 = NL;
  protected final String TEXT_95 = NL + "\tthrow new java.lang.Exception(\"Error: illegal number of Label! Should have one and only one input column mark as Label! Please check the Columns type table in ";
  protected final String TEXT_96 = ".\");";

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    
final String modelType = ElementParameterParser.getValue((INode) ((BigDataCodeGeneratorArgument) argument).getArgument(), "__SPARK_VERSION__");

// NAIVEBAYES sparkcode
if("SPARK_VERSION_1.4+".equals(modelType)){

    stringBuffer.append(TEXT_1);
    
//Parse the inputs to this javajet generator.
final BigDataCodeGeneratorArgument codeGenArgument = (BigDataCodeGeneratorArgument) argument;
final INode node = (INode)codeGenArgument.getArgument();
final String cid = node.getUniqueName();
final boolean isLog4jEnabled = ("true").equals(ElementParameterParser.getValue(node.getProcess(), "__LOG4J_ACTIVATE__"));
final String lebleColumn= ElementParameterParser.getValue(node, "__LABEL_COLUMN__");

final String dataframeClass = org.talend.hadoop.distribution.ESparkVersion.SPARK_2_0.compareTo(codeGenArgument.getSparkVersion()) > 0
  ? "org.apache.spark.sql.DataFrame"
  : "org.apache.spark.sql.Dataset<org.apache.spark.sql.Row>";

final String labledPointClass="org.apache.spark.mllib.regression.LabeledPoint";
 
TModelEncoderUtil tModelEncoderUtil = new TModelEncoderUtil(node);
IConnection conn = null;
List<? extends IConnection> conns = node.getIncomingConnections();
if(conns != null && conns.size() > 0 && conns.get(0).getLineStyle().hasConnectionCategory(IConnectionCategory.DATA)){
  conn = conns.get(0);
}

if (conn == null) {
  return "";
}

String inRowStruct = codeGenArgument.getRecordStructName(conn);
String connName = conn.getName();

String trainingDataFrame = "training_" + tModelEncoderUtil.getFirstModelEncoderName();
String modelEncoderTransformationsList = "stages_" + tModelEncoderUtil.getFirstModelEncoderName();

String featuresVector = ElementParameterParser.getValue(node, "__FEATURES_COLUMN__");


    stringBuffer.append(TEXT_2);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_3);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_4);
    stringBuffer.append(featuresVector);
    stringBuffer.append(TEXT_5);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_6);
    stringBuffer.append(lebleColumn);
    stringBuffer.append(TEXT_7);
    stringBuffer.append(TEXT_8);
    stringBuffer.append(modelEncoderTransformationsList);
    stringBuffer.append(TEXT_9);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_10);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_11);
    stringBuffer.append(modelEncoderTransformationsList);
    stringBuffer.append(TEXT_12);
    stringBuffer.append(modelEncoderTransformationsList);
    stringBuffer.append(TEXT_13);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_14);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_15);
    stringBuffer.append(trainingDataFrame);
    stringBuffer.append(TEXT_16);
    stringBuffer.append(TEXT_17);
    stringBuffer.append(dataframeClass);
    stringBuffer.append(TEXT_18);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_19);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_20);
    stringBuffer.append(trainingDataFrame);
    stringBuffer.append(TEXT_21);
    stringBuffer.append(labledPointClass);
    stringBuffer.append(TEXT_22);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_23);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_24);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_25);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_26);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_27);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_28);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_29);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_30);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_31);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_32);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_33);
    
Boolean saveOnDisk = ElementParameterParser.getBooleanValue(node, "__SAVE_ON_DISK__");
String modelPath = ElementParameterParser.getValue(node, "__MODEL_PATH__");
boolean useConfigurationComponent = "true".equals(ElementParameterParser.getValue(node, "__DEFINE_STORAGE_CONFIGURATION__"));
String uriPrefix = "\"\"";
if(useConfigurationComponent) {
    uriPrefix = org.talend.designer.spark.generator.storage.SparkStorageUtils.getURIPrefix(node);
    modelPath = uriPrefix + " + " + modelPath;
}

    stringBuffer.append(TEXT_34);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_35);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_36);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_37);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_38);
    if (saveOnDisk) {
    if(!"\"\"".equals(uriPrefix)) {
        
    stringBuffer.append(TEXT_39);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_40);
    stringBuffer.append(uriPrefix);
    stringBuffer.append(TEXT_41);
    
    }

    stringBuffer.append(TEXT_42);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_43);
    stringBuffer.append(modelPath);
    stringBuffer.append(TEXT_44);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_45);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_46);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_47);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_48);
    stringBuffer.append(modelPath);
    stringBuffer.append(TEXT_49);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_50);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_51);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_52);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_53);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_54);
    stringBuffer.append(modelPath);
    stringBuffer.append(TEXT_55);
    
    if(!"\"\"".equals(uriPrefix)) {
        
    stringBuffer.append(TEXT_56);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_57);
    
    }
}

    
}else{
    
// Parse the inputs to this javajet generator.
final BigDataCodeGeneratorArgument codeGenArgument = (BigDataCodeGeneratorArgument) argument;
final INode node = (INode) codeGenArgument.getArgument();
final String cid = node.getUniqueName();

final String dataframeClass = org.talend.hadoop.distribution.ESparkVersion.SPARK_2_0.compareTo(codeGenArgument.getSparkVersion()) > 0
    ? "org.apache.spark.sql.DataFrame"
    : "org.apache.spark.sql.DataFrame";//keep using dataFrame even on spark 2.0

    
IConnection inConn = node.getIncomingConnections().get(0);
String inConnTypeName = codeGenArgument.getRecordStructName(inConn);

String trainingPercentage = ElementParameterParser.getValue(node, "__TRAIN_PERCENTAGE__");
String pmmlModelPath = ElementParameterParser.getValue(node, "__PMML_MODEL_PATH__");
String pmmlModelName = ElementParameterParser.getValue(node, "__PMML_MODEL_NAME__");
List<Map<String, String>> listMapColType = (List<Map<String,String>>)ElementParameterParser.getObjectValue(node, "__COLUMNS_TYPE__");
boolean useConfigurationComponent = "true".equals(ElementParameterParser.getValue(node, "__DEFINE_STORAGE_CONFIGURATION__"));

    stringBuffer.append(TEXT_58);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_59);
    stringBuffer.append(TEXT_60);
    stringBuffer.append(dataframeClass);
    stringBuffer.append(TEXT_61);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_62);
    stringBuffer.append(inConn.getName());
    stringBuffer.append(TEXT_63);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_64);
    stringBuffer.append(inConn.getName());
    stringBuffer.append(TEXT_65);
    stringBuffer.append(inConnTypeName);
    stringBuffer.append(TEXT_66);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_67);
    
Boolean flagLabel = false;
String strLabelVect = "";
String labelName = "";

for (Map<String, String> mapCol : listMapColType){
	String miningType = "";
	String strValuesVect = "";
	String columnName = mapCol.get("SCHEMA_COLUMN");
	String colUsage = mapCol.get("COLUMNS_USAGE");
	//match miningType & strValuesVect for the column
	if ("FEATURE_CONTINUOUS".equals(colUsage)){
		miningType = "Continuous";
		strValuesVect = mapCol.get("BINS_VECTOR");
	}// end if ("FEATURE_CONTINUOUS".equals(colUsage))
	if ("FEATURE_CATEGORICAL".equals(colUsage)){
		miningType = "Categorical";
		strValuesVect = mapCol.get("CATEGORIES_VECTOR");
	}// end if ("FEATURE_CATEGORICAL".equals(colUsage))
	if ("LABEL".equals(colUsage)){
		if(flagLabel){// Illegal case: already have one column mark as Label
			flagLabel = !flagLabel;
			break;
		} else{
			flagLabel = !flagLabel;
			labelName = columnName;
			strLabelVect = mapCol.get("LABEL_VECTOR");
		}
	}// end if ("LABEL".equals(colUsage))
	if(miningType != "" && strValuesVect != ""){// if it's not a unused column or label column

    stringBuffer.append(TEXT_68);
    stringBuffer.append(columnName);
    stringBuffer.append(TEXT_69);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_70);
    stringBuffer.append(columnName);
    stringBuffer.append(TEXT_71);
    stringBuffer.append(miningType);
    stringBuffer.append(TEXT_72);
    stringBuffer.append(strValuesVect);
    stringBuffer.append(TEXT_73);
    
	}// end if(miningType != null && strValuesVect != null && !flagLabel)
}// end for (Map<String, String> mapCol : listMapColType)
if(flagLabel){

    stringBuffer.append(TEXT_74);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_75);
    stringBuffer.append(labelName);
    stringBuffer.append(TEXT_76);
    stringBuffer.append(strLabelVect);
    stringBuffer.append(TEXT_77);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_78);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_79);
    stringBuffer.append(inConn.getName());
    stringBuffer.append(TEXT_80);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_81);
    stringBuffer.append(inConn.getName());
    stringBuffer.append(TEXT_82);
    stringBuffer.append(trainingPercentage);
    stringBuffer.append(TEXT_83);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_84);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_85);
    
	    if(useConfigurationComponent){// export to dfs
	
    stringBuffer.append(TEXT_86);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_87);
    stringBuffer.append(pmmlModelPath);
    stringBuffer.append(TEXT_88);
    stringBuffer.append(pmmlModelName);
    stringBuffer.append(TEXT_89);
    
	    }else{// export to Local FS
	
    stringBuffer.append(TEXT_90);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_91);
    stringBuffer.append(pmmlModelPath);
    stringBuffer.append(TEXT_92);
    stringBuffer.append(pmmlModelName);
    stringBuffer.append(TEXT_93);
    
	    }
	
    stringBuffer.append(TEXT_94);
    
}else{

    stringBuffer.append(TEXT_95);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_96);
    
}// end if(flagLabel) else..

    
}

    return stringBuffer.toString();
  }
}
