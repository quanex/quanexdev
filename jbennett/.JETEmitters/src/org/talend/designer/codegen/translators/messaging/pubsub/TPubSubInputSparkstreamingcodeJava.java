package org.talend.designer.codegen.translators.messaging.pubsub;

import java.util.Map.Entry;
import org.talend.core.model.process.ElementParameterParser;
import org.talend.core.model.process.IConnection;
import org.talend.core.model.process.INode;
import org.talend.designer.common.BigDataCodeGeneratorArgument;

public class TPubSubInputSparkstreamingcodeJava
{
  protected static String nl;
  public static synchronized TPubSubInputSparkstreamingcodeJava create(String lineSeparator)
  {
    nl = lineSeparator;
    TPubSubInputSparkstreamingcodeJava result = new TPubSubInputSparkstreamingcodeJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = NL + "public static class ";
  protected final String TEXT_2 = "_MapToOutputStruct implements org.apache.spark.api.java.function.Function<org.apache.spark.streaming.pubsub.SparkPubsubMessage, ";
  protected final String TEXT_3 = "> {" + NL + "    @Override" + NL + "    public ";
  protected final String TEXT_4 = " call(org.apache.spark.streaming.pubsub.SparkPubsubMessage input) throws Exception {" + NL + "    \t// System.out.println(\"isStringConnectionType: \"+";
  protected final String TEXT_5 = ");";
  protected final String TEXT_6 = NL + "        ";
  protected final String TEXT_7 = " output = new ";
  protected final String TEXT_8 = "();" + NL + "      \t";
  protected final String TEXT_9 = NL + "\t\t   output.payload = new String(input.getData(), \"UTF-8\");" + NL + "\t\t";
  protected final String TEXT_10 = NL + "\t\t\toutput.payload = java.nio.ByteBuffer.wrap(input.getData());" + NL + "\t\t";
  protected final String TEXT_11 = "\t" + NL + "\t    return output;" + NL + "    }" + NL + "}";
  protected final String TEXT_12 = NL;

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    
BigDataCodeGeneratorArgument codeGenArgument = (BigDataCodeGeneratorArgument) argument;
INode node = (INode)codeGenArgument.getArgument();
String cid = node.getUniqueName();
IConnection conn = node.getOutgoingConnections().get(0);
String connectionTypeName = codeGenArgument.getRecordStructName(conn);
String connName = conn.getUniqueName();
String typeConnection = ElementParameterParser.getValue(node, "__OUTPUT_TYPE__");
boolean isStringConnectionType = "STRING".equals(typeConnection.toUpperCase());


    stringBuffer.append(TEXT_1);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_2);
    stringBuffer.append(connectionTypeName);
    stringBuffer.append(TEXT_3);
    stringBuffer.append(connectionTypeName);
    stringBuffer.append(TEXT_4);
    stringBuffer.append(isStringConnectionType);
    stringBuffer.append(TEXT_5);
    stringBuffer.append(TEXT_6);
    stringBuffer.append(connectionTypeName);
    stringBuffer.append(TEXT_7);
    stringBuffer.append(connectionTypeName);
    stringBuffer.append(TEXT_8);
     
			if (isStringConnectionType) { 
		
    stringBuffer.append(TEXT_9);
    
		} else { 
		
    stringBuffer.append(TEXT_10);
     
			} 
		
    stringBuffer.append(TEXT_11);
    stringBuffer.append(TEXT_12);
    return stringBuffer.toString();
  }
}
