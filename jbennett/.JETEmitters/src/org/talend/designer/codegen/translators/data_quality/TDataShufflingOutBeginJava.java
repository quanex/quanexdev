package org.talend.designer.codegen.translators.data_quality;

import org.talend.core.model.process.INode;
import org.talend.core.model.process.ElementParameterParser;
import org.talend.core.model.metadata.IMetadataTable;
import org.talend.core.model.metadata.IMetadataColumn;
import org.talend.core.model.process.IConnection;
import org.talend.core.model.process.IConnectionCategory;
import org.talend.designer.codegen.config.CodeGeneratorArgument;
import org.talend.core.model.metadata.types.JavaTypesManager;
import org.talend.core.model.metadata.types.JavaType;
import org.talend.core.model.process.EConnectionType;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Iterator;

public class TDataShufflingOutBeginJava
{
  protected static String nl;
  public static synchronized TDataShufflingOutBeginJava create(String lineSeparator)
  {
    nl = lineSeparator;
    TDataShufflingOutBeginJava result = new TDataShufflingOutBeginJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "\t\tint nb_line_";
  protected final String TEXT_2 = " = 1;" + NL + "\t\t" + NL + "\t\t// generate the columns' name from input" + NL + "\t\tList<String> allcolumns_";
  protected final String TEXT_3 = " = new java.util.ArrayList<String>();" + NL + "\t";
  protected final String TEXT_4 = "\t\t" + NL + "\t\tallcolumns_";
  protected final String TEXT_5 = ".add(\"";
  protected final String TEXT_6 = "\");" + NL + "\t";
  protected final String TEXT_7 = NL + "\t\t" + NL + "\t\tList<List<String>> shufflingColumns_";
  protected final String TEXT_8 = " = new java.util.ArrayList<List<String>>();" + NL + "\t";
  protected final String TEXT_9 = NL + "\t\t" + NL + "\t\tshufflingColumns_";
  protected final String TEXT_10 = ".get(";
  protected final String TEXT_11 = ").add(\"";
  protected final String TEXT_12 = "\");" + NL + "\t";
  protected final String TEXT_13 = NL + "\t\tList<String> newList_";
  protected final String TEXT_14 = " = new java.util.ArrayList<String>();" + NL + "\t\tnewList_";
  protected final String TEXT_15 = ".add(\"";
  protected final String TEXT_16 = "\");" + NL + "\t\tshufflingColumns_";
  protected final String TEXT_17 = ".add(newList_";
  protected final String TEXT_18 = ");" + NL + "\t\t\t\t\t\t" + NL + "\t";
  protected final String TEXT_19 = NL + "\t\t" + NL + "\t\t// start the partition columns" + NL + "\t\tList<String> partitionColumns_";
  protected final String TEXT_20 = " = new java.util.ArrayList<String>();" + NL + "\t";
  protected final String TEXT_21 = NL + "\t\tpartitionColumns_";
  protected final String TEXT_22 = " = null;" + NL + "\t";
  protected final String TEXT_23 = NL + "\t\tpartitionColumns_";
  protected final String TEXT_24 = ".add(\"";
  protected final String TEXT_25 = "\");" + NL + "\t";
  protected final String TEXT_26 = NL + "\t" + NL + "\t    //Initializes ShufflingService and sets all the necessary variables (partition size, ShufflingHandler, random size)" + NL + "\t    org.talend.dataquality.datamasking.shuffling.ShufflingService shufflingService_";
  protected final String TEXT_27 = " = null;" + NL + "\t    if(partitionColumns_";
  protected final String TEXT_28 = " == null){" + NL + "\t    \tshufflingService_";
  protected final String TEXT_29 = " = new org.talend.dataquality.datamasking.shuffling.ShufflingService(shufflingColumns_";
  protected final String TEXT_30 = ", allcolumns_";
  protected final String TEXT_31 = ");" + NL + "\t    }else{" + NL + "\t    \tshufflingService_";
  protected final String TEXT_32 = " = new org.talend.dataquality.datamasking.shuffling.ShufflingService(shufflingColumns_";
  protected final String TEXT_33 = ", allcolumns_";
  protected final String TEXT_34 = ", partitionColumns_";
  protected final String TEXT_35 = ");" + NL + "\t    }" + NL + "\t        " + NL + "\t    ";
  protected final String TEXT_36 = NL + "\t    shufflingService_";
  protected final String TEXT_37 = ".setRandomSeed(Long.valueOf(";
  protected final String TEXT_38 = "));" + NL + "\t    ";
  protected final String TEXT_39 = NL + "\t    ";
  protected final String TEXT_40 = NL + "\t    shufflingService_";
  protected final String TEXT_41 = ".setSeperationSize(Integer.valueOf(";
  protected final String TEXT_42 = "));" + NL + "\t    ";
  protected final String TEXT_43 = NL + "\t    // The shuffling result is stored in the queue" + NL + "\t    java.util.Queue<List<List<Object>>> shufflingQueue_";
  protected final String TEXT_44 = " = new java.util.concurrent.ConcurrentLinkedQueue<List<List<Object>>>();" + NL + "\t\t" + NL + "\t\torg.talend.dataquality.datamasking.shuffling.ShufflingHandler shufflingHandler_";
  protected final String TEXT_45 = " = " + NL + "\t\t\tnew org.talend.dataquality.datamasking.shuffling.ShufflingHandler(shufflingService_";
  protected final String TEXT_46 = ", shufflingQueue_";
  protected final String TEXT_47 = ");" + NL + "\t\t\t" + NL + "\t\tshufflingService_";
  protected final String TEXT_48 = ".setShufflingHandler(shufflingHandler_";
  protected final String TEXT_49 = ");" + NL + "\t\t" + NL + "\t\tclass ThreadShufflingOutput_";
  protected final String TEXT_50 = " extends Thread {" + NL + "\t\t\tjava.util.Queue<List<List<Object>>> queue;" + NL + "\t\t\tjava.lang.Exception lastException;" + NL + "            String currentComponent;" + NL + "            " + NL + "            ThreadShufflingOutput_";
  protected final String TEXT_51 = "(java.util.Queue<List<List<Object>>> q) {" + NL + "         \t\tthis.queue = q;" + NL + "            \tglobalMap.put(\"globalresult_";
  protected final String TEXT_52 = "\", queue);" + NL + "            \tlastException = null;" + NL + "            }" + NL + "\t\t            " + NL + "\t\t     public java.lang.Exception getLastException() {" + NL + "\t\t         return this.lastException;" + NL + "\t\t     }" + NL + "\t\t     public String getCurrentComponent() {" + NL + "\t\t         return this.currentComponent;" + NL + "\t\t     }" + NL + "\t\t" + NL + "\t\t     @Override" + NL + "\t\t     public void run() {" + NL + "\t\t         try {" + NL + "\t\t             ";
  protected final String TEXT_53 = "Process(globalMap);" + NL + "\t\t         } catch (TalendException te) {" + NL + "\t\t             this.lastException = te.getException();" + NL + "\t\t             this.currentComponent = te.getCurrentComponent();" + NL + "\t\t         }" + NL + "\t\t     }" + NL + "\t\t}" + NL + "\t\t" + NL + "\t\tThreadShufflingOutput_";
  protected final String TEXT_54 = " tso_";
  protected final String TEXT_55 = " = new ThreadShufflingOutput_";
  protected final String TEXT_56 = "(shufflingQueue_";
  protected final String TEXT_57 = ");" + NL + "  \t\ttso_";
  protected final String TEXT_58 = ".start();" + NL + "  \t\t" + NL + "\t\t" + NL + "\t";

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
     
    CodeGeneratorArgument codeGenArgument = (CodeGeneratorArgument) argument;
    INode node = (INode)codeGenArgument.getArgument();
	String cid = ElementParameterParser.getValue(node, "__DESTINATION__");	
	
	INode virtualTargetNode = node.getOutgoingConnections(EConnectionType.ON_COMPONENT_OK).get(0).getTarget();
    String virtualTargetCid = virtualTargetNode.getUniqueName();
    
  	String incomingConnName = null;    
	IMetadataTable inputMetadataTable = null;
	java.util.List<IMetadataColumn> inputColumns = null;
	
	String randomSeedString = ElementParameterParser.getValue(node, "__RANDOM_SEED__");
	String seperationString = ElementParameterParser.getValue(node, "__SEPERATION_SIZE__");
	
	
	List<? extends IConnection> incomingConnections = node.getIncomingConnections();
	if (incomingConnections != null && !incomingConnections.isEmpty()) {	
		for (IConnection conn : incomingConnections) {
			if (conn.getLineStyle().hasConnectionCategory(IConnectionCategory.DATA)) {
				incomingConnName = conn.getName();				
				inputMetadataTable = conn.getMetadataTable();
				inputColumns = inputMetadataTable.getListColumns();
				break;
			}
		}
	}
	
    List<? extends IConnection> outgoingConnections = virtualTargetNode.getOutgoingConnections();
    IMetadataTable outputMetadataTable = null;
    String outgoingConnName = null;
	java.util.List<IMetadataColumn> outputColumns = null;
	if (outgoingConnections != null && !outgoingConnections.isEmpty()) {	
		for (IConnection conn : outgoingConnections) {
			if (conn.getLineStyle().hasConnectionCategory(IConnectionCategory.DATA)) {
				outgoingConnName = conn.getName();				
				outputMetadataTable = conn.getMetadataTable();
				outputColumns = outputMetadataTable.getListColumns();
				break;
			}
		}
	}     
	
	List<Map<String, String>> shufflingTable = (List<Map<String, String>>)ElementParameterParser.getObjectValue(node, "__SHUFFLING_COLUMNS__");
	List<Map<String, String>> partitionTable = (List<Map<String, String>>)ElementParameterParser.getObjectValue(node, "__PARTITIONING_COLUMNS__");
	String separationSize = ElementParameterParser.getValue(node, "__SEPERATION_SIZE__");

    stringBuffer.append(TEXT_1);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_2);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_3);
    
		for(IMetadataColumn meta : inputColumns){
	
    stringBuffer.append(TEXT_4);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_5);
    stringBuffer.append(meta.getLabel());
    stringBuffer.append(TEXT_6);
    
		}
	
    stringBuffer.append(TEXT_7);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_8);
    
		List<Integer> groupids = new ArrayList<Integer>();
		int i = 0;
		for (Iterator<Map<String, String>> iterator = shufflingTable.iterator(); iterator.hasNext();) {//start for iterator
			i++;
			Map<String, String> map = iterator.next();
			if (!map.get("GROUP_ID").toString().isEmpty()) { // start if 1
				int groupid = Integer.parseInt((String) map.get("GROUP_ID"));
				if(groupid != 0){//start if id
					if (groupids.contains(groupid)) { //start if 2
	
    stringBuffer.append(TEXT_9);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_10);
    stringBuffer.append(groupids.indexOf(groupid));
    stringBuffer.append(TEXT_11);
    stringBuffer.append(map.get("COLUMN"));
    stringBuffer.append(TEXT_12);
    
					}// end if 2
					else{ //start else 1
						groupids.add(groupid);
						String item = map.get("COLUMN").toString();
	
    stringBuffer.append(TEXT_13);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_14);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_15);
    stringBuffer.append(item);
    stringBuffer.append(TEXT_16);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_17);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_18);
    
					}//end else
				}//end if id 
			}//end if 1
		}// end for iterator
	
    stringBuffer.append(TEXT_19);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_20);
    
		if (partitionTable == null || partitionTable.isEmpty()) { // start if
		
	
    stringBuffer.append(TEXT_21);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_22);
    
		}// end if
		else{//start else
			for (Iterator<Map<String, String>> iterator = partitionTable.iterator(); iterator.hasNext();) {// start iterator
				Map<String, String> map = iterator.next();
	
    stringBuffer.append(TEXT_23);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_24);
    stringBuffer.append(map.get("PARTITIONING_COLUMN"));
    stringBuffer.append(TEXT_25);
    
			}// end iterator
		}//end else
	
    stringBuffer.append(TEXT_26);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_27);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_28);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_29);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_30);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_31);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_32);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_33);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_34);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_35);
    
	         if(randomSeedString.length() > 0 && !"\"\"".equals(randomSeedString)) {
	    
    stringBuffer.append(TEXT_36);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_37);
    stringBuffer.append(randomSeedString);
    stringBuffer.append(TEXT_38);
    
	         }
	    
    stringBuffer.append(TEXT_39);
    
	         if(seperationString.length() > 0 && !"\"\"".equals(seperationString)) {
	    
    stringBuffer.append(TEXT_40);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_41);
    stringBuffer.append(seperationString);
    stringBuffer.append(TEXT_42);
    
	         }
	    
    stringBuffer.append(TEXT_43);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_44);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_45);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_46);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_47);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_48);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_49);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_50);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_51);
    stringBuffer.append(virtualTargetCid);
    stringBuffer.append(TEXT_52);
    stringBuffer.append(virtualTargetCid);
    stringBuffer.append(TEXT_53);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_54);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_55);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_56);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_57);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_58);
    return stringBuffer.toString();
  }
}
