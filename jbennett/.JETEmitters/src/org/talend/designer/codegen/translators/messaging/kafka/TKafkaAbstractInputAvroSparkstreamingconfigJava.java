package org.talend.designer.codegen.translators.messaging.kafka;

import java.util.Map.Entry;
import org.talend.core.model.process.ElementParameterParser;
import org.talend.core.model.process.INode;
import org.talend.designer.common.BigDataCodeGeneratorArgument;
import org.talend.designer.common.tkafkainput.TKafkaInputUtil;
import org.talend.designer.common.tsetkeystore.TSetKeystoreUtil;
import org.talend.hadoop.distribution.kafka.SparkStreamingKafkaVersion;

public class TKafkaAbstractInputAvroSparkstreamingconfigJava
{
  protected static String nl;
  public static synchronized TKafkaAbstractInputAvroSparkstreamingconfigJava create(String lineSeparator)
  {
    nl = lineSeparator;
    TKafkaAbstractInputAvroSparkstreamingconfigJava result = new TKafkaAbstractInputAvroSparkstreamingconfigJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "\t";
  protected final String TEXT_2 = NL;
  protected final String TEXT_3 = NL;
  protected final String TEXT_4 = NL + "\t\tjava.util.Map<String, String> ";
  protected final String TEXT_5 = "_kafkaProperties = new java.util.HashMap<String, String>();";
  protected final String TEXT_6 = NL + "\t\t\tif(true){" + NL + "\t\t\t\tthrow new Exception(\"A broker list must be provided.\");" + NL + "\t\t\t}";
  protected final String TEXT_7 = NL + "\t\t\t";
  protected final String TEXT_8 = "_kafkaProperties.put(\"bootstrap.servers\", ";
  protected final String TEXT_9 = ");";
  protected final String TEXT_10 = NL + "\t\t\t\t";
  protected final String TEXT_11 = "_kafkaProperties.put(";
  protected final String TEXT_12 = ", ";
  protected final String TEXT_13 = ");";
  protected final String TEXT_14 = NL + "\t\t\t";
  protected final String TEXT_15 = NL + "\t\t\t\tSystem.setProperty(\"java.security.auth.login.config\", \"./\" + new java.io.File(";
  protected final String TEXT_16 = ").getName());" + NL + "\t\t\t";
  protected final String TEXT_17 = NL + "\t\t\t\tSystem.setProperty(\"java.security.auth.login.config\", ";
  protected final String TEXT_18 = ");" + NL + "\t\t\t";
  protected final String TEXT_19 = NL + "\t\t\t";
  protected final String TEXT_20 = "_kafkaProperties.put(\"security.protocol\", \"SASL_PLAINTEXT\");" + NL + "\t\t\t";
  protected final String TEXT_21 = "_kafkaProperties.put(\"sasl.kerberos.service.name\", ";
  protected final String TEXT_22 = ");";
  protected final String TEXT_23 = NL + "\t\t   \t";
  protected final String TEXT_24 = "_kafkaProperties.put(\"sasl.kerberos.kinit.cmd\", ";
  protected final String TEXT_25 = ");";
  protected final String TEXT_26 = NL + "\t\t";
  protected final String TEXT_27 = "_kafkaProperties.put(\"serializer.encoding\", ";
  protected final String TEXT_28 = ");" + NL + "\t\t";
  protected final String TEXT_29 = "_kafkaProperties.put(\"auto.offset.reset\", \"";
  protected final String TEXT_30 = "\");";
  protected final String TEXT_31 = NL + "\t\tjava.util.Set<String> ";
  protected final String TEXT_32 = "_kafkaTopics = new java.util.HashSet<String>();";
  protected final String TEXT_33 = NL + "\t\t\tif(true){" + NL + "\t\t\t\tthrow new Exception(\"A Kafka topic must be provided.\");" + NL + "\t\t\t}";
  protected final String TEXT_34 = NL + "\t\t\t\t";
  protected final String TEXT_35 = "_kafkaTopics.add(";
  protected final String TEXT_36 = ");";
  protected final String TEXT_37 = NL + "\t\t\t\tSystem.setProperty(\"java.security.krb5.conf\", ";
  protected final String TEXT_38 = ");";
  protected final String TEXT_39 = NL + "\t\t\t// Make sure the new security information is picked up." + NL + "\t\t\tjavax.security.auth.login.Configuration.setConfiguration(null);" + NL + "\t\t\t" + NL + "\t\t\t// Send the keytab to Spark executors. Its location is determined from the JAAS configuration contents." + NL + "\t\t\tjavax.security.auth.login.AppConfigurationEntry[] ";
  protected final String TEXT_40 = "_appConfigurationEntries = javax.security.auth.login.Configuration.getConfiguration().getAppConfigurationEntry(\"KafkaClient\");" + NL + "\t\t\tif(";
  protected final String TEXT_41 = "_appConfigurationEntries.length == 0) {" + NL + "\t\t\t\tthrow new RuntimeException(\"Cannot found any 'KafkaClient' login section within the JAAS configuration file [\" + ";
  protected final String TEXT_42 = " + \"].\");" + NL + "\t\t\t}" + NL + "\t\t\tif(";
  protected final String TEXT_43 = "_appConfigurationEntries.length > 1) {" + NL + "\t\t\t\tthrow new RuntimeException(\"Only 1 'KafkaClient' login section is expected from the JAAS configuration. Found \" + ";
  protected final String TEXT_44 = "_appConfigurationEntries.length + \".\");" + NL + "\t\t\t}" + NL + "\t\t\tString ";
  protected final String TEXT_45 = "_keytabPath = (String) ";
  protected final String TEXT_46 = "_appConfigurationEntries[0].getOptions().get(\"keyTab\");" + NL + "\t\t\tjava.io.File ";
  protected final String TEXT_47 = "_keytab = new java.io.File(";
  protected final String TEXT_48 = "_keytabPath);" + NL + "\t\t\tif(!";
  protected final String TEXT_49 = "_keytab.exists()) {" + NL + "\t\t\t\tthrow new RuntimeException(\"Could not find client keytab at location [\" + ";
  protected final String TEXT_50 = "_keytabPath + \"]. Please check the contents of the JAAS configuration file.\");" + NL + "\t\t\t}" + NL + "\t\t\tctx.sparkContext().addFile(";
  protected final String TEXT_51 = "_keytabPath);" + NL + "" + NL + "\t\t\t// Alter a copy of the JAAS configuration file. On the Spark executors, the keytab will be in the same folder as the JAAS file." + NL + "\t\t\t// That's why the keytab location must be changed inside the copy of the JAAS file to reflect this." + NL + "\t\t\tjava.io.File ";
  protected final String TEXT_52 = "_jaasFile = new java.io.File(";
  protected final String TEXT_53 = ");" + NL + "\t\t\tif(!";
  protected final String TEXT_54 = "_jaasFile.exists()) {" + NL + "\t\t\t\tthrow new RuntimeException(\"Could not find JAAS configuration file at location [\" + ";
  protected final String TEXT_55 = " + \"].\");" + NL + "\t\t\t}" + NL + "        \tString ";
  protected final String TEXT_56 = "_jaasOriginalContents = new String(java.nio.file.Files.readAllBytes(java.nio.file.Paths.get(";
  protected final String TEXT_57 = ")), java.nio.charset.StandardCharsets.UTF_8);" + NL + "        \tString ";
  protected final String TEXT_58 = "_jaasModifiedContents = ";
  protected final String TEXT_59 = "_jaasOriginalContents.replaceAll(";
  protected final String TEXT_60 = "_keytabPath, \"./\" + ";
  protected final String TEXT_61 = "_keytab.getName());" + NL + "        \tjava.nio.file.Path ";
  protected final String TEXT_62 = "_modifiedJaasPath = java.nio.file.Paths.get(System.getProperty(\"java.io.tmpdir\"), ";
  protected final String TEXT_63 = "_jaasFile.getName());" + NL + "        \tjava.nio.file.Files.write(";
  protected final String TEXT_64 = "_modifiedJaasPath, ";
  protected final String TEXT_65 = "_jaasModifiedContents.getBytes(java.nio.charset.StandardCharsets.UTF_8));" + NL + "        \t" + NL + "        \t// Send the altered copy of the JAAS configuration file to Spark executors." + NL + "        \tctx.sparkContext().addFile(";
  protected final String TEXT_66 = "_modifiedJaasPath.toAbsolutePath().toString());" + NL + "        \t";
  protected final String TEXT_67 = "_modifiedJaasPath.toFile().deleteOnExit();" + NL + "        \t";
  protected final String TEXT_68 = NL;
  protected final String TEXT_69 = NL + NL + "    String avroSchema_";
  protected final String TEXT_70 = " = org.apache.commons.io.FileUtils.readFileToString(new java.io.File(";
  protected final String TEXT_71 = "));";
  protected final String TEXT_72 = NL + "    ";
  protected final String TEXT_73 = "_kafkaProperties.put(\"talend.avro.schema\", avroSchema_";
  protected final String TEXT_74 = ");" + NL;
  protected final String TEXT_75 = NL + NL + "org.apache.spark.streaming.api.java.JavaPairDStream<NullWritable, org.apache.hadoop.io.ObjectWritable> rdd_";
  protected final String TEXT_76 = " = org.apache.spark.streaming.kafka.KafkaUtils.createDirectStream(ctx, NullWritable.class, org.apache.hadoop.io.ObjectWritable.class, ";
  protected final String TEXT_77 = "_DecoderFromByteArrayToNullWritable.class, ";
  protected final String TEXT_78 = "_DecoderFromByteArrayToObjectWritable.class, ";
  protected final String TEXT_79 = "_kafkaProperties, ";
  protected final String TEXT_80 = "_kafkaTopics);" + NL;
  protected final String TEXT_81 = NL + "\t";
  protected final String TEXT_82 = NL;
  protected final String TEXT_83 = NL;
  protected final String TEXT_84 = NL + "\t\tjava.util.Map<String, Object> ";
  protected final String TEXT_85 = "_kafkaProperties = new java.util.HashMap<String, Object>();" + NL + "\t\t";
  protected final String TEXT_86 = "_kafkaProperties.put(\"bootstrap.servers\", ";
  protected final String TEXT_87 = ");";
  protected final String TEXT_88 = NL + "\t\t\t";
  protected final String TEXT_89 = "_kafkaProperties.put(";
  protected final String TEXT_90 = ", ";
  protected final String TEXT_91 = ");";
  protected final String TEXT_92 = NL + "\t\t";
  protected final String TEXT_93 = "_kafkaProperties.put(org.apache.kafka.clients.consumer.ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, \"";
  protected final String TEXT_94 = "\");" + NL + "\t\t";
  protected final String TEXT_95 = "_kafkaProperties.put(\"serializer.encoding\", ";
  protected final String TEXT_96 = ");" + NL + "\t\t";
  protected final String TEXT_97 = "_kafkaProperties.put(\"group.id\", ";
  protected final String TEXT_98 = ");" + NL + "\t\t";
  protected final String TEXT_99 = "_kafkaProperties.put(org.apache.kafka.clients.consumer.ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, ";
  protected final String TEXT_100 = "_KeyDeserializer.class);" + NL + "\t\t";
  protected final String TEXT_101 = "_kafkaProperties.put(org.apache.kafka.clients.consumer.ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, ";
  protected final String TEXT_102 = "_ValueDeserializer.class);";
  protected final String TEXT_103 = NL + "\t\t\t ";
  protected final String TEXT_104 = "_kafkaProperties.put(\"security.protocol\", \"";
  protected final String TEXT_105 = "\");" + NL + "\t\t\t ";
  protected final String TEXT_106 = "_kafkaProperties.put(\"ssl.truststore.type\", ";
  protected final String TEXT_107 = ");" + NL + "\t\t\t // We call SparkContext#addFile() to make the added file available in the current working directory on every executor node." + NL + "\t\t\t // But on local spark, the file is accessible directly on local FS as executor is launched on local machine." + NL + "\t\t\t ";
  protected final String TEXT_108 = NL + "\t\t \t \t";
  protected final String TEXT_109 = "_kafkaProperties.put(\"ssl.truststore.location\", ";
  protected final String TEXT_110 = ");" + NL + "\t\t \t ";
  protected final String TEXT_111 = NL + "\t\t \t \t";
  protected final String TEXT_112 = "_kafkaProperties.put(\"ssl.truststore.location\", \"./\" + new java.io.File(";
  protected final String TEXT_113 = ").getName());" + NL + "\t\t \t ";
  protected final String TEXT_114 = NL + "\t\t \t ";
  protected final String TEXT_115 = "_kafkaProperties.put(\"ssl.truststore.password\", ";
  protected final String TEXT_116 = ");";
  protected final String TEXT_117 = NL + "\t\t\t \t";
  protected final String TEXT_118 = "_kafkaProperties.put(\"ssl.keystore.type\", ";
  protected final String TEXT_119 = ");" + NL + "\t\t\t \t// This location is relative to Spark executors, after the upload of the keystore." + NL + "\t\t \t \t";
  protected final String TEXT_120 = "_kafkaProperties.put(\"ssl.keystore.location\", \"./\" + new java.io.File(";
  protected final String TEXT_121 = ").getName());" + NL + "\t\t \t \t";
  protected final String TEXT_122 = "_kafkaProperties.put(\"ssl.keystore.password\", ";
  protected final String TEXT_123 = ");";
  protected final String TEXT_124 = NL + "\t\t\t";
  protected final String TEXT_125 = NL + "\t\t\t\tSystem.setProperty(\"java.security.auth.login.config\", \"./\" + new java.io.File(";
  protected final String TEXT_126 = ").getName());" + NL + "\t\t\t";
  protected final String TEXT_127 = NL + "\t\t\t\tSystem.setProperty(\"java.security.auth.login.config\", ";
  protected final String TEXT_128 = ");" + NL + "\t\t\t";
  protected final String TEXT_129 = NL + "\t\t\t";
  protected final String TEXT_130 = "_kafkaProperties.put(\"security.protocol\", \"";
  protected final String TEXT_131 = "\");" + NL + "\t\t\t";
  protected final String TEXT_132 = "_kafkaProperties.put(\"sasl.kerberos.service.name\", ";
  protected final String TEXT_133 = ");";
  protected final String TEXT_134 = NL + "\t   \t\t";
  protected final String TEXT_135 = "_kafkaProperties.put(\"sasl.kerberos.kinit.cmd\", ";
  protected final String TEXT_136 = ");";
  protected final String TEXT_137 = NL + "\t\tjava.util.Set<String> ";
  protected final String TEXT_138 = "_kafkaTopics = new java.util.HashSet<String>();";
  protected final String TEXT_139 = NL + "\t\t\tif(true){" + NL + "\t\t\t\tthrow new Exception(\"A topic must be provided.\");" + NL + "\t\t\t}";
  protected final String TEXT_140 = NL + "\t\t\t\t";
  protected final String TEXT_141 = "_kafkaTopics.add(";
  protected final String TEXT_142 = ");";
  protected final String TEXT_143 = NL + "\t\t\t\t// Send the keystore to Spark executors." + NL + "\t\t\t\tjava.io.File ";
  protected final String TEXT_144 = "_keystore = new java.io.File(";
  protected final String TEXT_145 = ");" + NL + "\t\t\t\tif(!";
  protected final String TEXT_146 = "_keystore.exists()) {" + NL + "\t\t\t\t\tthrow new RuntimeException(\"Could not find client keystore at location [\" + ";
  protected final String TEXT_147 = " + \"].\");" + NL + "\t\t\t\t}" + NL + "\t\t\t\tctx.sparkContext().addFile(";
  protected final String TEXT_148 = ");" + NL + "\t\t\t\t// Copy the keystore in the current directory (the keystore must exist in the same directory between driver and executors...)" + NL + "\t\t\t\tjava.nio.file.Files.copy(java.nio.file.Paths.get(";
  protected final String TEXT_149 = "), java.nio.file.Paths.get(\".\", ";
  protected final String TEXT_150 = "_keystore.getName()), java.nio.file.StandardCopyOption.REPLACE_EXISTING);" + NL + "\t\t\t\tjava.nio.file.Paths.get(\".\", ";
  protected final String TEXT_151 = "_keystore.getName()).toFile().deleteOnExit();" + NL + "\t\t\t\t";
  protected final String TEXT_152 = NL + "\t\t\t// Send the truststore to Spark executors." + NL + " \t\t\tjava.io.File ";
  protected final String TEXT_153 = "_truststore = new java.io.File(";
  protected final String TEXT_154 = ");" + NL + "\t\t\tif(!";
  protected final String TEXT_155 = "_truststore.exists()) {" + NL + "\t\t\t\tthrow new RuntimeException(\"Could not find client truststore at location [\" + ";
  protected final String TEXT_156 = " + \"].\");" + NL + "\t\t\t}" + NL + "\t\t\tctx.sparkContext().addFile(";
  protected final String TEXT_157 = ");" + NL + "\t\t\t// Copy the truststore in the current directory (the truststore must exist in the same directory between driver and executors...)" + NL + "\t\t\tjava.nio.file.Files.copy(java.nio.file.Paths.get(";
  protected final String TEXT_158 = "), java.nio.file.Paths.get(\".\", ";
  protected final String TEXT_159 = "_truststore.getName()), java.nio.file.StandardCopyOption.REPLACE_EXISTING);" + NL + "\t\t\tjava.nio.file.Paths.get(\".\", ";
  protected final String TEXT_160 = "_truststore.getName()).toFile().deleteOnExit();" + NL + "\t\t\t";
  protected final String TEXT_161 = NL + "\t\t\t\tSystem.setProperty(\"java.security.krb5.conf\", ";
  protected final String TEXT_162 = ");";
  protected final String TEXT_163 = NL + "\t\t\t// Make sure the new security information is picked up." + NL + "\t\t\tjavax.security.auth.login.Configuration.setConfiguration(null);" + NL + "\t\t\t" + NL + "\t\t\t// Send the keytab to Spark executors. Its location is determined from the JAAS configuration contents." + NL + "\t\t\tjavax.security.auth.login.AppConfigurationEntry[] ";
  protected final String TEXT_164 = "_appConfigurationEntries = javax.security.auth.login.Configuration.getConfiguration().getAppConfigurationEntry(\"KafkaClient\");" + NL + "\t\t\tif(";
  protected final String TEXT_165 = "_appConfigurationEntries.length == 0) {" + NL + "\t\t\t\tthrow new RuntimeException(\"Cannot found any 'KafkaClient' login section within the JAAS configuration file [\" + ";
  protected final String TEXT_166 = " + \"].\");" + NL + "\t\t\t}" + NL + "\t\t\tif(";
  protected final String TEXT_167 = "_appConfigurationEntries.length > 1) {" + NL + "\t\t\t\tthrow new RuntimeException(\"Only 1 'KafkaClient' login section is expected from the JAAS configuration. Found \" + ";
  protected final String TEXT_168 = "_appConfigurationEntries.length + \".\");" + NL + "\t\t\t}" + NL + "\t\t\tString ";
  protected final String TEXT_169 = "_keytabPath = (String) ";
  protected final String TEXT_170 = "_appConfigurationEntries[0].getOptions().get(\"keyTab\");" + NL + "\t\t\tjava.io.File ";
  protected final String TEXT_171 = "_keytab = new java.io.File(";
  protected final String TEXT_172 = "_keytabPath);" + NL + "\t\t\tif(!";
  protected final String TEXT_173 = "_keytab.exists()) {" + NL + "\t\t\t\tthrow new RuntimeException(\"Could not find client keytab at location [\" + ";
  protected final String TEXT_174 = "_keytabPath + \"]. Please check the contents of the JAAS configuration file.\");" + NL + "\t\t\t}" + NL + "\t\t\tctx.sparkContext().addFile(";
  protected final String TEXT_175 = "_keytabPath);" + NL + "" + NL + "\t\t\t// Alter a copy of the JAAS configuration file. On the Spark executors, the keytab will end up in the same folder as the JAAS file." + NL + "\t\t\t// That's why the keytab location must be changed inside the copy of the JAAS file to reflect this." + NL + "\t\t\tjava.io.File ";
  protected final String TEXT_176 = "_jaasFile = new java.io.File(";
  protected final String TEXT_177 = ");" + NL + "\t\t\tif(!";
  protected final String TEXT_178 = "_jaasFile.exists()) {" + NL + "\t\t\t\tthrow new RuntimeException(\"Could not find JAAS configuration file at location[\" + ";
  protected final String TEXT_179 = " + \"].\");" + NL + "\t\t\t}" + NL + "        \tString ";
  protected final String TEXT_180 = "_jaasOriginalContents = new String(java.nio.file.Files.readAllBytes(java.nio.file.Paths.get(";
  protected final String TEXT_181 = ")), java.nio.charset.StandardCharsets.UTF_8);" + NL + "        \tString ";
  protected final String TEXT_182 = "_jaasModifiedContents = ";
  protected final String TEXT_183 = "_jaasOriginalContents.replaceAll(";
  protected final String TEXT_184 = "_keytabPath, \"./\" + ";
  protected final String TEXT_185 = "_keytab.getName());" + NL + "        \tjava.nio.file.Path ";
  protected final String TEXT_186 = "_modifiedJaasPath = java.nio.file.Paths.get(System.getProperty(\"java.io.tmpdir\"), ";
  protected final String TEXT_187 = "_jaasFile.getName());" + NL + "        \tjava.nio.file.Files.write(";
  protected final String TEXT_188 = "_modifiedJaasPath, ";
  protected final String TEXT_189 = "_jaasModifiedContents.getBytes(java.nio.charset.StandardCharsets.UTF_8));" + NL + "        \t" + NL + "        \t// Send the altered copy of the JAAS configuration file to Spark executors." + NL + "        \tctx.sparkContext().addFile(";
  protected final String TEXT_190 = "_modifiedJaasPath.toAbsolutePath().toString());" + NL + "        \t";
  protected final String TEXT_191 = "_modifiedJaasPath.toFile().deleteOnExit();" + NL + "        \t";
  protected final String TEXT_192 = NL;
  protected final String TEXT_193 = NL + "    String avroSchema_";
  protected final String TEXT_194 = " = org.apache.commons.io.FileUtils.readFileToString(new java.io.File(";
  protected final String TEXT_195 = "));";
  protected final String TEXT_196 = NL + "    ";
  protected final String TEXT_197 = "_kafkaProperties.put(\"talend.avro.schema\", avroSchema_";
  protected final String TEXT_198 = ");";
  protected final String TEXT_199 = NL + NL + "org.apache.spark.streaming.api.java.JavaDStream<org.apache.kafka.clients.consumer.ConsumerRecord<NullWritable, org.apache.hadoop.io.ObjectWritable>> rdd_kafka_";
  protected final String TEXT_200 = " = org.apache.spark.streaming.kafka010.KafkaUtils.createDirectStream(" + NL + "\tctx, " + NL + "\torg.apache.spark.streaming.kafka010.LocationStrategies.PreferConsistent(), " + NL + "\torg.apache.spark.streaming.kafka010.ConsumerStrategies.<NullWritable, org.apache.hadoop.io.ObjectWritable>Subscribe(";
  protected final String TEXT_201 = "_kafkaTopics, ";
  protected final String TEXT_202 = "_kafkaProperties)" + NL + ");" + NL + "" + NL + "org.apache.spark.streaming.api.java.JavaPairDStream<NullWritable, org.apache.hadoop.io.ObjectWritable> rdd_";
  protected final String TEXT_203 = " = rdd_kafka_";
  protected final String TEXT_204 = ".mapToPair(new ";
  protected final String TEXT_205 = "_Function());";
  protected final String TEXT_206 = NL + "\t";
  protected final String TEXT_207 = NL;
  protected final String TEXT_208 = NL;
  protected final String TEXT_209 = NL + "\t\tjava.util.Map<String, String> ";
  protected final String TEXT_210 = "_kafkaProperties = new java.util.HashMap<String, String>();" + NL + "\t\t";
  protected final String TEXT_211 = "_kafkaProperties.put(\"bootstrap.servers\", ";
  protected final String TEXT_212 = ");";
  protected final String TEXT_213 = NL + "\t\t\t";
  protected final String TEXT_214 = "_kafkaProperties.put(";
  protected final String TEXT_215 = ", ";
  protected final String TEXT_216 = ");";
  protected final String TEXT_217 = NL + "\t\t";
  protected final String TEXT_218 = "_kafkaProperties.put(org.apache.kafka.clients.consumer.ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, \"";
  protected final String TEXT_219 = "\");" + NL + "\t\t";
  protected final String TEXT_220 = "_kafkaProperties.put(\"serializer.encoding\", ";
  protected final String TEXT_221 = ");" + NL + "\t\t";
  protected final String TEXT_222 = "_kafkaProperties.put(org.apache.kafka.clients.consumer.ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, ";
  protected final String TEXT_223 = "_KeyDeserializer.class.getName());" + NL + "\t\t";
  protected final String TEXT_224 = "_kafkaProperties.put(org.apache.kafka.clients.consumer.ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, ";
  protected final String TEXT_225 = "_ValueDeserializer.class.getName());";
  protected final String TEXT_226 = NL + "\t\tjava.util.Set<String> ";
  protected final String TEXT_227 = "_kafkaTopics = new java.util.HashSet<String>();";
  protected final String TEXT_228 = NL + "\t\t\tif(true){" + NL + "\t\t\t\tthrow new Exception(\"A topic must be provided.\");" + NL + "\t\t\t}";
  protected final String TEXT_229 = NL + "\t\t\t\t";
  protected final String TEXT_230 = "_kafkaTopics.add(";
  protected final String TEXT_231 = ");";
  protected final String TEXT_232 = NL;
  protected final String TEXT_233 = NL + "    String avroSchema_";
  protected final String TEXT_234 = " = org.apache.commons.io.FileUtils.readFileToString(new java.io.File(";
  protected final String TEXT_235 = "));";
  protected final String TEXT_236 = NL + "    ";
  protected final String TEXT_237 = "_kafkaProperties.put(\"talend.avro.schema\", avroSchema_";
  protected final String TEXT_238 = ");";
  protected final String TEXT_239 = NL + NL + "org.apache.spark.streaming.api.java.JavaPairDStream<NullWritable, org.apache.hadoop.io.ObjectWritable> rdd_";
  protected final String TEXT_240 = " = org.apache.spark.streaming.kafka.v09.KafkaUtils.createDirectStream(ctx, NullWritable.class, org.apache.hadoop.io.ObjectWritable.class, ";
  protected final String TEXT_241 = "_kafkaProperties, ";
  protected final String TEXT_242 = "_kafkaTopics);";
  protected final String TEXT_243 = NL + "\t";
  protected final String TEXT_244 = NL;
  protected final String TEXT_245 = NL;
  protected final String TEXT_246 = NL + "\t\tjava.util.Map<String, Object> ";
  protected final String TEXT_247 = "_kafkaProperties = new java.util.HashMap<String, Object>();" + NL + "\t\t";
  protected final String TEXT_248 = "_kafkaProperties.put(\"bootstrap.servers\", ";
  protected final String TEXT_249 = ");";
  protected final String TEXT_250 = NL + "\t\t\t";
  protected final String TEXT_251 = "_kafkaProperties.put(";
  protected final String TEXT_252 = ", ";
  protected final String TEXT_253 = ");";
  protected final String TEXT_254 = NL + "\t\t";
  protected final String TEXT_255 = "_kafkaProperties.put(org.apache.kafka.clients.consumer.ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, \"";
  protected final String TEXT_256 = "\");" + NL + "\t\t";
  protected final String TEXT_257 = "_kafkaProperties.put(\"serializer.encoding\", ";
  protected final String TEXT_258 = ");" + NL + "\t\t" + NL + "\t\t";
  protected final String TEXT_259 = "_kafkaProperties.put(org.apache.kafka.clients.consumer.ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, ";
  protected final String TEXT_260 = "_KeyDeserializer.class);" + NL + "\t\t";
  protected final String TEXT_261 = "_kafkaProperties.put(org.apache.kafka.clients.consumer.ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, ";
  protected final String TEXT_262 = "_ValueDeserializer.class);";
  protected final String TEXT_263 = NL + "\t\tjava.util.List<String> ";
  protected final String TEXT_264 = "_kafkaTopics = new java.util.ArrayList<String>();";
  protected final String TEXT_265 = NL + "\t\t\tif(true){" + NL + "\t\t\t\tthrow new Exception(\"A topic must be provided.\");" + NL + "\t\t\t}";
  protected final String TEXT_266 = NL + "\t\t\t\t";
  protected final String TEXT_267 = "_kafkaTopics.add(";
  protected final String TEXT_268 = ");";
  protected final String TEXT_269 = NL;
  protected final String TEXT_270 = NL + "    String avroSchema_";
  protected final String TEXT_271 = " = org.apache.commons.io.FileUtils.readFileToString(new java.io.File(";
  protected final String TEXT_272 = "));";
  protected final String TEXT_273 = NL + "    ";
  protected final String TEXT_274 = "_kafkaProperties.put(\"talend.avro.schema\", avroSchema_";
  protected final String TEXT_275 = ");";
  protected final String TEXT_276 = NL + NL + "org.apache.spark.streaming.api.java.JavaDStream<org.apache.kafka.clients.consumer.ConsumerRecord<NullWritable, org.apache.hadoop.io.ObjectWritable>> rdd_kafka_";
  protected final String TEXT_277 = " = org.apache.spark.streaming.kafka09.KafkaUtils.createDirectStream(" + NL + "\tctx, " + NL + "\torg.apache.spark.streaming.kafka09.LocationStrategies.PreferConsistent(), " + NL + "\torg.apache.spark.streaming.kafka09.ConsumerStrategies.<NullWritable, org.apache.hadoop.io.ObjectWritable>Subscribe(";
  protected final String TEXT_278 = "_kafkaTopics, ";
  protected final String TEXT_279 = "_kafkaProperties)" + NL + ");" + NL + "" + NL + "org.apache.spark.streaming.api.java.JavaPairDStream<NullWritable, org.apache.hadoop.io.ObjectWritable> rdd_";
  protected final String TEXT_280 = " = rdd_kafka_";
  protected final String TEXT_281 = ".mapToPair(new ";
  protected final String TEXT_282 = "_Function());";

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    
BigDataCodeGeneratorArgument codeGenArgument = (BigDataCodeGeneratorArgument) argument;
INode node = (INode)codeGenArgument.getArgument();
String cid = node.getUniqueName();

TKafkaInputUtil tKafkaInputUtil = new TKafkaInputUtil(node);
SparkStreamingKafkaVersion sparkStreamingKafkaVersion = tKafkaInputUtil.getSparkStreamingKafkaVersion();

if (SparkStreamingKafkaVersion.KAFKA_0_8.equals(sparkStreamingKafkaVersion)) {

    stringBuffer.append(TEXT_1);
    stringBuffer.append(TEXT_2);
    stringBuffer.append(TEXT_3);
    
final class TKafkaInputHelper {

	private TKafkaInputUtil tKafkaInputUtil;
	
	public TKafkaInputHelper(TKafkaInputUtil util){
		tKafkaInputUtil = util;
	}

	public void generateKafkaProperties(String cid, boolean useYarnClusterMode) {

    stringBuffer.append(TEXT_4);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_5);
    
		if(tKafkaInputUtil.getBrokerList() == null || "".equals(tKafkaInputUtil.getBrokerList())){

    stringBuffer.append(TEXT_6);
    
		} else {

    stringBuffer.append(TEXT_7);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_8);
    stringBuffer.append(tKafkaInputUtil.getBrokerList());
    stringBuffer.append(TEXT_9);
    
			for(Entry<String, String> kafkaProperty : tKafkaInputUtil.getKafkaConsumerProperties().entrySet()) {

    stringBuffer.append(TEXT_10);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_11);
    stringBuffer.append(kafkaProperty.getKey());
    stringBuffer.append(TEXT_12);
    stringBuffer.append(kafkaProperty.getValue());
    stringBuffer.append(TEXT_13);
    
			} // end for
			
		} // end else
		
		if(tKafkaInputUtil.useKrb()) {

    stringBuffer.append(TEXT_14);
    if(useYarnClusterMode){
    stringBuffer.append(TEXT_15);
    stringBuffer.append(tKafkaInputUtil.getJaasConf());
    stringBuffer.append(TEXT_16);
    }else{
    stringBuffer.append(TEXT_17);
    stringBuffer.append(tKafkaInputUtil.getJaasConf());
    stringBuffer.append(TEXT_18);
    }
    stringBuffer.append(TEXT_19);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_20);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_21);
    stringBuffer.append(tKafkaInputUtil.getKrbServiceName());
    stringBuffer.append(TEXT_22);
    
			if(tKafkaInputUtil.isSetKinitPath()) {

    stringBuffer.append(TEXT_23);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_24);
    stringBuffer.append(tKafkaInputUtil.getKinitPath());
    stringBuffer.append(TEXT_25);
    
		   }
		} // end if(tKafkaInputUtil.useKrb())

    stringBuffer.append(TEXT_26);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_27);
    stringBuffer.append(tKafkaInputUtil.getEncoding());
    stringBuffer.append(TEXT_28);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_29);
    stringBuffer.append(tKafkaInputUtil.getAutoOffsetReset());
    stringBuffer.append(TEXT_30);
    
	} // end generateKafkaProperties
	
	public void generateKafkaTopics(String cid) {

    stringBuffer.append(TEXT_31);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_32);
    
		if(tKafkaInputUtil.getKafkaTopics().isEmpty()){

    stringBuffer.append(TEXT_33);
    
		}else {
			for(String kafkaTopic : tKafkaInputUtil.getKafkaTopics()) {

    stringBuffer.append(TEXT_34);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_35);
    stringBuffer.append(kafkaTopic);
    stringBuffer.append(TEXT_36);
    
			} // end for
		} // end else
	} // end generateKafkaTopics
	
	public void generateKerberos(String cid) {
		if(tKafkaInputUtil.useKrb()) {
			if(tKafkaInputUtil.isSetKrb5Conf()) {

    stringBuffer.append(TEXT_37);
    stringBuffer.append(tKafkaInputUtil.getKrb5Conf());
    stringBuffer.append(TEXT_38);
    
			}

    stringBuffer.append(TEXT_39);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_40);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_41);
    stringBuffer.append(tKafkaInputUtil.getJaasConf());
    stringBuffer.append(TEXT_42);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_43);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_44);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_45);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_46);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_47);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_48);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_49);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_50);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_51);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_52);
    stringBuffer.append(tKafkaInputUtil.getJaasConf());
    stringBuffer.append(TEXT_53);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_54);
    stringBuffer.append(tKafkaInputUtil.getJaasConf());
    stringBuffer.append(TEXT_55);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_56);
    stringBuffer.append(tKafkaInputUtil.getJaasConf());
    stringBuffer.append(TEXT_57);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_58);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_59);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_60);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_61);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_62);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_63);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_64);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_65);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_66);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_67);
    
		} // end if(tKafkaInputUtil.useKrb()) 
	} // end generateKerberos
	
} // end class TKafkaInputHelper

    stringBuffer.append(TEXT_68);
    
TKafkaInputHelper tKafkaInputHelper = new TKafkaInputHelper(tKafkaInputUtil);

// SparkConfiguration to know whether we are in Yarn Cluster mode
final java.util.List<? extends INode> sparkConfigs = node.getProcess().getNodesOfType("tSparkConfiguration");
INode sparkConfig = null;
if(sparkConfigs != null && sparkConfigs.size() > 0) {
    sparkConfig = sparkConfigs.get(0);
}
boolean useYarnClusterMode = false;
if(sparkConfig != null) {
   String sparkMode = ElementParameterParser.getValue(sparkConfig, "__SPARK_MODE__");
   boolean useLocalMode = "true".equals(ElementParameterParser.getValue(sparkConfig, "__SPARK_LOCAL_MODE__"));
   useYarnClusterMode = !useLocalMode && "YARN_CLUSTER".equals(sparkMode);
}

tKafkaInputHelper.generateKafkaProperties(cid, useYarnClusterMode);
tKafkaInputHelper.generateKafkaTopics(cid);
tKafkaInputHelper.generateKerberos(cid);

boolean useHierarchical = "true".equals(ElementParameterParser.getValue(node, "__USE_HIERARCHICAL__"));

if (useHierarchical) {
    String schemaFileName = ElementParameterParser.getValue(node, "__SCHEMA_FILENAME__");
    
    stringBuffer.append(TEXT_69);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_70);
    stringBuffer.append(schemaFileName);
    stringBuffer.append(TEXT_71);
    stringBuffer.append(TEXT_72);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_73);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_74);
    
}

    stringBuffer.append(TEXT_75);
    stringBuffer.append(tKafkaInputUtil.getOutgoingConnection().getName());
    stringBuffer.append(TEXT_76);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_77);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_78);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_79);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_80);
    
} else if (SparkStreamingKafkaVersion.KAFKA_0_10.equals(sparkStreamingKafkaVersion)) {

    stringBuffer.append(TEXT_81);
    stringBuffer.append(TEXT_82);
    stringBuffer.append(TEXT_83);
    
final class TKafkaInputHelper {

	private TKafkaInputUtil tKafkaInputUtil;
	
	private TSetKeystoreUtil tSetKeystoreUtil;
		
	public TKafkaInputHelper(TKafkaInputUtil tKafkaInputUtil){
		this.tKafkaInputUtil = tKafkaInputUtil;
		this.tSetKeystoreUtil = tKafkaInputUtil.getTSetKeystoreUtil();
	}

	public void generateKafkaProperties(String cid, boolean useYarnClusterMode, boolean useLocalMode) {

    stringBuffer.append(TEXT_84);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_85);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_86);
    stringBuffer.append(tKafkaInputUtil.getBrokerList());
    stringBuffer.append(TEXT_87);
    
		for(Entry<String, String> property : tKafkaInputUtil.getKafkaConsumerProperties().entrySet()) {

    stringBuffer.append(TEXT_88);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_89);
    stringBuffer.append(property.getKey());
    stringBuffer.append(TEXT_90);
    stringBuffer.append(property.getValue());
    stringBuffer.append(TEXT_91);
    
		} // end for
		
		// Special case for MapR and Kafka 0.9+
		String autoOffsetReset = "largest".equals(tKafkaInputUtil.getAutoOffsetReset()) ? "latest" : "earliest";

    stringBuffer.append(TEXT_92);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_93);
    stringBuffer.append(autoOffsetReset);
    stringBuffer.append(TEXT_94);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_95);
    stringBuffer.append(tKafkaInputUtil.getEncoding());
    stringBuffer.append(TEXT_96);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_97);
    stringBuffer.append(tKafkaInputUtil.getGroupId());
    stringBuffer.append(TEXT_98);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_99);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_100);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_101);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_102);
    
		// SSL configuration
		if (tSetKeystoreUtil.useHTTPS()) {
			// When Kerberos is active as well, the security protocol is different
			String securityProtocol = tKafkaInputUtil.useKrb() ? "SASL_SSL" : "SSL";

    stringBuffer.append(TEXT_103);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_104);
    stringBuffer.append(securityProtocol);
    stringBuffer.append(TEXT_105);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_106);
    stringBuffer.append(tSetKeystoreUtil.getTrustStoreType());
    stringBuffer.append(TEXT_107);
    if(useLocalMode){
    stringBuffer.append(TEXT_108);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_109);
    stringBuffer.append(tSetKeystoreUtil.getTrustStorePath());
    stringBuffer.append(TEXT_110);
    } else {
    stringBuffer.append(TEXT_111);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_112);
    stringBuffer.append(tSetKeystoreUtil.getTrustStorePath());
    stringBuffer.append(TEXT_113);
    }
    stringBuffer.append(TEXT_114);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_115);
    stringBuffer.append(tSetKeystoreUtil.getTrustStorePassword());
    stringBuffer.append(TEXT_116);
    
			if (tSetKeystoreUtil.needClientAuth()) {

    stringBuffer.append(TEXT_117);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_118);
    stringBuffer.append(tSetKeystoreUtil.getKeyStoreType());
    stringBuffer.append(TEXT_119);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_120);
    stringBuffer.append(tSetKeystoreUtil.getKeyStorePath());
    stringBuffer.append(TEXT_121);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_122);
    stringBuffer.append(tSetKeystoreUtil.getKeyStorePassword());
    stringBuffer.append(TEXT_123);
    
			}
		} // end ssl
		
		// Kerberos configuration
		if (tKafkaInputUtil.useKrb()) {
			// When SSL is active as well, the security protocol is different
			String securityProtocol = tSetKeystoreUtil.useHTTPS() ? "SASL_SSL" : "SASL_PLAINTEXT";

    stringBuffer.append(TEXT_124);
    if(useYarnClusterMode){
    stringBuffer.append(TEXT_125);
    stringBuffer.append(tKafkaInputUtil.getJaasConf());
    stringBuffer.append(TEXT_126);
    }else{
    stringBuffer.append(TEXT_127);
    stringBuffer.append(tKafkaInputUtil.getJaasConf());
    stringBuffer.append(TEXT_128);
    }
    stringBuffer.append(TEXT_129);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_130);
    stringBuffer.append(securityProtocol);
    stringBuffer.append(TEXT_131);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_132);
    stringBuffer.append(tKafkaInputUtil.getKrbServiceName());
    stringBuffer.append(TEXT_133);
    
			if(tKafkaInputUtil.isSetKinitPath()) {

    stringBuffer.append(TEXT_134);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_135);
    stringBuffer.append(tKafkaInputUtil.getKinitPath());
    stringBuffer.append(TEXT_136);
    
	   	}
		} // end kerberos
	} // end generateProperties
	
	public void generateKafkaTopics(String cid) {

    stringBuffer.append(TEXT_137);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_138);
    
		if(tKafkaInputUtil.getKafkaTopics().isEmpty()){

    stringBuffer.append(TEXT_139);
    
		}else {
			for(String kafkaTopic : tKafkaInputUtil.getKafkaTopics()) {

    stringBuffer.append(TEXT_140);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_141);
    stringBuffer.append(kafkaTopic);
    stringBuffer.append(TEXT_142);
    
			} // end for
		} // end else
	} // end generateTopics
	
	public void generateSSL(String cid) {
		if(tSetKeystoreUtil.useHTTPS()) {
			if(tSetKeystoreUtil.needClientAuth()) {

    stringBuffer.append(TEXT_143);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_144);
    stringBuffer.append(tSetKeystoreUtil.getKeyStorePath());
    stringBuffer.append(TEXT_145);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_146);
    stringBuffer.append(tSetKeystoreUtil.getKeyStorePath());
    stringBuffer.append(TEXT_147);
    stringBuffer.append(tSetKeystoreUtil.getKeyStorePath());
    stringBuffer.append(TEXT_148);
    stringBuffer.append(tSetKeystoreUtil.getKeyStorePath());
    stringBuffer.append(TEXT_149);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_150);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_151);
    
			} // end if (tSetKeystoreUtil.needClientAuth())

    stringBuffer.append(TEXT_152);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_153);
    stringBuffer.append(tSetKeystoreUtil.getTrustStorePath());
    stringBuffer.append(TEXT_154);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_155);
    stringBuffer.append(tSetKeystoreUtil.getTrustStorePath());
    stringBuffer.append(TEXT_156);
    stringBuffer.append(tSetKeystoreUtil.getTrustStorePath());
    stringBuffer.append(TEXT_157);
    stringBuffer.append(tSetKeystoreUtil.getTrustStorePath());
    stringBuffer.append(TEXT_158);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_159);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_160);
    
		} // end if(tSetKeystoreUtil.useHTTPS())
	} // end generateSSL
	
	public void generateKerberos(String cid) {
		if(tKafkaInputUtil.useKrb()) {
			if(tKafkaInputUtil.isSetKrb5Conf()) {

    stringBuffer.append(TEXT_161);
    stringBuffer.append(tKafkaInputUtil.getKrb5Conf());
    stringBuffer.append(TEXT_162);
    
			}

    stringBuffer.append(TEXT_163);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_164);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_165);
    stringBuffer.append(tKafkaInputUtil.getJaasConf());
    stringBuffer.append(TEXT_166);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_167);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_168);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_169);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_170);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_171);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_172);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_173);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_174);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_175);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_176);
    stringBuffer.append(tKafkaInputUtil.getJaasConf());
    stringBuffer.append(TEXT_177);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_178);
    stringBuffer.append(tKafkaInputUtil.getJaasConf());
    stringBuffer.append(TEXT_179);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_180);
    stringBuffer.append(tKafkaInputUtil.getJaasConf());
    stringBuffer.append(TEXT_181);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_182);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_183);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_184);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_185);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_186);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_187);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_188);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_189);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_190);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_191);
    
		} // end if(tKafkaInputUtil.useKrb()) 
	} // end generateKerberos
	
} // end class TKafkaInputHelper

    stringBuffer.append(TEXT_192);
    
TKafkaInputHelper tKafkaInputHelper = new TKafkaInputHelper(tKafkaInputUtil);
String outStructName = codeGenArgument.getRecordStructName(tKafkaInputUtil.getOutgoingConnection());

// Whether we are in yarn cluster mode, or spark local mode
final java.util.List<? extends INode> sparkConfigs = node.getProcess().getNodesOfType("tSparkConfiguration");
INode sparkConfig = null;
if(sparkConfigs != null && sparkConfigs.size() > 0) {
    sparkConfig = sparkConfigs.get(0);
}
boolean useYarnClusterMode = false;
boolean useLocalMode = false;
if(sparkConfig != null) {
   String sparkMode = ElementParameterParser.getValue(sparkConfig, "__SPARK_MODE__");
   useLocalMode = "true".equals(ElementParameterParser.getValue(sparkConfig, "__SPARK_LOCAL_MODE__"));
   useYarnClusterMode = !useLocalMode && "YARN_CLUSTER".equals(sparkMode);
}

tKafkaInputHelper.generateKafkaProperties(cid, useYarnClusterMode, useLocalMode);
tKafkaInputHelper.generateKafkaTopics(cid);
tKafkaInputHelper.generateKerberos(cid);
tKafkaInputHelper.generateSSL(cid);

boolean useHierarchical = "true".equals(ElementParameterParser.getValue(node, "__USE_HIERARCHICAL__"));

if (useHierarchical) {
    String schemaFileName = ElementParameterParser.getValue(node, "__SCHEMA_FILENAME__");

    stringBuffer.append(TEXT_193);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_194);
    stringBuffer.append(schemaFileName);
    stringBuffer.append(TEXT_195);
    stringBuffer.append(TEXT_196);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_197);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_198);
    
}

    stringBuffer.append(TEXT_199);
    stringBuffer.append(tKafkaInputUtil.getOutgoingConnection().getName());
    stringBuffer.append(TEXT_200);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_201);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_202);
    stringBuffer.append(tKafkaInputUtil.getOutgoingConnection().getName());
    stringBuffer.append(TEXT_203);
    stringBuffer.append(tKafkaInputUtil.getOutgoingConnection().getName());
    stringBuffer.append(TEXT_204);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_205);
    
} else if (SparkStreamingKafkaVersion.MAPR_5X0_KAFKA.equals(sparkStreamingKafkaVersion)) {
	// Special case only for some MapR distributions 

    stringBuffer.append(TEXT_206);
    stringBuffer.append(TEXT_207);
    stringBuffer.append(TEXT_208);
    
final class TKafkaInputHelper {

	private TKafkaInputUtil tKafkaInputUtil;
	
	public TKafkaInputHelper(TKafkaInputUtil util){
		tKafkaInputUtil = util;
	}

	public void generateKafkaProperties(String cid) {

    stringBuffer.append(TEXT_209);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_210);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_211);
    stringBuffer.append(tKafkaInputUtil.getBrokerList());
    stringBuffer.append(TEXT_212);
    
		for(Entry<String, String> property : tKafkaInputUtil.getKafkaConsumerProperties().entrySet()) {

    stringBuffer.append(TEXT_213);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_214);
    stringBuffer.append(property.getKey());
    stringBuffer.append(TEXT_215);
    stringBuffer.append(property.getValue());
    stringBuffer.append(TEXT_216);
    
		} // end for
		
		// Special case for MapR and Kafka 0.9
		String autoOffsetReset = "largest".equals(tKafkaInputUtil.getAutoOffsetReset()) ? "latest" : "earliest";

    stringBuffer.append(TEXT_217);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_218);
    stringBuffer.append(autoOffsetReset);
    stringBuffer.append(TEXT_219);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_220);
    stringBuffer.append(tKafkaInputUtil.getEncoding());
    stringBuffer.append(TEXT_221);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_222);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_223);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_224);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_225);
    
	} // end generateProperties
	
	public void generateKafkaTopics(String cid) {

    stringBuffer.append(TEXT_226);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_227);
    
		if(tKafkaInputUtil.getKafkaTopics().isEmpty()){

    stringBuffer.append(TEXT_228);
    
		}else {
			for(String kafkaTopic : tKafkaInputUtil.getKafkaTopics()) {

    stringBuffer.append(TEXT_229);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_230);
    stringBuffer.append(kafkaTopic);
    stringBuffer.append(TEXT_231);
    
			} // end for
		} // end else
	} // end generateTopics
	
} // end class TKafkaInputHelper

    stringBuffer.append(TEXT_232);
    
TKafkaInputHelper tKafkaInputHelper = new TKafkaInputHelper(tKafkaInputUtil);
String outStructName = codeGenArgument.getRecordStructName(tKafkaInputUtil.getOutgoingConnection());

tKafkaInputHelper.generateKafkaProperties(cid);
tKafkaInputHelper.generateKafkaTopics(cid);

boolean useHierarchical = "true".equals(ElementParameterParser.getValue(node, "__USE_HIERARCHICAL__"));

if (useHierarchical) {
    String schemaFileName = ElementParameterParser.getValue(node, "__SCHEMA_FILENAME__");

    stringBuffer.append(TEXT_233);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_234);
    stringBuffer.append(schemaFileName);
    stringBuffer.append(TEXT_235);
    stringBuffer.append(TEXT_236);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_237);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_238);
    
}

    stringBuffer.append(TEXT_239);
    stringBuffer.append(tKafkaInputUtil.getOutgoingConnection().getName());
    stringBuffer.append(TEXT_240);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_241);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_242);
    
} else if (SparkStreamingKafkaVersion.MAPR_600_KAFKA.equals(sparkStreamingKafkaVersion)) {
	// Special case only for some MapR distributions 

    stringBuffer.append(TEXT_243);
    stringBuffer.append(TEXT_244);
    stringBuffer.append(TEXT_245);
    
final class TKafkaInputHelper {

	private TKafkaInputUtil tKafkaInputUtil;
	
	public TKafkaInputHelper(TKafkaInputUtil util){
		tKafkaInputUtil = util;
	}

	public void generateKafkaProperties(String cid) {

    stringBuffer.append(TEXT_246);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_247);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_248);
    stringBuffer.append(tKafkaInputUtil.getBrokerList());
    stringBuffer.append(TEXT_249);
    
		for(Entry<String, String> property : tKafkaInputUtil.getKafkaConsumerProperties().entrySet()) {

    stringBuffer.append(TEXT_250);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_251);
    stringBuffer.append(property.getKey());
    stringBuffer.append(TEXT_252);
    stringBuffer.append(property.getValue());
    stringBuffer.append(TEXT_253);
    
		} // end for
		
		// Special case for MapR and Kafka 0.9+
		String autoOffsetReset = "largest".equals(tKafkaInputUtil.getAutoOffsetReset()) ? "latest" : "earliest";

    stringBuffer.append(TEXT_254);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_255);
    stringBuffer.append(autoOffsetReset);
    stringBuffer.append(TEXT_256);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_257);
    stringBuffer.append(tKafkaInputUtil.getEncoding());
    stringBuffer.append(TEXT_258);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_259);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_260);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_261);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_262);
    
	} // end generateProperties
	
	public void generateKafkaTopics(String cid) {

    stringBuffer.append(TEXT_263);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_264);
    
		if(tKafkaInputUtil.getKafkaTopics().isEmpty()){

    stringBuffer.append(TEXT_265);
    
		}else {
			for(String kafkaTopic : tKafkaInputUtil.getKafkaTopics()) {

    stringBuffer.append(TEXT_266);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_267);
    stringBuffer.append(kafkaTopic);
    stringBuffer.append(TEXT_268);
    
			} // end for
		} // end else
	} // end generateTopics
	
} // end class TKafkaInputHelper

    stringBuffer.append(TEXT_269);
    
TKafkaInputHelper tKafkaInputHelper = new TKafkaInputHelper(tKafkaInputUtil);
String outStructName = codeGenArgument.getRecordStructName(tKafkaInputUtil.getOutgoingConnection());

tKafkaInputHelper.generateKafkaProperties(cid);
tKafkaInputHelper.generateKafkaTopics(cid);

boolean useHierarchical = "true".equals(ElementParameterParser.getValue(node, "__USE_HIERARCHICAL__"));

if (useHierarchical) {
    String schemaFileName = ElementParameterParser.getValue(node, "__SCHEMA_FILENAME__");

    stringBuffer.append(TEXT_270);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_271);
    stringBuffer.append(schemaFileName);
    stringBuffer.append(TEXT_272);
    stringBuffer.append(TEXT_273);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_274);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_275);
    
}

    stringBuffer.append(TEXT_276);
    stringBuffer.append(tKafkaInputUtil.getOutgoingConnection().getName());
    stringBuffer.append(TEXT_277);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_278);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_279);
    stringBuffer.append(tKafkaInputUtil.getOutgoingConnection().getName());
    stringBuffer.append(TEXT_280);
    stringBuffer.append(tKafkaInputUtil.getOutgoingConnection().getName());
    stringBuffer.append(TEXT_281);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_282);
    
}

    return stringBuffer.toString();
  }
}
