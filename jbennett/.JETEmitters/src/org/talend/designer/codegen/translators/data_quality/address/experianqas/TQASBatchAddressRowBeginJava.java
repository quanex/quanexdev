package org.talend.designer.codegen.translators.data_quality.address.experianqas;

import org.talend.core.model.process.INode;
import org.talend.core.model.process.ElementParameterParser;
import org.talend.core.model.metadata.IMetadataTable;
import org.talend.core.model.process.IConnection;
import org.talend.designer.codegen.config.CodeGeneratorArgument;
import java.util.List;

public class TQASBatchAddressRowBeginJava
{
  protected static String nl;
  public static synchronized TQASBatchAddressRowBeginJava create(String lineSeparator)
  {
    nl = lineSeparator;
    TQASBatchAddressRowBeginJava result = new TQASBatchAddressRowBeginJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = NL + "      org.talend.qasbatch.QASSearch qasSearch_";
  protected final String TEXT_2 = "=null;" + NL + "      if(\"QAS_750\".equals(\"";
  protected final String TEXT_3 = "\")){" + NL + "         try {" + NL + "             System.loadLibrary(\"QABatchJNI64\");" + NL + "         } catch (UnsatisfiedLinkError e) {" + NL + "             System.err.println(\"Native code library \" + \"failed to load.\\nMake sure you correctly defined the java.library.path VM property in the Job advanced settings like: java.library.path=\\\"<Qas batch installation path>\\\"\");" + NL + "             e.printStackTrace();" + NL + "         }" + NL + "         qasSearch_";
  protected final String TEXT_4 = " = new org.talend.qasbatch.QASSearch753();" + NL + "      }else{" + NL + "         qasSearch_";
  protected final String TEXT_5 = " = new org.talend.qasbatch.QASSearch();" + NL + "      }" + NL + "      qasSearch_";
  protected final String TEXT_6 = ".setLayout(\"";
  protected final String TEXT_7 = "\");" + NL + "      boolean bStarted_";
  protected final String TEXT_8 = " = qasSearch_";
  protected final String TEXT_9 = ".batchStart(";
  protected final String TEXT_10 = ");";

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    
  CodeGeneratorArgument codeGenArgument = (CodeGeneratorArgument) argument;
  INode node = (INode)codeGenArgument.getArgument();
  String cid = node.getUniqueName();
  List<IMetadataTable> metadatas = node.getMetadataList();
  
  if ((metadatas != null) && (metadatas.size() > 0)) {
    IMetadataTable metadata = metadatas.get(0);
    
    if (metadata != null) {
      String sIniFilePath = ElementParameterParser.getValue(node, "__PATH__");
      String sCountry = ElementParameterParser.getValue(node, "__COUNTRY__");
      String qasVersion = ElementParameterParser.getValue(node, "__QAS_VERSION__");
      
    stringBuffer.append(TEXT_1);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_2);
    stringBuffer.append(qasVersion);
    stringBuffer.append(TEXT_3);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_4);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_5);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_6);
    stringBuffer.append(sCountry);
    stringBuffer.append(TEXT_7);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_8);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_9);
    stringBuffer.append(sIniFilePath);
    stringBuffer.append(TEXT_10);
    
    }
  }

    return stringBuffer.toString();
  }
}
