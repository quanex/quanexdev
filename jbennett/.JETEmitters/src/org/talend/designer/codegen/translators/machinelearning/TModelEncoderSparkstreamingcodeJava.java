package org.talend.designer.codegen.translators.machinelearning;

import org.talend.core.model.process.INode;
import org.talend.core.model.process.IConnection;
import org.talend.core.model.metadata.IMetadataColumn;
import org.talend.designer.common.BigDataCodeGeneratorArgument;
import org.talend.designer.common.tsqlrow.TSqlRowUtil;
import org.talend.designer.common.tmodelencoder.TModelEncoderUtil;
import org.talend.core.model.process.IBigDataNode;
import org.talend.core.model.process.ElementParameterParser;

public class TModelEncoderSparkstreamingcodeJava
{
  protected static String nl;
  public static synchronized TModelEncoderSparkstreamingcodeJava create(String lineSeparator)
  {
    nl = lineSeparator;
    TModelEncoderSparkstreamingcodeJava result = new TModelEncoderSparkstreamingcodeJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = " " + NL + "    \tlog.warn(\"Component does not have Date/Timestamp checkbox, will use Timestamp as default\");";
  protected final String TEXT_2 = " " + NL + "    \tSystem.err.println(\"Component does not have Date/Timestamp checkbox, will use Timestamp as default\");";
  protected final String TEXT_3 = NL + "public static class ";
  protected final String TEXT_4 = "_FromRowTo";
  protected final String TEXT_5 = " implements org.apache.spark.api.java.function.Function<org.apache.spark.sql.Row, ";
  protected final String TEXT_6 = "> {" + NL + "" + NL + "    public ";
  protected final String TEXT_7 = " call(org.apache.spark.sql.Row row) {";
  protected final String TEXT_8 = NL + "        ";
  protected final String TEXT_9 = " result = new ";
  protected final String TEXT_10 = "();" + NL + "        org.apache.spark.sql.types.StructField[] structFields = row.schema().fields();" + NL + "        for (int i = 0; i < structFields.length; i++) {" + NL + "            org.apache.avro.Schema.Field avroField = ";
  protected final String TEXT_11 = ".getClassSchema().getField(structFields[i].name());" + NL + "            if (avroField != null){" + NL + "                result.put(avroField.pos(), row.get(i));" + NL + "            }" + NL + "        }" + NL + "        return result;" + NL + "    }" + NL + "}";
  protected final String TEXT_12 = NL + NL + "\t\tpublic static class ";
  protected final String TEXT_13 = "_From";
  protected final String TEXT_14 = "To";
  protected final String TEXT_15 = " implements org.apache.spark.api.java.function.Function<";
  protected final String TEXT_16 = ", ";
  protected final String TEXT_17 = "> {" + NL + "" + NL + "\t\t\tpublic ";
  protected final String TEXT_18 = " call(";
  protected final String TEXT_19 = " input) {" + NL + "\t\t\t\t";
  protected final String TEXT_20 = " result = new ";
  protected final String TEXT_21 = "();";
  protected final String TEXT_22 = NL + "\t\t\t\t\t\tif(input.";
  protected final String TEXT_23 = " != null) {" + NL + "\t\t\t\t\t\t\tresult.";
  protected final String TEXT_24 = " = new java.sql.";
  protected final String TEXT_25 = "(input.";
  protected final String TEXT_26 = ".getTime());" + NL + "\t\t\t\t\t\t} else {" + NL + "\t\t\t\t\t\t\tresult.";
  protected final String TEXT_27 = " = null;" + NL + "\t\t\t\t\t\t}";
  protected final String TEXT_28 = NL + "\t\t\t\t\tresult.";
  protected final String TEXT_29 = " = input.";
  protected final String TEXT_30 = ";";
  protected final String TEXT_31 = NL + "\t\t\t\treturn result;" + NL + "\t\t\t}" + NL + "\t\t}";
  protected final String TEXT_32 = NL + NL + "    static class ";
  protected final String TEXT_33 = "_JavaSQLContextSingleton {" + NL + "      static private transient org.apache.spark.sql.SQLContext instance = null;" + NL + "      static public org.apache.spark.sql.SQLContext getInstance(org.apache.spark.SparkContext sparkContext) {" + NL + "        if (instance == null) {" + NL + "          instance = new org.apache.spark.sql.SQLContext(sparkContext);" + NL + "        }" + NL + "        return instance;" + NL + "      }" + NL + "    }" + NL + "" + NL + "\tpublic static class Function_";
  protected final String TEXT_34 = " implements org.apache.spark.api.java.function.Function<org.apache.spark.api.java.JavaRDD<";
  protected final String TEXT_35 = ">, org.apache.spark.api.java.JavaRDD<";
  protected final String TEXT_36 = ">> {" + NL + "" + NL + "        private java.util.List<org.apache.spark.ml.Transformer> transformations;" + NL + "" + NL + "        public Function_";
  protected final String TEXT_37 = "(java.util.List<org.apache.spark.ml.Transformer> transformations){" + NL + "            this.transformations = transformations;" + NL + "        }" + NL + "" + NL + "        @Override" + NL + "        public org.apache.spark.api.java.JavaRDD<";
  protected final String TEXT_38 = "> call(org.apache.spark.api.java.JavaRDD<";
  protected final String TEXT_39 = "> inputrdd){" + NL + "            // Create dataframe from first tModelEncoder's input rdd" + NL + "            org.apache.spark.sql.SQLContext sqlContext = ";
  protected final String TEXT_40 = "_JavaSQLContextSingleton.getInstance(inputrdd.context());" + NL;
  protected final String TEXT_41 = NL + "            ";
  protected final String TEXT_42 = " inputDataframe = sqlContext.createDataFrame(inputrdd, ";
  protected final String TEXT_43 = ".class);" + NL + "" + NL + "            // apply transformations to input RDD";
  protected final String TEXT_44 = NL + "            ";
  protected final String TEXT_45 = " transformedInputDataframe = applyTransformations(inputDataframe, transformations);" + NL + "" + NL + "            // Creat output JavaRDD" + NL + "            return transformedInputDataframe.toJavaRDD().map(new ";
  protected final String TEXT_46 = "_FromRowTo";
  protected final String TEXT_47 = "());" + NL + "        }" + NL + "" + NL + "        public ";
  protected final String TEXT_48 = " applyTransformations(";
  protected final String TEXT_49 = " df, java.util.List<org.apache.spark.ml.Transformer> transformations){" + NL + "        \t\t";
  protected final String TEXT_50 = " currentDf = df;" + NL + "        \t\tfor(org.apache.spark.ml.Transformer transformation : transformations) {" + NL + "        \t\t\tcurrentDf = transformation.transform(currentDf);" + NL + "        \t\t}" + NL + "        \t\treturn currentDf;" + NL + "        }" + NL + "\t}";
  protected final String TEXT_51 = NL;
  protected final String TEXT_52 = NL;

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    
// Parse the inputs to this javajet generator.
final BigDataCodeGeneratorArgument codeGenArgument = (BigDataCodeGeneratorArgument)argument;
final INode node = (INode)codeGenArgument.getArgument();
final String cid = node.getUniqueName();

final String dataframeClass = org.talend.hadoop.distribution.ESparkVersion.SPARK_2_0.compareTo(codeGenArgument.getSparkVersion()) > 0
    ? "org.apache.spark.sql.DataFrame"
    : "org.apache.spark.sql.Dataset<org.apache.spark.sql.Row>";

    
TSqlRowUtil tSqlRowUtil = new TSqlRowUtil(node);
String validateError = tSqlRowUtil.validate(true, true);
if (validateError != null) {
    // Cause the job compilation to explicitly fail if there is a problem.
    return "throw new JobConfigurationError(\"" + validateError +"\");";
}

boolean isLog4jEnabled = ("true").equals(ElementParameterParser.getValue(node.getProcess(), "__LOG4J_ACTIVATE__"));

boolean useTimestampForDatesInDataframes = true;
try{
    useTimestampForDatesInDataframes = ElementParameterParser.getBooleanValue(node, "__DATE_TO_TIMESTAMP_DF_TYPE_SUBSTITUTION__");
} catch(Exception e){
    if (isLog4jEnabled) { 
    
    stringBuffer.append(TEXT_1);
     
    } else { 
    
    stringBuffer.append(TEXT_2);
     
    } 
}

String outStructName = codeGenArgument.getRecordStructName(tSqlRowUtil.getOutgoingConnection());

    stringBuffer.append(TEXT_3);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_4);
    stringBuffer.append(outStructName);
    stringBuffer.append(TEXT_5);
    stringBuffer.append(outStructName);
    stringBuffer.append(TEXT_6);
    stringBuffer.append(outStructName);
    stringBuffer.append(TEXT_7);
    stringBuffer.append(TEXT_8);
    stringBuffer.append(outStructName);
    stringBuffer.append(TEXT_9);
    stringBuffer.append(outStructName);
    stringBuffer.append(TEXT_10);
    stringBuffer.append(outStructName);
    stringBuffer.append(TEXT_11);
    
// If the incoming rowStruct contains a Date field (always typed as java.util.Date),
// we must generate a new structure which replaces these java.util.Date instances by
// java.sql.Date or java.sql.Timestamp instances.

org.talend.designer.bigdata.avro.AvroRecordStructGenerator avroRecordStructGenerator = (org.talend.designer.bigdata.avro.AvroRecordStructGenerator) codeGenArgument.getRecordStructGenerator();

// Some of the incoming connections might share the same schema (and then the same rowXStruct). We must generate the below code only once by schema (if necessary).
java.util.Set<String> knownStructNames = new java.util.HashSet();

for(IConnection incomingConnection : tSqlRowUtil.getIncomingConnections()) {
	String originalStructName = codeGenArgument.getRecordStructName(incomingConnection);
	if(tSqlRowUtil.containsDateFields(incomingConnection) && !knownStructNames.contains(originalStructName)) {
		java.util.List<IMetadataColumn> columns = tSqlRowUtil.getColumns(incomingConnection);
		String suggestedDfStructName = "DF_"+originalStructName;
		String dfStructName = avroRecordStructGenerator.generateRecordStructForDataFrame(suggestedDfStructName, originalStructName, useTimestampForDatesInDataframes);
		knownStructNames.add(originalStructName);

    stringBuffer.append(TEXT_12);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_13);
    stringBuffer.append(originalStructName);
    stringBuffer.append(TEXT_14);
    stringBuffer.append(dfStructName);
    stringBuffer.append(TEXT_15);
    stringBuffer.append(originalStructName);
    stringBuffer.append(TEXT_16);
    stringBuffer.append(dfStructName);
    stringBuffer.append(TEXT_17);
    stringBuffer.append(dfStructName);
    stringBuffer.append(TEXT_18);
    stringBuffer.append(originalStructName);
    stringBuffer.append(TEXT_19);
    stringBuffer.append(dfStructName);
    stringBuffer.append(TEXT_20);
    stringBuffer.append(dfStructName);
    stringBuffer.append(TEXT_21);
    
				for(IMetadataColumn column : columns) {
					if(tSqlRowUtil.isDateField(column)) {

    stringBuffer.append(TEXT_22);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_23);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_24);
    stringBuffer.append(useTimestampForDatesInDataframes ? "Timestamp" : "Date");
    stringBuffer.append(TEXT_25);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_26);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_27);
    
				} else {

    stringBuffer.append(TEXT_28);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_29);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_30);
    
				}
			} // end for(IMetadataColumn column : columns)

    stringBuffer.append(TEXT_31);
    
	} // end if(tSqlRowUtil.containsDateFields(incomingConnection) && !knownStructNames.contains(originalStructName))
} // end for(IConnection incomingConnection : tSqlRowUtil.getIncomingConnections())

    
TModelEncoderUtil tModelEncoderUtil = new TModelEncoderUtil(node);
String inputDStreamStructName = tModelEncoderUtil.getFirstModelEncoderStructName(codeGenArgument);
outStructName = codeGenArgument.getRecordStructName(tModelEncoderUtil.getOutgoingConnection());

if(tModelEncoderUtil.isLastModelEncoder()) {

    stringBuffer.append(TEXT_32);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_33);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_34);
    stringBuffer.append(inputDStreamStructName);
    stringBuffer.append(TEXT_35);
    stringBuffer.append(outStructName);
    stringBuffer.append(TEXT_36);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_37);
    stringBuffer.append(outStructName);
    stringBuffer.append(TEXT_38);
    stringBuffer.append(inputDStreamStructName);
    stringBuffer.append(TEXT_39);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_40);
    stringBuffer.append(TEXT_41);
    stringBuffer.append(dataframeClass);
    stringBuffer.append(TEXT_42);
    stringBuffer.append(inputDStreamStructName);
    stringBuffer.append(TEXT_43);
    stringBuffer.append(TEXT_44);
    stringBuffer.append(dataframeClass);
    stringBuffer.append(TEXT_45);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_46);
    stringBuffer.append(outStructName);
    stringBuffer.append(TEXT_47);
    stringBuffer.append(dataframeClass);
    stringBuffer.append(TEXT_48);
    stringBuffer.append(dataframeClass);
    stringBuffer.append(TEXT_49);
    stringBuffer.append(dataframeClass);
    stringBuffer.append(TEXT_50);
    
}

    stringBuffer.append(TEXT_51);
    stringBuffer.append(TEXT_52);
    return stringBuffer.toString();
  }
}
