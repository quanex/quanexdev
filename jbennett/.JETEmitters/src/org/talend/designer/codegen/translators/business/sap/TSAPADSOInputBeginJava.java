package org.talend.designer.codegen.translators.business.sap;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.talend.core.model.metadata.IMetadataColumn;
import org.talend.core.model.metadata.IMetadataTable;
import org.talend.core.model.metadata.types.JavaType;
import org.talend.core.model.metadata.types.JavaTypesManager;
import org.talend.core.model.process.ElementParameterParser;
import org.talend.core.model.process.IConnection;
import org.talend.core.model.process.IConnectionCategory;
import org.talend.core.model.process.INode;
import org.talend.designer.codegen.config.CodeGeneratorArgument;
import org.talend.core.model.utils.TalendTextUtils;

public class TSAPADSOInputBeginJava
{
  protected static String nl;
  public static synchronized TSAPADSOInputBeginJava create(String lineSeparator)
  {
    nl = lineSeparator;
    TSAPADSOInputBeginJava result = new TSAPADSOInputBeginJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "";
  protected final String TEXT_2 = NL + "\t" + NL + "\tclass PropertyUil_";
  protected final String TEXT_3 = " {" + NL + "\t\t" + NL + "        void validateAndSet(java.util.Properties p, String key, Object value) {" + NL + "        \tif(value==null) {" + NL + "        \t\tSystem.err.println(\"WARN : will ignore the property : \" + key + \" as setting the null value.\"); " + NL + "        \t\treturn;" + NL + "        \t}" + NL + "        \t" + NL + "        \tp.setProperty(key, String.valueOf(value));" + NL + "        }" + NL + "        " + NL + "\t}" + NL + "\t" + NL + "\tPropertyUil_";
  protected final String TEXT_4 = " pu_";
  protected final String TEXT_5 = " = new PropertyUil_";
  protected final String TEXT_6 = "();" + NL + "" + NL + "\t";
  protected final String TEXT_7 = " " + NL + "\tfinal String decryptedPassword_";
  protected final String TEXT_8 = " = routines.system.PasswordEncryptUtil.decryptPassword(";
  protected final String TEXT_9 = ");";
  protected final String TEXT_10 = NL + "\tfinal String decryptedPassword_";
  protected final String TEXT_11 = " = ";
  protected final String TEXT_12 = "; ";
  protected final String TEXT_13 = NL + "\tjava.util.Properties properties_";
  protected final String TEXT_14 = " = new java.util.Properties();" + NL + "\t\t" + NL + "\tpu_";
  protected final String TEXT_15 = ".validateAndSet(properties_";
  protected final String TEXT_16 = ", org.talend.sap.ISAPHanaConnection.PROP_DB_HOST, ";
  protected final String TEXT_17 = ");" + NL + "\tpu_";
  protected final String TEXT_18 = ".validateAndSet(properties_";
  protected final String TEXT_19 = ", org.talend.sap.ISAPHanaConnection.PROP_DB_PORT, ";
  protected final String TEXT_20 = ");" + NL + "\tpu_";
  protected final String TEXT_21 = ".validateAndSet(properties_";
  protected final String TEXT_22 = ", org.talend.sap.ISAPHanaConnection.PROP_DB_SCHEMA, ";
  protected final String TEXT_23 = ");" + NL + "\tpu_";
  protected final String TEXT_24 = ".validateAndSet(properties_";
  protected final String TEXT_25 = ", org.talend.sap.ISAPHanaConnection.PROP_DB_USERNAME, ";
  protected final String TEXT_26 = ");" + NL + "\tpu_";
  protected final String TEXT_27 = ".validateAndSet(properties_";
  protected final String TEXT_28 = ", org.talend.sap.ISAPHanaConnection.PROP_DB_PASSWORD, decryptedPassword_";
  protected final String TEXT_29 = ");" + NL + "        " + NL + "\torg.talend.sap.ISAPHanaConnection connection_";
  protected final String TEXT_30 = " = org.talend.sap.impl.SAPHanaConnectionFactory.getInstance().createConnection(properties_";
  protected final String TEXT_31 = ");" + NL + "\t" + NL + "\torg.talend.sap.service.ISAPBWService dataService_";
  protected final String TEXT_32 = " = connection_";
  protected final String TEXT_33 = ".getBWService();" + NL + "\t" + NL + "\ttry {";
  protected final String TEXT_34 = NL + "\t\torg.talend.sap.model.bw.request.ISAPAdvancedDataStoreObjectRequest request_";
  protected final String TEXT_35 = " = dataService_";
  protected final String TEXT_36 = ".createAdvancedDataStoreObjectRequest(";
  protected final String TEXT_37 = ")" + NL + "\t\t";
  protected final String TEXT_38 = NL + "\t\t\t.addField(\"";
  protected final String TEXT_39 = "\")";
  protected final String TEXT_40 = NL + "\t\t;" + NL + "\t\t" + NL + "\t\torg.talend.sap.IGenericDataAccess data_";
  protected final String TEXT_41 = " = request_";
  protected final String TEXT_42 = ".readable();" + NL + "" + NL + "\t\twhile (data_";
  protected final String TEXT_43 = ".hasNextRow()) {" + NL + "\t\t";
  protected final String TEXT_44 = NL + "\t\t\t";
  protected final String TEXT_45 = ".";
  protected final String TEXT_46 = " = data_";
  protected final String TEXT_47 = ".getString(\"";
  protected final String TEXT_48 = "\");";
  protected final String TEXT_49 = NL + "\t\t\t";
  protected final String TEXT_50 = ".";
  protected final String TEXT_51 = " = data_";
  protected final String TEXT_52 = ".getInteger(\"";
  protected final String TEXT_53 = "\");";
  protected final String TEXT_54 = NL + "\t\t\t";
  protected final String TEXT_55 = ".";
  protected final String TEXT_56 = " = data_";
  protected final String TEXT_57 = ".getLong(\"";
  protected final String TEXT_58 = "\");";
  protected final String TEXT_59 = NL + "\t\t\t";
  protected final String TEXT_60 = ".";
  protected final String TEXT_61 = " = data_";
  protected final String TEXT_62 = ".getShort(\"";
  protected final String TEXT_63 = "\");";
  protected final String TEXT_64 = NL + "\t\t\t";
  protected final String TEXT_65 = ".";
  protected final String TEXT_66 = " = data_";
  protected final String TEXT_67 = ".getDate(\"";
  protected final String TEXT_68 = "\");";
  protected final String TEXT_69 = NL + "\t\t\t";
  protected final String TEXT_70 = ".";
  protected final String TEXT_71 = " = data_";
  protected final String TEXT_72 = ".getByte(\"";
  protected final String TEXT_73 = "\");";
  protected final String TEXT_74 = NL + "\t\t\t";
  protected final String TEXT_75 = ".";
  protected final String TEXT_76 = " = data_";
  protected final String TEXT_77 = ".getRaw(\"";
  protected final String TEXT_78 = "\");";
  protected final String TEXT_79 = NL + "\t\t\t";
  protected final String TEXT_80 = ".";
  protected final String TEXT_81 = " = data_";
  protected final String TEXT_82 = ".getDouble(\"";
  protected final String TEXT_83 = "\");";
  protected final String TEXT_84 = NL + "\t\t\t";
  protected final String TEXT_85 = ".";
  protected final String TEXT_86 = " = data_";
  protected final String TEXT_87 = ".getFloat(\"";
  protected final String TEXT_88 = "\");";
  protected final String TEXT_89 = NL + "\t\t\tjava.math.BigInteger bi_";
  protected final String TEXT_90 = "_";
  protected final String TEXT_91 = " = data_";
  protected final String TEXT_92 = ".getBigInteger(\"";
  protected final String TEXT_93 = "\");" + NL + "\t\t\t";
  protected final String TEXT_94 = ".";
  protected final String TEXT_95 = " = bi_";
  protected final String TEXT_96 = "_";
  protected final String TEXT_97 = "==null ? null : new java.math.BigDecimal(bi_";
  protected final String TEXT_98 = "_";
  protected final String TEXT_99 = ");";
  protected final String TEXT_100 = NL + "\t\t\t";
  protected final String TEXT_101 = ".";
  protected final String TEXT_102 = " = data_";
  protected final String TEXT_103 = ".getBigDecimal(\"";
  protected final String TEXT_104 = "\");";
  protected final String TEXT_105 = NL + "\t\t\t";
  protected final String TEXT_106 = ".";
  protected final String TEXT_107 = " = data_";
  protected final String TEXT_108 = ".getBigInteger(\"";
  protected final String TEXT_109 = "\");\t\t";
  protected final String TEXT_110 = NL + "\t\t\t";
  protected final String TEXT_111 = ".";
  protected final String TEXT_112 = " = data_";
  protected final String TEXT_113 = ".getString(\"";
  protected final String TEXT_114 = "\");";
  protected final String TEXT_115 = NL + "\t\t\t";
  protected final String TEXT_116 = ".";
  protected final String TEXT_117 = " = ParserUtils.parseTo_";
  protected final String TEXT_118 = "(data_";
  protected final String TEXT_119 = ".getString(\"";
  protected final String TEXT_120 = "\"));";

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    stringBuffer.append(TEXT_1);
    
	CodeGeneratorArgument codeGenArgument = (CodeGeneratorArgument) argument;
	INode node = (INode)codeGenArgument.getArgument();
	String cid = node.getUniqueName();
	
	List<? extends IConnection> outputConnections = node.getOutgoingSortedConnections();
	if((outputConnections == null) || (outputConnections.size() == 0)) {
		return "";
	}
	IConnection outputConnection = outputConnections.get(0);
	
	if(!outputConnection.getLineStyle().hasConnectionCategory(IConnectionCategory.DATA)) {
		return "";
	}
	
	List<IMetadataTable> metadatas = node.getMetadataList();
	if ((metadatas == null) && (metadatas.size() == 0) || (metadatas.get(0) == null)) {
		return "";
	}
	IMetadataTable metadata = metadatas.get(0);
	
	List<IMetadataColumn> columnList = metadata.getListColumns();
	if((columnList == null) || (columnList.size() == 0)) {
		return "";
	}
	
	//sap hana parameters
	String saphana_host = ElementParameterParser.getValue(node, "__SAPHANA_HOST__");
	String saphana_port = ElementParameterParser.getValue(node, "__SAPHANA_PORT__");
	String saphana_tableschema = ElementParameterParser.getValue(node, "__SAPHANA_TABLESCHEMA__");
	String saphana_user = ElementParameterParser.getValue(node, "__SAPHANA_USER__");
	
	String tableName = ElementParameterParser.getValue(node, "__TABLE__");
	
	String passwordFieldName = "__SAPHANA_PASS__";

    stringBuffer.append(TEXT_2);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_3);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_4);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_5);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_6);
    if (ElementParameterParser.canEncrypt(node, passwordFieldName)) {
    stringBuffer.append(TEXT_7);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_8);
    stringBuffer.append(ElementParameterParser.getEncryptedValue(node, passwordFieldName));
    stringBuffer.append(TEXT_9);
    } else {
    stringBuffer.append(TEXT_10);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_11);
    stringBuffer.append( ElementParameterParser.getValue(node, passwordFieldName));
    stringBuffer.append(TEXT_12);
    }
    stringBuffer.append(TEXT_13);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_14);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_15);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_16);
    stringBuffer.append(saphana_host);
    stringBuffer.append(TEXT_17);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_18);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_19);
    stringBuffer.append(saphana_port);
    stringBuffer.append(TEXT_20);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_21);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_22);
    stringBuffer.append(saphana_tableschema);
    stringBuffer.append(TEXT_23);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_24);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_25);
    stringBuffer.append(saphana_user);
    stringBuffer.append(TEXT_26);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_27);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_28);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_29);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_30);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_31);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_32);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_33);
    
		
    stringBuffer.append(TEXT_34);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_35);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_36);
    stringBuffer.append(tableName);
    stringBuffer.append(TEXT_37);
    
		for(int i=0;i<columnList.size();i++) {
			IMetadataColumn column = columnList.get(i);
			String tableField = column.getOriginalDbColumnName();

    stringBuffer.append(TEXT_38);
    stringBuffer.append(tableField);
    stringBuffer.append(TEXT_39);
    
		}

    stringBuffer.append(TEXT_40);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_41);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_42);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_43);
    
	for(int i=0;i<columnList.size();i++) {
		IMetadataColumn column = columnList.get(i);
	    String typeToGenerate = JavaTypesManager.getTypeToGenerate(column.getTalendType(),column.isNullable());
	    JavaType javaType = JavaTypesManager.getJavaTypeFromId(column.getTalendType());
	    String dbType = column.getType();
		String tableField = column.getOriginalDbColumnName();
	    
		if(javaType == JavaTypesManager.STRING) {

    stringBuffer.append(TEXT_44);
    stringBuffer.append(outputConnection.getName() );
    stringBuffer.append(TEXT_45);
    stringBuffer.append(column.getLabel() );
    stringBuffer.append(TEXT_46);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_47);
    stringBuffer.append(tableField);
    stringBuffer.append(TEXT_48);
    
		} else if(javaType == JavaTypesManager.INTEGER) {

    stringBuffer.append(TEXT_49);
    stringBuffer.append(outputConnection.getName() );
    stringBuffer.append(TEXT_50);
    stringBuffer.append(column.getLabel() );
    stringBuffer.append(TEXT_51);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_52);
    stringBuffer.append(tableField);
    stringBuffer.append(TEXT_53);
    
		} else if(javaType == JavaTypesManager.LONG) {

    stringBuffer.append(TEXT_54);
    stringBuffer.append(outputConnection.getName() );
    stringBuffer.append(TEXT_55);
    stringBuffer.append(column.getLabel() );
    stringBuffer.append(TEXT_56);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_57);
    stringBuffer.append(tableField);
    stringBuffer.append(TEXT_58);
    
		} else if(javaType == JavaTypesManager.SHORT) {

    stringBuffer.append(TEXT_59);
    stringBuffer.append(outputConnection.getName() );
    stringBuffer.append(TEXT_60);
    stringBuffer.append(column.getLabel() );
    stringBuffer.append(TEXT_61);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_62);
    stringBuffer.append(tableField);
    stringBuffer.append(TEXT_63);
    
		} else if(javaType == JavaTypesManager.DATE) {

    stringBuffer.append(TEXT_64);
    stringBuffer.append(outputConnection.getName() );
    stringBuffer.append(TEXT_65);
    stringBuffer.append(column.getLabel() );
    stringBuffer.append(TEXT_66);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_67);
    stringBuffer.append(tableField);
    stringBuffer.append(TEXT_68);
    
		} else if(javaType == JavaTypesManager.BYTE) {

    stringBuffer.append(TEXT_69);
    stringBuffer.append(outputConnection.getName() );
    stringBuffer.append(TEXT_70);
    stringBuffer.append(column.getLabel() );
    stringBuffer.append(TEXT_71);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_72);
    stringBuffer.append(tableField);
    stringBuffer.append(TEXT_73);
    
		} else if(javaType == JavaTypesManager.BYTE_ARRAY) {

    stringBuffer.append(TEXT_74);
    stringBuffer.append(outputConnection.getName() );
    stringBuffer.append(TEXT_75);
    stringBuffer.append(column.getLabel() );
    stringBuffer.append(TEXT_76);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_77);
    stringBuffer.append(tableField);
    stringBuffer.append(TEXT_78);
    
		} else if(javaType == JavaTypesManager.DOUBLE) {

    stringBuffer.append(TEXT_79);
    stringBuffer.append(outputConnection.getName() );
    stringBuffer.append(TEXT_80);
    stringBuffer.append(column.getLabel() );
    stringBuffer.append(TEXT_81);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_82);
    stringBuffer.append(tableField);
    stringBuffer.append(TEXT_83);
    
		} else if(javaType == JavaTypesManager.FLOAT) {

    stringBuffer.append(TEXT_84);
    stringBuffer.append(outputConnection.getName() );
    stringBuffer.append(TEXT_85);
    stringBuffer.append(column.getLabel() );
    stringBuffer.append(TEXT_86);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_87);
    stringBuffer.append(tableField);
    stringBuffer.append(TEXT_88);
    
		} else if(javaType == JavaTypesManager.BIGDECIMAL) {
			if("BIG_INTEGER".equals(dbType)) {

    stringBuffer.append(TEXT_89);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_90);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_91);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_92);
    stringBuffer.append(tableField);
    stringBuffer.append(TEXT_93);
    stringBuffer.append(outputConnection.getName() );
    stringBuffer.append(TEXT_94);
    stringBuffer.append(column.getLabel() );
    stringBuffer.append(TEXT_95);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_96);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_97);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_98);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_99);
    
			} else {

    stringBuffer.append(TEXT_100);
    stringBuffer.append(outputConnection.getName() );
    stringBuffer.append(TEXT_101);
    stringBuffer.append(column.getLabel() );
    stringBuffer.append(TEXT_102);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_103);
    stringBuffer.append(tableField);
    stringBuffer.append(TEXT_104);
    
			}
		} else if(javaType == JavaTypesManager.OBJECT && "BIG_INTEGER".equals(dbType)) {

    stringBuffer.append(TEXT_105);
    stringBuffer.append(outputConnection.getName() );
    stringBuffer.append(TEXT_106);
    stringBuffer.append(column.getLabel() );
    stringBuffer.append(TEXT_107);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_108);
    stringBuffer.append(tableField);
    stringBuffer.append(TEXT_109);
    
		} else if(javaType == JavaTypesManager.OBJECT) {

    stringBuffer.append(TEXT_110);
    stringBuffer.append(outputConnection.getName() );
    stringBuffer.append(TEXT_111);
    stringBuffer.append(column.getLabel() );
    stringBuffer.append(TEXT_112);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_113);
    stringBuffer.append(tableField);
    stringBuffer.append(TEXT_114);
    
		} else {

    stringBuffer.append(TEXT_115);
    stringBuffer.append(outputConnection.getName() );
    stringBuffer.append(TEXT_116);
    stringBuffer.append(column.getLabel() );
    stringBuffer.append(TEXT_117);
    stringBuffer.append(typeToGenerate);
    stringBuffer.append(TEXT_118);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_119);
    stringBuffer.append(tableField);
    stringBuffer.append(TEXT_120);
    
		}
	}

    return stringBuffer.toString();
  }
}
