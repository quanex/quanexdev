package org.talend.designer.codegen.translators.databases.dbspecifics.saphana;

import org.talend.core.model.process.INode;
import org.talend.core.model.process.ElementParameterParser;
import org.talend.designer.codegen.config.CodeGeneratorArgument;
import org.talend.core.model.metadata.IMetadataTable;
import org.talend.core.model.metadata.IMetadataColumn;
import org.talend.core.model.metadata.MetadataTalendType;
import org.talend.core.model.metadata.MappingTypeRetriever;
import org.talend.core.model.metadata.types.JavaType;
import org.talend.core.model.metadata.types.JavaTypesManager;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;

public class TSAPHanaUnloadBeginJava
{
  protected static String nl;
  public static synchronized TSAPHanaUnloadBeginJava create(String lineSeparator)
  {
    nl = lineSeparator;
    TSAPHanaUnloadBeginJava result = new TSAPHanaUnloadBeginJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "";
  protected final String TEXT_2 = NL + "org.talend.sap.ISAPHanaConnection hanaConnection_";
  protected final String TEXT_3 = " = new org.talend.sap.impl.SAPHanaConnection((java.sql.Connection) globalMap.get(\"";
  protected final String TEXT_4 = "\"));" + NL + "String dbschema_";
  protected final String TEXT_5 = " = (String)globalMap.get(\"";
  protected final String TEXT_6 = "\");";
  protected final String TEXT_7 = " " + NL + "\tfinal String decryptedPassword_";
  protected final String TEXT_8 = " = routines.system.PasswordEncryptUtil.decryptPassword(";
  protected final String TEXT_9 = ");";
  protected final String TEXT_10 = NL + "\tfinal String decryptedPassword_";
  protected final String TEXT_11 = " = ";
  protected final String TEXT_12 = "; ";
  protected final String TEXT_13 = NL + NL + "java.util.Properties properties_";
  protected final String TEXT_14 = " = new java.util.Properties();" + NL + "properties_";
  protected final String TEXT_15 = ".setProperty(org.talend.sap.ISAPHanaConnection.PROP_DB_HOST, ";
  protected final String TEXT_16 = ");" + NL + "properties_";
  protected final String TEXT_17 = ".setProperty(org.talend.sap.ISAPHanaConnection.PROP_DB_PORT, ";
  protected final String TEXT_18 = ");" + NL + "properties_";
  protected final String TEXT_19 = ".setProperty(org.talend.sap.ISAPHanaConnection.PROP_DB_SCHEMA, ";
  protected final String TEXT_20 = ");" + NL + "properties_";
  protected final String TEXT_21 = ".setProperty(org.talend.sap.ISAPHanaConnection.PROP_DB_USERNAME, ";
  protected final String TEXT_22 = ");" + NL + "properties_";
  protected final String TEXT_23 = ".setProperty(org.talend.sap.ISAPHanaConnection.PROP_DB_PASSWORD, decryptedPassword_";
  protected final String TEXT_24 = ");" + NL + "properties_";
  protected final String TEXT_25 = ".setProperty(org.talend.sap.ISAPHanaConnection.PROP_DB_ADDITIONAL_PROPERTIES, ";
  protected final String TEXT_26 = ");" + NL + "" + NL + "org.talend.sap.ISAPHanaConnection hanaConnection_";
  protected final String TEXT_27 = " = org.talend.sap.impl.SAPHanaConnectionFactory.getInstance().createConnection(properties_";
  protected final String TEXT_28 = ");" + NL + "" + NL + "String dbschema_";
  protected final String TEXT_29 = " = ";
  protected final String TEXT_30 = ";";
  protected final String TEXT_31 = NL + NL + "org.talend.sap.model.hana.ISAPHanaCsvExportResult result_";
  protected final String TEXT_32 = ";" + NL + "try {" + NL + "" + NL + "  // Export CSV" + NL + "  result_";
  protected final String TEXT_33 = " = hanaConnection_";
  protected final String TEXT_34 = ".createCsvExport(dbschema_";
  protected final String TEXT_35 = ", ";
  protected final String TEXT_36 = ")" + NL + "  \t.replace(";
  protected final String TEXT_37 = ")" + NL + "    .catalogOnly(";
  protected final String TEXT_38 = ")" + NL + "    .noDependencies(";
  protected final String TEXT_39 = ")" + NL + "    .noStatistics(";
  protected final String TEXT_40 = ")" + NL + "    .scramble(";
  protected final String TEXT_41 = ")";
  protected final String TEXT_42 = NL + "    .scrambleBy(";
  protected final String TEXT_43 = ")";
  protected final String TEXT_44 = NL + "    .statisticsOnly(";
  protected final String TEXT_45 = ")" + NL + "    .strip(";
  protected final String TEXT_46 = ")";
  protected final String TEXT_47 = NL + "    .threadSize(Integer.valueOf(";
  protected final String TEXT_48 = "))";
  protected final String TEXT_49 = NL + "    .exportInto(";
  protected final String TEXT_50 = ");" + NL + "} finally {";
  protected final String TEXT_51 = NL + "  hanaConnection_";
  protected final String TEXT_52 = ".close();";
  protected final String TEXT_53 = NL + "}" + NL + "" + NL + "globalMap.put(\"";
  protected final String TEXT_54 = "_QUERY\", result_";
  protected final String TEXT_55 = ".getSql());";

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    
CodeGeneratorArgument codeGenArgument = (CodeGeneratorArgument) argument;
INode node = (INode)codeGenArgument.getArgument();
String cid = node.getUniqueName();

boolean useExistingConnection = "true".equalsIgnoreCase(ElementParameterParser.getValue(node, "__USE_EXISTING_CONNECTION__"));
String conn = "conn_" + ElementParameterParser.getValue(node,"__CONNECTION__");
String schema = "dbschema_" + ElementParameterParser.getValue(node,"__CONNECTION__");

String dbHost = ElementParameterParser.getValue(node, "__HOST__");
String dbPort = ElementParameterParser.getValue(node, "__PORT__");
String dbSchema = ElementParameterParser.getValue(node, "__TABLESCHEMA__");
String dbUsername = ElementParameterParser.getValue(node, "__USER__");
String passwordFieldName = "__PASS__";
String dbAdditionalProperties = ElementParameterParser.getValue(node, "__PROPERTIES__");

String table = ElementParameterParser.getValue(node, "__TABLE__");
String exportPath = ElementParameterParser.getValue(node, "__EXPORT_PATH__");

// Advanced
boolean replace = "true".equalsIgnoreCase(ElementParameterParser.getValue(node, "__REPLACE__"));
boolean catalogOnly = "true".equalsIgnoreCase(ElementParameterParser.getValue(node, "__CATALOG_ONLY__"));
boolean noDependencies = "true".equalsIgnoreCase(ElementParameterParser.getValue(node, "__NO_DEPENDENCIES__"));
boolean noStatistics = "true".equalsIgnoreCase(ElementParameterParser.getValue(node, "__NO_STATISTICS__"));
boolean scramble = "true".equalsIgnoreCase(ElementParameterParser.getValue(node, "__SCRAMBLE__"));
String scrambleSeed = ElementParameterParser.getValue(node, "__SCRAMBLE_SEED__");
boolean statisticsOnly = "true".equalsIgnoreCase(ElementParameterParser.getValue(node, "__STATISTICS_ONLY__"));
boolean strip = "true".equalsIgnoreCase(ElementParameterParser.getValue(node, "__STRIP__"));

String threadSize = ElementParameterParser.getValue(node, "__THREAD_SIZE__");

    stringBuffer.append(TEXT_1);
     if (useExistingConnection) { 
    stringBuffer.append(TEXT_2);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_3);
    stringBuffer.append(conn );
    stringBuffer.append(TEXT_4);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_5);
    stringBuffer.append(schema);
    stringBuffer.append(TEXT_6);
     } else { 
    if (ElementParameterParser.canEncrypt(node, passwordFieldName)) {
    stringBuffer.append(TEXT_7);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_8);
    stringBuffer.append(ElementParameterParser.getEncryptedValue(node, passwordFieldName));
    stringBuffer.append(TEXT_9);
    } else {
    stringBuffer.append(TEXT_10);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_11);
    stringBuffer.append( ElementParameterParser.getValue(node, passwordFieldName));
    stringBuffer.append(TEXT_12);
    }
    stringBuffer.append(TEXT_13);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_14);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_15);
    stringBuffer.append(dbHost);
    stringBuffer.append(TEXT_16);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_17);
    stringBuffer.append(dbPort);
    stringBuffer.append(TEXT_18);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_19);
    stringBuffer.append(dbSchema);
    stringBuffer.append(TEXT_20);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_21);
    stringBuffer.append(dbUsername);
    stringBuffer.append(TEXT_22);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_23);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_24);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_25);
    stringBuffer.append(dbAdditionalProperties);
    stringBuffer.append(TEXT_26);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_27);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_28);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_29);
    stringBuffer.append(dbSchema);
    stringBuffer.append(TEXT_30);
     } 
    stringBuffer.append(TEXT_31);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_32);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_33);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_34);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_35);
    stringBuffer.append(table );
    stringBuffer.append(TEXT_36);
    stringBuffer.append(replace );
    stringBuffer.append(TEXT_37);
    stringBuffer.append(catalogOnly );
    stringBuffer.append(TEXT_38);
    stringBuffer.append(noDependencies );
    stringBuffer.append(TEXT_39);
    stringBuffer.append(noStatistics );
    stringBuffer.append(TEXT_40);
    stringBuffer.append(scramble );
    stringBuffer.append(TEXT_41);
     if (scrambleSeed != null && !scrambleSeed.isEmpty() && !scrambleSeed.equals("\"\"")) { 
    stringBuffer.append(TEXT_42);
    stringBuffer.append(scrambleSeed );
    stringBuffer.append(TEXT_43);
     } 
    stringBuffer.append(TEXT_44);
    stringBuffer.append(statisticsOnly );
    stringBuffer.append(TEXT_45);
    stringBuffer.append(strip );
    stringBuffer.append(TEXT_46);
     if (threadSize != null && !threadSize.isEmpty() && !threadSize.equals("\"\"")) { 
    stringBuffer.append(TEXT_47);
    stringBuffer.append(threadSize );
    stringBuffer.append(TEXT_48);
     } 
    stringBuffer.append(TEXT_49);
    stringBuffer.append(exportPath );
    stringBuffer.append(TEXT_50);
     if (!useExistingConnection) { 
    stringBuffer.append(TEXT_51);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_52);
     } 
    stringBuffer.append(TEXT_53);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_54);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_55);
    return stringBuffer.toString();
  }
}
