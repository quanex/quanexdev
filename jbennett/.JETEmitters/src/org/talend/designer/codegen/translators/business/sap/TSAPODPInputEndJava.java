package org.talend.designer.codegen.translators.business.sap;

import java.util.List;
import org.talend.core.model.metadata.IMetadataColumn;
import org.talend.core.model.metadata.IMetadataTable;
import org.talend.core.model.process.ElementParameterParser;
import org.talend.core.model.process.IConnection;
import org.talend.core.model.process.IConnectionCategory;
import org.talend.core.model.process.INode;
import org.talend.designer.codegen.config.CodeGeneratorArgument;

public class TSAPODPInputEndJava
{
  protected static String nl;
  public static synchronized TSAPODPInputEndJava create(String lineSeparator)
  {
    nl = lineSeparator;
    TSAPODPInputEndJava result = new TSAPODPInputEndJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "";
  protected final String TEXT_2 = NL + NL + "    nb_line_";
  protected final String TEXT_3 = "++;" + NL + "" + NL + "\t}" + NL + "} finally {" + NL + "\t";
  protected final String TEXT_4 = NL + "\tif(connection_";
  protected final String TEXT_5 = "!=null && connection_";
  protected final String TEXT_6 = ".isAlive()) {" + NL + "\t\tconnection_";
  protected final String TEXT_7 = ".close();" + NL + "\t}" + NL + "  " + NL + "\t";
  protected final String TEXT_8 = NL + "  ";
  protected final String TEXT_9 = NL + "  //wait for the producer thread over as it need to release the generated files when exception which may come from other components appear" + NL + "  Object data_";
  protected final String TEXT_10 = " = resourceMap.get(\"data_";
  protected final String TEXT_11 = "\");" + NL + "  if(data_";
  protected final String TEXT_12 = "!=null) {" + NL + "  \t((org.talend.sap.model.table.ISAPBatchData)data_";
  protected final String TEXT_13 = ").waitForResourceRelease();" + NL + "  }";
  protected final String TEXT_14 = NL + "}" + NL + "" + NL + "globalMap.put(\"";
  protected final String TEXT_15 = "_NB_LINE\", nb_line_";
  protected final String TEXT_16 = ");";
  protected final String TEXT_17 = NL;

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    stringBuffer.append(TEXT_1);
    
	CodeGeneratorArgument codeGenArgument = (CodeGeneratorArgument) argument;
	INode node = (INode)codeGenArgument.getArgument();
	String cid = node.getUniqueName();
	
	List<? extends IConnection> outputConnections = node.getOutgoingSortedConnections();
	if((outputConnections == null) || (outputConnections.size() == 0)) {
		return "";
	}
	IConnection outputConnection = outputConnections.get(0);
	
	if(!outputConnection.getLineStyle().hasConnectionCategory(IConnectionCategory.DATA)) {
		return "";
	}
	
	List<IMetadataTable> metadatas = node.getMetadataList();
	if ((metadatas == null) && (metadatas.size() == 0) || (metadatas.get(0) == null)) {
		return "";
	}
	IMetadataTable metadata = metadatas.get(0);
	
	List<IMetadataColumn> columnList = metadata.getListColumns();
	if((columnList == null) || (columnList.size() == 0)) {
		return "";
	}
	
	boolean useExistingConn = ("true").equals(ElementParameterParser.getValue(node,"__USE_EXISTING_CONNECTION__"));
  boolean useFtp = "true".equals(ElementParameterParser.getValue(node,"__USE_FTP_BATCH__"));

    stringBuffer.append(TEXT_2);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_3);
    if(!useExistingConn) {
    stringBuffer.append(TEXT_4);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_5);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_6);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_7);
    }
    stringBuffer.append(TEXT_8);
    if(useFtp){
    stringBuffer.append(TEXT_9);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_10);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_11);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_12);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_13);
    }
    stringBuffer.append(TEXT_14);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_15);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_16);
    stringBuffer.append(TEXT_17);
    return stringBuffer.toString();
  }
}
