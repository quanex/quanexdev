package org.talend.designer.codegen.translators.connectivity.services;

import org.talend.core.model.process.INode;
import org.talend.core.model.process.IProcess;
import org.talend.core.model.process.ElementParameterParser;
import org.talend.core.model.process.IConnection;
import org.talend.core.model.process.IElementParameter;
import org.talend.core.model.utils.JavaResourcesHelper;
import org.talend.designer.codegen.config.CodeGeneratorArgument;
import org.talend.designer.codegen.config.NodeParamsHelper;
import org.talend.designer.codegen.config.CamelEndpointBuilder;
import java.util.List;
import org.talend.core.model.process.ProcessUtils;

public class CRESTMainJava
{
  protected static String nl;
  public static synchronized CRESTMainJava create(String lineSeparator)
  {
    nl = lineSeparator;
    CRESTMainJava result = new CRESTMainJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "";
  protected final String TEXT_2 = NL;
  protected final String TEXT_3 = NL + "\t\tfrom(inOSGi || inMS ?";
  protected final String TEXT_4 = ":";
  protected final String TEXT_5 = ")" + NL + "\t\t.process(new org.apache.camel.Processor() {" + NL + "\t\t\t\tpublic void process(org.apache.camel.Exchange exchange) throws Exception {" + NL + "\t\t\t\t\torg.apache.camel.Message inMessage = exchange.getIn();" + NL + "\t\t\t\t\tinMessage.setHeader(\"http_query\"," + NL + "\t\t\t\t\t\torg.apache.cxf.jaxrs.utils.JAXRSUtils.getStructuredParams((String) inMessage.getHeader(org.apache.camel.Exchange.HTTP_QUERY), \"&\", false, false));" + NL + "\t\t\t\t}" + NL + "\t\t\t})";
  protected final String TEXT_6 = NL + "\t\t.setHeader(org.apache.camel.Exchange.HTTP_PATH, ";
  protected final String TEXT_7 = ")" + NL + "\t\t.setHeader(org.apache.camel.Exchange.HTTP_METHOD, constant(\"";
  protected final String TEXT_8 = "\"))";
  protected final String TEXT_9 = NL + "\t\t.setHeader(org.apache.camel.Exchange.ACCEPT_CONTENT_TYPE, constant(\"";
  protected final String TEXT_10 = "\"))";
  protected final String TEXT_11 = NL + "\t\t.setHeader(org.apache.camel.Exchange.CONTENT_TYPE, constant(\"";
  protected final String TEXT_12 = "\"))";
  protected final String TEXT_13 = NL + "\t\t.setHeader(org.apache.camel.component.cxf.common.message.CxfConstants.CAMEL_CXF_RS_USING_HTTP_API, constant(Boolean.FALSE))" + NL + "\t\t.setHeader(org.apache.camel.component.cxf.common.message.CxfConstants.OPERATION_NAME, ";
  protected final String TEXT_14 = ")";
  protected final String TEXT_15 = NL + "\t\t.process(new org.apache.camel.Processor() {" + NL + "\t\t\t\tpublic void process(org.apache.camel.Exchange exchange) throws Exception {" + NL + "\t\t\t\t\tcorrelationIDCallbackHandler_";
  protected final String TEXT_16 = ".setCorrelationId(simple(";
  protected final String TEXT_17 = ").evaluate(exchange, String.class));" + NL + "\t\t\t\t}" + NL + "\t\t\t})";
  protected final String TEXT_18 = NL + "\t\t.inOut(inOSGi?";
  protected final String TEXT_19 = ":";
  protected final String TEXT_20 = ")" + NL + "\t\t.unmarshal(new  org.apache.camel.spi.DataFormat() {" + NL + "\t\t\tpublic java.lang.Object unmarshal(org.apache.camel.Exchange exchange, java.io.InputStream is) throws java.lang.Exception {" + NL + "\t\t   \tjava.lang.Object b = exchange.getOut().getBody();" + NL + "\t\t   \tif(b instanceof org.apache.cxf.jaxrs.impl.ResponseImpl){" + NL + "\t\t   \t\torg.apache.cxf.jaxrs.impl.ResponseImpl r = (org.apache.cxf.jaxrs.impl.ResponseImpl)b;" + NL + "\t\t   \t\tif (\"javax.ws.rs.core.Response.class\".equalsIgnoreCase(\"";
  protected final String TEXT_21 = "\")) {" + NL + "\t\t   \t\t\treturn ";
  protected final String TEXT_22 = ".cast(r);" + NL + "\t\t   \t\t}" + NL + "\t\t   \t\tint status = r.getStatus();" + NL + "\t\t   \t\tif ((status < 200 || status == 204) && r.getLength() <= 0 || status >= 300) {" + NL + "\t\t   \t\t\treturn null;" + NL + "\t\t   \t\t}" + NL + "\t\t   \t\treturn r.doReadEntity(";
  protected final String TEXT_23 = ", ";
  protected final String TEXT_24 = ", new java.lang.annotation.Annotation[]{});" + NL + "\t\t   \t}" + NL + "\t\t   \treturn b;" + NL + "\t\t   }" + NL + "\t\t   public void marshal(org.apache.camel.Exchange exchange, Object o, java.io.OutputStream os)" + NL + "\t\t\t\t\tthrows Exception {}" + NL + "\t\t})";
  protected final String TEXT_25 = NL;

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    stringBuffer.append(TEXT_1);
    
	CodeGeneratorArgument codeGenArgument = (CodeGeneratorArgument) argument;

	INode node = (INode)codeGenArgument.getArgument();

	IProcess process = node.getProcess();

	String cid = node.getUniqueName();

	NodeParamsHelper paramsHelper = new NodeParamsHelper(node);

    boolean isProvider = node.getIncomingConnections().isEmpty();

    boolean hasOidcClientAuthentication = "true".equals(ElementParameterParser.getValue(node, "__ENABLE_SECURITY__"))
                                              && "OIDC".equals(ElementParameterParser.getValue(node, "__SECURITY_TYPE__"))
                                              && !node.getIncomingConnections().isEmpty();

	CamelEndpointBuilder builder = CamelEndpointBuilder.getBuilder();
	CamelEndpointBuilder builderRT = CamelEndpointBuilder.getBuilder();
	builder.useComponentColon(false);
	builder.useDoubleSlash(false);
	builderRT.useComponentColon(false);
	builderRT.useDoubleSlash(false);



    stringBuffer.append(TEXT_2);
    
	String endpointUrlRT = ElementParameterParser.getValue(node, "__URL__");

	String endpointUrlStudio = endpointUrlRT;

    if(isProvider) {

        if (endpointUrlRT != null && !endpointUrlRT.trim().isEmpty() && !endpointUrlRT.contains("://")) {
            if (endpointUrlRT.startsWith("\"/services")) {
                endpointUrlRT = endpointUrlRT.replaceFirst("/services","");
            }
            if (!endpointUrlRT.startsWith("/")) {
            	if(!endpointUrlRT.startsWith("context.")){
            		endpointUrlRT = endpointUrlRT;
            	}
            }
        }
        String defaultUri = (String) System.getProperties().get("restServiceDefaultUri");
        if (defaultUri != null && defaultUri.endsWith("/")) {
            defaultUri = defaultUri.substring(0, defaultUri.length()-1);
        }
        if (null == defaultUri || defaultUri.trim().isEmpty() || !defaultUri.contains("://")) {
        	defaultUri = "http://127.0.0.1:8090";
        }
        String defaultEndpointUrl = "\""+defaultUri+"\"";

        if (null == endpointUrlStudio || endpointUrlStudio.trim().isEmpty()) {
            endpointUrlStudio = defaultEndpointUrl;
        } else if (!endpointUrlStudio.contains("://")) { // relative
            if (endpointUrlStudio.startsWith("/")) {
                endpointUrlStudio = endpointUrlStudio.substring(1);
            }

            if(!endpointUrlStudio.startsWith("context.")){
            	endpointUrlStudio = defaultEndpointUrl + "+" + endpointUrlStudio;
            }
        }

		boolean isTestContainer = ProcessUtils.isTestContainer(process);

		String className = isTestContainer ? process.getName() + "Test" : process.getName();

		String routeFolderName = "";
	    IProcess baseProcess = ProcessUtils.getTestContainerBaseProcess(process);

	    if (baseProcess != null) {
	        routeFolderName = JavaResourcesHelper.getJobFolderName(baseProcess.getName(), baseProcess.getVersion()) + ".";
	    }

	    routeFolderName = routeFolderName + JavaResourcesHelper.getJobFolderName(process.getName(), process.getVersion());

	    String packageName = codeGenArgument.getCurrentProjectName().toLowerCase() + "." + routeFolderName;

		builder.addParam("resourceClasses", "\"" + packageName + "." + className + "$Service_" + cid + "\"");
		builderRT.addParam("resourceClasses", "\"" + packageName + "." + className + "$Service_" + cid + "\"");

    } else {
        if ("true".equals(ElementParameterParser.getValue(node, "__SERVICE_LOCATOR__"))) {
            endpointUrlRT = "\"locator://rest\"";
            endpointUrlStudio = "\"locator://rest\"";
        }
    }

    builder.addParam("features", "\"#features_" + cid + "\"");
    builderRT.addParam("features", "\"#features_" + cid + "\"");

    builder.addParam("inInterceptors", "\"#inInterceptors_" + cid + "\"");
    builderRT.addParam("inInterceptors", "\"#inInterceptors_" + cid + "\"");

    builder.addParam("outInterceptors", "\"#outInterceptors_" + cid + "\"");
    builderRT.addParam("outInterceptors", "\"#outInterceptors_" + cid + "\"");

    builder.addParam("properties", "\"#properties_" + cid + "\"");
    builderRT.addParam("properties", "\"#properties_" + cid + "\"");

    builder.addParam("cxfRsEndpointConfigurer", "\"#endpointConfigurer_" + cid + "\"");
    builderRT.addParam("cxfRsEndpointConfigurer", "\"#endpointConfigurer_" + cid + "\"");

    builder.addParam("providers", "\"#providers\"");
    builderRT.addParam("providers", "\"#providers\"");

    builder.addParam("loggingFeatureEnabled", "\"" + (String) ElementParameterParser.getValue(node, "__LOG_MESSAGES__") + "\"");
    builderRT.addParam("loggingFeatureEnabled", "\"" + (String) ElementParameterParser.getValue(node, "__LOG_MESSAGES__") + "\"");

    java.util.List<java.util.Map<String, String>> tableValues = (java.util.List<java.util.Map<String, String>>) ElementParameterParser.getObjectValue(node, "__ADVARGUMENTS__");
    for (java.util.Map<String, String> map : tableValues) {
        String argName = map.get("NAME").trim();
        String argValue = map.get("VALUE").trim();
        if(argName.startsWith("\"") && argName.endsWith("\"") && argName.length() >= 2) {
			argName = argName.substring(1, argName.length() - 1);
		}
		builder.addParam(argName, argValue);
		builderRT.addParam(argName, argValue);
    }

   	builder.setComponent("cxfrs://");
	builderRT.setComponent("cxfrs://");

	if("".equals(endpointUrlStudio) || endpointUrlStudio == null || "\"\"".equals(endpointUrlStudio)){
		builder.appendPath("\"/\"");
	}else{
		builder.appendPath(endpointUrlStudio);
	}

	if("".equals(endpointUrlRT) || endpointUrlRT == null || "\"\"".equals(endpointUrlRT)){
		builderRT.appendPath("\"/\"");
	}else{
		builderRT.appendPath(endpointUrlRT);
	}

    String uriRT = builderRT.build();

    String uriStudio = builder.build();

    String responseClass = "javax.ws.rs.core.Response.class";
	if (node.getIncomingConnections().isEmpty()) {

    stringBuffer.append(TEXT_3);
    stringBuffer.append(uriRT);
    stringBuffer.append(TEXT_4);
    stringBuffer.append(uriStudio);
    stringBuffer.append(TEXT_5);
    
	} else {

		if ("MANUAL".equals(ElementParameterParser.getValue(node, "__SERVICE_TYPE__"))) {
			String acceptType = paramsHelper.getVisibleStringParam("__ACCEPT_TYPE__");
			String responseBean = paramsHelper.getVisibleStringParam("__RESPONSE_BEAN__");
			responseClass = "*/*".equals(acceptType) ? "String.class" : "org.w3c.dom.Document.class";
			responseClass = responseBean == null || responseBean.isEmpty() ? responseClass : responseBean + ".class";
			String contentType = paramsHelper.getVisibleStringParam("__CONTENT_TYPE__");

    stringBuffer.append(TEXT_6);
    stringBuffer.append(ElementParameterParser.getValue(node, "__PATH__"));
    stringBuffer.append(TEXT_7);
    stringBuffer.append(ElementParameterParser.getValue(node, "__HTTP_METHOD__"));
    stringBuffer.append(TEXT_8);
     if (!acceptType.isEmpty()) { 
    stringBuffer.append(TEXT_9);
    stringBuffer.append(acceptType);
    stringBuffer.append(TEXT_10);
     } 
     if (!contentType.isEmpty()) { 
    stringBuffer.append(TEXT_11);
    stringBuffer.append(contentType);
    stringBuffer.append(TEXT_12);
     }
} else { // RESOURCECLASS

    stringBuffer.append(TEXT_13);
    stringBuffer.append(ElementParameterParser.getValue(node, "__RESOURCE_OPERATION__"));
    stringBuffer.append(TEXT_14);
     } 
     if (paramsHelper.getBoolParam("__ENABLE_CORRELATION__")) { 
    stringBuffer.append(TEXT_15);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_16);
    stringBuffer.append(paramsHelper.getVisibleStringParam("__CORRELATION_VALUE__"));
    stringBuffer.append(TEXT_17);
     } 
    stringBuffer.append(TEXT_18);
    stringBuffer.append(uriRT);
    stringBuffer.append(TEXT_19);
    stringBuffer.append(uriStudio);
    stringBuffer.append(TEXT_20);
    stringBuffer.append(responseClass);
    stringBuffer.append(TEXT_21);
    stringBuffer.append(responseClass);
    stringBuffer.append(TEXT_22);
    stringBuffer.append(responseClass);
    stringBuffer.append(TEXT_23);
    stringBuffer.append(responseClass);
    stringBuffer.append(TEXT_24);
    
	}

    stringBuffer.append(TEXT_25);
    return stringBuffer.toString();
  }
}
