package org.talend.designer.codegen.translators.machinelearning.classification;

import java.util.List;
import org.talend.core.model.metadata.IMetadataColumn;
import org.talend.core.model.metadata.IMetadataTable;
import org.talend.core.model.metadata.types.JavaType;
import org.talend.core.model.metadata.types.JavaTypesManager;
import org.talend.core.model.process.ElementParameterParser;
import org.talend.core.model.process.EConnectionType;
import org.talend.core.model.process.IConnection;
import org.talend.core.model.process.IConnectionCategory;
import org.talend.core.model.process.INode;
import org.talend.core.model.process.IBigDataNode;
import org.talend.designer.common.tsqlrow.TSqlRowUtil;
import org.talend.designer.common.BigDataCodeGeneratorArgument;

public class TPredictSparkstreamingcodeJava
{
  protected static String nl;
  public static synchronized TPredictSparkstreamingcodeJava create(String lineSeparator)
  {
    nl = lineSeparator;
    TPredictSparkstreamingcodeJava result = new TPredictSparkstreamingcodeJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "    ";
  protected final String TEXT_2 = " " + NL + "    \tlog.warn(\"Component does not have Date/Timestamp checkbox, will use Timestamp as default\");";
  protected final String TEXT_3 = " " + NL + "    \tSystem.err.println(\"Component does not have Date/Timestamp checkbox, will use Timestamp as default\");";
  protected final String TEXT_4 = NL + "public static class ";
  protected final String TEXT_5 = "_FromRowTo";
  protected final String TEXT_6 = " implements org.apache.spark.api.java.function.Function<org.apache.spark.sql.Row, ";
  protected final String TEXT_7 = "> {" + NL + "" + NL + "    public ";
  protected final String TEXT_8 = " call(org.apache.spark.sql.Row row) {";
  protected final String TEXT_9 = NL + "        ";
  protected final String TEXT_10 = " result = new ";
  protected final String TEXT_11 = "();" + NL + "        org.apache.spark.sql.types.StructField[] structFields = row.schema().fields();" + NL + "        for (int i = 0; i < structFields.length; i++) {" + NL + "            org.apache.avro.Schema.Field avroField = ";
  protected final String TEXT_12 = ".getClassSchema().getField(structFields[i].name());" + NL + "            if (avroField != null){" + NL + "                result.put(avroField.pos(), row.get(i));" + NL + "            }" + NL + "        }" + NL + "        return result;" + NL + "    }" + NL + "}";
  protected final String TEXT_13 = NL + NL + "\t\tpublic static class ";
  protected final String TEXT_14 = "_From";
  protected final String TEXT_15 = "To";
  protected final String TEXT_16 = " implements org.apache.spark.api.java.function.Function<";
  protected final String TEXT_17 = ", ";
  protected final String TEXT_18 = "> {" + NL + "" + NL + "\t\t\tpublic ";
  protected final String TEXT_19 = " call(";
  protected final String TEXT_20 = " input) {" + NL + "\t\t\t\t";
  protected final String TEXT_21 = " result = new ";
  protected final String TEXT_22 = "();";
  protected final String TEXT_23 = NL + "\t\t\t\t\t\tif(input.";
  protected final String TEXT_24 = " != null) {" + NL + "\t\t\t\t\t\t\tresult.";
  protected final String TEXT_25 = " = new java.sql.";
  protected final String TEXT_26 = "(input.";
  protected final String TEXT_27 = ".getTime());" + NL + "\t\t\t\t\t\t} else {" + NL + "\t\t\t\t\t\t\tresult.";
  protected final String TEXT_28 = " = null;" + NL + "\t\t\t\t\t\t}";
  protected final String TEXT_29 = NL + "\t\t\t\t\tresult.";
  protected final String TEXT_30 = " = input.";
  protected final String TEXT_31 = ";";
  protected final String TEXT_32 = NL + "\t\t\t\treturn result;" + NL + "\t\t\t}" + NL + "\t\t}";
  protected final String TEXT_33 = NL + "    ";
  protected final String TEXT_34 = NL + NL + "public static class StringIndexerInverseFunction_";
  protected final String TEXT_35 = NL + "extends scala.runtime.AbstractFunction1<org.apache.spark.sql.Row, ";
  protected final String TEXT_36 = ">implements Serializable {" + NL + "    /** Default serial version UID. */" + NL + "    private static final long serialVersionUID = 1L;" + NL + "    private final java.util.Map<Object, String> i2s = new java.util.HashMap<>();" + NL + "    private org.apache.spark.mllib.classification.NaiveBayesModel currentModel;" + NL + "    private String vectorName;" + NL + "" + NL + "    StringIndexerInverseFunction_";
  protected final String TEXT_37 = "(org.apache.spark.ml.feature.StringIndexerModel sim,String vectorName,org.apache.spark.mllib.classification.NaiveBayesModel currentModel) {" + NL + "        this.currentModel = currentModel;" + NL + "        this.vectorName = vectorName;" + NL + "        for (scala.Tuple2<String, Object> label : scala.collection.JavaConversions.asJavaIterable(" + NL + "                sim.org$apache$spark$ml$feature$StringIndexerModel$$labelToIndex())) {" + NL + "                i2s.put(label._2(), label._1());" + NL + "        }" + NL + "     }" + NL + "" + NL + "     @Override" + NL + "     public ";
  protected final String TEXT_38 = " apply(org.apache.spark.sql.Row in) {";
  protected final String TEXT_39 = NL + "         ";
  protected final String TEXT_40 = " out = new ";
  protected final String TEXT_41 = "();" + NL + "         //features Vector";
  protected final String TEXT_42 = NL + "         ";
  protected final String TEXT_43 = " temporaryVector=null;";
  protected final String TEXT_44 = NL + "               temporaryVector=(";
  protected final String TEXT_45 = ") in.get(in.fieldIndex(this.vectorName));";
  protected final String TEXT_46 = NL + "               temporaryVector=org.apache.spark.mllib.linalg.Vectors.fromML((org.apache.spark.ml.linalg.DenseVector)in.get(in.fieldIndex(this.vectorName)));";
  protected final String TEXT_47 = "    " + NL + "         //the lable(Double) from predicting on features" + NL + "         Double indexLabeled=currentModel.predict(temporaryVector);";
  protected final String TEXT_48 = NL + "                 out.";
  protected final String TEXT_49 = " = (";
  protected final String TEXT_50 = ") in.get(in.fieldIndex(\"";
  protected final String TEXT_51 = "\"));";
  protected final String TEXT_52 = NL + "                 //get the related String lable from StringIndexrModel" + NL + "                 out.";
  protected final String TEXT_53 = " = i2s.get(indexLabeled);";
  protected final String TEXT_54 = NL + "         return out;" + NL + "     }" + NL + "}" + NL + "" + NL + "public static class Classify_";
  protected final String TEXT_55 = NL + "implements org.apache.spark.api.java.function.Function<" + NL + "        org.apache.spark.api.java.JavaRDD<";
  protected final String TEXT_56 = ">," + NL + "        org.apache.spark.api.java.JavaRDD<";
  protected final String TEXT_57 = ">> {" + NL + "" + NL + "/** Default serial version UID. */" + NL + "private static final long serialVersionUID = 1L;" + NL + "" + NL + "/** Lazy instantiation of SQLContext */" + NL + "private static org.apache.spark.sql.SQLContext sqlContext = null;" + NL + "" + NL + "private final org.apache.spark.ml.PipelineModel pipelineModel;" + NL;
  protected final String TEXT_58 = NL + "    private final StringIndexerInverseFunction_";
  protected final String TEXT_59 = " inverseIndexer;" + NL + "    private final String vectorName;" + NL + "    private org.apache.spark.mllib.classification.NaiveBayesModel currentModel;";
  protected final String TEXT_60 = NL + NL + "public Classify_";
  protected final String TEXT_61 = "(org.apache.spark.ml.PipelineModel pipelineModel,String vectorName,org.apache.spark.mllib.classification.NaiveBayesModel currentModel) {" + NL + "    this.pipelineModel = pipelineModel;" + NL + "    this.vectorName=vectorName;" + NL + "    this.currentModel=currentModel;";
  protected final String TEXT_62 = NL + "        org.apache.spark.ml.feature.StringIndexerModel sim = (org.apache.spark.ml.feature.StringIndexerModel) pipelineModel.stages()[1];" + NL + "        inverseIndexer = new StringIndexerInverseFunction_";
  protected final String TEXT_63 = "(sim,vectorName,currentModel);";
  protected final String TEXT_64 = NL + "}" + NL + "" + NL + "@Override" + NL + "public org.apache.spark.api.java.JavaRDD<";
  protected final String TEXT_65 = "> call(org.apache.spark.api.java.JavaRDD<";
  protected final String TEXT_66 = "> rddIn)" + NL + "        throws Exception {" + NL + "    if (sqlContext == null) {" + NL + "        sqlContext = new org.apache.spark.sql.SQLContext(rddIn.context());" + NL + "    }" + NL + "" + NL + "    // Convert incoming RDD to DataFrame";
  protected final String TEXT_67 = NL + "    ";
  protected final String TEXT_68 = " df = sqlContext.createDataFrame(rddIn, ";
  protected final String TEXT_69 = ".class);";
  protected final String TEXT_70 = NL + "    ";
  protected final String TEXT_71 = " results = pipelineModel.transform(df);" + NL + "    org.apache.spark.rdd.RDD<";
  protected final String TEXT_72 = "> rdd =null;" + NL;
  protected final String TEXT_73 = NL + "             rdd = results.map(inverseIndexer,scala.reflect.ClassManifestFactory.fromClass(";
  protected final String TEXT_74 = ".class));";
  protected final String TEXT_75 = NL + "             rdd = results.map(inverseIndexer,org.apache.spark.sql.Encoders.bean(";
  protected final String TEXT_76 = ".class)).rdd();";
  protected final String TEXT_77 = NL + "        return org.apache.spark.api.java.JavaRDD.fromRDD(rdd, rdd.org$apache$spark$rdd$RDD$$evidence$1);";
  protected final String TEXT_78 = NL + "        // TODO";
  protected final String TEXT_79 = NL + "}" + NL + "}";
  protected final String TEXT_80 = " " + NL + "    \tlog.warn(\"Component does not have Date/Timestamp checkbox, will use Timestamp as default\");";
  protected final String TEXT_81 = " " + NL + "    \tSystem.err.println(\"Component does not have Date/Timestamp checkbox, will use Timestamp as default\");";
  protected final String TEXT_82 = NL + "public static class ";
  protected final String TEXT_83 = "_FromRowTo";
  protected final String TEXT_84 = " implements org.apache.spark.api.java.function.Function<org.apache.spark.sql.Row, ";
  protected final String TEXT_85 = "> {" + NL + "" + NL + "    public ";
  protected final String TEXT_86 = " call(org.apache.spark.sql.Row row) {";
  protected final String TEXT_87 = NL + "        ";
  protected final String TEXT_88 = " result = new ";
  protected final String TEXT_89 = "();" + NL + "        org.apache.spark.sql.types.StructField[] structFields = row.schema().fields();" + NL + "        for (int i = 0; i < structFields.length; i++) {" + NL + "            org.apache.avro.Schema.Field avroField = ";
  protected final String TEXT_90 = ".getClassSchema().getField(structFields[i].name());" + NL + "            if (avroField != null){" + NL + "                result.put(avroField.pos(), row.get(i));" + NL + "            }" + NL + "        }" + NL + "        return result;" + NL + "    }" + NL + "}";
  protected final String TEXT_91 = NL + NL + "\t\tpublic static class ";
  protected final String TEXT_92 = "_From";
  protected final String TEXT_93 = "To";
  protected final String TEXT_94 = " implements org.apache.spark.api.java.function.Function<";
  protected final String TEXT_95 = ", ";
  protected final String TEXT_96 = "> {" + NL + "" + NL + "\t\t\tpublic ";
  protected final String TEXT_97 = " call(";
  protected final String TEXT_98 = " input) {" + NL + "\t\t\t\t";
  protected final String TEXT_99 = " result = new ";
  protected final String TEXT_100 = "();";
  protected final String TEXT_101 = NL + "\t\t\t\t\t\tif(input.";
  protected final String TEXT_102 = " != null) {" + NL + "\t\t\t\t\t\t\tresult.";
  protected final String TEXT_103 = " = new java.sql.";
  protected final String TEXT_104 = "(input.";
  protected final String TEXT_105 = ".getTime());" + NL + "\t\t\t\t\t\t} else {" + NL + "\t\t\t\t\t\t\tresult.";
  protected final String TEXT_106 = " = null;" + NL + "\t\t\t\t\t\t}";
  protected final String TEXT_107 = NL + "\t\t\t\t\tresult.";
  protected final String TEXT_108 = " = input.";
  protected final String TEXT_109 = ";";
  protected final String TEXT_110 = NL + "\t\t\t\treturn result;" + NL + "\t\t\t}" + NL + "\t\t}";
  protected final String TEXT_111 = NL + NL + "public static class Predict_";
  protected final String TEXT_112 = NL + "    implements org.apache.spark.api.java.function.Function<" + NL + "            org.apache.spark.api.java.JavaRDD<";
  protected final String TEXT_113 = ">," + NL + "            org.apache.spark.api.java.JavaRDD<";
  protected final String TEXT_114 = ">> {" + NL + "" + NL + "    /** Default serial version UID. */" + NL + "    private static final long serialVersionUID = 1L;" + NL + "" + NL + "    /** Lazy instantiation of SQLContext */" + NL + "    private static org.apache.spark.sql.SQLContext sqlContext = null;" + NL + "" + NL + "    private final org.apache.spark.ml.PipelineModel pipelineModel;" + NL + "" + NL + "    public Predict_";
  protected final String TEXT_115 = "(org.apache.spark.ml.PipelineModel pipelineModel) {" + NL + "        this.pipelineModel = pipelineModel;" + NL + "    }" + NL + "" + NL + "    @Override" + NL + "    public org.apache.spark.api.java.JavaRDD<";
  protected final String TEXT_116 = "> call(org.apache.spark.api.java.JavaRDD<";
  protected final String TEXT_117 = "> rddIn)" + NL + "            throws Exception {" + NL + "        if (sqlContext == null) {" + NL + "            sqlContext = new org.apache.spark.sql.SQLContext(rddIn.context());" + NL + "        }" + NL + "" + NL + "        // Convert incoming RDD to DataFrame";
  protected final String TEXT_118 = NL + "        ";
  protected final String TEXT_119 = " df = sqlContext.createDataFrame(rddIn, ";
  protected final String TEXT_120 = ".class);";
  protected final String TEXT_121 = NL + "        ";
  protected final String TEXT_122 = " results = pipelineModel.transform(df);" + NL + "" + NL + "        // Convert DataFrame back to RDD" + NL + "        return results.toJavaRDD().map(new ";
  protected final String TEXT_123 = "_FromRowTo";
  protected final String TEXT_124 = "());" + NL + "    }" + NL + "}";
  protected final String TEXT_125 = NL + NL + "public static class StringIndexerInverseFunction_";
  protected final String TEXT_126 = NL + "        extends scala.runtime.AbstractFunction1<org.apache.spark.sql.Row, ";
  protected final String TEXT_127 = ">implements Serializable {" + NL + "    /** Default serial version UID. */" + NL + "    private static final long serialVersionUID = 1L;" + NL + "    private final java.util.Map<Object, String> i2s = new java.util.HashMap<>();" + NL + "" + NL + "    StringIndexerInverseFunction_";
  protected final String TEXT_128 = "(org.apache.spark.ml.feature.StringIndexerModel sim) {" + NL + "        for (scala.Tuple2<String, Object> label : scala.collection.JavaConversions.asJavaIterable(" + NL + "                sim.org$apache$spark$ml$feature$StringIndexerModel$$labelToIndex())) {" + NL + "            i2s.put(label._2(), label._1());" + NL + "        }" + NL + "    }" + NL + "" + NL + "    @Override" + NL + "    public ";
  protected final String TEXT_129 = " apply(org.apache.spark.sql.Row in) {";
  protected final String TEXT_130 = NL + "        ";
  protected final String TEXT_131 = " out = new ";
  protected final String TEXT_132 = "();";
  protected final String TEXT_133 = NL + "                out.";
  protected final String TEXT_134 = " = (";
  protected final String TEXT_135 = ") in.get(in.fieldIndex(\"";
  protected final String TEXT_136 = "\"));";
  protected final String TEXT_137 = NL + "                out.";
  protected final String TEXT_138 = " = i2s.get(in.get(in.fieldIndex(\"";
  protected final String TEXT_139 = "\")));";
  protected final String TEXT_140 = NL + "        return out;" + NL + "    }" + NL + "}" + NL + "" + NL + "" + NL + "public static class Classify_";
  protected final String TEXT_141 = NL + "    implements org.apache.spark.api.java.function.Function<" + NL + "            org.apache.spark.api.java.JavaRDD<";
  protected final String TEXT_142 = ">," + NL + "            org.apache.spark.api.java.JavaRDD<";
  protected final String TEXT_143 = ">> {" + NL + "" + NL + "    /** Default serial version UID. */" + NL + "    private static final long serialVersionUID = 1L;" + NL + "" + NL + "    /** Lazy instantiation of SQLContext */" + NL + "    private static org.apache.spark.sql.SQLContext sqlContext = null;" + NL + "" + NL + "    private final org.apache.spark.ml.PipelineModel pipelineModel;" + NL;
  protected final String TEXT_144 = NL + "        private final StringIndexerInverseFunction_";
  protected final String TEXT_145 = " inverseIndexer;";
  protected final String TEXT_146 = NL + NL + "    public Classify_";
  protected final String TEXT_147 = "(org.apache.spark.ml.PipelineModel pipelineModel) {" + NL + "        this.pipelineModel = pipelineModel;";
  protected final String TEXT_148 = NL + "            org.apache.spark.ml.feature.StringIndexerModel sim = (org.apache.spark.ml.feature.StringIndexerModel) pipelineModel.stages()[1];" + NL + "            inverseIndexer = new StringIndexerInverseFunction_";
  protected final String TEXT_149 = "(sim);";
  protected final String TEXT_150 = NL + "    }" + NL + "" + NL + "    @Override" + NL + "    public org.apache.spark.api.java.JavaRDD<";
  protected final String TEXT_151 = "> call(org.apache.spark.api.java.JavaRDD<";
  protected final String TEXT_152 = "> rddIn)" + NL + "            throws Exception {" + NL + "        if (sqlContext == null) {" + NL + "            sqlContext = new org.apache.spark.sql.SQLContext(rddIn.context());" + NL + "        }" + NL + "" + NL + "        // Convert incoming RDD to DataFrame";
  protected final String TEXT_153 = NL + "        ";
  protected final String TEXT_154 = " df = sqlContext.createDataFrame(rddIn, ";
  protected final String TEXT_155 = ".class);";
  protected final String TEXT_156 = NL + "        ";
  protected final String TEXT_157 = " results = pipelineModel.transform(df);" + NL;
  protected final String TEXT_158 = NL + "            org.apache.spark.rdd.RDD<";
  protected final String TEXT_159 = "> rdd = results.rdd().map(inverseIndexer," + NL + "                    scala.reflect.ClassManifestFactory.fromClass(";
  protected final String TEXT_160 = ".class));" + NL + "            return org.apache.spark.api.java.JavaRDD.fromRDD(rdd, rdd.org$apache$spark$rdd$RDD$$evidence$1);";
  protected final String TEXT_161 = NL + "            // TODO";
  protected final String TEXT_162 = NL + "    }" + NL + "}";
  protected final String TEXT_163 = NL + "public static class ";
  protected final String TEXT_164 = " extends ";
  protected final String TEXT_165 = " {" + NL + "    public ";
  protected final String TEXT_166 = " temporaryVector;" + NL + "}" + NL + "public static class GetPrediction_";
  protected final String TEXT_167 = NL + "        implements org.apache.spark.api.java.function.Function<";
  protected final String TEXT_168 = ", ";
  protected final String TEXT_169 = "> {" + NL + "" + NL + "    org.apache.spark.mllib.classification.SVMModel currentModel;" + NL + "" + NL + "    public GetPrediction_";
  protected final String TEXT_170 = "(" + NL + "            org.apache.spark.mllib.classification.SVMModel currentModel) {" + NL + "        this.currentModel = currentModel;" + NL + "    }" + NL + "" + NL + "    public ";
  protected final String TEXT_171 = " call(";
  protected final String TEXT_172 = " input) {";
  protected final String TEXT_173 = NL + "        ";
  protected final String TEXT_174 = " output = new ";
  protected final String TEXT_175 = "();";
  protected final String TEXT_176 = NL + "                output.";
  protected final String TEXT_177 = " = input.";
  protected final String TEXT_178 = ";";
  protected final String TEXT_179 = NL + "        output.label = currentModel.predict(input.temporaryVector);" + NL + "        return output;" + NL + "    }" + NL + "}" + NL + "" + NL + "public static class GetEncodedStruct_";
  protected final String TEXT_180 = " implements org.apache.spark.api.java.function.Function<org.apache.spark.sql.Row, ";
  protected final String TEXT_181 = "> {" + NL + "" + NL + "    String vectorName;" + NL + "" + NL + "    public GetEncodedStruct_";
  protected final String TEXT_182 = "(String vectorName) {" + NL + "        this.vectorName = vectorName;" + NL + "    }" + NL + "" + NL + "    public ";
  protected final String TEXT_183 = " call(org.apache.spark.sql.Row input) {";
  protected final String TEXT_184 = NL + "        ";
  protected final String TEXT_185 = " output = new ";
  protected final String TEXT_186 = "();" + NL + "        try {";
  protected final String TEXT_187 = NL + "                output.temporaryVector = (";
  protected final String TEXT_188 = ") input.get(input.fieldIndex(this.vectorName));";
  protected final String TEXT_189 = NL + "                output.temporaryVector = org.apache.spark.mllib.linalg.Vectors.fromML((org.apache.spark.ml.linalg.Vector) input.get(input.fieldIndex(this.vectorName)));";
  protected final String TEXT_190 = NL + "        } catch (java.lang.ClassCastException e) {" + NL + "            // nothing, return null" + NL + "        }";
  protected final String TEXT_191 = NL + "                output.";
  protected final String TEXT_192 = " = (";
  protected final String TEXT_193 = ") input.get(input.fieldIndex(\"";
  protected final String TEXT_194 = "\"));";
  protected final String TEXT_195 = NL + "        return output;" + NL + "    }" + NL + "}" + NL + "" + NL + "" + NL + "public static class GenerateEncodedStruct_";
  protected final String TEXT_196 = NL + "    implements org.apache.spark.api.java.function.Function<org.apache.spark.api.java.JavaRDD<";
  protected final String TEXT_197 = ">," + NL + "            org.apache.spark.api.java.JavaRDD<";
  protected final String TEXT_198 = ">> {" + NL + "" + NL + "    private String vectorName = \"\";" + NL + "    private org.apache.spark.ml.PipelineModel pipelineModel;" + NL + "    private org.apache.spark.sql.SQLContext context;" + NL + "" + NL + "    public GenerateEncodedStruct_";
  protected final String TEXT_199 = " (String vectorName, org.apache.spark.ml.PipelineModel pipelineModel, org.apache.spark.sql.SQLContext context) {" + NL + "        this.vectorName = vectorName;" + NL + "        this.pipelineModel = pipelineModel;" + NL + "        this.context = context;" + NL + "    }" + NL + "" + NL + "    @Override" + NL + "    public org.apache.spark.api.java.JavaRDD<";
  protected final String TEXT_200 = "> call(org.apache.spark.api.java.JavaRDD<";
  protected final String TEXT_201 = "> inputRDD) throws Exception {";
  protected final String TEXT_202 = NL + "        ";
  protected final String TEXT_203 = " inputDataFrame = context" + NL + "              .createDataFrame(inputRDD, ";
  protected final String TEXT_204 = ".class);";
  protected final String TEXT_205 = NL + "        ";
  protected final String TEXT_206 = " outputDataFrame = pipelineModel" + NL + "              .transform(inputDataFrame);" + NL + "" + NL + "        return outputDataFrame.toJavaRDD().map(new GetEncodedStruct_";
  protected final String TEXT_207 = "(vectorName));" + NL + "    }" + NL + "}" + NL + "" + NL + "" + NL + "" + NL + "public static class GetKeyVectorStruct_";
  protected final String TEXT_208 = NL + "        implements" + NL + "        org.apache.spark.api.java.function.PairFunction<org.apache.spark.sql.Row, ";
  protected final String TEXT_209 = ", ";
  protected final String TEXT_210 = "> {" + NL + "" + NL + "    String vectorName;" + NL + "" + NL + "    public GetKeyVectorStruct_";
  protected final String TEXT_211 = "(String vectorName) {" + NL + "        this.vectorName = vectorName;" + NL + "    }" + NL + "" + NL + "    public scala.Tuple2<";
  protected final String TEXT_212 = ", ";
  protected final String TEXT_213 = "> call(org.apache.spark.sql.Row input) {";
  protected final String TEXT_214 = NL + "        ";
  protected final String TEXT_215 = " output = new ";
  protected final String TEXT_216 = "();";
  protected final String TEXT_217 = NL + "        ";
  protected final String TEXT_218 = " outputVector  = null;" + NL + "        try {" + NL + "            outputVector = (";
  protected final String TEXT_219 = ") input" + NL + "                    .get(input.fieldIndex(this.vectorName));" + NL + "        } catch (java.lang.ClassCastException e) {" + NL + "            // nothing, return null" + NL + "        }";
  protected final String TEXT_220 = NL + "                output.";
  protected final String TEXT_221 = " = (";
  protected final String TEXT_222 = ") input.get(input.fieldIndex(\"";
  protected final String TEXT_223 = "\"));";
  protected final String TEXT_224 = NL + "        return new scala.Tuple2(output, outputVector);" + NL + "    }" + NL + "}";
  protected final String TEXT_225 = NL + "public static class ";
  protected final String TEXT_226 = " extends ";
  protected final String TEXT_227 = " {" + NL + "    public ";
  protected final String TEXT_228 = " temporaryVector;" + NL + "}" + NL + "public static class GetPrediction_";
  protected final String TEXT_229 = NL + "        implements org.apache.spark.api.java.function.Function<";
  protected final String TEXT_230 = ", ";
  protected final String TEXT_231 = "> {" + NL + "" + NL + "    org.apache.spark.mllib.clustering.KMeansModel currentModel;" + NL + "" + NL + "    public GetPrediction_";
  protected final String TEXT_232 = "(" + NL + "            org.apache.spark.mllib.clustering.KMeansModel currentModel) {" + NL + "        this.currentModel = currentModel;" + NL + "    }" + NL + "" + NL + "    public ";
  protected final String TEXT_233 = " call(";
  protected final String TEXT_234 = " input) {";
  protected final String TEXT_235 = NL + "        ";
  protected final String TEXT_236 = " output = new ";
  protected final String TEXT_237 = "();";
  protected final String TEXT_238 = NL + "                output.";
  protected final String TEXT_239 = " = input.";
  protected final String TEXT_240 = ";";
  protected final String TEXT_241 = NL + "        output.label = currentModel.predict(input.temporaryVector);" + NL + "        return output;" + NL + "    }" + NL + "}" + NL + "" + NL + "public static class GetEncodedStruct_";
  protected final String TEXT_242 = " implements org.apache.spark.api.java.function.Function<org.apache.spark.sql.Row, ";
  protected final String TEXT_243 = "> {" + NL + "" + NL + "    String vectorName;" + NL + "" + NL + "    public GetEncodedStruct_";
  protected final String TEXT_244 = "(String vectorName) {" + NL + "        this.vectorName = vectorName;" + NL + "    }" + NL + "" + NL + "    public ";
  protected final String TEXT_245 = " call(org.apache.spark.sql.Row input) {";
  protected final String TEXT_246 = NL + "        ";
  protected final String TEXT_247 = " output = new ";
  protected final String TEXT_248 = "();" + NL + "        try {" + NL + "        \t";
  protected final String TEXT_249 = NL + "        \t\toutput.temporaryVector = (";
  protected final String TEXT_250 = ") input.get(input.fieldIndex(this.vectorName));" + NL + "\t    \t";
  protected final String TEXT_251 = NL + "\t    \t\toutput.temporaryVector = org.apache.spark.mllib.linalg.Vectors.fromML((org.apache.spark.ml.linalg.Vector) input.get(input.fieldIndex(this.vectorName)));" + NL + "\t    \t";
  protected final String TEXT_252 = NL + "        } catch (java.lang.ClassCastException e) {" + NL + "            // nothing, return null" + NL + "        }";
  protected final String TEXT_253 = NL + "                output.";
  protected final String TEXT_254 = " = (";
  protected final String TEXT_255 = ") input.get(input.fieldIndex(\"";
  protected final String TEXT_256 = "\"));";
  protected final String TEXT_257 = NL + "        return output;" + NL + "    }" + NL + "}";
  protected final String TEXT_258 = NL + NL + "public static class GenerateVector_";
  protected final String TEXT_259 = NL + "    implements org.apache.spark.api.java.function.Function<org.apache.spark.api.java.JavaRDD<";
  protected final String TEXT_260 = ">," + NL + "            org.apache.spark.api.java.JavaRDD<";
  protected final String TEXT_261 = ">> {" + NL + "" + NL + "    private String vectorName = \"\";" + NL + "    private org.apache.spark.ml.PipelineModel pipelineModel;" + NL + "    private org.apache.spark.sql.SQLContext context;" + NL + "" + NL + "    public GenerateVector_";
  protected final String TEXT_262 = " (String vectorName, org.apache.spark.ml.PipelineModel pipelineModel, org.apache.spark.sql.SQLContext context) {" + NL + "        this.vectorName = vectorName;" + NL + "        this.pipelineModel = pipelineModel;" + NL + "        this.context = context;" + NL + "    }" + NL + "" + NL + "    @Override" + NL + "    public org.apache.spark.api.java.JavaRDD<";
  protected final String TEXT_263 = "> call(org.apache.spark.api.java.JavaRDD<";
  protected final String TEXT_264 = "> inputRDD) throws Exception {";
  protected final String TEXT_265 = NL + "        ";
  protected final String TEXT_266 = " inputDataFrame = context" + NL + "              .createDataFrame(inputRDD, ";
  protected final String TEXT_267 = ".class);";
  protected final String TEXT_268 = NL + "        ";
  protected final String TEXT_269 = " outputDataFrame = pipelineModel" + NL + "              .transform(inputDataFrame);" + NL + "        return outputDataFrame.toJavaRDD().map(new GetEncodedStruct_";
  protected final String TEXT_270 = "(vectorName));" + NL + "    }" + NL + "}" + NL + "" + NL + "" + NL + "" + NL + "public static class GetKeyVectorStruct_";
  protected final String TEXT_271 = NL + "        implements" + NL + "        org.apache.spark.api.java.function.PairFunction<org.apache.spark.sql.Row, ";
  protected final String TEXT_272 = ", ";
  protected final String TEXT_273 = "> {" + NL + "" + NL + "    String vectorName;" + NL + "" + NL + "    public GetKeyVectorStruct_";
  protected final String TEXT_274 = "(String vectorName) {" + NL + "        this.vectorName = vectorName;" + NL + "    }" + NL + "" + NL + "    public scala.Tuple2<";
  protected final String TEXT_275 = ", ";
  protected final String TEXT_276 = "> call(org.apache.spark.sql.Row input) {";
  protected final String TEXT_277 = NL + "        ";
  protected final String TEXT_278 = " output = new ";
  protected final String TEXT_279 = "();";
  protected final String TEXT_280 = NL + "        ";
  protected final String TEXT_281 = " outputVector  = null;" + NL + "        " + NL + "        try {" + NL + "        \t";
  protected final String TEXT_282 = NL + "        \t\toutputVector = (";
  protected final String TEXT_283 = ") input.get(input.fieldIndex(this.vectorName));" + NL + "        \t";
  protected final String TEXT_284 = NL + "        \t\toutputVector = org.apache.spark.mllib.linalg.Vectors.fromML((org.apache.spark.ml.linalg.Vector) input.get(input.fieldIndex(this.vectorName)));" + NL + "        \t";
  protected final String TEXT_285 = NL + "        } catch (java.lang.ClassCastException e) {" + NL + "            System.out.println(e.getMessage());" + NL + "        }";
  protected final String TEXT_286 = NL + "                output.";
  protected final String TEXT_287 = " = (";
  protected final String TEXT_288 = ") input.get(input.fieldIndex(\"";
  protected final String TEXT_289 = "\"));";
  protected final String TEXT_290 = NL + "        return new scala.Tuple2(output, outputVector);" + NL + "    }" + NL + "}" + NL + NL;
  protected final String TEXT_291 = NL + "    public static class GenerateKeyVector_";
  protected final String TEXT_292 = NL + "        implements org.apache.spark.api.java.function.Function<org.apache.spark.api.java.JavaRDD<";
  protected final String TEXT_293 = ">," + NL + "                    org.apache.spark.api.java.JavaPairRDD<";
  protected final String TEXT_294 = ", ";
  protected final String TEXT_295 = ">> {" + NL + "" + NL + "        private String vectorName = \"\";" + NL + "        private org.apache.spark.ml.PipelineModel pipelineModel;" + NL + "        private org.apache.spark.sql.SQLContext context;" + NL + "" + NL + "        public GenerateKeyVector_";
  protected final String TEXT_296 = " (String vectorName, org.apache.spark.ml.PipelineModel pipelineModel, org.apache.spark.sql.SQLContext context) {" + NL + "            this.vectorName = vectorName;" + NL + "            this.pipelineModel = pipelineModel;" + NL + "            this.context = context;" + NL + "        }" + NL + "" + NL + "        @Override" + NL + "        public org.apache.spark.api.java.JavaPairRDD<";
  protected final String TEXT_297 = ", ";
  protected final String TEXT_298 = "> call(org.apache.spark.api.java.JavaRDD<";
  protected final String TEXT_299 = "> inputRDD) throws Exception {";
  protected final String TEXT_300 = NL + "            ";
  protected final String TEXT_301 = " inputDataFrame = context" + NL + "                  .createDataFrame(inputRDD, ";
  protected final String TEXT_302 = ".class);";
  protected final String TEXT_303 = NL + "            ";
  protected final String TEXT_304 = " outputDataFrame = pipelineModel" + NL + "                .transform(inputDataFrame);" + NL + "            return outputDataFrame.toJavaRDD().mapToPair(new GetKeyVectorStruct_";
  protected final String TEXT_305 = "(vectorName));" + NL + "        }" + NL + "    }";
  protected final String TEXT_306 = NL + "    public static class GenerateKeyVector_";
  protected final String TEXT_307 = NL + "        implements org.apache.spark.api.java.function.Function<org.apache.spark.api.java.JavaRDD<";
  protected final String TEXT_308 = ">," + NL + "                    org.apache.spark.api.java.JavaPairRDD<";
  protected final String TEXT_309 = ", ";
  protected final String TEXT_310 = ">> {" + NL + "" + NL + "        private String vectorName = \"\";" + NL + "        private org.apache.spark.ml.Pipeline pipeline;" + NL + "        private org.apache.spark.sql.SQLContext context;" + NL + "" + NL + "        public GenerateKeyVector_";
  protected final String TEXT_311 = " (String vectorName, org.apache.spark.ml.Pipeline pipeline, org.apache.spark.sql.SQLContext context) {" + NL + "            this.vectorName = vectorName;" + NL + "            this.pipeline = pipeline;" + NL + "            this.context = context;" + NL + "        }" + NL + "" + NL + "        @Override" + NL + "        public org.apache.spark.api.java.JavaPairRDD<";
  protected final String TEXT_312 = ", ";
  protected final String TEXT_313 = "> call(org.apache.spark.api.java.JavaRDD<";
  protected final String TEXT_314 = "> inputRDD) throws Exception {";
  protected final String TEXT_315 = NL + "            ";
  protected final String TEXT_316 = " inputDataFrame = context" + NL + "                  .createDataFrame(inputRDD, ";
  protected final String TEXT_317 = ".class);";
  protected final String TEXT_318 = NL + "            ";
  protected final String TEXT_319 = " outputDataFrame = pipeline" + NL + "                .fit(inputDataFrame)" + NL + "                .transform(inputDataFrame);" + NL + "            return outputDataFrame.toJavaRDD().mapToPair(new GetKeyVectorStruct_";
  protected final String TEXT_320 = "(vectorName));" + NL + "        }" + NL + "    }";
  protected final String TEXT_321 = NL + NL + "public static class GetStreamingPrediction_";
  protected final String TEXT_322 = NL + "        implements org.apache.spark.api.java.function.Function<scala.Tuple2<";
  protected final String TEXT_323 = ", Integer>, ";
  protected final String TEXT_324 = "> {" + NL + "" + NL + "    public ";
  protected final String TEXT_325 = " call(scala.Tuple2<";
  protected final String TEXT_326 = ", Integer> inputTuple) {";
  protected final String TEXT_327 = NL + "        ";
  protected final String TEXT_328 = " input = inputTuple._1();" + NL + "        Integer label = inputTuple._2();";
  protected final String TEXT_329 = NL + "        ";
  protected final String TEXT_330 = " output = new ";
  protected final String TEXT_331 = "();";
  protected final String TEXT_332 = NL + "                output.";
  protected final String TEXT_333 = " = input.";
  protected final String TEXT_334 = ";";
  protected final String TEXT_335 = NL + "        output.label = label;" + NL + "        return output;" + NL + "    }" + NL + "}";

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    
final String modelType = ElementParameterParser.getValue((INode) ((BigDataCodeGeneratorArgument) argument).getArgument(), "__MODEL_TYPE__");
final String sparkVersion=ElementParameterParser.getValue((INode) ((BigDataCodeGeneratorArgument) argument).getArgument(), "__NVB_VERSION__");

// NAIVEBAYES sparkcode
if("NAIVEBAYES".equals(modelType)){
    if(sparkVersion.equals("SPARK_VERSION_1.3")){
    stringBuffer.append(TEXT_1);
    
// Parse the inputs to this javajet generator.
final BigDataCodeGeneratorArgument codeGenArgument = (BigDataCodeGeneratorArgument)argument;
final INode node = (INode)codeGenArgument.getArgument();
final String cid = node.getUniqueName();

final String dataframeClass = org.talend.hadoop.distribution.ESparkVersion.SPARK_2_0.compareTo(codeGenArgument.getSparkVersion()) > 0
    ? "org.apache.spark.sql.DataFrame"
    : "org.apache.spark.sql.Dataset<org.apache.spark.sql.Row>";

    
TSqlRowUtil tSqlRowUtil = new TSqlRowUtil(node);
String validateError = tSqlRowUtil.validate(true, true);
if (validateError != null) {
    // Cause the job compilation to explicitly fail if there is a problem.
    return "throw new JobConfigurationError(\"" + validateError +"\");";
}

boolean isLog4jEnabled = ("true").equals(ElementParameterParser.getValue(node.getProcess(), "__LOG4J_ACTIVATE__"));

boolean useTimestampForDatesInDataframes = true;
try{
    useTimestampForDatesInDataframes = ElementParameterParser.getBooleanValue(node, "__DATE_TO_TIMESTAMP_DF_TYPE_SUBSTITUTION__");
} catch(Exception e){
    if (isLog4jEnabled) { 
    
    stringBuffer.append(TEXT_2);
     
    } else { 
    
    stringBuffer.append(TEXT_3);
     
    } 
}

String outStructName = codeGenArgument.getRecordStructName(tSqlRowUtil.getOutgoingConnection());

    stringBuffer.append(TEXT_4);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_5);
    stringBuffer.append(outStructName);
    stringBuffer.append(TEXT_6);
    stringBuffer.append(outStructName);
    stringBuffer.append(TEXT_7);
    stringBuffer.append(outStructName);
    stringBuffer.append(TEXT_8);
    stringBuffer.append(TEXT_9);
    stringBuffer.append(outStructName);
    stringBuffer.append(TEXT_10);
    stringBuffer.append(outStructName);
    stringBuffer.append(TEXT_11);
    stringBuffer.append(outStructName);
    stringBuffer.append(TEXT_12);
    
// If the incoming rowStruct contains a Date field (always typed as java.util.Date),
// we must generate a new structure which replaces these java.util.Date instances by
// java.sql.Date or java.sql.Timestamp instances.

org.talend.designer.bigdata.avro.AvroRecordStructGenerator avroRecordStructGenerator = (org.talend.designer.bigdata.avro.AvroRecordStructGenerator) codeGenArgument.getRecordStructGenerator();

// Some of the incoming connections might share the same schema (and then the same rowXStruct). We must generate the below code only once by schema (if necessary).
java.util.Set<String> knownStructNames = new java.util.HashSet();

for(IConnection incomingConnection : tSqlRowUtil.getIncomingConnections()) {
	String originalStructName = codeGenArgument.getRecordStructName(incomingConnection);
	if(tSqlRowUtil.containsDateFields(incomingConnection) && !knownStructNames.contains(originalStructName)) {
		java.util.List<IMetadataColumn> columns = tSqlRowUtil.getColumns(incomingConnection);
		String suggestedDfStructName = "DF_"+originalStructName;
		String dfStructName = avroRecordStructGenerator.generateRecordStructForDataFrame(suggestedDfStructName, originalStructName, useTimestampForDatesInDataframes);
		knownStructNames.add(originalStructName);

    stringBuffer.append(TEXT_13);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_14);
    stringBuffer.append(originalStructName);
    stringBuffer.append(TEXT_15);
    stringBuffer.append(dfStructName);
    stringBuffer.append(TEXT_16);
    stringBuffer.append(originalStructName);
    stringBuffer.append(TEXT_17);
    stringBuffer.append(dfStructName);
    stringBuffer.append(TEXT_18);
    stringBuffer.append(dfStructName);
    stringBuffer.append(TEXT_19);
    stringBuffer.append(originalStructName);
    stringBuffer.append(TEXT_20);
    stringBuffer.append(dfStructName);
    stringBuffer.append(TEXT_21);
    stringBuffer.append(dfStructName);
    stringBuffer.append(TEXT_22);
    
				for(IMetadataColumn column : columns) {
					if(tSqlRowUtil.isDateField(column)) {

    stringBuffer.append(TEXT_23);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_24);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_25);
    stringBuffer.append(useTimestampForDatesInDataframes ? "Timestamp" : "Date");
    stringBuffer.append(TEXT_26);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_27);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_28);
    
				} else {

    stringBuffer.append(TEXT_29);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_30);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_31);
    
				}
			} // end for(IMetadataColumn column : columns)

    stringBuffer.append(TEXT_32);
    
	} // end if(tSqlRowUtil.containsDateFields(incomingConnection) && !knownStructNames.contains(originalStructName))
} // end for(IConnection incomingConnection : tSqlRowUtil.getIncomingConnections())

    

    stringBuffer.append(TEXT_33);
      }else{
    
//Parse the inputs to this javajet generator.
final BigDataCodeGeneratorArgument codeGenArgument = (BigDataCodeGeneratorArgument) argument;
final INode node = (INode)codeGenArgument.getArgument();
final String cid = node.getUniqueName();
final boolean isLog4jEnabled = ("true").equals(ElementParameterParser.getValue(node.getProcess(), "__LOG4J_ACTIVATE__"));

final String dataframeClass = org.talend.hadoop.distribution.ESparkVersion.SPARK_2_0.compareTo(codeGenArgument.getSparkVersion()) > 0
    ? "org.apache.spark.sql.DataFrame"
    : "org.apache.spark.sql.Dataset<org.apache.spark.sql.Row>";
final String vectorClass ="org.apache.spark.mllib.linalg.Vector";

    
IMetadataTable metadata = null;
IConnection conn = null;
List<IMetadataColumn> columns = null;
List<IMetadataTable> metadatas = node.getMetadataList();
if((metadatas!=null) && (metadatas.size() > 0)){
    metadata = metadatas.get(0);
    if(metadata != null){
        columns = metadata.getListColumns();
    }
}
List<? extends IConnection> conns = node.getIncomingConnections();
if(conns != null && conns.size() > 0 && conns.get(0).getLineStyle().hasConnectionCategory(IConnectionCategory.DATA)){
    conn = conns.get(0);
}

List<? extends IConnection> outConns = node.getOutgoingConnections();
IConnection outConn = null;
if(outConns != null && outConns.size() > 0 && outConns.get(0).getLineStyle().hasConnectionCategory(IConnectionCategory.DATA)){
    outConn = outConns.get(0);
}

if(columns == null || columns.isEmpty() || conn == null || outConn == null){
    return "";
}

//inRowStruct is used by straming tPredict javajet
String inRowStruct = codeGenArgument.getRecordStructName(conn);
String outRowStruct = codeGenArgument.getRecordStructName(outConn);

//This is set to true if the output label is not double format.
//TODO: check if the labelColumn is double and provide a unique labelColumn.
boolean needsLabelIndexer = true;
String labelColumn = "LABEL";
final boolean isSpark1 = org.talend.hadoop.distribution.ESparkVersion.SPARK_2_0.compareTo(codeGenArgument.getSparkVersion()) > 0;

    stringBuffer.append(TEXT_34);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_35);
    stringBuffer.append(outRowStruct);
    stringBuffer.append(TEXT_36);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_37);
    stringBuffer.append(outRowStruct);
    stringBuffer.append(TEXT_38);
    stringBuffer.append(TEXT_39);
    stringBuffer.append(outRowStruct);
    stringBuffer.append(TEXT_40);
    stringBuffer.append(outRowStruct);
    stringBuffer.append(TEXT_41);
    stringBuffer.append(TEXT_42);
    stringBuffer.append(vectorClass);
    stringBuffer.append(TEXT_43);
    
         if (isSpark1) {
             
    stringBuffer.append(TEXT_44);
    stringBuffer.append(vectorClass);
    stringBuffer.append(TEXT_45);
    
         } else {
             
    stringBuffer.append(TEXT_46);
    }
    stringBuffer.append(TEXT_47);
    
         for (IMetadataColumn column: columns) {
             
              if (!labelColumn.equals(column.getLabel())) {

    stringBuffer.append(TEXT_48);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_49);
    stringBuffer.append(JavaTypesManager.getTypeToGenerate(column.getTalendType(),
                        column.isNullable()));
    stringBuffer.append(TEXT_50);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_51);
    
              } else {

    stringBuffer.append(TEXT_52);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_53);
    
              }
         }

    stringBuffer.append(TEXT_54);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_55);
    stringBuffer.append(inRowStruct);
    stringBuffer.append(TEXT_56);
    stringBuffer.append(outRowStruct);
    stringBuffer.append(TEXT_57);
    
if (needsLabelIndexer) {
    
    stringBuffer.append(TEXT_58);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_59);
    
}

    stringBuffer.append(TEXT_60);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_61);
    
    if (needsLabelIndexer) {
        
    stringBuffer.append(TEXT_62);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_63);
    
    }
    
    stringBuffer.append(TEXT_64);
    stringBuffer.append(outRowStruct);
    stringBuffer.append(TEXT_65);
    stringBuffer.append(inRowStruct);
    stringBuffer.append(TEXT_66);
    stringBuffer.append(TEXT_67);
    stringBuffer.append(dataframeClass);
    stringBuffer.append(TEXT_68);
    stringBuffer.append(inRowStruct);
    stringBuffer.append(TEXT_69);
    stringBuffer.append(TEXT_70);
    stringBuffer.append(dataframeClass);
    stringBuffer.append(TEXT_71);
    stringBuffer.append(outRowStruct);
    stringBuffer.append(TEXT_72);
    
    if (needsLabelIndexer) {
        if (org.talend.hadoop.distribution.ESparkVersion.SPARK_2_0.compareTo(codeGenArgument.getSparkVersion()) > 0) {//spark1
        
    stringBuffer.append(TEXT_73);
    stringBuffer.append(outRowStruct);
    stringBuffer.append(TEXT_74);
    
        }else{
    stringBuffer.append(TEXT_75);
    stringBuffer.append(outRowStruct);
    stringBuffer.append(TEXT_76);
        
        }
        
    stringBuffer.append(TEXT_77);
    
    } else {
        
    stringBuffer.append(TEXT_78);
    
    }
    
    stringBuffer.append(TEXT_79);
    
    }

}  // LINEAR_REGRESSION sparkcode 
else if("LINEAR_REGRESSION".equals(modelType)){

    
// Parse the inputs to this javajet generator.
final BigDataCodeGeneratorArgument codeGenArgument = (BigDataCodeGeneratorArgument)argument;
final INode node = (INode)codeGenArgument.getArgument();
final String cid = node.getUniqueName();

final String dataframeClass = org.talend.hadoop.distribution.ESparkVersion.SPARK_2_0.compareTo(codeGenArgument.getSparkVersion()) > 0
    ? "org.apache.spark.sql.DataFrame"
    : "org.apache.spark.sql.Dataset<org.apache.spark.sql.Row>";

    
TSqlRowUtil tSqlRowUtil = new TSqlRowUtil(node);
String validateError = tSqlRowUtil.validate(true, true);
if (validateError != null) {
    // Cause the job compilation to explicitly fail if there is a problem.
    return "throw new JobConfigurationError(\"" + validateError +"\");";
}

boolean isLog4jEnabled = ("true").equals(ElementParameterParser.getValue(node.getProcess(), "__LOG4J_ACTIVATE__"));

boolean useTimestampForDatesInDataframes = true;
try{
    useTimestampForDatesInDataframes = ElementParameterParser.getBooleanValue(node, "__DATE_TO_TIMESTAMP_DF_TYPE_SUBSTITUTION__");
} catch(Exception e){
    if (isLog4jEnabled) { 
    
    stringBuffer.append(TEXT_80);
     
    } else { 
    
    stringBuffer.append(TEXT_81);
     
    } 
}

String outStructName = codeGenArgument.getRecordStructName(tSqlRowUtil.getOutgoingConnection());

    stringBuffer.append(TEXT_82);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_83);
    stringBuffer.append(outStructName);
    stringBuffer.append(TEXT_84);
    stringBuffer.append(outStructName);
    stringBuffer.append(TEXT_85);
    stringBuffer.append(outStructName);
    stringBuffer.append(TEXT_86);
    stringBuffer.append(TEXT_87);
    stringBuffer.append(outStructName);
    stringBuffer.append(TEXT_88);
    stringBuffer.append(outStructName);
    stringBuffer.append(TEXT_89);
    stringBuffer.append(outStructName);
    stringBuffer.append(TEXT_90);
    
// If the incoming rowStruct contains a Date field (always typed as java.util.Date),
// we must generate a new structure which replaces these java.util.Date instances by
// java.sql.Date or java.sql.Timestamp instances.

org.talend.designer.bigdata.avro.AvroRecordStructGenerator avroRecordStructGenerator = (org.talend.designer.bigdata.avro.AvroRecordStructGenerator) codeGenArgument.getRecordStructGenerator();

// Some of the incoming connections might share the same schema (and then the same rowXStruct). We must generate the below code only once by schema (if necessary).
java.util.Set<String> knownStructNames = new java.util.HashSet();

for(IConnection incomingConnection : tSqlRowUtil.getIncomingConnections()) {
	String originalStructName = codeGenArgument.getRecordStructName(incomingConnection);
	if(tSqlRowUtil.containsDateFields(incomingConnection) && !knownStructNames.contains(originalStructName)) {
		java.util.List<IMetadataColumn> columns = tSqlRowUtil.getColumns(incomingConnection);
		String suggestedDfStructName = "DF_"+originalStructName;
		String dfStructName = avroRecordStructGenerator.generateRecordStructForDataFrame(suggestedDfStructName, originalStructName, useTimestampForDatesInDataframes);
		knownStructNames.add(originalStructName);

    stringBuffer.append(TEXT_91);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_92);
    stringBuffer.append(originalStructName);
    stringBuffer.append(TEXT_93);
    stringBuffer.append(dfStructName);
    stringBuffer.append(TEXT_94);
    stringBuffer.append(originalStructName);
    stringBuffer.append(TEXT_95);
    stringBuffer.append(dfStructName);
    stringBuffer.append(TEXT_96);
    stringBuffer.append(dfStructName);
    stringBuffer.append(TEXT_97);
    stringBuffer.append(originalStructName);
    stringBuffer.append(TEXT_98);
    stringBuffer.append(dfStructName);
    stringBuffer.append(TEXT_99);
    stringBuffer.append(dfStructName);
    stringBuffer.append(TEXT_100);
    
				for(IMetadataColumn column : columns) {
					if(tSqlRowUtil.isDateField(column)) {

    stringBuffer.append(TEXT_101);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_102);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_103);
    stringBuffer.append(useTimestampForDatesInDataframes ? "Timestamp" : "Date");
    stringBuffer.append(TEXT_104);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_105);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_106);
    
				} else {

    stringBuffer.append(TEXT_107);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_108);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_109);
    
				}
			} // end for(IMetadataColumn column : columns)

    stringBuffer.append(TEXT_110);
    
	} // end if(tSqlRowUtil.containsDateFields(incomingConnection) && !knownStructNames.contains(originalStructName))
} // end for(IConnection incomingConnection : tSqlRowUtil.getIncomingConnections())

    
boolean isLog4jEnabled = ("true").equals(ElementParameterParser.getValue(node.getProcess(), "__LOG4J_ACTIVATE__"));

    
List<IMetadataTable> metadatas = node.getMetadataList();
if ((metadatas == null) || (metadatas.size() == 0))
    return "" ;
IMetadataTable metadata = metadatas.get(0);
if (metadata == null)
    return "";
List<IMetadataColumn> columns = metadata.getListColumns();
if (columns == null)
    return "";
List<? extends IConnection> conns = node.getIncomingConnections();
if (conns == null || conns.size() == 0 || !conns.get(0).getLineStyle().hasConnectionCategory(IConnectionCategory.DATA))
    return "";
IConnection inConn = conns.get(0);
conns = node.getOutgoingConnections();
if (conns == null || conns.size() == 0 || !conns.get(0).getLineStyle().hasConnectionCategory(IConnectionCategory.DATA))
    return "";
IConnection outConn = conns.get(0);

String inRowStruct = codeGenArgument.getRecordStructName(inConn);
String inConnName = inConn.getName();

String outRowStruct = codeGenArgument.getRecordStructName(outConn);
String outConnName = outConn.getName();

    stringBuffer.append(TEXT_111);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_112);
    stringBuffer.append(inRowStruct);
    stringBuffer.append(TEXT_113);
    stringBuffer.append(outRowStruct);
    stringBuffer.append(TEXT_114);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_115);
    stringBuffer.append(outRowStruct);
    stringBuffer.append(TEXT_116);
    stringBuffer.append(inRowStruct);
    stringBuffer.append(TEXT_117);
    stringBuffer.append(TEXT_118);
    stringBuffer.append(dataframeClass);
    stringBuffer.append(TEXT_119);
    stringBuffer.append(inRowStruct);
    stringBuffer.append(TEXT_120);
    stringBuffer.append(TEXT_121);
    stringBuffer.append(dataframeClass);
    stringBuffer.append(TEXT_122);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_123);
    stringBuffer.append(outRowStruct);
    stringBuffer.append(TEXT_124);
    
}// ML_CLASSIFICATION (LOGISTIC_REGRESSION || RANDOM_FOREST || DECISION_TREE || GRADIENT_BOOSTED) sparkcode 
else if(("LOGISTIC_REGRESSION".equals(modelType)) || ("RANDOM_FOREST".equals(modelType)) || ("DECISION_TREE".equals(modelType)) || ("GRADIENT_BOOSTED".equals(modelType))){

    
//Parse the inputs to this javajet generator.
final BigDataCodeGeneratorArgument codeGenArgument = (BigDataCodeGeneratorArgument) argument;
final INode node = (INode)codeGenArgument.getArgument();
final String cid = node.getUniqueName();
final boolean isLog4jEnabled = ("true").equals(ElementParameterParser.getValue(node.getProcess(), "__LOG4J_ACTIVATE__"));

final String dataframeClass = org.talend.hadoop.distribution.ESparkVersion.SPARK_2_0.compareTo(codeGenArgument.getSparkVersion()) > 0
    ? "org.apache.spark.sql.DataFrame"
    : "org.apache.spark.sql.Dataset<org.apache.spark.sql.Row>";

    
List<IMetadataTable> metadatas = node.getMetadataList();
if ((metadatas == null) || (metadatas.size() == 0))
    return "" ;
IMetadataTable metadata = metadatas.get(0);
if (metadata == null)
    return "";
List<IMetadataColumn> columns = metadata.getListColumns();
if (columns == null)
    return "";
List<? extends IConnection> conns = node.getIncomingConnections();
if (conns == null || conns.size() == 0 || !conns.get(0).getLineStyle().hasConnectionCategory(IConnectionCategory.DATA))
    return "";
IConnection inConn = conns.get(0);
conns = node.getOutgoingConnections();
if (conns == null || conns.size() == 0 || !conns.get(0).getLineStyle().hasConnectionCategory(IConnectionCategory.DATA))
    return "";
IConnection outConn = conns.get(0);

String inRowStruct = codeGenArgument.getRecordStructName(inConn);
String inConnName = inConn.getName();

String outRowStruct = codeGenArgument.getRecordStructName(outConn);
String outConnName = outConn.getName();

// This is set to true if the output label is not double format.
// TODO: check if the labelColumn is double and provide a unique labelColumn.
boolean needsLabelIndexer = true;
String inputLabelColumn = "unique82464359435"; // TO READ FROM MODEL!
String labelColumn = "label";

    stringBuffer.append(TEXT_125);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_126);
    stringBuffer.append(outRowStruct);
    stringBuffer.append(TEXT_127);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_128);
    stringBuffer.append(outRowStruct);
    stringBuffer.append(TEXT_129);
    stringBuffer.append(TEXT_130);
    stringBuffer.append(outRowStruct);
    stringBuffer.append(TEXT_131);
    stringBuffer.append(outRowStruct);
    stringBuffer.append(TEXT_132);
    
        for (IMetadataColumn column: columns) {
            if (!labelColumn.equals(column.getLabel())) {
                
    stringBuffer.append(TEXT_133);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_134);
    stringBuffer.append(JavaTypesManager.getTypeToGenerate(column.getTalendType(),
                                column.isNullable()));
    stringBuffer.append(TEXT_135);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_136);
    
            } else {
                
    stringBuffer.append(TEXT_137);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_138);
    stringBuffer.append(inputLabelColumn);
    stringBuffer.append(TEXT_139);
    
            }
        }
        
    stringBuffer.append(TEXT_140);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_141);
    stringBuffer.append(inRowStruct);
    stringBuffer.append(TEXT_142);
    stringBuffer.append(outRowStruct);
    stringBuffer.append(TEXT_143);
    
    if (needsLabelIndexer) {
        
    stringBuffer.append(TEXT_144);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_145);
    
    }
    
    stringBuffer.append(TEXT_146);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_147);
    
        if (needsLabelIndexer) {
            
    stringBuffer.append(TEXT_148);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_149);
    
        }
        
    stringBuffer.append(TEXT_150);
    stringBuffer.append(outRowStruct);
    stringBuffer.append(TEXT_151);
    stringBuffer.append(inRowStruct);
    stringBuffer.append(TEXT_152);
    stringBuffer.append(TEXT_153);
    stringBuffer.append(dataframeClass);
    stringBuffer.append(TEXT_154);
    stringBuffer.append(inRowStruct);
    stringBuffer.append(TEXT_155);
    stringBuffer.append(TEXT_156);
    stringBuffer.append(dataframeClass);
    stringBuffer.append(TEXT_157);
    
        if (needsLabelIndexer) {
            
    stringBuffer.append(TEXT_158);
    stringBuffer.append(outRowStruct);
    stringBuffer.append(TEXT_159);
    stringBuffer.append(outRowStruct);
    stringBuffer.append(TEXT_160);
    
        } else {
            
    stringBuffer.append(TEXT_161);
    
        }
        
    stringBuffer.append(TEXT_162);
    
}// SVM_CLASSIFICATION sparkcode 
else if("SVM_CLASSIFICATION".equals(modelType)){

    
//Parse the inputs to this javajet generator.
final BigDataCodeGeneratorArgument codeGenArgument = (BigDataCodeGeneratorArgument) argument;
final INode node = (INode)codeGenArgument.getArgument();
final String cid = node.getUniqueName();
final boolean isLog4jEnabled = ("true").equals(ElementParameterParser.getValue(node.getProcess(), "__LOG4J_ACTIVATE__"));

final String dataframeClass = org.talend.hadoop.distribution.ESparkVersion.SPARK_2_0.compareTo(codeGenArgument.getSparkVersion()) > 0
    ? "org.apache.spark.sql.DataFrame"
    : "org.apache.spark.sql.Dataset<org.apache.spark.sql.Row>";
final String vectorClass = "org.apache.spark.mllib.linalg.Vector";

    
IMetadataTable metadata = null;
IConnection conn = null;
List<IMetadataColumn> columns = null;
List<IMetadataTable> metadatas = node.getMetadataList();
if((metadatas!=null) && (metadatas.size() > 0)){
    metadata = metadatas.get(0);
    if(metadata != null){
        columns = metadata.getListColumns();
    }
}
List<? extends IConnection> conns = node.getIncomingConnections();
if(conns != null && conns.size() > 0 && conns.get(0).getLineStyle().hasConnectionCategory(IConnectionCategory.DATA)){
    conn = conns.get(0);
}

List<? extends IConnection> outConns = node.getOutgoingConnections();
IConnection outConn = null;
if(outConns != null && outConns.size() > 0 && outConns.get(0).getLineStyle().hasConnectionCategory(IConnectionCategory.DATA)){
    outConn = outConns.get(0);
}

if(columns == null || columns.isEmpty() || conn == null || outConn == null){
    return "";
}

String inRowStruct = codeGenArgument.getRecordStructName(conn);
String inRowStructEncoded = conn.getName() + "Encoded";
String connName = conn.getName();

String outRowStruct = codeGenArgument.getRecordStructName(outConn);
String outConnName = outConn.getName();


    stringBuffer.append(TEXT_163);
    stringBuffer.append(inRowStructEncoded);
    stringBuffer.append(TEXT_164);
    stringBuffer.append(inRowStruct);
    stringBuffer.append(TEXT_165);
    stringBuffer.append(vectorClass);
    stringBuffer.append(TEXT_166);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_167);
    stringBuffer.append(inRowStructEncoded);
    stringBuffer.append(TEXT_168);
    stringBuffer.append(outRowStruct);
    stringBuffer.append(TEXT_169);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_170);
    stringBuffer.append(outRowStruct);
    stringBuffer.append(TEXT_171);
    stringBuffer.append(inRowStructEncoded);
    stringBuffer.append(TEXT_172);
    stringBuffer.append(TEXT_173);
    stringBuffer.append(outRowStruct);
    stringBuffer.append(TEXT_174);
    stringBuffer.append(outRowStruct);
    stringBuffer.append(TEXT_175);
    
        for (IMetadataColumn column: columns) {
            if (!"label".equals(column.getLabel())) {
                
    stringBuffer.append(TEXT_176);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_177);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_178);
    
            }
        }
        
    stringBuffer.append(TEXT_179);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_180);
    stringBuffer.append(inRowStructEncoded);
    stringBuffer.append(TEXT_181);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_182);
    stringBuffer.append(inRowStructEncoded);
    stringBuffer.append(TEXT_183);
    stringBuffer.append(TEXT_184);
    stringBuffer.append(inRowStructEncoded);
    stringBuffer.append(TEXT_185);
    stringBuffer.append(inRowStructEncoded);
    stringBuffer.append(TEXT_186);
     if (org.talend.hadoop.distribution.ESparkVersion.SPARK_2_0.compareTo(codeGenArgument.getSparkVersion()) > 0) {
    stringBuffer.append(TEXT_187);
    stringBuffer.append(vectorClass);
    stringBuffer.append(TEXT_188);
     } else { 
    stringBuffer.append(TEXT_189);
     } 
    stringBuffer.append(TEXT_190);
    
        for (IMetadataColumn column: columns) {
            if (!"label".equals(column.getLabel())) {
                
    stringBuffer.append(TEXT_191);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_192);
    stringBuffer.append(JavaTypesManager.getTypeToGenerate(column.getTalendType(),
                                column.isNullable()));
    stringBuffer.append(TEXT_193);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_194);
    
            }
        }
        
    stringBuffer.append(TEXT_195);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_196);
    stringBuffer.append(inRowStruct);
    stringBuffer.append(TEXT_197);
    stringBuffer.append(inRowStructEncoded);
    stringBuffer.append(TEXT_198);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_199);
    stringBuffer.append(inRowStructEncoded);
    stringBuffer.append(TEXT_200);
    stringBuffer.append(inRowStruct);
    stringBuffer.append(TEXT_201);
    stringBuffer.append(TEXT_202);
    stringBuffer.append(dataframeClass);
    stringBuffer.append(TEXT_203);
    stringBuffer.append(inRowStruct);
    stringBuffer.append(TEXT_204);
    stringBuffer.append(TEXT_205);
    stringBuffer.append(dataframeClass);
    stringBuffer.append(TEXT_206);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_207);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_208);
    stringBuffer.append(inRowStruct);
    stringBuffer.append(TEXT_209);
    stringBuffer.append(vectorClass);
    stringBuffer.append(TEXT_210);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_211);
    stringBuffer.append(inRowStruct);
    stringBuffer.append(TEXT_212);
    stringBuffer.append(vectorClass);
    stringBuffer.append(TEXT_213);
    stringBuffer.append(TEXT_214);
    stringBuffer.append(inRowStruct);
    stringBuffer.append(TEXT_215);
    stringBuffer.append(inRowStruct);
    stringBuffer.append(TEXT_216);
    stringBuffer.append(TEXT_217);
    stringBuffer.append(vectorClass);
    stringBuffer.append(TEXT_218);
    stringBuffer.append(vectorClass);
    stringBuffer.append(TEXT_219);
    
        for (IMetadataColumn column: columns) {
            if (!"label".equals(column.getLabel())) {
                
    stringBuffer.append(TEXT_220);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_221);
    stringBuffer.append(JavaTypesManager.getTypeToGenerate(column.getTalendType(),
                                column.isNullable()));
    stringBuffer.append(TEXT_222);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_223);
    
            }
        }
        
    stringBuffer.append(TEXT_224);
    
}// KMEANS sparkcode 
else if("KMEANS".equals(modelType)){

    
//Parse the inputs to this javajet generator.
final BigDataCodeGeneratorArgument codeGenArgument = (BigDataCodeGeneratorArgument) argument;
final INode node = (INode)codeGenArgument.getArgument();
final String cid = node.getUniqueName();
final boolean isLog4jEnabled = ("true").equals(ElementParameterParser.getValue(node.getProcess(), "__LOG4J_ACTIVATE__"));

final String dataframeClass = org.talend.hadoop.distribution.ESparkVersion.SPARK_2_0.compareTo(codeGenArgument.getSparkVersion()) > 0
    ? "org.apache.spark.sql.DataFrame"
    : "org.apache.spark.sql.Dataset<org.apache.spark.sql.Row>";
final String vectorClass = "org.apache.spark.mllib.linalg.Vector";

    
IMetadataTable metadata = null;
IConnection conn = null;
List<IMetadataColumn> columns = null;
List<IMetadataTable> metadatas = node.getMetadataList();
if((metadatas!=null) && (metadatas.size() > 0)){
    metadata = metadatas.get(0);
    if(metadata != null){
        columns = metadata.getListColumns();
    }
}
List<? extends IConnection> conns = node.getIncomingConnections();
if(conns != null && conns.size() > 0 && conns.get(0).getLineStyle().hasConnectionCategory(IConnectionCategory.DATA)){
    conn = conns.get(0);
}

List<? extends IConnection> outConns = node.getOutgoingConnections();
IConnection outConn = null;
if(outConns != null && outConns.size() > 0 && outConns.get(0).getLineStyle().hasConnectionCategory(IConnectionCategory.DATA)){
    outConn = outConns.get(0);
}

if(columns == null || columns.isEmpty() || conn == null || outConn == null){
    return "";
}

String inRowStruct = codeGenArgument.getRecordStructName(conn);
String inRowStructEncoded = conn.getName() + "Encoded";
String connName = conn.getName();

String outRowStruct = codeGenArgument.getRecordStructName(outConn);
String outConnName = outConn.getName();


    stringBuffer.append(TEXT_225);
    stringBuffer.append(inRowStructEncoded);
    stringBuffer.append(TEXT_226);
    stringBuffer.append(inRowStruct);
    stringBuffer.append(TEXT_227);
    stringBuffer.append(vectorClass);
    stringBuffer.append(TEXT_228);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_229);
    stringBuffer.append(inRowStructEncoded);
    stringBuffer.append(TEXT_230);
    stringBuffer.append(outRowStruct);
    stringBuffer.append(TEXT_231);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_232);
    stringBuffer.append(outRowStruct);
    stringBuffer.append(TEXT_233);
    stringBuffer.append(inRowStructEncoded);
    stringBuffer.append(TEXT_234);
    stringBuffer.append(TEXT_235);
    stringBuffer.append(outRowStruct);
    stringBuffer.append(TEXT_236);
    stringBuffer.append(outRowStruct);
    stringBuffer.append(TEXT_237);
    
        for (IMetadataColumn column: columns) {
            if (!"label".equals(column.getLabel())) {
                
    stringBuffer.append(TEXT_238);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_239);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_240);
    
            }
        }
        
    stringBuffer.append(TEXT_241);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_242);
    stringBuffer.append(inRowStructEncoded);
    stringBuffer.append(TEXT_243);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_244);
    stringBuffer.append(inRowStructEncoded);
    stringBuffer.append(TEXT_245);
    stringBuffer.append(TEXT_246);
    stringBuffer.append(inRowStructEncoded);
    stringBuffer.append(TEXT_247);
    stringBuffer.append(inRowStructEncoded);
    stringBuffer.append(TEXT_248);
     if (org.talend.hadoop.distribution.ESparkVersion.SPARK_2_0.compareTo(codeGenArgument.getSparkVersion()) > 0) {
    stringBuffer.append(TEXT_249);
    stringBuffer.append(vectorClass);
    stringBuffer.append(TEXT_250);
     } else { 
    stringBuffer.append(TEXT_251);
     } 
    stringBuffer.append(TEXT_252);
    
        for (IMetadataColumn column: columns) {
            if (!"label".equals(column.getLabel())) {
                
    stringBuffer.append(TEXT_253);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_254);
    stringBuffer.append(JavaTypesManager.getTypeToGenerate(column.getTalendType(),
                                column.isNullable()));
    stringBuffer.append(TEXT_255);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_256);
    
            }
        }
        
    stringBuffer.append(TEXT_257);
    
final boolean isSpark1 = org.talend.hadoop.distribution.ESparkVersion.SPARK_2_0.compareTo(codeGenArgument.getSparkVersion()) > 0;

    stringBuffer.append(TEXT_258);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_259);
    stringBuffer.append(inRowStruct);
    stringBuffer.append(TEXT_260);
    stringBuffer.append(inRowStructEncoded);
    stringBuffer.append(TEXT_261);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_262);
    stringBuffer.append(inRowStructEncoded);
    stringBuffer.append(TEXT_263);
    stringBuffer.append(inRowStruct);
    stringBuffer.append(TEXT_264);
    stringBuffer.append(TEXT_265);
    stringBuffer.append(dataframeClass);
    stringBuffer.append(TEXT_266);
    stringBuffer.append(inRowStruct);
    stringBuffer.append(TEXT_267);
    stringBuffer.append(TEXT_268);
    stringBuffer.append(dataframeClass);
    stringBuffer.append(TEXT_269);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_270);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_271);
    stringBuffer.append(inRowStruct);
    stringBuffer.append(TEXT_272);
    stringBuffer.append(vectorClass);
    stringBuffer.append(TEXT_273);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_274);
    stringBuffer.append(inRowStruct);
    stringBuffer.append(TEXT_275);
    stringBuffer.append(vectorClass);
    stringBuffer.append(TEXT_276);
    stringBuffer.append(TEXT_277);
    stringBuffer.append(inRowStruct);
    stringBuffer.append(TEXT_278);
    stringBuffer.append(inRowStruct);
    stringBuffer.append(TEXT_279);
    stringBuffer.append(TEXT_280);
    stringBuffer.append(vectorClass);
    stringBuffer.append(TEXT_281);
     if(isSpark1) { 
    stringBuffer.append(TEXT_282);
    stringBuffer.append(vectorClass);
    stringBuffer.append(TEXT_283);
     } else { 
    stringBuffer.append(TEXT_284);
     } 
    stringBuffer.append(TEXT_285);
    
        for (IMetadataColumn column: columns) {
            if (!"label".equals(column.getLabel())) {
                
    stringBuffer.append(TEXT_286);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_287);
    stringBuffer.append(JavaTypesManager.getTypeToGenerate(column.getTalendType(),
                                column.isNullable()));
    stringBuffer.append(TEXT_288);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_289);
    
            }
        }
        
    stringBuffer.append(TEXT_290);
    

String modelLocation = ElementParameterParser.getValue(node, "__KMEANS_MODEL_LOCATION__");
Boolean inputAsPipelineModel = false;
for(INode targetNode : node.getProcess().getGeneratingNodes()){
    if (targetNode.getUniqueName().equals(modelLocation)) {
        // If the tKmeansStrModel load the pipeline from disk, it will save a PipelineModel.
        // Otherwise (the default case), it will be a Pipeline.
        // This is because this pipeline was read from HDFS and can be a complexe pipeline computed with a batch KMeans
        inputAsPipelineModel = ElementParameterParser.getBooleanValue(targetNode, "__REUSE_PIPELINE__")
                && ElementParameterParser.getBooleanValue(targetNode, "__LOAD_FROM_DISK__");
        break;
    }
}

if (inputAsPipelineModel) {
    
    stringBuffer.append(TEXT_291);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_292);
    stringBuffer.append(inRowStruct);
    stringBuffer.append(TEXT_293);
    stringBuffer.append(inRowStruct);
    stringBuffer.append(TEXT_294);
    stringBuffer.append(vectorClass);
    stringBuffer.append(TEXT_295);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_296);
    stringBuffer.append(inRowStruct);
    stringBuffer.append(TEXT_297);
    stringBuffer.append(vectorClass);
    stringBuffer.append(TEXT_298);
    stringBuffer.append(inRowStruct);
    stringBuffer.append(TEXT_299);
    stringBuffer.append(TEXT_300);
    stringBuffer.append(dataframeClass);
    stringBuffer.append(TEXT_301);
    stringBuffer.append(inRowStruct);
    stringBuffer.append(TEXT_302);
    stringBuffer.append(TEXT_303);
    stringBuffer.append(dataframeClass);
    stringBuffer.append(TEXT_304);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_305);
    
} else {
    
    stringBuffer.append(TEXT_306);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_307);
    stringBuffer.append(inRowStruct);
    stringBuffer.append(TEXT_308);
    stringBuffer.append(inRowStruct);
    stringBuffer.append(TEXT_309);
    stringBuffer.append(vectorClass);
    stringBuffer.append(TEXT_310);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_311);
    stringBuffer.append(inRowStruct);
    stringBuffer.append(TEXT_312);
    stringBuffer.append(vectorClass);
    stringBuffer.append(TEXT_313);
    stringBuffer.append(inRowStruct);
    stringBuffer.append(TEXT_314);
    stringBuffer.append(TEXT_315);
    stringBuffer.append(dataframeClass);
    stringBuffer.append(TEXT_316);
    stringBuffer.append(inRowStruct);
    stringBuffer.append(TEXT_317);
    stringBuffer.append(TEXT_318);
    stringBuffer.append(dataframeClass);
    stringBuffer.append(TEXT_319);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_320);
    
}

    stringBuffer.append(TEXT_321);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_322);
    stringBuffer.append(inRowStruct);
    stringBuffer.append(TEXT_323);
    stringBuffer.append(outRowStruct);
    stringBuffer.append(TEXT_324);
    stringBuffer.append(outRowStruct);
    stringBuffer.append(TEXT_325);
    stringBuffer.append(inRowStruct);
    stringBuffer.append(TEXT_326);
    stringBuffer.append(TEXT_327);
    stringBuffer.append(inRowStruct);
    stringBuffer.append(TEXT_328);
    stringBuffer.append(TEXT_329);
    stringBuffer.append(outRowStruct);
    stringBuffer.append(TEXT_330);
    stringBuffer.append(outRowStruct);
    stringBuffer.append(TEXT_331);
    
        for (IMetadataColumn column: columns) {
            if (!"label".equals(column.getLabel())) {
                
    stringBuffer.append(TEXT_332);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_333);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_334);
    
            }
        }
        
    stringBuffer.append(TEXT_335);
    
}

    return stringBuffer.toString();
  }
}
