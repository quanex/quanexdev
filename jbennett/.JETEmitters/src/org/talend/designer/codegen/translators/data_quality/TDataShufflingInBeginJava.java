package org.talend.designer.codegen.translators.data_quality;

import org.talend.core.model.process.INode;
import org.talend.core.model.process.ElementParameterParser;
import org.talend.designer.codegen.config.CodeGeneratorArgument;
import org.talend.core.model.metadata.IMetadataTable;
import org.talend.core.model.metadata.IMetadataColumn;
import org.talend.core.model.process.IConnection;
import org.talend.core.model.process.IConnectionCategory;
import org.talend.core.model.metadata.types.JavaTypesManager;
import org.talend.core.model.metadata.types.JavaType;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import org.talend.core.model.utils.NodeUtil;

public class TDataShufflingInBeginJava
{
  protected static String nl;
  public static synchronized TDataShufflingInBeginJava create(String lineSeparator)
  {
    nl = lineSeparator;
    TDataShufflingInBeginJava result = new TDataShufflingInBeginJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "";
  protected final String TEXT_2 = NL + "  " + NL + "\tjava.util.Queue<List<List<Object>>> shufflingQueue_";
  protected final String TEXT_3 = " = " + NL + "  \t\t(java.util.concurrent.ConcurrentLinkedQueue<List<List<Object>>>) globalMap.get(\"globalresult_";
  protected final String TEXT_4 = "\");" + NL + "\tint nb_line_";
  protected final String TEXT_5 = " = 0;" + NL + "\tString readFinishMarkWithPipeId_";
  protected final String TEXT_6 = " = \"";
  protected final String TEXT_7 = "_FINISH\"+(shufflingQueue_";
  protected final String TEXT_8 = "==null?\"\":shufflingQueue_";
  protected final String TEXT_9 = ".hashCode());" + NL + "\twhile(!globalMap.containsKey(readFinishMarkWithPipeId_";
  protected final String TEXT_10 = ") || !shufflingQueue_";
  protected final String TEXT_11 = ".isEmpty()) {//while1" + NL + "\t\tif(shufflingQueue_";
  protected final String TEXT_12 = ".isEmpty()){//if1" + NL + "\t\t\tThread.sleep(100);" + NL + "\t\t}else{" + NL + "\t\t" + NL + NL + NL;
  protected final String TEXT_13 = NL;

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    stringBuffer.append(TEXT_1);
    
    CodeGeneratorArgument codeGenArgument = (CodeGeneratorArgument) argument;
    INode node = (INode)codeGenArgument.getArgument();
    String cid = node.getUniqueName();
        
    String incomingConnName = null, outgoingConnName = null;    
	IMetadataTable outputMetadataTable = null;
	java.util.List<IMetadataColumn> outputColumns = null;
	
	IMetadataTable inputMetadataTable = null;
	java.util.List<IMetadataColumn> inputColumns = null;

	String searchedComponentName = cid + "_SHUFFLIN";
	List<? extends INode> generatedNodes = node.getProcess().getGeneratingNodes();
	for(INode loopNode : generatedNodes) {
		if(loopNode.getUniqueName().equals(searchedComponentName)) {
			List<IConnection> incomingConnections = (List<IConnection>) loopNode.getIncomingConnections();
			if (incomingConnections != null && !incomingConnections.isEmpty()) {
				for (IConnection conn : incomingConnections) {
					incomingConnName = conn.getName();				
				inputMetadataTable = conn.getMetadataTable();
				inputColumns = inputMetadataTable.getListColumns();
				break;
				}
			}
			break;
		}
	}

    List<? extends IConnection> outgoingConnections = node.getOutgoingConnections();
	if (outgoingConnections != null && !outgoingConnections.isEmpty()) {	
		for (IConnection conn : outgoingConnections) {
			if (conn.getLineStyle().hasConnectionCategory(IConnectionCategory.DATA)) {
				outgoingConnName = conn.getName();				
				outputMetadataTable = conn.getMetadataTable();
				outputColumns = outputMetadataTable.getListColumns();
				break;
			}
		}
	}     
  
    stringBuffer.append(TEXT_2);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_3);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_4);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_5);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_6);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_7);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_8);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_9);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_10);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_11);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_12);
    stringBuffer.append(TEXT_13);
    return stringBuffer.toString();
  }
}
