package org.talend.designer.codegen.translators.business.sap;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.talend.core.model.metadata.IMetadataColumn;
import org.talend.core.model.metadata.IMetadataTable;
import org.talend.core.model.metadata.types.JavaType;
import org.talend.core.model.metadata.types.JavaTypesManager;
import org.talend.core.model.process.ElementParameterParser;
import org.talend.core.model.process.IConnection;
import org.talend.core.model.process.IConnectionCategory;
import org.talend.core.model.process.INode;
import org.talend.designer.codegen.config.CodeGeneratorArgument;
import org.talend.core.model.utils.TalendTextUtils;

public class TSAPInfoCubeInputBeginJava
{
  protected static String nl;
  public static synchronized TSAPInfoCubeInputBeginJava create(String lineSeparator)
  {
    nl = lineSeparator;
    TSAPInfoCubeInputBeginJava result = new TSAPInfoCubeInputBeginJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = NL + "\t";
  protected final String TEXT_2 = NL;
  protected final String TEXT_3 = NL;
  protected final String TEXT_4 = NL + "\t\torg.talend.sap.model.bw.ISAPInfoCubeDetail detail_";
  protected final String TEXT_5 = " = metadataService_";
  protected final String TEXT_6 = ".getInfoCubeDetailByName(";
  protected final String TEXT_7 = ");";
  protected final String TEXT_8 = NL + "\t\torg.talend.sap.model.bw.request.ISAPInfoCubeRequest request_";
  protected final String TEXT_9 = " = dataService_";
  protected final String TEXT_10 = ".createInfoCubeRequest(";
  protected final String TEXT_11 = ")";
  protected final String TEXT_12 = NL + NL + "\t";
  protected final String TEXT_13 = NL;
  protected final String TEXT_14 = NL + "\t" + NL + "" + NL + "\t";
  protected final String TEXT_15 = "\t    " + NL + "\t\torg.talend.sap.ISAPConnection connection_";
  protected final String TEXT_16 = " = (org.talend.sap.ISAPConnection)globalMap.get(\"conn_";
  protected final String TEXT_17 = "\");\t";
  protected final String TEXT_18 = NL + "\t\t\t\tif(connection_";
  protected final String TEXT_19 = " == null){" + NL + "\t\t\t\t\tconnection_";
  protected final String TEXT_20 = " = ((org.talend.sap.impl.SAPConnectionFactory)(org.talend.sap.impl.SAPConnectionFactory.getInstance())).createConnection(";
  protected final String TEXT_21 = ");" + NL + "\t\t\t\t}";
  protected final String TEXT_22 = NL + "\t";
  protected final String TEXT_23 = NL + "\t\torg.talend.sap.ISAPConnection connection_";
  protected final String TEXT_24 = " = null;";
  protected final String TEXT_25 = NL + "\t\t\t\tconnection_";
  protected final String TEXT_26 = " = ((org.talend.sap.impl.SAPConnectionFactory)(org.talend.sap.impl.SAPConnectionFactory.getInstance())).createConnection(";
  protected final String TEXT_27 = ");";
  protected final String TEXT_28 = NL + "\t\t\tif (connection_";
  protected final String TEXT_29 = " == null) {//}";
  protected final String TEXT_30 = NL + "\t\t";
  protected final String TEXT_31 = NL + NL + "\tclass PropertyUil_";
  protected final String TEXT_32 = " {" + NL + "\t\t" + NL + "        void validateAndSet(java.util.Properties p, String key, Object value) {" + NL + "        \tif(value==null) {" + NL + "        \t\tSystem.err.println(\"WARN : will ignore the property : \" + key + \" as setting the null value.\"); " + NL + "        \t\treturn;" + NL + "        \t}" + NL + "        \t" + NL + "        \tp.setProperty(key, String.valueOf(value));" + NL + "        }" + NL + "        " + NL + "\t}" + NL + "\t" + NL + "\tPropertyUil_";
  protected final String TEXT_33 = " pu_";
  protected final String TEXT_34 = " = new PropertyUil_";
  protected final String TEXT_35 = "();" + NL + "" + NL + "\t";
  protected final String TEXT_36 = " " + NL + "\tfinal String decryptedPassword_";
  protected final String TEXT_37 = " = routines.system.PasswordEncryptUtil.decryptPassword(";
  protected final String TEXT_38 = ");";
  protected final String TEXT_39 = NL + "\tfinal String decryptedPassword_";
  protected final String TEXT_40 = " = ";
  protected final String TEXT_41 = "; ";
  protected final String TEXT_42 = NL + "\tjava.util.Properties properties_";
  protected final String TEXT_43 = " = new java.util.Properties();" + NL + "    pu_";
  protected final String TEXT_44 = ".validateAndSet(properties_";
  protected final String TEXT_45 = ", org.talend.sap.ISAPConnection.PROP_CLIENT, ";
  protected final String TEXT_46 = ");" + NL + "    pu_";
  protected final String TEXT_47 = ".validateAndSet(properties_";
  protected final String TEXT_48 = ", org.talend.sap.ISAPConnection.PROP_USER, ";
  protected final String TEXT_49 = ");" + NL + "    pu_";
  protected final String TEXT_50 = ".validateAndSet(properties_";
  protected final String TEXT_51 = ", org.talend.sap.ISAPConnection.PROP_PASSWORD, decryptedPassword_";
  protected final String TEXT_52 = ");" + NL + "    pu_";
  protected final String TEXT_53 = ".validateAndSet(properties_";
  protected final String TEXT_54 = ", org.talend.sap.ISAPConnection.PROP_LANGUAGE, ";
  protected final String TEXT_55 = ");" + NL + "    ";
  protected final String TEXT_56 = NL + "    pu_";
  protected final String TEXT_57 = ".validateAndSet(properties_";
  protected final String TEXT_58 = ", org.talend.sap.ISAPConnection.PROP_APPLICATION_SERVER_HOST, ";
  protected final String TEXT_59 = ");" + NL + "    pu_";
  protected final String TEXT_60 = ".validateAndSet(properties_";
  protected final String TEXT_61 = ", org.talend.sap.ISAPConnection.PROP_SYSTEM_NUMBER, ";
  protected final String TEXT_62 = ");";
  protected final String TEXT_63 = NL + "    pu_";
  protected final String TEXT_64 = ".validateAndSet(properties_";
  protected final String TEXT_65 = ", \"jco.client.mshost\", ";
  protected final String TEXT_66 = ");" + NL + "    pu_";
  protected final String TEXT_67 = ".validateAndSet(properties_";
  protected final String TEXT_68 = ", \"jco.client.r3name\", ";
  protected final String TEXT_69 = ");" + NL + "    pu_";
  protected final String TEXT_70 = ".validateAndSet(properties_";
  protected final String TEXT_71 = ", \"jco.client.group\", ";
  protected final String TEXT_72 = ");";
  protected final String TEXT_73 = NL + "    " + NL + "\t";
  protected final String TEXT_74 = NL + "\t\tpu_";
  protected final String TEXT_75 = ".validateAndSet(properties_";
  protected final String TEXT_76 = ", ";
  protected final String TEXT_77 = " ,";
  protected final String TEXT_78 = ");" + NL + "\t\t";
  protected final String TEXT_79 = NL + "        " + NL + "    \tconnection_";
  protected final String TEXT_80 = " = org.talend.sap.impl.SAPConnectionFactory.getInstance().createConnection(properties_";
  protected final String TEXT_81 = ");";
  protected final String TEXT_82 = NL + "\t\t\t//{" + NL + "\t\t\t}";
  protected final String TEXT_83 = NL + "\torg.talend.sap.service.ISAPBWService dataService_";
  protected final String TEXT_84 = " = connection_";
  protected final String TEXT_85 = ".getBWService();" + NL + "\t" + NL + "\ttry {";
  protected final String TEXT_86 = NL + "\t\t\torg.talend.sap.service.ISAPBWMetadataService metadataService_";
  protected final String TEXT_87 = " = connection_";
  protected final String TEXT_88 = ".getBWMetadataService();" + NL + "\t\t\t";
  protected final String TEXT_89 = NL + "\t\t\tjava.util.List<org.talend.sap.model.bw.ISAPInfoObjectField> fields_";
  protected final String TEXT_90 = " = detail_";
  protected final String TEXT_91 = ".getFields();" + NL + "\t\t\t" + NL + "\t\t\troutines.system.Dynamic dcg_";
  protected final String TEXT_92 = " =  new routines.system.Dynamic();" + NL + "\t\t\tjava.util.List<String> listSchema_";
  protected final String TEXT_93 = " = new java.util.ArrayList<String>();";
  protected final String TEXT_94 = NL + "    \t\t\t\tlistSchema_";
  protected final String TEXT_95 = ".add(\"";
  protected final String TEXT_96 = "\");";
  protected final String TEXT_97 = NL + "\t\t\t";
  protected final String TEXT_98 = ";" + NL + "\t\t\t" + NL + "\t\t\t//register the non dynamic fields for fetching the data from it firstly" + NL + "\t\t\tfor(String fieldName_";
  protected final String TEXT_99 = " : listSchema_";
  protected final String TEXT_100 = ") {" + NL + "\t\t\t\trequest_";
  protected final String TEXT_101 = ".addField(fieldName_";
  protected final String TEXT_102 = ");" + NL + "\t\t\t}" + NL + "\t\t\t" + NL + "\t\t\t//prepare the dynamic metadata" + NL + "\t\t\tfor(org.talend.sap.model.bw.ISAPInfoObjectField field_";
  protected final String TEXT_103 = " : fields_";
  protected final String TEXT_104 = ") {" + NL + "\t\t\t\tif(listSchema_";
  protected final String TEXT_105 = ".contains(field_";
  protected final String TEXT_106 = ".getName())) {" + NL + "\t\t\t\t\t//do nothing" + NL + "\t\t\t\t} else {" + NL + "\t\t\t\t\t//dynamic columns" + NL + "\t\t\t\t\troutines.system.DynamicMetadata dcm_";
  protected final String TEXT_107 = " = new routines.system.DynamicMetadata();" + NL + "\t\t\t\t\t" + NL + "\t\t\t\t\t//register the fields in dynamic column for fetching the data from it secondly" + NL + "\t\t\t\t\trequest_";
  protected final String TEXT_108 = ".addField(field_";
  protected final String TEXT_109 = ".getName());" + NL + "\t\t\t\t\t" + NL + "\t\t\t\t\tdcm_";
  protected final String TEXT_110 = ".setName(field_";
  protected final String TEXT_111 = ".getNameForTalend());" + NL + "                \tdcm_";
  protected final String TEXT_112 = ".setDbName(field_";
  protected final String TEXT_113 = ".getName());" + NL + "                \tdcm_";
  protected final String TEXT_114 = ".setType(routines.system.Dynamic.getTalendTypeFromDBType(\"sap_id\", field_";
  protected final String TEXT_115 = ".getType().name(), field_";
  protected final String TEXT_116 = ".getLength(), field_";
  protected final String TEXT_117 = ".getScale()));" + NL + "                \tdcm_";
  protected final String TEXT_118 = ".setDbType(field_";
  protected final String TEXT_119 = ".getType().name());" + NL + "                \t" + NL + "                \t";
  protected final String TEXT_120 = NL + "                \tdcm_";
  protected final String TEXT_121 = ".setFormat(";
  protected final String TEXT_122 = ");" + NL + "                \t";
  protected final String TEXT_123 = NL + "                \tdcm_";
  protected final String TEXT_124 = ".setLength(field_";
  protected final String TEXT_125 = ".getLength() == null ? 100 : field_";
  protected final String TEXT_126 = ".getLength());" + NL + "                \tdcm_";
  protected final String TEXT_127 = ".setPrecision(field_";
  protected final String TEXT_128 = ".getScale() == null ? 0 : field_";
  protected final String TEXT_129 = ".getScale());" + NL + "                \tdcm_";
  protected final String TEXT_130 = ".setNullable(true);" + NL + "                \tdcm_";
  protected final String TEXT_131 = ".setKey(field_";
  protected final String TEXT_132 = ".isKey());" + NL + "                \tdcm_";
  protected final String TEXT_133 = ".setSourceType(DynamicMetadata.sourceTypes.sap);" + NL + "                \t" + NL + "                \tdcg_";
  protected final String TEXT_134 = ".metadatas.add(dcm_";
  protected final String TEXT_135 = ");" + NL + "\t\t\t\t}" + NL + "\t\t\t}";
  protected final String TEXT_136 = NL + "\t\t\t\t.addField(\"";
  protected final String TEXT_137 = "\")";
  protected final String TEXT_138 = NL + "\t\t\t;";
  protected final String TEXT_139 = NL + "\t\t\trequest_";
  protected final String TEXT_140 = ".addFilter(\"";
  protected final String TEXT_141 = "\".trim(), org.talend.sap.model.SAPSign.";
  protected final String TEXT_142 = ", org.talend.sap.model.SAPComparisonOperator.";
  protected final String TEXT_143 = ", ";
  protected final String TEXT_144 = ", ";
  protected final String TEXT_145 = "null";
  protected final String TEXT_146 = ");" + NL + "\t\t\t";
  protected final String TEXT_147 = NL + "\t\t" + NL + "\t\tint fetchSize_";
  protected final String TEXT_148 = " = ";
  protected final String TEXT_149 = ";" + NL + "\t\tint maxRowCount_";
  protected final String TEXT_150 = " = ";
  protected final String TEXT_151 = ";" + NL + "\t\t" + NL + "\t\tif(fetchSize_";
  protected final String TEXT_152 = " > 0) {" + NL + "\t\t\trequest_";
  protected final String TEXT_153 = ".fetchSize(fetchSize_";
  protected final String TEXT_154 = ");" + NL + "\t\t}" + NL + "\t\t" + NL + "\t\tif(maxRowCount_";
  protected final String TEXT_155 = " > 0) {" + NL + "\t\t\trequest_";
  protected final String TEXT_156 = ".maxRows(maxRowCount_";
  protected final String TEXT_157 = ");" + NL + "\t\t}";
  protected final String TEXT_158 = " " + NL + "\t\tfinal String decrypted_ftp_pwd_";
  protected final String TEXT_159 = " = routines.system.PasswordEncryptUtil.decryptPassword(";
  protected final String TEXT_160 = ");" + NL + "\t\t";
  protected final String TEXT_161 = NL + "\t\tfinal String decrypted_ftp_pwd_";
  protected final String TEXT_162 = " = ";
  protected final String TEXT_163 = "; " + NL + "\t\t";
  protected final String TEXT_164 = NL + "\t\t" + NL + "\t\t";
  protected final String TEXT_165 = NL + "\t\t\trequest_";
  protected final String TEXT_166 = ".ftp(org.talend.sap.model.FTP_MODE.";
  protected final String TEXT_167 = ", ";
  protected final String TEXT_168 = ", ";
  protected final String TEXT_169 = ", ";
  protected final String TEXT_170 = ", decrypted_ftp_pwd_";
  protected final String TEXT_171 = ");" + NL + "\t\t\torg.talend.sap.model.table.ISAPBatchData data_";
  protected final String TEXT_172 = " = request_";
  protected final String TEXT_173 = ".readableBatch(";
  protected final String TEXT_174 = ", ";
  protected final String TEXT_175 = ");" + NL + "\t\t\t" + NL + "\t\t\tresourceMap.put(\"data_";
  protected final String TEXT_176 = "\", data_";
  protected final String TEXT_177 = ");" + NL + "\t\t";
  protected final String TEXT_178 = NL + "\t\t\torg.talend.sap.model.table.ISAPTableData data_";
  protected final String TEXT_179 = " = request_";
  protected final String TEXT_180 = ".readable();" + NL + "\t\t";
  protected final String TEXT_181 = NL + " \t" + NL + "\t \tboolean hasData_";
  protected final String TEXT_182 = " = data_";
  protected final String TEXT_183 = ".nextRow();" + NL + "\t \t" + NL + "\t\tdo {" + NL + "\t\t\tif(!hasData_";
  protected final String TEXT_184 = ") {" + NL + "\t\t\t\tbreak;" + NL + "\t\t\t}" + NL + "\t\t\t" + NL + "\t\t";
  protected final String TEXT_185 = NL + "\t\t\t";
  protected final String TEXT_186 = ".";
  protected final String TEXT_187 = " = dcg_";
  protected final String TEXT_188 = ";" + NL + "" + NL + "\t\t\tdcg_";
  protected final String TEXT_189 = ".clearColumnValues();" + NL + "\t\t\tfor (int i_";
  protected final String TEXT_190 = " = 0; i_";
  protected final String TEXT_191 = " < dcg_";
  protected final String TEXT_192 = ".getColumnCount(); i_";
  protected final String TEXT_193 = "++) {" + NL + "                routines.system.DynamicMetadata dcm_";
  protected final String TEXT_194 = " = dcg_";
  protected final String TEXT_195 = ".getColumnMetadata(i_";
  protected final String TEXT_196 = ");" + NL + "    \t\t\tObject value_";
  protected final String TEXT_197 = " = null;" + NL + "    \t\t\tint currentIndex_";
  protected final String TEXT_198 = " = ";
  protected final String TEXT_199 = " + i_";
  protected final String TEXT_200 = ";" + NL + "    \t\t\t" + NL + "                if (\"id_String\".equals(dcm_";
  protected final String TEXT_201 = ".getType())) {" + NL + "                    value_";
  protected final String TEXT_202 = " = data_";
  protected final String TEXT_203 = ".getString(currentIndex_";
  protected final String TEXT_204 = ");" + NL + "                } else if (\"id_Integer\".equals(dcm_";
  protected final String TEXT_205 = ".getType())) {" + NL + "            \t\tvalue_";
  protected final String TEXT_206 = " = data_";
  protected final String TEXT_207 = ".getInteger(currentIndex_";
  protected final String TEXT_208 = ");" + NL + "                } else if (\"id_Long\".equals(dcm_";
  protected final String TEXT_209 = ".getType())) {" + NL + "            \t\tvalue_";
  protected final String TEXT_210 = " = data_";
  protected final String TEXT_211 = ".getLong(currentIndex_";
  protected final String TEXT_212 = ");" + NL + "                } else if (\"id_Short\".equals(dcm_";
  protected final String TEXT_213 = ".getType())) {" + NL + "            \t\tvalue_";
  protected final String TEXT_214 = " = data_";
  protected final String TEXT_215 = ".getShort(currentIndex_";
  protected final String TEXT_216 = ");" + NL + "                } else if (\"id_Date\".equals(dcm_";
  protected final String TEXT_217 = ".getType())) {" + NL + "            \t\tvalue_";
  protected final String TEXT_218 = " = data_";
  protected final String TEXT_219 = ".getDate(currentIndex_";
  protected final String TEXT_220 = ");" + NL + "                } else if (\"id_Byte\".equals(dcm_";
  protected final String TEXT_221 = ".getType())) {" + NL + "            \t\tvalue_";
  protected final String TEXT_222 = " = data_";
  protected final String TEXT_223 = ".getByte(currentIndex_";
  protected final String TEXT_224 = ");" + NL + "                } else if (\"id_byte[]\".equals(dcm_";
  protected final String TEXT_225 = ".getType())) {" + NL + "            \t\tvalue_";
  protected final String TEXT_226 = " = data_";
  protected final String TEXT_227 = ".getRaw(currentIndex_";
  protected final String TEXT_228 = ");" + NL + "                } else if (\"id_Double\".equals(dcm_";
  protected final String TEXT_229 = ".getType())) {" + NL + "            \t\tvalue_";
  protected final String TEXT_230 = " = data_";
  protected final String TEXT_231 = ".getDouble(currentIndex_";
  protected final String TEXT_232 = ");" + NL + "                } else if (\"id_Float\".equals(dcm_";
  protected final String TEXT_233 = ".getType())) {" + NL + "            \t\tvalue_";
  protected final String TEXT_234 = " = data_";
  protected final String TEXT_235 = ".getFloat(currentIndex_";
  protected final String TEXT_236 = ");" + NL + "                } else if (\"id_BigDecimal\".equals(dcm_";
  protected final String TEXT_237 = ".getType())) {" + NL + "                \tif(\"BIG_INTEGER\".equals(dcm_";
  protected final String TEXT_238 = ".getDbType())) {" + NL + "                \t\tvalue_";
  protected final String TEXT_239 = " = new java.math.BigDecimal(data_";
  protected final String TEXT_240 = ".getBigInteger(currentIndex_";
  protected final String TEXT_241 = "));" + NL + "                \t} else {" + NL + "                \t\tvalue_";
  protected final String TEXT_242 = " = data_";
  protected final String TEXT_243 = ".getBigDecimal(currentIndex_";
  protected final String TEXT_244 = ");" + NL + "                \t}" + NL + "                } else if (\"id_Object\".equals(dcm_";
  protected final String TEXT_245 = ".getType())) {" + NL + "                \tif(\"BIG_INTEGER\".equals(dcm_";
  protected final String TEXT_246 = ".getDbType())) {" + NL + "                \t\tvalue_";
  protected final String TEXT_247 = " = data_";
  protected final String TEXT_248 = ".getBigInteger(currentIndex_";
  protected final String TEXT_249 = ");" + NL + "                \t} else {" + NL + "                \t\tvalue_";
  protected final String TEXT_250 = " = data_";
  protected final String TEXT_251 = ".getString(currentIndex_";
  protected final String TEXT_252 = ");" + NL + "                \t}" + NL + "                } else {" + NL + "                \t//no other possible as the talend type come from the mapping_sap.xml, so do nothing" + NL + "                }" + NL + "                " + NL + "                dcg_";
  protected final String TEXT_253 = ".addColumnValue(value_";
  protected final String TEXT_254 = ");" + NL + "        \t}";
  protected final String TEXT_255 = NL + "\t\t\t";
  protected final String TEXT_256 = ".";
  protected final String TEXT_257 = " = data_";
  protected final String TEXT_258 = ".getString(";
  protected final String TEXT_259 = ");";
  protected final String TEXT_260 = NL + "\t\t\t";
  protected final String TEXT_261 = ".";
  protected final String TEXT_262 = " = data_";
  protected final String TEXT_263 = ".getInteger(";
  protected final String TEXT_264 = ");";
  protected final String TEXT_265 = NL + "\t\t\t";
  protected final String TEXT_266 = ".";
  protected final String TEXT_267 = " = data_";
  protected final String TEXT_268 = ".getLong(";
  protected final String TEXT_269 = ");";
  protected final String TEXT_270 = NL + "\t\t\t";
  protected final String TEXT_271 = ".";
  protected final String TEXT_272 = " = data_";
  protected final String TEXT_273 = ".getShort(";
  protected final String TEXT_274 = ");";
  protected final String TEXT_275 = NL + "\t\t\t";
  protected final String TEXT_276 = ".";
  protected final String TEXT_277 = " = data_";
  protected final String TEXT_278 = ".getDate(";
  protected final String TEXT_279 = ");";
  protected final String TEXT_280 = NL + "\t\t\t";
  protected final String TEXT_281 = ".";
  protected final String TEXT_282 = " = data_";
  protected final String TEXT_283 = ".getByte(";
  protected final String TEXT_284 = ");";
  protected final String TEXT_285 = NL + "\t\t\t";
  protected final String TEXT_286 = ".";
  protected final String TEXT_287 = " = data_";
  protected final String TEXT_288 = ".getRaw(";
  protected final String TEXT_289 = ");";
  protected final String TEXT_290 = NL + "\t\t\t";
  protected final String TEXT_291 = ".";
  protected final String TEXT_292 = " = data_";
  protected final String TEXT_293 = ".getDouble(";
  protected final String TEXT_294 = ");";
  protected final String TEXT_295 = NL + "\t\t\t";
  protected final String TEXT_296 = ".";
  protected final String TEXT_297 = " = data_";
  protected final String TEXT_298 = ".getFloat(";
  protected final String TEXT_299 = ");";
  protected final String TEXT_300 = NL + "\t\t\tjava.math.BigInteger bi_";
  protected final String TEXT_301 = "_";
  protected final String TEXT_302 = " = data_";
  protected final String TEXT_303 = ".getBigInteger(";
  protected final String TEXT_304 = ");" + NL + "\t\t\t";
  protected final String TEXT_305 = ".";
  protected final String TEXT_306 = " = bi_";
  protected final String TEXT_307 = "_";
  protected final String TEXT_308 = "==null ? null : new java.math.BigDecimal(bi_";
  protected final String TEXT_309 = "_";
  protected final String TEXT_310 = ");";
  protected final String TEXT_311 = NL + "\t\t\t";
  protected final String TEXT_312 = ".";
  protected final String TEXT_313 = " = data_";
  protected final String TEXT_314 = ".getBigDecimal(";
  protected final String TEXT_315 = ");";
  protected final String TEXT_316 = NL + "\t\t\t";
  protected final String TEXT_317 = ".";
  protected final String TEXT_318 = " = data_";
  protected final String TEXT_319 = ".getBigInteger(";
  protected final String TEXT_320 = ");\t\t";
  protected final String TEXT_321 = NL + "\t\t\t";
  protected final String TEXT_322 = ".";
  protected final String TEXT_323 = " = data_";
  protected final String TEXT_324 = ".getString(";
  protected final String TEXT_325 = ");";
  protected final String TEXT_326 = NL + "\t\t\t";
  protected final String TEXT_327 = ".";
  protected final String TEXT_328 = " = ParserUtils.parseTo_";
  protected final String TEXT_329 = "(data_";
  protected final String TEXT_330 = ".getString(";
  protected final String TEXT_331 = "));";

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    stringBuffer.append(TEXT_1);
    stringBuffer.append(TEXT_2);
    
	class DefaultSAPInputUtil {
		INode node;
		
		DefaultSAPInputUtil(INode node) {
			this.node = node;
		}
		
		void generateMetadataRequestCallStatement() {
			//to be overwrite
		}
		
		void generateDataRequestCallStatement() {
			//to be overwrite
		}
	}

    stringBuffer.append(TEXT_3);
    
	class SAPDSOInputBeginUtil extends DefaultSAPInputUtil {
		String cid = node.getUniqueName();
		String tableName = ElementParameterParser.getValue(node, "__TABLE__");
		
		SAPDSOInputBeginUtil(INode node) {
			super(node);
		}
		
		void generateMetadataRequestCallStatement() {

    stringBuffer.append(TEXT_4);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_5);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_6);
    stringBuffer.append(tableName);
    stringBuffer.append(TEXT_7);
    
		}
		
		void generateDataRequestCallStatement() {

    stringBuffer.append(TEXT_8);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_9);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_10);
    stringBuffer.append(tableName);
    stringBuffer.append(TEXT_11);
    
		}
	}

    stringBuffer.append(TEXT_12);
    stringBuffer.append(TEXT_13);
    
	CodeGeneratorArgument codeGenArgument = (CodeGeneratorArgument) argument;
	INode node = (INode)codeGenArgument.getArgument();
	String cid = node.getUniqueName();
	
	SAPDSOInputBeginUtil saputil = new SAPDSOInputBeginUtil(node);
	
	List<? extends IConnection> outputConnections = node.getOutgoingSortedConnections();
	if((outputConnections == null) || (outputConnections.size() == 0)) {
		return "";
	}
	IConnection outputConnection = outputConnections.get(0);
	
	if(!outputConnection.getLineStyle().hasConnectionCategory(IConnectionCategory.DATA)) {
		return "";
	}
	
	List<IMetadataTable> metadatas = node.getMetadataList();
	if ((metadatas == null) && (metadatas.size() == 0) || (metadatas.get(0) == null)) {
		return "";
	}
	IMetadataTable metadata = metadatas.get(0);
	
	List<IMetadataColumn> columnList = metadata.getListColumns();
	if((columnList == null) || (columnList.size() == 0)) {
		return "";
	}
	
	String client = ElementParameterParser.getValue(node, "__CLIENT__");
	String userid = ElementParameterParser.getValue(node, "__USERID__");
	String password = ElementParameterParser.getValue(node, "__PASSWORD__");
	String language = ElementParameterParser.getValue(node, "__LANGUAGE__");
	String hostname = ElementParameterParser.getValue(node, "__HOSTNAME__");
	String systemnumber = ElementParameterParser.getValue(node, "__SYSTEMNUMBER__");
	
	String systemId = ElementParameterParser.getValue(node,"__SYSTEMID__");
	String groupName = ElementParameterParser.getValue(node,"__GROUPNAME__");
	
	String serverType = ElementParameterParser.getValue(node,"__SERVERTYPE__");
	
	String tableName = ElementParameterParser.getValue(node, "__TABLE__");
	String filter = ElementParameterParser.getValue(node, "__FILTER__");
	
	String fetchSize = ElementParameterParser.getValue(node, "__FETCH_SIZE__");
	String maxRowCount = ElementParameterParser.getValue(node, "__MAX_ROW_COUNT__");
	
	String file_protocol = ElementParameterParser.getValue(node,"__FILE_PROTOCOL__");
	
	List<Map<String, String>> sapProps = (List<Map<String,String>>)ElementParameterParser.getObjectValue(node, "__SAP_PROPERTIES__");
	
	String passwordFieldName = "__PASSWORD__";
	
    boolean useExistingConn = ("true").equals(ElementParameterParser.getValue(node,"__USE_EXISTING_CONNECTION__"));
	String connection = ElementParameterParser.getValue(node,"__CONNECTION__");

    stringBuffer.append(TEXT_14);
    if(useExistingConn){
    stringBuffer.append(TEXT_15);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_16);
    stringBuffer.append(connection );
    stringBuffer.append(TEXT_17);
    	
		INode connectionNode = null; 
		for (INode processNode : node.getProcess().getGeneratingNodes()) { 
			if(connection.equals(processNode.getUniqueName())) { 
				connectionNode = processNode; 
				break; 
			} 
		} 
		boolean specify_alias = "true".equals(ElementParameterParser.getValue(connectionNode, "__SPECIFY_DATASOURCE_ALIAS__"));
		if(specify_alias){
			String alias = ElementParameterParser.getValue(connectionNode, "__SAP_DATASOURCE_ALIAS__");
			if(null != alias && !("".equals(alias))){

    stringBuffer.append(TEXT_18);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_19);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_20);
    stringBuffer.append(alias);
    stringBuffer.append(TEXT_21);
    
			}
		}

    stringBuffer.append(TEXT_22);
    }else{
    stringBuffer.append(TEXT_23);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_24);
    
		boolean specify_alias = "true".equals(ElementParameterParser.getValue(node, "__SPECIFY_DATASOURCE_ALIAS__"));
		if(specify_alias){
			String alias = ElementParameterParser.getValue(node, "__SAP_DATASOURCE_ALIAS__");
			if(null != alias && !("".equals(alias))){

    stringBuffer.append(TEXT_25);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_26);
    stringBuffer.append(alias);
    stringBuffer.append(TEXT_27);
    
			}

    stringBuffer.append(TEXT_28);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_29);
    
		}

    stringBuffer.append(TEXT_30);
    stringBuffer.append(TEXT_31);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_32);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_33);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_34);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_35);
    if (ElementParameterParser.canEncrypt(node, passwordFieldName)) {
    stringBuffer.append(TEXT_36);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_37);
    stringBuffer.append(ElementParameterParser.getEncryptedValue(node, passwordFieldName));
    stringBuffer.append(TEXT_38);
    } else {
    stringBuffer.append(TEXT_39);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_40);
    stringBuffer.append( ElementParameterParser.getValue(node, passwordFieldName));
    stringBuffer.append(TEXT_41);
    }
    stringBuffer.append(TEXT_42);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_43);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_44);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_45);
    stringBuffer.append(client);
    stringBuffer.append(TEXT_46);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_47);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_48);
    stringBuffer.append(userid);
    stringBuffer.append(TEXT_49);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_50);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_51);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_52);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_53);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_54);
    stringBuffer.append(language);
    stringBuffer.append(TEXT_55);
    if("ApplicationServer".equals(serverType)){
    stringBuffer.append(TEXT_56);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_57);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_58);
    stringBuffer.append(hostname);
    stringBuffer.append(TEXT_59);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_60);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_61);
    stringBuffer.append(systemnumber);
    stringBuffer.append(TEXT_62);
    }else{
    stringBuffer.append(TEXT_63);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_64);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_65);
    stringBuffer.append(hostname);
    stringBuffer.append(TEXT_66);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_67);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_68);
    stringBuffer.append(systemId);
    stringBuffer.append(TEXT_69);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_70);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_71);
    stringBuffer.append(groupName);
    stringBuffer.append(TEXT_72);
    }
    stringBuffer.append(TEXT_73);
    
    if(sapProps!=null) {
		for(Map<String, String> item : sapProps){
		
    stringBuffer.append(TEXT_74);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_75);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_76);
    stringBuffer.append(item.get("PROPERTY") );
    stringBuffer.append(TEXT_77);
    stringBuffer.append(item.get("VALUE") );
    stringBuffer.append(TEXT_78);
     
		}
    }
	
    stringBuffer.append(TEXT_79);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_80);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_81);
    
		if(specify_alias){

    stringBuffer.append(TEXT_82);
    
		}
	}

    stringBuffer.append(TEXT_83);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_84);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_85);
    
    	boolean isDynamic = metadata.isDynamicSchema();
    	int dynamicColumnIndex = -1;
    	if(isDynamic) {

    stringBuffer.append(TEXT_86);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_87);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_88);
    saputil.generateMetadataRequestCallStatement();
    stringBuffer.append(TEXT_89);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_90);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_91);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_92);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_93);
    
			IMetadataColumn dynamicColumn = null;
        	for(int i=0;i<columnList.size();i++) {
                IMetadataColumn column = columnList.get(i);
                if("id_Dynamic".equals(column.getTalendType())) {
        			dynamicColumn = column;
        			dynamicColumnIndex = i;
    			} else {
    				String tableField = column.getOriginalDbColumnName();

    stringBuffer.append(TEXT_94);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_95);
    stringBuffer.append(tableField);
    stringBuffer.append(TEXT_96);
    
    			}
    		}

    stringBuffer.append(TEXT_97);
    saputil.generateDataRequestCallStatement();
    stringBuffer.append(TEXT_98);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_99);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_100);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_101);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_102);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_103);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_104);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_105);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_106);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_107);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_108);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_109);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_110);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_111);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_112);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_113);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_114);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_115);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_116);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_117);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_118);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_119);
    
                	String datePattern4DynamicColumn = dynamicColumn.getPattern();
                	if(datePattern4DynamicColumn!=null && !"".equals(datePattern4DynamicColumn) && !"\"\"".equals(datePattern4DynamicColumn)) {
                	
    stringBuffer.append(TEXT_120);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_121);
    stringBuffer.append(datePattern4DynamicColumn);
    stringBuffer.append(TEXT_122);
    
                	}
                	
    stringBuffer.append(TEXT_123);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_124);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_125);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_126);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_127);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_128);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_129);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_130);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_131);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_132);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_133);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_134);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_135);
    
		} else {
			saputil.generateDataRequestCallStatement();
			
			for(int i=0;i<columnList.size();i++) {
				IMetadataColumn column = columnList.get(i);
				String tableField = column.getOriginalDbColumnName();

    stringBuffer.append(TEXT_136);
    stringBuffer.append(tableField);
    stringBuffer.append(TEXT_137);
    
			}

    stringBuffer.append(TEXT_138);
    
		}
		
		List<Map<String, String>> filters = (List<Map<String,String>>)ElementParameterParser.getObjectValue(node,  "__FILTER_TABLE__");
		if(filters == null) {
			filters = new ArrayList<Map<String, String>>();
		}
		for (Map<String, String> element : filters) {
			String field = element.get("FILTER_COLUMN");
			String sign = element.get("SIGN");
			String operator = element.get("OPERATOR");
			String lowValue = element.get("LOWVALUE");
			String highValue = element.get("HIGHVALUE");
			
    stringBuffer.append(TEXT_139);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_140);
    stringBuffer.append(field);
    stringBuffer.append(TEXT_141);
    stringBuffer.append(sign);
    stringBuffer.append(TEXT_142);
    stringBuffer.append(operator);
    stringBuffer.append(TEXT_143);
    stringBuffer.append(lowValue);
    stringBuffer.append(TEXT_144);
    if(operator!=null && operator.contains("BETWEEN")){
    stringBuffer.append(highValue);
    } else {
    stringBuffer.append(TEXT_145);
    }
    stringBuffer.append(TEXT_146);
    
		}

    stringBuffer.append(TEXT_147);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_148);
    stringBuffer.append(fetchSize);
    stringBuffer.append(TEXT_149);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_150);
    stringBuffer.append(maxRowCount);
    stringBuffer.append(TEXT_151);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_152);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_153);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_154);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_155);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_156);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_157);
    
		boolean use_ftp = "true".equals(ElementParameterParser.getValue(node,"__USE_FTP_BATCH__"));
		String ftp_host = ElementParameterParser.getValue(node, "__FTP_HOST__");
		String ftp_port = ElementParameterParser.getValue(node, "__FTP_PORT__");
		String ftp_user = ElementParameterParser.getValue(node, "__FTP_USER__");
		
		String ftp_pass_field_name = "__FTP_PASS__";
		if (ElementParameterParser.canEncrypt(node, ftp_pass_field_name)) {
    stringBuffer.append(TEXT_158);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_159);
    stringBuffer.append(ElementParameterParser.getEncryptedValue(node, ftp_pass_field_name));
    stringBuffer.append(TEXT_160);
    } else {
    stringBuffer.append(TEXT_161);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_162);
    stringBuffer.append( ElementParameterParser.getValue(node, ftp_pass_field_name));
    stringBuffer.append(TEXT_163);
    }
		
		String ftp_dir = ElementParameterParser.getValue(node, "__FTP_DIR__");
		String filename_prefix = ElementParameterParser.getValue(node, "__GENERATED_FILENAME_PREFIX__");

    stringBuffer.append(TEXT_164);
     if (use_ftp){ 
    stringBuffer.append(TEXT_165);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_166);
    stringBuffer.append(file_protocol);
    stringBuffer.append(TEXT_167);
    stringBuffer.append(ftp_host);
    stringBuffer.append(TEXT_168);
    stringBuffer.append(ftp_port);
    stringBuffer.append(TEXT_169);
    stringBuffer.append(ftp_user);
    stringBuffer.append(TEXT_170);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_171);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_172);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_173);
    stringBuffer.append(ftp_dir);
    stringBuffer.append(TEXT_174);
    stringBuffer.append(filename_prefix);
    stringBuffer.append(TEXT_175);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_176);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_177);
     }else{ 
    stringBuffer.append(TEXT_178);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_179);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_180);
     } 
    stringBuffer.append(TEXT_181);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_182);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_183);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_184);
    
	for(int i=0;i<columnList.size();i++) {
		IMetadataColumn column = columnList.get(i);
	    String typeToGenerate = JavaTypesManager.getTypeToGenerate(column.getTalendType(),column.isNullable());
	    JavaType javaType = JavaTypesManager.getJavaTypeFromId(column.getTalendType());
	    String dbType = column.getType();
	    
	    if(dynamicColumnIndex == i) {//dynamic column
	    	int startIndex4DynamicColumn = columnList.size() - 1;

    stringBuffer.append(TEXT_185);
    stringBuffer.append(outputConnection.getName());
    stringBuffer.append(TEXT_186);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_187);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_188);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_189);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_190);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_191);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_192);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_193);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_194);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_195);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_196);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_197);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_198);
    stringBuffer.append(startIndex4DynamicColumn);
    stringBuffer.append(TEXT_199);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_200);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_201);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_202);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_203);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_204);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_205);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_206);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_207);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_208);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_209);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_210);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_211);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_212);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_213);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_214);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_215);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_216);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_217);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_218);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_219);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_220);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_221);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_222);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_223);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_224);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_225);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_226);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_227);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_228);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_229);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_230);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_231);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_232);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_233);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_234);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_235);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_236);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_237);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_238);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_239);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_240);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_241);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_242);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_243);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_244);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_245);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_246);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_247);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_248);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_249);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_250);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_251);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_252);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_253);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_254);
    
	    	continue;
	    }
	    
	    int index4NonDynamicColumn = (isDynamic && (i > dynamicColumnIndex)) ? (i-1) : i;
	    
		if(javaType == JavaTypesManager.STRING) {

    stringBuffer.append(TEXT_255);
    stringBuffer.append(outputConnection.getName() );
    stringBuffer.append(TEXT_256);
    stringBuffer.append(column.getLabel() );
    stringBuffer.append(TEXT_257);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_258);
    stringBuffer.append(index4NonDynamicColumn);
    stringBuffer.append(TEXT_259);
    
		} else if(javaType == JavaTypesManager.INTEGER) {

    stringBuffer.append(TEXT_260);
    stringBuffer.append(outputConnection.getName() );
    stringBuffer.append(TEXT_261);
    stringBuffer.append(column.getLabel() );
    stringBuffer.append(TEXT_262);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_263);
    stringBuffer.append(index4NonDynamicColumn);
    stringBuffer.append(TEXT_264);
    
		} else if(javaType == JavaTypesManager.LONG) {

    stringBuffer.append(TEXT_265);
    stringBuffer.append(outputConnection.getName() );
    stringBuffer.append(TEXT_266);
    stringBuffer.append(column.getLabel() );
    stringBuffer.append(TEXT_267);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_268);
    stringBuffer.append(index4NonDynamicColumn);
    stringBuffer.append(TEXT_269);
    
		} else if(javaType == JavaTypesManager.SHORT) {

    stringBuffer.append(TEXT_270);
    stringBuffer.append(outputConnection.getName() );
    stringBuffer.append(TEXT_271);
    stringBuffer.append(column.getLabel() );
    stringBuffer.append(TEXT_272);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_273);
    stringBuffer.append(index4NonDynamicColumn);
    stringBuffer.append(TEXT_274);
    
		} else if(javaType == JavaTypesManager.DATE) {

    stringBuffer.append(TEXT_275);
    stringBuffer.append(outputConnection.getName() );
    stringBuffer.append(TEXT_276);
    stringBuffer.append(column.getLabel() );
    stringBuffer.append(TEXT_277);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_278);
    stringBuffer.append(index4NonDynamicColumn);
    stringBuffer.append(TEXT_279);
    
		} else if(javaType == JavaTypesManager.BYTE) {

    stringBuffer.append(TEXT_280);
    stringBuffer.append(outputConnection.getName() );
    stringBuffer.append(TEXT_281);
    stringBuffer.append(column.getLabel() );
    stringBuffer.append(TEXT_282);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_283);
    stringBuffer.append(index4NonDynamicColumn);
    stringBuffer.append(TEXT_284);
    
		} else if(javaType == JavaTypesManager.BYTE_ARRAY) {

    stringBuffer.append(TEXT_285);
    stringBuffer.append(outputConnection.getName() );
    stringBuffer.append(TEXT_286);
    stringBuffer.append(column.getLabel() );
    stringBuffer.append(TEXT_287);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_288);
    stringBuffer.append(index4NonDynamicColumn);
    stringBuffer.append(TEXT_289);
    
		} else if(javaType == JavaTypesManager.DOUBLE) {

    stringBuffer.append(TEXT_290);
    stringBuffer.append(outputConnection.getName() );
    stringBuffer.append(TEXT_291);
    stringBuffer.append(column.getLabel() );
    stringBuffer.append(TEXT_292);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_293);
    stringBuffer.append(index4NonDynamicColumn);
    stringBuffer.append(TEXT_294);
    
		} else if(javaType == JavaTypesManager.FLOAT) {

    stringBuffer.append(TEXT_295);
    stringBuffer.append(outputConnection.getName() );
    stringBuffer.append(TEXT_296);
    stringBuffer.append(column.getLabel() );
    stringBuffer.append(TEXT_297);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_298);
    stringBuffer.append(index4NonDynamicColumn);
    stringBuffer.append(TEXT_299);
    
		} else if(javaType == JavaTypesManager.BIGDECIMAL) {
			if("BIG_INTEGER".equals(dbType)) {

    stringBuffer.append(TEXT_300);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_301);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_302);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_303);
    stringBuffer.append(index4NonDynamicColumn);
    stringBuffer.append(TEXT_304);
    stringBuffer.append(outputConnection.getName() );
    stringBuffer.append(TEXT_305);
    stringBuffer.append(column.getLabel() );
    stringBuffer.append(TEXT_306);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_307);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_308);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_309);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_310);
    
			} else {

    stringBuffer.append(TEXT_311);
    stringBuffer.append(outputConnection.getName() );
    stringBuffer.append(TEXT_312);
    stringBuffer.append(column.getLabel() );
    stringBuffer.append(TEXT_313);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_314);
    stringBuffer.append(index4NonDynamicColumn);
    stringBuffer.append(TEXT_315);
    
			}
		} else if(javaType == JavaTypesManager.OBJECT && "BIG_INTEGER".equals(dbType)) {

    stringBuffer.append(TEXT_316);
    stringBuffer.append(outputConnection.getName() );
    stringBuffer.append(TEXT_317);
    stringBuffer.append(column.getLabel() );
    stringBuffer.append(TEXT_318);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_319);
    stringBuffer.append(index4NonDynamicColumn);
    stringBuffer.append(TEXT_320);
    
		} else if(javaType == JavaTypesManager.OBJECT) {

    stringBuffer.append(TEXT_321);
    stringBuffer.append(outputConnection.getName() );
    stringBuffer.append(TEXT_322);
    stringBuffer.append(column.getLabel() );
    stringBuffer.append(TEXT_323);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_324);
    stringBuffer.append(index4NonDynamicColumn);
    stringBuffer.append(TEXT_325);
    
		} else {

    stringBuffer.append(TEXT_326);
    stringBuffer.append(outputConnection.getName() );
    stringBuffer.append(TEXT_327);
    stringBuffer.append(column.getLabel() );
    stringBuffer.append(TEXT_328);
    stringBuffer.append(typeToGenerate);
    stringBuffer.append(TEXT_329);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_330);
    stringBuffer.append(index4NonDynamicColumn);
    stringBuffer.append(TEXT_331);
    
		}
	}

    return stringBuffer.toString();
  }
}
