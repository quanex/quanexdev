package org.talend.designer.codegen.translators.databases.kudu;

import java.util.List;
import org.talend.core.model.metadata.IMetadataColumn;
import org.talend.core.model.metadata.IMetadataTable;
import org.talend.core.model.metadata.types.JavaType;
import org.talend.core.model.metadata.types.JavaTypesManager;
import org.talend.core.model.process.EConnectionType;
import org.talend.core.model.process.ElementParameterParser;
import org.talend.core.model.process.IConnection;
import org.talend.core.model.process.IConnectionCategory;
import org.talend.core.model.process.INode;
import org.talend.designer.common.BigDataCodeGeneratorArgument;

public class TKuduOutputSparkcodeJava
{
  protected static String nl;
  public static synchronized TKuduOutputSparkcodeJava create(String lineSeparator)
  {
    nl = lineSeparator;
    TKuduOutputSparkcodeJava result = new TKuduOutputSparkcodeJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "\t\t\t" + NL + "\t\t\tpublic static org.apache.spark.sql.types.StructField getStructField_";
  protected final String TEXT_2 = "(org.apache.spark.sql.types.StructField[] fields, String name, org.apache.spark.sql.types.DataType dataType) {" + NL + "\t\t\t\t" + NL + "\t\t\t\tfor(int i=0; i<fields.length; i++) {" + NL + "\t\t\t\t\tif(fields[i].name().equals(name)) {" + NL + "\t\t\t\t\t\tif(dataType == null) {" + NL + "\t\t\t\t\t\t\treturn new org.apache.spark.sql.types.StructField(fields[i].name(), fields[i].dataType(), fields[i].nullable(), fields[i].metadata());" + NL + "\t\t\t\t\t\t} else {" + NL + "\t\t\t\t\t\t\treturn new org.apache.spark.sql.types.StructField(fields[i].name(), dataType, fields[i].nullable(), fields[i].metadata());" + NL + "\t\t\t\t\t\t}" + NL + "\t\t\t\t\t}" + NL + "\t\t\t\t}" + NL + "\t\t\t\treturn null;" + NL + "\t\t\t}" + NL + "    \t\t" + NL + "\t\t\t";
  protected final String TEXT_3 = NL + "\t\t\t\tpublic static class ";
  protected final String TEXT_4 = "_";
  protected final String TEXT_5 = "ToRow implements org.apache.spark.api.java.function.Function<";
  protected final String TEXT_6 = ", org.apache.spark.sql.Row> {" + NL + "\t\t\t\t\tpublic org.apache.spark.sql.Row call(";
  protected final String TEXT_7 = " input) {" + NL + "\t\t\t\t\t\treturn org.apache.spark.sql.RowFactory.create(";
  protected final String TEXT_8 = ", ";
  protected final String TEXT_9 = NL + "\t\t\t\t\t\t\t\t\tinput.";
  protected final String TEXT_10 = " == null ? null : new java.sql.Timestamp(input.";
  protected final String TEXT_11 = ".getTime())" + NL + "\t\t\t\t\t\t\t\t";
  protected final String TEXT_12 = NL + "\t\t\t\t\t\t\t\t\tinput.";
  protected final String TEXT_13 = " == null ? null : input.";
  protected final String TEXT_14 = " + \"\"" + NL + "\t\t\t\t\t\t\t\t";
  protected final String TEXT_15 = NL + "\t\t\t\t\t\t\t\t\tinput.";
  protected final String TEXT_16 = NL + "\t\t\t\t\t\t);" + NL + "\t\t\t\t\t}" + NL + "\t\t\t\t}" + NL + "\t\t\t";
  protected final String TEXT_17 = NL + "\t\t\t\tpublic static class ";
  protected final String TEXT_18 = "_";
  protected final String TEXT_19 = "ToRow implements org.apache.spark.api.java.function.Function<";
  protected final String TEXT_20 = ", org.apache.spark.sql.Row> {" + NL + "\t\t\t\t\tpublic org.apache.spark.sql.Row call(";
  protected final String TEXT_21 = " input) {" + NL + "\t\t\t\t\t\treturn org.apache.spark.sql.RowFactory.create(";
  protected final String TEXT_22 = ", ";
  protected final String TEXT_23 = NL + "\t\t\t\t\t\t\t\t\t\tinput.";
  protected final String TEXT_24 = " == null ? null : new java.sql.Timestamp(input.";
  protected final String TEXT_25 = ".getTime())" + NL + "\t\t\t\t\t\t\t\t\t";
  protected final String TEXT_26 = NL + "\t\t\t\t\t\t\t\t\t\tinput.";
  protected final String TEXT_27 = " == null ? null : input.";
  protected final String TEXT_28 = " + \"\"" + NL + "\t\t\t\t\t\t\t\t\t";
  protected final String TEXT_29 = NL + "\t\t\t\t\t\t\t\t\t\tinput.";
  protected final String TEXT_30 = NL + "\t\t\t\t\t\t);" + NL + "\t\t\t\t\t}" + NL + "\t\t\t\t}" + NL + "\t\t\t";
  protected final String TEXT_31 = NL;

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    
BigDataCodeGeneratorArgument codeGenArgument = (BigDataCodeGeneratorArgument) argument;
INode node = (INode)codeGenArgument.getArgument();
String cid = node.getUniqueName();
List<IMetadataTable> metadatas = node.getMetadataList();

    
if(metadatas!=null && metadatas.size() > 0) {
    IMetadataTable metadata = metadatas.get(0);
    if(metadata != null){
    	List< ? extends IConnection> connections = node.getIncomingConnections();
    	if ((connections != null) && (connections.size() > 0)) {
        	IConnection connection = connections.get(0);
        	String connName = connection.getName();
        	String originalStructName = codeGenArgument.getRecordStructName(connection);
        	String inStructName = originalStructName;
        	String dataAction = ElementParameterParser.getValue(node, "__DATA_ACTION__");
        	
			java.util.List<IMetadataColumn> columns = metadata.getListColumns();
			
    stringBuffer.append(TEXT_1);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_2);
    
			if(("INSERT").equals(dataAction) || ("UPDATE").equals(dataAction) || ("UPDATE_OR_INSERT").equals(dataAction)) {
			
    stringBuffer.append(TEXT_3);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_4);
    stringBuffer.append(originalStructName);
    stringBuffer.append(TEXT_5);
    stringBuffer.append(inStructName);
    stringBuffer.append(TEXT_6);
    stringBuffer.append(inStructName);
    stringBuffer.append(TEXT_7);
     
							boolean isFirst = true;
							java.util.List<IMetadataColumn> columnsCopy = new java.util.ArrayList<IMetadataColumn>(columns);
							for(IMetadataColumn column : columnsCopy) {
								if(!isFirst) {
									
    stringBuffer.append(TEXT_8);
    
								}
								isFirst = false;
								JavaType javaType = JavaTypesManager.getJavaTypeFromId(column.getTalendType());
								if(javaType == JavaTypesManager.DATE) {
								
    stringBuffer.append(TEXT_9);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_10);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_11);
    
								} else if(javaType == JavaTypesManager.CHARACTER) {
								
    stringBuffer.append(TEXT_12);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_13);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_14);
    
								} else { 

    stringBuffer.append(TEXT_15);
    stringBuffer.append(column.getLabel());
     								
								}
							}

    stringBuffer.append(TEXT_16);
    
			} else if(("DELETE").equals(dataAction)) {
			
    stringBuffer.append(TEXT_17);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_18);
    stringBuffer.append(originalStructName);
    stringBuffer.append(TEXT_19);
    stringBuffer.append(inStructName);
    stringBuffer.append(TEXT_20);
    stringBuffer.append(inStructName);
    stringBuffer.append(TEXT_21);
     
							boolean isFirst = true;
							java.util.List<IMetadataColumn> columnsCopy = new java.util.ArrayList<IMetadataColumn>(columns);
							for(IMetadataColumn column : columnsCopy) {
								if(column.isKey()) {
									if(!isFirst) {
										
    stringBuffer.append(TEXT_22);
    
									}
									isFirst = false;
									JavaType javaType = JavaTypesManager.getJavaTypeFromId(column.getTalendType());
									if(javaType == JavaTypesManager.DATE) {
									
    stringBuffer.append(TEXT_23);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_24);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_25);
    
									} else if(javaType == JavaTypesManager.CHARACTER) {
									
    stringBuffer.append(TEXT_26);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_27);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_28);
    
									} else { 

    stringBuffer.append(TEXT_29);
    stringBuffer.append(column.getLabel());
     								
									}
								}
							} 

    stringBuffer.append(TEXT_30);
    
			}
		}
	}
}

    stringBuffer.append(TEXT_31);
    return stringBuffer.toString();
  }
}
