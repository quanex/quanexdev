package org.talend.designer.codegen.translators.dataquality.matching;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.talend.core.model.metadata.IMetadataColumn;
import org.talend.core.model.metadata.IMetadataTable;
import org.talend.core.model.metadata.types.JavaType;
import org.talend.core.model.metadata.types.JavaTypesManager;
import org.talend.core.model.process.ElementParameterParser;
import org.talend.core.model.process.IBigDataNode;
import org.talend.core.model.process.IConnection;
import org.talend.core.model.process.IConnectionCategory;
import org.talend.core.model.process.INode;
import org.talend.core.model.utils.NodeUtil;
import org.talend.designer.common.BigDataCodeGeneratorArgument;
import org.talend.designer.common.tmodelencoder.TModelEncoderUtil;
import org.talend.designer.common.tsqlrow.TSqlRowUtil;

public class TMatchModelSparkconfigJava
{
  protected static String nl;
  public static synchronized TMatchModelSparkconfigJava create(String lineSeparator)
  {
    nl = lineSeparator;
    TMatchModelSparkconfigJava result = new TMatchModelSparkconfigJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "";
  protected final String TEXT_2 = NL + "                if(log.is";
  protected final String TEXT_3 = "Enabled())";
  protected final String TEXT_4 = NL + "            log.";
  protected final String TEXT_5 = "(\"";
  protected final String TEXT_6 = " - \" ";
  protected final String TEXT_7 = " + (";
  protected final String TEXT_8 = ") ";
  protected final String TEXT_9 = ");";
  protected final String TEXT_10 = NL + "            if (log.isDebugEnabled()) {" + NL + "                class BytesLimit65535_";
  protected final String TEXT_11 = "{" + NL + "                    public void limitLog4jByte() throws Exception{" + NL + "                    StringBuilder ";
  protected final String TEXT_12 = " = new StringBuilder();";
  protected final String TEXT_13 = NL + "                    ";
  protected final String TEXT_14 = ".append(\"Parameters:\");";
  protected final String TEXT_15 = NL + "                            ";
  protected final String TEXT_16 = ".append(\"";
  protected final String TEXT_17 = "\" + \" = \" + String.valueOf(";
  protected final String TEXT_18 = ").substring(0, 4) + \"...\");     ";
  protected final String TEXT_19 = NL + "                            ";
  protected final String TEXT_20 = ".append(\"";
  protected final String TEXT_21 = "\" + \" = \" + ";
  protected final String TEXT_22 = ");";
  protected final String TEXT_23 = NL + "                        ";
  protected final String TEXT_24 = ".append(\" | \");";
  protected final String TEXT_25 = NL + "                    } " + NL + "                } " + NL + "            new BytesLimit65535_";
  protected final String TEXT_26 = "().limitLog4jByte();" + NL + "            }";
  protected final String TEXT_27 = NL + "            StringBuilder ";
  protected final String TEXT_28 = " = new StringBuilder();    ";
  protected final String TEXT_29 = NL + "                    ";
  protected final String TEXT_30 = ".append(";
  protected final String TEXT_31 = ".";
  protected final String TEXT_32 = ");";
  protected final String TEXT_33 = NL + "                    if(";
  protected final String TEXT_34 = ".";
  protected final String TEXT_35 = " == null){";
  protected final String TEXT_36 = NL + "                        ";
  protected final String TEXT_37 = ".append(\"<null>\");" + NL + "                    }else{";
  protected final String TEXT_38 = NL + "                        ";
  protected final String TEXT_39 = ".append(";
  protected final String TEXT_40 = ".";
  protected final String TEXT_41 = ");" + NL + "                    }   ";
  protected final String TEXT_42 = NL + "                ";
  protected final String TEXT_43 = ".append(\"|\");";
  protected final String TEXT_44 = NL + "java.util.List<org.apache.spark.sql.types.StructField> fields_out = new java.util.ArrayList<>();";
  protected final String TEXT_45 = "  " + NL + "" + NL + "\torg.apache.spark.sql.types.StructField field_";
  protected final String TEXT_46 = ";" + NL + "\torg.apache.spark.sql.types.DataType dataType_";
  protected final String TEXT_47 = ";";
  protected final String TEXT_48 = NL + "        dataType_";
  protected final String TEXT_49 = " = org.apache.spark.sql.types.DataTypes.IntegerType;";
  protected final String TEXT_50 = NL + "        dataType_";
  protected final String TEXT_51 = " = org.apache.spark.sql.types.DataTypes.LongType;";
  protected final String TEXT_52 = NL + "        dataType_";
  protected final String TEXT_53 = " = org.apache.spark.sql.types.DataTypes.DateType;";
  protected final String TEXT_54 = NL + "        dataType_";
  protected final String TEXT_55 = " = org.apache.spark.sql.types.DataTypes.DoubleType;";
  protected final String TEXT_56 = NL + "        dataType_";
  protected final String TEXT_57 = " = org.apache.spark.sql.types.DataTypes.BooleanType;";
  protected final String TEXT_58 = NL + "        dataType_";
  protected final String TEXT_59 = " = org.apache.spark.sql.types.DataTypes.StringType;";
  protected final String TEXT_60 = NL + "\tfield_";
  protected final String TEXT_61 = " = org.apache.spark.sql.types.DataTypes.createStructField(" + NL + "\t\t\t\t\t\t\t\t\t\"";
  protected final String TEXT_62 = "\", dataType_";
  protected final String TEXT_63 = ", ";
  protected final String TEXT_64 = ");          " + NL + "    fields_out.add(field_";
  protected final String TEXT_65 = ");";
  protected final String TEXT_66 = NL + "org.apache.spark.sql.SQLContext sqlContext_";
  protected final String TEXT_67 = " = new org.apache.spark.sql.SQLContext(ctx);";
  protected final String TEXT_68 = NL;
  protected final String TEXT_69 = " ";
  protected final String TEXT_70 = " = null;";
  protected final String TEXT_71 = NL + "\t\t// SparkSQL will use the default TimeZone to handle dates (see org.apache.spark.sql.catalyst.util.DateTimeUtils)." + NL + "        // To avoid inconsistencies with others components in the Studio, ensure that UTC is set as the default TimeZone." + NL + "        java.util.TimeZone.setDefault(java.util.TimeZone.getTimeZone(\"UTC\"));" + NL + "        " + NL + "        // java.util.Date -> java.sql.Date or java.sql.Timestamp conversions to be compliant with Spark SQL before creating our DataFrame" + NL + "        org.apache.spark.api.java.JavaRDD<";
  protected final String TEXT_72 = "> ";
  protected final String TEXT_73 = " = rdd_";
  protected final String TEXT_74 = ".map(new ";
  protected final String TEXT_75 = "_From";
  protected final String TEXT_76 = "To";
  protected final String TEXT_77 = "());" + NL + "\t";
  protected final String TEXT_78 = NL + "    //Convert the incoming RDD to DataFrame";
  protected final String TEXT_79 = NL + "    ";
  protected final String TEXT_80 = " = " + NL + "\t        sqlContext_";
  protected final String TEXT_81 = ".createDataFrame(";
  protected final String TEXT_82 = ", ";
  protected final String TEXT_83 = ".class);";
  protected final String TEXT_84 = NL + NL + "    String tdsUrl = !";
  protected final String TEXT_85 = ".endsWith(\"/\") ? ";
  protected final String TEXT_86 = " + \"/\" : ";
  protected final String TEXT_87 = ";" + NL + "    String loginUrl = tdsUrl+\"login?username=\"+java.net.URLEncoder.encode(";
  protected final String TEXT_88 = ", ";
  protected final String TEXT_89 = ".CHARSET_UTF8)" + NL + "                    +\"&password=\"+java.net.URLEncoder.encode(";
  protected final String TEXT_90 = ", ";
  protected final String TEXT_91 = ".CHARSET_UTF8)+\"&client-app=STUDIO\";";
  protected final String TEXT_92 = NL + "    ";
  protected final String TEXT_93 = " loginRequest = ";
  protected final String TEXT_94 = ".post(loginUrl);" + NL + "    int statusCode = loginRequest.code();" + NL + "    if(statusCode==403){";
  protected final String TEXT_95 = NL + "    } else if (statusCode==404){";
  protected final String TEXT_96 = NL + "    } else if(statusCode>=300){";
  protected final String TEXT_97 = NL + "    }" + NL + "    String authorization = loginRequest.header(\"AUTHORIZATION\");" + NL + "" + NL + "    String resource = tdsUrl + \"api/v1/campaigns/owned/\" + ";
  protected final String TEXT_98 = " + \"/tasks/Resolved\";" + NL + "    " + NL + "    List<Map<String, Object>> taskList = new ArrayList<>();" + NL + "    int page = 0;" + NL + "    boolean hasMoreRecords = true;" + NL + "    while(hasMoreRecords){" + NL + "        final java.lang.StringBuilder sbUrl = new java.lang.StringBuilder();" + NL + "        sbUrl.append(resource.trim());" + NL + "        sbUrl.append(\"?\").append(\"page=\").append(String.valueOf(page));" + NL + "        sbUrl.append(\"&\").append(\"size=\").append(String.valueOf(";
  protected final String TEXT_99 = "));";
  protected final String TEXT_100 = NL + "        ";
  protected final String TEXT_101 = " request= ";
  protected final String TEXT_102 = ".get(sbUrl.toString()).authorization(authorization);" + NL + "        statusCode = request.code();" + NL + "        if (statusCode >= 300) {";
  protected final String TEXT_103 = NL + "        }" + NL + "        String newAuthorization = request.header(\"AUTHORIZATION\");" + NL + "        if (newAuthorization != null) {" + NL + "            authorization = newAuthorization;" + NL + "        }";
  protected final String TEXT_104 = NL + "        java.lang.StringBuilder sb = new java.lang.StringBuilder();" + NL + "        if (request.noContent()) {";
  protected final String TEXT_105 = NL + "        } else {" + NL + "            java.io.BufferedReader reader = request.bufferedReader();" + NL + "            String line;" + NL + "            while ((line = reader.readLine()) != null) {" + NL + "                sb.append(line);" + NL + "            }" + NL + "        }" + NL + "        com.talend.fasterxml.jackson.databind.ObjectMapper MAPPER = new com.talend.fasterxml.jackson.databind.ObjectMapper();" + NL + "        List<Map<String, Object>> tasks = MAPPER.readValue(sb.toString(), List.class);" + NL + "        if(tasks.size()>=";
  protected final String TEXT_106 = "){" + NL + "            page++;" + NL + "        }else{" + NL + "            hasMoreRecords=false;" + NL + "        }" + NL + "        taskList.addAll(tasks);" + NL + "    }" + NL + "" + NL + "        java.util.Map<String, Object> rawJson = null;                                    " + NL + "        java.util.List<org.apache.spark.sql.Row> tdsRows = new java.util.ArrayList<>();" + NL + "        for (Object taskObj : taskList) {" + NL + "            rawJson = (java.util.Map<String, Object>) taskObj;" + NL + "            java.util.List<Map<String, Object>> sourceRecords = (java.util.List<Map<String, Object>>) rawJson.get(\"sourceRecords\");" + NL + "            for (Object obj : sourceRecords) {                   " + NL + "               Map<String, Object> sourceRawJson = (Map<String, Object>) obj;" + NL + "               java.util.List<Object> list = new java.util.ArrayList<>();" + NL + "               Map<String, Object> sourceRecord = (Map<String, Object>) sourceRawJson.get(\"record\");" + NL + "               for(org.apache.spark.sql.types.StructField field : fields_out){" + NL + "                    String fieldName = field.name();" + NL + "                    if(fieldName.equals(\"PAIR_ID\")){" + NL + "                        list.add(rawJson.get(\"id\"));" + NL + "                    } else if(fieldName.equals(\"SCORE\")){" + NL + "                        list.add(rawJson.get(\"matchScore\"));" + NL + "                    } else if(fieldName.equals(\"ARBITRATION\")){" + NL + "                        list.add(rawJson.get(\"arbitrationLabel\"));" + NL + "                    } else {" + NL + "                        if (field.dataType().sameType(org.apache.spark.sql.types.DataTypes.DateType) && sourceRecord.get(fieldName)!=null){" + NL + "                            //TDS server returns number of days from epoch time" + NL + "                            list.add(new java.sql.Date(java.lang.Long.parseLong(sourceRecord.get(fieldName).toString()) * (1000*3600*24)));" + NL + "                        } else {" + NL + "                            list.add(sourceRecord.get(fieldName));" + NL + "                        }" + NL + "                    }" + NL + "               }" + NL + "               tdsRows.add(org.apache.spark.sql.RowFactory.create(list.toArray()));" + NL + "            }" + NL + "        }";
  protected final String TEXT_107 = NL + "        ";
  protected final String TEXT_108 = " = " + NL + "        sqlContext_";
  protected final String TEXT_109 = ".createDataFrame(tdsRows, org.apache.spark.sql.types.DataTypes.createStructType(fields_out));\t        " + NL;
  protected final String TEXT_110 = NL + NL + "    // Create holder for all transformations to perform" + NL + "    java.util.List<org.apache.spark.ml.PipelineStage> ";
  protected final String TEXT_111 = " =" + NL + "            new java.util.ArrayList<org.apache.spark.ml.PipelineStage>();" + NL + "" + NL + "    // List of columnNames for generating the features vector" + NL + "    java.util.List<String> ";
  protected final String TEXT_112 = " = new java.util.ArrayList<String>();";
  protected final String TEXT_113 = NL + "              ";
  protected final String TEXT_114 = ".add(\"";
  protected final String TEXT_115 = "\");";
  protected final String TEXT_116 = NL + NL + "    org.talend.dataquality.reconciliation.api.MatchModel.Config conf_";
  protected final String TEXT_117 = " = " + NL + "        new org.talend.dataquality.reconciliation.api.MatchModel.Config(" + NL + "            \"";
  protected final String TEXT_118 = "\",";
  protected final String TEXT_119 = NL + "            ";
  protected final String TEXT_120 = ".toArray(new String[";
  protected final String TEXT_121 = ".size()])," + NL + "            new org.talend.dataquality.reconciliation.api.MatchModel.ParamRange(";
  protected final String TEXT_122 = ",";
  protected final String TEXT_123 = ",";
  protected final String TEXT_124 = "), " + NL + "            new org.talend.dataquality.reconciliation.api.MatchModel.ParamRange(";
  protected final String TEXT_125 = ",";
  protected final String TEXT_126 = ",";
  protected final String TEXT_127 = ")," + NL + "            \"";
  protected final String TEXT_128 = "\",";
  protected final String TEXT_129 = NL + "            ";
  protected final String TEXT_130 = ",";
  protected final String TEXT_131 = NL + "            ";
  protected final String TEXT_132 = ", " + NL + "            \"";
  protected final String TEXT_133 = "\", ";
  protected final String TEXT_134 = NL + "            ";
  protected final String TEXT_135 = ", ";
  protected final String TEXT_136 = NL + "            ";
  protected final String TEXT_137 = ", ";
  protected final String TEXT_138 = NL + "            ";
  protected final String TEXT_139 = ", " + NL + "            \"";
  protected final String TEXT_140 = "\",";
  protected final String TEXT_141 = NL + "            ";
  protected final String TEXT_142 = "," + NL + "            scala.Option.apply(";
  protected final String TEXT_143 = ")" + NL + "            );" + NL + "    " + NL + "    org.apache.spark.ml.PipelineModel pipelineModel_";
  protected final String TEXT_144 = " = " + NL + "            org.talend.dataquality.reconciliation.api.MatchModel.run(";
  protected final String TEXT_145 = ", conf_";
  protected final String TEXT_146 = ");" + NL + "" + NL + "    // save on global map" + NL + "    globalMap.put(\"";
  protected final String TEXT_147 = "_MODEL\", pipelineModel_";
  protected final String TEXT_148 = ");" + NL;
  protected final String TEXT_149 = NL + "            java.net.URI currentURI_";
  protected final String TEXT_150 = "_config = FileSystem.getDefaultUri(ctx.hadoopConfiguration());" + NL + "            FileSystem.setDefaultUri(ctx.hadoopConfiguration(), new java.net.URI(";
  protected final String TEXT_151 = "));" + NL + "            fs = FileSystem.get(ctx.hadoopConfiguration());";
  protected final String TEXT_152 = NL + "        Path pathToDelete_";
  protected final String TEXT_153 = " = new Path(";
  protected final String TEXT_154 = ");" + NL + "        if (fs.exists(pathToDelete_";
  protected final String TEXT_155 = ")) {" + NL + "            fs.delete(pathToDelete_";
  protected final String TEXT_156 = ", true);" + NL + "        }" + NL + "" + NL + "        // Serialize the model" + NL + "        com.esotericsoftware.kryo.Kryo kryo_";
  protected final String TEXT_157 = " = new com.esotericsoftware.kryo.Kryo();" + NL + "        com.esotericsoftware.kryo.io.Output modelOutput_";
  protected final String TEXT_158 = " = new com.esotericsoftware.kryo.io.Output(" + NL + "                fs.create(new org.apache.hadoop.fs.Path( ";
  protected final String TEXT_159 = " + \"/model\")));" + NL + "        kryo_";
  protected final String TEXT_160 = ".writeObject(modelOutput_";
  protected final String TEXT_161 = "," + NL + "                new TalendPipelineModel(pipelineModel_";
  protected final String TEXT_162 = ")," + NL + "                new TalendPipelineModelSerializer());" + NL + "        modelOutput_";
  protected final String TEXT_163 = ".close();";

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    stringBuffer.append(TEXT_1);
    
class BasicLogUtil{
    protected String cid  = "";
    protected org.talend.core.model.process.INode node = null;
    protected boolean log4jEnabled = false;
    private String logID = "";
    
    private BasicLogUtil(){}
    
    public BasicLogUtil(org.talend.core.model.process.INode node){
        this.node = node;
        String cidx = this.node.getUniqueName();
        if(cidx.matches("^.*?tAmazonAuroraOutput_\\d+_out$") || cidx.matches("^.*?tDBOutput_\\d+_out$")){
             cidx = cidx.substring(0,cidx.length()-4);// 4 ==> "_out".length();
        }
        this.cid = cidx;
        this.log4jEnabled = ("true").equals(org.talend.core.model.process.ElementParameterParser.getValue(this.node.getProcess(), "__LOG4J_ACTIVATE__"));
        this.log4jEnabled = this.log4jEnabled &&
                            this.node.getComponent().isLog4JEnabled() && !"JOBLET".equals(node.getComponent().getComponentType().toString());
        this.logID = this.cid;
    }
    
    public String var(String varName){
        return varName + "_" + this.cid;
    }
    public String str(String content){
        return "\"" + content + "\"";
    }
    
    public void info(String... message){
        log4j("info", message);
    }
    
    public void debug(String... message){
        log4j("debug", message);
    }
    
    public void warn(String... message){
        log4j("warn", message);
    }
    
    public void error(String... message){
        log4j("error", message);
    }
    
    public void fatal(String... message){
        log4j("fatal", message);
    }
    
    public void trace(String... message){
        log4j("trace", message);
    }
    java.util.List<String> checkableList = java.util.Arrays.asList(new String[]{"info", "debug", "trace"});     
    public void log4j(String level, String... messages){
        if(this.log4jEnabled){
            if(checkableList.contains(level)){
            
    stringBuffer.append(TEXT_2);
    stringBuffer.append(level.substring(0, 1).toUpperCase() + level.substring(1));
    stringBuffer.append(TEXT_3);
    
            }
            
    stringBuffer.append(TEXT_4);
    stringBuffer.append(level);
    stringBuffer.append(TEXT_5);
    stringBuffer.append(logID);
    stringBuffer.append(TEXT_6);
    for(String message : messages){
    stringBuffer.append(TEXT_7);
    stringBuffer.append(message);
    stringBuffer.append(TEXT_8);
    }
    stringBuffer.append(TEXT_9);
    
        }
    }
    
    public boolean isActive(){
        return this.log4jEnabled;
    }
}

class LogUtil extends BasicLogUtil{
    
    private LogUtil(){
    }
    
    public LogUtil(org.talend.core.model.process.INode node){
        super(node);
    }
    
    public void startWork(){
        debug(str("Start to work."));
    }
    
    public void endWork(){
        debug(str("Done."));
    }
    
    public void logIgnoredException(String exception){
        warn(exception);
    }
    
    public void logPrintedException(String exception){
        error(exception);
    }
    
    public void logException(String exception){
        fatal(exception);
    }
    
    public void logCompSetting(){
    
    
        if(log4jEnabled){
        
    stringBuffer.append(TEXT_10);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_11);
    stringBuffer.append(var("log4jParamters"));
    stringBuffer.append(TEXT_12);
    stringBuffer.append(TEXT_13);
    stringBuffer.append(var("log4jParamters"));
    stringBuffer.append(TEXT_14);
    
                    java.util.Set<org.talend.core.model.process.EParameterFieldType> ignoredParamsTypes = new java.util.HashSet<org.talend.core.model.process.EParameterFieldType>(); 
                    ignoredParamsTypes.addAll(
                        java.util.Arrays.asList(
                            org.talend.core.model.process.EParameterFieldType.SCHEMA_TYPE,
                            org.talend.core.model.process.EParameterFieldType.LABEL,
                            org.talend.core.model.process.EParameterFieldType.EXTERNAL,
                            org.talend.core.model.process.EParameterFieldType.MAPPING_TYPE,
                            org.talend.core.model.process.EParameterFieldType.IMAGE,
                            org.talend.core.model.process.EParameterFieldType.TNS_EDITOR,
                            org.talend.core.model.process.EParameterFieldType.WSDL2JAVA,
                            org.talend.core.model.process.EParameterFieldType.GENERATEGRAMMARCONTROLLER,
                            org.talend.core.model.process.EParameterFieldType.GENERATE_SURVIVORSHIP_RULES_CONTROLLER,
                            org.talend.core.model.process.EParameterFieldType.REFRESH_REPORTS,
                            org.talend.core.model.process.EParameterFieldType.BROWSE_REPORTS,
                            org.talend.core.model.process.EParameterFieldType.PALO_DIM_SELECTION,
                            org.talend.core.model.process.EParameterFieldType.GUESS_SCHEMA,
                            org.talend.core.model.process.EParameterFieldType.MATCH_RULE_IMEX_CONTROLLER,
                            org.talend.core.model.process.EParameterFieldType.MEMO_PERL,
                            org.talend.core.model.process.EParameterFieldType.DBTYPE_LIST,
                            org.talend.core.model.process.EParameterFieldType.VERSION,
                            org.talend.core.model.process.EParameterFieldType.TECHNICAL,
                            org.talend.core.model.process.EParameterFieldType.ICON_SELECTION,
                            org.talend.core.model.process.EParameterFieldType.JAVA_COMMAND,
                            org.talend.core.model.process.EParameterFieldType.TREE_TABLE,
                            org.talend.core.model.process.EParameterFieldType.VALIDATION_RULE_TYPE,
                            org.talend.core.model.process.EParameterFieldType.DCSCHEMA,
                            org.talend.core.model.process.EParameterFieldType.SURVIVOR_RELATION,
                            org.talend.core.model.process.EParameterFieldType.REST_RESPONSE_SCHEMA_TYPE
                            )
                    );
                    for(org.talend.core.model.process.IElementParameter ep : org.talend.core.model.utils.NodeUtil.getDisplayedParameters(node)){
                        if(!ep.isLog4JEnabled() || ignoredParamsTypes.contains(ep.getFieldType())){
                            continue;
                        }
                        String name = ep.getName();
                        if(org.talend.core.model.process.EParameterFieldType.PASSWORD.equals(ep.getFieldType())){
                            String epName = "__" + name + "__";
                            String password = "";
                            if(org.talend.core.model.process.ElementParameterParser.canEncrypt(node, epName)){
                                password = org.talend.core.model.process.ElementParameterParser.getEncryptedValue(node, epName);
                            }else{
                                String passwordValue = org.talend.core.model.process.ElementParameterParser.getValue(node, epName);
                                if (passwordValue == null || "".equals(passwordValue.trim())) {// for the value which empty
                                    passwordValue = "\"\"";
                                } 
                                password = "routines.system.PasswordEncryptUtil.encryptPassword(" + passwordValue + ")";
                            } 
                            
    stringBuffer.append(TEXT_15);
    stringBuffer.append(var("log4jParamters"));
    stringBuffer.append(TEXT_16);
    stringBuffer.append(name);
    stringBuffer.append(TEXT_17);
    stringBuffer.append(password);
    stringBuffer.append(TEXT_18);
    
                        }else{
                            String value = org.talend.core.model.utils.NodeUtil.getNormalizeParameterValue(node, ep);
                            if(value.length()>10000){
                            value = org.talend.core.model.utils.NodeUtil.replaceCRLFInMEMO_SQL(value);
                            }
                            
    stringBuffer.append(TEXT_19);
    stringBuffer.append(var("log4jParamters"));
    stringBuffer.append(TEXT_20);
    stringBuffer.append(name);
    stringBuffer.append(TEXT_21);
    stringBuffer.append(value);
    stringBuffer.append(TEXT_22);
    
                        }   
                        
    stringBuffer.append(TEXT_23);
    stringBuffer.append(var("log4jParamters"));
    stringBuffer.append(TEXT_24);
    
                    }
                    debug(var("log4jParamters")); 
                    
    stringBuffer.append(TEXT_25);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_26);
    
        } 
        
    
    } 
    
    //no use for now, because we log the data by rowStruct
    public void traceData(String rowStruct, java.util.List<org.talend.core.model.metadata.IMetadataColumn> columnList, String nbline){
        if(log4jEnabled){
        
    stringBuffer.append(TEXT_27);
    stringBuffer.append(var("log4jSb"));
    stringBuffer.append(TEXT_28);
    
            for(org.talend.core.model.metadata.IMetadataColumn column : columnList){
                org.talend.core.model.metadata.types.JavaType javaType = org.talend.core.model.metadata.types.JavaTypesManager.getJavaTypeFromId(column.getTalendType());
                String columnName = column.getLabel();
                boolean isPrimit = org.talend.core.model.metadata.types.JavaTypesManager.isJavaPrimitiveType(column.getTalendType(), column.isNullable());
                if(isPrimit){
                
    stringBuffer.append(TEXT_29);
    stringBuffer.append(var("log4jSb"));
    stringBuffer.append(TEXT_30);
    stringBuffer.append(rowStruct);
    stringBuffer.append(TEXT_31);
    stringBuffer.append(columnName);
    stringBuffer.append(TEXT_32);
    
                }else{
                
    stringBuffer.append(TEXT_33);
    stringBuffer.append(rowStruct);
    stringBuffer.append(TEXT_34);
    stringBuffer.append(columnName);
    stringBuffer.append(TEXT_35);
    stringBuffer.append(TEXT_36);
    stringBuffer.append(var("log4jSb"));
    stringBuffer.append(TEXT_37);
    stringBuffer.append(TEXT_38);
    stringBuffer.append(var("log4jSb"));
    stringBuffer.append(TEXT_39);
    stringBuffer.append(rowStruct);
    stringBuffer.append(TEXT_40);
    stringBuffer.append(columnName);
    stringBuffer.append(TEXT_41);
    
                }
                
    stringBuffer.append(TEXT_42);
    stringBuffer.append(var("log4jSb"));
    stringBuffer.append(TEXT_43);
    
            }
        }
        trace(str("Content of the record "), nbline, str(": "), var("log4jSb"));
        
    
    }
}

class LogHelper{
    
    java.util.Map<String, String> pastDict = null;
    
    public LogHelper(){
        pastDict = new java.util.HashMap<String, String>();
        pastDict.put("insert", "inserted");
        pastDict.put("update", "updated");
        pastDict.put("delete", "deleted");
        pastDict.put("upsert", "upserted");
    }   
    
    public String upperFirstChar(String data){ 
        return data.substring(0, 1).toUpperCase() + data.substring(1);
    }
    
    public String toPastTense(String data){
        return pastDict.get(data);
    }
}
LogHelper logHelper = new LogHelper();

LogUtil log = null;

    
// Parse the inputs to this javajet generator.
final BigDataCodeGeneratorArgument codeGenArgument = (BigDataCodeGeneratorArgument) argument;
final INode node = (INode) codeGenArgument.getArgument();
final IBigDataNode bigDataNode = (IBigDataNode) codeGenArgument.getArgument();
final String cid = node.getUniqueName();

LogUtil logUtil = new LogUtil(node);

final String dataframeClass = org.talend.hadoop.distribution.ESparkVersion.SPARK_2_0.compareTo(codeGenArgument.getSparkVersion()) > 0
    ? "org.apache.spark.sql.DataFrame"
    : "org.apache.spark.sql.Dataset<org.apache.spark.sql.Row>";

    

//The name of the List containing all transformations
String stagesName = "stages_" + cid;
//The name of the training DataFrame
String trainingName = "training_" + cid;

//Component params
List<Map<String, String>> inputCols = (List<Map<String,String>>)ElementParameterParser.getObjectValue(node, "__COLUMN_SELECTOR_TABLE__");
String selectedColNames = "inputCols_" + cid;
String labelColName = ElementParameterParser.getValue(node,"__LABEL_COLUMN__");

/*********** rf settings ************/
//The number of trees
String numTreesLower = ElementParameterParser.getValue(node,"__NUM_TREES_LOWER__");
String numTreesUpper = ElementParameterParser.getValue(node,"__NUM_TREES_UPPER__");
String numTreesStep = ElementParameterParser.getValue(node,"__NUM_TREES_STEP__");
//max depth
String maxDepthLower = ElementParameterParser.getValue(node,"__MAX_DEPTH_LOWER__");
String maxDepthUpper = ElementParameterParser.getValue(node,"__MAX_DEPTH_UPPER__");
String maxDepthStep = ElementParameterParser.getValue(node,"__MAX_DEPTH_STEP__");

/*********** Model saving ************/
//True if save model on disk(file system), otherwise false
Boolean saveOnDisk = ElementParameterParser.getBooleanValue(node, "__SAVE_ON_DISK__");
//HDFS folder to save in
String hdfsFolder = ElementParameterParser.getValue(node,"__HDFS_FOLDER__");
boolean useConfigurationComponent = "true".equals(ElementParameterParser.getValue(node, "__DEFINE_STORAGE_CONFIGURATION__"));
String uriPrefix = "\"\"";
if(useConfigurationComponent) {
    uriPrefix = org.talend.designer.spark.generator.storage.SparkStorageUtils.getURIPrefix(node);
    hdfsFolder = uriPrefix + " + " + hdfsFolder;
}

/*********** TDS Configuration ************/
boolean isUsingTDS = "true".equals(ElementParameterParser.getValue(node, "__IS_USING_TDS__")); 
String tdsUrl = ElementParameterParser.getValue(node, "__TDS_URL__");
String tdsUsername = ElementParameterParser.getValue(node, "__TDS_USERNAME__");
String tdsPassword = ElementParameterParser.getValue(node, "__TDS_PASSWORD__");
String tdsCampaign = ElementParameterParser.getValue(node, "__TDS_CAMPAIGN__");
String tdsBatchSize = ElementParameterParser.getValue(node, "__TDS_BATCH_SIZE__");

if(isUsingTDS){
	//Component params
	inputCols = (List<Map<String,String>>)ElementParameterParser.getObjectValue(node, "__COLUMN_SELECTOR_TABLE_TDS__");
	selectedColNames = "inputCols_" + cid;
	labelColName = "ARBITRATION";
}

List<IMetadataColumn> outputSchema = null;
List<IMetadataTable> schemaTables = node.getMetadataList();
if(schemaTables != null && schemaTables.size() > 0) {
    IMetadataTable schemaTable = schemaTables.get(0);
    if(schemaTable != null) {
        outputSchema = schemaTable.getListColumns();
    }
}

    stringBuffer.append(TEXT_44);
    
for (IMetadataColumn column : outputSchema) { 	

    stringBuffer.append(TEXT_45);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_46);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_47);
     if(column.getTalendType().contains("Integer")){ 
    stringBuffer.append(TEXT_48);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_49);
     }else if(column.getTalendType().contains("Long")){ 
    stringBuffer.append(TEXT_50);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_51);
     }else if(column.getTalendType().contains("Date")){ 
    stringBuffer.append(TEXT_52);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_53);
     }else if(column.getTalendType().contains("Double")){ 
    stringBuffer.append(TEXT_54);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_55);
     }else if(column.getTalendType().contains("Boolean")){ 
    stringBuffer.append(TEXT_56);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_57);
     }else{ 
    stringBuffer.append(TEXT_58);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_59);
     } 
    stringBuffer.append(TEXT_60);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_61);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_62);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_63);
    stringBuffer.append(column.isNullable());
    stringBuffer.append(TEXT_64);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_65);
    
}

/********* Advanced settings **********/
//Threshold for phonitic comparisons
String thresholdText = ElementParameterParser.getValue(node, "__THRESHOLD_TEXT__");
// CV params
String numFolds = ElementParameterParser.getValue(node, "__NUM_FOLDS__");
String evaluationMetric = ElementParameterParser.getValue(node, "__EVAL_METRIC__");
// Trees parmas
//Subsampling Rate
String subsamplingRate = ElementParameterParser.getValue(node,"__SUBSAMPLING_RATE__");
//Feature Subset Strategy
String featureSubsetStrategy = ElementParameterParser.getValue(node,"__FEATURE_SUBSET_STRATEGY__");
//max bins
String maxBins = ElementParameterParser.getValue(node,"__MAX_BINS__");
//minInfoGain
String minInfoGain = ElementParameterParser.getValue(node,"__MIN_INFO_GAIN__");
//minInstancesPerNode
String minInstancesPerNode = ElementParameterParser.getValue(node,"__MIN_INSTANCES_PER_NODE__");
//impurity
String impurity = ElementParameterParser.getValue(node,"__IMPURITY__");
//seed
Boolean hasSeed = ElementParameterParser.getBooleanValue(node, "__HASSEED__");
//checking point
String checkpointInterval = ElementParameterParser.getValue(node, "__CHECKPOINT_INTERVAL__");

String seed = hasSeed? "(Object)new Long(" + ElementParameterParser.getValue(node,"__SEED__") + ")" : null;//hasSeed? ElementParameterParser.getValue(node,"__SEED__"): null;

    stringBuffer.append(TEXT_66);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_67);
    stringBuffer.append(TEXT_68);
    stringBuffer.append(dataframeClass);
    stringBuffer.append(TEXT_69);
    stringBuffer.append(trainingName);
    stringBuffer.append(TEXT_70);
    
/**** TODO: (expert-only) rf tuning Parameters ****/
if(!isUsingTDS){
	//IO Connections
	TModelEncoderUtil tModelEncoderUtil = new TModelEncoderUtil(node);
	IConnection incomingConnection = tModelEncoderUtil.getIncomingConnection();
	String incomingConnectionName = incomingConnection.getName();
	String incomingStructName = codeGenArgument.getRecordStructName(incomingConnection);
	String rddName, structName;
	TSqlRowUtil tSqlRowUtil= new TSqlRowUtil(node);
	if(tSqlRowUtil.containsDateFields(incomingConnection)) {
    	// Additional map to convert from java.util.Date to java.sql.Date or java.sql.Timestamp
        String newRddName = "tmp_rdd_"+incomingConnectionName;
        String newStructName = "DF_"+incomingStructName+"AvroRecord";
	
    stringBuffer.append(TEXT_71);
    stringBuffer.append(newStructName);
    stringBuffer.append(TEXT_72);
    stringBuffer.append(newRddName);
    stringBuffer.append(TEXT_73);
    stringBuffer.append(incomingConnectionName);
    stringBuffer.append(TEXT_74);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_75);
    stringBuffer.append(incomingStructName);
    stringBuffer.append(TEXT_76);
    stringBuffer.append(newStructName);
    stringBuffer.append(TEXT_77);
    
		rddName = newRddName;
        structName = newStructName;
	} else {
        // No need for additional map
        rddName = "rdd_"+incomingConnectionName;
        structName = incomingStructName;
    }
    
    stringBuffer.append(TEXT_78);
    stringBuffer.append(TEXT_79);
    stringBuffer.append(trainingName);
    stringBuffer.append(TEXT_80);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_81);
    stringBuffer.append(rddName);
    stringBuffer.append(TEXT_82);
    stringBuffer.append(structName);
    stringBuffer.append(TEXT_83);
    
}else{
    if(node.getIncomingConnections().size()>0){

     logUtil.error("\"tMatchModel should not have input link when using TDS. \"");
    
    }else{
        String httpRequestClass = "com.github.kevinsawicki.http.HttpRequest";
    
    stringBuffer.append(TEXT_84);
    stringBuffer.append(tdsUrl);
    stringBuffer.append(TEXT_85);
    stringBuffer.append(tdsUrl);
    stringBuffer.append(TEXT_86);
    stringBuffer.append(tdsUrl);
    stringBuffer.append(TEXT_87);
    stringBuffer.append(tdsUsername);
    stringBuffer.append(TEXT_88);
    stringBuffer.append(httpRequestClass);
    stringBuffer.append(TEXT_89);
    stringBuffer.append(tdsPassword);
    stringBuffer.append(TEXT_90);
    stringBuffer.append(httpRequestClass);
    stringBuffer.append(TEXT_91);
    stringBuffer.append(TEXT_92);
    stringBuffer.append(httpRequestClass);
    stringBuffer.append(TEXT_93);
    stringBuffer.append(httpRequestClass);
    stringBuffer.append(TEXT_94);
     logUtil.error("\"Data Stewardship username or password is incorrect.\"");
    stringBuffer.append(TEXT_95);
     logUtil.error("\"Data Stewardship URL not found.\"");
    stringBuffer.append(TEXT_96);
     logUtil.error("\"Login Data Stewardship failed.\"");
    stringBuffer.append(TEXT_97);
    stringBuffer.append(tdsCampaign);
    stringBuffer.append(TEXT_98);
    stringBuffer.append(tdsBatchSize);
    stringBuffer.append(TEXT_99);
    stringBuffer.append(TEXT_100);
    stringBuffer.append(httpRequestClass);
    stringBuffer.append(TEXT_101);
    stringBuffer.append(httpRequestClass);
    stringBuffer.append(TEXT_102);
     logUtil.error("\"Find TDS tasks failed: \"+statusCode");
    stringBuffer.append(TEXT_103);
     logUtil.debug("\"Successfully processed {} \"");
    stringBuffer.append(TEXT_104);
     logUtil.warn("\"TDS response contains no content \"");
    stringBuffer.append(TEXT_105);
    stringBuffer.append(tdsBatchSize);
    stringBuffer.append(TEXT_106);
     logUtil.debug("\"Find TDS records : \"+ tdsRows.size() ");
    stringBuffer.append(TEXT_107);
    stringBuffer.append(trainingName);
    stringBuffer.append(TEXT_108);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_109);
    
    }
}

    stringBuffer.append(TEXT_110);
    stringBuffer.append(stagesName);
    stringBuffer.append(TEXT_111);
    stringBuffer.append(selectedColNames);
    stringBuffer.append(TEXT_112);
    
       if((inputCols!=null)&&(inputCols.size() > 0)){
          for(Map<String,String> colMap: inputCols){
    
    stringBuffer.append(TEXT_113);
    stringBuffer.append(selectedColNames);
    stringBuffer.append(TEXT_114);
    stringBuffer.append(colMap.get("FEATURE_COLUMN"));
    stringBuffer.append(TEXT_115);
    
          }
       }
    
    stringBuffer.append(TEXT_116);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_117);
    stringBuffer.append(labelColName);
    stringBuffer.append(TEXT_118);
    stringBuffer.append(TEXT_119);
    stringBuffer.append(selectedColNames);
    stringBuffer.append(TEXT_120);
    stringBuffer.append(selectedColNames);
    stringBuffer.append(TEXT_121);
    stringBuffer.append(numTreesLower);
    stringBuffer.append(TEXT_122);
    stringBuffer.append(numTreesUpper);
    stringBuffer.append(TEXT_123);
    stringBuffer.append(numTreesStep);
    stringBuffer.append(TEXT_124);
    stringBuffer.append(maxDepthLower);
    stringBuffer.append(TEXT_125);
    stringBuffer.append(maxDepthUpper);
    stringBuffer.append(TEXT_126);
    stringBuffer.append(maxDepthStep);
    stringBuffer.append(TEXT_127);
    stringBuffer.append(evaluationMetric);
    stringBuffer.append(TEXT_128);
    stringBuffer.append(TEXT_129);
    stringBuffer.append(numFolds);
    stringBuffer.append(TEXT_130);
    stringBuffer.append(TEXT_131);
    stringBuffer.append(subsamplingRate);
    stringBuffer.append(TEXT_132);
    stringBuffer.append(featureSubsetStrategy);
    stringBuffer.append(TEXT_133);
    stringBuffer.append(TEXT_134);
    stringBuffer.append(maxBins);
    stringBuffer.append(TEXT_135);
    stringBuffer.append(TEXT_136);
    stringBuffer.append(minInfoGain);
    stringBuffer.append(TEXT_137);
    stringBuffer.append(TEXT_138);
    stringBuffer.append(minInstancesPerNode);
    stringBuffer.append(TEXT_139);
    stringBuffer.append(impurity);
    stringBuffer.append(TEXT_140);
    stringBuffer.append(TEXT_141);
    stringBuffer.append(checkpointInterval);
    stringBuffer.append(TEXT_142);
    stringBuffer.append(seed);
    stringBuffer.append(TEXT_143);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_144);
    stringBuffer.append(trainingName);
    stringBuffer.append(TEXT_145);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_146);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_147);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_148);
     if (saveOnDisk) {
        if(!"\"\"".equals(uriPrefix)) {
            
    stringBuffer.append(TEXT_149);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_150);
    stringBuffer.append(uriPrefix);
    stringBuffer.append(TEXT_151);
    
        }
        
    stringBuffer.append(TEXT_152);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_153);
    stringBuffer.append(hdfsFolder);
    stringBuffer.append(TEXT_154);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_155);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_156);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_157);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_158);
    stringBuffer.append(hdfsFolder);
    stringBuffer.append(TEXT_159);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_160);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_161);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_162);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_163);
    
    }
    
    return stringBuffer.toString();
  }
}
