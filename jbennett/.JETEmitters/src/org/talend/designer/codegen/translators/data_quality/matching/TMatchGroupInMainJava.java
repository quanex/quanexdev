package org.talend.designer.codegen.translators.data_quality.matching;

import org.talend.core.model.process.INode;
import org.talend.core.model.process.ElementParameterParser;
import org.talend.core.model.metadata.IMetadataTable;
import org.talend.core.model.metadata.IMetadataColumn;
import org.talend.core.model.process.IConnection;
import org.talend.designer.codegen.config.CodeGeneratorArgument;
import org.talend.core.model.process.BlockCode;
import org.talend.core.model.metadata.types.JavaTypesManager;
import org.talend.core.model.metadata.types.JavaType;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import org.eclipse.emf.common.util.EMap;
import org.talend.designer.tdqmatching.MatchingData;
import org.talend.designer.tdqmatching.RuleMatcher;
import org.talend.designer.tdqmatching.JoinkeyRecord;

public class TMatchGroupInMainJava
{
  protected static String nl;
  public static synchronized TMatchGroupInMainJava create(String lineSeparator)
  {
    nl = lineSeparator;
    TMatchGroupInMainJava result = new TMatchGroupInMainJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "java.util.List<java.util.List<java.util.Map<String, String>>> matchingRulesAll_";
  protected final String TEXT_2 = " = new java.util.ArrayList<java.util.List<java.util.Map<String, String>>>();" + NL + "java.util.List<java.util.Map<String, String>> matcherList_";
  protected final String TEXT_3 = " = null;" + NL + "java.util.Map<String,String> tmpMap_";
  protected final String TEXT_4 = " =null;" + NL + "java.util.Map<String,String> paramMapTmp_";
  protected final String TEXT_5 = " =null;" + NL + "java.util.List<java.util.Map<String, String>> defaultSurvivorshipRules_";
  protected final String TEXT_6 = " = new java.util.ArrayList<java.util.Map<String, String>>();" + NL + "java.util.List<java.util.Map<String, String>> particularSurvivorshipRules_";
  protected final String TEXT_7 = " = new java.util.ArrayList<java.util.Map<String, String>>();";
  protected final String TEXT_8 = NL + "        matcherList_";
  protected final String TEXT_9 = " = new java.util.ArrayList<java.util.Map<String, String>>();";
  protected final String TEXT_10 = NL + "                tmpMap_";
  protected final String TEXT_11 = "=new java.util.HashMap<String,String>();";
  protected final String TEXT_12 = NL + "                       tmpMap_";
  protected final String TEXT_13 = ".put(\"";
  protected final String TEXT_14 = "\",";
  protected final String TEXT_15 = "+\"\");";
  protected final String TEXT_16 = NL + "                       tmpMap_";
  protected final String TEXT_17 = ".put(\"";
  protected final String TEXT_18 = "\",\"";
  protected final String TEXT_19 = "\");";
  protected final String TEXT_20 = NL + "                matcherList_";
  protected final String TEXT_21 = ".add(tmpMap_";
  protected final String TEXT_22 = ");";
  protected final String TEXT_23 = NL + "        java.util.Collections.sort(matcherList_";
  protected final String TEXT_24 = ",new Comparator<java.util.Map<String, String>>() {" + NL + "" + NL + "            @Override" + NL + "            public int compare(java.util.Map<String, String> map1, java.util.Map<String, String> map2) {" + NL + "" + NL + "                return Integer.valueOf(map1.get(\"COLUMN_IDX\")).compareTo(Integer.valueOf(map2.get(\"COLUMN_IDX\")));" + NL + "            }" + NL + "" + NL + "        });";
  protected final String TEXT_25 = NL + "            matchingRulesAll_";
  protected final String TEXT_26 = ".add(matcherList_";
  protected final String TEXT_27 = ");";
  protected final String TEXT_28 = NL + "    java.util.Map<String,String> realSurShipMap_";
  protected final String TEXT_29 = "=null;";
  protected final String TEXT_30 = NL + "        realSurShipMap_";
  protected final String TEXT_31 = "=new java.util.HashMap<String,String>();";
  protected final String TEXT_32 = NL + "                realSurShipMap_";
  protected final String TEXT_33 = ".put(\"";
  protected final String TEXT_34 = "\",\"";
  protected final String TEXT_35 = "\");";
  protected final String TEXT_36 = NL + "        defaultSurvivorshipRules_";
  protected final String TEXT_37 = ".add(realSurShipMap_";
  protected final String TEXT_38 = ");";
  protected final String TEXT_39 = NL + "    realSurShipMap_";
  protected final String TEXT_40 = "=null;";
  protected final String TEXT_41 = NL + "        realSurShipMap_";
  protected final String TEXT_42 = "=new java.util.HashMap<String,String>();";
  protected final String TEXT_43 = NL + "            realSurShipMap_";
  protected final String TEXT_44 = ".put(\"";
  protected final String TEXT_45 = "\",\"";
  protected final String TEXT_46 = "\");";
  protected final String TEXT_47 = NL + "        particularSurvivorshipRules_";
  protected final String TEXT_48 = ".add(realSurShipMap_";
  protected final String TEXT_49 = ");";
  protected final String TEXT_50 = NL + "if(matchingRulesAll_";
  protected final String TEXT_51 = ".size()==0){" + NL + "   //If no matching rules in external data, try to get to rule from JOIN_KEY table (which will be compatible to old version less than 5.3)   ";
  protected final String TEXT_52 = NL + "      matcherList_";
  protected final String TEXT_53 = " = new java.util.ArrayList<java.util.Map<String, String>>();" + NL + "      ";
  protected final String TEXT_54 = NL + "          tmpMap_";
  protected final String TEXT_55 = "=new java.util.HashMap<String,String>();";
  protected final String TEXT_56 = NL + "                  tmpMap_";
  protected final String TEXT_57 = ".put(\"ATTRIBUTE_NAME\",\"";
  protected final String TEXT_58 = "\");";
  protected final String TEXT_59 = NL + "                  tmpMap_";
  protected final String TEXT_60 = ".put(\"";
  protected final String TEXT_61 = "\",";
  protected final String TEXT_62 = "+\"\");";
  protected final String TEXT_63 = NL + "              tmpMap_";
  protected final String TEXT_64 = ".put(\"";
  protected final String TEXT_65 = "\",\"";
  protected final String TEXT_66 = "\");";
  protected final String TEXT_67 = NL + "          matcherList_";
  protected final String TEXT_68 = ".add(tmpMap_";
  protected final String TEXT_69 = ");";
  protected final String TEXT_70 = NL + "//      paramMapTmp_";
  protected final String TEXT_71 = " = new java.util.HashMap<String,String>();" + NL + "//      paramMapTmp_";
  protected final String TEXT_72 = ".put(\"INTERVAL_RULE\", \"";
  protected final String TEXT_73 = "\");" + NL + "//      paramMapTmp_";
  protected final String TEXT_74 = ".put(\"MATCHING_ALGORITHM\", \"Simple VSR Matcher\");" + NL + "//      matcherList_";
  protected final String TEXT_75 = ".add( paramMapTmp_";
  protected final String TEXT_76 = ");" + NL + "      matchingRulesAll_";
  protected final String TEXT_77 = ".add(matcherList_";
  protected final String TEXT_78 = ");";
  protected final String TEXT_79 = " " + NL + "}" + NL;
  protected final String TEXT_80 = NL + "            System.err.println(\"When use multiPass function There should be another tMatchGroup component before it and directly link! But now it is \"+\"";
  protected final String TEXT_81 = "\");" + NL + "            System.exit(1);";
  protected final String TEXT_82 = NL + "                System.err.println(\"When use multiPass function There should be another tMatchGroup component before/behind it and directly link! But now before it is \"+\"";
  protected final String TEXT_83 = "\"+ \" and behind it is \"+\"";
  protected final String TEXT_84 = "\" );" + NL + "                System.exit(1);";
  protected final String TEXT_85 = NL + "  tHash_Lookup_";
  protected final String TEXT_86 = ".initGet();";
  protected final String TEXT_87 = NL;
  protected final String TEXT_88 = NL + "    ";
  protected final String TEXT_89 = "Struct ";
  protected final String TEXT_90 = " = null; // main output is used even in \"separate output\" mode";
  protected final String TEXT_91 = NL;
  protected final String TEXT_92 = "Struct masterRow_";
  protected final String TEXT_93 = " = null; // a master-row in a group";
  protected final String TEXT_94 = NL;
  protected final String TEXT_95 = "Struct subRow_";
  protected final String TEXT_96 = " = null; // a sub-row in a group." + NL + "// key row";
  protected final String TEXT_97 = NL;
  protected final String TEXT_98 = "Struct hashKey_";
  protected final String TEXT_99 = " = new ";
  protected final String TEXT_100 = "Struct();  " + NL + "// rows respond to key row ";
  protected final String TEXT_101 = NL;
  protected final String TEXT_102 = "Struct hashValue_";
  protected final String TEXT_103 = " = new ";
  protected final String TEXT_104 = "Struct();" + NL + "java.util.concurrent.ConcurrentHashMap<Object, Object> concurrentHashMap_";
  protected final String TEXT_105 = " = (java.util.concurrent.ConcurrentHashMap) globalMap.get(\"concurrentHashMap\");" + NL + "concurrentHashMap_";
  protected final String TEXT_106 = ".putIfAbsent(\"gid_";
  protected final String TEXT_107 = "\", new java.util.concurrent.atomic.AtomicLong(0L));" + NL + "java.util.concurrent.atomic.AtomicLong gid_";
  protected final String TEXT_108 = " = (java.util.concurrent.atomic.AtomicLong) concurrentHashMap_";
  protected final String TEXT_109 = ".get(\"gid_";
  protected final String TEXT_110 = "\");" + NL + "// master rows in a group" + NL + "final java.util.List<";
  protected final String TEXT_111 = "Struct> masterRows_";
  protected final String TEXT_112 = " = new java.util.ArrayList<";
  protected final String TEXT_113 = "Struct>();" + NL + "// all rows in a group " + NL + "final java.util.List<";
  protected final String TEXT_114 = "Struct> groupRows_";
  protected final String TEXT_115 = " = new java.util.ArrayList<";
  protected final String TEXT_116 = "Struct>();" + NL + "// this Map key is MASTER GID,value is this MASTER index of all MASTERS.it will be used to get DUPLICATE GRP_QUALITY from MASTER and only in case of separate output." + NL + "final java.util.Map<String,Double> gID2GQMap_";
  protected final String TEXT_117 = " = new java.util.HashMap<String,Double>();" + NL + "final double CONFIDENCE_THRESHOLD_";
  protected final String TEXT_118 = " = Double.valueOf(";
  protected final String TEXT_119 = ");" + NL + "" + NL + "" + NL + "" + NL + "//Long gid_";
  protected final String TEXT_120 = " = 0L;" + NL + "boolean forceLoop_";
  protected final String TEXT_121 = " = (blockRows_";
  protected final String TEXT_122 = ".isEmpty());";
  protected final String TEXT_123 = "    " + NL + "        java.util.Iterator<Object[]> iter_";
  protected final String TEXT_124 = " = blockRows_";
  protected final String TEXT_125 = ".iterator();";
  protected final String TEXT_126 = NL + "        java.util.Iterator<";
  protected final String TEXT_127 = "_2Struct> iter_";
  protected final String TEXT_128 = " = blockRows_";
  protected final String TEXT_129 = ".keySet().iterator();";
  protected final String TEXT_130 = NL + NL + "//TDQ-9172 reuse JAVA API at here." + NL + "" + NL + "" + NL + "org.talend.dataquality.record.linkage.grouping.AbstractRecordGrouping<Object> recordGroupImp_";
  protected final String TEXT_131 = ";";
  protected final String TEXT_132 = NL + " recordGroupImp_";
  protected final String TEXT_133 = "=new org.talend.dataquality.record.linkage.grouping.AbstractRecordGrouping<Object>(){" + NL + "    @Override" + NL + "    protected void outputRow(Object[] row) {";
  protected final String TEXT_134 = NL + "        ";
  protected final String TEXT_135 = "Struct outStuct_";
  protected final String TEXT_136 = " = new ";
  protected final String TEXT_137 = "Struct ();" + NL + "        boolean isMaster=false; ";
  protected final String TEXT_138 = NL + "           " + NL + "              if(";
  protected final String TEXT_139 = " <row.length){" + NL + "                  outStuct_";
  protected final String TEXT_140 = ".";
  protected final String TEXT_141 = "=row[";
  protected final String TEXT_142 = "]==null?null:(";
  protected final String TEXT_143 = ")row[";
  protected final String TEXT_144 = "];" + NL + "              }" + NL + "              ";
  protected final String TEXT_145 = NL + "          if(outStuct_";
  protected final String TEXT_146 = ".MASTER==true){" + NL + "             masterRows_";
  protected final String TEXT_147 = ".add(outStuct_";
  protected final String TEXT_148 = ");";
  protected final String TEXT_149 = NL + "                 gID2GQMap_";
  protected final String TEXT_150 = ".put(String.valueOf(outStuct_";
  protected final String TEXT_151 = ".GID), outStuct_";
  protected final String TEXT_152 = ".GRP_QUALITY);";
  protected final String TEXT_153 = NL + "         }else{" + NL + "             groupRows_";
  protected final String TEXT_154 = ".add(outStuct_";
  protected final String TEXT_155 = ");" + NL + "         }" + NL + "    }" + NL + "    @Override" + NL + "    protected boolean isMaster(Object col) {" + NL + "        return String.valueOf(col).equals(\"true\");" + NL + "    }" + NL + "    @Override" + NL + "    protected Object incrementGroupSize(Object oldGroupSize) {" + NL + "        return Integer.parseInt(String.valueOf(oldGroupSize)) + 1;" + NL + "    }" + NL + "    @Override" + NL + "    protected Object[] createTYPEArray(int size) {" + NL + "        return  new Object[size];" + NL + "    }" + NL + "    @Override" + NL + "    protected Object castAsType(Object objectValue) {" + NL + "        return objectValue;" + NL + "    }" + NL + "   @Override" + NL + "   protected void outputRow(org.talend.dataquality.record.linkage.grouping.swoosh.RichRecord row) {" + NL + "       // No implementation temporarily." + NL + "" + NL + "   }      " + NL + "};" + NL + "recordGroupImp_";
  protected final String TEXT_156 = ".setOrginalInputColumnSize(";
  protected final String TEXT_157 = ");" + NL;
  protected final String TEXT_158 = NL + "    recordGroupImp_";
  protected final String TEXT_159 = "=new org.talend.dataquality.record.linkage.grouping.swoosh.ComponentSwooshMatchRecordGrouping(){" + NL + "    @Override" + NL + "    protected void outputRow(Object[] row) {";
  protected final String TEXT_160 = NL + "        ";
  protected final String TEXT_161 = "Struct outStuct_";
  protected final String TEXT_162 = " = new ";
  protected final String TEXT_163 = "Struct ();" + NL + "        boolean isMaster=false; ";
  protected final String TEXT_164 = NL + "           " + NL + "              if(";
  protected final String TEXT_165 = " <row.length){";
  protected final String TEXT_166 = NL + "                  String currentPattern = null;" + NL + "                  if(getColumnDatePatternMap()!=null){" + NL + "                      currentPattern = getColumnDatePatternMap().get(\"";
  protected final String TEXT_167 = "\");" + NL + "                  }" + NL + "                  if(currentPattern == null){" + NL + "                      currentPattern = \"yyyy-MM-dd\";" + NL + "                  }" + NL + "                      java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat(currentPattern);" + NL + "                      java.util.Date date =null;" + NL + "                      if(row[";
  protected final String TEXT_168 = "]==null){" + NL + "                          date =null;" + NL + "                      }else{" + NL + "                          try{" + NL + "                              date =sdf.parse((String)row[";
  protected final String TEXT_169 = "]);" + NL + "                          }catch(ParseException e){" + NL + "                              sdf = new java.text.SimpleDateFormat(\"EEE MMM dd HH:mm:ss zzz yyyy\", java.util.Locale.US);" + NL + "                              try{" + NL + "                                date =sdf.parse((String)row[";
  protected final String TEXT_170 = "]);" + NL + "                              }catch(ParseException e1){" + NL + "                                  " + NL + "                              }" + NL + "                          }" + NL + "                      }" + NL + "                      outStuct_";
  protected final String TEXT_171 = ".";
  protected final String TEXT_172 = "=date;";
  protected final String TEXT_173 = NL + "                  " + NL + "                  try{" + NL + "                      outStuct_";
  protected final String TEXT_174 = ".";
  protected final String TEXT_175 = "=";
  protected final String TEXT_176 = ".valueOf((String)row[";
  protected final String TEXT_177 = "]);" + NL + "                      }catch(java.lang.NumberFormatException e){" + NL + "                          outStuct_";
  protected final String TEXT_178 = ".";
  protected final String TEXT_179 = "=";
  protected final String TEXT_180 = NL + "                          row[";
  protected final String TEXT_181 = "]==null?null:0;";
  protected final String TEXT_182 = NL + "                              0.0;";
  protected final String TEXT_183 = NL + "                              0;";
  protected final String TEXT_184 = NL + "                              0l;";
  protected final String TEXT_185 = NL + "                          row[";
  protected final String TEXT_186 = "]==null?null:0;";
  protected final String TEXT_187 = NL + "                      }";
  protected final String TEXT_188 = NL + "                      outStuct_";
  protected final String TEXT_189 = ".";
  protected final String TEXT_190 = "=row[";
  protected final String TEXT_191 = "]==null?null:row[";
  protected final String TEXT_192 = "];";
  protected final String TEXT_193 = NL + "                      outStuct_";
  protected final String TEXT_194 = ".";
  protected final String TEXT_195 = "=row[";
  protected final String TEXT_196 = "]==null?null:((String) row[";
  protected final String TEXT_197 = "]).charAt(0);";
  protected final String TEXT_198 = NL + "                      try{" + NL + "                          outStuct_";
  protected final String TEXT_199 = ".";
  protected final String TEXT_200 = "=new BigDecimal(((String) row[";
  protected final String TEXT_201 = "]));" + NL + "                      }catch(java.lang.NumberFormatException e){" + NL + "                          outStuct_";
  protected final String TEXT_202 = ".";
  protected final String TEXT_203 = "=new BigDecimal(0.0);" + NL + "                      }";
  protected final String TEXT_204 = NL + "                      outStuct_";
  protected final String TEXT_205 = ".";
  protected final String TEXT_206 = "=row[";
  protected final String TEXT_207 = "]==null?null:";
  protected final String TEXT_208 = ".valueOf((String)row[";
  protected final String TEXT_209 = "]);";
  protected final String TEXT_210 = NL + "              }" + NL + "              ";
  protected final String TEXT_211 = NL + "          if(outStuct_";
  protected final String TEXT_212 = ".MASTER==true){" + NL + "             masterRows_";
  protected final String TEXT_213 = ".add(outStuct_";
  protected final String TEXT_214 = ");" + NL + "             gID2GQMap_";
  protected final String TEXT_215 = ".put(String.valueOf(outStuct_";
  protected final String TEXT_216 = ".GID), outStuct_";
  protected final String TEXT_217 = ".GRP_QUALITY);" + NL + "         }else{" + NL + "             groupRows_";
  protected final String TEXT_218 = ".add(outStuct_";
  protected final String TEXT_219 = ");" + NL + "         }" + NL + "    }" + NL + "    @Override" + NL + "    protected boolean isMaster(Object col) {" + NL + "        return String.valueOf(col).equals(\"true\");" + NL + "    }" + NL + "};";
  protected final String TEXT_220 = NL + "recordGroupImp_";
  protected final String TEXT_221 = ".setOrginalInputColumnSize(";
  protected final String TEXT_222 = ");";
  protected final String TEXT_223 = NL + "recordGroupImp_";
  protected final String TEXT_224 = ".setOrginalInputColumnSize(";
  protected final String TEXT_225 = ");";
  protected final String TEXT_226 = NL + "recordGroupImp_";
  protected final String TEXT_227 = ".setRecordLinkAlgorithm(org.talend.dataquality.record.linkage.constant.RecordMatcherType.T_SwooshAlgorithm);" + NL + "//add mutch rules" + NL + "for(java.util.List<java.util.Map<String, String>> matcherList:matchingRulesAll_";
  protected final String TEXT_228 = "){" + NL + "   recordGroupImp_";
  protected final String TEXT_229 = ".addMatchRule(matcherList);" + NL + "}" + NL + "recordGroupImp_";
  protected final String TEXT_230 = ".initialize();" + NL + "//init the parameters of the tswoosh algorithm" + NL + "java.util.Map<String, String> columnWithType_";
  protected final String TEXT_231 = " = new java.util.HashMap<String, String>();" + NL + "java.util.Map<String, String> columnWithIndex_";
  protected final String TEXT_232 = " = new java.util.HashMap<String, String>();" + NL + "java.util.Map<String, String> columnDatePattern_";
  protected final String TEXT_233 = " = new java.util.HashMap<String, String>();" + NL + "java.util.List<java.util.List<java.util.Map<String, String>>> allRules_";
  protected final String TEXT_234 = " = new java.util.ArrayList<java.util.List<java.util.Map<String, String>>>();" + NL + "" + NL + "//default survivorship";
  protected final String TEXT_235 = NL + "    java.util.Map<String,String> realSurShipMap=new java.util.HashMap<String,String>();";
  protected final String TEXT_236 = NL + "        realSurShipMap.put(\"";
  protected final String TEXT_237 = "\",\"";
  protected final String TEXT_238 = "\");";
  protected final String TEXT_239 = NL + "    defaultSurvivorshipRules_";
  protected final String TEXT_240 = ".add(realSurShipMap);";
  protected final String TEXT_241 = NL + "    java.util.List targetMatchRuleList=new java.util.ArrayList<java.util.Map<String,String>>();";
  protected final String TEXT_242 = NL + "        java.util.Map<String,String> realJoinKeyMap=new java.util.HashMap<String,String>();";
  protected final String TEXT_243 = NL + "            realJoinKeyMap.put(\"";
  protected final String TEXT_244 = "\",\"";
  protected final String TEXT_245 = "\");";
  protected final String TEXT_246 = NL + "        targetMatchRuleList.add(realJoinKeyMap);";
  protected final String TEXT_247 = NL + "    allRules_";
  protected final String TEXT_248 = ".add(targetMatchRuleList);";
  protected final String TEXT_249 = NL + "    columnWithType_";
  protected final String TEXT_250 = ".put(\"";
  protected final String TEXT_251 = "\",\"";
  protected final String TEXT_252 = "\");" + NL + "    columnWithIndex_";
  protected final String TEXT_253 = ".put(\"";
  protected final String TEXT_254 = "\",\"";
  protected final String TEXT_255 = "\");";
  protected final String TEXT_256 = NL + "        columnDatePattern_";
  protected final String TEXT_257 = ".put(\"";
  protected final String TEXT_258 = "\",";
  protected final String TEXT_259 = ");";
  protected final String TEXT_260 = NL + "recordGroupImp_";
  protected final String TEXT_261 = ".setColumnDatePatternMap(columnDatePattern_";
  protected final String TEXT_262 = ");" + NL + "org.talend.dataquality.record.linkage.grouping.swoosh.SurvivorShipAlgorithmParams survivorShipAlgorithmParams_";
  protected final String TEXT_263 = " = org.talend.dataquality.record.linkage.grouping.swoosh.SurvivorshipUtils." + NL + "    createSurvivorShipAlgorithmParams((org.talend.dataquality.record.linkage.grouping.swoosh.AnalysisSwooshMatchRecordGrouping)recordGroupImp_";
  protected final String TEXT_264 = ",matchingRulesAll_";
  protected final String TEXT_265 = ",defaultSurvivorshipRules_";
  protected final String TEXT_266 = ",particularSurvivorshipRules_";
  protected final String TEXT_267 = ",columnWithType_";
  protected final String TEXT_268 = ",columnWithIndex_";
  protected final String TEXT_269 = ");" + NL + "((org.talend.dataquality.record.linkage.grouping.swoosh.AnalysisSwooshMatchRecordGrouping)recordGroupImp_";
  protected final String TEXT_270 = ").setSurvivorShipAlgorithmParams(survivorShipAlgorithmParams_";
  protected final String TEXT_271 = ");";
  protected final String TEXT_272 = NL + "recordGroupImp_";
  protected final String TEXT_273 = ".setColumnDelimiter(\";\");" + NL + "recordGroupImp_";
  protected final String TEXT_274 = ".setIsOutputDistDetails(";
  protected final String TEXT_275 = ");" + NL + "" + NL + "recordGroupImp_";
  protected final String TEXT_276 = ".setAcceptableThreshold(Float.valueOf(";
  protected final String TEXT_277 = "+\"\"));" + NL + "recordGroupImp_";
  protected final String TEXT_278 = ".setIsLinkToPrevious(";
  protected final String TEXT_279 = "&&";
  protected final String TEXT_280 = ");" + NL + "recordGroupImp_";
  protected final String TEXT_281 = ".setIsDisplayAttLabels(";
  protected final String TEXT_282 = ");" + NL + "recordGroupImp_";
  protected final String TEXT_283 = ".setIsGIDStringType(\"true\".equals(\"";
  protected final String TEXT_284 = "\")?true :false);" + NL + "recordGroupImp_";
  protected final String TEXT_285 = ".setIsPassOriginalValue(";
  protected final String TEXT_286 = "&&";
  protected final String TEXT_287 = ");" + NL + "recordGroupImp_";
  protected final String TEXT_288 = ".setIsStoreOndisk(";
  protected final String TEXT_289 = ");" + NL + "gID2GQMap_";
  protected final String TEXT_290 = ".clear();" + NL + "" + NL + "while (iter_";
  protected final String TEXT_291 = ".hasNext() || forceLoop_";
  protected final String TEXT_292 = "){ // C_01" + NL + "" + NL + "  if (forceLoop_";
  protected final String TEXT_293 = "){" + NL + "    forceLoop_";
  protected final String TEXT_294 = " = false;" + NL + "  } else {" + NL + "    // block existing" + NL;
  protected final String TEXT_295 = NL + "      ";
  protected final String TEXT_296 = "_2Struct row_";
  protected final String TEXT_297 = " =null;";
  protected final String TEXT_298 = NL + "          row_";
  protected final String TEXT_299 = " = new ";
  protected final String TEXT_300 = "_2Struct();" + NL + "          row_";
  protected final String TEXT_301 = ".restoreObjectByArrays(iter_";
  protected final String TEXT_302 = ".next());";
  protected final String TEXT_303 = NL + "          row_";
  protected final String TEXT_304 = " = iter_";
  protected final String TEXT_305 = ".next();";
  protected final String TEXT_306 = NL + "          hashKey_";
  protected final String TEXT_307 = ".";
  protected final String TEXT_308 = " = row_";
  protected final String TEXT_309 = ".";
  protected final String TEXT_310 = ";" + NL + "          hashKey_";
  protected final String TEXT_311 = ".hashCodeDirty = true;";
  protected final String TEXT_312 = NL + "  }" + NL + "  tHash_Lookup_";
  protected final String TEXT_313 = ".lookup(hashKey_";
  protected final String TEXT_314 = ");" + NL + "  masterRows_";
  protected final String TEXT_315 = ".clear();";
  protected final String TEXT_316 = NL + "  groupRows_";
  protected final String TEXT_317 = ".clear();";
  protected final String TEXT_318 = NL + "  " + NL + "  ";
  protected final String TEXT_319 = NL + "  //add mutch rules" + NL + "  for(java.util.List<java.util.Map<String, String>> matcherList:matchingRulesAll_";
  protected final String TEXT_320 = "){" + NL + "     recordGroupImp_";
  protected final String TEXT_321 = ".addMatchRule(matcherList);" + NL + "  }" + NL + "  recordGroupImp_";
  protected final String TEXT_322 = ".initialize();";
  protected final String TEXT_323 = NL + " " + NL + "  while (tHash_Lookup_";
  protected final String TEXT_324 = ".hasNext()){  // loop on each data in one block" + NL + "    hashValue_";
  protected final String TEXT_325 = " = tHash_Lookup_";
  protected final String TEXT_326 = ".next();" + NL + "    // set the a loop data into inputTexts one column by one column. " + NL + "    java.util.List<Object> inputTexts=new java.util.ArrayList<Object>();";
  protected final String TEXT_327 = NL + "          inputTexts.add(hashValue_";
  protected final String TEXT_328 = ".";
  protected final String TEXT_329 = ");" + NL + "          ";
  protected final String TEXT_330 = NL + "    " + NL + "    recordGroupImp_";
  protected final String TEXT_331 = ".doGroup((Object[]) inputTexts.toArray(new Object[inputTexts.size()]));" + NL + "    " + NL + "  } // while" + NL + "" + NL + "  recordGroupImp_";
  protected final String TEXT_332 = ".end();" + NL + "  groupRows_";
  protected final String TEXT_333 = ".addAll(masterRows_";
  protected final String TEXT_334 = ");" + NL + "    ";
  protected final String TEXT_335 = NL + "    java.util.Collections.sort(groupRows_";
  protected final String TEXT_336 = ", " + NL + "      new Comparator<";
  protected final String TEXT_337 = "Struct>(){" + NL + "        public int compare(";
  protected final String TEXT_338 = "Struct row1, ";
  protected final String TEXT_339 = "Struct row2){" + NL + "          if (!(row1.GID).equals(row2.GID)){" + NL + "            return (row1.GID).compareTo(row2.GID);" + NL + "          } else {" + NL + "            // false < true" + NL + "            return (row2.MASTER).compareTo(row1.MASTER); " + NL + "          }" + NL + "        }" + NL + "      }" + NL + "    );";
  protected final String TEXT_340 = NL;
  protected final String TEXT_341 = NL + "} // C_01 //TDQ-9655  if the second tMatchGroup select sort and mulyipass, make the C_01 while end here, and sort all rows " + NL + "    java.util.Collections.sort(groupRows_";
  protected final String TEXT_342 = ", " + NL + "      new Comparator<";
  protected final String TEXT_343 = "Struct>(){" + NL + "        public int compare(";
  protected final String TEXT_344 = "Struct row1, ";
  protected final String TEXT_345 = "Struct row2){" + NL + "          if (!(row1.GID).equals(row2.GID)){" + NL + "            return (row1.GID).compareTo(row2.GID);" + NL + "          } else {" + NL + "            // false < true" + NL + "            return (row2.MASTER).compareTo(row1.MASTER); " + NL + "          }" + NL + "        }" + NL + "      }" + NL + "    );" + NL;
  protected final String TEXT_346 = NL + "//remember the master for each group, when swoosh + multipass";
  protected final String TEXT_347 = NL;
  protected final String TEXT_348 = "Struct ";
  protected final String TEXT_349 = "_master = null;";
  protected final String TEXT_350 = NL + "  // output data" + NL + "  for (";
  protected final String TEXT_351 = "Struct out_";
  protected final String TEXT_352 = " : groupRows_";
  protected final String TEXT_353 = "){ // C_02" + NL + "  " + NL + "    if (out_";
  protected final String TEXT_354 = " != null){ // C_03";
  protected final String TEXT_355 = NL + "          if(out_";
  protected final String TEXT_356 = ".GRP_SIZE == 1){ // unique rows";
  protected final String TEXT_357 = NL + "              ";
  protected final String TEXT_358 = " = new ";
  protected final String TEXT_359 = "Struct();";
  protected final String TEXT_360 = NL + "                  ";
  protected final String TEXT_361 = ".";
  protected final String TEXT_362 = " = out_";
  protected final String TEXT_363 = ".";
  protected final String TEXT_364 = ";";
  protected final String TEXT_365 = NL + "                  ";
  protected final String TEXT_366 = " = null;";
  protected final String TEXT_367 = NL + "                  ";
  protected final String TEXT_368 = " = null;";
  protected final String TEXT_369 = NL + "          }else{" + NL + "              double groupQuality;" + NL + "              if(out_";
  protected final String TEXT_370 = ".MASTER == true){ // master rows case we get group quality directly" + NL + "                  groupQuality = out_";
  protected final String TEXT_371 = ".GRP_QUALITY;" + NL + "              }else{ // sub rows case we get group quality from gID2GQMap" + NL + "                  groupQuality = gID2GQMap_";
  protected final String TEXT_372 = ".get(String.valueOf(out_";
  protected final String TEXT_373 = ".GID));" + NL + "              }" + NL + "              if(groupQuality >= CONFIDENCE_THRESHOLD_";
  protected final String TEXT_374 = "){";
  protected final String TEXT_375 = NL + "                      ";
  protected final String TEXT_376 = " = null;";
  protected final String TEXT_377 = NL + "                      ";
  protected final String TEXT_378 = " = new ";
  protected final String TEXT_379 = "Struct();";
  protected final String TEXT_380 = NL + "                          ";
  protected final String TEXT_381 = ".";
  protected final String TEXT_382 = " = out_";
  protected final String TEXT_383 = ".";
  protected final String TEXT_384 = ";";
  protected final String TEXT_385 = NL + "                      ";
  protected final String TEXT_386 = " = null;";
  protected final String TEXT_387 = NL + "              }else{";
  protected final String TEXT_388 = NL + "                      ";
  protected final String TEXT_389 = " = null;";
  protected final String TEXT_390 = NL + "                      ";
  protected final String TEXT_391 = " = null;";
  protected final String TEXT_392 = NL + "                  ";
  protected final String TEXT_393 = " = new ";
  protected final String TEXT_394 = "Struct();";
  protected final String TEXT_395 = NL + "                      ";
  protected final String TEXT_396 = ".";
  protected final String TEXT_397 = " = out_";
  protected final String TEXT_398 = ".";
  protected final String TEXT_399 = "; ";
  protected final String TEXT_400 = NL + "              }" + NL + "          }";
  protected final String TEXT_401 = NL;
  protected final String TEXT_402 = NL + "              ";
  protected final String TEXT_403 = " = null;";
  protected final String TEXT_404 = NL + "              ";
  protected final String TEXT_405 = " = null;";
  protected final String TEXT_406 = NL + "              ";
  protected final String TEXT_407 = " = null;";
  protected final String TEXT_408 = NL + NL + "      // all output";
  protected final String TEXT_409 = NL + "      ";
  protected final String TEXT_410 = " = new ";
  protected final String TEXT_411 = "Struct();";
  protected final String TEXT_412 = " ";
  protected final String TEXT_413 = NL + "          ";
  protected final String TEXT_414 = ".";
  protected final String TEXT_415 = " = out_";
  protected final String TEXT_416 = ".";
  protected final String TEXT_417 = ";";

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
     
CodeGeneratorArgument codeGenArgument = (CodeGeneratorArgument) argument;
INode node = (INode)codeGenArgument.getArgument();
org.talend.core.model.process.IProcess process = node.getProcess();
String cid = node.getUniqueName();
cid = cid.replace("_GroupIn", "");
boolean bSeparateOutput = "true".equals(ElementParameterParser.getValue(node, "__SEPARATE_OUTPUT__"));
boolean bOutputDetails = "true".equals(ElementParameterParser.getValue(node, "__OUTPUTDD__"));
final List<org.talend.core.model.process.IContextParameter> params = process.getContextManager().getDefaultContext().getContextParameterList();
boolean bSortOnDisk = "true".equals(ElementParameterParser.getValue(node, "__STORE_ON_DISK__")); 
// incoming connection
INode previousNode=node.getIncomingConnections().get(0).getSource();
List<? extends IConnection> inConns = previousNode.getIncomingConnections();
IConnection inConn = null; 
String connNameMain = null;

if (inConns == null || inConns.size() == 0){
  return "";
} else{
  inConn = inConns.get(0);
  connNameMain = inConn.getName();

}

// schema of tGroup
IMetadataTable table = node.getMetadataList().get(0);
List<IMetadataColumn> columns = null;

// 'originalInputColumnSize' except the exernal columns as follow.
int extSize = 5;
extSize=bOutputDetails ? extSize+1 : extSize;
int originalInputColumnSize=0;
if (table!=null) {
    columns = table.getListColumns();
    originalInputColumnSize=columns.size()-extSize;
}
String [] externalColumnsName=new String []{"GID","GRP_SIZE","MASTER","SCORE","GRP_QUALITY","MATCHING_DISTANCES"};
java.util.List externalColumnList=java.util.Arrays.asList(externalColumnsName);



// input schema
IMetadataTable tableIn = inConn.getMetadataTable();
List<IMetadataColumn> inColumns = null;
if (tableIn!=null) {
    inColumns = tableIn.getListColumns();
}

// get previous node name
INode preNode= inConn.getSource();
String preNodeName=preNode.getComponent().getName();

//used only for double click, which will add tJavaRow before any tmatchgroup
List<? extends IConnection> inConns2=preNode.getIncomingConnections();
String ppreNodeName = "";
if(inConns2!=null && inConns2.size()>0){
    INode ppreNode = preNode.getIncomingConnections().get(0).getSource();
    ppreNodeName = ppreNode.getComponent().getName();
}

//check more than one tmatchGroup exist
INode checkTmatchGroupNode=preNode;
boolean existMoreTmatchGroupNode=false;
while(checkTmatchGroupNode!=null){ 
  String checkNodeName=checkTmatchGroupNode.getComponent().getName();
  if("tMatchGroup".startsWith(checkNodeName)){
      existMoreTmatchGroupNode=true;
      break;
  }
  List<? extends IConnection> incomingList=checkTmatchGroupNode.getIncomingConnections();
  if(incomingList!=null&&incomingList.size()>0){
      checkTmatchGroupNode=incomingList.get(0).getSource();
  }else{
      checkTmatchGroupNode=null;
  }
}

// outing connection
String connNameOut = null, connNameMainOut = null,nextConnName = null,nextConnOutName=null;
String connNameUniqueRowsOut = null, connNameConfidentGroupsOut = null, connNameUncertainGroupsOut = null;
List<? extends IConnection> outConns = node.getOutgoingSortedConnections();

if (outConns != null && outConns.size() > 0){

    connNameOut = outConns.get(0).getName(); 
    //TDQ-13120 get the next node name
    INode nextNode = outConns.get(0).getTarget();
    
    while(!(nextNode.getUniqueName().indexOf("tMatchGroup")>-1)&&nextNode.getOutgoingConnections().size()>0){
        nextNode=nextNode.getOutgoingConnections().get(0).getTarget();
    }
    if(nextNode!=null){
        nextConnName= nextNode.getUniqueName();
        nextConnName = nextConnName.replace("_GroupOut", "");
        nextConnOutName=nextNode.getIncomingConnections().get(0).getName();
    }
    if(connNameMainOut == null){
        connNameMainOut = "mainOutput_" + cid; 
    }    
        for (IConnection connection: outConns){    
            if (connection.isActivate()){
                String connOutCntorName = connection.getConnectorName();
                String connOutFlowName = connection.getName();        
                if ("UNIQUE_ROWS".equals(connOutCntorName)){
                    connNameUniqueRowsOut = connOutFlowName;
                } else if ("CONFIDENT_GROUPS".equals(connOutCntorName)){
                    connNameConfidentGroupsOut = connOutFlowName;
                } else if ("UNCERTAIN_GROUPS".equals(connOutCntorName)){
                    connNameUncertainGroupsOut = connOutFlowName;
                } else if ("FLOW_OUTPUT".equals(connOutCntorName)){
                    connNameMainOut = connOutFlowName;
                }
            }
        }
} else {
    return "";
}

// blocks
List<Map<String, String>> listBlocking = (List<Map<String,String>>)ElementParameterParser.getObjectValue(node, "__BLOCKING_DEFINITION__");
List<String> blockColumns = new java.util.ArrayList<String>(); // to store unduplicated blocking columns
boolean hasAtLeastOneBlock = false;
String sSorted = "";

for (Map<String, String> blocking : listBlocking){
  String sColumnName = blocking.get("INPUT_COLUMN");
  
  if (!hasAtLeastOneBlock){
    hasAtLeastOneBlock = true;
    sSorted = "Sorted";
  }

  if (!blockColumns.contains(sColumnName)){
    blockColumns.add(sColumnName);
  }
}

String _ACCEPTABLE_THRESHOLD = ElementParameterParser.getValue(node, "__INTERVAL__");

//Get the join key parameter
//mzhao TDQ-5981 add multi rule set feature. Get the multi rules from matching component external data.

MatchingData matchData = (MatchingData)node.getExternalNode().getExternalEmfData();


java.util.List<Object> inputTexts=new java.util.ArrayList<Object>();
for(int i=0;i<inColumns.size();i++){  
    IMetadataColumn column=inColumns.get(i);
      String label= column.getLabel();
      //filter the external columns if it is not multiple pass.
      if(externalColumnList.contains(label)&&i>originalInputColumnSize-1){
              continue;
      }

      inputTexts.add(label);
      
}





    stringBuffer.append(TEXT_1);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_2);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_3);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_4);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_5);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_6);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_7);
    
if (matchData != null && matchData.getRuleMatchers().size() > 0) {
    java.util.List<java.util.List<java.util.Map<String, String>>> matchlist= matchData.getRuleListOfMap(columns,params,inputTexts);
        for(java.util.List<java.util.Map<String, String>> list: matchlist){
        
    stringBuffer.append(TEXT_8);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_9);
    
        
            for(java.util.Map<String, String> map: list){
                
    stringBuffer.append(TEXT_10);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_11);
                    
                for(String key: map.keySet()){
                    String value = map.get(key);
                    //TDQ-12860:fix compilation problems: allow expression, like as "globalMap.get("col1")"
                    if ("CONFIDENCE_WEIGHT".equals(key)||"CUSTOM_MATCHER".equals(key) || "INTERVAL_RULE".equals(key) || "RECORD_MATCH_THRESHOLD".equals(key) || "ATTRIBUTE_THRESHOLD".equals(key)){
                      // TDQ-14280: fix compile error for ATTRIBUTE_THRESHOLD
                      if("".equals(value.trim()) && "ATTRIBUTE_THRESHOLD".equals(key)) {
                         value = "1";
                      }
                    
    stringBuffer.append(TEXT_12);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_13);
    stringBuffer.append(key);
    stringBuffer.append(TEXT_14);
    stringBuffer.append(value);
    stringBuffer.append(TEXT_15);
    
                    } else {
                    
    stringBuffer.append(TEXT_16);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_17);
    stringBuffer.append(key);
    stringBuffer.append(TEXT_18);
    stringBuffer.append(value);
    stringBuffer.append(TEXT_19);
    }
                }
    stringBuffer.append(TEXT_20);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_21);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_22);
    
            }
            if(list.size()>1){
        
    stringBuffer.append(TEXT_23);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_24);
    }
            
    stringBuffer.append(TEXT_25);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_26);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_27);
    
        }
        
      //default survivorship
    List<org.talend.designer.tdqmatching.DefaultSurvivorshipDefinitions> defSurs = matchData.getDefaultSurvivorshipDefinitions();
    
    stringBuffer.append(TEXT_28);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_29);
    
    for(org.talend.designer.tdqmatching.DefaultSurvivorshipDefinitions defSur :defSurs){
        
    stringBuffer.append(TEXT_30);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_31);
    
        EMap<String, Object> columnMap= defSur.getColumnMap();
        for(String key:columnMap.keySet()){
            String value=columnMap.get(key)==null ? "" :columnMap.get(key).toString();
            value=value.replace("\"", "");
            
    stringBuffer.append(TEXT_32);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_33);
    stringBuffer.append(key);
    stringBuffer.append(TEXT_34);
    stringBuffer.append(value);
    stringBuffer.append(TEXT_35);
    
        }
        
    stringBuffer.append(TEXT_36);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_37);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_38);
    
    }
    //particular survivorship
    List<org.talend.designer.tdqmatching.ParticularDefaultSurvivorshipDefinitions> parSurs = matchData.getParticularDefaultSurvivorshipDefinitions();
    
    stringBuffer.append(TEXT_39);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_40);
    
    for(org.talend.designer.tdqmatching.ParticularDefaultSurvivorshipDefinitions parSur :parSurs){
        String dataType="";
        
    stringBuffer.append(TEXT_41);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_42);
    
        EMap<String, Object> columnMap= parSur.getColumnMap();
        for(String key:columnMap.keySet()){
            String value=columnMap.get(key)==null ? "" :columnMap.get(key).toString();
            value=value.replace("\"", "");
           
            
    stringBuffer.append(TEXT_43);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_44);
    stringBuffer.append(key);
    stringBuffer.append(TEXT_45);
    stringBuffer.append(value);
    stringBuffer.append(TEXT_46);
    
            if("INPUT_COLUMN".equals(key)){
                for(IMetadataColumn column:inColumns){
                    String label= column.getLabel();
                    if(value.equals(label)){
                        dataType=column.getTalendType();
                    }
                }
            }
        }
        
    stringBuffer.append(TEXT_47);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_48);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_49);
    
    }
    
}


    stringBuffer.append(TEXT_50);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_51);
    
  List<List<Map<String, String>>> allRules = (List<List<Map<String, String>>>)ElementParameterParser.getMultiObjectValue(node, "__JOIN_KEY__");
  if(allRules!=null&&allRules.size()>0){
      List<Map<String, String>> firstMatcherList = allRules.get(0);
      
    stringBuffer.append(TEXT_52);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_53);
    
      for(Map<String,String> columnMap:firstMatcherList){
      
    stringBuffer.append(TEXT_54);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_55);
    
          for(String key: columnMap.keySet()){
              String value=columnMap.get(key)==null?"":columnMap.get(key).toString();
              String newKey=key;
              String newValue=value;
              //replace "INPUT_COLUMN" with "ATTRIBUTE_NAME" and "COLUMN_IDX" to JAVA API.
              if("INPUT_COLUMN".equals(key)){
                  
    stringBuffer.append(TEXT_56);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_57);
    stringBuffer.append(value);
    stringBuffer.append(TEXT_58);
    
                  newKey="COLUMN_IDX";
                  for(int index = 0; index < columns.size(); index++) {
                      IMetadataColumn column = columns.get(index);
                      if(value.equals(column.getLabel())) {
                          newValue=String.valueOf(index);
                          break;
                      }
                    }
              }else if("CUSTOM_MATCHER".equals(key)){
                //only add CUSTOMER_MATCH_CLASS for custom Mating Type.
                  if("custom".equals(columnMap.get("MATCHING_TYPE"))){
                      newKey="CUSTOMER_MATCH_CLASS";
                  }else{
                      continue;
                  }
              }
              // it allows expression like as "globalMap.get("col1")"
              if ("CONFIDENCE_WEIGHT".equals(key)||"CUSTOM_MATCHER".equals(key)){
                  
    stringBuffer.append(TEXT_59);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_60);
    stringBuffer.append(newKey);
    stringBuffer.append(TEXT_61);
    stringBuffer.append(newValue);
    stringBuffer.append(TEXT_62);
    
                  continue;
              }
              //avoid to appear this style """"
              newValue=newValue.replace("\"", "");
              
    stringBuffer.append(TEXT_63);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_64);
    stringBuffer.append(newKey);
    stringBuffer.append(TEXT_65);
    stringBuffer.append(newValue);
    stringBuffer.append(TEXT_66);
    
          }
          
    stringBuffer.append(TEXT_67);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_68);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_69);
    
      }
      //Add the parameter map of algorithm interval of each matching rule, and  algorithm name.
      
    stringBuffer.append(TEXT_70);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_71);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_72);
    stringBuffer.append(_ACCEPTABLE_THRESHOLD);
    stringBuffer.append(TEXT_73);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_74);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_75);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_76);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_77);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_78);
    
      
  }  
 
    stringBuffer.append(TEXT_79);
    
String _CONFIDENCE_THRESHOLD = ElementParameterParser.getValue(node, "__CONFIDENCE_THRESHOLD__");
boolean bStoreOnDisk = "true".equals(ElementParameterParser.getValue(node, "__STORE_ON_DISK__")); 
boolean bSortData = "true".equals(ElementParameterParser.getValue(node, "__SORT_DATA__")); 
boolean bGIDUseString = "true".equals(ElementParameterParser.getValue(node, "__GID_USE_STRING__"));
boolean bMultiPass="true".equals(ElementParameterParser.getValue(node, "__LINK_WITH_PREVIOUS__"));
boolean displayAttLabels = "true".equals(ElementParameterParser.getValue(node, "__DISPLAY_ATTR_LABELS__"));
boolean passOriginal = "true".equals(ElementParameterParser.getValue(node, "__PROPAGATE_ORIGINAL__"));
boolean previousLinkExist=false;
boolean afterLinkExist=false;
String algorithm=ElementParameterParser.getValue(node, "__MATCHING_ALGORITHM__");

/*
 * 
 * case1:bMultiPass=true passOriginal=false
 * check previous node name is tMatchGroup or shawdom job case
 * 
 * case2:bMultiPass=true passOriginal=true
 * case2-1:check previous node is tmatchGroup or shawdom job case
 * case2-2:check next node is tmatchgroup or shawdom tmatchGroup node case
 * case2-3:check second node is shawdom tamtchGroup node case
 * 
 * 
 */
if("TSWOOSH_MATCHER".equals(algorithm)){
    if(bMultiPass&&!passOriginal){
        previousLinkExist=true;
        if(!preNodeName.startsWith("tMatchGroup")&&!(preNodeName.equals("tJavaRow") && ppreNodeName.startsWith("tMatchGroup"))){
            
    stringBuffer.append(TEXT_80);
    stringBuffer.append(preNodeName.equals("tJavaRow")?ppreNodeName:preNodeName);
    stringBuffer.append(TEXT_81);
    
        }
    }else if(bMultiPass&&passOriginal){
        //only one tmatchGroup case need to popup error
        boolean isUseError=false;
        //can not find tmatchGroup in the previous of current node
        if(!preNodeName.startsWith("tMatchGroup")&&!(preNodeName.equals("tJavaRow") && ppreNodeName.startsWith("tMatchGroup"))){
            isUseError=true;
        }else{
            previousLinkExist=true;
        }
        //can not find tmatchGroup in the after of current node
        if (outConns != null && outConns.size() > 0&&isUseError){
            INode nextNode = outConns.get(0).getTarget();
            String nextNodeName=nextNode.getComponent().getName();
            List<? extends IConnection> nnOutConns =nextNode.getOutgoingSortedConnections();
            INode nnextNode =null;
            INode nnnextNode =null;
            INode nnnnextNode =null;
            boolean nextNodeExist=true;
            if(nnOutConns!=null&&nnOutConns.size()>0){
                
                nnextNode = nnOutConns.get(0).getTarget();
            }else{
                nextNodeExist=false;
            }
            
            if(nextNodeExist){
                List<? extends IConnection> nnnOutConns = nnextNode.getOutgoingSortedConnections();
                if(nnnOutConns!=null&&nnnOutConns.size()>0){
                    nnnextNode = nnnOutConns.get(0).getTarget();
                }else{
                    nextNodeExist=false;
                }
            }
            if(nextNodeExist){
                List<? extends IConnection> nnnnOutConns = nnnextNode.getOutgoingSortedConnections();
                if(nnnnOutConns!=null&&nnnnOutConns.size()>0){
                    nnnnextNode = nnnnOutConns.get(0).getTarget();
                }else{
                    nextNodeExist=false;
                }
            }
            String nnextNodeName="";
            String nnnextNodeName="";
            String nnnnextNodeName="";
            if(nextNodeExist){
                nnextNodeName=nnextNode.getComponent().getName();
                nnnextNodeName=nnnextNode.getComponent().getName();
                nnnnextNodeName=nnnnextNode.getComponent().getName();
            }
            boolean nextNodeBeOpen=nextNodeExist&&nextNodeName.startsWith("tJavaRow")&&nnextNodeName.startsWith("tMatchGroup")&&nnnextNodeName.startsWith("tMatchGroup")&&nnnnextNodeName.startsWith("tSocketOutput");
            if(!nextNodeName.startsWith("tMatchGroup")&&!nextNodeName.startsWith("tSocketOutput")&&!(nextNodeBeOpen)){
                
    stringBuffer.append(TEXT_82);
    stringBuffer.append(preNodeName.equals("tJavaRow")?ppreNodeName:preNodeName);
    stringBuffer.append(TEXT_83);
    stringBuffer.append(nextNodeName);
    stringBuffer.append(TEXT_84);
    
            }
            afterLinkExist=true;
        }
    }
}else{
    previousLinkExist=true;
}

if (bStoreOnDisk){

    stringBuffer.append(TEXT_85);
    stringBuffer.append(connNameMain);
    stringBuffer.append(TEXT_86);
    
}

    stringBuffer.append(TEXT_87);
    if(connNameMainOut.startsWith("mainOutput_")){
    stringBuffer.append(TEXT_88);
    stringBuffer.append(connNameOut);
    stringBuffer.append(TEXT_89);
    stringBuffer.append(connNameMainOut);
    stringBuffer.append(TEXT_90);
    }
    stringBuffer.append(TEXT_91);
    stringBuffer.append(connNameOut);
    stringBuffer.append(TEXT_92);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_93);
    stringBuffer.append(TEXT_94);
    stringBuffer.append(connNameOut);
    stringBuffer.append(TEXT_95);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_96);
    stringBuffer.append(TEXT_97);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_98);
    stringBuffer.append(connNameMain);
    stringBuffer.append(TEXT_99);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_100);
    stringBuffer.append(TEXT_101);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_102);
    stringBuffer.append(connNameMain);
    stringBuffer.append(TEXT_103);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_104);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_105);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_106);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_107);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_108);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_109);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_110);
    stringBuffer.append(connNameOut);
    stringBuffer.append(TEXT_111);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_112);
    stringBuffer.append(connNameOut);
    stringBuffer.append(TEXT_113);
    stringBuffer.append(connNameOut);
    stringBuffer.append(TEXT_114);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_115);
    stringBuffer.append(connNameOut);
    stringBuffer.append(TEXT_116);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_117);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_118);
    stringBuffer.append(_CONFIDENCE_THRESHOLD);
    stringBuffer.append(TEXT_119);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_120);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_121);
    stringBuffer.append(connNameMain);
    stringBuffer.append(TEXT_122);
    
    if (bSortOnDisk && hasAtLeastOneBlock){

    stringBuffer.append(TEXT_123);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_124);
    stringBuffer.append(connNameMain);
    stringBuffer.append(TEXT_125);
    }else{
    stringBuffer.append(TEXT_126);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_127);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_128);
    stringBuffer.append(connNameMain);
    stringBuffer.append(TEXT_129);
    }
    stringBuffer.append(TEXT_130);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_131);
    if("Simple VSR Matcher".equals(algorithm)){
    stringBuffer.append(TEXT_132);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_133);
    stringBuffer.append(TEXT_134);
    stringBuffer.append(connNameOut);
    stringBuffer.append(TEXT_135);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_136);
    stringBuffer.append(connNameOut);
    stringBuffer.append(TEXT_137);
     
         
         for(int i=0;i< columns.size();i++){
              IMetadataColumn column=columns.get(i);
              String type=column.getTalendType();
              type=type.replace("id_", "");
        
    stringBuffer.append(TEXT_138);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_139);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_140);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_141);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_142);
    stringBuffer.append(type);
    stringBuffer.append(TEXT_143);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_144);
    }
    stringBuffer.append(TEXT_145);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_146);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_147);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_148);
    if (bSeparateOutput){
    stringBuffer.append(TEXT_149);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_150);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_151);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_152);
    }
    stringBuffer.append(TEXT_153);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_154);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_155);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_156);
    stringBuffer.append(originalInputColumnSize);
    stringBuffer.append(TEXT_157);
    }else{
    stringBuffer.append(TEXT_158);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_159);
    stringBuffer.append(TEXT_160);
    stringBuffer.append(connNameOut);
    stringBuffer.append(TEXT_161);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_162);
    stringBuffer.append(connNameOut);
    stringBuffer.append(TEXT_163);
     
         
         for(int i=0;i< columns.size();i++){
              IMetadataColumn column=columns.get(i);
              String type=column.getTalendType();
              type=type.replace("id_", "");
        
    stringBuffer.append(TEXT_164);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_165);
    
                  if("Date".equals(type)){
                  
    stringBuffer.append(TEXT_166);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_167);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_168);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_169);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_170);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_171);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_172);
    
                  }else if("Double".equals(type)||"Float".equals(type)||"Integer".equals(type)||"Long".equals(type)||"Short".equals(type)){
                  
    stringBuffer.append(TEXT_173);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_174);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_175);
    stringBuffer.append(type);
    stringBuffer.append(TEXT_176);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_177);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_178);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_179);
    
                          if("Integer".equals(type)){
                          
    stringBuffer.append(TEXT_180);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_181);
    
                          }else if("Double".equals(type)){
                              
    stringBuffer.append(TEXT_182);
    
                          }else if("Short".equals(type)){
                              
    stringBuffer.append(TEXT_183);
      
                          }else if("Long".equals(type)){
                              
    stringBuffer.append(TEXT_184);
      
                          }else{
                          
    stringBuffer.append(TEXT_185);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_186);
    
                          }
                          
    stringBuffer.append(TEXT_187);
    
                  }else if("Object".equals(type)){
                      
    stringBuffer.append(TEXT_188);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_189);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_190);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_191);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_192);
    
                  }else if("Character".equals(type)){
                      
    stringBuffer.append(TEXT_193);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_194);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_195);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_196);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_197);
    
                  }else if("BigDecimal".equals(type)){
                      
    stringBuffer.append(TEXT_198);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_199);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_200);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_201);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_202);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_203);
    
                  }else{  
                   
    stringBuffer.append(TEXT_204);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_205);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_206);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_207);
    stringBuffer.append(type);
    stringBuffer.append(TEXT_208);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_209);
    
                  }
                  
    stringBuffer.append(TEXT_210);
    }
    stringBuffer.append(TEXT_211);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_212);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_213);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_214);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_215);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_216);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_217);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_218);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_219);
    if(bMultiPass){
    stringBuffer.append(TEXT_220);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_221);
    stringBuffer.append(originalInputColumnSize-1);
    stringBuffer.append(TEXT_222);
    }else{
    stringBuffer.append(TEXT_223);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_224);
    stringBuffer.append(originalInputColumnSize);
    stringBuffer.append(TEXT_225);
    }
    stringBuffer.append(TEXT_226);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_227);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_228);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_229);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_230);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_231);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_232);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_233);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_234);
    
for(Map<String,String> surShipMap:(List<Map<String, String>>)ElementParameterParser.getMultiObjectValue(node, "__SURVIVORSHIP__")){
    
    stringBuffer.append(TEXT_235);
    
    for(String key:surShipMap.keySet()){
        
    stringBuffer.append(TEXT_236);
    stringBuffer.append(key);
    stringBuffer.append(TEXT_237);
    stringBuffer.append(surShipMap.get(key));
    stringBuffer.append(TEXT_238);
    
    }
    
    stringBuffer.append(TEXT_239);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_240);
    
}

List<List<Map<String, String>>> allRules2 = (List<List<Map<String, String>>>)ElementParameterParser.getMultiObjectValue(node, "__JOIN_KEY__");

for(List<Map<String,String>> matchRuleList:allRules2){
    
    stringBuffer.append(TEXT_241);
    
    for(Map<String,String> joinKeyMap:matchRuleList){
        
    stringBuffer.append(TEXT_242);
    
        for(String key: joinKeyMap.keySet()){
            
    stringBuffer.append(TEXT_243);
    stringBuffer.append(key);
    stringBuffer.append(TEXT_244);
    stringBuffer.append(joinKeyMap.get(key));
    stringBuffer.append(TEXT_245);
    
        }
        
    stringBuffer.append(TEXT_246);
    
    }
    
    stringBuffer.append(TEXT_247);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_248);
    
}
for(int i=0;i< columns.size();i++){
    IMetadataColumn column=columns.get(i);
    
    stringBuffer.append(TEXT_249);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_250);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_251);
    stringBuffer.append(column.getTalendType());
    stringBuffer.append(TEXT_252);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_253);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_254);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_255);
    
    if("id_Date".equals(column.getTalendType())){
                String pattern = column.getPattern();
                if (pattern==null || pattern.equals("")) {
                    pattern = "yyyy-MM-dd HH:mm:ss.SSS";
                }
    
    stringBuffer.append(TEXT_256);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_257);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_258);
    stringBuffer.append(pattern);
    stringBuffer.append(TEXT_259);
    
    }
}

    stringBuffer.append(TEXT_260);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_261);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_262);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_263);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_264);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_265);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_266);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_267);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_268);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_269);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_270);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_271);
    }
    stringBuffer.append(TEXT_272);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_273);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_274);
    stringBuffer.append(bOutputDetails);
    stringBuffer.append(TEXT_275);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_276);
    stringBuffer.append(_ACCEPTABLE_THRESHOLD);
    stringBuffer.append(TEXT_277);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_278);
    stringBuffer.append(bMultiPass);
    stringBuffer.append(TEXT_279);
    stringBuffer.append(previousLinkExist);
    stringBuffer.append(TEXT_280);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_281);
    stringBuffer.append(displayAttLabels);
    stringBuffer.append(TEXT_282);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_283);
    stringBuffer.append(bGIDUseString);
    stringBuffer.append(TEXT_284);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_285);
    stringBuffer.append(passOriginal);
    stringBuffer.append(TEXT_286);
    stringBuffer.append(bMultiPass);
    stringBuffer.append(TEXT_287);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_288);
    stringBuffer.append(bSortOnDisk);
    stringBuffer.append(TEXT_289);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_290);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_291);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_292);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_293);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_294);
    
  if (hasAtLeastOneBlock){
  
    stringBuffer.append(TEXT_295);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_296);
    stringBuffer.append(connNameMain);
    stringBuffer.append(TEXT_297);
        
      if(bSortOnDisk){
      
    stringBuffer.append(TEXT_298);
    stringBuffer.append(connNameMain);
    stringBuffer.append(TEXT_299);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_300);
    stringBuffer.append(connNameMain);
    stringBuffer.append(TEXT_301);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_302);
    
      }else{
      
    stringBuffer.append(TEXT_303);
    stringBuffer.append(connNameMain);
    stringBuffer.append(TEXT_304);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_305);
    
      }
      for (String block : blockColumns){
      
    stringBuffer.append(TEXT_306);
    stringBuffer.append(connNameMain);
    stringBuffer.append(TEXT_307);
    stringBuffer.append(block);
    stringBuffer.append(TEXT_308);
    stringBuffer.append(connNameMain);
    stringBuffer.append(TEXT_309);
    stringBuffer.append(block);
    stringBuffer.append(TEXT_310);
    stringBuffer.append(connNameMain);
    stringBuffer.append(TEXT_311);
    
      }
  }
  
    stringBuffer.append(TEXT_312);
    stringBuffer.append(connNameMain);
    stringBuffer.append(TEXT_313);
    stringBuffer.append(connNameMain);
    stringBuffer.append(TEXT_314);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_315);
    if( !(bSortData && bMultiPass)){ //TDQ-9655 if the second tMatchGroup select sort and mulyipass, not clear 
    stringBuffer.append(TEXT_316);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_317);
    }
    stringBuffer.append(TEXT_318);
    if("Simple VSR Matcher".equals(algorithm)){
    stringBuffer.append(TEXT_319);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_320);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_321);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_322);
    }
    stringBuffer.append(TEXT_323);
    stringBuffer.append(connNameMain);
    stringBuffer.append(TEXT_324);
    stringBuffer.append(connNameMain);
    stringBuffer.append(TEXT_325);
    stringBuffer.append(connNameMain);
    stringBuffer.append(TEXT_326);
    
    for(int i=0;i<inColumns.size();i++){  
        IMetadataColumn column=inColumns.get(i);
          String label= column.getLabel();
          //filter the external columns if it is not multiple pass.
          if(!bMultiPass && externalColumnList.contains(label)&&i>originalInputColumnSize-1){
                  continue;
          }
    
    stringBuffer.append(TEXT_327);
    stringBuffer.append(connNameMain);
    stringBuffer.append(TEXT_328);
    stringBuffer.append(label);
    stringBuffer.append(TEXT_329);
    }
    stringBuffer.append(TEXT_330);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_331);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_332);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_333);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_334);
    
  if (bSortData && !bMultiPass){ // order by gid, master
  
    stringBuffer.append(TEXT_335);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_336);
    stringBuffer.append(connNameOut);
    stringBuffer.append(TEXT_337);
    stringBuffer.append(connNameOut);
    stringBuffer.append(TEXT_338);
    stringBuffer.append(connNameOut);
    stringBuffer.append(TEXT_339);
    }
    stringBuffer.append(TEXT_340);
    if( bSortData && bMultiPass){  
    stringBuffer.append(TEXT_341);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_342);
    stringBuffer.append(connNameOut);
    stringBuffer.append(TEXT_343);
    stringBuffer.append(connNameOut);
    stringBuffer.append(TEXT_344);
    stringBuffer.append(connNameOut);
    stringBuffer.append(TEXT_345);
    } 
if(nextConnName!=null && (nextConnName.indexOf("tMatchGroup")>-1)){
  // TDQ-13980: fix when double click tMatchGroup, get complier error.  the size when double click is 0 and run job the size bigger than 0
  if(outConns.get(0).getTarget().getOutgoingSortedConnections().get(0).getTarget().getOutgoingConnections().size()>0) {

    stringBuffer.append(TEXT_346);
    stringBuffer.append(TEXT_347);
    stringBuffer.append(nextConnName);
    stringBuffer.append(TEXT_348);
    stringBuffer.append(nextConnOutName==null?connNameOut:nextConnOutName);
    stringBuffer.append(TEXT_349);
     }
} 

    stringBuffer.append(TEXT_350);
    stringBuffer.append(connNameOut);
    stringBuffer.append(TEXT_351);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_352);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_353);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_354);
    if (bSeparateOutput){
    stringBuffer.append(TEXT_355);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_356);
    if (connNameUniqueRowsOut != null){
    stringBuffer.append(TEXT_357);
    stringBuffer.append(connNameUniqueRowsOut);
    stringBuffer.append(TEXT_358);
    stringBuffer.append(connNameUniqueRowsOut);
    stringBuffer.append(TEXT_359);
    for (IMetadataColumn column : table.getListColumns()){
    stringBuffer.append(TEXT_360);
    stringBuffer.append(connNameUniqueRowsOut);
    stringBuffer.append(TEXT_361);
    stringBuffer.append(column);
    stringBuffer.append(TEXT_362);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_363);
    stringBuffer.append(column);
    stringBuffer.append(TEXT_364);
    }
              }
              if (connNameConfidentGroupsOut != null){
    stringBuffer.append(TEXT_365);
    stringBuffer.append(connNameConfidentGroupsOut);
    stringBuffer.append(TEXT_366);
    }
              if (connNameUncertainGroupsOut != null){
    stringBuffer.append(TEXT_367);
    stringBuffer.append(connNameUncertainGroupsOut);
    stringBuffer.append(TEXT_368);
    }
    stringBuffer.append(TEXT_369);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_370);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_371);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_372);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_373);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_374);
    if (connNameUniqueRowsOut != null){
    stringBuffer.append(TEXT_375);
    stringBuffer.append(connNameUniqueRowsOut);
    stringBuffer.append(TEXT_376);
    }
                  if (connNameConfidentGroupsOut != null){
    stringBuffer.append(TEXT_377);
    stringBuffer.append(connNameConfidentGroupsOut);
    stringBuffer.append(TEXT_378);
    stringBuffer.append(connNameConfidentGroupsOut);
    stringBuffer.append(TEXT_379);
    for (IMetadataColumn column : table.getListColumns()){
    stringBuffer.append(TEXT_380);
    stringBuffer.append(connNameConfidentGroupsOut);
    stringBuffer.append(TEXT_381);
    stringBuffer.append(column);
    stringBuffer.append(TEXT_382);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_383);
    stringBuffer.append(column);
    stringBuffer.append(TEXT_384);
    }
                  }
                  if (connNameUncertainGroupsOut != null){
    stringBuffer.append(TEXT_385);
    stringBuffer.append(connNameUncertainGroupsOut);
    stringBuffer.append(TEXT_386);
    }
    stringBuffer.append(TEXT_387);
    if (connNameUniqueRowsOut != null){
    stringBuffer.append(TEXT_388);
    stringBuffer.append(connNameUniqueRowsOut);
    stringBuffer.append(TEXT_389);
    }
                  if (connNameConfidentGroupsOut != null){
    stringBuffer.append(TEXT_390);
    stringBuffer.append(connNameConfidentGroupsOut);
    stringBuffer.append(TEXT_391);
    }
                  if (connNameUncertainGroupsOut != null){
    stringBuffer.append(TEXT_392);
    stringBuffer.append(connNameUncertainGroupsOut);
    stringBuffer.append(TEXT_393);
    stringBuffer.append(connNameUncertainGroupsOut);
    stringBuffer.append(TEXT_394);
    for (IMetadataColumn column : table.getListColumns()){
    stringBuffer.append(TEXT_395);
    stringBuffer.append(connNameUncertainGroupsOut);
    stringBuffer.append(TEXT_396);
    stringBuffer.append(column);
    stringBuffer.append(TEXT_397);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_398);
    stringBuffer.append(column);
    stringBuffer.append(TEXT_399);
    }
                  }
    stringBuffer.append(TEXT_400);
    }else{
    stringBuffer.append(TEXT_401);
    if (connNameUniqueRowsOut != null){
    stringBuffer.append(TEXT_402);
    stringBuffer.append(connNameUniqueRowsOut);
    stringBuffer.append(TEXT_403);
    }
          if (connNameConfidentGroupsOut != null){
    stringBuffer.append(TEXT_404);
    stringBuffer.append(connNameConfidentGroupsOut);
    stringBuffer.append(TEXT_405);
    }
          if (connNameUncertainGroupsOut != null){
    stringBuffer.append(TEXT_406);
    stringBuffer.append(connNameUncertainGroupsOut);
    stringBuffer.append(TEXT_407);
    }
      }
    stringBuffer.append(TEXT_408);
    stringBuffer.append(TEXT_409);
    stringBuffer.append(connNameMainOut);
    stringBuffer.append(TEXT_410);
    stringBuffer.append(connNameOut);
    stringBuffer.append(TEXT_411);
    for(IMetadataColumn column : table.getListColumns()){
    stringBuffer.append(TEXT_412);
    stringBuffer.append(TEXT_413);
    stringBuffer.append(connNameMainOut);
    stringBuffer.append(TEXT_414);
    stringBuffer.append(column);
    stringBuffer.append(TEXT_415);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_416);
    stringBuffer.append(column);
    stringBuffer.append(TEXT_417);
    }
        
      List<BlockCode> blockCodes = new java.util.ArrayList<BlockCode>();
    blockCodes.add(new BlockCode("C_03"));
  blockCodes.add(new BlockCode("C_02"));
  if( !(bSortData && bMultiPass)){  // TDQ-9655 if the second tMatchGroup select sort and mulyipass, do not need C_01 here
    blockCodes.add(new BlockCode("C_01"));
  }
((org.talend.core.model.process.AbstractNode) node).setBlocksCodeToClose(blockCodes);

    return stringBuffer.toString();
  }
}
