package org.talend.designer.codegen.translators.processing;

import java.util.Set;
import org.talend.transform.component.thmap.MapperComponent;
import org.talend.core.model.metadata.IMetadataTable;
import org.talend.core.model.metadata.IMetadataColumn;
import org.talend.core.model.metadata.types.JavaTypesManager;
import org.talend.core.model.process.IConnection;
import org.talend.core.model.process.IConnectionCategory;
import org.talend.core.model.process.INode;
import org.talend.core.model.process.ElementParameterParser;
import org.talend.core.model.utils.TalendTextUtils;
import org.talend.designer.codegen.config.CodeGeneratorArgument;

public class THMapMainJava
{
  protected static String nl;
  public static synchronized THMapMainJava create(String lineSeparator)
  {
    nl = lineSeparator;
    THMapMainJava result = new THMapMainJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "    //THMAP_MAIN";
  protected final String TEXT_2 = NL + "    " + NL + "    list_";
  protected final String TEXT_3 = " = new java.util.ArrayList();";
  protected final String TEXT_4 = NL + "    java.util.List<java.util.Map<String, Object>> list_";
  protected final String TEXT_5 = "_";
  protected final String TEXT_6 = " = list_";
  protected final String TEXT_7 = ";";
  protected final String TEXT_8 = NL + "    java.util.Map<String, Object> map_";
  protected final String TEXT_9 = "_";
  protected final String TEXT_10 = " = new java.util.HashMap<>();";
  protected final String TEXT_11 = NL + "    map_";
  protected final String TEXT_12 = "_";
  protected final String TEXT_13 = ".put(\"";
  protected final String TEXT_14 = "\", ";
  protected final String TEXT_15 = ".";
  protected final String TEXT_16 = ");";
  protected final String TEXT_17 = NL + "    list_";
  protected final String TEXT_18 = "_";
  protected final String TEXT_19 = ".add(map_";
  protected final String TEXT_20 = "_";
  protected final String TEXT_21 = ");";
  protected final String TEXT_22 = NL;
  protected final String TEXT_23 = NL + "    String executorId_";
  protected final String TEXT_24 = " = Thread.currentThread().getId()+\"_";
  protected final String TEXT_25 = "_\"+\"mapExecutor\";" + NL + "    org.talend.transform.runtime.common.MapExecutor mapExec_";
  protected final String TEXT_26 = " = (org.talend.transform.runtime.common.MapExecutor) ";
  protected final String TEXT_27 = ".this.globalMap.get(executorId_";
  protected final String TEXT_28 = ");" + NL + "    if (mapExec_";
  protected final String TEXT_29 = " == null) {" + NL + "      mapExec_";
  protected final String TEXT_30 = " = org.talend.transform.runtime.common.MapExecutorFactory.get();";
  protected final String TEXT_31 = NL + "      ";
  protected final String TEXT_32 = ".this.globalMap.put(executorId_";
  protected final String TEXT_33 = ", mapExec_";
  protected final String TEXT_34 = ");" + NL + "  " + NL + "      mapExec_";
  protected final String TEXT_35 = ".setLoggingLevel(\"";
  protected final String TEXT_36 = "\");" + NL + "      mapExec_";
  protected final String TEXT_37 = ".setExceptionThreshold(org.talend.transform.runtime.common.SeverityLevel.HIGHEST);" + NL + "    }" + NL + "" + NL + "    java.util.Map<String, Object> ecProps_";
  protected final String TEXT_38 = " = new java.util.HashMap<String, Object>();";
  protected final String TEXT_39 = NL + "    ";
  protected final String TEXT_40 = ".synchronizeContext();" + NL + "" + NL + "    java.util.Enumeration<?> propertyNames_";
  protected final String TEXT_41 = " = ";
  protected final String TEXT_42 = ".propertyNames();" + NL + "    while (propertyNames_";
  protected final String TEXT_43 = ".hasMoreElements()) {" + NL + "        String key_";
  protected final String TEXT_44 = " = (String) propertyNames_";
  protected final String TEXT_45 = ".nextElement();" + NL + "        Object value_";
  protected final String TEXT_46 = " = (Object) ";
  protected final String TEXT_47 = ".get(key_";
  protected final String TEXT_48 = ");" + NL + "        ecProps_";
  protected final String TEXT_49 = ".put(\"context.\"+key_";
  protected final String TEXT_50 = ", value_";
  protected final String TEXT_51 = ");" + NL + "    }" + NL + "     mapExec_";
  protected final String TEXT_52 = ".setExecutionProperties(\"";
  protected final String TEXT_53 = "\", ecProps_";
  protected final String TEXT_54 = ");" + NL;
  protected final String TEXT_55 = NL + "    String mapPath = (String) context.get(\"";
  protected final String TEXT_56 = "\");" + NL + "    mapExec_";
  protected final String TEXT_57 = ".setUp(\"";
  protected final String TEXT_58 = "\", new org.talend.transform.runtime.common.MapLocation(\"";
  protected final String TEXT_59 = "\", mapPath));";
  protected final String TEXT_60 = NL + "    mapExec_";
  protected final String TEXT_61 = ".setUp(\"";
  protected final String TEXT_62 = "\", new org.talend.transform.runtime.common.MapLocation(\"";
  protected final String TEXT_63 = "\", \"";
  protected final String TEXT_64 = "\"));";
  protected final String TEXT_65 = NL + "    java.util.Map<String, javax.xml.transform.Source> sources_";
  protected final String TEXT_66 = " = new java.util.LinkedHashMap<>();";
  protected final String TEXT_67 = NL + "    //Setting one source only" + NL + "    mapExec_";
  protected final String TEXT_68 = ".setObjectSource(\"";
  protected final String TEXT_69 = "\", list_";
  protected final String TEXT_70 = ");";
  protected final String TEXT_71 = NL + "   //Source for Connection:  ";
  protected final String TEXT_72 = NL + "   javax.xml.transform.Source connectionSource_";
  protected final String TEXT_73 = "_";
  protected final String TEXT_74 = " = null;";
  protected final String TEXT_75 = NL + "   java.util.Map<";
  protected final String TEXT_76 = "Struct, ";
  protected final String TEXT_77 = "Struct> tHash_";
  protected final String TEXT_78 = " = (java.util.Map<";
  protected final String TEXT_79 = "Struct, ";
  protected final String TEXT_80 = "Struct>)globalMap.get(\"tHash_";
  protected final String TEXT_81 = "\");";
  protected final String TEXT_82 = NL + "   ";
  protected final String TEXT_83 = "Struct ";
  protected final String TEXT_84 = " = null;" + NL + "   if (tHash_";
  protected final String TEXT_85 = ".values()!=null" + NL + "      && tHash_";
  protected final String TEXT_86 = ".values().iterator().hasNext())";
  protected final String TEXT_87 = NL + "      ";
  protected final String TEXT_88 = " = tHash_";
  protected final String TEXT_89 = ".values().iterator().next();";
  protected final String TEXT_90 = NL + "    //Source for ";
  protected final String TEXT_91 = " ";
  protected final String TEXT_92 = NL + "    javax.xml.transform.stream.StreamSource ss_";
  protected final String TEXT_93 = "_";
  protected final String TEXT_94 = " = new javax.xml.transform.stream.StreamSource();" + NL + "    if (";
  protected final String TEXT_95 = "!=null&&";
  protected final String TEXT_96 = ".";
  protected final String TEXT_97 = "!=null) {" + NL + "        ss_";
  protected final String TEXT_98 = "_";
  protected final String TEXT_99 = ".setReader(new java.io.StringReader(";
  protected final String TEXT_100 = ".";
  protected final String TEXT_101 = "));" + NL + "    } else {" + NL + "        ss_";
  protected final String TEXT_102 = "_";
  protected final String TEXT_103 = ".setReader(new java.io.StringReader(\"\"));" + NL + "    }" + NL + "    connectionSource_";
  protected final String TEXT_104 = "_";
  protected final String TEXT_105 = " = ss_";
  protected final String TEXT_106 = "_";
  protected final String TEXT_107 = ";";
  protected final String TEXT_108 = NL + "    org.dom4j.io.DocumentSource ds_";
  protected final String TEXT_109 = "_";
  protected final String TEXT_110 = " = null;" + NL + "    if (";
  protected final String TEXT_111 = "!=null&&";
  protected final String TEXT_112 = ".";
  protected final String TEXT_113 = "!=null){" + NL + "        ds_";
  protected final String TEXT_114 = "_";
  protected final String TEXT_115 = " = new org.dom4j.io.DocumentSource(((routines.system.Document)";
  protected final String TEXT_116 = ".";
  protected final String TEXT_117 = ").getDocument());" + NL + "    } else {" + NL + "        ds_";
  protected final String TEXT_118 = "_";
  protected final String TEXT_119 = " = new org.dom4j.io.DocumentSource(new routines.system.Document().getDocument());" + NL + "    }" + NL + "    connectionSource_";
  protected final String TEXT_120 = "_";
  protected final String TEXT_121 = " = ds_";
  protected final String TEXT_122 = "_";
  protected final String TEXT_123 = ";";
  protected final String TEXT_124 = NL + "    javax.xml.transform.stream.StreamSource ss_";
  protected final String TEXT_125 = "_";
  protected final String TEXT_126 = " = new javax.xml.transform.stream.StreamSource();" + NL + "    ss_";
  protected final String TEXT_127 = "_";
  protected final String TEXT_128 = ".setInputStream(new java.io.ByteArrayInputStream((byte[])";
  protected final String TEXT_129 = ".";
  protected final String TEXT_130 = "));" + NL + "    connectionSource_";
  protected final String TEXT_131 = "_";
  protected final String TEXT_132 = " = ss_";
  protected final String TEXT_133 = "_";
  protected final String TEXT_134 = ";";
  protected final String TEXT_135 = NL + "    if (";
  protected final String TEXT_136 = ".";
  protected final String TEXT_137 = " instanceof java.io.InputStream) {" + NL + "         javax.xml.transform.stream.StreamSource ss_";
  protected final String TEXT_138 = "_";
  protected final String TEXT_139 = " = new javax.xml.transform.stream.StreamSource();" + NL + "         ss_";
  protected final String TEXT_140 = "_";
  protected final String TEXT_141 = ".setInputStream((java.io.InputStream)";
  protected final String TEXT_142 = ".";
  protected final String TEXT_143 = ");" + NL + "         connectionSource_";
  protected final String TEXT_144 = "_";
  protected final String TEXT_145 = " = ss_";
  protected final String TEXT_146 = "_";
  protected final String TEXT_147 = ";" + NL + "    } else if (";
  protected final String TEXT_148 = ".";
  protected final String TEXT_149 = " instanceof String) {" + NL + "        javax.xml.transform.stream.StreamSource ss_";
  protected final String TEXT_150 = "_";
  protected final String TEXT_151 = " = new javax.xml.transform.stream.StreamSource();" + NL + "        ss_";
  protected final String TEXT_152 = "_";
  protected final String TEXT_153 = ".setReader(new java.io.StringReader((String)";
  protected final String TEXT_154 = ".";
  protected final String TEXT_155 = "));" + NL + "        connectionSource_";
  protected final String TEXT_156 = "_";
  protected final String TEXT_157 = " = ss_";
  protected final String TEXT_158 = "_";
  protected final String TEXT_159 = ";" + NL + "    } else if (";
  protected final String TEXT_160 = ".";
  protected final String TEXT_161 = " instanceof byte[]) {" + NL + "        javax.xml.transform.stream.StreamSource ss_";
  protected final String TEXT_162 = "_";
  protected final String TEXT_163 = " = new javax.xml.transform.stream.StreamSource();" + NL + "        ss_";
  protected final String TEXT_164 = "_";
  protected final String TEXT_165 = ".setInputStream(new java.io.ByteArrayInputStream((byte[])";
  protected final String TEXT_166 = ".";
  protected final String TEXT_167 = "));" + NL + "        connectionSource_";
  protected final String TEXT_168 = "_";
  protected final String TEXT_169 = " = ss_";
  protected final String TEXT_170 = "_";
  protected final String TEXT_171 = ";" + NL + "    } else if (";
  protected final String TEXT_172 = ".";
  protected final String TEXT_173 = " instanceof routines.system.Document) {" + NL + "        org.dom4j.io.DocumentSource ds_";
  protected final String TEXT_174 = "_";
  protected final String TEXT_175 = " = new org.dom4j.io.DocumentSource(((routines.system.Document)";
  protected final String TEXT_176 = ".";
  protected final String TEXT_177 = ").getDocument());" + NL + "        connectionSource_";
  protected final String TEXT_178 = "_";
  protected final String TEXT_179 = " =ds_";
  protected final String TEXT_180 = "_";
  protected final String TEXT_181 = ";" + NL + "    }";
  protected final String TEXT_182 = NL + "    connectionSource_";
  protected final String TEXT_183 = "_";
  protected final String TEXT_184 = " = new org.talend.transform.io.ObjectSource(list_";
  protected final String TEXT_185 = ");";
  protected final String TEXT_186 = NL + "   java.util.Map<";
  protected final String TEXT_187 = "Struct, ";
  protected final String TEXT_188 = "Struct> tHash_";
  protected final String TEXT_189 = " = (java.util.Map<";
  protected final String TEXT_190 = "Struct, ";
  protected final String TEXT_191 = "Struct>)globalMap.get(\"tHash_";
  protected final String TEXT_192 = "\");" + NL + "   java.util.List<java.util.Map<String, Object>> list_";
  protected final String TEXT_193 = "_";
  protected final String TEXT_194 = " = new java.util.ArrayList<>();" + NL + "   for (";
  protected final String TEXT_195 = "Struct connStruct : tHash_";
  protected final String TEXT_196 = ".values()) {";
  protected final String TEXT_197 = NL + "     ";
  protected final String TEXT_198 = "Struct ";
  protected final String TEXT_199 = " = connStruct;";
  protected final String TEXT_200 = NL + "    java.util.Map<String, Object> map_";
  protected final String TEXT_201 = "_";
  protected final String TEXT_202 = " = new java.util.HashMap<>();";
  protected final String TEXT_203 = NL + "    map_";
  protected final String TEXT_204 = "_";
  protected final String TEXT_205 = ".put(\"";
  protected final String TEXT_206 = "\", ";
  protected final String TEXT_207 = ".";
  protected final String TEXT_208 = ");";
  protected final String TEXT_209 = NL + "    list_";
  protected final String TEXT_210 = "_";
  protected final String TEXT_211 = ".add(map_";
  protected final String TEXT_212 = "_";
  protected final String TEXT_213 = ");" + NL + "   }" + NL + "   connectionSource_";
  protected final String TEXT_214 = "_";
  protected final String TEXT_215 = " = new org.talend.transform.io.ObjectSource(list_";
  protected final String TEXT_216 = "_";
  protected final String TEXT_217 = ");";
  protected final String TEXT_218 = NL + "    sources_";
  protected final String TEXT_219 = ".put(\"";
  protected final String TEXT_220 = "\", connectionSource_";
  protected final String TEXT_221 = "_";
  protected final String TEXT_222 = ");";
  protected final String TEXT_223 = NL + "   //Setting a single connection of payload-type" + NL + "    mapExec_";
  protected final String TEXT_224 = ".setSource(\"";
  protected final String TEXT_225 = "\", connectionSource_";
  protected final String TEXT_226 = "_";
  protected final String TEXT_227 = ");";
  protected final String TEXT_228 = NL + "   //Setting multiple sources" + NL + "    mapExec_";
  protected final String TEXT_229 = ".setSources(\"";
  protected final String TEXT_230 = "\", sources_";
  protected final String TEXT_231 = ");";
  protected final String TEXT_232 = NL;
  protected final String TEXT_233 = NL;
  protected final String TEXT_234 = "    " + NL + "    java.util.Map<String, javax.xml.transform.Result> output_";
  protected final String TEXT_235 = " = new java.util.HashMap<>();";
  protected final String TEXT_236 = "  " + NL + "    javax.xml.transform.Result output_";
  protected final String TEXT_237 = " = null;";
  protected final String TEXT_238 = "  " + NL + "   //Result for thMap Connection \"";
  protected final String TEXT_239 = "\" : ObjectResult" + NL + "\t java.util.List<java.util.Map<String, Object>> outList_";
  protected final String TEXT_240 = "_";
  protected final String TEXT_241 = " = new java.util.ArrayList<>();" + NL + "\t org.talend.transform.io.ObjectResult result_";
  protected final String TEXT_242 = "_";
  protected final String TEXT_243 = " = new org.talend.transform.io.ObjectResult(outList_";
  protected final String TEXT_244 = "_";
  protected final String TEXT_245 = ");";
  protected final String TEXT_246 = NL + "\t//Result is of single column string" + NL + "    javax.xml.transform.stream.StreamResult result_";
  protected final String TEXT_247 = "_";
  protected final String TEXT_248 = " = new javax.xml.transform.stream.StreamResult();" + NL + "    java.io.StringWriter sw_";
  protected final String TEXT_249 = "_";
  protected final String TEXT_250 = " = new java.io.StringWriter();" + NL + "    ((javax.xml.transform.stream.StreamResult)result_";
  protected final String TEXT_251 = "_";
  protected final String TEXT_252 = ").setWriter(sw_";
  protected final String TEXT_253 = "_";
  protected final String TEXT_254 = ");";
  protected final String TEXT_255 = NL + "\t//Result is of single column byte array" + NL + "    javax.xml.transform.stream.StreamResult result_";
  protected final String TEXT_256 = "_";
  protected final String TEXT_257 = " = new javax.xml.transform.stream.StreamResult();" + NL + "    java.io.ByteArrayOutputStream bas_";
  protected final String TEXT_258 = "_";
  protected final String TEXT_259 = " = new java.io.ByteArrayOutputStream();" + NL + "    ((javax.xml.transform.stream.StreamResult)result_";
  protected final String TEXT_260 = "_";
  protected final String TEXT_261 = ").setOutputStream(bas_";
  protected final String TEXT_262 = "_";
  protected final String TEXT_263 = ");    ";
  protected final String TEXT_264 = NL + "    //Result is of single column document" + NL + "    org.dom4j.io.DocumentResult result_";
  protected final String TEXT_265 = "_";
  protected final String TEXT_266 = " = new org.dom4j.io.DocumentResult();";
  protected final String TEXT_267 = NL + "   //Add to map (";
  protected final String TEXT_268 = " record_path, result) " + NL + "\toutput_";
  protected final String TEXT_269 = ".put(\"";
  protected final String TEXT_270 = "\",result_";
  protected final String TEXT_271 = "_";
  protected final String TEXT_272 = ");";
  protected final String TEXT_273 = NL + "   //Set the single result" + NL + "   output_";
  protected final String TEXT_274 = " = result_";
  protected final String TEXT_275 = "_";
  protected final String TEXT_276 = ";" + NL + "   mapExec_";
  protected final String TEXT_277 = ".setResult(\"";
  protected final String TEXT_278 = "\",output_";
  protected final String TEXT_279 = ");";
  protected final String TEXT_280 = "  " + NL + "   //Setting multiple/wrapper results" + NL + "\tmapExec_";
  protected final String TEXT_281 = ".setResults(\"";
  protected final String TEXT_282 = "\", output_";
  protected final String TEXT_283 = "); ";
  protected final String TEXT_284 = NL + "    ";
  protected final String TEXT_285 = ".this.globalMap.put(Thread.currentThread().getId()+\"_";
  protected final String TEXT_286 = "_\"+\"outputResult\", output_";
  protected final String TEXT_287 = ");";
  protected final String TEXT_288 = NL;
  protected final String TEXT_289 = NL + "    // Runs the map when the InputStream is accessed";
  protected final String TEXT_290 = NL + "    ";
  protected final String TEXT_291 = ".this.globalMap.put(Thread" + NL + "            .currentThread().getId()+\"_";
  protected final String TEXT_292 = "_\"+\"outputResult\", mapExec_";
  protected final String TEXT_293 = ".executeToStream(\"";
  protected final String TEXT_294 = "\"));";
  protected final String TEXT_295 = NL + "    org.talend.transform.runtime.common.MapExecutionStatus results_";
  protected final String TEXT_296 = " = mapExec_";
  protected final String TEXT_297 = ".execute(\"";
  protected final String TEXT_298 = "\");" + NL + "    mapExec_";
  protected final String TEXT_299 = ".freeExecutionResources(\"";
  protected final String TEXT_300 = "\");";
  protected final String TEXT_301 = NL + "    ";
  protected final String TEXT_302 = ".this.globalMap.put(\"";
  protected final String TEXT_303 = "_\"+\"EXECUTION_STATUS\",results_";
  protected final String TEXT_304 = ");";
  protected final String TEXT_305 = NL + "    ";
  protected final String TEXT_306 = ".this.globalMap.put(\"";
  protected final String TEXT_307 = "_\"+\"EXECUTION_SEVERITY\",results_";
  protected final String TEXT_308 = ".getHighestSeverityLevel().getNumValue());" + NL + "    if (results_";
  protected final String TEXT_309 = ".getHighestSeverityLevel().isGreaterOrEqualsTo(";
  protected final String TEXT_310 = ")) {" + NL + "        throw new TalendException(new java.lang.Exception(String.valueOf(results_";
  protected final String TEXT_311 = ")),currentComponent,globalMap);" + NL + "    }";
  protected final String TEXT_312 = NL + NL + "    if (results_";
  protected final String TEXT_313 = ".getHighestSeverityLevel().isGreaterThan(org.talend.transform.runtime.common.SeverityLevel.INFO)) {" + NL + "        System.err.println(results_";
  protected final String TEXT_314 = ");" + NL + "    }";
  protected final String TEXT_315 = NL;
  protected final String TEXT_316 = NL;
  protected final String TEXT_317 = "    java.util.List<java.lang.Object> rows_";
  protected final String TEXT_318 = " = new java.util.ArrayList<>();";
  protected final String TEXT_319 = NL + "    //Result for multiple connections" + NL + "    java.util.Map<java.lang.String, javax.xml.transform.Result> resultsMapList_";
  protected final String TEXT_320 = " = (java.util.Map)";
  protected final String TEXT_321 = ".this.globalMap.get(Thread.currentThread().getId()+\"_";
  protected final String TEXT_322 = "_\"+\"outputResult\");";
  protected final String TEXT_323 = "   " + NL + "    //Result for single col connection: ";
  protected final String TEXT_324 = NL + "    javax.xml.transform.Result execResult_";
  protected final String TEXT_325 = "_";
  protected final String TEXT_326 = " = (javax.xml.transform.Result)";
  protected final String TEXT_327 = ".this.globalMap.get(Thread.currentThread().getId()+\"_";
  protected final String TEXT_328 = "_\"+\"outputResult\");";
  protected final String TEXT_329 = NL + "    //Main job's connection: ";
  protected final String TEXT_330 = " ; thMap: ";
  protected final String TEXT_331 = NL + "    javax.xml.transform.Result execResult_";
  protected final String TEXT_332 = "_";
  protected final String TEXT_333 = " = resultsMapList_";
  protected final String TEXT_334 = ".get(\"";
  protected final String TEXT_335 = "\");";
  protected final String TEXT_336 = NL + "\t //Connection ";
  protected final String TEXT_337 = " with object result type" + NL + "    org.talend.transform.io.ObjectResult objectResult_";
  protected final String TEXT_338 = "_";
  protected final String TEXT_339 = " = (org.talend.transform.io.ObjectResult)execResult_";
  protected final String TEXT_340 = "_";
  protected final String TEXT_341 = ";" + NL + "    java.util.List<java.util.Map<String, Object>> outputList_";
  protected final String TEXT_342 = "_";
  protected final String TEXT_343 = " = (java.util.List)objectResult_";
  protected final String TEXT_344 = "_";
  protected final String TEXT_345 = ".getObject();" + NL + "    if (outputList_";
  protected final String TEXT_346 = "_";
  protected final String TEXT_347 = " != null) {" + NL + "      for (java.util.Map<String, Object> outMap_";
  protected final String TEXT_348 = " : outputList_";
  protected final String TEXT_349 = "_";
  protected final String TEXT_350 = ") {";
  protected final String TEXT_351 = NL + "        ";
  protected final String TEXT_352 = " = new ";
  protected final String TEXT_353 = "Struct();\t       ";
  protected final String TEXT_354 = NL + "\t//Output as direct map column: ";
  protected final String TEXT_355 = ".";
  protected final String TEXT_356 = NL + "    if(outMap_";
  protected final String TEXT_357 = ".get(\"";
  protected final String TEXT_358 = "\")==null){";
  protected final String TEXT_359 = NL + "        ";
  protected final String TEXT_360 = ".";
  protected final String TEXT_361 = " = null;" + NL + "    }else if (outMap_";
  protected final String TEXT_362 = ".get(\"";
  protected final String TEXT_363 = "\") instanceof String) {";
  protected final String TEXT_364 = NL + "        ";
  protected final String TEXT_365 = ".";
  protected final String TEXT_366 = " = (String)outMap_";
  protected final String TEXT_367 = ".get(\"";
  protected final String TEXT_368 = "\");" + NL + "    }";
  protected final String TEXT_369 = NL + "    if(outMap_";
  protected final String TEXT_370 = ".get(\"";
  protected final String TEXT_371 = "\")==null){";
  protected final String TEXT_372 = NL + "        ";
  protected final String TEXT_373 = ".";
  protected final String TEXT_374 = " = false;" + NL + "    }else if (outMap_";
  protected final String TEXT_375 = ".get(\"";
  protected final String TEXT_376 = "\") instanceof Boolean) {";
  protected final String TEXT_377 = NL + "        ";
  protected final String TEXT_378 = ".";
  protected final String TEXT_379 = " = ((Boolean)outMap_";
  protected final String TEXT_380 = ".get(\"";
  protected final String TEXT_381 = "\")).booleanValue();" + NL + "    }";
  protected final String TEXT_382 = NL + "    if (outMap_";
  protected final String TEXT_383 = ".get(\"";
  protected final String TEXT_384 = "\")==null){";
  protected final String TEXT_385 = NL + "       ";
  protected final String TEXT_386 = ".";
  protected final String TEXT_387 = " = 0;" + NL + "    } else if (outMap_";
  protected final String TEXT_388 = ".get(\"";
  protected final String TEXT_389 = "\") instanceof Byte) {";
  protected final String TEXT_390 = NL + "       ";
  protected final String TEXT_391 = ".";
  protected final String TEXT_392 = " = ((Byte)outMap_";
  protected final String TEXT_393 = ".get(\"";
  protected final String TEXT_394 = "\")).byteValue();" + NL + "    }";
  protected final String TEXT_395 = NL + "    if (outMap_";
  protected final String TEXT_396 = ".get(\"";
  protected final String TEXT_397 = "\")==null){";
  protected final String TEXT_398 = NL + "       ";
  protected final String TEXT_399 = ".";
  protected final String TEXT_400 = " = 0;" + NL + "    } else if (outMap_";
  protected final String TEXT_401 = ".get(\"";
  protected final String TEXT_402 = "\") instanceof Character) {";
  protected final String TEXT_403 = NL + "       ";
  protected final String TEXT_404 = ".";
  protected final String TEXT_405 = " = ((Character)outMap_";
  protected final String TEXT_406 = ".get(\"";
  protected final String TEXT_407 = "\")).charValue();" + NL + "    } else if (outMap_";
  protected final String TEXT_408 = ".get(\"";
  protected final String TEXT_409 = "\") instanceof Short) {";
  protected final String TEXT_410 = NL + "       ";
  protected final String TEXT_411 = ".";
  protected final String TEXT_412 = " = (char)((Short)outMap_";
  protected final String TEXT_413 = ".get(\"";
  protected final String TEXT_414 = "\")).shortValue();" + NL + "    }";
  protected final String TEXT_415 = NL + "    if (outMap_";
  protected final String TEXT_416 = ".get(\"";
  protected final String TEXT_417 = "\")==null){";
  protected final String TEXT_418 = NL + "       ";
  protected final String TEXT_419 = ".";
  protected final String TEXT_420 = " = 0;" + NL + "    } else if (outMap_";
  protected final String TEXT_421 = ".get(\"";
  protected final String TEXT_422 = "\") instanceof Short) {";
  protected final String TEXT_423 = NL + "       ";
  protected final String TEXT_424 = ".";
  protected final String TEXT_425 = " = ((Short)outMap_";
  protected final String TEXT_426 = ".get(\"";
  protected final String TEXT_427 = "\")).shortValue();" + NL + "    }";
  protected final String TEXT_428 = NL + "    if (outMap_";
  protected final String TEXT_429 = ".get(\"";
  protected final String TEXT_430 = "\")==null){";
  protected final String TEXT_431 = NL + "       ";
  protected final String TEXT_432 = ".";
  protected final String TEXT_433 = " = 0;" + NL + "    } else if (outMap_";
  protected final String TEXT_434 = ".get(\"";
  protected final String TEXT_435 = "\") instanceof Integer) {";
  protected final String TEXT_436 = NL + "       ";
  protected final String TEXT_437 = ".";
  protected final String TEXT_438 = " = ((Integer)outMap_";
  protected final String TEXT_439 = ".get(\"";
  protected final String TEXT_440 = "\")).intValue();" + NL + "    }";
  protected final String TEXT_441 = NL + "    if (outMap_";
  protected final String TEXT_442 = ".get(\"";
  protected final String TEXT_443 = "\")==null){";
  protected final String TEXT_444 = NL + "       ";
  protected final String TEXT_445 = ".";
  protected final String TEXT_446 = " = (long)0;" + NL + "    } else if (outMap_";
  protected final String TEXT_447 = ".get(\"";
  protected final String TEXT_448 = "\") instanceof Long) {";
  protected final String TEXT_449 = NL + "       ";
  protected final String TEXT_450 = ".";
  protected final String TEXT_451 = " = ((Long)outMap_";
  protected final String TEXT_452 = ".get(\"";
  protected final String TEXT_453 = "\")).longValue();" + NL + "    }";
  protected final String TEXT_454 = NL + "    if (outMap_";
  protected final String TEXT_455 = ".get(\"";
  protected final String TEXT_456 = "\")==null){";
  protected final String TEXT_457 = NL + "       ";
  protected final String TEXT_458 = ".";
  protected final String TEXT_459 = " = 0.0f;" + NL + "    } else if (outMap_";
  protected final String TEXT_460 = ".get(\"";
  protected final String TEXT_461 = "\") instanceof Float) {";
  protected final String TEXT_462 = NL + "       ";
  protected final String TEXT_463 = ".";
  protected final String TEXT_464 = " = ((Float)outMap_";
  protected final String TEXT_465 = ".get(\"";
  protected final String TEXT_466 = "\")).floatValue();" + NL + "    }";
  protected final String TEXT_467 = NL + "    if (outMap_";
  protected final String TEXT_468 = ".get(\"";
  protected final String TEXT_469 = "\")==null){";
  protected final String TEXT_470 = NL + "       ";
  protected final String TEXT_471 = ".";
  protected final String TEXT_472 = " = 0.0;" + NL + "    } else if (outMap_";
  protected final String TEXT_473 = ".get(\"";
  protected final String TEXT_474 = "\") instanceof Double) {";
  protected final String TEXT_475 = NL + "       ";
  protected final String TEXT_476 = ".";
  protected final String TEXT_477 = " = ((Double)outMap_";
  protected final String TEXT_478 = ".get(\"";
  protected final String TEXT_479 = "\")).doubleValue();" + NL + "    }";
  protected final String TEXT_480 = NL + "    if (outMap_";
  protected final String TEXT_481 = ".get(\"";
  protected final String TEXT_482 = "\")==null){";
  protected final String TEXT_483 = NL + "        ";
  protected final String TEXT_484 = ".";
  protected final String TEXT_485 = " = null;" + NL + "    } else if (outMap_";
  protected final String TEXT_486 = ".get(\"";
  protected final String TEXT_487 = "\") instanceof java.math.BigDecimal) {";
  protected final String TEXT_488 = NL + "        ";
  protected final String TEXT_489 = ".";
  protected final String TEXT_490 = " = (java.math.BigDecimal)outMap_";
  protected final String TEXT_491 = ".get(\"";
  protected final String TEXT_492 = "\");" + NL + "    }";
  protected final String TEXT_493 = NL + "    if (outMap_";
  protected final String TEXT_494 = ".get(\"";
  protected final String TEXT_495 = "\")==null){";
  protected final String TEXT_496 = NL + "        ";
  protected final String TEXT_497 = ".";
  protected final String TEXT_498 = " = null;" + NL + "    } else if (outMap_";
  protected final String TEXT_499 = ".get(\"";
  protected final String TEXT_500 = "\") instanceof java.util.Date) {";
  protected final String TEXT_501 = NL + "        ";
  protected final String TEXT_502 = ".";
  protected final String TEXT_503 = " = (java.util.Date)outMap_";
  protected final String TEXT_504 = ".get(\"";
  protected final String TEXT_505 = "\");" + NL + "    }";
  protected final String TEXT_506 = "\t\t\t\t\t\t";
  protected final String TEXT_507 = "  " + NL + "    //Output as string for single column connection" + NL + "    javax.xml.transform.stream.StreamResult streamResult_";
  protected final String TEXT_508 = "_";
  protected final String TEXT_509 = " = (javax.xml.transform.stream.StreamResult)execResult_";
  protected final String TEXT_510 = "_";
  protected final String TEXT_511 = ";" + NL + "    java.io.StringWriter swOut_";
  protected final String TEXT_512 = "_";
  protected final String TEXT_513 = " = (java.io.StringWriter)streamResult_";
  protected final String TEXT_514 = "_";
  protected final String TEXT_515 = ".getWriter();\t" + NL + "    if (swOut_";
  protected final String TEXT_516 = "_";
  protected final String TEXT_517 = " != null) {";
  protected final String TEXT_518 = NL + "      ";
  protected final String TEXT_519 = " = new ";
  protected final String TEXT_520 = "Struct();\t";
  protected final String TEXT_521 = NL + "      ";
  protected final String TEXT_522 = ".";
  protected final String TEXT_523 = " = swOut_";
  protected final String TEXT_524 = "_";
  protected final String TEXT_525 = ".toString();" + NL + "      rows_";
  protected final String TEXT_526 = ".add(";
  protected final String TEXT_527 = ");" + NL + "    }";
  protected final String TEXT_528 = NL + "    //Output as byte array for single col connection " + NL + "    javax.xml.transform.stream.StreamResult streamResult_";
  protected final String TEXT_529 = "_";
  protected final String TEXT_530 = " = (javax.xml.transform.stream.StreamResult)execResult_";
  protected final String TEXT_531 = "_";
  protected final String TEXT_532 = ";" + NL + "    java.io.ByteArrayOutputStream basOut_";
  protected final String TEXT_533 = "_";
  protected final String TEXT_534 = " = (java.io.ByteArrayOutputStream)streamResult_";
  protected final String TEXT_535 = "_";
  protected final String TEXT_536 = ".getOutputStream();" + NL + "    if (basOut_";
  protected final String TEXT_537 = "_";
  protected final String TEXT_538 = " != null) {";
  protected final String TEXT_539 = NL + "      ";
  protected final String TEXT_540 = " = new ";
  protected final String TEXT_541 = "Struct();\t";
  protected final String TEXT_542 = NL + "      ";
  protected final String TEXT_543 = ".";
  protected final String TEXT_544 = " = (byte[])basOut_";
  protected final String TEXT_545 = "_";
  protected final String TEXT_546 = ".toByteArray();" + NL + "      rows_";
  protected final String TEXT_547 = ".add(";
  protected final String TEXT_548 = ");" + NL + "    }";
  protected final String TEXT_549 = NL + "    //Output as a stream for single col connection ";
  protected final String TEXT_550 = NL + "    ";
  protected final String TEXT_551 = ".";
  protected final String TEXT_552 = " = (java.io.InputStream)";
  protected final String TEXT_553 = ".this.globalMap.get(Thread" + NL + "                .currentThread().getId()+\"_";
  protected final String TEXT_554 = "_\"+\"outputResult\");";
  protected final String TEXT_555 = NL + "    //Output as a document for single col connection " + NL + "    org.dom4j.io.DocumentResult drOut_";
  protected final String TEXT_556 = " = (org.dom4j.io.DocumentResult)execResult_";
  protected final String TEXT_557 = "_";
  protected final String TEXT_558 = ";" + NL + "    if (drOut_";
  protected final String TEXT_559 = " != null) {";
  protected final String TEXT_560 = NL + "        ";
  protected final String TEXT_561 = " = new ";
  protected final String TEXT_562 = "Struct();\t";
  protected final String TEXT_563 = NL + "        ";
  protected final String TEXT_564 = ".";
  protected final String TEXT_565 = " = new routines.system.Document();" + NL + "        ((routines.system.Document)";
  protected final String TEXT_566 = ".";
  protected final String TEXT_567 = ").setDocument(drOut_";
  protected final String TEXT_568 = ".getDocument());" + NL + "        rows_";
  protected final String TEXT_569 = ".add(";
  protected final String TEXT_570 = ");" + NL + "    }";
  protected final String TEXT_571 = NL + "        rows_";
  protected final String TEXT_572 = ".add(";
  protected final String TEXT_573 = ");" + NL + "      } // for (java.util.Map" + NL + "    } // if (outputList_" + NL;
  protected final String TEXT_574 = NL;

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    stringBuffer.append(TEXT_1);
    
    CodeGeneratorArgument codeGenArgument = (CodeGeneratorArgument) argument;
    INode node = (INode)codeGenArgument.getArgument();
    String processName = org.talend.core.model.utils.JavaResourcesHelper.getSimpleClassName(node.getProcess());
    String this_cid = ElementParameterParser.getValue(node, "__UNIQUE_NAME__");
    String tHMap_id = this_cid;
    String cid = tHMap_id;
    
    //Parameters for the subjavajets
    INode tHMapNode = MapperComponent.getThMapNode(node); //Find the thMap in case it is in a joblet 
    
    boolean sourceAsMap = "true".equals(ElementParameterParser.getValue(node, "__SOURCE_AS_MAP__"));
    if (sourceAsMap) {

    stringBuffer.append(TEXT_2);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_3);
    	 // Populate a TDM HashMap using a DI rowStruct
    	 if (node.getIncomingConnections()!=null) {
        	for (IConnection incomingConn : node.getIncomingConnections()) {
        		//Create the TDM map from the rowStruct here for the main connection only
            	if (incomingConn.getLineStyle().hasConnectionCategory(IConnectionCategory.MAIN)) {

    stringBuffer.append(TEXT_4);
    stringBuffer.append(incomingConn.getUniqueName());
    stringBuffer.append(TEXT_5);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_6);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_7);
    
    // Populate a TDM HashMap using a DI rowStruct
    String incomingConnName = incomingConn.getUniqueName();

    stringBuffer.append(TEXT_8);
    stringBuffer.append(incomingConnName);
    stringBuffer.append(TEXT_9);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_10);
    
                IMetadataTable inputMetadataTable = incomingConn.getMetadataTable();
                for (IMetadataColumn inputCol : inputMetadataTable.getListColumns()) {

    stringBuffer.append(TEXT_11);
    stringBuffer.append(incomingConnName);
    stringBuffer.append(TEXT_12);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_13);
    stringBuffer.append(inputCol.getLabel() );
    stringBuffer.append(TEXT_14);
    stringBuffer.append(incomingConn.getName());
    stringBuffer.append(TEXT_15);
    stringBuffer.append(inputCol.getLabel() );
    stringBuffer.append(TEXT_16);
    
                } // for (IMetadataColumn

    stringBuffer.append(TEXT_17);
    stringBuffer.append(incomingConnName);
    stringBuffer.append(TEXT_18);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_19);
    stringBuffer.append(incomingConnName);
    stringBuffer.append(TEXT_20);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_21);
    
				break;
				} //end if
			} //end forIConnection
		}//end ifnode.getIncomingConnections
    } // if (sourceAsMap)

    stringBuffer.append(TEXT_22);
    
    // Fills the dependency property of the component
    org.talend.transform.components.ExportDependencyProvider edp = new org.talend.transform.components.ExportDependencyProvider();
    edp.createTDMArchives(tHMapNode);
    MapperComponent mapperComponent = ((MapperComponent)tHMapNode.getExternalNode());

    String mapPath = ElementParameterParser.getValue(node, "__HMAP_PATH__");
    String mapVarPath = ElementParameterParser.getValue(node, "__HMAP_VAR_PATH__");

    org.talend.transform.component.thmap.MapInfo mapInfo = mapperComponent.getMapInfo(mapPath, mapVarPath);
    String mainProjectName = mapInfo.getMainProjectName();
    String relativeMapPath = mapInfo.getRelativeMapPath();

    String log = ElementParameterParser.getValue(node, "__LOG__");

    boolean asInputstream = "true".equals(ElementParameterParser.getValue(node, "__AS_INPUTSTREAM__"));

    String devWorkspace = ElementParameterParser.getValue(node.getProcess(), "__COMP_DEFAULT_FILE_DIR__");
    devWorkspace = TalendTextUtils.addQuotes(devWorkspace);
    String devInstall = ElementParameterParser.getValue(node.getProcess(), "__PRODUCT_ROOT_DIR__");
    devInstall = TalendTextUtils.addQuotes(devInstall);

    Integer exceptionLevel = Integer.parseInt(ElementParameterParser.getValue(node, "__EXCEPTION__"));

    //Use the main job's node to find the incoming connections that have the declared <connection>Struct classes
    java.util.List<? extends IConnection> mainInConnections = ((MapperComponent)node.getExternalNode()).getNonVirtualMainIncomingConnections();

    stringBuffer.append(TEXT_23);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_24);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_25);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_26);
    stringBuffer.append(processName);
    stringBuffer.append(TEXT_27);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_28);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_29);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_30);
    stringBuffer.append(TEXT_31);
    stringBuffer.append(processName);
    stringBuffer.append(TEXT_32);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_33);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_34);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_35);
    stringBuffer.append(log);
    stringBuffer.append(TEXT_36);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_37);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_38);
    
    String localContext = "context";

    stringBuffer.append(TEXT_39);
    stringBuffer.append(localContext);
    stringBuffer.append(TEXT_40);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_41);
    stringBuffer.append(localContext);
    stringBuffer.append(TEXT_42);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_43);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_44);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_45);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_46);
    stringBuffer.append(localContext);
    stringBuffer.append(TEXT_47);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_48);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_49);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_50);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_51);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_52);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_53);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_54);
    
	String mapVarPathWithoutQuotes = TalendTextUtils.removeQuotes(mapVarPath);
    if (mapVarPathWithoutQuotes.startsWith("context.")) {
        String contextMapVar = mapVarPathWithoutQuotes.substring(mapVarPathWithoutQuotes.indexOf(".") + 1);

    stringBuffer.append(TEXT_55);
    stringBuffer.append(contextMapVar);
    stringBuffer.append(TEXT_56);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_57);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_58);
    stringBuffer.append(mainProjectName);
    stringBuffer.append(TEXT_59);
    
    } else {

    stringBuffer.append(TEXT_60);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_61);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_62);
    stringBuffer.append(mainProjectName);
    stringBuffer.append(TEXT_63);
    stringBuffer.append(relativeMapPath);
    stringBuffer.append(TEXT_64);
    
  }  

    
    if (mainInConnections!=null && mainInConnections.size() >= 1) {
      boolean requiresMultipleSources = mapperComponent.requiresMultipleSources();

    stringBuffer.append(TEXT_65);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_66);
    
      for (IConnection incomingConn : mainInConnections) {
        //In the event that the connection name in the main job and joblet are not the same, this is the connection which has a struct declaration
        String mainConnectionName = incomingConn.getUniqueName();
        //Find the corresponding connection if the thMap is in a joblet
        IConnection thMapInConnection = org.talend.transform.components.utils.ComponentUtils.findInConnectionFromTransformNode(tHMapNode, node, incomingConn); //Use this to find the record for the mapExecutor
        String recordName = thMapInConnection.getUniqueName();
        if (mapperComponent.isSingleSchemaTypeInputConnection(thMapInConnection)) {
          //Single connection schema-type; non-wrapped

    stringBuffer.append(TEXT_67);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_68);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_69);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_70);
    
        } else { //Create the Source accordingly

    
   //Create the corresponding source for the connection

    stringBuffer.append(TEXT_71);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_72);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_73);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_74);
               if (!mapperComponent.isIncomingHashMap(thMapInConnection)) {
                 if (!incomingConn.getLineStyle().hasConnectionCategory(IConnectionCategory.MAIN)) {

    stringBuffer.append(TEXT_75);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_76);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_77);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_78);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_79);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_80);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_81);
    stringBuffer.append(TEXT_82);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_83);
    stringBuffer.append(incomingConn.getName());
    stringBuffer.append(TEXT_84);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_85);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_86);
    stringBuffer.append(TEXT_87);
    stringBuffer.append(incomingConn.getName());
    stringBuffer.append(TEXT_88);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_89);
    
                 } //End for Lookup Connection
                 IMetadataColumn inputCol = incomingConn.getMetadataTable().getListColumns().get(0);
                 if (JavaTypesManager.STRING.getId().equals(inputCol.getTalendType())) {

    stringBuffer.append(TEXT_90);
    stringBuffer.append(node.getUniqueName());
    stringBuffer.append(TEXT_91);
    stringBuffer.append(incomingConn.getName() );
    stringBuffer.append(TEXT_92);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_93);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_94);
    stringBuffer.append(incomingConn.getName() );
    stringBuffer.append(TEXT_95);
    stringBuffer.append(incomingConn.getName() );
    stringBuffer.append(TEXT_96);
    stringBuffer.append(inputCol.getLabel() );
    stringBuffer.append(TEXT_97);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_98);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_99);
    stringBuffer.append(incomingConn.getName() );
    stringBuffer.append(TEXT_100);
    stringBuffer.append(inputCol.getLabel() );
    stringBuffer.append(TEXT_101);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_102);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_103);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_104);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_105);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_106);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_107);
    
                 } else if ("id_Document".equals(inputCol.getTalendType())) {

    stringBuffer.append(TEXT_108);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_109);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_110);
    stringBuffer.append(incomingConn.getName() );
    stringBuffer.append(TEXT_111);
    stringBuffer.append(incomingConn.getName() );
    stringBuffer.append(TEXT_112);
    stringBuffer.append(inputCol.getLabel() );
    stringBuffer.append(TEXT_113);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_114);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_115);
    stringBuffer.append(incomingConn.getName() );
    stringBuffer.append(TEXT_116);
    stringBuffer.append(inputCol.getLabel() );
    stringBuffer.append(TEXT_117);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_118);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_119);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_120);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_121);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_122);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_123);
    
                 } else if ("id_byte[]".equals(inputCol.getTalendType())) {

    stringBuffer.append(TEXT_124);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_125);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_126);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_127);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_128);
    stringBuffer.append(incomingConn.getName());
    stringBuffer.append(TEXT_129);
    stringBuffer.append(inputCol.getLabel());
    stringBuffer.append(TEXT_130);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_131);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_132);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_133);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_134);
    
                 } else {

    stringBuffer.append(TEXT_135);
    stringBuffer.append(incomingConn.getName() );
    stringBuffer.append(TEXT_136);
    stringBuffer.append(inputCol.getLabel() );
    stringBuffer.append(TEXT_137);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_138);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_139);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_140);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_141);
    stringBuffer.append(incomingConn.getName() );
    stringBuffer.append(TEXT_142);
    stringBuffer.append(inputCol.getLabel() );
    stringBuffer.append(TEXT_143);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_144);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_145);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_146);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_147);
    stringBuffer.append(incomingConn.getName() );
    stringBuffer.append(TEXT_148);
    stringBuffer.append(inputCol.getLabel() );
    stringBuffer.append(TEXT_149);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_150);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_151);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_152);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_153);
    stringBuffer.append(incomingConn.getName() );
    stringBuffer.append(TEXT_154);
    stringBuffer.append(inputCol.getLabel() );
    stringBuffer.append(TEXT_155);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_156);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_157);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_158);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_159);
    stringBuffer.append(incomingConn.getName() );
    stringBuffer.append(TEXT_160);
    stringBuffer.append(inputCol.getLabel() );
    stringBuffer.append(TEXT_161);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_162);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_163);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_164);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_165);
    stringBuffer.append(incomingConn.getName() );
    stringBuffer.append(TEXT_166);
    stringBuffer.append(inputCol.getLabel() );
    stringBuffer.append(TEXT_167);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_168);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_169);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_170);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_171);
    stringBuffer.append(incomingConn.getName() );
    stringBuffer.append(TEXT_172);
    stringBuffer.append(inputCol.getLabel() );
    stringBuffer.append(TEXT_173);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_174);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_175);
    stringBuffer.append(incomingConn.getName() );
    stringBuffer.append(TEXT_176);
    stringBuffer.append(inputCol.getLabel() );
    stringBuffer.append(TEXT_177);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_178);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_179);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_180);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_181);
    
                 }//End if-else types
               } else { //End if !isIncomingHashMap
                   if (incomingConn.getLineStyle().hasConnectionCategory(IConnectionCategory.MAIN)) {
                      //The source data is read first(for virtual components)

    stringBuffer.append(TEXT_182);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_183);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_184);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_185);
                     } else if (incomingConn.getLineStyle().hasConnectionCategory(IConnectionCategory.DATA)) {

    stringBuffer.append(TEXT_186);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_187);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_188);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_189);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_190);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_191);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_192);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_193);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_194);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_195);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_196);
    stringBuffer.append(TEXT_197);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_198);
    stringBuffer.append(incomingConn.getName());
    stringBuffer.append(TEXT_199);
    
    // Populate a TDM HashMap using a DI rowStruct
    String incomingConnName = incomingConn.getUniqueName();

    stringBuffer.append(TEXT_200);
    stringBuffer.append(incomingConnName);
    stringBuffer.append(TEXT_201);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_202);
    
                IMetadataTable inputMetadataTable = incomingConn.getMetadataTable();
                for (IMetadataColumn inputCol : inputMetadataTable.getListColumns()) {

    stringBuffer.append(TEXT_203);
    stringBuffer.append(incomingConnName);
    stringBuffer.append(TEXT_204);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_205);
    stringBuffer.append(inputCol.getLabel() );
    stringBuffer.append(TEXT_206);
    stringBuffer.append(incomingConn.getName());
    stringBuffer.append(TEXT_207);
    stringBuffer.append(inputCol.getLabel() );
    stringBuffer.append(TEXT_208);
    
                } // for (IMetadataColumn

    stringBuffer.append(TEXT_209);
    stringBuffer.append(incomingConnName);
    stringBuffer.append(TEXT_210);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_211);
    stringBuffer.append(incomingConnName);
    stringBuffer.append(TEXT_212);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_213);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_214);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_215);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_216);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_217);
    
                   } //End if MAIN or DATA
               } //End else metadataColumns.size()>1

    
          if (requiresMultipleSources) { //MultiSource structure
              String recordPath = mapperComponent.findInputConnectionRecordPath(thMapInConnection);

    stringBuffer.append(TEXT_218);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_219);
    stringBuffer.append(recordPath);
    stringBuffer.append(TEXT_220);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_221);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_222);
    
          } else { //Single-connection / payloadtype

    stringBuffer.append(TEXT_223);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_224);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_225);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_226);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_227);
    
          } //End if Multirep structure
        } //End else if isSingleSchemaTypeInputConnection
      } //End forIConnection
      if (requiresMultipleSources) {

    stringBuffer.append(TEXT_228);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_229);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_230);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_231);
    
      }
    } //End mainInConnections!=null

    stringBuffer.append(TEXT_232);
    stringBuffer.append(TEXT_233);
    
	//Note: Create the result using the logic here if it is not of type inputStream
    boolean asInputstream_ = "true".equals(ElementParameterParser.getValue(node, "__AS_INPUTSTREAM__"));
    java.util.List<? extends IConnection> mainOutConnections_setResults = ((MapperComponent)node.getExternalNode()).getNonVirtualMainOutgoingConnections();
    boolean requires_setResults = mapperComponent.requiresMultipleResults();    
    boolean requiresMultipleResults = mapperComponent.requiresMultipleResults();

      
    if (requires_setResults) {  //The output is a wrapper structure

    stringBuffer.append(TEXT_234);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_235);
      
	 } else if (!asInputstream_ && mainOutConnections_setResults.size()==1){ //single connection

    stringBuffer.append(TEXT_236);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_237);
    	
    } //end if-requires_setResults
    for (IConnection outConnection : mainOutConnections_setResults) {
        //In the event that the connection name in the main job and joblet are not the same, this is the connection which has a struct declaration
        String mainConnectionName = outConnection.getUniqueName();
        //Find the corresponding connection if the thMap is in a joblet
        IConnection thMapOutConnection = org.talend.transform.components.utils.ComponentUtils.findOutConnectionFromTransformNode(tHMapNode, node, outConnection); //Use this to find the record for the mapExecutor
        if (!asInputstream_) { //Create the Result accordingly
	         String defaultDataType = mapperComponent.getOutputConnectionDataType(outConnection);
	         if ("MAP".equals(defaultDataType)) { //Schema-type connection 

    stringBuffer.append(TEXT_238);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_239);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_240);
    stringBuffer.append(outConnection.getName());
    stringBuffer.append(TEXT_241);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_242);
    stringBuffer.append(outConnection.getName());
    stringBuffer.append(TEXT_243);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_244);
    stringBuffer.append(outConnection.getName());
    stringBuffer.append(TEXT_245);
        
            } else if ((JavaTypesManager.STRING.getId().equals(defaultDataType))) {

    stringBuffer.append(TEXT_246);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_247);
    stringBuffer.append(outConnection.getName());
    stringBuffer.append(TEXT_248);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_249);
    stringBuffer.append(outConnection.getName());
    stringBuffer.append(TEXT_250);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_251);
    stringBuffer.append(outConnection.getName());
    stringBuffer.append(TEXT_252);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_253);
    stringBuffer.append(outConnection.getName());
    stringBuffer.append(TEXT_254);
    
            } else if ("id_byte[]".equals(defaultDataType)) {

    stringBuffer.append(TEXT_255);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_256);
    stringBuffer.append(outConnection.getName());
    stringBuffer.append(TEXT_257);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_258);
    stringBuffer.append(outConnection.getName());
    stringBuffer.append(TEXT_259);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_260);
    stringBuffer.append(outConnection.getName());
    stringBuffer.append(TEXT_261);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_262);
    stringBuffer.append(outConnection.getName());
    stringBuffer.append(TEXT_263);
    
            } else if ("id_Document".equals(defaultDataType)) {

    stringBuffer.append(TEXT_264);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_265);
    stringBuffer.append(outConnection.getName());
    stringBuffer.append(TEXT_266);
    
            } // end if-defaultDataType
            if (requires_setResults) {
               String recordPath = mapperComponent.findOutputConnectionRecordPath(thMapOutConnection); 

    stringBuffer.append(TEXT_267);
    stringBuffer.append(outConnection.getName());
    stringBuffer.append(TEXT_268);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_269);
    stringBuffer.append(recordPath);
    stringBuffer.append(TEXT_270);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_271);
    stringBuffer.append(outConnection.getName());
    stringBuffer.append(TEXT_272);
    	  
	         } else if (!asInputstream_ && mainOutConnections_setResults.size()==1){ //single connection only 

    stringBuffer.append(TEXT_273);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_274);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_275);
    stringBuffer.append(outConnection.getName());
    stringBuffer.append(TEXT_276);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_277);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_278);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_279);
    
            } //End if-requires_setResults

        
        } //end if !asInputstream_
    } //end for loop
    if (requires_setResults) { 

    stringBuffer.append(TEXT_280);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_281);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_282);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_283);
      
	} if (!asInputstream_ && mainOutConnections_setResults.size()>=1) {

    stringBuffer.append(TEXT_284);
    stringBuffer.append(processName);
    stringBuffer.append(TEXT_285);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_286);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_287);
      
    } //end if !asInputstream_

    stringBuffer.append(TEXT_288);
    
    if (asInputstream) {

    stringBuffer.append(TEXT_289);
    stringBuffer.append(TEXT_290);
    stringBuffer.append( processName );
    stringBuffer.append(TEXT_291);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_292);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_293);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_294);
    
    } else {

    stringBuffer.append(TEXT_295);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_296);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_297);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_298);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_299);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_300);
    stringBuffer.append(TEXT_301);
    stringBuffer.append( processName );
    stringBuffer.append(TEXT_302);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_303);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_304);
    stringBuffer.append(TEXT_305);
    stringBuffer.append( processName );
    stringBuffer.append(TEXT_306);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_307);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_308);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_309);
    stringBuffer.append(exceptionLevel);
    stringBuffer.append(TEXT_310);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_311);
    
        if(!org.talend.designer.runprocess.ProcessorUtilities.isExportConfig()){

    stringBuffer.append(TEXT_312);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_313);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_314);
    
        } // (!org.talend.designer.runprocess
    } // if (asInputstream)

    stringBuffer.append(TEXT_315);
    stringBuffer.append(TEXT_316);
    stringBuffer.append(TEXT_317);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_318);
    
    java.util.List<? extends IConnection> mainOutConnections_getResults = ((MapperComponent)node.getExternalNode()).getNonVirtualMainOutgoingConnections();
    boolean requires_getResults = mapperComponent.requiresMultipleResults();    
    if (requires_getResults) {
    	//Note: Only map output type should support multiple outputs

    stringBuffer.append(TEXT_319);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_320);
    stringBuffer.append( processName );
    stringBuffer.append(TEXT_321);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_322);
      
    } else if (!asInputstream && mainOutConnections_getResults.size()==1) { 
       //Single column schema/payload-type output
       String outputConnName = mainOutConnections_getResults.get(0).getName();       

    stringBuffer.append(TEXT_323);
    stringBuffer.append(outputConnName);
    stringBuffer.append(TEXT_324);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_325);
    stringBuffer.append(outputConnName);
    stringBuffer.append(TEXT_326);
    stringBuffer.append( processName );
    stringBuffer.append(TEXT_327);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_328);
      
    } //end if-requires_getResults
    if (mainOutConnections_getResults!=null) {
      //Use the thMapNode to get the record names
      for (IConnection outgoingConn : mainOutConnections_getResults) {
        String outputConnName = outgoingConn.getName();
        IConnection thMapOutConnection = org.talend.transform.components.utils.ComponentUtils.findOutConnectionFromTransformNode(tHMapNode, node, outgoingConn);            
        String defaultDataType = mapperComponent.getOutputConnectionDataType(thMapOutConnection);
          if (requires_getResults) { 
            //This is the record of the connection to the thMap 
            String recordPath = mapperComponent.findOutputConnectionRecordPath(thMapOutConnection);
            

    stringBuffer.append(TEXT_329);
    stringBuffer.append(outputConnName);
    stringBuffer.append(TEXT_330);
    stringBuffer.append(recordPath);
    stringBuffer.append(TEXT_331);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_332);
    stringBuffer.append(outputConnName);
    stringBuffer.append(TEXT_333);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_334);
    stringBuffer.append(recordPath);
    stringBuffer.append(TEXT_335);
           
	       } 
	       if ("MAP".equals(defaultDataType)) { //Schema-type connection 

    stringBuffer.append(TEXT_336);
    stringBuffer.append(outputConnName);
    stringBuffer.append(TEXT_337);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_338);
    stringBuffer.append(outputConnName);
    stringBuffer.append(TEXT_339);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_340);
    stringBuffer.append(outputConnName);
    stringBuffer.append(TEXT_341);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_342);
    stringBuffer.append(outputConnName);
    stringBuffer.append(TEXT_343);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_344);
    stringBuffer.append(outputConnName);
    stringBuffer.append(TEXT_345);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_346);
    stringBuffer.append(outputConnName);
    stringBuffer.append(TEXT_347);
    stringBuffer.append(outputConnName);
    stringBuffer.append(TEXT_348);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_349);
    stringBuffer.append(outputConnName);
    stringBuffer.append(TEXT_350);
    stringBuffer.append(TEXT_351);
    stringBuffer.append(outputConnName);
    stringBuffer.append(TEXT_352);
    stringBuffer.append(outputConnName);
    stringBuffer.append(TEXT_353);
    	       
          } //end if asMap                      
          IMetadataTable outputMetadataTable = outgoingConn.getMetadataTable();
          for (IMetadataColumn outputCol : outputMetadataTable.getListColumns()) {

    stringBuffer.append(TEXT_354);
    stringBuffer.append(outgoingConn.getName() );
    stringBuffer.append(TEXT_355);
    stringBuffer.append(outputCol.getLabel());
              
           if ("MAP".equals(defaultDataType)) { //Schema-type connection 

    
    // Populate a DI rowStruct using a TDM HashMap
    if (JavaTypesManager.STRING.getId().equals(outputCol.getTalendType())) {

    stringBuffer.append(TEXT_356);
    stringBuffer.append(outgoingConn.getName());
    stringBuffer.append(TEXT_357);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_358);
    stringBuffer.append(TEXT_359);
    stringBuffer.append(outgoingConn.getName() );
    stringBuffer.append(TEXT_360);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_361);
    stringBuffer.append(outgoingConn.getName());
    stringBuffer.append(TEXT_362);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_363);
    stringBuffer.append(TEXT_364);
    stringBuffer.append(outgoingConn.getName() );
    stringBuffer.append(TEXT_365);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_366);
    stringBuffer.append(outgoingConn.getName());
    stringBuffer.append(TEXT_367);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_368);
    
    } else if (JavaTypesManager.BOOLEAN.getId().equals(outputCol.getTalendType())) {

    stringBuffer.append(TEXT_369);
    stringBuffer.append(outgoingConn.getName());
    stringBuffer.append(TEXT_370);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_371);
    stringBuffer.append(TEXT_372);
    stringBuffer.append(outgoingConn.getName() );
    stringBuffer.append(TEXT_373);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_374);
    stringBuffer.append(outgoingConn.getName());
    stringBuffer.append(TEXT_375);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_376);
    stringBuffer.append(TEXT_377);
    stringBuffer.append(outgoingConn.getName() );
    stringBuffer.append(TEXT_378);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_379);
    stringBuffer.append(outgoingConn.getName());
    stringBuffer.append(TEXT_380);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_381);
    
    } else if (JavaTypesManager.BYTE.getId().equals(outputCol.getTalendType())) {

    stringBuffer.append(TEXT_382);
    stringBuffer.append(outgoingConn.getName());
    stringBuffer.append(TEXT_383);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_384);
    stringBuffer.append(TEXT_385);
    stringBuffer.append(outgoingConn.getName() );
    stringBuffer.append(TEXT_386);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_387);
    stringBuffer.append(outgoingConn.getName());
    stringBuffer.append(TEXT_388);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_389);
    stringBuffer.append(TEXT_390);
    stringBuffer.append(outgoingConn.getName() );
    stringBuffer.append(TEXT_391);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_392);
    stringBuffer.append(outgoingConn.getName());
    stringBuffer.append(TEXT_393);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_394);
    
    } else if (JavaTypesManager.CHARACTER.getId().equals(outputCol.getTalendType())) {

    stringBuffer.append(TEXT_395);
    stringBuffer.append(outgoingConn.getName());
    stringBuffer.append(TEXT_396);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_397);
    stringBuffer.append(TEXT_398);
    stringBuffer.append(outgoingConn.getName() );
    stringBuffer.append(TEXT_399);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_400);
    stringBuffer.append(outgoingConn.getName());
    stringBuffer.append(TEXT_401);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_402);
    stringBuffer.append(TEXT_403);
    stringBuffer.append(outgoingConn.getName() );
    stringBuffer.append(TEXT_404);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_405);
    stringBuffer.append(outgoingConn.getName());
    stringBuffer.append(TEXT_406);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_407);
    stringBuffer.append(outgoingConn.getName());
    stringBuffer.append(TEXT_408);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_409);
    stringBuffer.append(TEXT_410);
    stringBuffer.append(outgoingConn.getName() );
    stringBuffer.append(TEXT_411);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_412);
    stringBuffer.append(outgoingConn.getName());
    stringBuffer.append(TEXT_413);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_414);
    
    } else if (JavaTypesManager.SHORT.getId().equals(outputCol.getTalendType())) {

    stringBuffer.append(TEXT_415);
    stringBuffer.append(outgoingConn.getName());
    stringBuffer.append(TEXT_416);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_417);
    stringBuffer.append(TEXT_418);
    stringBuffer.append(outgoingConn.getName() );
    stringBuffer.append(TEXT_419);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_420);
    stringBuffer.append(outgoingConn.getName());
    stringBuffer.append(TEXT_421);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_422);
    stringBuffer.append(TEXT_423);
    stringBuffer.append(outgoingConn.getName() );
    stringBuffer.append(TEXT_424);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_425);
    stringBuffer.append(outgoingConn.getName());
    stringBuffer.append(TEXT_426);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_427);
    
    } else if (JavaTypesManager.INTEGER.getId().equals(outputCol.getTalendType())) {

    stringBuffer.append(TEXT_428);
    stringBuffer.append(outgoingConn.getName());
    stringBuffer.append(TEXT_429);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_430);
    stringBuffer.append(TEXT_431);
    stringBuffer.append(outgoingConn.getName() );
    stringBuffer.append(TEXT_432);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_433);
    stringBuffer.append(outgoingConn.getName());
    stringBuffer.append(TEXT_434);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_435);
    stringBuffer.append(TEXT_436);
    stringBuffer.append(outgoingConn.getName() );
    stringBuffer.append(TEXT_437);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_438);
    stringBuffer.append(outgoingConn.getName());
    stringBuffer.append(TEXT_439);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_440);
    
    } else if (JavaTypesManager.LONG.getId().equals(outputCol.getTalendType())) {

    stringBuffer.append(TEXT_441);
    stringBuffer.append(outgoingConn.getName());
    stringBuffer.append(TEXT_442);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_443);
    stringBuffer.append(TEXT_444);
    stringBuffer.append(outgoingConn.getName() );
    stringBuffer.append(TEXT_445);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_446);
    stringBuffer.append(outgoingConn.getName());
    stringBuffer.append(TEXT_447);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_448);
    stringBuffer.append(TEXT_449);
    stringBuffer.append(outgoingConn.getName() );
    stringBuffer.append(TEXT_450);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_451);
    stringBuffer.append(outgoingConn.getName());
    stringBuffer.append(TEXT_452);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_453);
    
    } else if (JavaTypesManager.FLOAT.getId().equals(outputCol.getTalendType())) {

    stringBuffer.append(TEXT_454);
    stringBuffer.append(outgoingConn.getName());
    stringBuffer.append(TEXT_455);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_456);
    stringBuffer.append(TEXT_457);
    stringBuffer.append(outgoingConn.getName() );
    stringBuffer.append(TEXT_458);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_459);
    stringBuffer.append(outgoingConn.getName());
    stringBuffer.append(TEXT_460);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_461);
    stringBuffer.append(TEXT_462);
    stringBuffer.append(outgoingConn.getName() );
    stringBuffer.append(TEXT_463);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_464);
    stringBuffer.append(outgoingConn.getName());
    stringBuffer.append(TEXT_465);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_466);
    
    } else if (JavaTypesManager.DOUBLE.getId().equals(outputCol.getTalendType())) {

    stringBuffer.append(TEXT_467);
    stringBuffer.append(outgoingConn.getName());
    stringBuffer.append(TEXT_468);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_469);
    stringBuffer.append(TEXT_470);
    stringBuffer.append(outgoingConn.getName() );
    stringBuffer.append(TEXT_471);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_472);
    stringBuffer.append(outgoingConn.getName());
    stringBuffer.append(TEXT_473);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_474);
    stringBuffer.append(TEXT_475);
    stringBuffer.append(outgoingConn.getName() );
    stringBuffer.append(TEXT_476);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_477);
    stringBuffer.append(outgoingConn.getName());
    stringBuffer.append(TEXT_478);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_479);
    
    } else if (JavaTypesManager.BIGDECIMAL.getId().equals(outputCol.getTalendType())) {

    stringBuffer.append(TEXT_480);
    stringBuffer.append(outgoingConn.getName());
    stringBuffer.append(TEXT_481);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_482);
    stringBuffer.append(TEXT_483);
    stringBuffer.append(outgoingConn.getName() );
    stringBuffer.append(TEXT_484);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_485);
    stringBuffer.append(outgoingConn.getName());
    stringBuffer.append(TEXT_486);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_487);
    stringBuffer.append(TEXT_488);
    stringBuffer.append(outgoingConn.getName() );
    stringBuffer.append(TEXT_489);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_490);
    stringBuffer.append(outgoingConn.getName());
    stringBuffer.append(TEXT_491);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_492);
    
    } else if (JavaTypesManager.DATE.getId().equals(outputCol.getTalendType())) {

    stringBuffer.append(TEXT_493);
    stringBuffer.append(outgoingConn.getName());
    stringBuffer.append(TEXT_494);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_495);
    stringBuffer.append(TEXT_496);
    stringBuffer.append(outgoingConn.getName() );
    stringBuffer.append(TEXT_497);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_498);
    stringBuffer.append(outgoingConn.getName());
    stringBuffer.append(TEXT_499);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_500);
    stringBuffer.append(TEXT_501);
    stringBuffer.append(outgoingConn.getName() );
    stringBuffer.append(TEXT_502);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_503);
    stringBuffer.append(outgoingConn.getName());
    stringBuffer.append(TEXT_504);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_505);
    
    } // if (JavaTypesManager.STRING

    stringBuffer.append(TEXT_506);
    
           } else if ((JavaTypesManager.STRING.getId().equals(defaultDataType))) {

    stringBuffer.append(TEXT_507);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_508);
    stringBuffer.append(outputConnName);
    stringBuffer.append(TEXT_509);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_510);
    stringBuffer.append(outputConnName);
    stringBuffer.append(TEXT_511);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_512);
    stringBuffer.append(outputConnName);
    stringBuffer.append(TEXT_513);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_514);
    stringBuffer.append(outputConnName);
    stringBuffer.append(TEXT_515);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_516);
    stringBuffer.append(outputConnName);
    stringBuffer.append(TEXT_517);
    stringBuffer.append(TEXT_518);
    stringBuffer.append(outputConnName);
    stringBuffer.append(TEXT_519);
    stringBuffer.append(outputConnName);
    stringBuffer.append(TEXT_520);
    stringBuffer.append(TEXT_521);
    stringBuffer.append(outgoingConn.getName() );
    stringBuffer.append(TEXT_522);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_523);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_524);
    stringBuffer.append(outputConnName);
    stringBuffer.append(TEXT_525);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_526);
    stringBuffer.append(outputConnName);
    stringBuffer.append(TEXT_527);
    
               
           } else if ("id_byte[]".equals(defaultDataType)) {

    stringBuffer.append(TEXT_528);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_529);
    stringBuffer.append(outputConnName);
    stringBuffer.append(TEXT_530);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_531);
    stringBuffer.append(outputConnName);
    stringBuffer.append(TEXT_532);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_533);
    stringBuffer.append(outputConnName);
    stringBuffer.append(TEXT_534);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_535);
    stringBuffer.append(outputConnName);
    stringBuffer.append(TEXT_536);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_537);
    stringBuffer.append(outputConnName);
    stringBuffer.append(TEXT_538);
    stringBuffer.append(TEXT_539);
    stringBuffer.append(outputConnName);
    stringBuffer.append(TEXT_540);
    stringBuffer.append(outputConnName);
    stringBuffer.append(TEXT_541);
    stringBuffer.append(TEXT_542);
    stringBuffer.append(outgoingConn.getName() );
    stringBuffer.append(TEXT_543);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_544);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_545);
    stringBuffer.append(outputConnName);
    stringBuffer.append(TEXT_546);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_547);
    stringBuffer.append(outputConnName);
    stringBuffer.append(TEXT_548);
    
          } else if (asInputstream) {

    stringBuffer.append(TEXT_549);
    stringBuffer.append(TEXT_550);
    stringBuffer.append(outgoingConn.getName() );
    stringBuffer.append(TEXT_551);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_552);
    stringBuffer.append( processName );
    stringBuffer.append(TEXT_553);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_554);
    
               
          } else if ("id_Document".equals(defaultDataType)) {

    stringBuffer.append(TEXT_555);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_556);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_557);
    stringBuffer.append(outputConnName);
    stringBuffer.append(TEXT_558);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_559);
    stringBuffer.append(TEXT_560);
    stringBuffer.append(outputConnName);
    stringBuffer.append(TEXT_561);
    stringBuffer.append(outputConnName);
    stringBuffer.append(TEXT_562);
    stringBuffer.append(TEXT_563);
    stringBuffer.append(outgoingConn.getName() );
    stringBuffer.append(TEXT_564);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_565);
    stringBuffer.append(outgoingConn.getName() );
    stringBuffer.append(TEXT_566);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_567);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_568);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_569);
    stringBuffer.append(outputConnName);
    stringBuffer.append(TEXT_570);
    
               
             } // if-else (defaultDataType)
          } // for (IMetadataColumn
        //For schema-type outputs, save the rows in a list 
        if ("MAP".equals(defaultDataType)) { //Schema-type connection 

    stringBuffer.append(TEXT_571);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_572);
    stringBuffer.append(outputConnName);
    stringBuffer.append(TEXT_573);
               
        } //end if (MAP)
      } // for (IConnection           
    } // if (thMapOutConnections

    stringBuffer.append(TEXT_574);
    return stringBuffer.toString();
  }
}
