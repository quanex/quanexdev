package org.talend.designer.codegen.translators.common;

import java.util.List;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Vector;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import org.talend.core.GlobalServiceRegister;
import org.talend.core.model.process.IProcess;
import org.talend.core.model.process.IProcess2;
import org.talend.core.model.process.INode;
import org.talend.core.model.process.IConnection;
import org.talend.core.model.process.EConnectionType;
import org.talend.core.model.process.IContextParameter;
import org.talend.core.model.process.IElementParameter;
import org.talend.core.model.process.ElementParameterParser;
import org.talend.core.model.metadata.types.JavaTypesManager;
import org.talend.core.model.utils.NodeUtil;
import org.talend.core.model.utils.JavaResourcesHelper;
import org.talend.core.ui.branding.IBrandingService;
import org.talend.core.ui.branding.AbstractBrandingService;
import org.talend.designer.codegen.i18n.Messages;
import org.talend.designer.codegen.ITalendSynchronizer;
import org.talend.designer.codegen.config.CamelEndpointBuilder;
import org.talend.designer.codegen.config.CamelEndpointBuilder.BuildingValueParamAppender;
import org.talend.designer.codegen.config.CamelEndpointBuilder.ConditionParamAppender;
import org.talend.designer.codegen.config.CamelEndpointBuilder.NodeParamNotDefaultAppender;
import org.talend.designer.codegen.config.CodeGeneratorArgument;
import org.talend.designer.codegen.config.NodeParamsHelper;
import org.talend.designer.runprocess.CodeGeneratorRoutine;
import org.talend.core.model.process.EParameterFieldType;
import org.talend.core.model.process.ProcessUtils;

public class Microservice_appJava
{
  protected static String nl;
  public static synchronized Microservice_appJava create(String lineSeparator)
  {
    nl = lineSeparator;
    Microservice_appJava result = new Microservice_appJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "package ";
  protected final String TEXT_2 = ";" + NL + NL;
  protected final String TEXT_3 = NL + "        ";
  protected final String TEXT_4 = " ;";
  protected final String TEXT_5 = NL + "/**" + NL + " * Job: ";
  protected final String TEXT_6 = " Purpose: ";
  protected final String TEXT_7 = "<br>" + NL + " * Description: ESB microservice launcher application for ";
  protected final String TEXT_8 = "<br>" + NL + " * @author ";
  protected final String TEXT_9 = NL + " * @version ";
  protected final String TEXT_10 = NL + " * @status ";
  protected final String TEXT_11 = NL + " */" + NL + "" + NL + "@SpringBootApplication" + NL + "@Configuration" + NL + "@EnableAutoConfiguration" + NL + "@ImportResource({ \"classpath:META-INF/cxf/cxf.xml\" })" + NL + "public class App extends ";
  protected final String TEXT_12 = " implements ApplicationRunner {" + NL + "" + NL + "    @Autowired" + NL + "    Environment env;" + NL + "" + NL + "    public App() {" + NL + "        setRunInTalendEsbRuntimeContainer(true);" + NL + "        setRunInDaemonMode(false);" + NL + "        runJobInTOS(mainArgs);" + NL + "    }" + NL + "" + NL + "    @Autowired" + NL + "    private ApplicationContext applicationContext;" + NL + "" + NL + "    public static void main( String[] args )" + NL + "    {" + NL + "        String[] resetArgs = resetArgs(args);" + NL + "        SpringApplication.run(App.class, resetArgs);" + NL + "    }" + NL + "" + NL + "    @Bean" + NL + "    public org.apache.cxf.endpoint.Server restServer() {" + NL + "        " + NL + "        Thread4RestServiceProviderEndpoint thread4RestServiceProviderEndpoint = new Thread4RestServiceProviderEndpoint(this," + NL + "                getCXFRSEndpointAddress(restEndpoint));" + NL + "        JAXRSServerFactoryBean sf = thread4RestServiceProviderEndpoint.getJAXRSServerFactoryBean();" + NL + "        sf.setBus(springBus());" + NL + "        " + NL + "        sf.getFeatures().add(new org.apache.cxf.metrics.MetricsFeature(new org.apache.cxf.metrics.codahale.CodahaleMetricsProvider(sf.getBus())));" + NL + "        ";
  protected final String TEXT_13 = NL + "\t\t\tsetBus(sf.getBus());" + NL + "\t\t";
  protected final String TEXT_14 = NL + "        List providers = sf.getProviders();" + NL + "        // JAASAuthenticationFilter jaasAuthenticationFilter = new JAASAuthenticationFilter();" + NL + "        // jaasAuthenticationFilter.setContextName(\"karaf\");" + NL + "        // providers.add(jaasAuthenticationFilter);" + NL + "        // sf.setProviders(providers);" + NL + "        ";
  protected final String TEXT_15 = NL + "        " + NL + "\t\tjava.util.Map<String, Object> securityProps = null;" + NL + "\t\t" + NL + "\t\t";
  protected final String TEXT_16 = NL + "\t\t\torg.apache.cxf.interceptor.Interceptor authorizationInterceptor;" + NL + "\t\t\t" + NL + "\t\t\tjava.util.Properties pepProps = new java.util.Properties();" + NL + "\t\t\tjava.io.InputStream pepCfg = getConfigLocation(\"org.talend.esb.authorization.pep.cfg\");" + NL + "\t\t\t" + NL + "\t\t\ttry {" + NL + "\t\t\t    pepProps.load(pepCfg);" + NL + "\t\t\t    org.talend.esb.authorization.xacml.rt.pep.CXFXACMLAuthorizingInterceptor _authorizationInterceptor = new org.talend.esb.authorization.xacml.rt.pep.CXFXACMLAuthorizingInterceptor();" + NL + "\t\t\t    _authorizationInterceptor.setPdpAddress(pepProps.getProperty(\"tesb.pdp.address\"));" + NL + "\t\t\t    authorizationInterceptor = (org.apache.cxf.interceptor.Interceptor) _authorizationInterceptor;" + NL + "\t\t\t" + NL + "\t\t\t} catch (Exception e) {" + NL + "\t\t\t    throw new RuntimeException(\"Cannot load pep properties\", e);" + NL + "\t\t\t}" + NL + "\t\t";
  protected final String TEXT_17 = NL + "\t\t" + NL + "\t\tjava.util.Properties serviceProps = new java.util.Properties();" + NL + "\t\tjava.io.InputStream serviceCfg = getConfigLocation(\"org.talend.esb.job.service.cfg\");" + NL + "\t\t" + NL + "\t\ttry {" + NL + "\t\t    serviceProps.load(serviceCfg);" + NL + "\t\t    securityProps = new java.util.HashMap<String, Object>();" + NL + "\t\t    for (String key : serviceProps.stringPropertyNames()) {" + NL + "\t\t        securityProps.put(key, serviceProps.getProperty(key));" + NL + "\t\t    }" + NL + "\t\t} catch (Exception e) {" + NL + "\t\t    throw new RuntimeException(\"Cannot load service properties\", e);" + NL + "\t\t}" + NL + "\t\t" + NL + "\t\tif (null != securityProps) {" + NL + "\t\t    org.talend.esb.security.saml.SAMLRESTUtils.configureServer(sf, securityProps);" + NL + "\t\t    ";
  protected final String TEXT_18 = NL + "\t\t    \tsf.getInInterceptors().add(authorizationInterceptor);" + NL + "\t\t    ";
  protected final String TEXT_19 = NL + "\t\t}" + NL;
  protected final String TEXT_20 = NL + NL + "        thread4RestServiceProviderEndpoint.run();" + NL + "        " + NL + "        return thread4RestServiceProviderEndpoint.getServer();" + NL + "        " + NL + "    }" + NL + "    " + NL + "\t@Bean" + NL + "\tpublic com.codahale.metrics.MetricRegistry metricRegistry() {" + NL + "\t\treturn new com.codahale.metrics.MetricRegistry();" + NL + "\t}" + NL + "" + NL + "\t@Bean(initMethod = \"start\", destroyMethod = \"stop\")" + NL + "\tpublic com.codahale.metrics.JmxReporter jmxReporter() {" + NL + "\t\treturn com.codahale.metrics.JmxReporter.forRegistry(metricRegistry()).inDomain(\"org.apache.cxf\").createsObjectNamesWith(new com.codahale.metrics.ObjectNameFactory() {" + NL + "            public javax.management.ObjectName createName(String type, String domain, String name) {" + NL + "                try {" + NL + "                \tif(name.startsWith(\"org.apache.cxf\")){" + NL + "                \t\treturn new javax.management.ObjectName(name);" + NL + "                \t}else{" + NL + "                \t\treturn new com.codahale.metrics.DefaultObjectNameFactory().createName(type, domain, name);" + NL + "                \t}" + NL + "                    " + NL + "                } catch (javax.management.MalformedObjectNameException e) {" + NL + "                    throw new RuntimeException(e);" + NL + "                }" + NL + "            }" + NL + "        }).build();" + NL + "\t}" + NL + "" + NL + "    @Bean" + NL + "    public ServletRegistrationBean servletRegistrationBean(ApplicationContext context) {" + NL + "        return new ServletRegistrationBean(new CXFServlet(), \"/services/*\");" + NL + "    }" + NL + "" + NL + "    private String[] mainArgs = new String[] {};" + NL + "" + NL + "    public void run(ApplicationArguments args) throws Exception {" + NL + "        String[] ma = args.getSourceArgs();" + NL + "        mainArgs = ma;" + NL + "    }" + NL + "    " + NL + "    public InputStream getConfigLocation(String fileName) {" + NL + "        InputStream stream = null;" + NL + "" + NL + "        String configFile = \"config/\" + fileName;" + NL + "" + NL + "        String configPath = this.env.getProperty(\"spring.config.location\");" + NL + "" + NL + "        String file = \"\";" + NL + "        if (configPath != null) {" + NL + "            file = configPath + File.separator + fileName;" + NL + "        } else {" + NL + "            file = System.getProperty(\"user.dir\") + File.separator + configFile;" + NL + "        }" + NL + "        File usersfile = new File(file);" + NL + "        if (usersfile.exists()) {" + NL + "            try {" + NL + "                stream = new FileInputStream(file);" + NL + "            } catch (Exception e) {" + NL + "                stream = getClass().getClassLoader().getResourceAsStream(configFile);" + NL + "            }" + NL + "        } else {" + NL + "            stream = getClass().getClassLoader().getResourceAsStream(configFile);" + NL + "        }" + NL + "        return stream;" + NL + "    }" + NL + "" + NL + "    private static void loadConfig(Map<String, String> argsMap, String configName, String configValue) {" + NL + "        String configFileValue = \"classpath:config/\" + configValue;" + NL + "        if (argsMap.get(configName) == null) {" + NL + "            if (argsMap.get(\"--spring.config.location\") == null) {" + NL + "                argsMap.put(configName, configFileValue);" + NL + "            } else {" + NL + "                String value = (String) argsMap.get(\"--spring.config.location\") + File.separator + configValue;" + NL + "                if (new File(value).exists()) {" + NL + "                    argsMap.put(configName, \"file:\" + value);" + NL + "                } else if (((String) argsMap.get(\"--spring.config.location\")).contains(\":\")) {" + NL + "                    try {" + NL + "                        if (new File(new URI(value)).exists()) {" + NL + "                            argsMap.put(configName, value);" + NL + "                        } else {" + NL + "                            argsMap.put(configName, configFileValue);" + NL + "                        }" + NL + "                    } catch (Exception e) {" + NL + "                        argsMap.put(configName, configFileValue);" + NL + "                    }" + NL + "                } else {" + NL + "                    argsMap.put(configName, configFileValue);" + NL + "                }" + NL + "            }" + NL + "        }" + NL + "    }" + NL + "    " + NL + "    private static String[] resetArgs(String... args) {" + NL + "        Map<String, String> argsMap = new HashMap<String, String>();" + NL + "" + NL + "        for (int i = 0; i < args.length; i++) {" + NL + "            String[] kv = args[i].split(\"=\");" + NL + "            argsMap.put(kv[0], kv[1]);" + NL + "        }" + NL + "" + NL + "        if (argsMap.get(\"--spring.config.location\") != null) {" + NL + "            System.setProperty(\"spring.config.location\", argsMap.get(\"--spring.config.location\"));" + NL + "        }" + NL + "        loadConfig(argsMap, \"--banner.location\", \"banner.txt\");" + NL + "        loadConfig(argsMap, \"--logging.config\", \"log4j2.xml\");" + NL + "" + NL + "        if (argsMap.get(\"--camel.springboot.typeConversion\") == null) {" + NL + "            argsMap.put(\"--camel.springboot.typeConversion\", \"false\");" + NL + "        }" + NL + "" + NL + "        String[] resetArgs = new String[argsMap.size()];" + NL + "" + NL + "        java.util.Set<String> keySet = argsMap.keySet();" + NL + "" + NL + "        int idx = 0;" + NL + "" + NL + "        for (String key : keySet) {" + NL + "            resetArgs[idx] = key + \"=\" + argsMap.get(key);" + NL + "            idx++;" + NL + "        }" + NL + "" + NL + "        return resetArgs;" + NL + "    }" + NL + "    " + NL + "    public String getCXFRSEndpointAddress(String endpointUrl) {" + NL + "        if (endpointUrl != null && !endpointUrl.trim().isEmpty() && !endpointUrl.contains(\"://\")) {" + NL + "            if (endpointUrl.startsWith(\"/services\")) {" + NL + "                endpointUrl = endpointUrl.substring(\"/services\".length());" + NL + "            }" + NL + "            if (!endpointUrl.startsWith(\"/\")) {" + NL + "                endpointUrl = '/' + endpointUrl;" + NL + "            }" + NL + "        }" + NL + "        return endpointUrl;" + NL + "    }" + NL + "    " + NL + "    @org.springframework.context.annotation.Bean(name = \"cxf\")" + NL + "    public org.apache.cxf.bus.spring.SpringBus springBus() {" + NL + "    \treturn new org.apache.cxf.bus.spring.SpringBus();" + NL + "    }" + NL + "" + NL + "}";

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    
	CodeGeneratorArgument codeGenArgument = (CodeGeneratorArgument) argument;
	Vector v = (Vector) codeGenArgument.getArgument();
    IProcess process = (IProcess) v.get(0);
	String version = (String) v.get(1);
	List<? extends INode> graphicalNodes = process.getGraphicalNodes();
    String jobFolderName = "";
    IProcess baseProcess = ProcessUtils.getTestContainerBaseProcess(process);
    if (baseProcess != null) {
        jobFolderName = JavaResourcesHelper.getJobFolderName(baseProcess.getName(), baseProcess.getVersion()) + ".";
    }
    jobFolderName = jobFolderName + JavaResourcesHelper.getJobFolderName(process.getName(), process.getVersion());
    String packageName = codeGenArgument.getCurrentProjectName().toLowerCase() + "." + jobFolderName;
    String className = process.getName();
    
    // codegen variables
    boolean enableSAMLToken = false;
    

    stringBuffer.append(TEXT_1);
    stringBuffer.append( packageName );
    stringBuffer.append(TEXT_2);
    
    java.util.Set<String> importsSet = new java.util.TreeSet<String>();
	importsSet.add("import java.io.File");
	importsSet.add("import java.io.FileInputStream");
	importsSet.add("import java.io.InputStream");
	importsSet.add("import java.net.URI");
	importsSet.add("import java.util.HashMap");
	importsSet.add("import java.util.List");
	importsSet.add("import java.util.Map");
	importsSet.add("import java.util.Properties");
	
	importsSet.add("import org.apache.cxf.Bus");
	importsSet.add("import org.apache.cxf.jaxrs.JAXRSServerFactoryBean");
	importsSet.add("import org.apache.cxf.transport.servlet.CXFServlet");
	importsSet.add("import org.springframework.beans.factory.annotation.Autowired");
	importsSet.add("import org.springframework.boot.ApplicationArguments");
	importsSet.add("import org.springframework.boot.ApplicationRunner");
	importsSet.add("import org.springframework.boot.SpringApplication");
	importsSet.add("import org.springframework.boot.autoconfigure.EnableAutoConfiguration");
	importsSet.add("import org.springframework.boot.autoconfigure.SpringBootApplication");
	importsSet.add("import org.springframework.boot.web.servlet.ServletRegistrationBean");
	importsSet.add("import org.springframework.context.ApplicationContext");
	importsSet.add("import org.springframework.context.annotation.Bean");
	importsSet.add("import org.springframework.context.annotation.Configuration");
	importsSet.add("import org.springframework.context.annotation.ImportResource");
	importsSet.add("import org.springframework.core.env.Environment");
	
    for(String s: importsSet){

    stringBuffer.append(TEXT_3);
    stringBuffer.append(s);
    stringBuffer.append(TEXT_4);
    
    }
    
    // all initalize
    boolean needAuthorization = false;
    for (INode node : graphicalNodes) {
    	if ("tRESTRequest".equals(node.getComponent().getName())) {
    		boolean needAuth = "true".equals(ElementParameterParser.getValue(node, "__NEED_AUTH__"));
    		needAuthorization = "true".equals(ElementParameterParser.getValue(node, "__NEED_AUTHORIZATION__"));
			if (needAuth &&"SAML".equals(ElementParameterParser.getValue(node, "__AUTH_TYPE__"))) {
				enableSAMLToken = true;
			}
    	}
    }

    stringBuffer.append(TEXT_5);
    stringBuffer.append(className);
    stringBuffer.append(TEXT_6);
    stringBuffer.append(ElementParameterParser.getValue(process, "__PURPOSE__") );
    stringBuffer.append(TEXT_7);
    stringBuffer.append(className);
    stringBuffer.append(TEXT_8);
    stringBuffer.append(ElementParameterParser.getValue(process, "__AUTHOR__") );
    stringBuffer.append(TEXT_9);
    stringBuffer.append(version );
    stringBuffer.append(TEXT_10);
    stringBuffer.append(ElementParameterParser.getValue(process, "__STATUS__") );
    stringBuffer.append(TEXT_11);
    stringBuffer.append(className);
    stringBuffer.append(TEXT_12);
            
		if(enableSAMLToken){
		
    stringBuffer.append(TEXT_13);
    }
    stringBuffer.append(TEXT_14);
            
if(enableSAMLToken){

    stringBuffer.append(TEXT_15);
    if(needAuthorization){
    stringBuffer.append(TEXT_16);
    }
    stringBuffer.append(TEXT_17);
    if(needAuthorization){
    stringBuffer.append(TEXT_18);
    }
    stringBuffer.append(TEXT_19);
    
} // end if enableSAMLToken

    stringBuffer.append(TEXT_20);
    return stringBuffer.toString();
  }
}
