package org.talend.designer.codegen.translators.processing;

import org.talend.core.model.metadata.IMetadataTable;
import org.talend.core.model.metadata.IMetadataColumn;
import org.talend.core.model.process.IConnection;
import org.talend.core.model.process.IConnectionCategory;
import org.talend.core.model.process.INode;
import org.talend.core.model.process.ElementParameterParser;
import org.talend.core.model.utils.TalendTextUtils;
import org.talend.designer.codegen.config.CodeGeneratorArgument;

public class THMapEndJava
{
  protected static String nl;
  public static synchronized THMapEndJava create(String lineSeparator)
  {
    nl = lineSeparator;
    THMapEndJava result = new THMapEndJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "";
  protected final String TEXT_2 = NL;
  protected final String TEXT_3 = NL + "    java.io.InputStream is = (java.io.InputStream)";
  protected final String TEXT_4 = ".";
  protected final String TEXT_5 = ";" + NL + "    is.close();" + NL + "  " + NL + "    org.talend.transform.runtime.common.MapExecutor mapExec_";
  protected final String TEXT_6 = "_end = (org.talend.transform.runtime.common.MapExecutor)";
  protected final String TEXT_7 = ".this.globalMap.get(Thread" + NL + "            .currentThread().getId()+\"_";
  protected final String TEXT_8 = "_\"+\"mapExecutor\");" + NL + "" + NL + "    org.talend.transform.runtime.common.MapExecutionStatus results_";
  protected final String TEXT_9 = " = mapExec_";
  protected final String TEXT_10 = "_end.getLastExecutionStatus(\"";
  protected final String TEXT_11 = "\");" + NL + "    mapExec_";
  protected final String TEXT_12 = "_end.freeExecutionResources(\"";
  protected final String TEXT_13 = "\");";
  protected final String TEXT_14 = NL + "    ";
  protected final String TEXT_15 = ".this.globalMap.put(\"";
  protected final String TEXT_16 = "_\"+\"EXECUTION_STATUS\",results_";
  protected final String TEXT_17 = ");";
  protected final String TEXT_18 = NL + "    ";
  protected final String TEXT_19 = ".this.globalMap.put(\"";
  protected final String TEXT_20 = "_\"+\"EXECUTION_SEVERITY\",results_";
  protected final String TEXT_21 = ".getHighestSeverityLevel().getNumValue());" + NL + "    if (results_";
  protected final String TEXT_22 = ".getHighestSeverityLevel().isGreaterOrEqualsTo(";
  protected final String TEXT_23 = ")) {" + NL + "       throw new TalendException(new java.lang.Exception(String.valueOf(results_";
  protected final String TEXT_24 = ")),currentComponent,globalMap);" + NL + "    }";
  protected final String TEXT_25 = NL + NL + "    if (results_";
  protected final String TEXT_26 = ".getHighestSeverityLevel().isGreaterThan(org.talend.transform.runtime.common.SeverityLevel.INFO)) {" + NL + "        System.err.println(results_";
  protected final String TEXT_27 = ");" + NL + "    }";
  protected final String TEXT_28 = NL + NL;
  protected final String TEXT_29 = NL;

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    stringBuffer.append(TEXT_1);
    
    CodeGeneratorArgument codeGenArgument = (CodeGeneratorArgument) argument;
    INode node = (INode)codeGenArgument.getArgument();
    String this_cid = ElementParameterParser.getValue(node, "__UNIQUE_NAME__");
    String tHMap_id = this_cid.replace("_THMAP_IN", "");
    String cid = tHMap_id;
  
    Integer exceptionLevel = Integer.parseInt(ElementParameterParser.getValue(node, "__EXCEPTION__"));
    boolean asInputstream = "true".equals(ElementParameterParser.getValue(node, "__AS_INPUTSTREAM__"));
    String processName = org.talend.core.model.utils.JavaResourcesHelper.getSimpleClassName(node.getProcess());

    stringBuffer.append(TEXT_2);
    
    if (node.getOutgoingConnections()!=null) {
        java.util.Set<IConnection> dataConns = new java.util.HashSet<IConnection>();
        for (IConnection outgoingConn : node.getOutgoingConnections()) {
            if (outgoingConn.getLineStyle().hasConnectionCategory(IConnectionCategory.DATA)) {
                dataConns.add(outgoingConn);
            }
        }
        for (IConnection outgoingConn : node.getOutgoingConnections()) {
            if (outgoingConn.getLineStyle().hasConnectionCategory(IConnectionCategory.DATA)) {
                String outputConnName = outgoingConn.getName();
                IMetadataTable outputMetadataTable = outgoingConn.getMetadataTable();
                for (IMetadataColumn outputCol : outputMetadataTable.getListColumns()) {
                    if (asInputstream) {

    stringBuffer.append(TEXT_3);
    stringBuffer.append(outgoingConn.getName() );
    stringBuffer.append(TEXT_4);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_5);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_6);
    stringBuffer.append( processName );
    stringBuffer.append(TEXT_7);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_8);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_9);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_10);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_11);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_12);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_13);
    stringBuffer.append(TEXT_14);
    stringBuffer.append( processName );
    stringBuffer.append(TEXT_15);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_16);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_17);
    stringBuffer.append(TEXT_18);
    stringBuffer.append( processName );
    stringBuffer.append(TEXT_19);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_20);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_21);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_22);
    stringBuffer.append(exceptionLevel);
    stringBuffer.append(TEXT_23);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_24);
    
                        if(!org.talend.designer.runprocess.ProcessorUtilities.isExportConfig()){

    stringBuffer.append(TEXT_25);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_26);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_27);
    
                        } // (!org.talend.designer.runprocess
                        break;
                    } // if (asInputstream)
                } // for (IMetadataColumn outputCol
                break;
            } // if (outgoingConn.getLineStyle()
        } //  for (IConnection outgoingConn
    } // if (node.getOutgoingConnections()!=null)

    stringBuffer.append(TEXT_28);
    stringBuffer.append(TEXT_29);
    return stringBuffer.toString();
  }
}
