package org.talend.designer.codegen.translators.databases.maprdb;

import org.talend.core.model.metadata.IMetadataColumn;
import org.talend.core.model.metadata.IMetadataTable;
import org.talend.core.model.process.EConnectionType;
import org.talend.core.model.process.ElementParameterParser;
import org.talend.core.model.process.IConnection;
import org.talend.core.model.process.INode;
import org.talend.designer.common.BigDataCodeGeneratorArgument;

public class TMapRDBOutputSparkstreamingconfigJava
{
  protected static String nl;
  public static synchronized TMapRDBOutputSparkstreamingconfigJava create(String lineSeparator)
  {
    nl = lineSeparator;
    TMapRDBOutputSparkstreamingconfigJava result = new TMapRDBOutputSparkstreamingconfigJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "        System.setProperty(\"pname\", \"MapRLogin\");" + NL + "        System.setProperty(\"https.protocols\", \"TLSv1.2\");" + NL + "        System.setProperty(\"mapr.home.dir\", ";
  protected final String TEXT_2 = ");" + NL + "        System.setProperty(\"hadoop.login\", ";
  protected final String TEXT_3 = ");";
  protected final String TEXT_4 = NL + "    job.set(\"hbase.master.kerberos.principal\",";
  protected final String TEXT_5 = ");" + NL + "    job.set(\"hbase.regionserver.kerberos.principal\",";
  protected final String TEXT_6 = ");" + NL + "    job.set(\"hbase.security.authorization\",\"true\");" + NL + "    job.set(\"hbase.security.authentication\",\"kerberos\");" + NL + "    job.set(\"hbase.zookeeper.quorum\", ";
  protected final String TEXT_7 = ");" + NL + "    job.set(\"hbase.zookeeper.property.clientPort\", ";
  protected final String TEXT_8 = ");";
  protected final String TEXT_9 = NL + "        job.set(\"zookeeper.znode.parent\", ";
  protected final String TEXT_10 = ");";
  protected final String TEXT_11 = NL + "        org.apache.hadoop.security.UserGroupInformation.loginUserFromKeytab(";
  protected final String TEXT_12 = ", ";
  protected final String TEXT_13 = ");";
  protected final String TEXT_14 = NL + "        com.mapr.login.client.MapRLoginHttpsClient maprLogin_";
  protected final String TEXT_15 = " = new com.mapr.login.client.MapRLoginHttpsClient();" + NL + "        maprLogin_";
  protected final String TEXT_16 = ".getMapRCredentialsViaKerberos(";
  protected final String TEXT_17 = ", ";
  protected final String TEXT_18 = ");";
  protected final String TEXT_19 = NL + "        // Get MapReduce job authentication token from Hbase";
  protected final String TEXT_20 = NL + "        org.apache.hadoop.hbase.security.token.TokenUtil.obtainTokenForJob(job, org.apache.hadoop.security.UserGroupInformation.getCurrentUser());";
  protected final String TEXT_21 = NL + "            final String decryptedPasswordhbase_";
  protected final String TEXT_22 = " = routines.system.PasswordEncryptUtil.decryptPassword(";
  protected final String TEXT_23 = ");";
  protected final String TEXT_24 = NL + "            final String decryptedPasswordhbase_";
  protected final String TEXT_25 = " = ";
  protected final String TEXT_26 = ";";
  protected final String TEXT_27 = NL + "        System.setProperty(\"pname\", \"MapRLogin\");" + NL + "        System.setProperty(\"https.protocols\", \"TLSv1.2\");" + NL + "        System.setProperty(\"mapr.home.dir\", ";
  protected final String TEXT_28 = ");" + NL + "        com.mapr.login.client.MapRLoginHttpsClient maprLogin_";
  protected final String TEXT_29 = " = new com.mapr.login.client.MapRLoginHttpsClient();";
  protected final String TEXT_30 = NL + "            System.setProperty(\"hadoop.login\", ";
  protected final String TEXT_31 = ");";
  protected final String TEXT_32 = NL + "            maprLogin_";
  protected final String TEXT_33 = ".setCheckUGI(false);";
  protected final String TEXT_34 = NL + "        ";
  protected final String TEXT_35 = NL + "            maprLogin_";
  protected final String TEXT_36 = ".getMapRCredentialsViaPassword(";
  protected final String TEXT_37 = ", ";
  protected final String TEXT_38 = ", decryptedPasswordhbase_";
  protected final String TEXT_39 = ", ";
  protected final String TEXT_40 = ", \"\");";
  protected final String TEXT_41 = NL + "            maprLogin_";
  protected final String TEXT_42 = ".getMapRCredentialsViaPassword(";
  protected final String TEXT_43 = ", ";
  protected final String TEXT_44 = ", decryptedPasswordhbase_";
  protected final String TEXT_45 = ", ";
  protected final String TEXT_46 = ");";
  protected final String TEXT_47 = NL + "\tif ((ctx.sparkContext().getConf().get(\"spark.hadoop.yarn.application.classpath\") != null)" + NL + "\t\t\t&& (!ctx.sparkContext().getConf().get(\"spark.hadoop.yarn.application.classpath\").toLowerCase().contains(\"hbase\"))) {" + NL + "\t\t";
  protected final String TEXT_48 = NL + "\t\t\tlog.warn(\"Your YARN application classpath does not contain the HBase classpath infomation.\"" + NL + "\t\t\t\t\t+ \"This could prevent the MapRDB components from working correctly. To resolve this issue, see: \"" + NL + "\t\t\t\t\t+ \"https://help.talend.com/pages/viewpage.action?pageId=272320939\");" + NL + "\t\t";
  protected final String TEXT_49 = NL + "\t\t\tSystem.out.println(\"Your YARN application classpath does not contain the HBase classpath infomation.\"" + NL + "\t\t\t\t\t+ \"This could prevent the MapRDB components from working correctly. To resolve this issue, see: \"" + NL + "\t\t\t\t\t+ \"https://help.talend.com/pages/viewpage.action?pageId=272320939\");" + NL + "\t\t";
  protected final String TEXT_50 = NL + "\t}" + NL + "\tjob.set(\"hbase.table.namespace.mappings\", ";
  protected final String TEXT_51 = ");" + NL + "\t";
  protected final String TEXT_52 = NL + "final JobConf job";
  protected final String TEXT_53 = " = new JobConf(job);" + NL + "job";
  protected final String TEXT_54 = ".setOutputFormat(";
  protected final String TEXT_55 = "StructOutputFormat.class);" + NL + "rdd_";
  protected final String TEXT_56 = ".foreachRDD(new ";
  protected final String TEXT_57 = "_ForeachRDDOutput(job";
  protected final String TEXT_58 = "));";
  protected final String TEXT_59 = NL;

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    
// Parse the inputs to this javajet generator.
final BigDataCodeGeneratorArgument codeGenArgument = (BigDataCodeGeneratorArgument) argument;
final INode node = (INode) codeGenArgument.getArgument();
final String cid = node.getUniqueName();

INode configurationNode = null;
String configurationNodeName = ElementParameterParser.getValue(node,"__STORAGE_CONFIGURATION__");

for (INode pNode : node.getProcess().getNodesOfType("tHBaseConfiguration")) {
    if(configurationNodeName!=null && configurationNodeName.equals(pNode.getUniqueName())) {
        configurationNode = pNode;
        break;
    }
}
if (configurationNode == null) { // MapRDBMode
	for (INode pNode : node.getProcess().getNodesOfType("tMapRDBConfiguration")) {
	    if(configurationNodeName!=null && configurationNodeName.equals(pNode.getUniqueName())) {
	        configurationNode = pNode;
	        break;
	    }
    }
}

// SparkConfiguration to know whether we are in Yarn Cluster mode
final java.util.List<? extends INode> sparkConfigs = node.getProcess().getNodesOfType("tSparkConfiguration");
INode sparkConfig = null;
if(sparkConfigs != null && sparkConfigs.size() > 0) {
    sparkConfig = sparkConfigs.get(0);
}
boolean useYarnClusterMode = false;
if(sparkConfig != null) {
   String sparkMode = ElementParameterParser.getValue(sparkConfig, "__SPARK_MODE__");
	boolean useLocalMode = "true".equals(ElementParameterParser.getValue(sparkConfig, "__SPARK_LOCAL_MODE__"));
	useYarnClusterMode = !useLocalMode && "YARN_CLUSTER".equals(sparkMode);
}

// Input Connections
IConnection inMainCon = null;
java.util.List<? extends IConnection> connsIn = node.getIncomingConnections(EConnectionType.FLOW_MAIN);
if (connsIn == null || connsIn.size() == 0 ){
    return "";
} else{
    inMainCon = connsIn.get(0);
}
final String incomingConnectionName = inMainCon.getName();
final String inConnTypeName = codeGenArgument.getRecordStructName(inMainCon);

boolean isLog4jEnabled = ("true").equals(ElementParameterParser.getValue(node.getProcess(), "__LOG4J_ACTIVATE__"));
String zookeeperUrl = ElementParameterParser.getValue(configurationNode,"__ZOOKEEPER_QUORUM__");
String zookeeperPort = ElementParameterParser.getValue(configurationNode,"__ZOOKEEPER_CLIENT_PORT__");
boolean useKrb = "true".equals(ElementParameterParser.getValue(configurationNode, "__USE_KRB__"));
boolean useKeytab = "true".equals(ElementParameterParser.getValue(configurationNode, "__USE_KEYTAB__"));
String userPrincipal = ElementParameterParser.getValue(configurationNode, "__PRINCIPAL__");
String keytabPath = ElementParameterParser.getValue(configurationNode, "__KEYTAB_PATH__");
String hbaseMasterPrincipal = ElementParameterParser.getValue(configurationNode, "__HBASE_MASTER_PRINCIPAL__");
String hbaseRegionServerPrincipal = ElementParameterParser.getValue(configurationNode, "__HBASE_REGIONSERVER_PRINCIPAL__");
boolean setZNodeParent = "true".equals(ElementParameterParser.getValue(configurationNode, "__SET_ZNODE_PARENT__"));
String zNodeParent = ElementParameterParser.getValue(configurationNode, "__ZNODE_PARENT__");

String hbaseDistribution = ElementParameterParser.getValue(configurationNode, "__DISTRIBUTION__");
String hbaseVersion = ElementParameterParser.getValue(configurationNode, "__HBASE_VERSION__");

boolean useMapRTicket = ElementParameterParser.getBooleanValue(configurationNode, "__USE_MAPRTICKET__");
String username = ElementParameterParser.getValue(configurationNode, "__USERNAME__");
String mapRTicketCluster = ElementParameterParser.getValue(configurationNode, "__MAPRTICKET_CLUSTER__");
String mapRTicketDuration = ElementParameterParser.getValue(configurationNode, "__MAPRTICKET_DURATION__");

boolean setMapRHomeDir = ElementParameterParser.getBooleanValue(configurationNode, "__SET_MAPR_HOME_DIR__");
String mapRHomeDir = ElementParameterParser.getValue(configurationNode, "__MAPR_HOME_DIR__");

boolean setMapRHadoopLogin = ElementParameterParser.getBooleanValue(configurationNode, "__SET_HADOOP_LOGIN__");
String mapRHadoopLogin = ElementParameterParser.getValue(configurationNode, "__HADOOP_LOGIN__");
    boolean setTableNsMapping = "true".equals(ElementParameterParser.getValue(node, "__SET_TABLE_NS_MAPPING__"));
    String tableNsMapping = ElementParameterParser.getValue(node, "__TABLE_NS_MAPPING__");
    boolean useTableNsMapping = setTableNsMapping && ((tableNsMapping != null)&&(!tableNsMapping.equals("")));

org.talend.hadoop.distribution.component.HBaseComponent hbaseDistrib = null;
try {
    hbaseDistrib = (org.talend.hadoop.distribution.component.HBaseComponent) org.talend.hadoop.distribution.DistributionFactory.buildDistribution(hbaseDistribution, hbaseVersion);
} catch (java.lang.Exception e) {
    e.printStackTrace();
    return "";
}

boolean isCustom = hbaseDistrib instanceof org.talend.hadoop.distribution.custom.CustomDistribution;
if((isCustom || hbaseDistrib.doSupportKerberos()) && useKrb){
    if ((isCustom || hbaseDistrib.doSupportMapRTicket()) && useMapRTicket) {

    stringBuffer.append(TEXT_1);
    stringBuffer.append(setMapRHomeDir ? mapRHomeDir : "\"/opt/mapr\"" );
    stringBuffer.append(TEXT_2);
    stringBuffer.append(setMapRHadoopLogin ? mapRHadoopLogin : "\"kerberos\"");
    stringBuffer.append(TEXT_3);
    
    }

    stringBuffer.append(TEXT_4);
    stringBuffer.append(hbaseMasterPrincipal);
    stringBuffer.append(TEXT_5);
    stringBuffer.append(hbaseRegionServerPrincipal);
    stringBuffer.append(TEXT_6);
    stringBuffer.append(zookeeperUrl);
    stringBuffer.append(TEXT_7);
    stringBuffer.append(zookeeperPort);
    stringBuffer.append(TEXT_8);
    
    if (setZNodeParent) {

    stringBuffer.append(TEXT_9);
    stringBuffer.append(zNodeParent);
    stringBuffer.append(TEXT_10);
    
    }
    if(useKeytab && !useYarnClusterMode){

    stringBuffer.append(TEXT_11);
    stringBuffer.append(userPrincipal);
    stringBuffer.append(TEXT_12);
    stringBuffer.append(keytabPath);
    stringBuffer.append(TEXT_13);
    
    }
    if ((isCustom || hbaseDistrib.doSupportMapRTicket()) && useMapRTicket) {

    stringBuffer.append(TEXT_14);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_15);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_16);
    stringBuffer.append(mapRTicketCluster);
    stringBuffer.append(TEXT_17);
    stringBuffer.append(mapRTicketDuration);
    stringBuffer.append(TEXT_18);
    
    } else {

    stringBuffer.append(TEXT_19);
    
		if (!useYarnClusterMode) {

    stringBuffer.append(TEXT_20);
    
		}
	}
} else {
    // MapR ticket
    if ((isCustom || hbaseDistrib.doSupportMapRTicket()) && useMapRTicket) {
        String passwordFieldName = "__MAPRTICKET_PASSWORD__";
        if (ElementParameterParser.canEncrypt(configurationNode, passwordFieldName)) {
    stringBuffer.append(TEXT_21);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_22);
    stringBuffer.append(ElementParameterParser.getEncryptedValue(configurationNode, passwordFieldName));
    stringBuffer.append(TEXT_23);
    } else {
    stringBuffer.append(TEXT_24);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_25);
    stringBuffer.append( ElementParameterParser.getValue(configurationNode, passwordFieldName));
    stringBuffer.append(TEXT_26);
    }
    stringBuffer.append(TEXT_27);
    stringBuffer.append(setMapRHomeDir ? mapRHomeDir : "\"/opt/mapr\"" );
    stringBuffer.append(TEXT_28);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_29);
    
        if (setMapRHadoopLogin) {
            
    stringBuffer.append(TEXT_30);
    stringBuffer.append(mapRHadoopLogin);
    stringBuffer.append(TEXT_31);
    
        } else {
            
    stringBuffer.append(TEXT_32);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_33);
    
        }
        
    stringBuffer.append(TEXT_34);
    
        if(hbaseDistrib.doSupportMaprTicketV52API()){
            
    stringBuffer.append(TEXT_35);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_36);
    stringBuffer.append(mapRTicketCluster);
    stringBuffer.append(TEXT_37);
    stringBuffer.append(username);
    stringBuffer.append(TEXT_38);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_39);
    stringBuffer.append(mapRTicketDuration);
    stringBuffer.append(TEXT_40);
    
        } else {
            
    stringBuffer.append(TEXT_41);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_42);
    stringBuffer.append(mapRTicketCluster);
    stringBuffer.append(TEXT_43);
    stringBuffer.append(username);
    stringBuffer.append(TEXT_44);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_45);
    stringBuffer.append(mapRTicketDuration);
    stringBuffer.append(TEXT_46);
    
        }

    }
}
if(hbaseDistrib != null && hbaseDistrib.doSupportMapRDB() && useTableNsMapping){
	
    stringBuffer.append(TEXT_47);
    if(isLog4jEnabled){
    stringBuffer.append(TEXT_48);
    } else {
    stringBuffer.append(TEXT_49);
    }
    stringBuffer.append(TEXT_50);
    stringBuffer.append(tableNsMapping);
    stringBuffer.append(TEXT_51);
    
}

    stringBuffer.append(TEXT_52);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_53);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_54);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_55);
    stringBuffer.append(incomingConnectionName);
    stringBuffer.append(TEXT_56);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_57);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_58);
    

    stringBuffer.append(TEXT_59);
    return stringBuffer.toString();
  }
}
