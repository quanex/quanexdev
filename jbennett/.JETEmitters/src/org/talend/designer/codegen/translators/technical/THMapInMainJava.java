package org.talend.designer.codegen.translators.technical;

import org.talend.transform.component.thmap.MapperComponent;
import org.talend.core.model.metadata.IMetadataTable;
import org.talend.core.model.metadata.IMetadataColumn;
import org.talend.core.model.metadata.types.JavaTypesManager;
import org.talend.core.model.process.IConnection;
import org.talend.core.model.process.IConnectionCategory;
import org.talend.core.model.process.INode;
import org.talend.core.model.process.ElementParameterParser;
import org.talend.core.model.utils.TalendTextUtils;
import org.talend.designer.codegen.config.CodeGeneratorArgument;

public class THMapInMainJava
{
  protected static String nl;
  public static synchronized THMapInMainJava create(String lineSeparator)
  {
    nl = lineSeparator;
    THMapInMainJava result = new THMapInMainJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "    //THMAPIN_MAIN tHMap: ";
  protected final String TEXT_2 = NL;
  protected final String TEXT_3 = "    java.util.List<java.lang.Object> rows_";
  protected final String TEXT_4 = " = new java.util.ArrayList<>();";
  protected final String TEXT_5 = NL + "    //Result for multiple connections" + NL + "    java.util.Map<java.lang.String, javax.xml.transform.Result> resultsMapList_";
  protected final String TEXT_6 = " = (java.util.Map)";
  protected final String TEXT_7 = ".this.globalMap.get(Thread.currentThread().getId()+\"_";
  protected final String TEXT_8 = "_\"+\"outputResult\");";
  protected final String TEXT_9 = "   " + NL + "    //Result for single col connection: ";
  protected final String TEXT_10 = NL + "    javax.xml.transform.Result execResult_";
  protected final String TEXT_11 = "_";
  protected final String TEXT_12 = " = (javax.xml.transform.Result)";
  protected final String TEXT_13 = ".this.globalMap.get(Thread.currentThread().getId()+\"_";
  protected final String TEXT_14 = "_\"+\"outputResult\");";
  protected final String TEXT_15 = NL + "    //Main job's connection: ";
  protected final String TEXT_16 = " ; thMap: ";
  protected final String TEXT_17 = NL + "    javax.xml.transform.Result execResult_";
  protected final String TEXT_18 = "_";
  protected final String TEXT_19 = " = resultsMapList_";
  protected final String TEXT_20 = ".get(\"";
  protected final String TEXT_21 = "\");";
  protected final String TEXT_22 = NL + "\t //Connection ";
  protected final String TEXT_23 = " with object result type" + NL + "    org.talend.transform.io.ObjectResult objectResult_";
  protected final String TEXT_24 = "_";
  protected final String TEXT_25 = " = (org.talend.transform.io.ObjectResult)execResult_";
  protected final String TEXT_26 = "_";
  protected final String TEXT_27 = ";" + NL + "    java.util.List<java.util.Map<String, Object>> outputList_";
  protected final String TEXT_28 = "_";
  protected final String TEXT_29 = " = (java.util.List)objectResult_";
  protected final String TEXT_30 = "_";
  protected final String TEXT_31 = ".getObject();" + NL + "    if (outputList_";
  protected final String TEXT_32 = "_";
  protected final String TEXT_33 = " != null) {" + NL + "      for (java.util.Map<String, Object> outMap_";
  protected final String TEXT_34 = " : outputList_";
  protected final String TEXT_35 = "_";
  protected final String TEXT_36 = ") {";
  protected final String TEXT_37 = NL + "        ";
  protected final String TEXT_38 = " = new ";
  protected final String TEXT_39 = "Struct();\t       ";
  protected final String TEXT_40 = NL + "\t//Output as direct map column: ";
  protected final String TEXT_41 = ".";
  protected final String TEXT_42 = NL + "    if(outMap_";
  protected final String TEXT_43 = ".get(\"";
  protected final String TEXT_44 = "\")==null){";
  protected final String TEXT_45 = NL + "        ";
  protected final String TEXT_46 = ".";
  protected final String TEXT_47 = " = null;" + NL + "    }else if (outMap_";
  protected final String TEXT_48 = ".get(\"";
  protected final String TEXT_49 = "\") instanceof String) {";
  protected final String TEXT_50 = NL + "        ";
  protected final String TEXT_51 = ".";
  protected final String TEXT_52 = " = (String)outMap_";
  protected final String TEXT_53 = ".get(\"";
  protected final String TEXT_54 = "\");" + NL + "    }";
  protected final String TEXT_55 = NL + "    if(outMap_";
  protected final String TEXT_56 = ".get(\"";
  protected final String TEXT_57 = "\")==null){";
  protected final String TEXT_58 = NL + "        ";
  protected final String TEXT_59 = ".";
  protected final String TEXT_60 = " = false;" + NL + "    }else if (outMap_";
  protected final String TEXT_61 = ".get(\"";
  protected final String TEXT_62 = "\") instanceof Boolean) {";
  protected final String TEXT_63 = NL + "        ";
  protected final String TEXT_64 = ".";
  protected final String TEXT_65 = " = ((Boolean)outMap_";
  protected final String TEXT_66 = ".get(\"";
  protected final String TEXT_67 = "\")).booleanValue();" + NL + "    }";
  protected final String TEXT_68 = NL + "    if (outMap_";
  protected final String TEXT_69 = ".get(\"";
  protected final String TEXT_70 = "\")==null){";
  protected final String TEXT_71 = NL + "       ";
  protected final String TEXT_72 = ".";
  protected final String TEXT_73 = " = 0;" + NL + "    } else if (outMap_";
  protected final String TEXT_74 = ".get(\"";
  protected final String TEXT_75 = "\") instanceof Byte) {";
  protected final String TEXT_76 = NL + "       ";
  protected final String TEXT_77 = ".";
  protected final String TEXT_78 = " = ((Byte)outMap_";
  protected final String TEXT_79 = ".get(\"";
  protected final String TEXT_80 = "\")).byteValue();" + NL + "    }";
  protected final String TEXT_81 = NL + "    if (outMap_";
  protected final String TEXT_82 = ".get(\"";
  protected final String TEXT_83 = "\")==null){";
  protected final String TEXT_84 = NL + "       ";
  protected final String TEXT_85 = ".";
  protected final String TEXT_86 = " = 0;" + NL + "    } else if (outMap_";
  protected final String TEXT_87 = ".get(\"";
  protected final String TEXT_88 = "\") instanceof Character) {";
  protected final String TEXT_89 = NL + "       ";
  protected final String TEXT_90 = ".";
  protected final String TEXT_91 = " = ((Character)outMap_";
  protected final String TEXT_92 = ".get(\"";
  protected final String TEXT_93 = "\")).charValue();" + NL + "    } else if (outMap_";
  protected final String TEXT_94 = ".get(\"";
  protected final String TEXT_95 = "\") instanceof Short) {";
  protected final String TEXT_96 = NL + "       ";
  protected final String TEXT_97 = ".";
  protected final String TEXT_98 = " = (char)((Short)outMap_";
  protected final String TEXT_99 = ".get(\"";
  protected final String TEXT_100 = "\")).shortValue();" + NL + "    }";
  protected final String TEXT_101 = NL + "    if (outMap_";
  protected final String TEXT_102 = ".get(\"";
  protected final String TEXT_103 = "\")==null){";
  protected final String TEXT_104 = NL + "       ";
  protected final String TEXT_105 = ".";
  protected final String TEXT_106 = " = 0;" + NL + "    } else if (outMap_";
  protected final String TEXT_107 = ".get(\"";
  protected final String TEXT_108 = "\") instanceof Short) {";
  protected final String TEXT_109 = NL + "       ";
  protected final String TEXT_110 = ".";
  protected final String TEXT_111 = " = ((Short)outMap_";
  protected final String TEXT_112 = ".get(\"";
  protected final String TEXT_113 = "\")).shortValue();" + NL + "    }";
  protected final String TEXT_114 = NL + "    if (outMap_";
  protected final String TEXT_115 = ".get(\"";
  protected final String TEXT_116 = "\")==null){";
  protected final String TEXT_117 = NL + "       ";
  protected final String TEXT_118 = ".";
  protected final String TEXT_119 = " = 0;" + NL + "    } else if (outMap_";
  protected final String TEXT_120 = ".get(\"";
  protected final String TEXT_121 = "\") instanceof Integer) {";
  protected final String TEXT_122 = NL + "       ";
  protected final String TEXT_123 = ".";
  protected final String TEXT_124 = " = ((Integer)outMap_";
  protected final String TEXT_125 = ".get(\"";
  protected final String TEXT_126 = "\")).intValue();" + NL + "    }";
  protected final String TEXT_127 = NL + "    if (outMap_";
  protected final String TEXT_128 = ".get(\"";
  protected final String TEXT_129 = "\")==null){";
  protected final String TEXT_130 = NL + "       ";
  protected final String TEXT_131 = ".";
  protected final String TEXT_132 = " = (long)0;" + NL + "    } else if (outMap_";
  protected final String TEXT_133 = ".get(\"";
  protected final String TEXT_134 = "\") instanceof Long) {";
  protected final String TEXT_135 = NL + "       ";
  protected final String TEXT_136 = ".";
  protected final String TEXT_137 = " = ((Long)outMap_";
  protected final String TEXT_138 = ".get(\"";
  protected final String TEXT_139 = "\")).longValue();" + NL + "    }";
  protected final String TEXT_140 = NL + "    if (outMap_";
  protected final String TEXT_141 = ".get(\"";
  protected final String TEXT_142 = "\")==null){";
  protected final String TEXT_143 = NL + "       ";
  protected final String TEXT_144 = ".";
  protected final String TEXT_145 = " = 0.0f;" + NL + "    } else if (outMap_";
  protected final String TEXT_146 = ".get(\"";
  protected final String TEXT_147 = "\") instanceof Float) {";
  protected final String TEXT_148 = NL + "       ";
  protected final String TEXT_149 = ".";
  protected final String TEXT_150 = " = ((Float)outMap_";
  protected final String TEXT_151 = ".get(\"";
  protected final String TEXT_152 = "\")).floatValue();" + NL + "    }";
  protected final String TEXT_153 = NL + "    if (outMap_";
  protected final String TEXT_154 = ".get(\"";
  protected final String TEXT_155 = "\")==null){";
  protected final String TEXT_156 = NL + "       ";
  protected final String TEXT_157 = ".";
  protected final String TEXT_158 = " = 0.0;" + NL + "    } else if (outMap_";
  protected final String TEXT_159 = ".get(\"";
  protected final String TEXT_160 = "\") instanceof Double) {";
  protected final String TEXT_161 = NL + "       ";
  protected final String TEXT_162 = ".";
  protected final String TEXT_163 = " = ((Double)outMap_";
  protected final String TEXT_164 = ".get(\"";
  protected final String TEXT_165 = "\")).doubleValue();" + NL + "    }";
  protected final String TEXT_166 = NL + "    if (outMap_";
  protected final String TEXT_167 = ".get(\"";
  protected final String TEXT_168 = "\")==null){";
  protected final String TEXT_169 = NL + "        ";
  protected final String TEXT_170 = ".";
  protected final String TEXT_171 = " = null;" + NL + "    } else if (outMap_";
  protected final String TEXT_172 = ".get(\"";
  protected final String TEXT_173 = "\") instanceof java.math.BigDecimal) {";
  protected final String TEXT_174 = NL + "        ";
  protected final String TEXT_175 = ".";
  protected final String TEXT_176 = " = (java.math.BigDecimal)outMap_";
  protected final String TEXT_177 = ".get(\"";
  protected final String TEXT_178 = "\");" + NL + "    }";
  protected final String TEXT_179 = NL + "    if (outMap_";
  protected final String TEXT_180 = ".get(\"";
  protected final String TEXT_181 = "\")==null){";
  protected final String TEXT_182 = NL + "        ";
  protected final String TEXT_183 = ".";
  protected final String TEXT_184 = " = null;" + NL + "    } else if (outMap_";
  protected final String TEXT_185 = ".get(\"";
  protected final String TEXT_186 = "\") instanceof java.util.Date) {";
  protected final String TEXT_187 = NL + "        ";
  protected final String TEXT_188 = ".";
  protected final String TEXT_189 = " = (java.util.Date)outMap_";
  protected final String TEXT_190 = ".get(\"";
  protected final String TEXT_191 = "\");" + NL + "    }";
  protected final String TEXT_192 = "\t\t\t\t\t\t";
  protected final String TEXT_193 = "  " + NL + "    //Output as string for single column connection" + NL + "    javax.xml.transform.stream.StreamResult streamResult_";
  protected final String TEXT_194 = "_";
  protected final String TEXT_195 = " = (javax.xml.transform.stream.StreamResult)execResult_";
  protected final String TEXT_196 = "_";
  protected final String TEXT_197 = ";" + NL + "    java.io.StringWriter swOut_";
  protected final String TEXT_198 = "_";
  protected final String TEXT_199 = " = (java.io.StringWriter)streamResult_";
  protected final String TEXT_200 = "_";
  protected final String TEXT_201 = ".getWriter();\t" + NL + "    if (swOut_";
  protected final String TEXT_202 = "_";
  protected final String TEXT_203 = " != null) {";
  protected final String TEXT_204 = NL + "      ";
  protected final String TEXT_205 = " = new ";
  protected final String TEXT_206 = "Struct();\t";
  protected final String TEXT_207 = NL + "      ";
  protected final String TEXT_208 = ".";
  protected final String TEXT_209 = " = swOut_";
  protected final String TEXT_210 = "_";
  protected final String TEXT_211 = ".toString();" + NL + "      rows_";
  protected final String TEXT_212 = ".add(";
  protected final String TEXT_213 = ");" + NL + "    }";
  protected final String TEXT_214 = NL + "    //Output as byte array for single col connection " + NL + "    javax.xml.transform.stream.StreamResult streamResult_";
  protected final String TEXT_215 = "_";
  protected final String TEXT_216 = " = (javax.xml.transform.stream.StreamResult)execResult_";
  protected final String TEXT_217 = "_";
  protected final String TEXT_218 = ";" + NL + "    java.io.ByteArrayOutputStream basOut_";
  protected final String TEXT_219 = "_";
  protected final String TEXT_220 = " = (java.io.ByteArrayOutputStream)streamResult_";
  protected final String TEXT_221 = "_";
  protected final String TEXT_222 = ".getOutputStream();" + NL + "    if (basOut_";
  protected final String TEXT_223 = "_";
  protected final String TEXT_224 = " != null) {";
  protected final String TEXT_225 = NL + "      ";
  protected final String TEXT_226 = " = new ";
  protected final String TEXT_227 = "Struct();\t";
  protected final String TEXT_228 = NL + "      ";
  protected final String TEXT_229 = ".";
  protected final String TEXT_230 = " = (byte[])basOut_";
  protected final String TEXT_231 = "_";
  protected final String TEXT_232 = ".toByteArray();" + NL + "      rows_";
  protected final String TEXT_233 = ".add(";
  protected final String TEXT_234 = ");" + NL + "    }";
  protected final String TEXT_235 = NL + "    //Output as a stream for single col connection ";
  protected final String TEXT_236 = NL + "    ";
  protected final String TEXT_237 = ".";
  protected final String TEXT_238 = " = (java.io.InputStream)";
  protected final String TEXT_239 = ".this.globalMap.get(Thread" + NL + "                .currentThread().getId()+\"_";
  protected final String TEXT_240 = "_\"+\"outputResult\");";
  protected final String TEXT_241 = NL + "    //Output as a document for single col connection " + NL + "    org.dom4j.io.DocumentResult drOut_";
  protected final String TEXT_242 = " = (org.dom4j.io.DocumentResult)execResult_";
  protected final String TEXT_243 = "_";
  protected final String TEXT_244 = ";" + NL + "    if (drOut_";
  protected final String TEXT_245 = " != null) {";
  protected final String TEXT_246 = NL + "        ";
  protected final String TEXT_247 = " = new ";
  protected final String TEXT_248 = "Struct();\t";
  protected final String TEXT_249 = NL + "        ";
  protected final String TEXT_250 = ".";
  protected final String TEXT_251 = " = new routines.system.Document();" + NL + "        ((routines.system.Document)";
  protected final String TEXT_252 = ".";
  protected final String TEXT_253 = ").setDocument(drOut_";
  protected final String TEXT_254 = ".getDocument());" + NL + "        rows_";
  protected final String TEXT_255 = ".add(";
  protected final String TEXT_256 = ");" + NL + "    }";
  protected final String TEXT_257 = NL + "        rows_";
  protected final String TEXT_258 = ".add(";
  protected final String TEXT_259 = ");" + NL + "      } // for (java.util.Map" + NL + "    } // if (outputList_" + NL;
  protected final String TEXT_260 = NL;
  protected final String TEXT_261 = " " + NL + "   for (java.lang.Object row_";
  protected final String TEXT_262 = " : rows_";
  protected final String TEXT_263 = ") {";
  protected final String TEXT_264 = NL + "\tnb_line_";
  protected final String TEXT_265 = "++;";
  protected final String TEXT_266 = "\t  ";
  protected final String TEXT_267 = NL + "     ";
  protected final String TEXT_268 = " = null;" + NL + "\t  if (row_";
  protected final String TEXT_269 = " instanceof ";
  protected final String TEXT_270 = "Struct) {" + NL + "\t    ";
  protected final String TEXT_271 = " = (";
  protected final String TEXT_272 = "Struct)row_";
  protected final String TEXT_273 = ";" + NL + "\t  }";

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    
    CodeGeneratorArgument codeGenArgument = (CodeGeneratorArgument) argument;
    INode node = (INode)codeGenArgument.getArgument();
    String processName = org.talend.core.model.utils.JavaResourcesHelper.getSimpleClassName(node.getProcess());
    String this_cid = ElementParameterParser.getValue(node, "__UNIQUE_NAME__");
    String tHMap_id = this_cid.replace("_THMAP_IN", "");
    String cid = tHMap_id;

    //Parameters for the subjavajet
    boolean asMap = "true".equals(ElementParameterParser.getValue(node, "__AS_MAP__"));
    boolean asString = "true".equals(ElementParameterParser.getValue(node, "__AS_STRING__"));
    boolean asBytearray = "true".equals(ElementParameterParser.getValue(node, "__AS_BYTEARRAY__"));
    boolean asInputstream = "true".equals(ElementParameterParser.getValue(node, "__AS_INPUTSTREAM__"));
    boolean asDocument = "true".equals(ElementParameterParser.getValue(node, "__AS_DOCUMENT__"));
    INode tHMapNode = MapperComponent.getThMapNode(node);

    //Filter out non FLOW_MAIN/ROUTE type connections
    //Use the thMapNode to get the thMap's outgoing connections' records
    MapperComponent mapperComponent = ((MapperComponent)tHMapNode.getExternalNode());
    java.util.List<? extends IConnection> mainOutConnections = ((MapperComponent)node.getExternalNode()).getNonVirtualMainOutgoingConnections();

    stringBuffer.append(TEXT_1);
    stringBuffer.append(tHMap_id);
    stringBuffer.append(TEXT_2);
    stringBuffer.append(TEXT_3);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_4);
    
    java.util.List<? extends IConnection> mainOutConnections_getResults = ((MapperComponent)node.getExternalNode()).getNonVirtualMainOutgoingConnections();
    boolean requires_getResults = mapperComponent.requiresMultipleResults();    
    if (requires_getResults) {
    	//Note: Only map output type should support multiple outputs

    stringBuffer.append(TEXT_5);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_6);
    stringBuffer.append( processName );
    stringBuffer.append(TEXT_7);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_8);
      
    } else if (!asInputstream && mainOutConnections_getResults.size()==1) { 
       //Single column schema/payload-type output
       String outputConnName = mainOutConnections_getResults.get(0).getName();       

    stringBuffer.append(TEXT_9);
    stringBuffer.append(outputConnName);
    stringBuffer.append(TEXT_10);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_11);
    stringBuffer.append(outputConnName);
    stringBuffer.append(TEXT_12);
    stringBuffer.append( processName );
    stringBuffer.append(TEXT_13);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_14);
      
    } //end if-requires_getResults
    if (mainOutConnections_getResults!=null) {
      //Use the thMapNode to get the record names
      for (IConnection outgoingConn : mainOutConnections_getResults) {
        String outputConnName = outgoingConn.getName();
        IConnection thMapOutConnection = org.talend.transform.components.utils.ComponentUtils.findOutConnectionFromTransformNode(tHMapNode, node, outgoingConn);            
        String defaultDataType = mapperComponent.getOutputConnectionDataType(thMapOutConnection);
          if (requires_getResults) { 
            //This is the record of the connection to the thMap 
            String recordPath = mapperComponent.findOutputConnectionRecordPath(thMapOutConnection);
            

    stringBuffer.append(TEXT_15);
    stringBuffer.append(outputConnName);
    stringBuffer.append(TEXT_16);
    stringBuffer.append(recordPath);
    stringBuffer.append(TEXT_17);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_18);
    stringBuffer.append(outputConnName);
    stringBuffer.append(TEXT_19);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_20);
    stringBuffer.append(recordPath);
    stringBuffer.append(TEXT_21);
           
	       } 
	       if ("MAP".equals(defaultDataType)) { //Schema-type connection 

    stringBuffer.append(TEXT_22);
    stringBuffer.append(outputConnName);
    stringBuffer.append(TEXT_23);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_24);
    stringBuffer.append(outputConnName);
    stringBuffer.append(TEXT_25);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_26);
    stringBuffer.append(outputConnName);
    stringBuffer.append(TEXT_27);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_28);
    stringBuffer.append(outputConnName);
    stringBuffer.append(TEXT_29);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_30);
    stringBuffer.append(outputConnName);
    stringBuffer.append(TEXT_31);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_32);
    stringBuffer.append(outputConnName);
    stringBuffer.append(TEXT_33);
    stringBuffer.append(outputConnName);
    stringBuffer.append(TEXT_34);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_35);
    stringBuffer.append(outputConnName);
    stringBuffer.append(TEXT_36);
    stringBuffer.append(TEXT_37);
    stringBuffer.append(outputConnName);
    stringBuffer.append(TEXT_38);
    stringBuffer.append(outputConnName);
    stringBuffer.append(TEXT_39);
    	       
          } //end if asMap                      
          IMetadataTable outputMetadataTable = outgoingConn.getMetadataTable();
          for (IMetadataColumn outputCol : outputMetadataTable.getListColumns()) {

    stringBuffer.append(TEXT_40);
    stringBuffer.append(outgoingConn.getName() );
    stringBuffer.append(TEXT_41);
    stringBuffer.append(outputCol.getLabel());
              
           if ("MAP".equals(defaultDataType)) { //Schema-type connection 

    
    // Populate a DI rowStruct using a TDM HashMap
    if (JavaTypesManager.STRING.getId().equals(outputCol.getTalendType())) {

    stringBuffer.append(TEXT_42);
    stringBuffer.append(outgoingConn.getName());
    stringBuffer.append(TEXT_43);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_44);
    stringBuffer.append(TEXT_45);
    stringBuffer.append(outgoingConn.getName() );
    stringBuffer.append(TEXT_46);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_47);
    stringBuffer.append(outgoingConn.getName());
    stringBuffer.append(TEXT_48);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_49);
    stringBuffer.append(TEXT_50);
    stringBuffer.append(outgoingConn.getName() );
    stringBuffer.append(TEXT_51);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_52);
    stringBuffer.append(outgoingConn.getName());
    stringBuffer.append(TEXT_53);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_54);
    
    } else if (JavaTypesManager.BOOLEAN.getId().equals(outputCol.getTalendType())) {

    stringBuffer.append(TEXT_55);
    stringBuffer.append(outgoingConn.getName());
    stringBuffer.append(TEXT_56);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_57);
    stringBuffer.append(TEXT_58);
    stringBuffer.append(outgoingConn.getName() );
    stringBuffer.append(TEXT_59);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_60);
    stringBuffer.append(outgoingConn.getName());
    stringBuffer.append(TEXT_61);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_62);
    stringBuffer.append(TEXT_63);
    stringBuffer.append(outgoingConn.getName() );
    stringBuffer.append(TEXT_64);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_65);
    stringBuffer.append(outgoingConn.getName());
    stringBuffer.append(TEXT_66);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_67);
    
    } else if (JavaTypesManager.BYTE.getId().equals(outputCol.getTalendType())) {

    stringBuffer.append(TEXT_68);
    stringBuffer.append(outgoingConn.getName());
    stringBuffer.append(TEXT_69);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_70);
    stringBuffer.append(TEXT_71);
    stringBuffer.append(outgoingConn.getName() );
    stringBuffer.append(TEXT_72);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_73);
    stringBuffer.append(outgoingConn.getName());
    stringBuffer.append(TEXT_74);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_75);
    stringBuffer.append(TEXT_76);
    stringBuffer.append(outgoingConn.getName() );
    stringBuffer.append(TEXT_77);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_78);
    stringBuffer.append(outgoingConn.getName());
    stringBuffer.append(TEXT_79);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_80);
    
    } else if (JavaTypesManager.CHARACTER.getId().equals(outputCol.getTalendType())) {

    stringBuffer.append(TEXT_81);
    stringBuffer.append(outgoingConn.getName());
    stringBuffer.append(TEXT_82);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_83);
    stringBuffer.append(TEXT_84);
    stringBuffer.append(outgoingConn.getName() );
    stringBuffer.append(TEXT_85);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_86);
    stringBuffer.append(outgoingConn.getName());
    stringBuffer.append(TEXT_87);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_88);
    stringBuffer.append(TEXT_89);
    stringBuffer.append(outgoingConn.getName() );
    stringBuffer.append(TEXT_90);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_91);
    stringBuffer.append(outgoingConn.getName());
    stringBuffer.append(TEXT_92);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_93);
    stringBuffer.append(outgoingConn.getName());
    stringBuffer.append(TEXT_94);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_95);
    stringBuffer.append(TEXT_96);
    stringBuffer.append(outgoingConn.getName() );
    stringBuffer.append(TEXT_97);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_98);
    stringBuffer.append(outgoingConn.getName());
    stringBuffer.append(TEXT_99);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_100);
    
    } else if (JavaTypesManager.SHORT.getId().equals(outputCol.getTalendType())) {

    stringBuffer.append(TEXT_101);
    stringBuffer.append(outgoingConn.getName());
    stringBuffer.append(TEXT_102);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_103);
    stringBuffer.append(TEXT_104);
    stringBuffer.append(outgoingConn.getName() );
    stringBuffer.append(TEXT_105);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_106);
    stringBuffer.append(outgoingConn.getName());
    stringBuffer.append(TEXT_107);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_108);
    stringBuffer.append(TEXT_109);
    stringBuffer.append(outgoingConn.getName() );
    stringBuffer.append(TEXT_110);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_111);
    stringBuffer.append(outgoingConn.getName());
    stringBuffer.append(TEXT_112);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_113);
    
    } else if (JavaTypesManager.INTEGER.getId().equals(outputCol.getTalendType())) {

    stringBuffer.append(TEXT_114);
    stringBuffer.append(outgoingConn.getName());
    stringBuffer.append(TEXT_115);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_116);
    stringBuffer.append(TEXT_117);
    stringBuffer.append(outgoingConn.getName() );
    stringBuffer.append(TEXT_118);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_119);
    stringBuffer.append(outgoingConn.getName());
    stringBuffer.append(TEXT_120);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_121);
    stringBuffer.append(TEXT_122);
    stringBuffer.append(outgoingConn.getName() );
    stringBuffer.append(TEXT_123);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_124);
    stringBuffer.append(outgoingConn.getName());
    stringBuffer.append(TEXT_125);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_126);
    
    } else if (JavaTypesManager.LONG.getId().equals(outputCol.getTalendType())) {

    stringBuffer.append(TEXT_127);
    stringBuffer.append(outgoingConn.getName());
    stringBuffer.append(TEXT_128);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_129);
    stringBuffer.append(TEXT_130);
    stringBuffer.append(outgoingConn.getName() );
    stringBuffer.append(TEXT_131);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_132);
    stringBuffer.append(outgoingConn.getName());
    stringBuffer.append(TEXT_133);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_134);
    stringBuffer.append(TEXT_135);
    stringBuffer.append(outgoingConn.getName() );
    stringBuffer.append(TEXT_136);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_137);
    stringBuffer.append(outgoingConn.getName());
    stringBuffer.append(TEXT_138);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_139);
    
    } else if (JavaTypesManager.FLOAT.getId().equals(outputCol.getTalendType())) {

    stringBuffer.append(TEXT_140);
    stringBuffer.append(outgoingConn.getName());
    stringBuffer.append(TEXT_141);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_142);
    stringBuffer.append(TEXT_143);
    stringBuffer.append(outgoingConn.getName() );
    stringBuffer.append(TEXT_144);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_145);
    stringBuffer.append(outgoingConn.getName());
    stringBuffer.append(TEXT_146);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_147);
    stringBuffer.append(TEXT_148);
    stringBuffer.append(outgoingConn.getName() );
    stringBuffer.append(TEXT_149);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_150);
    stringBuffer.append(outgoingConn.getName());
    stringBuffer.append(TEXT_151);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_152);
    
    } else if (JavaTypesManager.DOUBLE.getId().equals(outputCol.getTalendType())) {

    stringBuffer.append(TEXT_153);
    stringBuffer.append(outgoingConn.getName());
    stringBuffer.append(TEXT_154);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_155);
    stringBuffer.append(TEXT_156);
    stringBuffer.append(outgoingConn.getName() );
    stringBuffer.append(TEXT_157);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_158);
    stringBuffer.append(outgoingConn.getName());
    stringBuffer.append(TEXT_159);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_160);
    stringBuffer.append(TEXT_161);
    stringBuffer.append(outgoingConn.getName() );
    stringBuffer.append(TEXT_162);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_163);
    stringBuffer.append(outgoingConn.getName());
    stringBuffer.append(TEXT_164);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_165);
    
    } else if (JavaTypesManager.BIGDECIMAL.getId().equals(outputCol.getTalendType())) {

    stringBuffer.append(TEXT_166);
    stringBuffer.append(outgoingConn.getName());
    stringBuffer.append(TEXT_167);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_168);
    stringBuffer.append(TEXT_169);
    stringBuffer.append(outgoingConn.getName() );
    stringBuffer.append(TEXT_170);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_171);
    stringBuffer.append(outgoingConn.getName());
    stringBuffer.append(TEXT_172);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_173);
    stringBuffer.append(TEXT_174);
    stringBuffer.append(outgoingConn.getName() );
    stringBuffer.append(TEXT_175);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_176);
    stringBuffer.append(outgoingConn.getName());
    stringBuffer.append(TEXT_177);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_178);
    
    } else if (JavaTypesManager.DATE.getId().equals(outputCol.getTalendType())) {

    stringBuffer.append(TEXT_179);
    stringBuffer.append(outgoingConn.getName());
    stringBuffer.append(TEXT_180);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_181);
    stringBuffer.append(TEXT_182);
    stringBuffer.append(outgoingConn.getName() );
    stringBuffer.append(TEXT_183);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_184);
    stringBuffer.append(outgoingConn.getName());
    stringBuffer.append(TEXT_185);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_186);
    stringBuffer.append(TEXT_187);
    stringBuffer.append(outgoingConn.getName() );
    stringBuffer.append(TEXT_188);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_189);
    stringBuffer.append(outgoingConn.getName());
    stringBuffer.append(TEXT_190);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_191);
    
    } // if (JavaTypesManager.STRING

    stringBuffer.append(TEXT_192);
    
           } else if ((JavaTypesManager.STRING.getId().equals(defaultDataType))) {

    stringBuffer.append(TEXT_193);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_194);
    stringBuffer.append(outputConnName);
    stringBuffer.append(TEXT_195);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_196);
    stringBuffer.append(outputConnName);
    stringBuffer.append(TEXT_197);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_198);
    stringBuffer.append(outputConnName);
    stringBuffer.append(TEXT_199);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_200);
    stringBuffer.append(outputConnName);
    stringBuffer.append(TEXT_201);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_202);
    stringBuffer.append(outputConnName);
    stringBuffer.append(TEXT_203);
    stringBuffer.append(TEXT_204);
    stringBuffer.append(outputConnName);
    stringBuffer.append(TEXT_205);
    stringBuffer.append(outputConnName);
    stringBuffer.append(TEXT_206);
    stringBuffer.append(TEXT_207);
    stringBuffer.append(outgoingConn.getName() );
    stringBuffer.append(TEXT_208);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_209);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_210);
    stringBuffer.append(outputConnName);
    stringBuffer.append(TEXT_211);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_212);
    stringBuffer.append(outputConnName);
    stringBuffer.append(TEXT_213);
    
               
           } else if ("id_byte[]".equals(defaultDataType)) {

    stringBuffer.append(TEXT_214);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_215);
    stringBuffer.append(outputConnName);
    stringBuffer.append(TEXT_216);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_217);
    stringBuffer.append(outputConnName);
    stringBuffer.append(TEXT_218);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_219);
    stringBuffer.append(outputConnName);
    stringBuffer.append(TEXT_220);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_221);
    stringBuffer.append(outputConnName);
    stringBuffer.append(TEXT_222);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_223);
    stringBuffer.append(outputConnName);
    stringBuffer.append(TEXT_224);
    stringBuffer.append(TEXT_225);
    stringBuffer.append(outputConnName);
    stringBuffer.append(TEXT_226);
    stringBuffer.append(outputConnName);
    stringBuffer.append(TEXT_227);
    stringBuffer.append(TEXT_228);
    stringBuffer.append(outgoingConn.getName() );
    stringBuffer.append(TEXT_229);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_230);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_231);
    stringBuffer.append(outputConnName);
    stringBuffer.append(TEXT_232);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_233);
    stringBuffer.append(outputConnName);
    stringBuffer.append(TEXT_234);
    
          } else if (asInputstream) {

    stringBuffer.append(TEXT_235);
    stringBuffer.append(TEXT_236);
    stringBuffer.append(outgoingConn.getName() );
    stringBuffer.append(TEXT_237);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_238);
    stringBuffer.append( processName );
    stringBuffer.append(TEXT_239);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_240);
    
               
          } else if ("id_Document".equals(defaultDataType)) {

    stringBuffer.append(TEXT_241);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_242);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_243);
    stringBuffer.append(outputConnName);
    stringBuffer.append(TEXT_244);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_245);
    stringBuffer.append(TEXT_246);
    stringBuffer.append(outputConnName);
    stringBuffer.append(TEXT_247);
    stringBuffer.append(outputConnName);
    stringBuffer.append(TEXT_248);
    stringBuffer.append(TEXT_249);
    stringBuffer.append(outgoingConn.getName() );
    stringBuffer.append(TEXT_250);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_251);
    stringBuffer.append(outgoingConn.getName() );
    stringBuffer.append(TEXT_252);
    stringBuffer.append(outputCol.getLabel() );
    stringBuffer.append(TEXT_253);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_254);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_255);
    stringBuffer.append(outputConnName);
    stringBuffer.append(TEXT_256);
    
               
             } // if-else (defaultDataType)
          } // for (IMetadataColumn
        //For schema-type outputs, save the rows in a list 
        if ("MAP".equals(defaultDataType)) { //Schema-type connection 

    stringBuffer.append(TEXT_257);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_258);
    stringBuffer.append(outputConnName);
    stringBuffer.append(TEXT_259);
               
        } //end if (MAP)
      } // for (IConnection           
    } // if (thMapOutConnections

    stringBuffer.append(TEXT_260);
    
   if (asMap && mainOutConnections.size()>=1) {

    stringBuffer.append(TEXT_261);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_262);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_263);
    
	if (node.isDesignSubjobStartNode()) {
	// if the thMapIn virtual component is the first node, it means that thMapOut virtual component is not activated.
	// In this case thMapIn is responsible for updating nb_line counter.

    stringBuffer.append(TEXT_264);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_265);
    
	} // node.isDesignSubjobStartNode()

    	
     for (IConnection outgoingConn : mainOutConnections) {
       String outputConnName = outgoingConn.getName();		

    stringBuffer.append(TEXT_266);
    stringBuffer.append(TEXT_267);
    stringBuffer.append(outputConnName);
    stringBuffer.append(TEXT_268);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_269);
    stringBuffer.append(outputConnName);
    stringBuffer.append(TEXT_270);
    stringBuffer.append(outputConnName);
    stringBuffer.append(TEXT_271);
    stringBuffer.append(outputConnName);
    stringBuffer.append(TEXT_272);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_273);
       
     } //end for mainOutConnections
   } //end if (asMap && >=1)
   //Note: the for (rows_) ends in the thMap*_end as outgoing connection code will follow this code

    return stringBuffer.toString();
  }
}
