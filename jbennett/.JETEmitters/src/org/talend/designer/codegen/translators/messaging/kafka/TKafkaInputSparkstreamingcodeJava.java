package org.talend.designer.codegen.translators.messaging.kafka;

import org.talend.core.model.process.INode;
import org.talend.designer.common.BigDataCodeGeneratorArgument;
import org.talend.designer.common.tkafkainput.TKafkaInputUtil;
import org.talend.hadoop.distribution.kafka.SparkStreamingKafkaVersion;

public class TKafkaInputSparkstreamingcodeJava
{
  protected static String nl;
  public static synchronized TKafkaInputSparkstreamingcodeJava create(String lineSeparator)
  {
    nl = lineSeparator;
    TKafkaInputSparkstreamingcodeJava result = new TKafkaInputSparkstreamingcodeJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "\t";
  protected final String TEXT_2 = NL + NL + "public static class ";
  protected final String TEXT_3 = "_DecoderFromByteArrayTo";
  protected final String TEXT_4 = " implements kafka.serializer.Decoder<";
  protected final String TEXT_5 = "> {" + NL + "" + NL + "    private final kafka.serializer.StringDecoder stringDecoder;" + NL + "" + NL + "    public ";
  protected final String TEXT_6 = "_DecoderFromByteArrayTo";
  protected final String TEXT_7 = "(kafka.utils.VerifiableProperties props){" + NL + "        this.stringDecoder = new kafka.serializer.StringDecoder(props);" + NL + "    }" + NL + "" + NL + "    public ";
  protected final String TEXT_8 = " fromBytes(byte[] bytes) {";
  protected final String TEXT_9 = NL + "        ";
  protected final String TEXT_10 = " result = new ";
  protected final String TEXT_11 = "();";
  protected final String TEXT_12 = NL + "            result.";
  protected final String TEXT_13 = " = this.stringDecoder.fromBytes(bytes);";
  protected final String TEXT_14 = NL + "            result.";
  protected final String TEXT_15 = " = java.nio.ByteBuffer.wrap(bytes);";
  protected final String TEXT_16 = NL + "        return result;" + NL + "    }" + NL + "}" + NL + "" + NL + "public static class ";
  protected final String TEXT_17 = "_DecoderFromByteArrayToNullWritable implements kafka.serializer.Decoder<org.apache.hadoop.io.NullWritable> {" + NL + "" + NL + "    public ";
  protected final String TEXT_18 = "_DecoderFromByteArrayToNullWritable(kafka.utils.VerifiableProperties props){" + NL + "        // nothing but Decoder implementations must define a constructor with VerifiableProperties" + NL + "    }" + NL + "" + NL + "    public org.apache.hadoop.io.NullWritable fromBytes(byte[] bytes) {" + NL + "        return org.apache.hadoop.io.NullWritable.get();" + NL + "    }" + NL + "}";
  protected final String TEXT_19 = NL + "\t";
  protected final String TEXT_20 = NL + NL + "public static class ";
  protected final String TEXT_21 = "_ValueDeserializer implements org.apache.kafka.common.serialization.Deserializer<";
  protected final String TEXT_22 = "> {" + NL + "" + NL + "\tprivate org.apache.kafka.common.serialization.StringDeserializer stringDeserializer;" + NL + "\t" + NL + "\tpublic ";
  protected final String TEXT_23 = "_ValueDeserializer() {" + NL + "\t\t// empty" + NL + "\t}" + NL + "" + NL + "\tpublic void configure(java.util.Map<java.lang.String,?> configs, boolean isKey) {" + NL + "\t\tstringDeserializer = new org.apache.kafka.common.serialization.StringDeserializer();" + NL + "\t\tstringDeserializer.configure(configs, isKey);" + NL + "\t}" + NL + "" + NL + "\tpublic ";
  protected final String TEXT_24 = " deserialize(String topic, byte[] data) {" + NL + "\t\t";
  protected final String TEXT_25 = " result = new ";
  protected final String TEXT_26 = "();";
  protected final String TEXT_27 = NL + "\t\t    String line = stringDeserializer.deserialize(topic, data);" + NL + "\t\t    result.";
  protected final String TEXT_28 = " = line;";
  protected final String TEXT_29 = NL + "\t\t    result.";
  protected final String TEXT_30 = " = java.nio.ByteBuffer.wrap(data);";
  protected final String TEXT_31 = NL + "\t\treturn result;" + NL + "    }" + NL + "" + NL + "\tpublic void close() {" + NL + "\t\t// nothing" + NL + "\t}" + NL + "}" + NL + "" + NL + "public static class ";
  protected final String TEXT_32 = "_KeyDeserializer implements org.apache.kafka.common.serialization.Deserializer<org.apache.hadoop.io.NullWritable> {" + NL + "" + NL + "\tpublic ";
  protected final String TEXT_33 = "_KeyDeserializer() {" + NL + "\t\t// empty" + NL + "\t}" + NL + "\t" + NL + "\tpublic void configure(java.util.Map<java.lang.String,?> configs, boolean isKey) {" + NL + "\t\t// nothing" + NL + "\t}" + NL + "" + NL + "\tpublic org.apache.hadoop.io.NullWritable deserialize(String topic, byte[] data) {" + NL + "\t    return org.apache.hadoop.io.NullWritable.get();" + NL + "\t}" + NL + "" + NL + "\tpublic void close() {" + NL + "\t\t// nothing" + NL + "\t}" + NL + "}" + NL + "" + NL + "public static class ";
  protected final String TEXT_34 = "_Function implements org.apache.spark.api.java.function.PairFunction<org.apache.kafka.clients.consumer.ConsumerRecord<NullWritable, ";
  protected final String TEXT_35 = ">, NullWritable, ";
  protected final String TEXT_36 = "> {" + NL + "" + NL + "\tpublic ";
  protected final String TEXT_37 = "_Function() {" + NL + "\t\t// empty" + NL + "\t}" + NL + "\t" + NL + "\t@Override" + NL + "\tpublic scala.Tuple2<NullWritable, ";
  protected final String TEXT_38 = "> call(org.apache.kafka.clients.consumer.ConsumerRecord<NullWritable, ";
  protected final String TEXT_39 = "> record) {" + NL + "\t\treturn new scala.Tuple2(record.key(), record.value());" + NL + "\t}" + NL + "" + NL + "}";
  protected final String TEXT_40 = NL + "\t";
  protected final String TEXT_41 = NL + NL + "public static class ";
  protected final String TEXT_42 = "_ValueDeserializer implements org.apache.kafka.common.serialization.Deserializer<";
  protected final String TEXT_43 = "> {" + NL + "" + NL + "\tprivate org.apache.kafka.common.serialization.StringDeserializer stringDeserializer;" + NL + "\t" + NL + "\tpublic ";
  protected final String TEXT_44 = "_ValueDeserializer() {" + NL + "\t\t// empty" + NL + "\t}" + NL + "" + NL + "\tpublic void configure(java.util.Map<java.lang.String,?> configs, boolean isKey) {" + NL + "\t\tstringDeserializer = new org.apache.kafka.common.serialization.StringDeserializer();" + NL + "\t\tstringDeserializer.configure(configs, isKey);" + NL + "\t}" + NL + "" + NL + "\tpublic ";
  protected final String TEXT_45 = " deserialize(String topic, byte[] data) {" + NL + "\t\t";
  protected final String TEXT_46 = " result = new ";
  protected final String TEXT_47 = "();";
  protected final String TEXT_48 = NL + "\t\t    String line = stringDeserializer.deserialize(topic, data);" + NL + "\t\t    result.";
  protected final String TEXT_49 = " = line;";
  protected final String TEXT_50 = NL + "\t\t    result.";
  protected final String TEXT_51 = " = java.nio.ByteBuffer.wrap(data);";
  protected final String TEXT_52 = NL + "\t\treturn result;" + NL + "    }" + NL + "" + NL + "\tpublic void close() {" + NL + "\t\t// nothing" + NL + "\t}" + NL + "}" + NL + "" + NL + "public static class ";
  protected final String TEXT_53 = "_KeyDeserializer implements org.apache.kafka.common.serialization.Deserializer<org.apache.hadoop.io.NullWritable> {" + NL + "" + NL + "\tpublic ";
  protected final String TEXT_54 = "_KeyDeserializer() {" + NL + "\t\t// empty" + NL + "\t}" + NL + "\t" + NL + "\tpublic void configure(java.util.Map<java.lang.String,?> configs, boolean isKey) {" + NL + "\t\t// nothing" + NL + "\t}" + NL + "" + NL + "\tpublic org.apache.hadoop.io.NullWritable deserialize(String topic, byte[] data) {" + NL + "\t    return org.apache.hadoop.io.NullWritable.get();" + NL + "\t}" + NL + "" + NL + "\tpublic void close() {" + NL + "\t\t// nothing" + NL + "\t}" + NL + "}";
  protected final String TEXT_55 = NL + "\t";
  protected final String TEXT_56 = NL + NL + "public static class ";
  protected final String TEXT_57 = "_ValueDeserializer implements org.apache.kafka.common.serialization.Deserializer<";
  protected final String TEXT_58 = "> {" + NL + "" + NL + "\tprivate org.apache.kafka.common.serialization.StringDeserializer stringDeserializer;" + NL + "\t" + NL + "\tpublic ";
  protected final String TEXT_59 = "_ValueDeserializer() {" + NL + "\t\t// empty" + NL + "\t}" + NL + "" + NL + "\tpublic void configure(java.util.Map<java.lang.String,?> configs, boolean isKey) {" + NL + "\t\tstringDeserializer = new org.apache.kafka.common.serialization.StringDeserializer();" + NL + "\t\tstringDeserializer.configure(configs, isKey);" + NL + "\t}" + NL + "" + NL + "\tpublic ";
  protected final String TEXT_60 = " deserialize(String topic, byte[] data) {" + NL + "\t\t";
  protected final String TEXT_61 = " result = new ";
  protected final String TEXT_62 = "();";
  protected final String TEXT_63 = NL + "\t\t    String line = stringDeserializer.deserialize(topic, data);" + NL + "\t\t    result.";
  protected final String TEXT_64 = " = line;";
  protected final String TEXT_65 = NL + "\t\t    result.";
  protected final String TEXT_66 = " = java.nio.ByteBuffer.wrap(data);";
  protected final String TEXT_67 = NL + "\t\treturn result;" + NL + "    }" + NL + "" + NL + "\tpublic void close() {" + NL + "\t\t// nothing" + NL + "\t}" + NL + "}" + NL + "" + NL + "public static class ";
  protected final String TEXT_68 = "_KeyDeserializer implements org.apache.kafka.common.serialization.Deserializer<org.apache.hadoop.io.NullWritable> {" + NL + "" + NL + "\tpublic ";
  protected final String TEXT_69 = "_KeyDeserializer() {" + NL + "\t\t// empty" + NL + "\t}" + NL + "\t" + NL + "\tpublic void configure(java.util.Map<java.lang.String,?> configs, boolean isKey) {" + NL + "\t\t// nothing" + NL + "\t}" + NL + "" + NL + "\tpublic org.apache.hadoop.io.NullWritable deserialize(String topic, byte[] data) {" + NL + "\t    return org.apache.hadoop.io.NullWritable.get();" + NL + "\t}" + NL + "" + NL + "\tpublic void close() {" + NL + "\t\t// nothing" + NL + "\t}" + NL + "}" + NL + "" + NL + "public static class ";
  protected final String TEXT_70 = "_Function implements org.apache.spark.api.java.function.PairFunction<org.apache.kafka.clients.consumer.ConsumerRecord<NullWritable, ";
  protected final String TEXT_71 = ">, NullWritable, ";
  protected final String TEXT_72 = "> {" + NL + "" + NL + "\tpublic ";
  protected final String TEXT_73 = "_Function() {" + NL + "\t\t// empty" + NL + "\t}" + NL + "\t" + NL + "\t@Override" + NL + "\tpublic scala.Tuple2<NullWritable, ";
  protected final String TEXT_74 = "> call(org.apache.kafka.clients.consumer.ConsumerRecord<NullWritable, ";
  protected final String TEXT_75 = "> record) {" + NL + "\t\treturn new scala.Tuple2(record.key(), record.value());" + NL + "\t}" + NL + "" + NL + "}";

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    
BigDataCodeGeneratorArgument codeGenArgument = (BigDataCodeGeneratorArgument) argument;
INode node = (INode)codeGenArgument.getArgument();
String cid = node.getUniqueName();

TKafkaInputUtil tKafkaInputUtil = new TKafkaInputUtil(node);
SparkStreamingKafkaVersion sparkStreamingKafkaVersion = tKafkaInputUtil.getSparkStreamingKafkaVersion();

if (SparkStreamingKafkaVersion.KAFKA_0_8.equals(sparkStreamingKafkaVersion)) {

    stringBuffer.append(TEXT_1);
    
String outStructName = codeGenArgument.getRecordStructName(tKafkaInputUtil.getOutgoingConnection());

    stringBuffer.append(TEXT_2);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_3);
    stringBuffer.append(outStructName);
    stringBuffer.append(TEXT_4);
    stringBuffer.append(outStructName);
    stringBuffer.append(TEXT_5);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_6);
    stringBuffer.append(outStructName);
    stringBuffer.append(TEXT_7);
    stringBuffer.append(outStructName);
    stringBuffer.append(TEXT_8);
    stringBuffer.append(TEXT_9);
    stringBuffer.append(outStructName);
    stringBuffer.append(TEXT_10);
    stringBuffer.append(outStructName);
    stringBuffer.append(TEXT_11);
    
        if("STRING".equals(tKafkaInputUtil.getOutputType())) {

    stringBuffer.append(TEXT_12);
    stringBuffer.append(tKafkaInputUtil.getOutgoingColumnName());
    stringBuffer.append(TEXT_13);
    
        } else if("BYTES".equals(tKafkaInputUtil.getOutputType())) {

    stringBuffer.append(TEXT_14);
    stringBuffer.append(tKafkaInputUtil.getOutgoingColumnName());
    stringBuffer.append(TEXT_15);
    
        }

    stringBuffer.append(TEXT_16);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_17);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_18);
    
} else if (SparkStreamingKafkaVersion.KAFKA_0_10.equals(sparkStreamingKafkaVersion)) {

    stringBuffer.append(TEXT_19);
    
String outStructName = codeGenArgument.getRecordStructName(tKafkaInputUtil.getOutgoingConnection());

    stringBuffer.append(TEXT_20);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_21);
    stringBuffer.append(outStructName);
    stringBuffer.append(TEXT_22);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_23);
    stringBuffer.append(outStructName);
    stringBuffer.append(TEXT_24);
    stringBuffer.append(outStructName);
    stringBuffer.append(TEXT_25);
    stringBuffer.append(outStructName);
    stringBuffer.append(TEXT_26);
    
		if("STRING".equals(tKafkaInputUtil.getOutputType())) {

    stringBuffer.append(TEXT_27);
    stringBuffer.append(tKafkaInputUtil.getOutgoingColumnName());
    stringBuffer.append(TEXT_28);
    
		} else if("BYTES".equals(tKafkaInputUtil.getOutputType())) {

    stringBuffer.append(TEXT_29);
    stringBuffer.append(tKafkaInputUtil.getOutgoingColumnName());
    stringBuffer.append(TEXT_30);
    
		}

    stringBuffer.append(TEXT_31);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_32);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_33);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_34);
    stringBuffer.append(outStructName);
    stringBuffer.append(TEXT_35);
    stringBuffer.append(outStructName);
    stringBuffer.append(TEXT_36);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_37);
    stringBuffer.append(outStructName);
    stringBuffer.append(TEXT_38);
    stringBuffer.append(outStructName);
    stringBuffer.append(TEXT_39);
    
} else if (SparkStreamingKafkaVersion.MAPR_5X0_KAFKA.equals(sparkStreamingKafkaVersion)) {
	// Special case only for some MapR distributions 

    stringBuffer.append(TEXT_40);
    
String outStructName = codeGenArgument.getRecordStructName(tKafkaInputUtil.getOutgoingConnection());

    stringBuffer.append(TEXT_41);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_42);
    stringBuffer.append(outStructName);
    stringBuffer.append(TEXT_43);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_44);
    stringBuffer.append(outStructName);
    stringBuffer.append(TEXT_45);
    stringBuffer.append(outStructName);
    stringBuffer.append(TEXT_46);
    stringBuffer.append(outStructName);
    stringBuffer.append(TEXT_47);
    
		if("STRING".equals(tKafkaInputUtil.getOutputType())) {

    stringBuffer.append(TEXT_48);
    stringBuffer.append(tKafkaInputUtil.getOutgoingColumnName());
    stringBuffer.append(TEXT_49);
    
		} else if("BYTES".equals(tKafkaInputUtil.getOutputType())) {

    stringBuffer.append(TEXT_50);
    stringBuffer.append(tKafkaInputUtil.getOutgoingColumnName());
    stringBuffer.append(TEXT_51);
    
		}

    stringBuffer.append(TEXT_52);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_53);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_54);
    
} else if (SparkStreamingKafkaVersion.MAPR_600_KAFKA.equals(sparkStreamingKafkaVersion)) {

    stringBuffer.append(TEXT_55);
    
String outStructName = codeGenArgument.getRecordStructName(tKafkaInputUtil.getOutgoingConnection());

    stringBuffer.append(TEXT_56);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_57);
    stringBuffer.append(outStructName);
    stringBuffer.append(TEXT_58);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_59);
    stringBuffer.append(outStructName);
    stringBuffer.append(TEXT_60);
    stringBuffer.append(outStructName);
    stringBuffer.append(TEXT_61);
    stringBuffer.append(outStructName);
    stringBuffer.append(TEXT_62);
    
		if("STRING".equals(tKafkaInputUtil.getOutputType())) {

    stringBuffer.append(TEXT_63);
    stringBuffer.append(tKafkaInputUtil.getOutgoingColumnName());
    stringBuffer.append(TEXT_64);
    
		} else if("BYTES".equals(tKafkaInputUtil.getOutputType())) {

    stringBuffer.append(TEXT_65);
    stringBuffer.append(tKafkaInputUtil.getOutgoingColumnName());
    stringBuffer.append(TEXT_66);
    
		}

    stringBuffer.append(TEXT_67);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_68);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_69);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_70);
    stringBuffer.append(outStructName);
    stringBuffer.append(TEXT_71);
    stringBuffer.append(outStructName);
    stringBuffer.append(TEXT_72);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_73);
    stringBuffer.append(outStructName);
    stringBuffer.append(TEXT_74);
    stringBuffer.append(outStructName);
    stringBuffer.append(TEXT_75);
    
}

    return stringBuffer.toString();
  }
}
