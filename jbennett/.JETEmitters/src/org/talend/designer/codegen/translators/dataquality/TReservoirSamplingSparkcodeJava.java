package org.talend.designer.codegen.translators.dataquality;

import org.talend.core.model.process.INode;
import org.talend.core.model.process.IConnection;
import org.talend.core.model.metadata.IMetadataColumn;
import org.talend.designer.common.BigDataCodeGeneratorArgument;
import org.talend.designer.common.tsqlrow.TSqlRowUtil;
import org.talend.designer.common.tmodelencoder.TModelEncoderUtil;
import org.talend.core.model.process.INode;
import java.util.List;

public class TReservoirSamplingSparkcodeJava
{
  protected static String nl;
  public static synchronized TReservoirSamplingSparkcodeJava create(String lineSeparator)
  {
    nl = lineSeparator;
    TReservoirSamplingSparkcodeJava result = new TReservoirSamplingSparkcodeJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "";
  protected final String TEXT_2 = NL + NL + " " + NL + "public static class SparkSamplingUtil_";
  protected final String TEXT_3 = "<T> {" + NL + "    private static Long seed = null;" + NL + "    " + NL + "    public SparkSamplingUtil_";
  protected final String TEXT_4 = "(Long seed) {" + NL + "        this.seed = seed;" + NL + "    }" + NL + "    " + NL + "    public SparkSamplingUtil_";
  protected final String TEXT_5 = "() {" + NL + "        this(null);" + NL + "    }" + NL + "    " + NL + "    /**" + NL + "     * do sampling on RDD" + NL + "     * " + NL + "     * @param rdd" + NL + "     * @param nbSamples" + NL + "     * @return list of sample pairs, with generated score as left value and original data as right value." + NL + "     */" + NL + "    public java.util.List<T> getSampleList(org.apache.spark.api.java.JavaRDD<T> rdd, int nbSamples) {" + NL + "    if (this.seed == null)" + NL + "    \treturn rdd.takeSample(false,nbSamples);" + NL + "    \telse" + NL + "    \treturn rdd.takeSample(false,nbSamples,this.seed);" + NL + "    \t" + NL + "    }" + NL + "" + NL + "}    ";
  protected final String TEXT_6 = NL + "    ";

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    stringBuffer.append(TEXT_1);
    
final BigDataCodeGeneratorArgument codeGenArgument = (BigDataCodeGeneratorArgument) argument;
final INode node = (INode) codeGenArgument.getArgument();
final String cid = node.getUniqueName();

    stringBuffer.append(TEXT_2);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_3);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_4);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_5);
    stringBuffer.append(TEXT_6);
    return stringBuffer.toString();
  }
}
