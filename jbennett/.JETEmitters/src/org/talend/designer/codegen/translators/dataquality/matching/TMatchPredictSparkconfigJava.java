package org.talend.designer.codegen.translators.dataquality.matching;

import java.util.List;
import org.talend.core.model.metadata.IMetadataColumn;
import org.talend.core.model.metadata.IMetadataTable;
import org.talend.core.model.metadata.types.JavaType;
import org.talend.core.model.metadata.types.JavaTypesManager;
import org.talend.core.model.process.ElementParameterParser;
import org.talend.core.model.process.IBigDataNode;
import org.talend.core.model.process.IConnection;
import org.talend.core.model.process.IConnectionCategory;
import org.talend.core.model.process.INode;
import org.talend.core.model.utils.NodeUtil;
import org.talend.designer.common.BigDataCodeGeneratorArgument;
import java.util.Map;
import org.talend.designer.common.tsqlrow.TSqlRowUtil;

public class TMatchPredictSparkconfigJava
{
  protected static String nl;
  public static synchronized TMatchPredictSparkconfigJava create(String lineSeparator)
  {
    nl = lineSeparator;
    TMatchPredictSparkconfigJava result = new TMatchPredictSparkconfigJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "    java.net.URI currentURI_";
  protected final String TEXT_2 = "_config = FileSystem.getDefaultUri(ctx.hadoopConfiguration());" + NL + "    FileSystem.setDefaultUri(ctx.hadoopConfiguration(), new java.net.URI(";
  protected final String TEXT_3 = "));" + NL + "    fs = FileSystem.get(ctx.hadoopConfiguration());";
  protected final String TEXT_4 = NL + "List<String> labelsNeedGroup_";
  protected final String TEXT_5 = " = new java.util.ArrayList<String>();";
  protected final String TEXT_6 = NL + "            labelsNeedGroup_";
  protected final String TEXT_7 = ".add(";
  protected final String TEXT_8 = ");";
  protected final String TEXT_9 = NL + NL + "com.esotericsoftware.kryo.Kryo kryo_";
  protected final String TEXT_10 = " = new com.esotericsoftware.kryo.Kryo();" + NL + "TalendPipelineModelSerializer talendPipelineModelSerializer_";
  protected final String TEXT_11 = " = new TalendPipelineModelSerializer();" + NL;
  protected final String TEXT_12 = NL + "    //Get the pairing pipeline model." + NL + "    com.esotericsoftware.kryo.io.Input blockingModelInput_";
  protected final String TEXT_13 = " = new com.esotericsoftware.kryo.io.Input(fs.open(new org.apache.hadoop.fs.Path(";
  protected final String TEXT_14 = " + \"/blocking/\")));" + NL + "    TalendPipelineModel blockingModel_";
  protected final String TEXT_15 = " = kryo_";
  protected final String TEXT_16 = ".readObject(blockingModelInput_";
  protected final String TEXT_17 = ", TalendPipelineModel.class, talendPipelineModelSerializer_";
  protected final String TEXT_18 = ");" + NL + "    org.apache.spark.ml.PipelineModel blockingPM_";
  protected final String TEXT_19 = " = blockingModel_";
  protected final String TEXT_20 = ".getPipelineModel();" + NL + "    " + NL + "    com.esotericsoftware.kryo.io.Input filteringModelInput_";
  protected final String TEXT_21 = " = new com.esotericsoftware.kryo.io.Input(fs.open(new org.apache.hadoop.fs.Path(";
  protected final String TEXT_22 = " + \"/filtering/\")));" + NL + "    TalendPipelineModel filteringModel_";
  protected final String TEXT_23 = " = kryo_";
  protected final String TEXT_24 = ".readObject(filteringModelInput_";
  protected final String TEXT_25 = ", TalendPipelineModel.class, talendPipelineModelSerializer_";
  protected final String TEXT_26 = ");" + NL + "    org.apache.spark.ml.PipelineModel filteringPM_";
  protected final String TEXT_27 = " = filteringModel_";
  protected final String TEXT_28 = ".getPipelineModel();";
  protected final String TEXT_29 = NL + "    // from file system." + NL + "    com.esotericsoftware.kryo.io.Input featuresInput_";
  protected final String TEXT_30 = " = new com.esotericsoftware.kryo.io.Input(fs.open(new org.apache.hadoop.fs.Path(";
  protected final String TEXT_31 = " + \"/model/\")));" + NL + "    TalendPipelineModel mlModel_";
  protected final String TEXT_32 = " = kryo_";
  protected final String TEXT_33 = ".readObject(featuresInput_";
  protected final String TEXT_34 = ", TalendPipelineModel.class, talendPipelineModelSerializer_";
  protected final String TEXT_35 = ");" + NL + "    org.apache.spark.ml.PipelineModel mlPM_";
  protected final String TEXT_36 = " = mlModel_";
  protected final String TEXT_37 = ".getPipelineModel();";
  protected final String TEXT_38 = NL + "    // from current job." + NL + "    org.apache.spark.ml.PipelineModel mlPM_";
  protected final String TEXT_39 = ";" + NL + "    {" + NL + "        Object model = globalMap.get(\"";
  protected final String TEXT_40 = "_MODEL\");" + NL + "        if (model instanceof org.apache.spark.ml.PipelineModel) {" + NL + "            mlPM_";
  protected final String TEXT_41 = " = (org.apache.spark.ml.PipelineModel) model;" + NL + "        } else {" + NL + "            throw new Exception(\"Unsupported model type: \" + model.getClass());" + NL + "        }" + NL + "    }";
  protected final String TEXT_42 = NL + "        // SparkSQL will use the default TimeZone to handle dates (see org.apache.spark.sql.catalyst.util.DateTimeUtils)." + NL + "        // To avoid inconsistencies with others components in the Studio, ensure that UTC is set as the default TimeZone." + NL + "        java.util.TimeZone.setDefault(java.util.TimeZone.getTimeZone(\"UTC\"));" + NL + "        " + NL + "        // java.util.Date -> java.sql.Timestamp conversions to be compliant with Spark SQL before creating our DataFrame" + NL + "        org.apache.spark.api.java.JavaRDD<";
  protected final String TEXT_43 = "> ";
  protected final String TEXT_44 = " = rdd_";
  protected final String TEXT_45 = ".map(new ";
  protected final String TEXT_46 = "_From";
  protected final String TEXT_47 = "To";
  protected final String TEXT_48 = "());";
  protected final String TEXT_49 = NL + NL + "//Convert incoming RDD to DataFrame" + NL + "org.apache.spark.sql.SQLContext sqlContext_";
  protected final String TEXT_50 = " = new org.apache.spark.sql.SQLContext(ctx);";
  protected final String TEXT_51 = NL;
  protected final String TEXT_52 = " df_";
  protected final String TEXT_53 = " = sqlContext_";
  protected final String TEXT_54 = ".createDataFrame(";
  protected final String TEXT_55 = ", ";
  protected final String TEXT_56 = ".class);" + NL;
  protected final String TEXT_57 = NL + "org.talend.dataquality.reconciliation.util.MatchResultSplitter.MatchResults matchResult_";
  protected final String TEXT_58 = " = " + NL + "    org.talend.dataquality.reconciliation.api.MatchPredict.run(" + NL + "            df_";
  protected final String TEXT_59 = "," + NL + "            blockingPM_";
  protected final String TEXT_60 = "," + NL + "            filteringPM_";
  protected final String TEXT_61 = "," + NL + "            mlPM_";
  protected final String TEXT_62 = ",";
  protected final String TEXT_63 = NL + "            ";
  protected final String TEXT_64 = ",";
  protected final String TEXT_65 = NL + "            ";
  protected final String TEXT_66 = "," + NL + "            labelsNeedGroup_";
  protected final String TEXT_67 = ".toArray(new String[labelsNeedGroup_";
  protected final String TEXT_68 = ".size()]),";
  protected final String TEXT_69 = NL + "            ";
  protected final String TEXT_70 = ");" + NL;
  protected final String TEXT_71 = NL;
  protected final String TEXT_72 = " suspectDF_";
  protected final String TEXT_73 = " = matchResult_";
  protected final String TEXT_74 = ".suspectOut();" + NL;
  protected final String TEXT_75 = NL;
  protected final String TEXT_76 = NL;
  protected final String TEXT_77 = " suspectDF_";
  protected final String TEXT_78 = " = " + NL + "    org.talend.dataquality.reconciliation.api.MatchPredict.run(" + NL + "            df_";
  protected final String TEXT_79 = ", " + NL + "            mlPM_";
  protected final String TEXT_80 = "," + NL + "            labelsNeedGroup_";
  protected final String TEXT_81 = ".toArray(new String[labelsNeedGroup_";
  protected final String TEXT_82 = ".size()]),";
  protected final String TEXT_83 = NL + "            ";
  protected final String TEXT_84 = ");" + NL;
  protected final String TEXT_85 = "   " + NL + "// Convert suspect outgoing DataFrame to RDD" + NL + "org.apache.spark.api.java.JavaRDD<";
  protected final String TEXT_86 = "> rdd_";
  protected final String TEXT_87 = " =" + NL + "    suspectDF_";
  protected final String TEXT_88 = ".toJavaRDD().map(new ";
  protected final String TEXT_89 = "_FromRowTo";
  protected final String TEXT_90 = "());";
  protected final String TEXT_91 = NL;
  protected final String TEXT_92 = NL + "// Convert exact outgoing DataFrame to RDD" + NL + "org.apache.spark.api.java.JavaRDD<";
  protected final String TEXT_93 = "> rdd_";
  protected final String TEXT_94 = " =" + NL + "    matchResult_";
  protected final String TEXT_95 = ".exactOut().get().toJavaRDD().map(new ";
  protected final String TEXT_96 = "_FromRowTo";
  protected final String TEXT_97 = "());";
  protected final String TEXT_98 = NL;
  protected final String TEXT_99 = NL + "// Convert unique outgoing DataFrame to RDD" + NL + "org.apache.spark.api.java.JavaRDD<";
  protected final String TEXT_100 = "> rdd_";
  protected final String TEXT_101 = " =" + NL + "    matchResult_";
  protected final String TEXT_102 = ".uniqueOut().get().toJavaRDD().map(new ";
  protected final String TEXT_103 = "_FromRowTo";
  protected final String TEXT_104 = "());";

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    
//Parse the inputs to this javajet generator.
final BigDataCodeGeneratorArgument codeGenArgument = (BigDataCodeGeneratorArgument) argument;
final INode node = (INode)codeGenArgument.getArgument();
final String cid = node.getUniqueName();
final boolean isLog4jEnabled = ("true").equals(ElementParameterParser.getValue(node.getProcess(), "__LOG4J_ACTIVATE__"));

final boolean isSpark1 = org.talend.hadoop.distribution.ESparkVersion.SPARK_2_0.compareTo(codeGenArgument.getSparkVersion()) > 0;

final String dataframeClass = isSpark1
    ? "org.apache.spark.sql.DataFrame"
    : "org.apache.spark.sql.Dataset<org.apache.spark.sql.Row>";

TSqlRowUtil tSqlRowUtil = new TSqlRowUtil(node);
String validateError = tSqlRowUtil.validate(true, true);
if (validateError != null) {
    // Cause the job compilation to explicitly fail if there is a problem.
    return "throw new JobConfigurationError(\"" + validateError +"\");";
}

    
//IO Connections
IConnection incomingConnection = null;
if (node.getIncomingConnections() != null) {
 for (IConnection in : node.getIncomingConnections()) {
     if (in.getLineStyle().hasConnectionCategory(IConnectionCategory.DATA)) {
         incomingConnection = in;
         break;
     }
 }
}
String incomingConnectionName = incomingConnection.getName();
String incomingStructName = codeGenArgument.getRecordStructName(incomingConnection);

IConnection suspectConnection = null;
boolean hasSuspectConnection = false;
List<? extends IConnection> outgoingSuspectConnections = node.getOutgoingConnections("SUSPECT");
if (outgoingSuspectConnections.size() > 0) {
    suspectConnection = outgoingSuspectConnections.get(0);
}
if(suspectConnection != null){
    hasSuspectConnection = true;
}

IConnection exactConnection = null;
boolean hasExactConnection = false;
List<? extends IConnection> outgoingExactConnections = node.getOutgoingConnections("EXACT");
if (outgoingExactConnections.size() > 0) {
    exactConnection = outgoingExactConnections.get(0);
}
if(exactConnection != null){
    hasExactConnection = true;
}

IConnection uniqueConnection = null;
boolean hasUniqueConnection = false;
List<? extends IConnection> outgoingUniqueConnections = node.getOutgoingConnections("UNIQUE");
if (outgoingUniqueConnections.size() > 0) {
    uniqueConnection = outgoingUniqueConnections.get(0);
}
if(uniqueConnection != null){
    hasUniqueConnection = true;
}

IConnection suspectSamplingConnection = null;
boolean hasSuspectSamplingConnection = false;
List<? extends IConnection> outgoingSuspectSamplingConnections = node.getOutgoingConnections("SUSPECT_SAMPLING");
if (outgoingSuspectSamplingConnections.size() > 0) {
    suspectSamplingConnection = outgoingSuspectSamplingConnections.get(0);
}
if(suspectSamplingConnection != null){
    hasSuspectSamplingConnection = true;
}


    
boolean useConfigurationComponent = "true".equals(ElementParameterParser.getValue(node, "__DEFINE_STORAGE_CONFIGURATION__"));
String pairingModelFolder = ElementParameterParser.getValue(node,"__PAIRING_HDFS_FOLDER__");
Boolean savedOnDisk = ElementParameterParser.getValue(node, "__ML_MODEL_LOCATION__").equals("FILE_SYSTEM")? true : false;
String mlModelFolder = ElementParameterParser.getValue(node,"__ML_HDFS_FOLDER__");
String checkpointInterval = ElementParameterParser.getValue(node,"__CHECKPOINT_INTERVAL__");
String uriPrefix = "\"\"";
if(useConfigurationComponent) {
    uriPrefix = org.talend.designer.spark.generator.storage.SparkStorageUtils.getURIPrefix(node);
    pairingModelFolder = uriPrefix + " + " + pairingModelFolder;
    mlModelFolder = uriPrefix + " + " + mlModelFolder;
}
if(!"\"\"".equals(uriPrefix)) {
    
    stringBuffer.append(TEXT_1);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_2);
    stringBuffer.append(uriPrefix);
    stringBuffer.append(TEXT_3);
    
}
boolean needPairing = "UNPAIRED".equals(ElementParameterParser.getValue(node, "__INPUT_TYPE__"));
List<Map<String, String>> labelsTable = (List<Map<String, String>>)ElementParameterParser.getObjectValue(node, "__CLUSTERING_LABELS__");

    stringBuffer.append(TEXT_4);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_5);
    
if(labelsTable!=null){
    for (Map<String, String> labelsMap : labelsTable) {
        String label = labelsMap.get("LABEL");
        if(label!=null){
        
    stringBuffer.append(TEXT_6);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_7);
    stringBuffer.append(label);
    stringBuffer.append(TEXT_8);
    
        }
    }
}

    stringBuffer.append(TEXT_9);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_10);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_11);
    
if(needPairing){

    stringBuffer.append(TEXT_12);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_13);
    stringBuffer.append(pairingModelFolder);
    stringBuffer.append(TEXT_14);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_15);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_16);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_17);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_18);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_19);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_20);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_21);
    stringBuffer.append(pairingModelFolder);
    stringBuffer.append(TEXT_22);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_23);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_24);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_25);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_26);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_27);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_28);
    
}

//Get the ml pipeline model,
if (savedOnDisk) {
    
    stringBuffer.append(TEXT_29);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_30);
    stringBuffer.append(mlModelFolder);
    stringBuffer.append(TEXT_31);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_32);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_33);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_34);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_35);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_36);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_37);
    
} else {
    String mlModelCid = ElementParameterParser.getValue(node, "__ML_MODEL_NAME__");
    
    stringBuffer.append(TEXT_38);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_39);
    stringBuffer.append(mlModelCid);
    stringBuffer.append(TEXT_40);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_41);
    
}

String rddName, structName;
if(tSqlRowUtil.containsDateFields(incomingConnection)) {
    // Additional map to convert from java.util.Date to java.sql.Date or java.sql.Timestamp
        String newRddName = "tmp_rdd_"+incomingConnectionName;
        String newStructName = "DF_"+incomingStructName+"AvroRecord";

    stringBuffer.append(TEXT_42);
    stringBuffer.append(newStructName);
    stringBuffer.append(TEXT_43);
    stringBuffer.append(newRddName);
    stringBuffer.append(TEXT_44);
    stringBuffer.append(incomingConnectionName);
    stringBuffer.append(TEXT_45);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_46);
    stringBuffer.append(incomingStructName);
    stringBuffer.append(TEXT_47);
    stringBuffer.append(newStructName);
    stringBuffer.append(TEXT_48);
    
        rddName = newRddName;
        structName = newStructName;
} else {
        // No need for additional map
        rddName = "rdd_"+incomingConnectionName;
        structName = incomingStructName;
}

    stringBuffer.append(TEXT_49);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_50);
    stringBuffer.append(TEXT_51);
    stringBuffer.append(dataframeClass);
    stringBuffer.append(TEXT_52);
    stringBuffer.append(incomingConnectionName);
    stringBuffer.append(TEXT_53);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_54);
    stringBuffer.append(rddName);
    stringBuffer.append(TEXT_55);
    stringBuffer.append(structName);
    stringBuffer.append(TEXT_56);
    
if(needPairing){

    stringBuffer.append(TEXT_57);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_58);
    stringBuffer.append(incomingConnectionName);
    stringBuffer.append(TEXT_59);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_60);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_61);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_62);
    stringBuffer.append(TEXT_63);
    stringBuffer.append(hasExactConnection);
    stringBuffer.append(TEXT_64);
    stringBuffer.append(TEXT_65);
    stringBuffer.append(hasUniqueConnection);
    stringBuffer.append(TEXT_66);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_67);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_68);
    stringBuffer.append(TEXT_69);
    stringBuffer.append(checkpointInterval);
    stringBuffer.append(TEXT_70);
    stringBuffer.append(TEXT_71);
    stringBuffer.append(dataframeClass);
    stringBuffer.append(TEXT_72);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_73);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_74);
    
}else{

    stringBuffer.append(TEXT_75);
    stringBuffer.append(TEXT_76);
    stringBuffer.append(dataframeClass);
    stringBuffer.append(TEXT_77);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_78);
    stringBuffer.append(incomingConnectionName);
    stringBuffer.append(TEXT_79);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_80);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_81);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_82);
    stringBuffer.append(TEXT_83);
    stringBuffer.append(checkpointInterval);
    stringBuffer.append(TEXT_84);
    
}

if(hasSuspectConnection){
    String suspectOutConnectionName = suspectConnection.getName();
    String suspectOutStructName = codeGenArgument.getRecordStructName(suspectConnection);

    stringBuffer.append(TEXT_85);
    stringBuffer.append(suspectOutStructName);
    stringBuffer.append(TEXT_86);
    stringBuffer.append(suspectOutConnectionName);
    stringBuffer.append(TEXT_87);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_88);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_89);
    stringBuffer.append(suspectOutStructName);
    stringBuffer.append(TEXT_90);
    
}

    stringBuffer.append(TEXT_91);
    
if(hasExactConnection && needPairing){
    String exactOutConnectionName = exactConnection.getName();
    String exactOutStructName = codeGenArgument.getRecordStructName(exactConnection);

    stringBuffer.append(TEXT_92);
    stringBuffer.append(exactOutStructName);
    stringBuffer.append(TEXT_93);
    stringBuffer.append(exactOutConnectionName);
    stringBuffer.append(TEXT_94);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_95);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_96);
    stringBuffer.append(exactOutStructName);
    stringBuffer.append(TEXT_97);
    
}

    stringBuffer.append(TEXT_98);
    
if(hasUniqueConnection && needPairing){
    String uniqueOutConnectionName = uniqueConnection.getName();
    String uniqueOutStructName = codeGenArgument.getRecordStructName(uniqueConnection);

    stringBuffer.append(TEXT_99);
    stringBuffer.append(uniqueOutStructName);
    stringBuffer.append(TEXT_100);
    stringBuffer.append(uniqueOutConnectionName);
    stringBuffer.append(TEXT_101);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_102);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_103);
    stringBuffer.append(uniqueOutStructName);
    stringBuffer.append(TEXT_104);
    
}

    return stringBuffer.toString();
  }
}
