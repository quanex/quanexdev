package org.talend.designer.codegen.translators.machinelearning.classification;

import java.util.List;
import org.talend.core.model.metadata.IMetadataColumn;
import org.talend.core.model.metadata.IMetadataTable;
import org.talend.core.model.metadata.types.JavaType;
import org.talend.core.model.metadata.types.JavaTypesManager;
import org.talend.core.model.process.ElementParameterParser;
import org.talend.core.model.process.EConnectionType;
import org.talend.core.model.process.IConnection;
import org.talend.core.model.process.IConnectionCategory;
import org.talend.core.model.process.INode;
import org.talend.core.model.process.IBigDataNode;
import org.talend.designer.common.tsqlrow.TSqlRowUtil;
import org.talend.designer.common.BigDataCodeGeneratorArgument;

public class TPredictSparkstreamingconfigJava
{
  protected static String nl;
  public static synchronized TPredictSparkstreamingconfigJava create(String lineSeparator)
  {
    nl = lineSeparator;
    TPredictSparkstreamingconfigJava result = new TPredictSparkstreamingconfigJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = NL + "// 1. Model Loading" + NL + "// Common processing for both batch & streaming version of tPredict" + NL + "final org.apache.spark.mllib.classification.talend.NaiveBayesRegularRecordsModel model_";
  protected final String TEXT_2 = " =";
  protected final String TEXT_3 = NL + "        (org.apache.spark.mllib.classification.talend.NaiveBayesRegularRecordsModel) org.talend.datascience.mllib.pmml.imports.ModelImporter" + NL + "            .fromPMML(";
  protected final String TEXT_4 = ", ";
  protected final String TEXT_5 = ");";
  protected final String TEXT_6 = NL + "        (org.apache.spark.mllib.classification.talend.NaiveBayesRegularRecordsModel) org.talend.datascience.mllib.pmml.imports.ModelImporter" + NL + "                .fromPMML(";
  protected final String TEXT_7 = ");";
  protected final String TEXT_8 = NL;
  protected final String TEXT_9 = NL + NL + "\t//Streaming version of tPredict" + NL + "\torg.apache.spark.streaming.api.java.JavaDStream<";
  protected final String TEXT_10 = ">" + NL + "\t\trdd_";
  protected final String TEXT_11 = " = rdd_";
  protected final String TEXT_12 = ".transform(" + NL + "" + NL + "\t\t    new org.apache.spark.api.java.function.Function<org.apache.spark.api.java.JavaRDD<";
  protected final String TEXT_13 = ">, org.apache.spark.api.java.JavaRDD<";
  protected final String TEXT_14 = ">>() {" + NL + "\t\t\t\t@Override" + NL + "\t\t\t\tpublic org.apache.spark.api.java.JavaRDD<";
  protected final String TEXT_15 = "> call(org.apache.spark.api.java.JavaRDD<";
  protected final String TEXT_16 = "> rdd) {" + NL + "" + NL + "\t\t\t\t\t// 2. Create dataFrame" + NL + "\t\t\t\t\torg.apache.spark.sql.SQLContext sqlContext_";
  protected final String TEXT_17 = " = org.talend.datascience.util.SQLUtil.getSQLContextSingleton(rdd.context());" + NL + "\t\t\t\t    org.apache.spark.sql.DataFrame df_";
  protected final String TEXT_18 = " =" + NL + "\t\t\t\t    \tsqlContext_";
  protected final String TEXT_19 = ".createDataFrame(rdd, ";
  protected final String TEXT_20 = ".class);" + NL + "" + NL + "\t\t\t\t    // 3. Call predictor" + NL + "\t\t\t\t    return (org.talend.datascience.mllib.classification.NaiveBayes.predictor(" + NL + "\t\t\t\t    \t\t\tdf_";
  protected final String TEXT_21 = ".rdd(), // rdd<Row>" + NL + "\t\t\t\t    \t\t\tdf_";
  protected final String TEXT_22 = ".schema(), // schema" + NL + "\t\t\t\t    \t\t\tmodel_";
  protected final String TEXT_23 = " // model" + NL + "\t\t\t\t    \t\t).toJavaRDD()).map(new ";
  protected final String TEXT_24 = "_FromRowTo";
  protected final String TEXT_25 = "());" + NL + "\t\t\t\t}" + NL + "\t\t\t}" + NL + "\t\t);" + NL;
  protected final String TEXT_26 = NL + "\t// Batch version of tPredict" + NL + "\t// 2. Create dataFrame from incoming rdd & rowStruct" + NL + "\torg.apache.spark.sql.SQLContext sqlContext_";
  protected final String TEXT_27 = " = new org.apache.spark.sql.SQLContext(";
  protected final String TEXT_28 = ");" + NL + "\torg.apache.spark.sql.DataFrame df_";
  protected final String TEXT_29 = "_";
  protected final String TEXT_30 = " =" + NL + "\t\tsqlContext_";
  protected final String TEXT_31 = ".createDataFrame(rdd_";
  protected final String TEXT_32 = ", ";
  protected final String TEXT_33 = ".class);" + NL + "" + NL + "\t// 3. Call predictor" + NL + "\torg.apache.spark.api.java.JavaRDD<";
  protected final String TEXT_34 = "> rdd_";
  protected final String TEXT_35 = " =" + NL + "\t\t(org.talend.datascience.mllib.classification.NaiveBayes.predictor(" + NL + "\t\t\tdf_";
  protected final String TEXT_36 = "_";
  protected final String TEXT_37 = ".rdd(), // rdd<Row>" + NL + "\t\t\tdf_";
  protected final String TEXT_38 = "_";
  protected final String TEXT_39 = ".schema(), // schema" + NL + "\t\t\tmodel_";
  protected final String TEXT_40 = " // model" + NL + "\t\t).toJavaRDD()).map(new ";
  protected final String TEXT_41 = "_FromRowTo";
  protected final String TEXT_42 = "());";
  protected final String TEXT_43 = NL + "org.apache.spark.streaming.api.java.JavaDStream<";
  protected final String TEXT_44 = "> rdd_";
  protected final String TEXT_45 = ";" + NL + "org.apache.spark.mllib.classification.NaiveBayesModel currentModel_";
  protected final String TEXT_46 = ";" + NL + "{";
  protected final String TEXT_47 = NL + "            java.net.URI currentURI_";
  protected final String TEXT_48 = "_config = FileSystem.getDefaultUri(ctx.sparkContext().hadoopConfiguration());" + NL + "            FileSystem.setDefaultUri(ctx.sparkContext().hadoopConfiguration(), new java.net.URI(";
  protected final String TEXT_49 = "));" + NL + "            fs = FileSystem.get(ctx.sparkContext().hadoopConfiguration());";
  protected final String TEXT_50 = NL + "        com.esotericsoftware.kryo.Kryo kryo_";
  protected final String TEXT_51 = " = new com.esotericsoftware.kryo.Kryo();" + NL + "        com.esotericsoftware.kryo.io.Input featuresInput_";
  protected final String TEXT_52 = " = new com.esotericsoftware.kryo.io.Input(fs.open(new org.apache.hadoop.fs.Path(";
  protected final String TEXT_53 = " + \"/features\")));" + NL + "        TalendPipelineModel featuresTalendPipelineModel_";
  protected final String TEXT_54 = " = kryo_";
  protected final String TEXT_55 = ".readObject(featuresInput_";
  protected final String TEXT_56 = ", TalendPipelineModel.class, new TalendPipelineModelSerializer());" + NL + "" + NL + "        currentModel_";
  protected final String TEXT_57 = " = org.apache.spark.mllib.classification.NaiveBayesModel.load(ctx.sparkContext().sc(), ";
  protected final String TEXT_58 = " + \"/model\");";
  protected final String TEXT_59 = NL + "             FileSystem.setDefaultUri(ctx.sparkContext().hadoopConfiguration(), currentURI_";
  protected final String TEXT_60 = "_config);" + NL + "             fs = FileSystem.get(ctx.sparkContext().hadoopConfiguration());";
  protected final String TEXT_61 = NL + "    " + NL + "    java.util.Map<String, String> featuresParamsMap_";
  protected final String TEXT_62 = " = featuresTalendPipelineModel_";
  protected final String TEXT_63 = ".getParams();" + NL + "    String vectorName_";
  protected final String TEXT_64 = " = featuresParamsMap_";
  protected final String TEXT_65 = ".get(\"VECTOR_NAME\");" + NL + "    org.apache.spark.ml.PipelineModel pipelineModel_";
  protected final String TEXT_66 = " = featuresTalendPipelineModel_";
  protected final String TEXT_67 = ".getPipelineModel();" + NL + "    " + NL + "" + NL + "    rdd_";
  protected final String TEXT_68 = " = rdd_";
  protected final String TEXT_69 = ".transform(new Classify_";
  protected final String TEXT_70 = "(pipelineModel_";
  protected final String TEXT_71 = ",vectorName_";
  protected final String TEXT_72 = ",currentModel_";
  protected final String TEXT_73 = " ));" + NL + "}";
  protected final String TEXT_74 = NL + "    java.net.URI currentURI_";
  protected final String TEXT_75 = "_config = FileSystem.getDefaultUri(ctx.sparkContext().hadoopConfiguration());" + NL + "    FileSystem.setDefaultUri(ctx.sparkContext().hadoopConfiguration(), new java.net.URI(";
  protected final String TEXT_76 = "));" + NL + "    fs = FileSystem.get(ctx.sparkContext().hadoopConfiguration());";
  protected final String TEXT_77 = NL + "org.apache.spark.streaming.api.java.JavaDStream<";
  protected final String TEXT_78 = "> rdd_";
  protected final String TEXT_79 = ";" + NL + "{";
  protected final String TEXT_80 = NL + "        com.esotericsoftware.kryo.Kryo kryo_";
  protected final String TEXT_81 = " = new com.esotericsoftware.kryo.Kryo();" + NL + "        com.esotericsoftware.kryo.io.Input featuresInput_";
  protected final String TEXT_82 = " = new com.esotericsoftware.kryo.io.Input(fs.open(new org.apache.hadoop.fs.Path(";
  protected final String TEXT_83 = " + \"/model/\")));" + NL + "        TalendPipelineModelSerializer talendPipelineModelSerializer_";
  protected final String TEXT_84 = " = new TalendPipelineModelSerializer();" + NL + "        TalendPipelineModel featuresTalendPipelineModel_";
  protected final String TEXT_85 = " = kryo_";
  protected final String TEXT_86 = ".readObject(featuresInput_";
  protected final String TEXT_87 = ", TalendPipelineModel.class, talendPipelineModelSerializer_";
  protected final String TEXT_88 = ");" + NL + "" + NL + "        java.util.Map<String, String> featuresParamsMap_";
  protected final String TEXT_89 = " = featuresTalendPipelineModel_";
  protected final String TEXT_90 = ".getParams();" + NL + "" + NL + "        org.apache.spark.ml.PipelineModel pipelineModel_";
  protected final String TEXT_91 = " = featuresTalendPipelineModel_";
  protected final String TEXT_92 = ".getPipelineModel();";
  protected final String TEXT_93 = NL + "        org.apache.spark.ml.PipelineModel pipelineModel_";
  protected final String TEXT_94 = ";" + NL + "        {" + NL + "            Object model = globalMap.get(\"";
  protected final String TEXT_95 = "_MODEL\");" + NL + "            if (model instanceof org.apache.spark.ml.PipelineModel) {" + NL + "                pipelineModel_";
  protected final String TEXT_96 = " = (org.apache.spark.ml.PipelineModel) model;" + NL + "            } else {" + NL + "                throw new Exception(\"Unsupported model type: \" + model.getClass());" + NL + "            }" + NL + "        }";
  protected final String TEXT_97 = NL + NL + "    rdd_";
  protected final String TEXT_98 = " = rdd_";
  protected final String TEXT_99 = ".transform(new Predict_";
  protected final String TEXT_100 = "(pipelineModel_";
  protected final String TEXT_101 = "));" + NL + "}" + NL;
  protected final String TEXT_102 = NL + "org.apache.spark.streaming.api.java.JavaDStream<";
  protected final String TEXT_103 = "> rdd_";
  protected final String TEXT_104 = ";" + NL + "{";
  protected final String TEXT_105 = NL + "            java.net.URI currentURI_";
  protected final String TEXT_106 = "_config = FileSystem.getDefaultUri(ctx.sparkContext().hadoopConfiguration());" + NL + "            FileSystem.setDefaultUri(ctx.sparkContext().hadoopConfiguration(), new java.net.URI(";
  protected final String TEXT_107 = "));" + NL + "            fs = FileSystem.get(ctx.sparkContext().hadoopConfiguration());";
  protected final String TEXT_108 = NL + "        com.esotericsoftware.kryo.Kryo kryo_";
  protected final String TEXT_109 = " = new com.esotericsoftware.kryo.Kryo();" + NL + "        com.esotericsoftware.kryo.io.Input featuresInput_";
  protected final String TEXT_110 = " = new com.esotericsoftware.kryo.io.Input(fs.open(new org.apache.hadoop.fs.Path(";
  protected final String TEXT_111 = " + \"/model/\")));" + NL + "        TalendPipelineModelSerializer talendPipelineModelSerializer_";
  protected final String TEXT_112 = " = new TalendPipelineModelSerializer();" + NL + "        TalendPipelineModel featuresTalendPipelineModel_";
  protected final String TEXT_113 = " = kryo_";
  protected final String TEXT_114 = ".readObject(featuresInput_";
  protected final String TEXT_115 = ", TalendPipelineModel.class, talendPipelineModelSerializer_";
  protected final String TEXT_116 = ");" + NL + "" + NL + "        java.util.Map<String, String> featuresParamsMap_";
  protected final String TEXT_117 = " = featuresTalendPipelineModel_";
  protected final String TEXT_118 = ".getParams();" + NL + "" + NL + "        org.apache.spark.ml.PipelineModel pipelineModel_";
  protected final String TEXT_119 = " = featuresTalendPipelineModel_";
  protected final String TEXT_120 = ".getPipelineModel();";
  protected final String TEXT_121 = NL + "        org.apache.spark.ml.PipelineModel pipelineModel_";
  protected final String TEXT_122 = ";" + NL + "        {" + NL + "            Object model = globalMap.get(\"";
  protected final String TEXT_123 = "_MODEL\");" + NL + "            if (model instanceof org.apache.spark.ml.PipelineModel) {" + NL + "                pipelineModel_";
  protected final String TEXT_124 = " = (org.apache.spark.ml.PipelineModel) model;" + NL + "            } else {" + NL + "                throw new Exception(\"Unsupported model type: \" + model.getClass());" + NL + "            }" + NL + "        }";
  protected final String TEXT_125 = NL + NL + "    rdd_";
  protected final String TEXT_126 = " = rdd_";
  protected final String TEXT_127 = ".transform(new Classify_";
  protected final String TEXT_128 = "(pipelineModel_";
  protected final String TEXT_129 = "));" + NL + "}" + NL;
  protected final String TEXT_130 = NL + "org.apache.spark.streaming.api.java.JavaDStream<";
  protected final String TEXT_131 = "> rdd_";
  protected final String TEXT_132 = ";" + NL + "{";
  protected final String TEXT_133 = NL + "            java.net.URI currentURI_";
  protected final String TEXT_134 = "_config = FileSystem.getDefaultUri(ctx.sparkContext().hadoopConfiguration());" + NL + "            FileSystem.setDefaultUri(ctx.sparkContext().hadoopConfiguration(), new java.net.URI(";
  protected final String TEXT_135 = "));" + NL + "            fs = FileSystem.get(ctx.sparkContext().hadoopConfiguration());";
  protected final String TEXT_136 = NL + "        com.esotericsoftware.kryo.Kryo kryo_";
  protected final String TEXT_137 = " = new com.esotericsoftware.kryo.Kryo();" + NL + "        com.esotericsoftware.kryo.io.Input featuresInput_";
  protected final String TEXT_138 = " = new com.esotericsoftware.kryo.io.Input(fs.open(new org.apache.hadoop.fs.Path(";
  protected final String TEXT_139 = " + \"/model/\")));" + NL + "        TalendPipelineModelSerializer talendPipelineModelSerializer_";
  protected final String TEXT_140 = " = new TalendPipelineModelSerializer();" + NL + "        TalendPipelineModel featuresTalendPipelineModel_";
  protected final String TEXT_141 = " = kryo_";
  protected final String TEXT_142 = ".readObject(featuresInput_";
  protected final String TEXT_143 = ", TalendPipelineModel.class, talendPipelineModelSerializer_";
  protected final String TEXT_144 = ");" + NL + "" + NL + "        java.util.Map<String, String> featuresParamsMap_";
  protected final String TEXT_145 = " = featuresTalendPipelineModel_";
  protected final String TEXT_146 = ".getParams();" + NL + "" + NL + "        org.apache.spark.ml.PipelineModel pipelineModel_";
  protected final String TEXT_147 = " = featuresTalendPipelineModel_";
  protected final String TEXT_148 = ".getPipelineModel();";
  protected final String TEXT_149 = NL + "        org.apache.spark.ml.PipelineModel pipelineModel_";
  protected final String TEXT_150 = ";" + NL + "        {" + NL + "            Object model = globalMap.get(\"";
  protected final String TEXT_151 = "_MODEL\");" + NL + "            if (model instanceof org.apache.spark.ml.PipelineModel) {" + NL + "                pipelineModel_";
  protected final String TEXT_152 = " = (org.apache.spark.ml.PipelineModel) model;" + NL + "            } else {" + NL + "                throw new Exception(\"Unsupported model type: \" + model.getClass());" + NL + "            }" + NL + "        }";
  protected final String TEXT_153 = NL + NL + "    rdd_";
  protected final String TEXT_154 = " = rdd_";
  protected final String TEXT_155 = ".transform(new Classify_";
  protected final String TEXT_156 = "(pipelineModel_";
  protected final String TEXT_157 = "));" + NL + "}" + NL;
  protected final String TEXT_158 = NL + "org.apache.spark.streaming.api.java.JavaDStream<";
  protected final String TEXT_159 = "> rdd_";
  protected final String TEXT_160 = ";" + NL + "{";
  protected final String TEXT_161 = NL + "            java.net.URI currentURI_";
  protected final String TEXT_162 = "_config = FileSystem.getDefaultUri(ctx.sparkContext().hadoopConfiguration());" + NL + "            FileSystem.setDefaultUri(ctx.sparkContext().hadoopConfiguration(), new java.net.URI(";
  protected final String TEXT_163 = "));" + NL + "            fs = FileSystem.get(ctx.sparkContext().hadoopConfiguration());";
  protected final String TEXT_164 = NL + "        com.esotericsoftware.kryo.Kryo kryo_";
  protected final String TEXT_165 = " = new com.esotericsoftware.kryo.Kryo();" + NL + "        com.esotericsoftware.kryo.io.Input featuresInput_";
  protected final String TEXT_166 = " = new com.esotericsoftware.kryo.io.Input(fs.open(new org.apache.hadoop.fs.Path(";
  protected final String TEXT_167 = " + \"/model/\")));" + NL + "        TalendPipelineModelSerializer talendPipelineModelSerializer_";
  protected final String TEXT_168 = " = new TalendPipelineModelSerializer();" + NL + "        TalendPipelineModel featuresTalendPipelineModel_";
  protected final String TEXT_169 = " = kryo_";
  protected final String TEXT_170 = ".readObject(featuresInput_";
  protected final String TEXT_171 = ", TalendPipelineModel.class, talendPipelineModelSerializer_";
  protected final String TEXT_172 = ");" + NL + "" + NL + "        java.util.Map<String, String> featuresParamsMap_";
  protected final String TEXT_173 = " = featuresTalendPipelineModel_";
  protected final String TEXT_174 = ".getParams();" + NL + "" + NL + "        org.apache.spark.ml.PipelineModel pipelineModel_";
  protected final String TEXT_175 = " = featuresTalendPipelineModel_";
  protected final String TEXT_176 = ".getPipelineModel();";
  protected final String TEXT_177 = NL + "        org.apache.spark.ml.PipelineModel pipelineModel_";
  protected final String TEXT_178 = ";" + NL + "        {" + NL + "            Object model = globalMap.get(\"";
  protected final String TEXT_179 = "_MODEL\");" + NL + "            if (model instanceof org.apache.spark.ml.PipelineModel) {" + NL + "                pipelineModel_";
  protected final String TEXT_180 = " = (org.apache.spark.ml.PipelineModel) model;" + NL + "            } else {" + NL + "                throw new Exception(\"Unsupported model type: \" + model.getClass());" + NL + "            }" + NL + "        }";
  protected final String TEXT_181 = NL + NL + "    rdd_";
  protected final String TEXT_182 = " = rdd_";
  protected final String TEXT_183 = ".transform(new Classify_";
  protected final String TEXT_184 = "(pipelineModel_";
  protected final String TEXT_185 = "));" + NL + "}" + NL;
  protected final String TEXT_186 = NL + "org.apache.spark.streaming.api.java.JavaDStream<";
  protected final String TEXT_187 = "> rdd_";
  protected final String TEXT_188 = ";" + NL + "{";
  protected final String TEXT_189 = NL + "            java.net.URI currentURI_";
  protected final String TEXT_190 = "_config = FileSystem.getDefaultUri(ctx.sparkContext().hadoopConfiguration());" + NL + "            FileSystem.setDefaultUri(ctx.sparkContext().hadoopConfiguration(), new java.net.URI(";
  protected final String TEXT_191 = "));" + NL + "            fs = FileSystem.get(ctx.sparkContext().hadoopConfiguration());";
  protected final String TEXT_192 = NL + "        com.esotericsoftware.kryo.Kryo kryo_";
  protected final String TEXT_193 = " = new com.esotericsoftware.kryo.Kryo();" + NL + "        com.esotericsoftware.kryo.io.Input featuresInput_";
  protected final String TEXT_194 = " = new com.esotericsoftware.kryo.io.Input(fs.open(new org.apache.hadoop.fs.Path(";
  protected final String TEXT_195 = " + \"/model/\")));" + NL + "        TalendPipelineModelSerializer talendPipelineModelSerializer_";
  protected final String TEXT_196 = " = new TalendPipelineModelSerializer();" + NL + "        TalendPipelineModel featuresTalendPipelineModel_";
  protected final String TEXT_197 = " = kryo_";
  protected final String TEXT_198 = ".readObject(featuresInput_";
  protected final String TEXT_199 = ", TalendPipelineModel.class, talendPipelineModelSerializer_";
  protected final String TEXT_200 = ");" + NL + "" + NL + "        java.util.Map<String, String> featuresParamsMap_";
  protected final String TEXT_201 = " = featuresTalendPipelineModel_";
  protected final String TEXT_202 = ".getParams();" + NL + "" + NL + "        org.apache.spark.ml.PipelineModel pipelineModel_";
  protected final String TEXT_203 = " = featuresTalendPipelineModel_";
  protected final String TEXT_204 = ".getPipelineModel();";
  protected final String TEXT_205 = NL + "        org.apache.spark.ml.PipelineModel pipelineModel_";
  protected final String TEXT_206 = ";" + NL + "        {" + NL + "            Object model = globalMap.get(\"";
  protected final String TEXT_207 = "_MODEL\");" + NL + "            if (model instanceof org.apache.spark.ml.PipelineModel) {" + NL + "                pipelineModel_";
  protected final String TEXT_208 = " = (org.apache.spark.ml.PipelineModel) model;" + NL + "            } else {" + NL + "                throw new Exception(\"Unsupported model type: \" + model.getClass());" + NL + "            }" + NL + "        }";
  protected final String TEXT_209 = NL + NL + "    rdd_";
  protected final String TEXT_210 = " = rdd_";
  protected final String TEXT_211 = ".transform(new Classify_";
  protected final String TEXT_212 = "(pipelineModel_";
  protected final String TEXT_213 = "));" + NL + "}" + NL;
  protected final String TEXT_214 = NL + NL;
  protected final String TEXT_215 = NL + "    java.net.URI currentURI_";
  protected final String TEXT_216 = "_config = FileSystem.getDefaultUri(ctx.sparkContext().hadoopConfiguration());" + NL + "    FileSystem.setDefaultUri(ctx.sparkContext().hadoopConfiguration(), new java.net.URI(";
  protected final String TEXT_217 = "));" + NL + "    fs = FileSystem.get(ctx.sparkContext().hadoopConfiguration());";
  protected final String TEXT_218 = NL + "com.esotericsoftware.kryo.Kryo kryo_";
  protected final String TEXT_219 = " = new com.esotericsoftware.kryo.Kryo();" + NL + "com.esotericsoftware.kryo.io.Input featuresInput_";
  protected final String TEXT_220 = " = new com.esotericsoftware.kryo.io.Input(fs.open(new org.apache.hadoop.fs.Path(";
  protected final String TEXT_221 = " + \"/features\")));" + NL + "TalendPipelineModel featuresTalendPipelineModel_";
  protected final String TEXT_222 = " = kryo_";
  protected final String TEXT_223 = ".readObject(featuresInput_";
  protected final String TEXT_224 = ", TalendPipelineModel.class, new TalendPipelineModelSerializer());" + NL + "" + NL + "org.apache.spark.mllib.classification.SVMModel currentModel_";
  protected final String TEXT_225 = " = org.apache.spark.mllib.classification.SVMModel.load(ctx.sparkContext().sc(), ";
  protected final String TEXT_226 = " + \"/model\");" + NL + "" + NL + "java.util.Map<String, String> featuresParamsMap_";
  protected final String TEXT_227 = " = featuresTalendPipelineModel_";
  protected final String TEXT_228 = ".getParams();" + NL + "org.apache.spark.ml.PipelineModel featuresTransformationsModel_";
  protected final String TEXT_229 = " = featuresTalendPipelineModel_";
  protected final String TEXT_230 = ".getPipelineModel();" + NL + "String vectorName_";
  protected final String TEXT_231 = " = featuresParamsMap_";
  protected final String TEXT_232 = ".get(\"VECTOR_NAME\");" + NL + "" + NL + "" + NL + " // Create context" + NL + " org.apache.spark.sql.SQLContext sqlContext_";
  protected final String TEXT_233 = " = new org.apache.spark.sql.SQLContext(ctx.sparkContext());" + NL + "" + NL + " // Go to RDD view in order to use dataframe" + NL + " org.apache.spark.streaming.api.java.JavaDStream<";
  protected final String TEXT_234 = "> rdd_";
  protected final String TEXT_235 = " =" + NL + "     rdd_";
  protected final String TEXT_236 = ".transform(new GenerateEncodedStruct_";
  protected final String TEXT_237 = "(vectorName_";
  protected final String TEXT_238 = ", featuresTransformationsModel_";
  protected final String TEXT_239 = ", sqlContext_";
  protected final String TEXT_240 = "))" + NL + "             .map(new GetPrediction_";
  protected final String TEXT_241 = "(currentModel_";
  protected final String TEXT_242 = "));";
  protected final String TEXT_243 = NL + "    java.net.URI currentURI_";
  protected final String TEXT_244 = "_config = FileSystem.getDefaultUri(ctx.sparkContext().hadoopConfiguration());" + NL + "    FileSystem.setDefaultUri(ctx.sparkContext().hadoopConfiguration(), new java.net.URI(";
  protected final String TEXT_245 = "));" + NL + "    fs = FileSystem.get(ctx.sparkContext().hadoopConfiguration());";
  protected final String TEXT_246 = NL + "    com.esotericsoftware.kryo.Kryo kryo_";
  protected final String TEXT_247 = " = new com.esotericsoftware.kryo.Kryo();" + NL + "    com.esotericsoftware.kryo.io.Input featuresInput_";
  protected final String TEXT_248 = " = new com.esotericsoftware.kryo.io.Input(fs.open(new org.apache.hadoop.fs.Path(";
  protected final String TEXT_249 = " + \"/features\")));" + NL + "    TalendPipelineModel featuresTalendPipelineModel_";
  protected final String TEXT_250 = " = kryo_";
  protected final String TEXT_251 = ".readObject(featuresInput_";
  protected final String TEXT_252 = ", TalendPipelineModel.class, new TalendPipelineModelSerializer());" + NL + "" + NL + "    org.apache.spark.mllib.clustering.KMeansModel currentModel_";
  protected final String TEXT_253 = " = org.apache.spark.mllib.clustering.KMeansModel.load(ctx.sparkContext().sc(), ";
  protected final String TEXT_254 = " + \"/model\");" + NL + "" + NL + "    java.util.Map<String, String> featuresParamsMap_";
  protected final String TEXT_255 = " = featuresTalendPipelineModel_";
  protected final String TEXT_256 = ".getParams();" + NL + "    org.apache.spark.ml.PipelineModel featuresTransformationsModel_";
  protected final String TEXT_257 = " = featuresTalendPipelineModel_";
  protected final String TEXT_258 = ".getPipelineModel();" + NL + "    String vectorName_";
  protected final String TEXT_259 = " = featuresParamsMap_";
  protected final String TEXT_260 = ".get(\"VECTOR_NAME\");" + NL + "" + NL + "" + NL + "     // Create context" + NL + "     org.apache.spark.sql.SQLContext sqlContext_";
  protected final String TEXT_261 = " = new org.apache.spark.sql.SQLContext(ctx.sparkContext());" + NL + "" + NL + "     // Go to RDD view in order to use dataframe" + NL + "     org.apache.spark.streaming.api.java.JavaDStream<";
  protected final String TEXT_262 = "> rdd_";
  protected final String TEXT_263 = " =" + NL + "         rdd_";
  protected final String TEXT_264 = ".transform(new GenerateVector_";
  protected final String TEXT_265 = "(vectorName_";
  protected final String TEXT_266 = ", featuresTransformationsModel_";
  protected final String TEXT_267 = ", sqlContext_";
  protected final String TEXT_268 = "))" + NL + "                 .map(new GetPrediction_";
  protected final String TEXT_269 = "(currentModel_";
  protected final String TEXT_270 = "));";
  protected final String TEXT_271 = NL + "    if ((globalMap.getLocal(\"";
  protected final String TEXT_272 = "_PIPELINE\") == null)" + NL + "            || (globalMap.getLocal(\"";
  protected final String TEXT_273 = "_MODEL\")  == null)) {" + NL + "       throw new IOException(\"Pipeline or model initialized before the component ";
  protected final String TEXT_274 = "\");" + NL + "    }" + NL;
  protected final String TEXT_275 = NL + "        TalendPipelineModel featuresTalendPipelineModel_";
  protected final String TEXT_276 = " = (TalendPipelineModel)globalMap.getLocal(\"";
  protected final String TEXT_277 = "_PIPELINE\");" + NL + "        java.util.Map<String, String> featuresParamsMap_";
  protected final String TEXT_278 = " = featuresTalendPipelineModel_";
  protected final String TEXT_279 = ".getParams();" + NL + "        String vectorName_";
  protected final String TEXT_280 = " = featuresParamsMap_";
  protected final String TEXT_281 = ".get(\"VECTOR_NAME\");" + NL + "        org.apache.spark.ml.PipelineModel featuresTransformations_";
  protected final String TEXT_282 = " = featuresTalendPipelineModel_";
  protected final String TEXT_283 = ".getPipelineModel();";
  protected final String TEXT_284 = NL + "        TalendPipeline featuresTalendPipeline_";
  protected final String TEXT_285 = " = (TalendPipeline)globalMap.getLocal(\"";
  protected final String TEXT_286 = "_PIPELINE\");" + NL + "        java.util.Map<String, String> featuresParamsMap_";
  protected final String TEXT_287 = " = featuresTalendPipeline_";
  protected final String TEXT_288 = ".getParams();" + NL + "        String vectorName_";
  protected final String TEXT_289 = " = featuresParamsMap_";
  protected final String TEXT_290 = ".get(\"VECTOR_NAME\");" + NL + "        org.apache.spark.ml.Pipeline featuresTransformations_";
  protected final String TEXT_291 = " = featuresTalendPipeline_";
  protected final String TEXT_292 = ".getPipeline();";
  protected final String TEXT_293 = NL + NL + "    Object temporaryModel_";
  protected final String TEXT_294 = " = globalMap.getLocal(\"";
  protected final String TEXT_295 = "_MODEL\");" + NL + "    if (!(temporaryModel_";
  protected final String TEXT_296 = " instanceof org.apache.spark.mllib.clustering.StreamingKMeans)) {" + NL + "        throw new RuntimeException(\"The selected model is of type \" + temporaryModel_";
  protected final String TEXT_297 = ".getClass() + \" is should be of type org.apache.spark.mllib.clustering.StreamingKMeans\");" + NL + "    }" + NL + "    org.apache.spark.mllib.clustering.StreamingKMeans strKMeans_";
  protected final String TEXT_298 = " = (org.apache.spark.mllib.clustering.StreamingKMeans) temporaryModel_";
  protected final String TEXT_299 = ";" + NL + "" + NL + "    org.apache.spark.sql.SQLContext sqlContext_";
  protected final String TEXT_300 = " = new org.apache.spark.sql.SQLContext(ctx.sparkContext());" + NL + "" + NL + "    org.apache.spark.streaming.api.java.JavaPairDStream<";
  protected final String TEXT_301 = ", ";
  protected final String TEXT_302 = "> transformedrdd_";
  protected final String TEXT_303 = " = rdd_";
  protected final String TEXT_304 = ".transformToPair(" + NL + "            new GenerateKeyVector_";
  protected final String TEXT_305 = "(vectorName_";
  protected final String TEXT_306 = ", featuresTransformations_";
  protected final String TEXT_307 = ", sqlContext_";
  protected final String TEXT_308 = "));" + NL + "" + NL + "    org.apache.spark.streaming.api.java.JavaPairDStream<";
  protected final String TEXT_309 = ", Integer> pairrdd_";
  protected final String TEXT_310 = " = strKMeans_";
  protected final String TEXT_311 = ".predictOnValues(transformedrdd_";
  protected final String TEXT_312 = ");" + NL + "    org.apache.spark.streaming.api.java.JavaDStream<";
  protected final String TEXT_313 = "> rdd_";
  protected final String TEXT_314 = " = pairrdd_";
  protected final String TEXT_315 = ".map(new GetStreamingPrediction_";
  protected final String TEXT_316 = "());";

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    
//Parse the inputs to this javajet generator.
final BigDataCodeGeneratorArgument codeGenArgument = (BigDataCodeGeneratorArgument) argument;
final INode node = (INode) codeGenArgument.getArgument();
final String cid = node.getUniqueName();

final String dataframeClass = org.talend.hadoop.distribution.ESparkVersion.SPARK_2_0.compareTo(codeGenArgument.getSparkVersion()) > 0
    ? "org.apache.spark.sql.DataFrame"
    : "org.apache.spark.sql.Dataset<org.apache.spark.sql.Row>";
final String vectorClass = "org.apache.spark.mllib.linalg.Vector";

    
final String modelType = ElementParameterParser.getValue(node, "__MODEL_TYPE__");
final String sparkVersion=ElementParameterParser.getValue((INode) ((BigDataCodeGeneratorArgument) argument).getArgument(), "__NVB_VERSION__");

// NAIVEBAYES sparkcode
if("NAIVEBAYES".equals(modelType)){
    if(sparkVersion.equals("SPARK_VERSION_1.3")){

    
TSqlRowUtil tSqlRowUtil = new TSqlRowUtil(node);
String validateError = tSqlRowUtil.validate(true, true);
if (validateError != null) {
    // Cause the job compilation to explicitly fail if there is a problem.
    return "throw new JobConfigurationError(\"" + validateError +"\");";
}

IConnection inConn = tSqlRowUtil.getIncomingConnections().get(0);
String pmmlModelPath = ElementParameterParser.getValue(node, "__PMML_MODEL_PATH__");

String inStructName = codeGenArgument.getRecordStructName(inConn);
String outStructName = codeGenArgument.getRecordStructName(tSqlRowUtil.getOutgoingConnection());
boolean useConfigurationComponent = "true".equals(ElementParameterParser.getValue(node, "__DEFINE_STORAGE_CONFIGURATION__"));
String ctx = ("SPARKSTREAMING".equals(node.getComponent().getType())) ? "ctx.sparkContext().sc()" : "ctx.sc()" ;


    stringBuffer.append(TEXT_1);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_2);
    
    if(useConfigurationComponent){//import from dfs


    stringBuffer.append(TEXT_3);
    stringBuffer.append(ctx);
    stringBuffer.append(TEXT_4);
    stringBuffer.append(pmmlModelPath);
    stringBuffer.append(TEXT_5);
    
    }else{//import from local

    stringBuffer.append(TEXT_6);
    stringBuffer.append(pmmlModelPath);
    stringBuffer.append(TEXT_7);
    
    }

    stringBuffer.append(TEXT_8);
    
if("SPARKSTREAMING".equals(node.getComponent().getType())
	&& !org.talend.designer.common.tmap.LookupUtil.isNodeInBatchMode(node)) {

    stringBuffer.append(TEXT_9);
    stringBuffer.append(outStructName);
    stringBuffer.append(TEXT_10);
    stringBuffer.append(tSqlRowUtil.getOutgoingConnection().getName());
    stringBuffer.append(TEXT_11);
    stringBuffer.append(inConn.getName());
    stringBuffer.append(TEXT_12);
    stringBuffer.append(inStructName);
    stringBuffer.append(TEXT_13);
    stringBuffer.append(outStructName);
    stringBuffer.append(TEXT_14);
    stringBuffer.append(outStructName);
    stringBuffer.append(TEXT_15);
    stringBuffer.append(inStructName);
    stringBuffer.append(TEXT_16);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_17);
    stringBuffer.append(inConn.getName());
    stringBuffer.append(TEXT_18);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_19);
    stringBuffer.append(inStructName);
    stringBuffer.append(TEXT_20);
    stringBuffer.append(inConn.getName());
    stringBuffer.append(TEXT_21);
    stringBuffer.append(inConn.getName());
    stringBuffer.append(TEXT_22);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_23);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_24);
    stringBuffer.append(outStructName);
    stringBuffer.append(TEXT_25);
    
}else{

    stringBuffer.append(TEXT_26);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_27);
    stringBuffer.append(ctx);
    stringBuffer.append(TEXT_28);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_29);
    stringBuffer.append(inConn.getName());
    stringBuffer.append(TEXT_30);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_31);
    stringBuffer.append(inConn.getName());
    stringBuffer.append(TEXT_32);
    stringBuffer.append(inStructName);
    stringBuffer.append(TEXT_33);
    stringBuffer.append(outStructName);
    stringBuffer.append(TEXT_34);
    stringBuffer.append(tSqlRowUtil.getOutgoingConnection().getName());
    stringBuffer.append(TEXT_35);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_36);
    stringBuffer.append(inConn.getName());
    stringBuffer.append(TEXT_37);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_38);
    stringBuffer.append(inConn.getName());
    stringBuffer.append(TEXT_39);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_40);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_41);
    stringBuffer.append(outStructName);
    stringBuffer.append(TEXT_42);
    
}

    
    }else{

    
boolean isLog4jEnabled = ("true").equals(ElementParameterParser.getValue(node.getProcess(), "__LOG4J_ACTIVATE__"));
String modelCid = ElementParameterParser.getValue(node, "__NVB_MODEL_LOCATION__");

    
List<IMetadataTable> metadatas = node.getMetadataList();
if ((metadatas == null) || (metadatas.size() == 0))
    return "" ;
IMetadataTable metadata = metadatas.get(0);
if (metadata == null)
    return "";
List<IMetadataColumn> columns = metadata.getListColumns();
if (columns == null)
    return "";
List<? extends IConnection> conns = node.getIncomingConnections();
if (conns == null || conns.size() == 0 || !conns.get(0).getLineStyle().hasConnectionCategory(IConnectionCategory.DATA))
    return "";
IConnection inConn = conns.get(0);
conns = node.getOutgoingConnections();
if (conns == null || conns.size() == 0 || !conns.get(0).getLineStyle().hasConnectionCategory(IConnectionCategory.DATA))
    return "";
IConnection outConn = conns.get(0);

String inRowStruct = codeGenArgument.getRecordStructName(inConn);
String inConnName = inConn.getName();

String outRowStruct = codeGenArgument.getRecordStructName(outConn);
String outConnName = outConn.getName();


// This is set to true if the output label is not double format.
boolean needsLabelIndexer = true;

Boolean savedOnDisk = ElementParameterParser.getBooleanValue(node, "__SAVE_ON_DISK__");


    stringBuffer.append(TEXT_43);
    stringBuffer.append(outRowStruct);
    stringBuffer.append(TEXT_44);
    stringBuffer.append(outConnName);
    stringBuffer.append(TEXT_45);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_46);
        
    if (savedOnDisk) {

        String modelPath = ElementParameterParser.getValue(node, "__HDFS_FOLDER__");
        boolean useConfigurationComponent = "true".equals(ElementParameterParser.getValue(node, "__DEFINE_STORAGE_CONFIGURATION__"));
        String uriPrefix = "\"\"";
        if(useConfigurationComponent) {
            uriPrefix = org.talend.designer.spark.generator.storage.SparkStorageUtils.getURIPrefix(node);
            modelPath = uriPrefix + " + " + modelPath;
        }
        if(!"\"\"".equals(uriPrefix)) {
            
    stringBuffer.append(TEXT_47);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_48);
    stringBuffer.append(uriPrefix);
    stringBuffer.append(TEXT_49);
    
        }

        
    stringBuffer.append(TEXT_50);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_51);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_52);
    stringBuffer.append(modelPath);
    stringBuffer.append(TEXT_53);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_54);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_55);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_56);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_57);
    stringBuffer.append(modelPath);
    stringBuffer.append(TEXT_58);
    
        if(!"\"\"".equals(uriPrefix)) {
             
    stringBuffer.append(TEXT_59);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_60);
    
         }
    } 
    
    stringBuffer.append(TEXT_61);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_62);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_63);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_64);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_65);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_66);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_67);
    stringBuffer.append(outConnName);
    stringBuffer.append(TEXT_68);
    stringBuffer.append(inConnName);
    stringBuffer.append(TEXT_69);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_70);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_71);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_72);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_73);
      }
} // REGRESSION sparkcode
 else if("LINEAR_REGRESSION".equals(modelType)){

    
boolean isLog4jEnabled = ("true").equals(ElementParameterParser.getValue(node.getProcess(), "__LOG4J_ACTIVATE__"));

    
List<IMetadataTable> metadatas = node.getMetadataList();
if ((metadatas == null) || (metadatas.size() == 0))
    return "" ;
IMetadataTable metadata = metadatas.get(0);
if (metadata == null)
    return "";
List<IMetadataColumn> columns = metadata.getListColumns();
if (columns == null)
    return "";
List<? extends IConnection> conns = node.getIncomingConnections();
if (conns == null || conns.size() == 0 || !conns.get(0).getLineStyle().hasConnectionCategory(IConnectionCategory.DATA))
    return "";
IConnection inConn = conns.get(0);
conns = node.getOutgoingConnections();
if (conns == null || conns.size() == 0 || !conns.get(0).getLineStyle().hasConnectionCategory(IConnectionCategory.DATA))
    return "";
IConnection outConn = conns.get(0);

String inRowStruct = codeGenArgument.getRecordStructName(inConn);
String inConnName = inConn.getName();

String outRowStruct = codeGenArgument.getRecordStructName(outConn);
String outConnName = outConn.getName();


// This is always set to true until there are model encoders that can exist in
// A spark streaming job.
Boolean savedOnDisk = true; // ElementParameterParser.getBooleanValue(node, "__SAVE_ON_DISK__");
String hdfsFolder = ElementParameterParser.getValue(node,"__HDFS_FOLDER__");
boolean useConfigurationComponent = "true".equals(ElementParameterParser.getValue(node, "__DEFINE_STORAGE_CONFIGURATION__"));

String uriPrefix = "\"\"";
if(useConfigurationComponent) {
    uriPrefix = org.talend.designer.spark.generator.storage.SparkStorageUtils.getURIPrefix(node);
    hdfsFolder = uriPrefix + " + " + hdfsFolder;
}
if(!"\"\"".equals(uriPrefix)) {
    
    stringBuffer.append(TEXT_74);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_75);
    stringBuffer.append(uriPrefix);
    stringBuffer.append(TEXT_76);
    
}

    stringBuffer.append(TEXT_77);
    stringBuffer.append(outRowStruct);
    stringBuffer.append(TEXT_78);
    stringBuffer.append(outConnName);
    stringBuffer.append(TEXT_79);
    
    // Get the pipeline model.
    if (savedOnDisk) {
        
    stringBuffer.append(TEXT_80);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_81);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_82);
    stringBuffer.append(hdfsFolder);
    stringBuffer.append(TEXT_83);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_84);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_85);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_86);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_87);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_88);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_89);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_90);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_91);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_92);
    
    } else {
        String modelCid = ElementParameterParser.getValue(node, "__MODEL_LOCATION__");
        
    stringBuffer.append(TEXT_93);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_94);
    stringBuffer.append(modelCid);
    stringBuffer.append(TEXT_95);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_96);
    
    }
    
    stringBuffer.append(TEXT_97);
    stringBuffer.append(outConnName);
    stringBuffer.append(TEXT_98);
    stringBuffer.append(inConnName);
    stringBuffer.append(TEXT_99);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_100);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_101);
    
}// LOGISTIC_REGRESSION sparkconfig
 else if("LOGISTIC_REGRESSION".equals(modelType)){

    
String modelCid = ElementParameterParser.getValue(node, "__LOR_MODEL_LOCATION__");

    
boolean isLog4jEnabled = ("true").equals(ElementParameterParser.getValue(node.getProcess(), "__LOG4J_ACTIVATE__"));

    
List<IMetadataTable> metadatas = node.getMetadataList();
if ((metadatas == null) || (metadatas.size() == 0))
    return "" ;
IMetadataTable metadata = metadatas.get(0);
if (metadata == null)
    return "";
List<IMetadataColumn> columns = metadata.getListColumns();
if (columns == null)
    return "";
List<? extends IConnection> conns = node.getIncomingConnections();
if (conns == null || conns.size() == 0 || !conns.get(0).getLineStyle().hasConnectionCategory(IConnectionCategory.DATA))
    return "";
IConnection inConn = conns.get(0);
conns = node.getOutgoingConnections();
if (conns == null || conns.size() == 0 || !conns.get(0).getLineStyle().hasConnectionCategory(IConnectionCategory.DATA))
    return "";
IConnection outConn = conns.get(0);

String inRowStruct = codeGenArgument.getRecordStructName(inConn);
String inConnName = inConn.getName();

String outRowStruct = codeGenArgument.getRecordStructName(outConn);
String outConnName = outConn.getName();


// This is set to true if the output label is not double format.
// TODO: check if the labelColumn is double and provide a unique labelColumn.
boolean needsLabelIndexer = true;
String labelColumn = "label";

// This is always set to true until there are model encoders that can exist in
// A spark streaming job.
Boolean savedOnDisk = true; // ElementParameterParser.getBooleanValue(node, "__SAVE_ON_DISK__");
String hdfsFolder = ElementParameterParser.getValue(node,"__HDFS_FOLDER__");
boolean useConfigurationComponent = "true".equals(ElementParameterParser.getValue(node, "__DEFINE_STORAGE_CONFIGURATION__"));
String uriPrefix = "\"\"";
if(useConfigurationComponent) {
    uriPrefix = org.talend.designer.spark.generator.storage.SparkStorageUtils.getURIPrefix(node);
    hdfsFolder = uriPrefix + " + " + hdfsFolder;
}


    stringBuffer.append(TEXT_102);
    stringBuffer.append(outRowStruct);
    stringBuffer.append(TEXT_103);
    stringBuffer.append(outConnName);
    stringBuffer.append(TEXT_104);
    
    // Get the pipeline model.
    if (savedOnDisk) {
        if(!"\"\"".equals(uriPrefix)) {
            
    stringBuffer.append(TEXT_105);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_106);
    stringBuffer.append(uriPrefix);
    stringBuffer.append(TEXT_107);
    
        }
        
    stringBuffer.append(TEXT_108);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_109);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_110);
    stringBuffer.append(hdfsFolder);
    stringBuffer.append(TEXT_111);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_112);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_113);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_114);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_115);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_116);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_117);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_118);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_119);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_120);
    
    } else {
        
    stringBuffer.append(TEXT_121);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_122);
    stringBuffer.append(modelCid);
    stringBuffer.append(TEXT_123);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_124);
    
    }
    
    stringBuffer.append(TEXT_125);
    stringBuffer.append(outConnName);
    stringBuffer.append(TEXT_126);
    stringBuffer.append(inConnName);
    stringBuffer.append(TEXT_127);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_128);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_129);
    

    
}// RANDOM_FOREST sparkconfig
 else if("RANDOM_FOREST".equals(modelType)){

    
String modelCid = ElementParameterParser.getValue(node, "__RF_MODEL_LOCATION__");

    
boolean isLog4jEnabled = ("true").equals(ElementParameterParser.getValue(node.getProcess(), "__LOG4J_ACTIVATE__"));

    
List<IMetadataTable> metadatas = node.getMetadataList();
if ((metadatas == null) || (metadatas.size() == 0))
    return "" ;
IMetadataTable metadata = metadatas.get(0);
if (metadata == null)
    return "";
List<IMetadataColumn> columns = metadata.getListColumns();
if (columns == null)
    return "";
List<? extends IConnection> conns = node.getIncomingConnections();
if (conns == null || conns.size() == 0 || !conns.get(0).getLineStyle().hasConnectionCategory(IConnectionCategory.DATA))
    return "";
IConnection inConn = conns.get(0);
conns = node.getOutgoingConnections();
if (conns == null || conns.size() == 0 || !conns.get(0).getLineStyle().hasConnectionCategory(IConnectionCategory.DATA))
    return "";
IConnection outConn = conns.get(0);

String inRowStruct = codeGenArgument.getRecordStructName(inConn);
String inConnName = inConn.getName();

String outRowStruct = codeGenArgument.getRecordStructName(outConn);
String outConnName = outConn.getName();


// This is set to true if the output label is not double format.
// TODO: check if the labelColumn is double and provide a unique labelColumn.
boolean needsLabelIndexer = true;
String labelColumn = "label";

// This is always set to true until there are model encoders that can exist in
// A spark streaming job.
Boolean savedOnDisk = true; // ElementParameterParser.getBooleanValue(node, "__SAVE_ON_DISK__");
String hdfsFolder = ElementParameterParser.getValue(node,"__HDFS_FOLDER__");
boolean useConfigurationComponent = "true".equals(ElementParameterParser.getValue(node, "__DEFINE_STORAGE_CONFIGURATION__"));
String uriPrefix = "\"\"";
if(useConfigurationComponent) {
    uriPrefix = org.talend.designer.spark.generator.storage.SparkStorageUtils.getURIPrefix(node);
    hdfsFolder = uriPrefix + " + " + hdfsFolder;
}


    stringBuffer.append(TEXT_130);
    stringBuffer.append(outRowStruct);
    stringBuffer.append(TEXT_131);
    stringBuffer.append(outConnName);
    stringBuffer.append(TEXT_132);
    
    // Get the pipeline model.
    if (savedOnDisk) {
        if(!"\"\"".equals(uriPrefix)) {
            
    stringBuffer.append(TEXT_133);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_134);
    stringBuffer.append(uriPrefix);
    stringBuffer.append(TEXT_135);
    
        }
        
    stringBuffer.append(TEXT_136);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_137);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_138);
    stringBuffer.append(hdfsFolder);
    stringBuffer.append(TEXT_139);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_140);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_141);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_142);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_143);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_144);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_145);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_146);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_147);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_148);
    
    } else {
        
    stringBuffer.append(TEXT_149);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_150);
    stringBuffer.append(modelCid);
    stringBuffer.append(TEXT_151);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_152);
    
    }
    
    stringBuffer.append(TEXT_153);
    stringBuffer.append(outConnName);
    stringBuffer.append(TEXT_154);
    stringBuffer.append(inConnName);
    stringBuffer.append(TEXT_155);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_156);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_157);
    

    
}// DECISION_TREE sparkconfig
 else if("DECISION_TREE".equals(modelType)){

    
String modelCid = ElementParameterParser.getValue(node, "__DT_MODEL_LOCATION__");

    
boolean isLog4jEnabled = ("true").equals(ElementParameterParser.getValue(node.getProcess(), "__LOG4J_ACTIVATE__"));

    
List<IMetadataTable> metadatas = node.getMetadataList();
if ((metadatas == null) || (metadatas.size() == 0))
    return "" ;
IMetadataTable metadata = metadatas.get(0);
if (metadata == null)
    return "";
List<IMetadataColumn> columns = metadata.getListColumns();
if (columns == null)
    return "";
List<? extends IConnection> conns = node.getIncomingConnections();
if (conns == null || conns.size() == 0 || !conns.get(0).getLineStyle().hasConnectionCategory(IConnectionCategory.DATA))
    return "";
IConnection inConn = conns.get(0);
conns = node.getOutgoingConnections();
if (conns == null || conns.size() == 0 || !conns.get(0).getLineStyle().hasConnectionCategory(IConnectionCategory.DATA))
    return "";
IConnection outConn = conns.get(0);

String inRowStruct = codeGenArgument.getRecordStructName(inConn);
String inConnName = inConn.getName();

String outRowStruct = codeGenArgument.getRecordStructName(outConn);
String outConnName = outConn.getName();


// This is set to true if the output label is not double format.
// TODO: check if the labelColumn is double and provide a unique labelColumn.
boolean needsLabelIndexer = true;
String labelColumn = "label";

// This is always set to true until there are model encoders that can exist in
// A spark streaming job.
Boolean savedOnDisk = true; // ElementParameterParser.getBooleanValue(node, "__SAVE_ON_DISK__");
String hdfsFolder = ElementParameterParser.getValue(node,"__HDFS_FOLDER__");
boolean useConfigurationComponent = "true".equals(ElementParameterParser.getValue(node, "__DEFINE_STORAGE_CONFIGURATION__"));
String uriPrefix = "\"\"";
if(useConfigurationComponent) {
    uriPrefix = org.talend.designer.spark.generator.storage.SparkStorageUtils.getURIPrefix(node);
    hdfsFolder = uriPrefix + " + " + hdfsFolder;
}


    stringBuffer.append(TEXT_158);
    stringBuffer.append(outRowStruct);
    stringBuffer.append(TEXT_159);
    stringBuffer.append(outConnName);
    stringBuffer.append(TEXT_160);
    
    // Get the pipeline model.
    if (savedOnDisk) {
        if(!"\"\"".equals(uriPrefix)) {
            
    stringBuffer.append(TEXT_161);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_162);
    stringBuffer.append(uriPrefix);
    stringBuffer.append(TEXT_163);
    
        }
        
    stringBuffer.append(TEXT_164);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_165);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_166);
    stringBuffer.append(hdfsFolder);
    stringBuffer.append(TEXT_167);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_168);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_169);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_170);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_171);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_172);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_173);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_174);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_175);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_176);
    
    } else {
        
    stringBuffer.append(TEXT_177);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_178);
    stringBuffer.append(modelCid);
    stringBuffer.append(TEXT_179);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_180);
    
    }
    
    stringBuffer.append(TEXT_181);
    stringBuffer.append(outConnName);
    stringBuffer.append(TEXT_182);
    stringBuffer.append(inConnName);
    stringBuffer.append(TEXT_183);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_184);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_185);
    

    
}// GRADIENT_BOOSTED sparkconfig
 else if("GRADIENT_BOOSTED".equals(modelType)){

    
String modelCid = ElementParameterParser.getValue(node, "__GB_MODEL_LOCATION__");

    
boolean isLog4jEnabled = ("true").equals(ElementParameterParser.getValue(node.getProcess(), "__LOG4J_ACTIVATE__"));

    
List<IMetadataTable> metadatas = node.getMetadataList();
if ((metadatas == null) || (metadatas.size() == 0))
    return "" ;
IMetadataTable metadata = metadatas.get(0);
if (metadata == null)
    return "";
List<IMetadataColumn> columns = metadata.getListColumns();
if (columns == null)
    return "";
List<? extends IConnection> conns = node.getIncomingConnections();
if (conns == null || conns.size() == 0 || !conns.get(0).getLineStyle().hasConnectionCategory(IConnectionCategory.DATA))
    return "";
IConnection inConn = conns.get(0);
conns = node.getOutgoingConnections();
if (conns == null || conns.size() == 0 || !conns.get(0).getLineStyle().hasConnectionCategory(IConnectionCategory.DATA))
    return "";
IConnection outConn = conns.get(0);

String inRowStruct = codeGenArgument.getRecordStructName(inConn);
String inConnName = inConn.getName();

String outRowStruct = codeGenArgument.getRecordStructName(outConn);
String outConnName = outConn.getName();


// This is set to true if the output label is not double format.
// TODO: check if the labelColumn is double and provide a unique labelColumn.
boolean needsLabelIndexer = true;
String labelColumn = "label";

// This is always set to true until there are model encoders that can exist in
// A spark streaming job.
Boolean savedOnDisk = true; // ElementParameterParser.getBooleanValue(node, "__SAVE_ON_DISK__");
String hdfsFolder = ElementParameterParser.getValue(node,"__HDFS_FOLDER__");
boolean useConfigurationComponent = "true".equals(ElementParameterParser.getValue(node, "__DEFINE_STORAGE_CONFIGURATION__"));
String uriPrefix = "\"\"";
if(useConfigurationComponent) {
    uriPrefix = org.talend.designer.spark.generator.storage.SparkStorageUtils.getURIPrefix(node);
    hdfsFolder = uriPrefix + " + " + hdfsFolder;
}


    stringBuffer.append(TEXT_186);
    stringBuffer.append(outRowStruct);
    stringBuffer.append(TEXT_187);
    stringBuffer.append(outConnName);
    stringBuffer.append(TEXT_188);
    
    // Get the pipeline model.
    if (savedOnDisk) {
        if(!"\"\"".equals(uriPrefix)) {
            
    stringBuffer.append(TEXT_189);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_190);
    stringBuffer.append(uriPrefix);
    stringBuffer.append(TEXT_191);
    
        }
        
    stringBuffer.append(TEXT_192);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_193);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_194);
    stringBuffer.append(hdfsFolder);
    stringBuffer.append(TEXT_195);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_196);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_197);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_198);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_199);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_200);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_201);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_202);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_203);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_204);
    
    } else {
        
    stringBuffer.append(TEXT_205);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_206);
    stringBuffer.append(modelCid);
    stringBuffer.append(TEXT_207);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_208);
    
    }
    
    stringBuffer.append(TEXT_209);
    stringBuffer.append(outConnName);
    stringBuffer.append(TEXT_210);
    stringBuffer.append(inConnName);
    stringBuffer.append(TEXT_211);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_212);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_213);
    

    
}
// SVM_CLASSIFICATION sparkconfig
 else if("SVM_CLASSIFICATION".equals(modelType)){

    stringBuffer.append(TEXT_214);
    
boolean isLog4jEnabled = ("true").equals(ElementParameterParser.getValue(node.getProcess(), "__LOG4J_ACTIVATE__"));

    
IConnection conn = null;
List<? extends IConnection> conns = node.getIncomingConnections();
if(conns != null && conns.size() > 0 && conns.get(0).getLineStyle().hasConnectionCategory(IConnectionCategory.DATA)){
    conn = conns.get(0);
}

List<? extends IConnection> outConns = node.getOutgoingConnections();
IConnection outConn = null;
if(outConns != null && outConns.size() > 0 && outConns.get(0).getLineStyle().hasConnectionCategory(IConnectionCategory.DATA)){
    outConn = outConns.get(0);
}

if(conn == null || outConn == null){
    return "";
}

String inRowStruct = codeGenArgument.getRecordStructName(conn);
String inConnName = conn.getName();

String outRowStruct = codeGenArgument.getRecordStructName(outConn);
String outConnName = outConn.getName();


String modelPath = ElementParameterParser.getValue(node, "__HDFS_FOLDER__");
boolean useConfigurationComponent = "true".equals(ElementParameterParser.getValue(node, "__DEFINE_STORAGE_CONFIGURATION__"));
String uriPrefix = "\"\"";
if(useConfigurationComponent) {
        uriPrefix = org.talend.designer.spark.generator.storage.SparkStorageUtils.getURIPrefix(node);
        modelPath = uriPrefix + " + " + modelPath;
}

if(!"\"\"".equals(uriPrefix)) {
    
    stringBuffer.append(TEXT_215);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_216);
    stringBuffer.append(uriPrefix);
    stringBuffer.append(TEXT_217);
    
}

// We are on SVM

    stringBuffer.append(TEXT_218);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_219);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_220);
    stringBuffer.append(modelPath);
    stringBuffer.append(TEXT_221);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_222);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_223);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_224);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_225);
    stringBuffer.append(modelPath);
    stringBuffer.append(TEXT_226);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_227);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_228);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_229);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_230);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_231);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_232);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_233);
    stringBuffer.append(outRowStruct);
    stringBuffer.append(TEXT_234);
    stringBuffer.append(outConnName);
    stringBuffer.append(TEXT_235);
    stringBuffer.append(inConnName);
    stringBuffer.append(TEXT_236);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_237);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_238);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_239);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_240);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_241);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_242);
    
}// KMEANS sparkconfig
 else if("KMEANS".equals(modelType)){

    
boolean isLog4jEnabled = ("true").equals(ElementParameterParser.getValue(node.getProcess(), "__LOG4J_ACTIVATE__"));

    
IConnection conn = null;
List<? extends IConnection> conns = node.getIncomingConnections();
if(conns != null && conns.size() > 0 && conns.get(0).getLineStyle().hasConnectionCategory(IConnectionCategory.DATA)){
    conn = conns.get(0);
}

List<? extends IConnection> outConns = node.getOutgoingConnections();
IConnection outConn = null;
if(outConns != null && outConns.size() > 0 && outConns.get(0).getLineStyle().hasConnectionCategory(IConnectionCategory.DATA)){
    outConn = outConns.get(0);
}

if(conn == null || outConn == null){
    return "";
}

String inRowStruct = codeGenArgument.getRecordStructName(conn);
String inConnName = conn.getName();

String outRowStruct = codeGenArgument.getRecordStructName(outConn);
String outConnName = outConn.getName();

Boolean modelOnFilesystem = ElementParameterParser.getBooleanValue(node, "__SAVE_ON_DISK__");
Boolean modelComputed = ElementParameterParser.getBooleanValue(node, "__KMEANS_MODEL_COMPUTED__");

String modelPath = ElementParameterParser.getValue(node, "__HDFS_FOLDER__");
boolean useConfigurationComponent = "true".equals(ElementParameterParser.getValue(node, "__DEFINE_STORAGE_CONFIGURATION__"));
String uriPrefix = "\"\"";
if(useConfigurationComponent) {
    uriPrefix = org.talend.designer.spark.generator.storage.SparkStorageUtils.getURIPrefix(node);
    modelPath = uriPrefix + " + " + modelPath;
}

if(!"\"\"".equals(uriPrefix)) {
    
    stringBuffer.append(TEXT_243);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_244);
    stringBuffer.append(uriPrefix);
    stringBuffer.append(TEXT_245);
    
}

if (modelOnFilesystem) {
    // We are on Kmeans (or a Kmeans streaming dumped into a Kmeans format)
    
    stringBuffer.append(TEXT_246);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_247);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_248);
    stringBuffer.append(modelPath);
    stringBuffer.append(TEXT_249);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_250);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_251);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_252);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_253);
    stringBuffer.append(modelPath);
    stringBuffer.append(TEXT_254);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_255);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_256);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_257);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_258);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_259);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_260);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_261);
    stringBuffer.append(outRowStruct);
    stringBuffer.append(TEXT_262);
    stringBuffer.append(outConnName);
    stringBuffer.append(TEXT_263);
    stringBuffer.append(inConnName);
    stringBuffer.append(TEXT_264);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_265);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_266);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_267);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_268);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_269);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_270);
    
} else if (modelComputed) {
    // We are on Kmeans streamimng
    String modelLocation = ElementParameterParser.getValue(node, "__KMEANS_MODEL_LOCATION__");

    Boolean inputAsPipelineModel = false;
    List<? extends INode> nodes = node.getProcess().getGeneratingNodes();
    for(INode targetNode : nodes){
        if (targetNode.getUniqueName().equals(modelLocation)) {
            // If the tKmeansStrModel load the pipeline from disk, it will save a PipelineModel.
            // Otherwise (the default case), it will be a Pipeline.
            // This is because this pipeline was read from HDFS and can be a complexe pipeline computed with a batch KMeans
            inputAsPipelineModel = ElementParameterParser.getBooleanValue(targetNode, "__REUSE_PIPELINE__")
                    && ElementParameterParser.getBooleanValue(targetNode, "__LOAD_FROM_DISK__");
            break;
        }
    }

    // retrieve name of the external component
    
    stringBuffer.append(TEXT_271);
    stringBuffer.append(modelLocation);
    stringBuffer.append(TEXT_272);
    stringBuffer.append(modelLocation);
    stringBuffer.append(TEXT_273);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_274);
    
    if (inputAsPipelineModel) {
        
    stringBuffer.append(TEXT_275);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_276);
    stringBuffer.append(modelLocation);
    stringBuffer.append(TEXT_277);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_278);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_279);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_280);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_281);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_282);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_283);
    
    } else {
        
    stringBuffer.append(TEXT_284);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_285);
    stringBuffer.append(modelLocation);
    stringBuffer.append(TEXT_286);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_287);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_288);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_289);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_290);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_291);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_292);
    
    }
    
    stringBuffer.append(TEXT_293);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_294);
    stringBuffer.append(modelLocation);
    stringBuffer.append(TEXT_295);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_296);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_297);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_298);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_299);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_300);
    stringBuffer.append(inRowStruct);
    stringBuffer.append(TEXT_301);
    stringBuffer.append(vectorClass);
    stringBuffer.append(TEXT_302);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_303);
    stringBuffer.append(inConnName);
    stringBuffer.append(TEXT_304);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_305);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_306);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_307);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_308);
    stringBuffer.append(inRowStruct);
    stringBuffer.append(TEXT_309);
    stringBuffer.append(outConnName);
    stringBuffer.append(TEXT_310);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_311);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_312);
    stringBuffer.append(outRowStruct);
    stringBuffer.append(TEXT_313);
    stringBuffer.append(outConnName);
    stringBuffer.append(TEXT_314);
    stringBuffer.append(outConnName);
    stringBuffer.append(TEXT_315);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_316);
    
}

    
}

    return stringBuffer.toString();
  }
}
