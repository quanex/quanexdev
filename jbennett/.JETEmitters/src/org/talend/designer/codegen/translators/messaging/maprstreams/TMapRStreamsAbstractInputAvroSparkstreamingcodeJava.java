package org.talend.designer.codegen.translators.messaging.maprstreams;

import java.util.List;
import java.util.Map;
import org.talend.core.model.metadata.IMetadataColumn;
import org.talend.core.model.metadata.IMetadataTable;
import org.talend.core.model.metadata.types.JavaType;
import org.talend.core.model.metadata.types.JavaTypesManager;
import org.talend.core.model.process.ElementParameterParser;
import org.talend.core.model.process.IConnection;
import org.talend.core.model.process.IConnectionCategory;
import org.talend.core.model.process.INode;
import org.talend.designer.common.BigDataCodeGeneratorArgument;
import org.talend.designer.common.tmaprstreamsinput.TMapRStreamsInputUtil;
import org.talend.hadoop.distribution.kafka.SparkStreamingKafkaVersion;

public class TMapRStreamsAbstractInputAvroSparkstreamingcodeJava
{
  protected static String nl;
  public static synchronized TMapRStreamsAbstractInputAvroSparkstreamingcodeJava create(String lineSeparator)
  {
    nl = lineSeparator;
    TMapRStreamsAbstractInputAvroSparkstreamingcodeJava result = new TMapRStreamsAbstractInputAvroSparkstreamingcodeJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "\t";
  protected final String TEXT_2 = NL + NL + "//Use this object to override the ouput struct default ";
  protected final String TEXT_3 = NL + "//public static class ";
  protected final String TEXT_4 = " extends org.apache.hadoop.io.ObjectWritable {" + NL + "//}" + NL + "" + NL + "public static class ";
  protected final String TEXT_5 = "_ValueDeserializer implements org.apache.kafka.common.serialization.Deserializer<";
  protected final String TEXT_6 = "> {" + NL + "" + NL + "\tprivate org.apache.avro.io.DatumReader<org.apache.avro.generic.GenericRecord> datumReader;" + NL + "\t\t" + NL + "\tprivate org.apache.avro.io.BinaryDecoder decoder;" + NL + "\t";
  protected final String TEXT_7 = NL + "\t    private String avroSchema;";
  protected final String TEXT_8 = NL + "\t\t" + NL + "\tpublic ";
  protected final String TEXT_9 = "_ValueDeserializer() {" + NL + "\t\t// empty" + NL + "\t}" + NL + "" + NL + "\tpublic void configure(java.util.Map<java.lang.String,?> configs, boolean isKey) {";
  protected final String TEXT_10 = NL + "        avroSchema = (String) configs.get(\"talend.avro.schema\");" + NL + "        org.apache.avro.Schema schema = new org.apache.avro.Schema.Parser().parse(avroSchema);";
  protected final String TEXT_11 = NL + "        org.apache.avro.Schema schema = createSchema();";
  protected final String TEXT_12 = NL + "\t\tdatumReader = new org.apache.avro.generic.GenericDatumReader<org.apache.avro.generic.GenericRecord>(schema);" + NL + "\t}" + NL + "" + NL + "\tpublic ";
  protected final String TEXT_13 = " deserialize(String topic, byte[] data) {" + NL + "\t\t";
  protected final String TEXT_14 = " result = new ";
  protected final String TEXT_15 = "();" + NL + "\t\tdecoder = org.apache.avro.io.DecoderFactory.get().binaryDecoder(data, decoder);" + NL + "\t\ttry {" + NL + "\t\t\tresult.set(datumReader.read(null, decoder));" + NL + "\t\t} catch(java.io.IOException e) {" + NL + "\t\t\t// TODO : how to handle this ?" + NL + "\t\t}" + NL + "\t\treturn result;" + NL + "    }" + NL + "" + NL + "\tpublic void close() {" + NL + "\t\t// nothing" + NL + "\t}" + NL + "\t";
  protected final String TEXT_16 = NL + "\t\tprivate org.apache.avro.Schema createSchema() {" + NL + "\t\t\t//set schema" + NL + "\t\t\tList<org.apache.avro.Schema.Field> fields = new java.util.ArrayList<org.apache.avro.Schema.Field>();" + NL + "\t\t\tList<org.apache.avro.Schema> unionSchema = null;" + NL;
  protected final String TEXT_17 = NL + "             \tfields.add(new org.apache.avro.Schema.Field(\"";
  protected final String TEXT_18 = "\",org.apache.avro.Schema.create(org.apache.avro.Schema.Type.";
  protected final String TEXT_19 = "),null,null));";
  protected final String TEXT_20 = NL + "\t             unionSchema = new java.util.ArrayList<org.apache.avro.Schema>();" + NL + "\t             unionSchema.add(org.apache.avro.Schema.create(org.apache.avro.Schema.Type.";
  protected final String TEXT_21 = "));" + NL + "\t             unionSchema.add(org.apache.avro.Schema.create(org.apache.avro.Schema.Type.NULL));" + NL + "\t             fields.add(new org.apache.avro.Schema.Field(\"";
  protected final String TEXT_22 = "\",org.apache.avro.Schema.createUnion(unionSchema),null,null));     ";
  protected final String TEXT_23 = NL + "         return org.apache.avro.Schema.createRecord(fields);" + NL + "     }";
  protected final String TEXT_24 = NL + "}" + NL + "" + NL + "public static class ";
  protected final String TEXT_25 = "_KeyDeserializer implements org.apache.kafka.common.serialization.Deserializer<org.apache.hadoop.io.NullWritable> {" + NL + "" + NL + "\tpublic ";
  protected final String TEXT_26 = "_KeyDeserializer() {" + NL + "\t\t// empty" + NL + "\t}" + NL + "\t" + NL + "\tpublic void configure(java.util.Map<java.lang.String,?> configs, boolean isKey) {" + NL + "\t\t// nothing" + NL + "\t}" + NL + "" + NL + "\tpublic org.apache.hadoop.io.NullWritable deserialize(String topic, byte[] data) {" + NL + "\t    return org.apache.hadoop.io.NullWritable.get();" + NL + "\t}" + NL + "" + NL + "\tpublic void close() {" + NL + "\t\t// nothing" + NL + "\t}" + NL + "}";
  protected final String TEXT_27 = NL + "\t";
  protected final String TEXT_28 = NL + NL + "//Use this object to override the ouput struct default ";
  protected final String TEXT_29 = NL + "//public static class ";
  protected final String TEXT_30 = " extends org.apache.hadoop.io.ObjectWritable {" + NL + "//}" + NL + "" + NL + "public static class ";
  protected final String TEXT_31 = "_ValueDeserializer implements org.apache.kafka.common.serialization.Deserializer<";
  protected final String TEXT_32 = "> {" + NL + "" + NL + "\tprivate org.apache.avro.io.DatumReader<org.apache.avro.generic.GenericRecord> datumReader;" + NL + "\t\t" + NL + "\tprivate org.apache.avro.io.BinaryDecoder decoder;" + NL + "\t";
  protected final String TEXT_33 = NL + "\t    private String avroSchema;";
  protected final String TEXT_34 = NL + "\t\t" + NL + "\tpublic ";
  protected final String TEXT_35 = "_ValueDeserializer() {" + NL + "\t\t// empty" + NL + "\t}" + NL + "" + NL + "\tpublic void configure(java.util.Map<java.lang.String,?> configs, boolean isKey) {";
  protected final String TEXT_36 = NL + "        avroSchema = (String) configs.get(\"talend.avro.schema\");" + NL + "        org.apache.avro.Schema schema = new org.apache.avro.Schema.Parser().parse(avroSchema);";
  protected final String TEXT_37 = NL + "        org.apache.avro.Schema schema = createSchema();";
  protected final String TEXT_38 = NL + "\t\tdatumReader = new org.apache.avro.generic.GenericDatumReader<org.apache.avro.generic.GenericRecord>(schema);" + NL + "\t}" + NL + "" + NL + "\tpublic ";
  protected final String TEXT_39 = " deserialize(String topic, byte[] data) {" + NL + "\t\t";
  protected final String TEXT_40 = " result = new ";
  protected final String TEXT_41 = "();" + NL + "\t\tdecoder = org.apache.avro.io.DecoderFactory.get().binaryDecoder(data, decoder);" + NL + "\t\ttry {" + NL + "\t\t\tresult.set(datumReader.read(null, decoder));" + NL + "\t\t} catch(java.io.IOException e) {" + NL + "\t\t\t// TODO : how to handle this ?" + NL + "\t\t}" + NL + "\t\treturn result;" + NL + "    }" + NL + "" + NL + "\tpublic void close() {" + NL + "\t\t// nothing" + NL + "\t}" + NL + "\t";
  protected final String TEXT_42 = NL + "\t\tprivate org.apache.avro.Schema createSchema() {" + NL + "\t\t\t//set schema" + NL + "\t\t\tList<org.apache.avro.Schema.Field> fields = new java.util.ArrayList<org.apache.avro.Schema.Field>();" + NL + "\t\t\tList<org.apache.avro.Schema> unionSchema = null;" + NL;
  protected final String TEXT_43 = NL + "             \tfields.add(new org.apache.avro.Schema.Field(\"";
  protected final String TEXT_44 = "\",org.apache.avro.Schema.create(org.apache.avro.Schema.Type.";
  protected final String TEXT_45 = "),null,null));";
  protected final String TEXT_46 = NL + "\t             unionSchema = new java.util.ArrayList<org.apache.avro.Schema>();" + NL + "\t             unionSchema.add(org.apache.avro.Schema.create(org.apache.avro.Schema.Type.";
  protected final String TEXT_47 = "));" + NL + "\t             unionSchema.add(org.apache.avro.Schema.create(org.apache.avro.Schema.Type.NULL));" + NL + "\t             fields.add(new org.apache.avro.Schema.Field(\"";
  protected final String TEXT_48 = "\",org.apache.avro.Schema.createUnion(unionSchema),null,null));     ";
  protected final String TEXT_49 = NL + "         return org.apache.avro.Schema.createRecord(fields);" + NL + "     }";
  protected final String TEXT_50 = NL + "}" + NL + "" + NL + "public static class ";
  protected final String TEXT_51 = "_KeyDeserializer implements org.apache.kafka.common.serialization.Deserializer<org.apache.hadoop.io.NullWritable> {" + NL + "" + NL + "\tpublic ";
  protected final String TEXT_52 = "_KeyDeserializer() {" + NL + "\t\t// empty" + NL + "\t}" + NL + "\t" + NL + "\tpublic void configure(java.util.Map<java.lang.String,?> configs, boolean isKey) {" + NL + "\t\t// nothing" + NL + "\t}" + NL + "" + NL + "\tpublic org.apache.hadoop.io.NullWritable deserialize(String topic, byte[] data) {" + NL + "\t    return org.apache.hadoop.io.NullWritable.get();" + NL + "\t}" + NL + "" + NL + "\tpublic void close() {" + NL + "\t\t// nothing" + NL + "\t}" + NL + "}" + NL + "" + NL + "public static class ";
  protected final String TEXT_53 = "_Function implements org.apache.spark.api.java.function.PairFunction<org.apache.kafka.clients.consumer.ConsumerRecord<NullWritable, ";
  protected final String TEXT_54 = ">, NullWritable, ";
  protected final String TEXT_55 = "> {" + NL + "" + NL + "\tpublic ";
  protected final String TEXT_56 = "_Function() {" + NL + "\t\t// empty" + NL + "\t}" + NL + "\t" + NL + "\t@Override" + NL + "\tpublic scala.Tuple2<NullWritable, ";
  protected final String TEXT_57 = "> call(org.apache.kafka.clients.consumer.ConsumerRecord<NullWritable, ";
  protected final String TEXT_58 = "> record) {" + NL + "\t\treturn new scala.Tuple2(record.key(), record.value());" + NL + "\t}" + NL + "" + NL + "}";
  protected final String TEXT_59 = NL;
  protected final String TEXT_60 = NL;

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    
BigDataCodeGeneratorArgument codeGenArgument = (BigDataCodeGeneratorArgument) argument;
INode node = (INode)codeGenArgument.getArgument();
String cid = node.getUniqueName();

TMapRStreamsInputUtil tMapRStreamsInputUtil = new TMapRStreamsInputUtil(node);
String connectionNameStruct = tMapRStreamsInputUtil.getOutgoingConnection().getName()+"Struct";
boolean useHierarchical = "true".equals(ElementParameterParser.getValue(node, "__USE_HIERARCHICAL__"));

String avroRecordStruct = "org.apache.hadoop.io.ObjectWritable";
codeGenArgument.getRecordStructGenerator().reserveRecordStructName(tMapRStreamsInputUtil.getOutgoingConnection(), avroRecordStruct);

SparkStreamingKafkaVersion sparkStreamingKafkaVersion = tMapRStreamsInputUtil.getSparkStreamingKafkaVersion();

    
if (SparkStreamingKafkaVersion.MAPR_5X0_KAFKA.equals(sparkStreamingKafkaVersion)) {
	// Special case only for some MapR distributions 

    stringBuffer.append(TEXT_1);
    stringBuffer.append(TEXT_2);
    stringBuffer.append(connectionNameStruct);
    stringBuffer.append(TEXT_3);
    stringBuffer.append(connectionNameStruct);
    stringBuffer.append(TEXT_4);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_5);
    stringBuffer.append(avroRecordStruct);
    stringBuffer.append(TEXT_6);
    
	if (useHierarchical) {

    stringBuffer.append(TEXT_7);
    
	}

    stringBuffer.append(TEXT_8);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_9);
    
		if (useHierarchical) {

    stringBuffer.append(TEXT_10);
    
		} else {

    stringBuffer.append(TEXT_11);
    
		}

    stringBuffer.append(TEXT_12);
    stringBuffer.append(avroRecordStruct);
    stringBuffer.append(TEXT_13);
    stringBuffer.append(avroRecordStruct);
    stringBuffer.append(TEXT_14);
    stringBuffer.append(avroRecordStruct);
    stringBuffer.append(TEXT_15);
    
 	if (!useHierarchical) {

    stringBuffer.append(TEXT_16);
    
         java.util.Map<JavaType,String> talendTypeToAvroType = tMapRStreamsInputUtil.getTalendTypesToAvroTypes();
         
         List<IMetadataColumn> columns = node.getMetadataList().get(0).getListColumns();
         int nbColumns = columns.size();
         
         for (int i = 0; i < nbColumns; i++ ){
             IMetadataColumn column = columns.get(i);
             JavaType javaType = JavaTypesManager.getJavaTypeFromId(column.getTalendType());
             String columnName = column.getLabel();  
             boolean isPrimitive = JavaTypesManager.isJavaPrimitiveType(javaType, column.isNullable());
             
             if(isPrimitive) {

    stringBuffer.append(TEXT_17);
    stringBuffer.append(columnName);
    stringBuffer.append(TEXT_18);
    stringBuffer.append(talendTypeToAvroType.get(javaType));
    stringBuffer.append(TEXT_19);
    
             } else {

    stringBuffer.append(TEXT_20);
    stringBuffer.append(talendTypeToAvroType.get(javaType));
    stringBuffer.append(TEXT_21);
    stringBuffer.append(columnName);
    stringBuffer.append(TEXT_22);
    
          	}
         } // end for

    stringBuffer.append(TEXT_23);
    
	} // end (!useHierarchical)

    stringBuffer.append(TEXT_24);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_25);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_26);
    
} else if (SparkStreamingKafkaVersion.MAPR_600_KAFKA.equals(sparkStreamingKafkaVersion)) {

    stringBuffer.append(TEXT_27);
    stringBuffer.append(TEXT_28);
    stringBuffer.append(connectionNameStruct);
    stringBuffer.append(TEXT_29);
    stringBuffer.append(connectionNameStruct);
    stringBuffer.append(TEXT_30);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_31);
    stringBuffer.append(avroRecordStruct);
    stringBuffer.append(TEXT_32);
    
	if (useHierarchical) {

    stringBuffer.append(TEXT_33);
    
	}

    stringBuffer.append(TEXT_34);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_35);
    
		if (useHierarchical) {

    stringBuffer.append(TEXT_36);
    
		} else {

    stringBuffer.append(TEXT_37);
    
		}

    stringBuffer.append(TEXT_38);
    stringBuffer.append(avroRecordStruct);
    stringBuffer.append(TEXT_39);
    stringBuffer.append(avroRecordStruct);
    stringBuffer.append(TEXT_40);
    stringBuffer.append(avroRecordStruct);
    stringBuffer.append(TEXT_41);
    
 	if (!useHierarchical) {

    stringBuffer.append(TEXT_42);
    
         java.util.Map<JavaType,String> talendTypeToAvroType = tMapRStreamsInputUtil.getTalendTypesToAvroTypes();
         
         List<IMetadataColumn> columns = node.getMetadataList().get(0).getListColumns();
         int nbColumns = columns.size();
         
         for (int i = 0; i < nbColumns; i++ ){
             IMetadataColumn column = columns.get(i);
             JavaType javaType = JavaTypesManager.getJavaTypeFromId(column.getTalendType());
             String columnName = column.getLabel();  
             boolean isPrimitive = JavaTypesManager.isJavaPrimitiveType(javaType, column.isNullable());
             
             if(isPrimitive) {

    stringBuffer.append(TEXT_43);
    stringBuffer.append(columnName);
    stringBuffer.append(TEXT_44);
    stringBuffer.append(talendTypeToAvroType.get(javaType));
    stringBuffer.append(TEXT_45);
    
             } else {

    stringBuffer.append(TEXT_46);
    stringBuffer.append(talendTypeToAvroType.get(javaType));
    stringBuffer.append(TEXT_47);
    stringBuffer.append(columnName);
    stringBuffer.append(TEXT_48);
    
          	}
         } // end for

    stringBuffer.append(TEXT_49);
    
	} // end (!useHierarchical)

    stringBuffer.append(TEXT_50);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_51);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_52);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_53);
    stringBuffer.append(avroRecordStruct);
    stringBuffer.append(TEXT_54);
    stringBuffer.append(avroRecordStruct);
    stringBuffer.append(TEXT_55);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_56);
    stringBuffer.append(avroRecordStruct);
    stringBuffer.append(TEXT_57);
    stringBuffer.append(avroRecordStruct);
    stringBuffer.append(TEXT_58);
    
} 

    stringBuffer.append(TEXT_59);
    stringBuffer.append(TEXT_60);
    return stringBuffer.toString();
  }
}
