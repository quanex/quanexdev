package org.talend.designer.codegen.translators.machinelearning.classification;

import org.talend.core.model.metadata.IMetadataColumn;
import org.talend.core.model.metadata.IMetadataTable;
import org.talend.core.model.metadata.types.JavaType;
import org.talend.core.model.metadata.types.JavaTypesManager;
import org.talend.core.model.process.ElementParameterParser;
import org.talend.core.model.process.EConnectionType;
import org.talend.core.model.process.IConnection;
import org.talend.core.model.process.IConnectionCategory;
import org.talend.core.model.process.INode;
import org.talend.core.model.process.IBigDataNode;
import org.talend.designer.common.BigDataCodeGeneratorArgument;

public class TNaiveBayesModelSparkcodeJava
{
  protected static String nl;
  public static synchronized TNaiveBayesModelSparkcodeJava create(String lineSeparator)
  {
    nl = lineSeparator;
    TNaiveBayesModelSparkcodeJava result = new TNaiveBayesModelSparkcodeJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = NL + "public static class GetLabledPoint_";
  protected final String TEXT_2 = " implements org.apache.spark.api.java.function.Function<org.apache.spark.sql.Row, ";
  protected final String TEXT_3 = "> {" + NL + "    private static final long serialVersionUID = -5951511723860660208L;" + NL + "    public ";
  protected final String TEXT_4 = " call(org.apache.spark.sql.Row input) {";
  protected final String TEXT_5 = NL + "        ";
  protected final String TEXT_6 = " vectors = null;";
  protected final String TEXT_7 = NL + "        ";
  protected final String TEXT_8 = " lablePointObj=null;" + NL + "        try {";
  protected final String TEXT_9 = NL + "                vectors=(";
  protected final String TEXT_10 = ") input.get(input.fieldIndex(\"";
  protected final String TEXT_11 = "\"));";
  protected final String TEXT_12 = NL + "                vectors=org.apache.spark.mllib.linalg.Vectors.fromML(" + NL + "                        (org.apache.spark.ml.linalg.Vector)input.get(input.fieldIndex(\"";
  protected final String TEXT_13 = "\")));";
  protected final String TEXT_14 = "    " + NL + "            Double label=(Double)input.get(input.fieldIndex(\"indexedLabel\"));" + NL + "            lablePointObj=new  ";
  protected final String TEXT_15 = "(label,vectors);" + NL + "        } catch (java.lang.ClassCastException e) {" + NL + "            // nothing, return null" + NL + "        }" + NL + "        return lablePointObj;" + NL + "    }" + NL + "}";

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    
final String modelType = ElementParameterParser.getValue((INode) ((BigDataCodeGeneratorArgument) argument).getArgument(), "__SPARK_VERSION__");

// NAIVEBAYES sparkcode
if("SPARK_VERSION_1.4+".equals(modelType)){

    
BigDataCodeGeneratorArgument codeGenArgument = (BigDataCodeGeneratorArgument)argument;
INode node = (INode)codeGenArgument.getArgument();
String cid = node.getUniqueName();
final String vectorClass="org.apache.spark.mllib.linalg.Vector";
final String labledPointClass="org.apache.spark.mllib.regression.LabeledPoint";

String featuresColumn = ElementParameterParser.getValue(node, "__FEATURES_COLUMN__");
final boolean isSpark1 = org.talend.hadoop.distribution.ESparkVersion.SPARK_2_0.compareTo(codeGenArgument.getSparkVersion()) > 0;

    stringBuffer.append(TEXT_1);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_2);
    stringBuffer.append(labledPointClass);
    stringBuffer.append(TEXT_3);
    stringBuffer.append(labledPointClass);
    stringBuffer.append(TEXT_4);
    stringBuffer.append(TEXT_5);
    stringBuffer.append(vectorClass);
    stringBuffer.append(TEXT_6);
    stringBuffer.append(TEXT_7);
    stringBuffer.append(labledPointClass);
    stringBuffer.append(TEXT_8);
    
            if (isSpark1) {
                
    stringBuffer.append(TEXT_9);
    stringBuffer.append(vectorClass);
    stringBuffer.append(TEXT_10);
    stringBuffer.append(featuresColumn);
    stringBuffer.append(TEXT_11);
    
            } else {
                
    stringBuffer.append(TEXT_12);
    stringBuffer.append(featuresColumn);
    stringBuffer.append(TEXT_13);
    }
    stringBuffer.append(TEXT_14);
    stringBuffer.append(labledPointClass);
    stringBuffer.append(TEXT_15);
    
}else{
    
}

    return stringBuffer.toString();
  }
}
