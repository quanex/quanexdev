package org.talend.designer.codegen.translators.elt.map.sap;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.talend.core.model.metadata.IMetadataColumn;
import org.talend.core.model.metadata.IMetadataTable;
import org.talend.core.model.metadata.types.JavaType;
import org.talend.core.model.metadata.types.JavaTypesManager;
import org.talend.core.model.process.ElementParameterParser;
import org.talend.core.model.process.IConnection;
import org.talend.core.model.process.IConnectionCategory;
import org.talend.core.model.process.INode;
import org.talend.designer.codegen.config.CodeGeneratorArgument;
import org.talend.core.model.utils.TalendTextUtils;
import org.talend.designer.sapelt.SAPMapComponent;
import org.talend.designer.sapelt.model.emf.sapelt.SapEltData;
import org.talend.designer.sapelt.model.emf.sapelt.InputTable;
import org.talend.designer.sapelt.model.emf.sapelt.OutputTable;
import org.talend.designer.sapelt.language.ScriptGenerationManager;
import org.talend.designer.sapelt.model.emf.sapelt.TableEntry;
import org.talend.designer.sapelt.expressionutils.DataMapExpressionParser;
import org.talend.designer.sapelt.language.SapLanguage;
import org.talend.designer.sapelt.model.tableentry.TableEntryLocation;

public class TELTSAPMapEndJava
{
  protected static String nl;
  public static synchronized TELTSAPMapEndJava create(String lineSeparator)
  {
    nl = lineSeparator;
    TELTSAPMapEndJava result = new TELTSAPMapEndJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "";
  protected final String TEXT_2 = NL + NL + "\t}" + NL + "} finally {" + NL + "\t";
  protected final String TEXT_3 = NL + "\tif(connection_";
  protected final String TEXT_4 = "!=null && connection_";
  protected final String TEXT_5 = ".isAlive()) {" + NL + "\t\tconnection_";
  protected final String TEXT_6 = ".close();" + NL + "\t}" + NL + "\t";
  protected final String TEXT_7 = NL + "}";
  protected final String TEXT_8 = NL;

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    stringBuffer.append(TEXT_1);
    
	CodeGeneratorArgument codeGenArgument = (CodeGeneratorArgument) argument;
	
	SAPMapComponent node = (SAPMapComponent) codeGenArgument.getArgument();
	SapEltData sapdata = node.getExternalEmfData();
	List<InputTable> inputTables = sapdata.getInputTables();
	List<OutputTable> outputTables = sapdata.getOutputTables();
	
	ScriptGenerationManager gm = node.getGenerationManager();
	
	String cid = node.getUniqueName();
	
	List<? extends IConnection> outputConnections = node.getOutgoingSortedConnections();
	if((outputConnections == null) || (outputConnections.size() == 0)) {
		return "";
	}
	IConnection outputConnection = outputConnections.get(0);
	
	if(!outputConnection.getLineStyle().hasConnectionCategory(IConnectionCategory.DATA)) {
		return "";
	}
	
	OutputTable outputTable = null;
	
	if(outputTables==null || outputTables.isEmpty()) {
		return "";
	}
	
	for(OutputTable ot : outputTables) {
		if(ot.getName().equals(outputConnection.getUniqueName())) {
			outputTable = ot;
		}
	}
	
	if(outputTable == null) {
		return "";
	}
	
	List<TableEntry> tableEntries = outputTable.getTableEntries();
	
	if(tableEntries == null || tableEntries.isEmpty()) {
		return "";
	}
	
	Map<String, TableEntry> entryMap = new HashMap<String, TableEntry>();
	for(TableEntry entry : tableEntries) {
		entryMap.put(entry.getName(), entry);
	}
	
	IMetadataTable outputMetadata = outputConnection.getMetadataTable();
	if(outputMetadata == null) {
		return "";
	}
	
	List<IMetadataColumn> columnList = outputMetadata.getListColumns();
	
	if(columnList == null || columnList.isEmpty()) {
		return "";
	}
	
	boolean useExistingConn = ("true").equals(ElementParameterParser.getValue(node,"__USE_EXISTING_CONNECTION__"));

    stringBuffer.append(TEXT_2);
    if(!useExistingConn) {
    stringBuffer.append(TEXT_3);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_4);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_5);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_6);
    }
    stringBuffer.append(TEXT_7);
    stringBuffer.append(TEXT_8);
    return stringBuffer.toString();
  }
}
