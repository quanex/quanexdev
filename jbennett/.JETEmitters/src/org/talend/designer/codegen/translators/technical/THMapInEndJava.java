package org.talend.designer.codegen.translators.technical;

import org.talend.core.model.metadata.IMetadataTable;
import org.talend.core.model.metadata.IMetadataColumn;
import org.talend.core.model.process.IConnection;
import org.talend.core.model.process.IConnectionCategory;
import org.talend.core.model.process.INode;
import org.talend.core.model.process.ElementParameterParser;
import org.talend.core.model.utils.TalendTextUtils;
import org.talend.designer.codegen.config.CodeGeneratorArgument;

public class THMapInEndJava
{
  protected static String nl;
  public static synchronized THMapInEndJava create(String lineSeparator)
  {
    nl = lineSeparator;
    THMapInEndJava result = new THMapInEndJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "\t//THMAPIN_END thMap: ";
  protected final String TEXT_2 = NL + "    } //end for (rows_)";
  protected final String TEXT_3 = NL + "    java.io.InputStream is_";
  protected final String TEXT_4 = " = (java.io.InputStream)";
  protected final String TEXT_5 = ".this.globalMap.get(Thread" + NL + "                  .currentThread().getId()+\"_";
  protected final String TEXT_6 = "_\"+\"outputResult\");" + NL + "    if (is_";
  protected final String TEXT_7 = " != null)" + NL + "        is_";
  protected final String TEXT_8 = ".close();";
  protected final String TEXT_9 = NL + "    globalMap.put(\"";
  protected final String TEXT_10 = "_NB_LINE\",nb_line_";
  protected final String TEXT_11 = ");";
  protected final String TEXT_12 = NL;

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    
    //THMAPIN_END
    CodeGeneratorArgument codeGenArgument = (CodeGeneratorArgument) argument;
    INode node = (INode)codeGenArgument.getArgument();
    String processName = org.talend.core.model.utils.JavaResourcesHelper.getSimpleClassName(node.getProcess());
    String this_cid = ElementParameterParser.getValue(node, "__UNIQUE_NAME__");
    String tHMap_id = this_cid.replace("_THMAP_IN", "");
    String cid = tHMap_id;
  
    boolean asMap = "true".equals(ElementParameterParser.getValue(node, "__AS_MAP__"));
    boolean asInputstream = "true".equals(ElementParameterParser.getValue(node, "__AS_INPUTSTREAM__"));

    //Filter out non FLOW_MAIN/ROUTE type connections

    stringBuffer.append(TEXT_1);
    stringBuffer.append(this_cid);
        
    java.util.List<? extends IConnection> mainOutConnections = org.talend.transform.components.utils.ComponentUtils.getMainConnections(node.getOutgoingConnections());

      if (asMap && mainOutConnections.size()>=1) {

    stringBuffer.append(TEXT_2);
      } else if (asInputstream) { 

    stringBuffer.append(TEXT_3);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_4);
    stringBuffer.append( processName );
    stringBuffer.append(TEXT_5);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_6);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_7);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_8);
    
  } 

    stringBuffer.append(TEXT_9);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_10);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_11);
    stringBuffer.append(TEXT_12);
    return stringBuffer.toString();
  }
}
