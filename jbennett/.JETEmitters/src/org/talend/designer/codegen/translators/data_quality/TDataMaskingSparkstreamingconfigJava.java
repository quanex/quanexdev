package org.talend.designer.codegen.translators.data_quality;

import java.util.List;
import org.talend.core.model.metadata.IMetadataColumn;
import org.talend.core.model.metadata.IMetadataTable;
import org.talend.core.model.metadata.types.JavaType;
import org.talend.core.model.metadata.types.JavaTypesManager;
import org.talend.core.model.process.EConnectionType;
import org.talend.core.model.process.ElementParameterParser;
import org.talend.core.model.process.IBigDataNode;
import org.talend.core.model.process.IConnection;
import org.talend.core.model.process.IConnectionCategory;
import org.talend.core.model.process.INode;
import org.talend.designer.common.BigDataCodeGeneratorArgument;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

public class TDataMaskingSparkstreamingconfigJava
{
  protected static String nl;
  public static synchronized TDataMaskingSparkstreamingconfigJava create(String lineSeparator)
  {
    nl = lineSeparator;
    TDataMaskingSparkstreamingconfigJava result = new TDataMaskingSparkstreamingconfigJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "    ";
  protected final String TEXT_2 = NL;
  protected final String TEXT_3 = NL + NL + "Map<Integer, org.talend.dataquality.datamasking.functions.Function> pairList_";
  protected final String TEXT_4 = " = new java.util.HashMap<Integer, org.talend.dataquality.datamasking.functions.Function>();" + NL + "final org.talend.dataquality.datamasking.FunctionFactory fact_";
  protected final String TEXT_5 = " = new org.talend.dataquality.datamasking.FunctionFactory();" + NL + "final org.talend.dataquality.datamasking.TypeTester t_";
  protected final String TEXT_6 = " = new org.talend.dataquality.datamasking.TypeTester();" + NL + "final org.talend.dataquality.duplicating.RandomWrapper rnd_";
  protected final String TEXT_7 = " = new org.talend.dataquality.duplicating.RandomWrapper" + NL + "      (";
  protected final String TEXT_8 = NL + "\t     Long.valueOf(";
  protected final String TEXT_9 = ")";
  protected final String TEXT_10 = NL + "      );" + NL;
  protected final String TEXT_11 = NL + "              ctx.sparkContext().sc().addFile(";
  protected final String TEXT_12 = ", true);";
  protected final String TEXT_13 = NL + "              ctx.sc().addFile(";
  protected final String TEXT_14 = ", true);";
  protected final String TEXT_15 = NL + NL + "\t    ";
  protected final String TEXT_16 = " value_";
  protected final String TEXT_17 = " = null;" + NL + "\t    int type_";
  protected final String TEXT_18 = " = t_";
  protected final String TEXT_19 = ".getType(value_";
  protected final String TEXT_20 = ");" + NL + "" + NL + "\t    @SuppressWarnings(\"unchecked\")" + NL + "\t    final org.talend.dataquality.datamasking.functions.Function<";
  protected final String TEXT_21 = "> fun_";
  protected final String TEXT_22 = " = (org.talend.dataquality.datamasking.functions.Function<";
  protected final String TEXT_23 = ">) fact_";
  protected final String TEXT_24 = ".getFunction(org.talend.dataquality.datamasking.FunctionType.";
  protected final String TEXT_25 = ", type_";
  protected final String TEXT_26 = ");" + NL + "\t\t" + NL + "\t\t" + NL + "" + NL + "\t    ";
  protected final String TEXT_27 = NL + "\t        ";
  protected final String TEXT_28 = NL + "              String filePath_";
  protected final String TEXT_29 = "_";
  protected final String TEXT_30 = " = ";
  protected final String TEXT_31 = ";" + NL + "              filePath_";
  protected final String TEXT_32 = "_";
  protected final String TEXT_33 = " = org.apache.spark.SparkFiles.get(filePath_";
  protected final String TEXT_34 = "_";
  protected final String TEXT_35 = ".substring(filePath_";
  protected final String TEXT_36 = "_";
  protected final String TEXT_37 = ".lastIndexOf(\"/\") + 1));" + NL + "              System.out.println(\">>> \"+ filePath_";
  protected final String TEXT_38 = "_";
  protected final String TEXT_39 = ");" + NL + "              fun_";
  protected final String TEXT_40 = ".parse(filePath_";
  protected final String TEXT_41 = "_";
  protected final String TEXT_42 = ", ";
  protected final String TEXT_43 = ", rnd_";
  protected final String TEXT_44 = ");";
  protected final String TEXT_45 = "    " + NL + "\t\t          fun_";
  protected final String TEXT_46 = ".parse(";
  protected final String TEXT_47 = ", ";
  protected final String TEXT_48 = ", rnd_";
  protected final String TEXT_49 = ");" + NL + "\t\t    ";
  protected final String TEXT_50 = NL + "\t    ";
  protected final String TEXT_51 = NL + "\t\t    fun_";
  protected final String TEXT_52 = ".parse(null, ";
  protected final String TEXT_53 = ", rnd_";
  protected final String TEXT_54 = ");" + NL + "\t    ";
  protected final String TEXT_55 = NL + "\t    fun_";
  protected final String TEXT_56 = ".setKeepFormat(";
  protected final String TEXT_57 = ");" + NL + "\t    fun_";
  protected final String TEXT_58 = ".setKeepEmpty(";
  protected final String TEXT_59 = ");" + NL + "\t    pairList_";
  protected final String TEXT_60 = ".put(";
  protected final String TEXT_61 = ", fun_";
  protected final String TEXT_62 = ");";
  protected final String TEXT_63 = NL;
  protected final String TEXT_64 = NL + "        org.apache.spark.streaming.api.java.JavaDStream<";
  protected final String TEXT_65 = "> rdd_";
  protected final String TEXT_66 = " = rdd_";
  protected final String TEXT_67 = ".";
  protected final String TEXT_68 = NL + "        org.apache.spark.api.java.JavaRDD<";
  protected final String TEXT_69 = "> rdd_";
  protected final String TEXT_70 = " = rdd_";
  protected final String TEXT_71 = ".";
  protected final String TEXT_72 = "flatMap";
  protected final String TEXT_73 = "map";
  protected final String TEXT_74 = "(new ";
  protected final String TEXT_75 = "maskFunction(job, pairList_";
  protected final String TEXT_76 = "));      ";

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    stringBuffer.append(TEXT_1);
    
// Parse the inputs to this javajet generator.
final BigDataCodeGeneratorArgument codeGenArgument = (BigDataCodeGeneratorArgument) argument;

final INode node = (INode) codeGenArgument.getArgument();
final IBigDataNode bigDataNode = (IBigDataNode) codeGenArgument.getArgument();
final String cid = node.getUniqueName();

List<Map<String, String>> modifTableList = (List<Map<String, String>>)ElementParameterParser.getObjectValue(node, "__MODIF_TABLE__");
String randomSeedString = ElementParameterParser.getValue(node, "__RANDOM_SEED__");
boolean keepNull = ("true").equals(ElementParameterParser.getValue(node, "__KEEP_NULL__"));
boolean keepEmpty = ("true").equals(ElementParameterParser.getValue(node, "__KEEP_EMPTY__"));
boolean keepOriginal = ("true").equals(ElementParameterParser.getValue(node, "__OUTPUT_ORIGINAL__"));

    stringBuffer.append(TEXT_2);
    

IConnection outConn = node.getOutgoingConnections().get(0);
String outConnTypeName = codeGenArgument.getRecordStructName(outConn);

String incomingConnName = null;
IMetadataTable inputMetadateTable = null;
java.util.List<IMetadataColumn> inputColumns = null;
List< ? extends IConnection> incomingConnections = node.getIncomingConnections();

String outgoingConnName = null;
IMetadataTable outputMetadataTable = null;
java.util.List<IMetadataColumn> outputColumns = null;
List< ? extends IConnection> outgoingConnections = node.getOutgoingConnections();

if (incomingConnections != null && !incomingConnections.isEmpty())
    {
	for (IConnection conn : incomingConnections)
	    {
		if (conn.getLineStyle().hasConnectionCategory(IConnectionCategory.DATA))
		    {
			incomingConnName = conn.getName();
			inputMetadateTable = conn.getMetadataTable();
			inputColumns = inputMetadateTable.getListColumns();
			break;
		    }
	    }
    }
if (outgoingConnections != null && !outgoingConnections.isEmpty())
    {
	for (IConnection conn : outgoingConnections)
	    {
		if (conn.getLineStyle().hasConnectionCategory(IConnectionCategory.DATA))
		    {
			outgoingConnName = conn.getName();
			outputMetadataTable = conn.getMetadataTable();
			outputColumns = outputMetadataTable.getListColumns();
			break;
		    }
	    }
    }

    stringBuffer.append(TEXT_3);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_4);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_5);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_6);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_7);
    
         if(randomSeedString.length() > 0 && !"\"\"".equals(randomSeedString)) {

    stringBuffer.append(TEXT_8);
    stringBuffer.append(randomSeedString);
    stringBuffer.append(TEXT_9);
    
         }

    stringBuffer.append(TEXT_10);
    
int count = 0;
for(int i = 0; i < inputColumns.size(); i++) {

    IMetadataColumn column = inputColumns.get(i);

    for(Map<String, String> columnModifMap : modifTableList) {

	if(column.getLabel().equalsIgnoreCase(columnModifMap.get("INPUT_COLUMN"))) {
		String function = columnModifMap.get("FUNCTION");
		String extraParam = columnModifMap.get("EXTRA_PARAMETER");
 
        if("GENERATE_FROM_FILE".equals(function) || "GENERATE_FROM_FILE_HASH".equals(function)){
       
    
          if("SPARKSTREAMING".equals(node.getComponent().getType())
            && !org.talend.designer.common.tmap.LookupUtil.isNodeInBatchMode(node)) {
          
    stringBuffer.append(TEXT_11);
    stringBuffer.append(extraParam);
    stringBuffer.append(TEXT_12);
    
          } else {
          
    stringBuffer.append(TEXT_13);
    stringBuffer.append(extraParam);
    stringBuffer.append(TEXT_14);
    
          }
        }
        String sKeepFormat = columnModifMap.get("KEEP_FORMAT");
        boolean bKeepFormat=sKeepFormat==null?false:Boolean.valueOf(sKeepFormat).booleanValue();
	    String typeToGenerate = JavaTypesManager.getTypeToGenerate(column.getTalendType(), true);//TDQ-11328: fix compil err for type "int"(not nullable)
	    JavaType javaType = JavaTypesManager.getJavaTypeFromId(column.getTalendType());
	    count++;

    stringBuffer.append(TEXT_15);
    stringBuffer.append(typeToGenerate);
    stringBuffer.append(TEXT_16);
    stringBuffer.append(cid);
    stringBuffer.append(count);
    stringBuffer.append(TEXT_17);
    stringBuffer.append(cid);
    stringBuffer.append(count);
    stringBuffer.append(TEXT_18);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_19);
    stringBuffer.append(cid);
    stringBuffer.append(count);
    stringBuffer.append(TEXT_20);
    stringBuffer.append(typeToGenerate);
    stringBuffer.append(TEXT_21);
    stringBuffer.append(cid);
    stringBuffer.append(count);
    stringBuffer.append(TEXT_22);
    stringBuffer.append(typeToGenerate);
    stringBuffer.append(TEXT_23);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_24);
    stringBuffer.append(function);
    stringBuffer.append(TEXT_25);
    stringBuffer.append(cid);
    stringBuffer.append(count);
    stringBuffer.append(TEXT_26);
    
		if (extraParam.length() > 0) {
		
    stringBuffer.append(TEXT_27);
     	  
		    if("GENERATE_FROM_FILE".equals(function) || "GENERATE_FROM_FILE_HASH".equals(function)){
            
    stringBuffer.append(TEXT_28);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_29);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_30);
    stringBuffer.append(extraParam);
    stringBuffer.append(TEXT_31);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_32);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_33);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_34);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_35);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_36);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_37);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_38);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_39);
    stringBuffer.append(cid);
    stringBuffer.append(count);
    stringBuffer.append(TEXT_40);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_41);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_42);
    stringBuffer.append(keepNull);
    stringBuffer.append(TEXT_43);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_44);
      
		    } else {
		      
    stringBuffer.append(TEXT_45);
    stringBuffer.append(cid);
    stringBuffer.append(count);
    stringBuffer.append(TEXT_46);
    stringBuffer.append(extraParam);
    stringBuffer.append(TEXT_47);
    stringBuffer.append(keepNull);
    stringBuffer.append(TEXT_48);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_49);
    
		    }
		    
    stringBuffer.append(TEXT_50);
    
		} else {
	    
    stringBuffer.append(TEXT_51);
    stringBuffer.append(cid);
    stringBuffer.append(count);
    stringBuffer.append(TEXT_52);
    stringBuffer.append(keepNull);
    stringBuffer.append(TEXT_53);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_54);
    
		}
	    
    stringBuffer.append(TEXT_55);
    stringBuffer.append(cid);
    stringBuffer.append(count);
    stringBuffer.append(TEXT_56);
    stringBuffer.append(bKeepFormat);
    stringBuffer.append(TEXT_57);
    stringBuffer.append(cid);
    stringBuffer.append(count);
    stringBuffer.append(TEXT_58);
    stringBuffer.append(keepEmpty);
    stringBuffer.append(TEXT_59);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_60);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_61);
    stringBuffer.append(cid);
    stringBuffer.append(count);
    stringBuffer.append(TEXT_62);
    
	}
	}
}

    stringBuffer.append(TEXT_63);
    
    if("SPARKSTREAMING".equals(node.getComponent().getType())
            && !org.talend.designer.common.tmap.LookupUtil.isNodeInBatchMode(node)) {

    stringBuffer.append(TEXT_64);
    stringBuffer.append(outConnTypeName);
    stringBuffer.append(TEXT_65);
    stringBuffer.append(outgoingConnName);
    stringBuffer.append(TEXT_66);
    stringBuffer.append(incomingConnName);
    stringBuffer.append(TEXT_67);
    
    }else{

    stringBuffer.append(TEXT_68);
    stringBuffer.append(outConnTypeName);
    stringBuffer.append(TEXT_69);
    stringBuffer.append(outgoingConnName);
    stringBuffer.append(TEXT_70);
    stringBuffer.append(incomingConnName);
    stringBuffer.append(TEXT_71);
    
    }    
    if (keepOriginal) {
        
    stringBuffer.append(TEXT_72);
     
    } else {
        
    stringBuffer.append(TEXT_73);
    
    }

    stringBuffer.append(TEXT_74);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_75);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_76);
    return stringBuffer.toString();
  }
}
