package org.talend.designer.codegen.translators.messaging.kafka;

import java.util.List;
import org.talend.core.model.metadata.IMetadataColumn;
import org.talend.core.model.metadata.IMetadataTable;
import org.talend.core.model.metadata.types.JavaType;
import org.talend.core.model.metadata.types.JavaTypesManager;
import org.talend.core.model.process.EConnectionType;
import org.talend.core.model.process.ElementParameterParser;
import org.talend.core.model.process.IConnection;
import org.talend.core.model.process.IConnectionCategory;
import org.talend.core.model.process.INode;
import org.talend.designer.common.BigDataCodeGeneratorArgument;
import org.talend.designer.common.tkafkaoutput.TKafkaOutputUtil;
import org.talend.designer.common.tsetkeystore.TSetKeystoreUtil;
import org.talend.designer.spark.generator.storage.KafkaSparkStorage;

public class TKafkaOutputSparkstreamingcodeJava
{
  protected static String nl;
  public static synchronized TKafkaOutputSparkstreamingcodeJava create(String lineSeparator)
  {
    nl = lineSeparator;
    TKafkaOutputSparkstreamingcodeJava result = new TKafkaOutputSparkstreamingcodeJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "";
  protected final String TEXT_2 = NL + "\t\tkafkaProperties = new java.util.Properties();";
  protected final String TEXT_3 = NL + "\t\t\tif(true){" + NL + "\t\t\t\tthrow new Exception(\"A broker list must be provided.\");" + NL + "\t\t\t}";
  protected final String TEXT_4 = NL + "\t\t\tkafkaProperties.setProperty(\"bootstrap.servers\", ";
  protected final String TEXT_5 = ");" + NL + "\t\t\tkafkaProperties.setProperty(\"compression.type\", \"";
  protected final String TEXT_6 = "\");";
  protected final String TEXT_7 = NL + "\t\t\t\tkafkaProperties.setProperty(";
  protected final String TEXT_8 = ", ";
  protected final String TEXT_9 = ");";
  protected final String TEXT_10 = NL + "\t\t\t\t kafkaProperties.setProperty(\"security.protocol\", \"";
  protected final String TEXT_11 = "\");" + NL + "\t\t\t\t kafkaProperties.setProperty(\"ssl.truststore.type\", ";
  protected final String TEXT_12 = ");" + NL + "\t\t\t \t // We call SparkContext#addFile() to make the added file available in the current working directory on every executor node." + NL + "\t\t\t \t // But on local spark, the file is accessible directly on local FS as executor is launched on local machine." + NL + "\t\t\t\t ";
  protected final String TEXT_13 = NL + "\t\t\t \t \tkafkaProperties.setProperty(\"ssl.truststore.location\", ";
  protected final String TEXT_14 = ");" + NL + "\t\t\t \t ";
  protected final String TEXT_15 = NL + "\t\t\t \t \tkafkaProperties.setProperty(\"ssl.truststore.location\", \"./\" + new java.io.File(";
  protected final String TEXT_16 = ").getName());" + NL + "\t\t\t \t ";
  protected final String TEXT_17 = NL + "\t\t\t \t kafkaProperties.setProperty(\"ssl.truststore.password\", ";
  protected final String TEXT_18 = ");";
  protected final String TEXT_19 = NL + "\t\t\t\t \tkafkaProperties.setProperty(\"ssl.keystore.type\", ";
  protected final String TEXT_20 = ");" + NL + "\t\t\t\t \t// This location is relative to Spark executors, after the upload of the keystore." + NL + "\t\t\t \t \tkafkaProperties.setProperty(\"ssl.keystore.location\", \"./\" + new java.io.File(";
  protected final String TEXT_21 = ").getName());" + NL + "\t\t\t \t \tkafkaProperties.setProperty(\"ssl.keystore.password\", ";
  protected final String TEXT_22 = ");";
  protected final String TEXT_23 = NL + "\t\t\t\tkafkaProperties.setProperty(\"security.protocol\", \"";
  protected final String TEXT_24 = "\");" + NL + "\t\t\t\tkafkaProperties.setProperty(\"sasl.kerberos.service.name\", ";
  protected final String TEXT_25 = ");";
  protected final String TEXT_26 = NL + "\t\t   \t\tkafkaProperties.setProperty(\"sasl.kerberos.kinit.cmd\", ";
  protected final String TEXT_27 = ");";
  protected final String TEXT_28 = NL + "\t\tkafkaTopics = new java.util.HashSet<String>();";
  protected final String TEXT_29 = NL + "\t\t\tif(true){" + NL + "\t\t\t\tthrow new Exception(\"At least one Kafka topic must be provided.\");" + NL + "\t\t\t}";
  protected final String TEXT_30 = NL + "\t\t\t\tkafkaTopics.add(";
  protected final String TEXT_31 = ");";
  protected final String TEXT_32 = NL + "\t\t\t\t// Send the keystore to Spark executors." + NL + "\t\t\t\tjava.io.File ";
  protected final String TEXT_33 = "_keystore = new java.io.File(";
  protected final String TEXT_34 = ");" + NL + "\t\t\t\tif(!";
  protected final String TEXT_35 = "_keystore.exists()) {" + NL + "\t\t\t\t\tthrow new RuntimeException(\"Could not find client keystore at location [\" + ";
  protected final String TEXT_36 = " + \"].\");" + NL + "\t\t\t\t}" + NL + "\t\t\t\tctx.sparkContext().addFile(";
  protected final String TEXT_37 = ");" + NL + "\t\t\t\t";
  protected final String TEXT_38 = NL + "\t\t\t// Send the truststore to Spark executors." + NL + "\t\t\tjava.io.File ";
  protected final String TEXT_39 = "_truststore = new java.io.File(";
  protected final String TEXT_40 = ");" + NL + "\t\t \tif(!";
  protected final String TEXT_41 = "_truststore.exists()) {" + NL + "\t\t\t \tthrow new RuntimeException(\"Could not find client truststore at location [\" + ";
  protected final String TEXT_42 = " + \"].\");" + NL + "\t\t  \t}" + NL + "\t\t\tctx.sparkContext().addFile(";
  protected final String TEXT_43 = ");" + NL + "\t\t\t";
  protected final String TEXT_44 = NL + "\t\t\tSystem.setProperty(\"java.security.auth.login.config\", ";
  protected final String TEXT_45 = ");";
  protected final String TEXT_46 = NL + "\t\t\t\tSystem.setProperty(\"java.security.krb5.conf\", ";
  protected final String TEXT_47 = ");";
  protected final String TEXT_48 = NL + "\t\t\t// Make sure the new security information is picked up." + NL + "\t\t\tjavax.security.auth.login.Configuration.setConfiguration(null);" + NL + "\t\t\t" + NL + "\t\t\t// Send the keytab to Spark executors. Its location is determined from the JAAS configuration contents." + NL + "\t\t\tjavax.security.auth.login.AppConfigurationEntry[] ";
  protected final String TEXT_49 = "_appConfigurationEntries = javax.security.auth.login.Configuration.getConfiguration().getAppConfigurationEntry(\"KafkaClient\");" + NL + "\t\t\tif(";
  protected final String TEXT_50 = "_appConfigurationEntries.length == 0) {" + NL + "\t\t\t\tthrow new RuntimeException(\"Cannot found any 'KafkaClient' login section within the JAAS configuration file [\" + ";
  protected final String TEXT_51 = " + \"].\");" + NL + "\t\t\t}" + NL + "\t\t\tif(";
  protected final String TEXT_52 = "_appConfigurationEntries.length > 1) {" + NL + "\t\t\t\tthrow new RuntimeException(\"Only 1 'KafkaClient' login section is expected from the JAAS configuration. Found \" + ";
  protected final String TEXT_53 = "_appConfigurationEntries.length + \".\");" + NL + "\t\t\t}" + NL + "\t\t\tString ";
  protected final String TEXT_54 = "_keytabPath = (String) ";
  protected final String TEXT_55 = "_appConfigurationEntries[0].getOptions().get(\"keyTab\");" + NL + "\t\t\tjava.io.File ";
  protected final String TEXT_56 = "_keytab = new java.io.File(";
  protected final String TEXT_57 = "_keytabPath);" + NL + "\t\t\tif(!";
  protected final String TEXT_58 = "_keytab.exists()) {" + NL + "\t\t\t\tthrow new RuntimeException(\"Could not find client keytab at location [\" + ";
  protected final String TEXT_59 = "_keytabPath + \"]. Please check the contents of the JAAS configuration file.\");" + NL + "\t\t\t}" + NL + "\t\t\tctx.sparkContext().addFile(";
  protected final String TEXT_60 = "_keytabPath);" + NL + "" + NL + "\t\t\t// Alter a copy of the JAAS configuration file. On the Spark executors, the keytab will be in the same folder as the JAAS file." + NL + "\t\t\t// That's why the keytab location must be changed inside the copy of the JAAS file to reflect this." + NL + "\t\t\tjava.io.File ";
  protected final String TEXT_61 = "_jaasFile = new java.io.File(";
  protected final String TEXT_62 = ");" + NL + "\t\t\tif(!";
  protected final String TEXT_63 = "_jaasFile.exists()) {" + NL + "\t\t\t\tthrow new RuntimeException(\"Could not find JAAS configuration file at location [\" + ";
  protected final String TEXT_64 = " + \"].\");" + NL + "\t\t\t}" + NL + "        \tString ";
  protected final String TEXT_65 = "_jaasOriginalContents = new String(java.nio.file.Files.readAllBytes(java.nio.file.Paths.get(";
  protected final String TEXT_66 = ")), java.nio.charset.StandardCharsets.UTF_8);" + NL + "        \tString ";
  protected final String TEXT_67 = "_jaasModifiedContents = ";
  protected final String TEXT_68 = "_jaasOriginalContents.replaceAll(";
  protected final String TEXT_69 = "_keytabPath, \"./\" + ";
  protected final String TEXT_70 = "_keytab.getName());" + NL + "        \tjava.nio.file.Path ";
  protected final String TEXT_71 = "_modifiedJaasPath = java.nio.file.Paths.get(System.getProperty(\"java.io.tmpdir\"), ";
  protected final String TEXT_72 = "_jaasFile.getName());" + NL + "        \tjava.nio.file.Files.write(";
  protected final String TEXT_73 = "_modifiedJaasPath, ";
  protected final String TEXT_74 = "_jaasModifiedContents.getBytes(java.nio.charset.StandardCharsets.UTF_8));" + NL + "        \t" + NL + "        \t// Send the altered copy of the JAAS configuration file to Spark executors." + NL + "        \tctx.sparkContext().addFile(";
  protected final String TEXT_75 = "_modifiedJaasPath.toAbsolutePath().toString());" + NL + "        \t";
  protected final String TEXT_76 = "_modifiedJaasPath.toFile().deleteOnExit();" + NL + "        \t";
  protected final String TEXT_77 = NL;
  protected final String TEXT_78 = NL;
  protected final String TEXT_79 = NL + NL + "public static class ";
  protected final String TEXT_80 = " {" + NL + "\tprivate static volatile org.apache.commons.pool2.ObjectPool<";
  protected final String TEXT_81 = "> pool;" + NL + "\t" + NL + "\tprivate ";
  protected final String TEXT_82 = "() {" + NL + "\t    // empty" + NL + "\t}" + NL + "" + NL + "\tpublic static org.apache.commons.pool2.ObjectPool<";
  protected final String TEXT_83 = "> get(ContextProperties context) {" + NL + "\t\tif(pool == null) {" + NL + "\t\t\tsynchronized(";
  protected final String TEXT_84 = ".class) {" + NL + "\t\t\t\tif(pool == null) {" + NL + "\t\t\t\t\tpool = createPool(context);" + NL + "\t\t\t\t}" + NL + "\t\t\t}" + NL + "\t\t}" + NL + "\t   return pool;" + NL + "\t}" + NL + "" + NL + "\tprivate static org.apache.commons.pool2.ObjectPool<";
  protected final String TEXT_85 = "> createPool(ContextProperties context) {" + NL + "\t    org.apache.commons.pool2.impl.GenericObjectPoolConfig config = new org.apache.commons.pool2.impl.GenericObjectPoolConfig();" + NL + "\t    config.setMaxTotal(";
  protected final String TEXT_86 = ");" + NL + "\t    config.setMaxWaitMillis(";
  protected final String TEXT_87 = ");" + NL + "\t    config.setMinIdle(";
  protected final String TEXT_88 = ");" + NL + "\t    config.setMaxIdle(";
  protected final String TEXT_89 = ");" + NL + "\t    config.setBlockWhenExhausted(true);";
  protected final String TEXT_90 = NL + "\t\t\tconfig.setTimeBetweenEvictionRunsMillis(";
  protected final String TEXT_91 = ");" + NL + "\t\t\tconfig.setMinEvictableIdleTimeMillis(";
  protected final String TEXT_92 = ");" + NL + "\t\t\tconfig.setSoftMinEvictableIdleTimeMillis(";
  protected final String TEXT_93 = ");";
  protected final String TEXT_94 = NL + "\t    return new org.apache.commons.pool2.impl.GenericObjectPool<";
  protected final String TEXT_95 = ">(new ";
  protected final String TEXT_96 = "(context), config);" + NL + "\t}" + NL + "}" + NL + "" + NL + "public static class ";
  protected final String TEXT_97 = " extends org.apache.commons.pool2.BasePooledObjectFactory<";
  protected final String TEXT_98 = "> {" + NL + "" + NL + "\tprivate ContextProperties context;" + NL + "" + NL + "\tprivate java.util.Properties kafkaProperties;" + NL + "" + NL + "\tpublic ";
  protected final String TEXT_99 = "(ContextProperties context) {" + NL + "\t\tthis.context = context;";
  protected final String TEXT_100 = NL + "\t}" + NL + "" + NL + "\t@Override" + NL + "\tpublic ";
  protected final String TEXT_101 = " create() throws java.lang.Exception {" + NL + "\t\treturn new ";
  protected final String TEXT_102 = "(this.kafkaProperties, new org.apache.kafka.common.serialization.ByteArraySerializer(), new org.apache.kafka.common.serialization.ByteArraySerializer());" + NL + "\t}" + NL + "" + NL + "\t@Override" + NL + "\tpublic org.apache.commons.pool2.PooledObject<";
  protected final String TEXT_103 = "> wrap(";
  protected final String TEXT_104 = " producer) {" + NL + "\t    return new org.apache.commons.pool2.impl.DefaultPooledObject<";
  protected final String TEXT_105 = ">(producer);" + NL + "\t}" + NL + "" + NL + "\t@Override" + NL + "\tpublic void destroyObject(org.apache.commons.pool2.PooledObject<";
  protected final String TEXT_106 = "> pooledObject) throws Exception {" + NL + "\t    if (pooledObject != null) {" + NL + "\t        pooledObject.getObject().close();" + NL + "\t    }" + NL + "\t    super.destroyObject(pooledObject);" + NL + "\t}" + NL + "}" + NL + "" + NL + "public static class ";
  protected final String TEXT_107 = "_ForeachRDD<KEY> implements ";
  protected final String TEXT_108 = " {" + NL + "" + NL + "\tprivate ContextProperties context;" + NL + "" + NL + "\tpublic ";
  protected final String TEXT_109 = "_ForeachRDD(JobConf job){" + NL + "\t\tthis.context = new ContextProperties(job);" + NL + "\t}" + NL + "" + NL + "\tpublic ";
  protected final String TEXT_110 = " call(org.apache.spark.api.java.JavaPairRDD<KEY,";
  protected final String TEXT_111 = "> rdd) throws java.lang.Exception {" + NL + "\t\trdd.foreachPartition(new ";
  protected final String TEXT_112 = "_ForeachPartition<KEY>(context));" + NL + "\t\t";
  protected final String TEXT_113 = NL + "\t}" + NL + "}" + NL + "" + NL + "public static class ";
  protected final String TEXT_114 = "_ForeachPartition<KEY> implements org.apache.spark.api.java.function.VoidFunction<java.util.Iterator<scala.Tuple2<KEY,";
  protected final String TEXT_115 = ">>> {" + NL + "" + NL + "\tprivate ContextProperties context;" + NL + "" + NL + "\tprivate java.util.Set<String> kafkaTopics;" + NL + "" + NL + "\tpublic ";
  protected final String TEXT_116 = "_ForeachPartition(ContextProperties context){" + NL + "\t\tthis.context = context;";
  protected final String TEXT_117 = NL + "\t}" + NL + "" + NL + "\tpublic void call(java.util.Iterator<scala.Tuple2<KEY,";
  protected final String TEXT_118 = ">> iterator) throws java.lang.Exception {" + NL + "\t\t";
  protected final String TEXT_119 = " kafkaProducer = ";
  protected final String TEXT_120 = ".get(context).borrowObject();" + NL + "\t\tscala.Tuple2<KEY,";
  protected final String TEXT_121 = "> tuple;" + NL + "\t\ttry {" + NL + "\t\t\twhile(iterator.hasNext()) {" + NL + "\t\t\t\ttuple = iterator.next();" + NL + "\t\t\t\tif(tuple._2() != null){" + NL + "\t\t\t\t\tfor(String topic : kafkaTopics){" + NL + "\t\t\t\t\t\tjava.nio.ByteBuffer byteBuffer = tuple._2().";
  protected final String TEXT_122 = ";" + NL + "\t\t\t\t\t\tif(byteBuffer != null){" + NL + "\t\t\t\t\t\t\tbyte[] value = new byte[byteBuffer.remaining()];" + NL + "\t\t\t\t\t\t\tbyteBuffer.get(value);" + NL + "\t\t\t\t\t\t\tkafkaProducer.send(new org.apache.kafka.clients.producer.ProducerRecord<byte[], byte[]>(topic, value));" + NL + "\t\t\t\t\t\t}" + NL + "\t\t\t\t\t}" + NL + "\t\t\t\t}" + NL + "\t\t\t}" + NL + "\t\t} finally {" + NL + "\t\t\tif(kafkaProducer != null) {" + NL + "\t\t\t\t";
  protected final String TEXT_123 = ".get(context).returnObject(kafkaProducer);" + NL + "\t\t\t}" + NL + "\t\t}" + NL + "\t}" + NL + "}" + NL + NL;
  protected final String TEXT_124 = NL;

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    stringBuffer.append(TEXT_1);
    
final class TKafkaOutputHelper {

	private TKafkaOutputUtil tKafkaOutputUtil;
	
	private TSetKeystoreUtil tSetKeystoreUtil;
	
	public TKafkaOutputHelper(TKafkaOutputUtil tKafkaOutputUtil){
		this.tKafkaOutputUtil = tKafkaOutputUtil;
		this.tSetKeystoreUtil = tKafkaOutputUtil.getTSetKeystoreUtil();
	}

	public void generateKafkaProperties(boolean useLocalMode) {

    stringBuffer.append(TEXT_2);
    
		if(tKafkaOutputUtil.getBrokerList() == null || "".equals(tKafkaOutputUtil.getBrokerList())){

    stringBuffer.append(TEXT_3);
     
		} else {

    stringBuffer.append(TEXT_4);
    stringBuffer.append(tKafkaOutputUtil.getBrokerList());
    stringBuffer.append(TEXT_5);
    stringBuffer.append(tKafkaOutputUtil.getCompression());
    stringBuffer.append(TEXT_6);
    
			for(java.util.Map.Entry<String, String> kafkaProperty : tKafkaOutputUtil.getKafkaProducerProperties().entrySet()) {

    stringBuffer.append(TEXT_7);
    stringBuffer.append(kafkaProperty.getKey());
    stringBuffer.append(TEXT_8);
    stringBuffer.append(kafkaProperty.getValue());
    stringBuffer.append(TEXT_9);
    
			} // end for
			
			// SSL configuration
			if (tSetKeystoreUtil.useHTTPS()) {
				// When Kerberos is active as well, the security protocol is different
				String securityProtocol = tKafkaOutputUtil.useKrb() ? "SASL_SSL" : "SSL";

    stringBuffer.append(TEXT_10);
    stringBuffer.append(securityProtocol);
    stringBuffer.append(TEXT_11);
    stringBuffer.append(tSetKeystoreUtil.getTrustStoreType());
    stringBuffer.append(TEXT_12);
    if(useLocalMode){
    stringBuffer.append(TEXT_13);
    stringBuffer.append(tSetKeystoreUtil.getTrustStorePath());
    stringBuffer.append(TEXT_14);
    } else {
    stringBuffer.append(TEXT_15);
    stringBuffer.append(tSetKeystoreUtil.getTrustStorePath());
    stringBuffer.append(TEXT_16);
    }
    stringBuffer.append(TEXT_17);
    stringBuffer.append(tSetKeystoreUtil.getTrustStorePassword());
    stringBuffer.append(TEXT_18);
    
				if (tSetKeystoreUtil.needClientAuth()) {

    stringBuffer.append(TEXT_19);
    stringBuffer.append(tSetKeystoreUtil.getKeyStoreType());
    stringBuffer.append(TEXT_20);
    stringBuffer.append(tSetKeystoreUtil.getKeyStorePath());
    stringBuffer.append(TEXT_21);
    stringBuffer.append(tSetKeystoreUtil.getKeyStorePassword());
    stringBuffer.append(TEXT_22);
    
				}
			} // end ssl
			
			// Kerberos configuration
			if (tKafkaOutputUtil.useKrb()) {
				// When SSL is active as well, the security protocol is different
				String securityProtocol = tSetKeystoreUtil.useHTTPS() ? "SASL_SSL" : "SASL_PLAINTEXT";

    stringBuffer.append(TEXT_23);
    stringBuffer.append(securityProtocol);
    stringBuffer.append(TEXT_24);
    stringBuffer.append(tKafkaOutputUtil.getKrbServiceName());
    stringBuffer.append(TEXT_25);
    
				if(tKafkaOutputUtil.isSetKinitPath()) {

    stringBuffer.append(TEXT_26);
    stringBuffer.append(tKafkaOutputUtil.getKinitPath());
    stringBuffer.append(TEXT_27);
    
		   	}
			} // end kerberos
		} // end else
	} // end generateKafkaProperties
	
	public void generateKafkaTopics() {

    stringBuffer.append(TEXT_28);
    
		if(tKafkaOutputUtil.getKafkaTopics().isEmpty()){

    stringBuffer.append(TEXT_29);
    
		}else {
			for(String kafkaTopic : tKafkaOutputUtil.getKafkaTopics()) {

    stringBuffer.append(TEXT_30);
    stringBuffer.append(kafkaTopic);
    stringBuffer.append(TEXT_31);
    
			} // end for
		} // end else
	} // end generateKafkaTopics
	
	public void generateSSL(String cid) {
		if(tSetKeystoreUtil.useHTTPS()) {
			if(tSetKeystoreUtil.needClientAuth()) {

    stringBuffer.append(TEXT_32);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_33);
    stringBuffer.append(tSetKeystoreUtil.getKeyStorePath());
    stringBuffer.append(TEXT_34);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_35);
    stringBuffer.append(tSetKeystoreUtil.getKeyStorePath());
    stringBuffer.append(TEXT_36);
    stringBuffer.append(tSetKeystoreUtil.getKeyStorePath());
    stringBuffer.append(TEXT_37);
    
			} // end if (tSetKeystoreUtil.needClientAuth())

    stringBuffer.append(TEXT_38);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_39);
    stringBuffer.append(tSetKeystoreUtil.getTrustStorePath());
    stringBuffer.append(TEXT_40);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_41);
    stringBuffer.append(tSetKeystoreUtil.getTrustStorePath());
    stringBuffer.append(TEXT_42);
    stringBuffer.append(tSetKeystoreUtil.getTrustStorePath());
    stringBuffer.append(TEXT_43);
    
		} // end if(tSetKeystoreUtil.useHTTPS())
	} // end generateSSL
	
	public void generateKerberos(String cid) {
		if(tKafkaOutputUtil.useKrb()) {

    stringBuffer.append(TEXT_44);
    stringBuffer.append(tKafkaOutputUtil.getJaasConf());
    stringBuffer.append(TEXT_45);
    
			if(tKafkaOutputUtil.isSetKrb5Conf()) {

    stringBuffer.append(TEXT_46);
    stringBuffer.append(tKafkaOutputUtil.getKrb5Conf());
    stringBuffer.append(TEXT_47);
    
			}

    stringBuffer.append(TEXT_48);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_49);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_50);
    stringBuffer.append(tKafkaOutputUtil.getJaasConf());
    stringBuffer.append(TEXT_51);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_52);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_53);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_54);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_55);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_56);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_57);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_58);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_59);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_60);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_61);
    stringBuffer.append(tKafkaOutputUtil.getJaasConf());
    stringBuffer.append(TEXT_62);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_63);
    stringBuffer.append(tKafkaOutputUtil.getJaasConf());
    stringBuffer.append(TEXT_64);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_65);
    stringBuffer.append(tKafkaOutputUtil.getJaasConf());
    stringBuffer.append(TEXT_66);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_67);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_68);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_69);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_70);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_71);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_72);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_73);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_74);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_75);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_76);
    
		} // end if(tKafkaOutputUtil.useKrb()) 
	} // end generateKerberos
	
} // end class TKafkaOutputHelper

    stringBuffer.append(TEXT_77);
    
// Parse the inputs to this javajet generator.
final BigDataCodeGeneratorArgument codeGenArgument = (BigDataCodeGeneratorArgument) argument;
final INode node = (INode)codeGenArgument.getArgument();
final TKafkaOutputUtil tKafkaOutputUtil = new TKafkaOutputUtil(node);
final TKafkaOutputHelper tKafkaOutputHelper = new TKafkaOutputHelper(tKafkaOutputUtil);
final KafkaSparkStorage storage = new KafkaSparkStorage(node);

final String cid = node.getUniqueName();
String inStructName = codeGenArgument.getRecordStructName(tKafkaOutputUtil.getIncomingConnection());

// SparkConfiguration to know whether we are in Yarn Cluster mode
final java.util.List<? extends INode> sparkConfigs = node.getProcess().getNodesOfType("tSparkConfiguration");
INode sparkConfig = null;
if(sparkConfigs != null && sparkConfigs.size() > 0) {
    sparkConfig = sparkConfigs.get(0);
}
boolean useLocalMode = false;
if(sparkConfig != null) {
   useLocalMode = "true".equals(ElementParameterParser.getValue(sparkConfig, "__SPARK_LOCAL_MODE__"));
}

final String foreachClass;
final String foreachReturnClass;
final String foreachReturn;
if (org.talend.hadoop.distribution.ESparkVersion.SPARK_2_0.compareTo(codeGenArgument.getSparkVersion()) > 0) {
    foreachClass = "org.apache.spark.api.java.function.Function<org.apache.spark.api.java.JavaPairRDD<KEY, " + inStructName + ">, Void>";
    foreachReturnClass = "Void";
    foreachReturn = "return null;";
} else {
    foreachClass = "org.apache.spark.api.java.function.VoidFunction<org.apache.spark.api.java.JavaPairRDD<KEY," + inStructName + ">>";
    foreachReturnClass = "void";
    foreachReturn = "";
}

    stringBuffer.append(TEXT_78);
    
	// Connection pool code generation.
	
	// Warning : the object factory class has to be generated elsewhere, since the factory implementation depends of
	// the actual pooling object we want to store in this pool.
	
	// This file should be included in every single Spark and Spark Streaming component sparkcode.javajet
	// which requires to create connections to an external system without relying on a dedicated Spark connector. 

    stringBuffer.append(TEXT_79);
    stringBuffer.append(storage.getPoolClassName());
    stringBuffer.append(TEXT_80);
    stringBuffer.append(storage.getPooledObjectClassName());
    stringBuffer.append(TEXT_81);
    stringBuffer.append(storage.getPoolClassName());
    stringBuffer.append(TEXT_82);
    stringBuffer.append(storage.getPooledObjectClassName());
    stringBuffer.append(TEXT_83);
    stringBuffer.append(storage.getPoolClassName());
    stringBuffer.append(TEXT_84);
    stringBuffer.append(storage.getPooledObjectClassName());
    stringBuffer.append(TEXT_85);
    stringBuffer.append(storage.getPoolMaxTotal());
    stringBuffer.append(TEXT_86);
    stringBuffer.append(storage.getPoolMaxWait());
    stringBuffer.append(TEXT_87);
    stringBuffer.append(storage.getPoolMinIdle());
    stringBuffer.append(TEXT_88);
    stringBuffer.append(storage.getPoolMaxIdle());
    stringBuffer.append(TEXT_89);
    
		if(storage.isPoolUseEviction()) {

    stringBuffer.append(TEXT_90);
    stringBuffer.append(storage.getPoolTimeBetweenEviction());
    stringBuffer.append(TEXT_91);
    stringBuffer.append(storage.getPoolEvictionMinIdleTime());
    stringBuffer.append(TEXT_92);
    stringBuffer.append(storage.getPoolEvictionSoftMinIdleTime());
    stringBuffer.append(TEXT_93);
    
		}

    stringBuffer.append(TEXT_94);
    stringBuffer.append(storage.getPooledObjectClassName());
    stringBuffer.append(TEXT_95);
    stringBuffer.append(storage.getFactoryClassName());
    stringBuffer.append(TEXT_96);
    stringBuffer.append(storage.getFactoryClassName());
    stringBuffer.append(TEXT_97);
    stringBuffer.append(storage.getPooledObjectClassName());
    stringBuffer.append(TEXT_98);
    stringBuffer.append(storage.getFactoryClassName());
    stringBuffer.append(TEXT_99);
    
		tKafkaOutputHelper.generateKafkaProperties(useLocalMode);

    stringBuffer.append(TEXT_100);
    stringBuffer.append(storage.getPooledObjectClassName());
    stringBuffer.append(TEXT_101);
    stringBuffer.append(storage.getPooledObjectClassName());
    stringBuffer.append(TEXT_102);
    stringBuffer.append(storage.getPooledObjectClassName());
    stringBuffer.append(TEXT_103);
    stringBuffer.append(storage.getPooledObjectClassName());
    stringBuffer.append(TEXT_104);
    stringBuffer.append(storage.getPooledObjectClassName());
    stringBuffer.append(TEXT_105);
    stringBuffer.append(storage.getPooledObjectClassName());
    stringBuffer.append(TEXT_106);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_107);
    stringBuffer.append(foreachClass);
    stringBuffer.append(TEXT_108);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_109);
    stringBuffer.append(foreachReturnClass);
    stringBuffer.append(TEXT_110);
    stringBuffer.append(inStructName);
    stringBuffer.append(TEXT_111);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_112);
    stringBuffer.append(foreachReturn);
    stringBuffer.append(TEXT_113);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_114);
    stringBuffer.append(inStructName);
    stringBuffer.append(TEXT_115);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_116);
    
		tKafkaOutputHelper.generateKafkaTopics();

    stringBuffer.append(TEXT_117);
    stringBuffer.append(inStructName);
    stringBuffer.append(TEXT_118);
    stringBuffer.append(storage.getPooledObjectClassName());
    stringBuffer.append(TEXT_119);
    stringBuffer.append(storage.getPoolClassName());
    stringBuffer.append(TEXT_120);
    stringBuffer.append(inStructName);
    stringBuffer.append(TEXT_121);
    stringBuffer.append(tKafkaOutputUtil.getIncomingColumnName());
    stringBuffer.append(TEXT_122);
    stringBuffer.append(storage.getPoolClassName());
    stringBuffer.append(TEXT_123);
    stringBuffer.append(TEXT_124);
    return stringBuffer.toString();
  }
}
