package org.talend.designer.codegen.translators.databases.kudu;

import java.util.List;
import org.talend.core.model.metadata.IMetadataColumn;
import org.talend.core.model.metadata.IMetadataTable;
import org.talend.core.model.metadata.types.JavaType;
import org.talend.core.model.metadata.types.JavaTypesManager;
import org.talend.core.model.process.EConnectionType;
import org.talend.core.model.process.ElementParameterParser;
import org.talend.core.model.process.IConnection;
import org.talend.core.model.process.IConnectionCategory;
import org.talend.core.model.process.INode;
import org.talend.designer.common.BigDataCodeGeneratorArgument;
import org.talend.designer.common.tkuduinput.TKuduInputUtil;

public class TKuduInputSparkcodeJava
{
  protected static String nl;
  public static synchronized TKuduInputSparkcodeJava create(String lineSeparator)
  {
    nl = lineSeparator;
    TKuduInputSparkcodeJava result = new TKuduInputSparkcodeJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "\t\t\t\t\t\t\t\tpublic static class KuduFilter_";
  protected final String TEXT_2 = "_";
  protected final String TEXT_3 = " extends scala.runtime.AbstractFunction1<org.apache.spark.sql.Row, Object> implements java.io.Serializable {" + NL + "\t\t\t\t\t\t\t\t\tprivate static final long serialVersionUID = 1L;" + NL + "\t\t\t\t\t\t\t\t\t" + NL + "\t\t\t\t\t\t\t\t\tprivate ContextProperties context;" + NL + "\t\t\t\t\t\t\t\t\t" + NL + "\t\t\t\t\t\t\t\t\tpublic KuduFilter_";
  protected final String TEXT_4 = "_";
  protected final String TEXT_5 = "(JobConf job) {" + NL + "\t\t\t\t\t\t\t\t\t\tthis.context = new ContextProperties(job);" + NL + "\t\t\t\t\t\t\t\t\t}" + NL + "\t\t\t\t\t        \t\t" + NL + "\t\t\t\t\t\t\t\t\tpublic Object call(org.apache.spark.sql.Row row) {" + NL + "\t\t\t\t\t\t\t\t\t\t";
  protected final String TEXT_6 = NL + "\t\t\t\t\t\t\t\t\t\t\treturn ";
  protected final String TEXT_7 = ".equals(row.getString(";
  protected final String TEXT_8 = "));" + NL + "\t\t\t\t\t\t\t\t\t\t";
  protected final String TEXT_9 = NL + "\t\t\t\t\t\t\t\t\t\t\treturn !";
  protected final String TEXT_10 = ".equals(row.getString(";
  protected final String TEXT_11 = "));" + NL + "\t\t\t\t\t\t\t\t\t\t";
  protected final String TEXT_12 = NL + "\t\t\t\t\t\t\t\t\t\t\treturn row.getString(";
  protected final String TEXT_13 = ") ";
  protected final String TEXT_14 = ";" + NL + "\t\t\t\t\t\t\t\t\t\t";
  protected final String TEXT_15 = NL + "\t\t\t\t\t\t\t\t\t\t\treturn 0 ";
  protected final String TEXT_16 = " ";
  protected final String TEXT_17 = ".compareTo(row.getString(";
  protected final String TEXT_18 = "));" + NL + "\t\t\t\t\t\t\t\t\t\t";
  protected final String TEXT_19 = NL + "\t\t\t\t\t\t\t\t\t}" + NL + "\t\t\t\t\t\t\t\t\t" + NL + "\t\t\t\t\t\t\t\t\tpublic Object apply(org.apache.spark.sql.Row row) {" + NL + "\t\t\t\t\t\t\t\t\t\treturn call(row);" + NL + "\t\t\t\t\t\t\t\t\t}" + NL + "\t\t\t\t\t\t\t\t}" + NL + "\t\t\t\t\t\t\t";
  protected final String TEXT_20 = NL + "\t\t\t\t\t\t\t\tpublic static class KuduFilter_";
  protected final String TEXT_21 = "_";
  protected final String TEXT_22 = " extends scala.runtime.AbstractFunction1<org.apache.spark.sql.Row, Object> implements java.io.Serializable {" + NL + "\t\t\t\t\t\t\t\t\tprivate static final long serialVersionUID = 1L;" + NL + "\t\t\t\t\t\t\t\t\t" + NL + "\t\t\t\t\t\t\t\t\tprivate ContextProperties context;" + NL + "\t\t\t\t\t\t\t\t\t" + NL + "\t\t\t\t\t\t\t\t\tpublic KuduFilter_";
  protected final String TEXT_23 = "_";
  protected final String TEXT_24 = "(JobConf job) {" + NL + "\t\t\t\t\t\t\t\t\t\tthis.context = new ContextProperties(job);" + NL + "\t\t\t\t\t\t\t\t\t}" + NL + "\t\t\t\t\t        \t\t" + NL + "\t\t\t\t\t\t\t\t\tpublic Object call(org.apache.spark.sql.Row row) {" + NL + "\t\t\t\t\t\t\t\t\t\treturn row.getInt(";
  protected final String TEXT_25 = ") ";
  protected final String TEXT_26 = " ";
  protected final String TEXT_27 = ";" + NL + "\t\t\t\t\t\t\t\t\t}" + NL + "\t\t\t\t\t\t\t\t\t" + NL + "\t\t\t\t\t\t\t\t\tpublic Object apply(org.apache.spark.sql.Row row) {" + NL + "\t\t\t\t\t\t\t\t\t\treturn call(row);" + NL + "\t\t\t\t\t\t\t\t\t}" + NL + "\t\t\t\t\t\t\t\t}" + NL + "\t\t\t\t\t\t\t";
  protected final String TEXT_28 = NL + "\t\t\t\t\t\t\t\tpublic static class KuduFilter_";
  protected final String TEXT_29 = "_";
  protected final String TEXT_30 = " extends scala.runtime.AbstractFunction1<org.apache.spark.sql.Row, Object> implements java.io.Serializable {" + NL + "\t\t\t\t\t\t\t\t\tprivate static final long serialVersionUID = 1L;" + NL + "\t\t\t\t\t\t\t\t\t" + NL + "\t\t\t\t\t\t\t\t\tprivate ContextProperties context;" + NL + "\t\t\t\t\t\t\t\t\t" + NL + "\t\t\t\t\t\t\t\t\tpublic KuduFilter_";
  protected final String TEXT_31 = "_";
  protected final String TEXT_32 = "(JobConf job) {" + NL + "\t\t\t\t\t\t\t\t\t\tthis.context = new ContextProperties(job);" + NL + "\t\t\t\t\t\t\t\t\t}" + NL + "\t\t\t\t\t        \t\t" + NL + "\t\t\t\t\t\t\t\t\tpublic Object call(org.apache.spark.sql.Row row) {" + NL + "\t\t\t\t\t\t\t\t\t\treturn row.getLong(";
  protected final String TEXT_33 = ") ";
  protected final String TEXT_34 = " ";
  protected final String TEXT_35 = ";" + NL + "\t\t\t\t\t\t\t\t\t}" + NL + "\t\t\t\t\t\t\t\t\t" + NL + "\t\t\t\t\t\t\t\t\tpublic Object apply(org.apache.spark.sql.Row row) {" + NL + "\t\t\t\t\t\t\t\t\t\treturn call(row);" + NL + "\t\t\t\t\t\t\t\t\t}" + NL + "\t\t\t\t\t\t\t\t}" + NL + "\t\t\t\t\t\t\t";
  protected final String TEXT_36 = NL + "\t\t\t\t\t\t\t\tpublic static class KuduFilter_";
  protected final String TEXT_37 = "_";
  protected final String TEXT_38 = " extends scala.runtime.AbstractFunction1<org.apache.spark.sql.Row, Object> implements java.io.Serializable {" + NL + "\t\t\t\t\t\t\t\t\tprivate static final long serialVersionUID = 1L;" + NL + "\t\t\t\t\t\t\t\t\t" + NL + "\t\t\t\t\t\t\t\t\tprivate ContextProperties context;" + NL + "\t\t\t\t\t\t\t\t\t" + NL + "\t\t\t\t\t\t\t\t\tpublic KuduFilter_";
  protected final String TEXT_39 = "_";
  protected final String TEXT_40 = "(JobConf job) {" + NL + "\t\t\t\t\t\t\t\t\t\tthis.context = new ContextProperties(job);" + NL + "\t\t\t\t\t\t\t\t\t}" + NL + "\t\t\t\t\t        \t\t" + NL + "\t\t\t\t\t\t\t\t\tpublic Object call(org.apache.spark.sql.Row row) {" + NL + "\t\t\t\t\t\t\t\t\t\treturn row.getDouble(";
  protected final String TEXT_41 = ") ";
  protected final String TEXT_42 = " ";
  protected final String TEXT_43 = ";" + NL + "\t\t\t\t\t\t\t\t\t}" + NL + "\t\t\t\t\t\t\t\t\t" + NL + "\t\t\t\t\t\t\t\t\tpublic Object apply(org.apache.spark.sql.Row row) {" + NL + "\t\t\t\t\t\t\t\t\t\treturn call(row);" + NL + "\t\t\t\t\t\t\t\t\t}" + NL + "\t\t\t\t\t\t\t\t}" + NL + "\t\t\t\t\t\t\t";
  protected final String TEXT_44 = NL + "\t\t\t\t\t\t\t\tpublic static class KuduFilter_";
  protected final String TEXT_45 = "_";
  protected final String TEXT_46 = " extends scala.runtime.AbstractFunction1<org.apache.spark.sql.Row, Object> implements java.io.Serializable {" + NL + "\t\t\t\t\t\t\t\t\tprivate static final long serialVersionUID = 1L;" + NL + "\t\t\t\t\t\t\t\t\t" + NL + "\t\t\t\t\t\t\t\t\tprivate ContextProperties context;" + NL + "\t\t\t\t\t\t\t\t\t" + NL + "\t\t\t\t\t\t\t\t\tpublic KuduFilter_";
  protected final String TEXT_47 = "_";
  protected final String TEXT_48 = "(JobConf job) {" + NL + "\t\t\t\t\t\t\t\t\t\tthis.context = new ContextProperties(job);" + NL + "\t\t\t\t\t\t\t\t\t}" + NL + "\t\t\t\t\t        \t\t" + NL + "\t\t\t\t\t\t\t\t\tpublic Object call(org.apache.spark.sql.Row row) {" + NL + "\t\t\t\t\t\t\t\t\t\treturn row.getFloat(";
  protected final String TEXT_49 = ") ";
  protected final String TEXT_50 = " ";
  protected final String TEXT_51 = ";" + NL + "\t\t\t\t\t\t\t\t\t}" + NL + "\t\t\t\t\t\t\t\t\t" + NL + "\t\t\t\t\t\t\t\t\tpublic Object apply(org.apache.spark.sql.Row row) {" + NL + "\t\t\t\t\t\t\t\t\t\treturn call(row);" + NL + "\t\t\t\t\t\t\t\t\t}" + NL + "\t\t\t\t\t\t\t\t}" + NL + "\t\t\t\t\t\t\t";
  protected final String TEXT_52 = NL + "\t\t\t\t\t\t\t\tpublic static class KuduFilter_";
  protected final String TEXT_53 = "_";
  protected final String TEXT_54 = " extends scala.runtime.AbstractFunction1<org.apache.spark.sql.Row, Object> implements java.io.Serializable {" + NL + "\t\t\t\t\t\t\t\t\tprivate static final long serialVersionUID = 1L;" + NL + "\t\t\t\t\t\t\t\t\t" + NL + "\t\t\t\t\t\t\t\t\tprivate ContextProperties context;" + NL + "\t\t\t\t\t\t\t\t\t" + NL + "\t\t\t\t\t\t\t\t\tpublic KuduFilter_";
  protected final String TEXT_55 = "_";
  protected final String TEXT_56 = "(JobConf job) {" + NL + "\t\t\t\t\t\t\t\t\t\tthis.context = new ContextProperties(job);" + NL + "\t\t\t\t\t\t\t\t\t}" + NL + "\t\t\t\t\t        \t\t" + NL + "\t\t\t\t\t\t\t\t\tpublic Object call(org.apache.spark.sql.Row row) {" + NL + "\t\t\t\t\t\t\t\t\t\treturn new java.lang.Character(row.getString(";
  protected final String TEXT_57 = ").charAt(0)) ";
  protected final String TEXT_58 = " ";
  protected final String TEXT_59 = ";" + NL + "\t\t\t\t\t\t\t\t\t}" + NL + "\t\t\t\t\t\t\t\t\t" + NL + "\t\t\t\t\t\t\t\t\tpublic Object apply(org.apache.spark.sql.Row row) {" + NL + "\t\t\t\t\t\t\t\t\t\treturn call(row);" + NL + "\t\t\t\t\t\t\t\t\t}" + NL + "\t\t\t\t\t\t\t\t}" + NL + "\t\t\t\t\t\t\t";
  protected final String TEXT_60 = NL + "\t\t\t\t\t\t\t\tpublic static class KuduFilter_";
  protected final String TEXT_61 = "_";
  protected final String TEXT_62 = " extends scala.runtime.AbstractFunction1<org.apache.spark.sql.Row, Object> implements java.io.Serializable {" + NL + "\t\t\t\t\t\t\t\t\tprivate static final long serialVersionUID = 1L;" + NL + "\t\t\t\t\t\t\t\t\t" + NL + "\t\t\t\t\t\t\t\t\tprivate ContextProperties context;" + NL + "\t\t\t\t\t\t\t\t\t" + NL + "\t\t\t\t\t\t\t\t\tpublic KuduFilter_";
  protected final String TEXT_63 = "_";
  protected final String TEXT_64 = "(JobConf job) {" + NL + "\t\t\t\t\t\t\t\t\t\tthis.context = new ContextProperties(job);" + NL + "\t\t\t\t\t\t\t\t\t}" + NL + "\t\t\t\t\t        \t\t" + NL + "\t\t\t\t\t\t\t\t\tpublic Object call(org.apache.spark.sql.Row row) {" + NL + "\t\t\t\t\t\t\t\t\t\treturn row.getShort(";
  protected final String TEXT_65 = ") ";
  protected final String TEXT_66 = " ";
  protected final String TEXT_67 = ";" + NL + "\t\t\t\t\t\t\t\t\t}" + NL + "\t\t\t\t\t\t\t\t\t" + NL + "\t\t\t\t\t\t\t\t\tpublic Object apply(org.apache.spark.sql.Row row) {" + NL + "\t\t\t\t\t\t\t\t\t\treturn call(row);" + NL + "\t\t\t\t\t\t\t\t\t}" + NL + "\t\t\t\t\t\t\t\t}" + NL + "\t\t\t\t\t\t\t";
  protected final String TEXT_68 = NL + "\t\t\t\t\t\t\t\tpublic static class KuduFilter_";
  protected final String TEXT_69 = "_";
  protected final String TEXT_70 = " extends scala.runtime.AbstractFunction1<org.apache.spark.sql.Row, Object> implements java.io.Serializable {" + NL + "\t\t\t\t\t\t\t\t\tprivate static final long serialVersionUID = 1L;" + NL + "\t\t\t\t\t\t\t\t\t" + NL + "\t\t\t\t\t\t\t\t\tprivate ContextProperties context;" + NL + "\t\t\t\t\t\t\t\t\t" + NL + "\t\t\t\t\t\t\t\t\tpublic KuduFilter_";
  protected final String TEXT_71 = "_";
  protected final String TEXT_72 = "(JobConf job) {" + NL + "\t\t\t\t\t\t\t\t\t\tthis.context = new ContextProperties(job);" + NL + "\t\t\t\t\t\t\t\t\t}" + NL + "\t\t\t\t\t        \t\t" + NL + "\t\t\t\t\t\t\t\t\tpublic Object call(org.apache.spark.sql.Row row) {" + NL + "\t\t\t\t\t\t\t\t\t\t";
  protected final String TEXT_73 = NL + "\t\t\t\t\t\t\t\t\t\t\treturn ((java.sql.Timestamp) row.get(";
  protected final String TEXT_74 = ")) ";
  protected final String TEXT_75 = ";" + NL + "\t\t\t\t\t\t\t\t\t\t";
  protected final String TEXT_76 = NL + "\t\t\t\t\t\t\t\t\t\t\tif(((java.sql.Timestamp) row.get(";
  protected final String TEXT_77 = ")) != null) {" + NL + "\t\t\t\t\t\t\t\t\t\t\t\treturn ((java.sql.Timestamp) row.get(";
  protected final String TEXT_78 = ")).getTime() ";
  protected final String TEXT_79 = " (";
  protected final String TEXT_80 = ").getTime();" + NL + "\t\t\t\t\t\t\t\t\t\t\t}" + NL + "\t\t\t\t\t\t\t\t\t\t\treturn false;" + NL + "\t\t\t\t\t\t\t\t\t\t";
  protected final String TEXT_81 = NL + "\t\t\t\t\t\t\t\t\t}" + NL + "\t\t\t\t\t\t\t\t\t" + NL + "\t\t\t\t\t\t\t\t\tpublic Object apply(org.apache.spark.sql.Row row) {" + NL + "\t\t\t\t\t\t\t\t\t\treturn call(row);" + NL + "\t\t\t\t\t\t\t\t\t}" + NL + "\t\t\t\t\t\t\t\t}" + NL + "\t\t\t\t\t\t\t";
  protected final String TEXT_82 = NL + "\t\t\t\t\t\t\t\tpublic static class KuduFilter_";
  protected final String TEXT_83 = "_";
  protected final String TEXT_84 = " extends scala.runtime.AbstractFunction1<org.apache.spark.sql.Row, Object> implements java.io.Serializable {" + NL + "\t\t\t\t\t\t\t\t\tprivate static final long serialVersionUID = 1L;" + NL + "\t\t\t\t\t\t\t\t\t" + NL + "\t\t\t\t\t\t\t\t\tprivate ContextProperties context;" + NL + "\t\t\t\t\t\t\t\t\t" + NL + "\t\t\t\t\t\t\t\t\tpublic KuduFilter_";
  protected final String TEXT_85 = "_";
  protected final String TEXT_86 = "(JobConf job) {" + NL + "\t\t\t\t\t\t\t\t\t\tthis.context = new ContextProperties(job);" + NL + "\t\t\t\t\t\t\t\t\t}" + NL + "\t\t\t\t\t        \t\t" + NL + "\t\t\t\t\t\t\t\t\tpublic Object call(org.apache.spark.sql.Row row) {" + NL + "\t\t\t\t\t\t\t\t\t\treturn row.getByte(";
  protected final String TEXT_87 = ") ";
  protected final String TEXT_88 = " ";
  protected final String TEXT_89 = ";" + NL + "\t\t\t\t\t\t\t\t\t}" + NL + "\t\t\t\t\t\t\t\t\t" + NL + "\t\t\t\t\t\t\t\t\tpublic Object apply(org.apache.spark.sql.Row row) {" + NL + "\t\t\t\t\t\t\t\t\t\treturn call(row);" + NL + "\t\t\t\t\t\t\t\t\t}" + NL + "\t\t\t\t\t\t\t\t}" + NL + "\t\t\t\t\t\t\t";
  protected final String TEXT_90 = NL + "\t\t\t\t\t\t\t\tpublic static class KuduFilter_";
  protected final String TEXT_91 = "_";
  protected final String TEXT_92 = " extends scala.runtime.AbstractFunction1<org.apache.spark.sql.Row, Object> implements java.io.Serializable {" + NL + "\t\t\t\t\t\t\t\t\tprivate static final long serialVersionUID = 1L;" + NL + "\t\t\t\t\t\t\t\t\t" + NL + "\t\t\t\t\t\t\t\t\tprivate ContextProperties context;" + NL + "\t\t\t\t\t\t\t\t\t" + NL + "\t\t\t\t\t\t\t\t\tpublic KuduFilter_";
  protected final String TEXT_93 = "_";
  protected final String TEXT_94 = "(JobConf job) {" + NL + "\t\t\t\t\t\t\t\t\t\tthis.context = new ContextProperties(job);" + NL + "\t\t\t\t\t\t\t\t\t}" + NL + "\t\t\t\t\t        \t\t" + NL + "\t\t\t\t\t\t\t\t\tpublic Object call(org.apache.spark.sql.Row row) {" + NL + "\t\t\t\t\t\t\t\t\t\tint value = row.getBoolean(";
  protected final String TEXT_95 = ") ? 1 : 0;" + NL + "\t\t\t\t\t\t\t\t\t\tint filter = ";
  protected final String TEXT_96 = " ? 1 : 0;" + NL + "\t\t\t\t\t\t\t\t\t\treturn value ";
  protected final String TEXT_97 = " filter;" + NL + "\t\t\t\t\t\t\t\t\t}" + NL + "\t\t\t\t\t\t\t\t\t" + NL + "\t\t\t\t\t\t\t\t\tpublic Object apply(org.apache.spark.sql.Row row) {" + NL + "\t\t\t\t\t\t\t\t\t\treturn call(row);" + NL + "\t\t\t\t\t\t\t\t\t}" + NL + "\t\t\t\t\t\t\t\t}" + NL + "\t\t\t\t\t\t\t";
  protected final String TEXT_98 = NL + "        \t" + NL + "        \tpublic static class ";
  protected final String TEXT_99 = "_FromRowTo";
  protected final String TEXT_100 = " implements org.apache.spark.api.java.function.PairFunction<org.apache.spark.sql.Row, NullWritable, ";
  protected final String TEXT_101 = "> {" + NL + "        \t" + NL + "        \t\tpublic scala.Tuple2<NullWritable, ";
  protected final String TEXT_102 = "> call(org.apache.spark.sql.Row row) {" + NL + "\t\t\t\t\t";
  protected final String TEXT_103 = " result = new ";
  protected final String TEXT_104 = "();" + NL + "\t\t\t\t\torg.apache.avro.Schema.Field avroField = null;" + NL + "\t\t\t\t\tString colType = \"\";" + NL + "\t\t\t\t\t";
  protected final String TEXT_105 = NL + "\t\t\t\t\t\tavroField = ";
  protected final String TEXT_106 = ".getClassSchema().getField(\"";
  protected final String TEXT_107 = "\");" + NL + "\t\t\t\t\t\tif (avroField != null) {" + NL + "\t\t\t\t\t\t\tif(row.get(";
  protected final String TEXT_108 = ") != null) {" + NL + "\t\t\t\t\t\t\t\t";
  protected final String TEXT_109 = NL + "\t\t\t\t\t\t\t\t\tresult.put(avroField.pos(), String.valueOf(row.get(";
  protected final String TEXT_110 = ")));" + NL + "\t\t\t\t\t\t\t\t";
  protected final String TEXT_111 = NL + "\t\t\t\t\t\t\t\t\tresult.put(avroField.pos(), new java.lang.Integer((byte) row.get(";
  protected final String TEXT_112 = ")).byteValue());" + NL + "\t\t\t\t\t\t\t\t";
  protected final String TEXT_113 = NL + "\t\t\t\t\t\t\t\t\tresult.put(avroField.pos(), new java.lang.Integer((short) row.get(";
  protected final String TEXT_114 = ")).shortValue());" + NL + "\t\t\t\t\t\t\t\t";
  protected final String TEXT_115 = NL + "\t\t\t\t\t\t\t\t\tresult.put(avroField.pos(), new java.lang.Character(((String) row.getAs(";
  protected final String TEXT_116 = ")).charAt(0)));" + NL + "\t\t\t\t\t\t\t\t";
  protected final String TEXT_117 = NL + "\t\t\t\t\t\t\t\t\tjava.sql.Timestamp timestamp = (java.sql.Timestamp) row.get(";
  protected final String TEXT_118 = ");" + NL + "\t\t\t\t\t\t\t\t\tjava.util.Calendar calendar = java.util.Calendar.getInstance();" + NL + "\t\t\t\t\t\t\t\t\tcalendar.setTimeInMillis(timestamp.getTime());" + NL + "\t\t\t\t\t\t\t\t\tresult.put(avroField.pos(), new Date(calendar.getTime().getTime()));" + NL + "\t\t\t\t\t\t\t\t";
  protected final String TEXT_119 = NL + "\t\t\t\t\t\t\t\t\tresult.put(avroField.pos(), row.get(";
  protected final String TEXT_120 = "));" + NL + "\t\t\t\t\t\t\t\t";
  protected final String TEXT_121 = "\t\t\t\t\t\t" + NL + "\t\t\t\t\t\t\t} else {" + NL + "\t\t\t\t\t\t\t\tresult.put(avroField.pos(), null);" + NL + "\t\t\t\t\t\t\t}" + NL + "\t\t\t\t\t\t}" + NL + "\t\t\t\t\t";
  protected final String TEXT_122 = NL + "\t\t\t\t\treturn new scala.Tuple2(NullWritable.get(), result);" + NL + "\t\t\t\t}" + NL + "\t\t\t}" + NL + "\t\t\t";
  protected final String TEXT_123 = NL;

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    
BigDataCodeGeneratorArgument codeGenArgument = (BigDataCodeGeneratorArgument) argument;
INode node = (INode)codeGenArgument.getArgument();
String cid = node.getUniqueName();
List<IMetadataTable> metadatas = node.getMetadataList();

    
if(metadatas != null && metadatas.size() > 0) {
	IMetadataTable metadata = metadatas.get(0);
	if(metadata != null){
    	List< ? extends IConnection> connections = node.getOutgoingConnections();
    	if ((connections != null) && (connections.size() > 0)) {
        	IConnection connection = connections.get(0);
        	String connTypeName = codeGenArgument.getRecordStructName(connection);
        	boolean useQuery = "true".equals(ElementParameterParser.getValue(node, "__USE_QUERY__"));
        	
        	if (useQuery) {
	        	TKuduInputUtil tKuduInputUtil = new TKuduInputUtil(node);
	        	org.talend.designer.common.tkuduinput.conditions.TKuduInputConditions conditions = tKuduInputUtil.findConditions();
	        	List<org.talend.designer.common.tkuduinput.condition.TKuduInputCondition> listConditions = conditions.getConditions();
	        	for(int i=0; i<listConditions.size(); i++) {
	        		List<IMetadataColumn> columns = metadata.getListColumns();
	        		int indexColF = -1;
					for(IMetadataColumn column: columns) {
						indexColF++;
						String typeToGenerate = JavaTypesManager.getTypeToGenerate(column.getTalendType(), column.isNullable());
						JavaType javaType = JavaTypesManager.getJavaTypeFromId(column.getTalendType());
						String columnName = column.getLabel();
						if(columnName.equals(listConditions.get(i).getColumn())) {
							String operator = listConditions.get(i).getOperator();
							String value = listConditions.get(i).getValue();
							if(javaType == JavaTypesManager.STRING) {
							
    stringBuffer.append(TEXT_1);
    stringBuffer.append(columnName);
    stringBuffer.append(TEXT_2);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_3);
    stringBuffer.append(columnName);
    stringBuffer.append(TEXT_4);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_5);
    
										if("==".equals(operator)) {
										
    stringBuffer.append(TEXT_6);
    stringBuffer.append(value);
    stringBuffer.append(TEXT_7);
    stringBuffer.append(indexColF);
    stringBuffer.append(TEXT_8);
    
										} else if("!=".equals(operator)) {
										
    stringBuffer.append(TEXT_9);
    stringBuffer.append(value);
    stringBuffer.append(TEXT_10);
    stringBuffer.append(indexColF);
    stringBuffer.append(TEXT_11);
    
										} else if("== null".equals(operator) || "!= null".equals(operator)) {
										
    stringBuffer.append(TEXT_12);
    stringBuffer.append(indexColF);
    stringBuffer.append(TEXT_13);
    stringBuffer.append(operator);
    stringBuffer.append(TEXT_14);
    
										} else {
										
    stringBuffer.append(TEXT_15);
    stringBuffer.append(operator);
    stringBuffer.append(TEXT_16);
    stringBuffer.append(value);
    stringBuffer.append(TEXT_17);
    stringBuffer.append(indexColF);
    stringBuffer.append(TEXT_18);
    
										}
										
    stringBuffer.append(TEXT_19);
    
							} else if(javaType == JavaTypesManager.INTEGER) {
							
    stringBuffer.append(TEXT_20);
    stringBuffer.append(columnName);
    stringBuffer.append(TEXT_21);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_22);
    stringBuffer.append(columnName);
    stringBuffer.append(TEXT_23);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_24);
    stringBuffer.append(indexColF);
    stringBuffer.append(TEXT_25);
    stringBuffer.append(operator);
    stringBuffer.append(TEXT_26);
    stringBuffer.append(value);
    stringBuffer.append(TEXT_27);
    
							} else if(javaType == JavaTypesManager.LONG) {
							
    stringBuffer.append(TEXT_28);
    stringBuffer.append(columnName);
    stringBuffer.append(TEXT_29);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_30);
    stringBuffer.append(columnName);
    stringBuffer.append(TEXT_31);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_32);
    stringBuffer.append(indexColF);
    stringBuffer.append(TEXT_33);
    stringBuffer.append(operator);
    stringBuffer.append(TEXT_34);
    stringBuffer.append(value);
    stringBuffer.append(TEXT_35);
    
							} else if(javaType == JavaTypesManager.DOUBLE) {
							
    stringBuffer.append(TEXT_36);
    stringBuffer.append(columnName);
    stringBuffer.append(TEXT_37);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_38);
    stringBuffer.append(columnName);
    stringBuffer.append(TEXT_39);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_40);
    stringBuffer.append(indexColF);
    stringBuffer.append(TEXT_41);
    stringBuffer.append(operator);
    stringBuffer.append(TEXT_42);
    stringBuffer.append(value);
    stringBuffer.append(TEXT_43);
    
							} else if(javaType == JavaTypesManager.FLOAT) {
							
    stringBuffer.append(TEXT_44);
    stringBuffer.append(columnName);
    stringBuffer.append(TEXT_45);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_46);
    stringBuffer.append(columnName);
    stringBuffer.append(TEXT_47);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_48);
    stringBuffer.append(indexColF);
    stringBuffer.append(TEXT_49);
    stringBuffer.append(operator);
    stringBuffer.append(TEXT_50);
    stringBuffer.append(value);
    stringBuffer.append(TEXT_51);
    
							} else if(javaType == JavaTypesManager.CHARACTER) {
							
    stringBuffer.append(TEXT_52);
    stringBuffer.append(columnName);
    stringBuffer.append(TEXT_53);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_54);
    stringBuffer.append(columnName);
    stringBuffer.append(TEXT_55);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_56);
    stringBuffer.append(indexColF);
    stringBuffer.append(TEXT_57);
    stringBuffer.append(operator);
    stringBuffer.append(TEXT_58);
    stringBuffer.append(value);
    stringBuffer.append(TEXT_59);
    
							} else if(javaType == JavaTypesManager.SHORT) {
							
    stringBuffer.append(TEXT_60);
    stringBuffer.append(columnName);
    stringBuffer.append(TEXT_61);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_62);
    stringBuffer.append(columnName);
    stringBuffer.append(TEXT_63);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_64);
    stringBuffer.append(indexColF);
    stringBuffer.append(TEXT_65);
    stringBuffer.append(operator);
    stringBuffer.append(TEXT_66);
    stringBuffer.append(value);
    stringBuffer.append(TEXT_67);
    
							} else if(javaType == JavaTypesManager.DATE) {
							
    stringBuffer.append(TEXT_68);
    stringBuffer.append(columnName);
    stringBuffer.append(TEXT_69);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_70);
    stringBuffer.append(columnName);
    stringBuffer.append(TEXT_71);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_72);
    
										if("== null".equals(operator) || "!= null".equals(operator)) {
										
    stringBuffer.append(TEXT_73);
    stringBuffer.append(indexColF);
    stringBuffer.append(TEXT_74);
    stringBuffer.append(operator);
    stringBuffer.append(TEXT_75);
    
										} else {
										
    stringBuffer.append(TEXT_76);
    stringBuffer.append(indexColF);
    stringBuffer.append(TEXT_77);
    stringBuffer.append(indexColF);
    stringBuffer.append(TEXT_78);
    stringBuffer.append(operator);
    stringBuffer.append(TEXT_79);
    stringBuffer.append(value);
    stringBuffer.append(TEXT_80);
    
										}
										
    stringBuffer.append(TEXT_81);
    
							} else if(javaType == JavaTypesManager.BYTE) {
							
    stringBuffer.append(TEXT_82);
    stringBuffer.append(columnName);
    stringBuffer.append(TEXT_83);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_84);
    stringBuffer.append(columnName);
    stringBuffer.append(TEXT_85);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_86);
    stringBuffer.append(indexColF);
    stringBuffer.append(TEXT_87);
    stringBuffer.append(operator);
    stringBuffer.append(TEXT_88);
    stringBuffer.append(value);
    stringBuffer.append(TEXT_89);
    
							} else if(javaType == JavaTypesManager.BOOLEAN) {
							
    stringBuffer.append(TEXT_90);
    stringBuffer.append(columnName);
    stringBuffer.append(TEXT_91);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_92);
    stringBuffer.append(columnName);
    stringBuffer.append(TEXT_93);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_94);
    stringBuffer.append(indexColF);
    stringBuffer.append(TEXT_95);
    stringBuffer.append(value);
    stringBuffer.append(TEXT_96);
    stringBuffer.append(operator);
    stringBuffer.append(TEXT_97);
    
							}
						}
					}
				}
			} // if (useQuery) {
			
    stringBuffer.append(TEXT_98);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_99);
    stringBuffer.append(connTypeName);
    stringBuffer.append(TEXT_100);
    stringBuffer.append(connTypeName);
    stringBuffer.append(TEXT_101);
    stringBuffer.append(connTypeName);
    stringBuffer.append(TEXT_102);
    stringBuffer.append(connTypeName);
    stringBuffer.append(TEXT_103);
    stringBuffer.append(connTypeName);
    stringBuffer.append(TEXT_104);
    
					List<IMetadataColumn> columns = metadata.getListColumns();
					int indexCol = -1;
					for(IMetadataColumn column: columns) {
						indexCol++;
						String typeToGenerate = JavaTypesManager.getTypeToGenerate(column.getTalendType(), column.isNullable());
						JavaType javaType = JavaTypesManager.getJavaTypeFromId(column.getTalendType());
						String columnName = column.getLabel();
					
    stringBuffer.append(TEXT_105);
    stringBuffer.append(connTypeName);
    stringBuffer.append(TEXT_106);
    stringBuffer.append(columnName);
    stringBuffer.append(TEXT_107);
    stringBuffer.append(indexCol);
    stringBuffer.append(TEXT_108);
    
								if(javaType == JavaTypesManager.STRING) {
								
    stringBuffer.append(TEXT_109);
    stringBuffer.append(indexCol);
    stringBuffer.append(TEXT_110);
    
								} else if(javaType == JavaTypesManager.BYTE) {
								
    stringBuffer.append(TEXT_111);
    stringBuffer.append(indexCol);
    stringBuffer.append(TEXT_112);
    
								} else if(javaType == JavaTypesManager.SHORT) {
								
    stringBuffer.append(TEXT_113);
    stringBuffer.append(indexCol);
    stringBuffer.append(TEXT_114);
    
								} else if(javaType == JavaTypesManager.CHARACTER) {
								
    stringBuffer.append(TEXT_115);
    stringBuffer.append(indexCol);
    stringBuffer.append(TEXT_116);
    
								} else if(javaType == JavaTypesManager.DATE) {
								
    stringBuffer.append(TEXT_117);
    stringBuffer.append(indexCol);
    stringBuffer.append(TEXT_118);
    
								} else {
								
    stringBuffer.append(TEXT_119);
    stringBuffer.append(indexCol);
    stringBuffer.append(TEXT_120);
    
								}
								
    stringBuffer.append(TEXT_121);
    
					}
					
    stringBuffer.append(TEXT_122);
    
		}
	}
}

    stringBuffer.append(TEXT_123);
    return stringBuffer.toString();
  }
}
