package org.talend.designer.codegen.translators.common;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import org.talend.core.CorePlugin;
import org.talend.core.model.metadata.IMetadataColumn;
import org.talend.core.model.metadata.IMetadataTable;
import org.talend.core.model.metadata.types.JavaTypesManager;
import org.talend.core.model.process.ElementParameterParser;
import org.talend.core.model.process.IConnection;
import org.talend.core.model.process.IContextParameter;
import org.talend.core.model.process.INode;
import org.talend.core.model.process.IProcess;
import org.talend.core.model.process.ProcessUtils;
import org.talend.core.model.utils.JavaResourcesHelper;
import org.talend.designer.codegen.config.CodeGeneratorArgument;
import org.talend.designer.common.tsetkeystore.TSetKeystoreUtil;
import org.talend.designer.runprocess.ProcessorException;
import org.talend.designer.runprocess.ProcessorUtilities;

public class Spark_footerJava
{
  protected static String nl;
  public static synchronized Spark_footerJava create(String lineSeparator)
  {
    nl = lineSeparator;
    Spark_footerJava result = new Spark_footerJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "    throw new java.lang.RuntimeException(\"A Spark job can't have more than 1 generated tHadoopConfManager in the process.\");";
  protected final String TEXT_2 = NL + "    ";
  protected final String TEXT_3 = NL + "        throw new java.lang.RuntimeException(\"A Spark job can't have more than 1 tAzureFSConfiguration defined in the designer.\");";
  protected final String TEXT_4 = NL;
  protected final String TEXT_5 = NL + "        throw new java.lang.RuntimeException(\"A Spark job can't have more than 1 tGSConfiguration defined in the designer.\");";
  protected final String TEXT_6 = NL + "    ";
  protected final String TEXT_7 = NL + "    ";
  protected final String TEXT_8 = NL + "    throw new java.lang.RuntimeException(\"A Spark job can't have more than 1 tTachyonConfiguration defined in the designer.\");";
  protected final String TEXT_9 = NL + "    ";
  protected final String TEXT_10 = NL + "    throw new java.lang.RuntimeException(\"A Spark job can't have more than 1 tHDFSConfiguration defined in the designer.\");";
  protected final String TEXT_11 = NL + "    ";
  protected final String TEXT_12 = NL + "    throw new java.lang.RuntimeException(\"A Spark job can't have more than 1 tHBaseConfiguration defined in the designer.\");";
  protected final String TEXT_13 = NL + "    ";
  protected final String TEXT_14 = NL + "    throw new java.lang.RuntimeException(\"A Spark job can't have more than 1 tCassandraConfiguration defined in the designer.\");";
  protected final String TEXT_15 = NL + "    ";
  protected final String TEXT_16 = NL + "    throw new java.lang.RuntimeException(\"A Spark job can't have more than 1 tBigQueryConfiguration defined in the designer.\");";
  protected final String TEXT_17 = NL;
  protected final String TEXT_18 = NL + "public static class TalendPipelineModel implements java.io.Serializable {" + NL + "" + NL + "\tprivate static final long serialVersionUID = 1L;" + NL + "\t" + NL + "\tprivate org.apache.spark.ml.PipelineModel pipelineModel;" + NL + "\tprivate java.util.Map<String, String> params;" + NL + "\t" + NL + "\tpublic TalendPipelineModel(org.apache.spark.ml.PipelineModel pipelineModel) {" + NL + "\t\tthis.pipelineModel = pipelineModel;" + NL + "\t\tthis.params = new java.util.HashMap<String, String>();" + NL + "\t}" + NL + "\t" + NL + "\tpublic TalendPipelineModel(org.apache.spark.ml.PipelineModel pipelineModel, java.util.Map<String, String> params) {" + NL + "\t\tthis.pipelineModel = pipelineModel;" + NL + "\t\tthis.params = params;" + NL + "\t}" + NL + "\t" + NL + "\tpublic org.apache.spark.ml.PipelineModel getPipelineModel() {" + NL + "\t\treturn this.pipelineModel;" + NL + "\t}" + NL + "\t" + NL + "\tpublic java.util.Map<String, String> getParams() {" + NL + "\t\treturn this.params;" + NL + "\t}" + NL + "}  ";
  protected final String TEXT_19 = NL + "public static class TalendPipeline implements java.io.Serializable {" + NL + "" + NL + "\tprivate static final long serialVersionUID = 1L;" + NL + "\t" + NL + "\tprivate org.apache.spark.ml.Pipeline pipeline;" + NL + "\tprivate java.util.Map<String, String> params;" + NL + "\t" + NL + "\tpublic TalendPipeline(org.apache.spark.ml.Pipeline pipeline) {" + NL + "\t\tthis.pipeline = pipeline;" + NL + "\t\tthis.params = new java.util.HashMap<String, String>();" + NL + "\t}" + NL + "\t" + NL + "\tpublic TalendPipeline(org.apache.spark.ml.Pipeline pipeline, java.util.Map<String, String> params) {" + NL + "\t\tthis.pipeline = pipeline;" + NL + "\t\tthis.params = params;" + NL + "\t}" + NL + "\t" + NL + "\tpublic org.apache.spark.ml.Pipeline getPipeline() {" + NL + "\t\treturn this.pipeline;" + NL + "\t}" + NL + "\t" + NL + "\tpublic java.util.Map<String, String> getParams() {" + NL + "\t\treturn this.params;" + NL + "\t}" + NL + "}  ";
  protected final String TEXT_20 = NL;
  protected final String TEXT_21 = NL + "public static class TalendKryoRegistrator implements org.apache.spark.serializer.KryoRegistrator {" + NL + "\t\t" + NL + "\t@Override" + NL + "\tpublic void registerClasses(com.esotericsoftware.kryo.Kryo kryo) {" + NL + "\t\ttry {" + NL + "\t\t\tkryo.register(Class.forName(\"org.talend.bigdata.dataflow.keys.JoinKeyRecord\"));" + NL + "\t\t} catch (java.lang.ClassNotFoundException e) {" + NL + "\t\t\t// Ignore" + NL + "\t\t}" + NL + "\t\t\t\t\t";
  protected final String TEXT_22 = "\t\t" + NL + "\t\ttry {" + NL + "\t\t\tkryo.register(" + NL + "\t\t\t\tClass.forName(\"org.apache.avro.generic.GenericData$Record\"), " + NL + "\t\t\t\tnew org.talend.bigdata.dataflow.serializer.KryoAvroRecordSerializer());" + NL + "\t\t} catch (java.lang.ClassNotFoundException e) {" + NL + "\t\t\t// Ignore" + NL + "\t\t}";
  protected final String TEXT_23 = NL + NL + "\t\tkryo.register(java.net.InetAddress.class, new InetAddressSerializer());" + NL + "\t\tkryo.addDefaultSerializer(java.net.InetAddress.class, new InetAddressSerializer());" + NL + "\t";
  protected final String TEXT_24 = NL + "\t\t\tkryo.register(TalendPipelineModel.class, new TalendPipelineModelSerializer());" + NL + "\t\t\tkryo.addDefaultSerializer(TalendPipelineModel.class, new TalendPipelineModelSerializer());" + NL + "\t\t\tkryo.register(TalendPipeline.class, new TalendPipelineSerializer());" + NL + "\t\t\tkryo.addDefaultSerializer(TalendPipeline.class, new TalendPipelineSerializer());" + NL + "\t";
  protected final String TEXT_25 = "\t\t\t" + NL + "\t\tkryo.register(";
  protected final String TEXT_26 = ".class);" + NL + "\t";
  protected final String TEXT_27 = NL + "\t}" + NL + "}" + NL + "\t" + NL + "public static class InetAddressSerializer extends com.esotericsoftware.kryo.Serializer<java.net.InetAddress> {" + NL + "" + NL + "\t@Override" + NL + "\tpublic void write(com.esotericsoftware.kryo.Kryo kryo, com.esotericsoftware.kryo.io.Output output, java.net.InetAddress value) {" + NL + "\t\toutput.writeInt(value.getAddress().length);" + NL + "\t\toutput.writeBytes(value.getAddress());" + NL + "\t}" + NL + "" + NL + "\t@Override" + NL + "\tpublic java.net.InetAddress read(com.esotericsoftware.kryo.Kryo kryo, com.esotericsoftware.kryo.io.Input input, Class<java.net.InetAddress> paramClass) {" + NL + "\t\tjava.net.InetAddress inetAddress = null;" + NL + "\t\ttry {" + NL + "\t\t\tint length = input.readInt();" + NL + "\t\t\tbyte[] address = input.readBytes(length);" + NL + "\t\t\tinetAddress = java.net.InetAddress.getByAddress(address);" + NL + "\t\t} catch (java.net.UnknownHostException e) {" + NL + "\t\t\t// Cannot recreate InetAddress instance : return null" + NL + "\t\t} catch (com.esotericsoftware.kryo.KryoException e) {" + NL + "\t\t\t// Should not happen since write() and read() methods are consistent, but if it does happen, it is an unrecoverable error." + NL + "            throw new RuntimeException(e);" + NL + "\t\t}" + NL + "\t\treturn inetAddress;" + NL + "\t}" + NL + "}" + NL;
  protected final String TEXT_28 = NL + "public static class TalendPipelineModelSerializer extends com.esotericsoftware.kryo.Serializer<TalendPipelineModel> {" + NL + "" + NL + "\t@Override" + NL + "\tpublic void write(com.esotericsoftware.kryo.Kryo kryo, com.esotericsoftware.kryo.io.Output output, TalendPipelineModel value) {" + NL + "\t\tkryo.writeObject(output, value.getParams(), new com.esotericsoftware.kryo.serializers.MapSerializer());" + NL + "\t\tkryo.writeObject(output, value.getPipelineModel(), new TalendJavaSerializer());" + NL + "\t}" + NL + "" + NL + "\t@Override" + NL + "\tpublic TalendPipelineModel read(com.esotericsoftware.kryo.Kryo kryo, com.esotericsoftware.kryo.io.Input input, Class<TalendPipelineModel> paramClass) {" + NL + "\t\tTalendPipelineModel talendPipelineModel = null;" + NL + "\t\ttry {" + NL + "\t\t\t@SuppressWarnings(\"unchecked\")" + NL + "\t\t\tjava.util.Map<String, String> params = kryo.readObject(input, java.util.HashMap.class, new com.esotericsoftware.kryo.serializers.MapSerializer());" + NL + "\t\t\torg.apache.spark.ml.PipelineModel pipelineModel = kryo.readObject(input, org.apache.spark.ml.PipelineModel.class, new TalendJavaSerializer());" + NL + "\t\t\ttalendPipelineModel = new TalendPipelineModel(pipelineModel, params);" + NL + "\t\t} catch (com.esotericsoftware.kryo.KryoException e) {" + NL + "\t\t\t// Should not happen since write() and read() methods are consistent, but if it does happen, it is an unrecoverable error." + NL + "            throw new RuntimeException(e);" + NL + "\t\t}" + NL + "\t\treturn talendPipelineModel;" + NL + "\t}" + NL + "}" + NL + "" + NL + "" + NL + "public static class TalendPipelineSerializer extends com.esotericsoftware.kryo.Serializer<TalendPipeline> {" + NL + "" + NL + "    @Override" + NL + "    public void write(com.esotericsoftware.kryo.Kryo kryo, com.esotericsoftware.kryo.io.Output output, TalendPipeline value) {" + NL + "        kryo.writeObject(output, value.getParams(), new com.esotericsoftware.kryo.serializers.MapSerializer());" + NL + "        kryo.writeObject(output, value.getPipeline(), new TalendJavaSerializer());" + NL + "    }" + NL + "" + NL + "    @Override" + NL + "    public TalendPipeline read(com.esotericsoftware.kryo.Kryo kryo, com.esotericsoftware.kryo.io.Input input, Class<TalendPipeline> paramClass) {" + NL + "        TalendPipeline talendPipeline = null;" + NL + "        try {" + NL + "            @SuppressWarnings(\"unchecked\")" + NL + "            java.util.Map<String, String> params = kryo.readObject(input, java.util.HashMap.class, new com.esotericsoftware.kryo.serializers.MapSerializer());" + NL + "            org.apache.spark.ml.Pipeline pipeline = kryo.readObject(input, org.apache.spark.ml.Pipeline.class, new TalendJavaSerializer());" + NL + "            talendPipeline = new TalendPipeline(pipeline, params);" + NL + "        } catch (com.esotericsoftware.kryo.KryoException e) {" + NL + "\t\t\t// Should not happen since write() and read() methods are consistent, but if it does happen, it is an unrecoverable error." + NL + "            throw new RuntimeException(e);" + NL + "        }" + NL + "        return talendPipeline;" + NL + "    }" + NL + "}" + NL + "" + NL + "public static class TalendJavaSerializer extends com.esotericsoftware.kryo.Serializer{" + NL + "    " + NL + "    public void write (com.esotericsoftware.kryo.Kryo kryo, com.esotericsoftware.kryo.io.Output output, Object object) {" + NL + "        try {" + NL + "            com.esotericsoftware.kryo.util.ObjectMap graphContext = kryo.getGraphContext();" + NL + "            java.io.ObjectOutputStream objectStream = (java.io.ObjectOutputStream)graphContext.get(this);" + NL + "            if (objectStream == null) {" + NL + "                objectStream = new java.io.ObjectOutputStream(output);" + NL + "                graphContext.put(this, objectStream);" + NL + "            }" + NL + "            objectStream.writeObject(object);" + NL + "            objectStream.flush();" + NL + "        } catch (Exception ex) {" + NL + "            throw new com.esotericsoftware.kryo.KryoException(\"Error during Java serialization.\", ex);" + NL + "        }" + NL + "    }" + NL + "    " + NL + "    public Object read (com.esotericsoftware.kryo.Kryo kryo, com.esotericsoftware.kryo.io.Input input, Class type) {" + NL + "        try {" + NL + "            com.esotericsoftware.kryo.util.ObjectMap graphContext = kryo.getGraphContext();" + NL + "            ObjectInputStream objectStream = (ObjectInputStream)graphContext.get(this);" + NL + "            if (objectStream == null) {" + NL + "                objectStream = new ObjectInputStreamWithCurrentThreadClassLoader(input);" + NL + "                graphContext.put(this, objectStream);" + NL + "            }" + NL + "            return objectStream.readObject();" + NL + "        } catch (Exception ex) {" + NL + "            throw new com.esotericsoftware.kryo.KryoException(\"Error during Java deserialization.\", ex);" + NL + "        }" + NL + "    }" + NL + "    " + NL + "    private static class ObjectInputStreamWithCurrentThreadClassLoader extends ObjectInputStream {" + NL + "        private final ClassLoader loader;" + NL + "" + NL + "        ObjectInputStreamWithCurrentThreadClassLoader(java.io.InputStream in) throws IOException {" + NL + "            super(in);" + NL + "            this.loader = Thread.currentThread().getContextClassLoader();" + NL + "        }" + NL + "" + NL + "        @Override" + NL + "        protected Class<?> resolveClass(java.io.ObjectStreamClass desc) {" + NL + "            try {" + NL + "                return Class.forName(desc.getName(), true, loader);" + NL + "            } catch (ClassNotFoundException e) {" + NL + "                throw new RuntimeException(\"Class not found: \" + desc.getName(), e);" + NL + "            }" + NL + "        }" + NL + "    }" + NL + "}// end class TalendJavaSerializer" + NL;
  protected final String TEXT_29 = NL + NL + "    public String resuming_logs_dir_path = null;" + NL + "    public String resuming_checkpoint_path = null;" + NL + "    public String parent_part_launcher = null;" + NL + "    public String pid = \"0\";" + NL + "    public String rootPid = null;" + NL + "    public String fatherPid = null;" + NL + "    private boolean watch = false;" + NL + "    public Integer portStats = null;" + NL + "    public String clientHost;" + NL + "    public String defaultClientHost = \"localhost\";" + NL + "    public String libjars = null;" + NL + "    private boolean execStat = true;" + NL + "    public boolean isChildJob = false;" + NL + "    public String fatherNode = null;" + NL + "    public String log4jLevel = \"\";" + NL + "    private boolean doInspect = false;";
  protected final String TEXT_30 = NL + "        private java.util.List<String> cloudApiArgs = new java.util.ArrayList<>();" + NL + "        private static boolean isDriverCall = false;";
  protected final String TEXT_31 = NL + NL + "    public String contextStr = \"";
  protected final String TEXT_32 = "\";" + NL + "    public boolean isDefaultContext = true;" + NL + "" + NL + "    private java.util.Properties context_param = new java.util.Properties();" + NL + "    public java.util.Map<String, Object> parentContextMap = new java.util.HashMap<String, Object>();" + NL + "" + NL + "    public String status= \"\";" + NL + "" + NL + "    public static void main(String[] args) throws java.lang.RuntimeException {";
  protected final String TEXT_33 = " isDriverCall = isDriverCall(args);";
  protected final String TEXT_34 = NL + "        int exitCode = new ";
  protected final String TEXT_35 = "().runJobInTOS(args);";
  protected final String TEXT_36 = NL + "            if(exitCode == 0) {" + NL + "                log.info(\"TalendJob: '";
  protected final String TEXT_37 = "' - Done.\");" + NL + "            } else {" + NL + "                log.error(\"TalendJob: '";
  protected final String TEXT_38 = "' - Failed with exit code: \" + exitCode + \".\");" + NL + "            }";
  protected final String TEXT_39 = NL + "        if(exitCode == 0) {";
  protected final String TEXT_40 = " if(!isDriverCall) { ";
  protected final String TEXT_41 = NL + "            \tSystem.exit(exitCode);";
  protected final String TEXT_42 = " } ";
  protected final String TEXT_43 = NL + "        } else {" + NL + "            System.exit(exitCode);" + NL + "        }" + NL + "    }" + NL;
  protected final String TEXT_44 = NL + "            @Test" + NL + "            public void test";
  protected final String TEXT_45 = "() throws java.lang.Exception{";
  protected final String TEXT_46 = NL + "                if(";
  protected final String TEXT_47 = "<=0){" + NL + "                    throw new java.lang.Exception(\"There is no tCollectAndCheck in your test case!\");" + NL + "                }" + NL + "                junitGlobalMap.put(\"tests.log\",new String());" + NL + "                junitGlobalMap.put(\"tests.nbFailure\",new Integer(0));" + NL + "                final ";
  protected final String TEXT_48 = " ";
  protected final String TEXT_49 = "Class = new ";
  protected final String TEXT_50 = "();" + NL + "                java.util.List<String> paraList_";
  protected final String TEXT_51 = " = new java.util.ArrayList<String>();" + NL + "                paraList_";
  protected final String TEXT_52 = ".add(\"--context=";
  protected final String TEXT_53 = "\");";
  protected final String TEXT_54 = NL + "                        paraList_";
  protected final String TEXT_55 = ".add(\"--context_param\");" + NL + "                        paraList_";
  protected final String TEXT_56 = ".add(\"";
  protected final String TEXT_57 = "=\" + ";
  protected final String TEXT_58 = ");";
  protected final String TEXT_59 = NL + "                String[] arrays = new String[paraList_";
  protected final String TEXT_60 = ".size()];" + NL + "                for(int i=0;i<paraList_";
  protected final String TEXT_61 = ".size();i++){" + NL + "                    arrays[i] = (String)paraList_";
  protected final String TEXT_62 = ".get(i);" + NL + "                }";
  protected final String TEXT_63 = NL + "            ";
  protected final String TEXT_64 = "Class.runJobInTOS(arrays);" + NL + "" + NL + "            String errors = (String)junitGlobalMap.get(\"tests.log\");" + NL + "            Integer nbFailure = (Integer)junitGlobalMap.get(\"tests.nbFailure\");" + NL + "            assertTrue(\"Failure=\"+nbFailure+java.lang.System.getProperty(\"line.separator\")+errors, errors.isEmpty());" + NL + "" + NL + "            }";
  protected final String TEXT_65 = NL + NL + "    public String[][] runJob(String[] args){" + NL + "        int exitCode = runJobInTOS(args);" + NL + "        String[][] bufferValue = new String[][] { { Integer.toString(exitCode) } };" + NL + "        return bufferValue;" + NL + "    }" + NL + "" + NL + "    public int runJobInTOS (String[] args) {" + NL + "        normalizeArgs(args);" + NL + "" + NL + "        String lastStr = \"\";" + NL + "        for (String arg : args) {" + NL + "            if (arg.equalsIgnoreCase(\"--context_param\")) {" + NL + "                lastStr = arg;" + NL + "            } else if (lastStr.equals(\"\")) {" + NL + "                evalParam(arg);" + NL + "            } else {" + NL + "                evalParam(lastStr + \" \" + arg);" + NL + "                lastStr = \"\";" + NL + "            }" + NL + "        }" + NL;
  protected final String TEXT_66 = NL + "            if(!\"\".equals(log4jLevel)){" + NL + "                if(\"trace\".equalsIgnoreCase(log4jLevel)){" + NL + "                    log.setLevel(org.apache.log4j.Level.TRACE);" + NL + "                }else if(\"debug\".equalsIgnoreCase(log4jLevel)){" + NL + "                    log.setLevel(org.apache.log4j.Level.DEBUG);" + NL + "                }else if(\"info\".equalsIgnoreCase(log4jLevel)){" + NL + "                    log.setLevel(org.apache.log4j.Level.INFO);" + NL + "                }else if(\"warn\".equalsIgnoreCase(log4jLevel)){" + NL + "                    log.setLevel(org.apache.log4j.Level.WARN);" + NL + "                }else if(\"error\".equalsIgnoreCase(log4jLevel)){" + NL + "                    log.setLevel(org.apache.log4j.Level.ERROR);" + NL + "                }else if(\"fatal\".equalsIgnoreCase(log4jLevel)){" + NL + "                    log.setLevel(org.apache.log4j.Level.FATAL);" + NL + "                }else if (\"off\".equalsIgnoreCase(log4jLevel)){" + NL + "                    log.setLevel(org.apache.log4j.Level.OFF);" + NL + "                }" + NL + "                org.apache.log4j.Logger.getRootLogger().setLevel(log.getLevel());" + NL + "            }" + NL + "            log.info(\"TalendJob: '";
  protected final String TEXT_67 = "' - Start.\");";
  protected final String TEXT_68 = NL + NL + "        initContext();" + NL + "" + NL + "        if (clientHost == null) {" + NL + "            clientHost = defaultClientHost;" + NL + "        }" + NL + "" + NL + "        if (pid == null || \"0\".equals(pid)) {" + NL + "            pid = TalendString.getAsciiRandomString(6);" + NL + "        }" + NL + "" + NL + "        if (rootPid == null) {" + NL + "            rootPid = pid;" + NL + "        }" + NL + "        if (fatherPid == null) {" + NL + "            fatherPid = pid;" + NL + "        } else {" + NL + "            isChildJob = true;" + NL + "        }" + NL;
  protected final String TEXT_69 = NL + NL + "            if (portStats != null) {" + NL + "                // portStats = -1; //for testing" + NL + "                if (portStats < 0 || portStats > 65535) {" + NL + "                    // issue:10869, the portStats is invalid, so this client socket" + NL + "                    // can't open" + NL + "                    System.err.println(\"The statistics socket port \" + portStats" + NL + "                            + \" is invalid.\");" + NL + "                    execStat = false;" + NL + "                }" + NL + "            } else {" + NL + "                execStat = false;" + NL + "            }" + NL + NL;
  protected final String TEXT_70 = NL + "            if(!\"\".equals(";
  protected final String TEXT_71 = ")) {" + NL + "                System.setProperty(\"HADOOP_USER_NAME\", ";
  protected final String TEXT_72 = ");" + NL + "            }";
  protected final String TEXT_73 = NL + "        String osName = System.getProperty(\"os.name\");" + NL + "        String snappyLibName = \"libsnappyjava.so\";" + NL + "        if(osName.startsWith(\"Windows\")) {" + NL + "            snappyLibName = \"snappyjava.dll\";" + NL + "        } else if(osName.startsWith(\"Mac\")) {" + NL + "            snappyLibName = \"libsnappyjava.jnilib\";" + NL + "        }" + NL + "        System.setProperty(\"org.xerial.snappy.lib.name\", snappyLibName);" + NL + "        " + NL + "        long startTime = System.currentTimeMillis();";
  protected final String TEXT_74 = NL + "            try {";
  protected final String TEXT_75 = NL + "                    ";
  protected final String TEXT_76 = "Process();";
  protected final String TEXT_77 = NL + "                // Set job wide SSL settings before creating the Spark context" + NL + "                setupJobWideSSLConfigurations();" + NL + "                java.util.Map<String, String> tuningConf = new java.util.HashMap<>();" + NL + "                org.apache.spark.SparkConf sparkConfiguration = getConf(tuningConf);";
  protected final String TEXT_78 = NL + "                    return runSparkJobServerJob(sparkConfiguration, tuningConf);";
  protected final String TEXT_79 = NL + "                        if(!isDriverCall) {" + NL + "                            return runClientJob(sparkConfiguration, cloudApiArgs);" + NL + "                        } else {";
  protected final String TEXT_80 = NL + "                                try {" + NL + "                                    org.apache.hadoop.conf.Configuration hadoopConfiguration = org.apache.spark.deploy.SparkHadoopUtil.get().newConfiguration(sparkConfiguration);" + NL + "                                    org.apache.hadoop.security.UserGroupInformation.setConfiguration(hadoopConfiguration);" + NL + "                                    org.apache.hadoop.security.UserGroupInformation.getLoginUser();" + NL + "                                } catch (IOException ioe) {";
  protected final String TEXT_81 = NL + "                                        log.warn(ioe.getMessage());";
  protected final String TEXT_82 = NL + "                                }";
  protected final String TEXT_83 = NL + "                                org.apache.spark.sql.SparkSession ss = org.apache.spark.sql.SparkSession.builder().sparkContext(org.apache.spark.api.java.JavaSparkContext.fromSparkContext(org.apache.spark.SparkContext.getOrCreate(sparkConfiguration)).sc()).getOrCreate();";
  protected final String TEXT_84 = NL + "                                org.apache.spark.api.java.JavaSparkContext ctx = org.apache.spark.api.java.JavaSparkContext.fromSparkContext(org.apache.spark.SparkContext.getOrCreate(sparkConfiguration));";
  protected final String TEXT_85 = NL + "                            \torg.apache.spark.sql.SparkSession ss = org.apache.spark.sql.SparkSession.builder().sparkContext(new org.apache.spark.api.java.JavaSparkContext(sparkConfiguration).sc()).getOrCreate();" + NL + "                            \torg.apache.spark.api.java.JavaSparkContext ctx = org.apache.spark.api.java.JavaSparkContext.fromSparkContext(ss.sparkContext());";
  protected final String TEXT_86 = NL + "\t                            org.apache.spark.api.java.JavaSparkContext ctx = new org.apache.spark.api.java.JavaSparkContext(sparkConfiguration);";
  protected final String TEXT_87 = NL + "   \t\t\t\t\t\t\t\tif (execStat) {" + NL + "   \t\t\t\t\t\t\t        try {";
  protected final String TEXT_88 = NL + "   \t\t\t\t\t\t\t        \trunStat = new SparkTrackerRunStat(ctx.statusTracker(), java.lang.Integer.parseInt(";
  protected final String TEXT_89 = "));";
  protected final String TEXT_90 = NL + "   \t\t\t\t\t\t\t            runStat.startThreadStat(clientHost, portStats);" + NL + "   \t\t\t\t\t\t\t            runStat.setAllPID(rootPid, fatherPid, pid);" + NL + "   \t\t\t\t\t\t\t        } catch (java.io.IOException ioException) {" + NL + "   \t\t\t\t\t\t\t            ioException.printStackTrace();" + NL + "   \t\t\t\t\t\t\t        }" + NL + "   \t\t\t\t\t\t\t    }";
  protected final String TEXT_91 = NL + "                            \treturn run(ss);";
  protected final String TEXT_92 = NL + "\t                            return run(ctx);";
  protected final String TEXT_93 = NL + "                        } // end of else part of if(!isCloudDriver(args))";
  protected final String TEXT_94 = NL + "                            try {" + NL + "                                org.apache.hadoop.conf.Configuration hadoopConfiguration = org.apache.spark.deploy.SparkHadoopUtil.get().newConfiguration(sparkConfiguration);" + NL + "                                org.apache.hadoop.security.UserGroupInformation.setConfiguration(hadoopConfiguration);" + NL + "                                org.apache.hadoop.security.UserGroupInformation.getLoginUser();" + NL + "                            } catch (IOException ioe) {";
  protected final String TEXT_95 = NL + "                                    log.warn(ioe.getMessage());";
  protected final String TEXT_96 = NL + "                            }";
  protected final String TEXT_97 = NL + "                            org.apache.spark.sql.SparkSession ss = org.apache.spark.sql.SparkSession.builder().sparkContext(org.apache.spark.api.java.JavaSparkContext.fromSparkContext(org.apache.spark.SparkContext.getOrCreate(sparkConfiguration)).sc()).getOrCreate();";
  protected final String TEXT_98 = NL + "                            org.apache.spark.api.java.JavaSparkContext ctx = org.apache.spark.api.java.JavaSparkContext.fromSparkContext(org.apache.spark.SparkContext.getOrCreate(sparkConfiguration));";
  protected final String TEXT_99 = NL + "                            org.apache.spark.sql.SparkSession ss = org.apache.spark.sql.SparkSession.builder().sparkContext(new org.apache.spark.api.java.JavaSparkContext(sparkConfiguration).sc()).getOrCreate();" + NL + "                            org.apache.spark.api.java.JavaSparkContext ctx = org.apache.spark.api.java.JavaSparkContext.fromSparkContext(ss.sparkContext());";
  protected final String TEXT_100 = NL + "                            org.apache.spark.api.java.JavaSparkContext ctx = new org.apache.spark.api.java.JavaSparkContext(sparkConfiguration);";
  protected final String TEXT_101 = NL + "\t\t\t\t\t\t\tif (execStat) {" + NL + "\t\t\t\t\t\t        try {";
  protected final String TEXT_102 = NL + "\t\t\t\t\t\t        \trunStat = new SparkTrackerRunStat(ctx.statusTracker(), java.lang.Integer.parseInt(";
  protected final String TEXT_103 = "));";
  protected final String TEXT_104 = NL + "\t\t\t\t\t\t            runStat.startThreadStat(clientHost, portStats);" + NL + "\t\t\t\t\t\t            runStat.setAllPID(rootPid, fatherPid, pid);" + NL + "\t\t\t\t\t\t        } catch (java.io.IOException ioException) {" + NL + "\t\t\t\t\t\t            ioException.printStackTrace();" + NL + "\t\t\t\t\t\t        }" + NL + "\t\t\t\t\t\t    }";
  protected final String TEXT_105 = NL + "                 \t\t\treturn run(ss);";
  protected final String TEXT_106 = NL + "                    \t\treturn run(ctx);";
  protected final String TEXT_107 = NL + "            } catch(Exception ex) {" + NL + "                ex.printStackTrace();" + NL + "                return 1;" + NL + "            } finally {" + NL + "            \tif (watch) {" + NL + "            \t\tSystem.out.println(\"Spark job execution time: \" + (System.currentTimeMillis() - startTime) + \" milliseconds.\");" + NL + "            \t}" + NL + "            }";
  protected final String TEXT_108 = NL + "    }" + NL + NL;
  protected final String TEXT_109 = NL + "            final String clouderaManagerPassword = routines.system.PasswordEncryptUtil.decryptPassword(";
  protected final String TEXT_110 = ");";
  protected final String TEXT_111 = NL + "            final String clouderaManagerPassword = ";
  protected final String TEXT_112 = ";";
  protected final String TEXT_113 = NL + "        this.lineageCreator = new org.talend.lineage.cloudera.LineageCreator(";
  protected final String TEXT_114 = NL + "                ";
  protected final String TEXT_115 = ",";
  protected final String TEXT_116 = NL + "                ";
  protected final String TEXT_117 = ",";
  protected final String TEXT_118 = NL + "                ";
  protected final String TEXT_119 = ",";
  protected final String TEXT_120 = NL + "                ";
  protected final String TEXT_121 = "," + NL + "                clouderaManagerPassword," + NL + "                jobName + \"_\" + jobVersion.replace(\".\", \"_\")," + NL + "                projectName,";
  protected final String TEXT_122 = NL + "                ";
  protected final String TEXT_123 = ",";
  protected final String TEXT_124 = NL + "                ";
  protected final String TEXT_125 = ",";
  protected final String TEXT_126 = NL + "                ";
  protected final String TEXT_127 = ");";
  protected final String TEXT_128 = NL + "            System.setProperty(\"atlas.conf\", ";
  protected final String TEXT_129 = ");";
  protected final String TEXT_130 = NL + "                String decryptedAtlasUserPassword = routines.system.PasswordEncryptUtil.decryptPassword(";
  protected final String TEXT_131 = ");";
  protected final String TEXT_132 = NL + "                String decryptedAtlasUserPassword = ";
  protected final String TEXT_133 = ";";
  protected final String TEXT_134 = NL + "            this.lineageCreator = new org.talend.lineage.atlas.AtlasLineageCreator(";
  protected final String TEXT_135 = ", ";
  protected final String TEXT_136 = ", decryptedAtlasUserPassword);";
  protected final String TEXT_137 = NL + "            this.lineageCreator = new org.talend.lineage.atlas.AtlasLineageCreator(";
  protected final String TEXT_138 = ");";
  protected final String TEXT_139 = NL + "        java.util.Map<String, Object> lineageCreatorJobMetadata = new java.util.HashMap<String, Object>();" + NL + "        lineageCreatorJobMetadata.put(\"name\", jobName);" + NL + "        lineageCreatorJobMetadata.put(\"description\", jobName);" + NL + "        lineageCreatorJobMetadata.put(\"purpose\", \"Talend BigData Job\");" + NL + "        lineageCreatorJobMetadata.put(\"author\", System.getProperty(\"user.name\"));" + NL + "        lineageCreatorJobMetadata.put(\"version\", jobVersion);" + NL + "        lineageCreatorJobMetadata.put(\"jobType\", \"Talend BigData Job\");" + NL + "        lineageCreatorJobMetadata.put(\"framework\", \"Talend BigData\");" + NL + "        lineageCreatorJobMetadata.put(\"status\", \"FINISHED\");" + NL + "        lineageCreatorJobMetadata.put(\"creationDate\", System.currentTimeMillis());" + NL + "        lineageCreatorJobMetadata.put(\"lastModificationDate\", System.currentTimeMillis());" + NL + "        lineageCreatorJobMetadata.put(\"startTime\", System.currentTimeMillis());" + NL + "        lineageCreatorJobMetadata.put(\"endTime\", System.currentTimeMillis());" + NL + "" + NL + "        this.lineageCreator.addJobInfo(lineageCreatorJobMetadata);";
  protected final String TEXT_140 = NL + "        java.util.Map<String, String> columnsLineage_";
  protected final String TEXT_141 = " = new java.util.HashMap<>();";
  protected final String TEXT_142 = NL + "            columnsLineage_";
  protected final String TEXT_143 = ".put(\"";
  protected final String TEXT_144 = "\", \"";
  protected final String TEXT_145 = "\");";
  protected final String TEXT_146 = NL + "        java.util.Map<String, String> columnsLineage_";
  protected final String TEXT_147 = " = new java.util.HashMap<>();";
  protected final String TEXT_148 = NL + "                        columnsLineage_";
  protected final String TEXT_149 = ".put(\"";
  protected final String TEXT_150 = "\", \"";
  protected final String TEXT_151 = "\");";
  protected final String TEXT_152 = NL + "                    columnsLineage_";
  protected final String TEXT_153 = ".put(\"";
  protected final String TEXT_154 = "\", \"";
  protected final String TEXT_155 = "\");";
  protected final String TEXT_156 = NL + "        lineageCreator.addDataset(columnsLineage_";
  protected final String TEXT_157 = ", \"";
  protected final String TEXT_158 = "\", ";
  protected final String TEXT_159 = ", \"";
  protected final String TEXT_160 = "\");";
  protected final String TEXT_161 = NL + "        java.util.List<String> inputNodes_";
  protected final String TEXT_162 = " = new java.util.ArrayList<String>();";
  protected final String TEXT_163 = NL + "                inputNodes_";
  protected final String TEXT_164 = ".add(\"";
  protected final String TEXT_165 = "\");";
  protected final String TEXT_166 = NL + "        java.util.List<String> outputNodes_";
  protected final String TEXT_167 = " = new java.util.ArrayList<String>();";
  protected final String TEXT_168 = NL + "                outputNodes_";
  protected final String TEXT_169 = ".add(\"";
  protected final String TEXT_170 = "\");";
  protected final String TEXT_171 = NL + "        this.lineageCreator.addNodeToLineage(\"";
  protected final String TEXT_172 = "\", columnsLineage_";
  protected final String TEXT_173 = ", inputNodes_";
  protected final String TEXT_174 = ", outputNodes_";
  protected final String TEXT_175 = ", new java.util.HashMap<String, Object>());";
  protected final String TEXT_176 = NL + "        org.talend.lineage.common.ILineageCreator lineageCreator;";
  protected final String TEXT_177 = NL + NL + "        /**" + NL + "         *" + NL + "         * This method runs the Spark job using the SparkSession in argument." + NL + "         * @param ss, the SparkSession." + NL + "         * @return the Spark job exit code." + NL + "         *" + NL + "        */" + NL + "        private int run(org.apache.spark.sql.SparkSession ss) throws java.lang.Exception {" + NL + "        \torg.apache.spark.api.java.JavaSparkContext ctx = org.apache.spark.api.java.JavaSparkContext.fromSparkContext(ss.sparkContext());" + NL + "    \t";
  protected final String TEXT_178 = NL + NL + "        /**" + NL + "         *" + NL + "         * This method runs the Spark job using the SparkContext in argument." + NL + "         * @param ctx, the SparkContext." + NL + "         * @return the Spark job exit code." + NL + "         *" + NL + "        */" + NL + "        private int run(org.apache.spark.api.java.JavaSparkContext ctx) throws java.lang.Exception {" + NL + "    \t";
  protected final String TEXT_179 = NL + "            ctx.sc().setCheckpointDir(";
  protected final String TEXT_180 = ");";
  protected final String TEXT_181 = NL + "                System.setProperty(\"pname\", \"MapRLogin\");" + NL + "                System.setProperty(\"https.protocols\", \"TLSv1.2\");" + NL + "                System.setProperty(\"mapr.home.dir\", ";
  protected final String TEXT_182 = ");" + NL + "                System.setProperty(\"hadoop.login\", ";
  protected final String TEXT_183 = ");";
  protected final String TEXT_184 = NL + "                org.apache.hadoop.security.UserGroupInformation.loginUserFromKeytab(";
  protected final String TEXT_185 = ", ";
  protected final String TEXT_186 = ");";
  protected final String TEXT_187 = NL + "                com.mapr.login.client.MapRLoginHttpsClient maprLogin = new com.mapr.login.client.MapRLoginHttpsClient();" + NL + "                maprLogin.getMapRCredentialsViaKerberos(";
  protected final String TEXT_188 = ", ";
  protected final String TEXT_189 = ");";
  protected final String TEXT_190 = NL + "                System.setProperty(\"pname\", \"MapRLogin\");" + NL + "                System.setProperty(\"https.protocols\", \"TLSv1.2\");" + NL + "                System.setProperty(\"mapr.home.dir\", ";
  protected final String TEXT_191 = ");" + NL + "                com.mapr.login.client.MapRLoginHttpsClient maprLogin = new com.mapr.login.client.MapRLoginHttpsClient();";
  protected final String TEXT_192 = NL + "                    System.setProperty(\"hadoop.login\", ";
  protected final String TEXT_193 = ");";
  protected final String TEXT_194 = NL + "                    maprLogin.setCheckUGI(false);";
  protected final String TEXT_195 = " " + NL + "\tfinal String decryptedPassword_";
  protected final String TEXT_196 = " = routines.system.PasswordEncryptUtil.decryptPassword(";
  protected final String TEXT_197 = ");";
  protected final String TEXT_198 = NL + "\tfinal String decryptedPassword_";
  protected final String TEXT_199 = " = ";
  protected final String TEXT_200 = "; ";
  protected final String TEXT_201 = NL;
  protected final String TEXT_202 = NL + "                    maprLogin.getMapRCredentialsViaPassword(";
  protected final String TEXT_203 = ", ";
  protected final String TEXT_204 = ", decryptedPassword_";
  protected final String TEXT_205 = ", ";
  protected final String TEXT_206 = ", \"\");";
  protected final String TEXT_207 = NL + "                    maprLogin.getMapRCredentialsViaPassword(";
  protected final String TEXT_208 = ", ";
  protected final String TEXT_209 = ", decryptedPassword_";
  protected final String TEXT_210 = ", ";
  protected final String TEXT_211 = ");";
  protected final String TEXT_212 = NL + "            ctx.sc().addSparkListener(new TalendRuntimeSparkListener(ctx.getConf()));";
  protected final String TEXT_213 = NL + "            ctx.sc().addSparkListener(new TalendEndOfRunSparkListener(jobName));";
  protected final String TEXT_214 = NL + "        ctx.setJobGroup(projectName + \"_\" + jobName + \"_\" + Thread.currentThread().getId(), \"\");" + NL + "" + NL + "        initContext();" + NL + "" + NL + "        setContext(ctx.hadoopConfiguration(), ctx);" + NL + "" + NL + "        if (doInspect) {" + NL + "            System.out.println(\"== inspect start ==\");" + NL + "            System.out.println(\"{\");" + NL + "            System.out.println(\"  \\\"SPARK_MASTER\\\": \\\"\" + ctx.getConf().get(\"spark.master\") + \"\\\",\");" + NL + "            System.out.println(\"  \\\"SPARK_UI_PORT\\\": \\\"\" + ctx.getConf().get(\"spark.ui.port\", \"4040\") + \"\\\",\");" + NL + "            System.out.println(\"  \\\"JOB_NAME\\\": \\\"\" + ctx.getConf().get(\"spark.app.name\", jobName) + \"\\\"\");" + NL + "            System.out.println(\"}\"); //$NON-NLS-1$" + NL + "            System.out.println(\"== inspect end ==\");" + NL + "        }";
  protected final String TEXT_215 = NL;
  protected final String TEXT_216 = NL + "                if(log.is";
  protected final String TEXT_217 = "Enabled())";
  protected final String TEXT_218 = NL + "            log.";
  protected final String TEXT_219 = "(\"";
  protected final String TEXT_220 = " - \" ";
  protected final String TEXT_221 = " + (";
  protected final String TEXT_222 = ") ";
  protected final String TEXT_223 = ");";
  protected final String TEXT_224 = NL + "            if (log.isDebugEnabled()) {" + NL + "                class BytesLimit65535_";
  protected final String TEXT_225 = "{" + NL + "                    public void limitLog4jByte() throws Exception{" + NL + "                    StringBuilder ";
  protected final String TEXT_226 = " = new StringBuilder();";
  protected final String TEXT_227 = NL + "                    ";
  protected final String TEXT_228 = ".append(\"Parameters:\");";
  protected final String TEXT_229 = NL + "                            ";
  protected final String TEXT_230 = ".append(\"";
  protected final String TEXT_231 = "\" + \" = \" + String.valueOf(";
  protected final String TEXT_232 = ").substring(0, 4) + \"...\");     ";
  protected final String TEXT_233 = NL + "                            ";
  protected final String TEXT_234 = ".append(\"";
  protected final String TEXT_235 = "\" + \" = \" + ";
  protected final String TEXT_236 = ");";
  protected final String TEXT_237 = NL + "                        ";
  protected final String TEXT_238 = ".append(\" | \");";
  protected final String TEXT_239 = NL + "                    } " + NL + "                } " + NL + "            new BytesLimit65535_";
  protected final String TEXT_240 = "().limitLog4jByte();" + NL + "            }";
  protected final String TEXT_241 = NL + "            StringBuilder ";
  protected final String TEXT_242 = " = new StringBuilder();    ";
  protected final String TEXT_243 = NL + "                    ";
  protected final String TEXT_244 = ".append(";
  protected final String TEXT_245 = ".";
  protected final String TEXT_246 = ");";
  protected final String TEXT_247 = NL + "                    if(";
  protected final String TEXT_248 = ".";
  protected final String TEXT_249 = " == null){";
  protected final String TEXT_250 = NL + "                        ";
  protected final String TEXT_251 = ".append(\"<null>\");" + NL + "                    }else{";
  protected final String TEXT_252 = NL + "                        ";
  protected final String TEXT_253 = ".append(";
  protected final String TEXT_254 = ".";
  protected final String TEXT_255 = ");" + NL + "                    }   ";
  protected final String TEXT_256 = NL + "                ";
  protected final String TEXT_257 = ".append(\"|\");";
  protected final String TEXT_258 = NL + "                    log.info(\"A <";
  protected final String TEXT_259 = "> configuration component has been found.\");";
  protected final String TEXT_260 = NL + "                try {" + NL + "                    globalMap = new GlobalVar(ctx.hadoopConfiguration());";
  protected final String TEXT_261 = NL + "                    ";
  protected final String TEXT_262 = "Process(";
  protected final String TEXT_263 = ", globalMap);" + NL + "                } catch(Exception e) {" + NL + "                    e.printStackTrace();" + NL + "                    e.printStackTrace(errorMessagePS);" + NL + "                    throw e;" + NL + "                } finally {";
  protected final String TEXT_264 = NL + "            \t\t\tThread.sleep(java.lang.Integer.parseInt(";
  protected final String TEXT_265 = "));";
  protected final String TEXT_266 = NL + "                \tctx.cancelJobGroup(projectName + \"_\" + jobName + \"_\" + Thread.currentThread().getId());";
  protected final String TEXT_267 = NL + "                        if (execStat) {" + NL + "                            runStat.stopThreadStat();" + NL + "                        }";
  protected final String TEXT_268 = NL + "                    ctx.stop();";
  protected final String TEXT_269 = NL + "                }" + NL;
  protected final String TEXT_270 = NL + "                    if ((junitGlobalMap.get(\"tests.nbFailure\") != null)" + NL + "                            && (((Integer)junitGlobalMap.get(\"tests.nbFailure\")) > 0)) {" + NL + "                        Integer failedTest = (Integer) junitGlobalMap.get(\"tests.nbFailure\");" + NL + "                        String logs = (String) junitGlobalMap.get(\"tests.log\");" + NL + "                        if (failedTest == 1) {" + NL + "                            throw new RuntimeException(\"1 test failed:\" + logs);" + NL + "                        } else {" + NL + "                            throw new RuntimeException(failedTest + \" tests failed:\" + logs);" + NL + "                        }" + NL + "                    }";
  protected final String TEXT_271 = NL + "            lineageCreator.sendToLineageProvider(";
  protected final String TEXT_272 = ");";
  protected final String TEXT_273 = NL + "        return 0;" + NL + "    }" + NL + "" + NL + "    /**" + NL + "     *" + NL + "     * This method has the responsibility to return a Spark configuration for the Spark job to run." + NL + "     * @return a Spark configuration." + NL + "     *" + NL + "     */" + NL + "    private org.apache.spark.SparkConf getConf(java.util.Map<String, String> tuningConf) throws java.lang.Exception {" + NL + "        org.apache.spark.SparkConf sparkConfiguration = new org.apache.spark.SparkConf();" + NL + "" + NL + "        sparkConfiguration.setAppName(projectName + \"_\" + jobName + \"_\" + jobVersion);" + NL + "        sparkConfiguration.set(\"spark.serializer\", \"org.apache.spark.serializer.KryoSerializer\");" + NL + "        sparkConfiguration.set(\"spark.kryo.registrator\", TalendKryoRegistrator.class.getName());";
  protected final String TEXT_274 = " sparkConfiguration.set(\"spark.yarn.submit.waitAppCompletion\", ";
  protected final String TEXT_275 = "\"true\"";
  protected final String TEXT_276 = "\"false\"";
  protected final String TEXT_277 = " ); ";
  protected final String TEXT_278 = NL + "            sparkConfiguration.setMaster(";
  protected final String TEXT_279 = ");" + NL + "" + NL + "            routines.system.GetJarsToRegister getJarsToRegister = new routines.system.GetJarsToRegister();" + NL + "            java.util.List<String> listJar = new java.util.ArrayList<String>();" + NL + "            String libJarUriPrefix = getLibJarURIPrefix();" + NL + "            if(libjars != null) {" + NL + "                for(String jar:libjars.split(\",\")) {";
  protected final String TEXT_280 = NL + "                        if(!jar.contains(\"";
  protected final String TEXT_281 = "\")) {";
  protected final String TEXT_282 = NL + "                            listJar.add(getJarsToRegister.replaceJarPaths(jar, libJarUriPrefix, true));";
  protected final String TEXT_283 = NL + "                        }";
  protected final String TEXT_284 = NL + "                }" + NL + "            }" + NL;
  protected final String TEXT_285 = NL + "                sparkConfiguration.set(\"spark.submit.deployMode\", ";
  protected final String TEXT_286 = ");" + NL + "                if (listJar != null) {" + NL + "                    String jarsLine = \"\";" + NL + "" + NL + "                    for (String jarPath: listJar)" + NL + "                      jarsLine=jarsLine+','+jarPath;" + NL + "" + NL + "                    jarsLine = (jarsLine.length()>0) ? jarsLine.substring(1): jarsLine;";
  protected final String TEXT_287 = NL + "                                log.info(\"Got \" + jarsLine.split(\",\").length + \" Spark libjars\");" + NL + "                                log.info(\"Spark libjars : \" + jarsLine);";
  protected final String TEXT_288 = NL + "                    sparkConfiguration.set(\"spark.yarn.jars\", jarsLine);" + NL + "                }";
  protected final String TEXT_289 = NL;
  protected final String TEXT_290 = NL + "\t\t\t\troutines.system.BigDataUtil.installWinutils(";
  protected final String TEXT_291 = ", getJarsToRegister.replaceJarPaths(\"";
  protected final String TEXT_292 = "\"));";
  protected final String TEXT_293 = NL + "                    String spark2JarsPaths = \"";
  protected final String TEXT_294 = "\";";
  protected final String TEXT_295 = NL + "                            log.info(\"Got \" + spark2JarsPaths.split(\",\").length + \" Spark jars\");" + NL + "                            log.info(\"Spark jars local paths = \" + spark2JarsPaths);";
  protected final String TEXT_296 = NL + "                    java.util.List<String> spark2JarsPathsList = java.util.Arrays.asList(spark2JarsPaths.split(\",\"));" + NL + "                    java.util.List<String> runtimeSpark2JarsPathsList = new java.util.ArrayList<String>();" + NL + "                    for(String sparkJarpath : spark2JarsPathsList){" + NL + "                        runtimeSpark2JarsPathsList.add(getJarsToRegister.replaceJarPaths(sparkJarpath, libJarUriPrefix));" + NL + "                    }";
  protected final String TEXT_297 = NL + "                            log.info(\"Runtime Spark jars = \" + runtimeSpark2JarsPathsList.toString());" + NL + "                            log.info(\"Got \" + runtimeSpark2JarsPathsList.size() + \" Runtime Spark jars\");";
  protected final String TEXT_298 = NL + "                    String allRuntimeSparkJarsPaths = org.apache.commons.lang3.StringUtils.join(runtimeSpark2JarsPathsList, \",\");" + NL + "                    if (sparkConfiguration.contains(\"spark.yarn.jars\")){" + NL + "                        sparkConfiguration.set(\"spark.yarn.jars\", allRuntimeSparkJarsPaths + \",\" + sparkConfiguration.get(\"spark.yarn.jars\"));" + NL + "                    } else {" + NL + "                        sparkConfiguration.set(\"spark.yarn.jars\", allRuntimeSparkJarsPaths);" + NL + "                    }";
  protected final String TEXT_299 = NL + "            sparkConfiguration.setJars(listJar.toArray(new String[listJar.size()]));";
  protected final String TEXT_300 = NL + "                   sparkConfiguration.set(\"spark.driver.host\", ";
  protected final String TEXT_301 = ");" + NL + "                   org.apache.spark.util.Utils.setCustomHostname(";
  protected final String TEXT_302 = ");";
  protected final String TEXT_303 = NL + "                sparkConfiguration.setSparkHome(";
  protected final String TEXT_304 = ");";
  protected final String TEXT_305 = NL + NL + "                sparkConfiguration.set(\"spark.hadoop.yarn.application.classpath\", \"";
  protected final String TEXT_306 = "\");" + NL + "                sparkConfiguration.set(\"spark.hadoop.yarn.resourcemanager.address\", ";
  protected final String TEXT_307 = ");";
  protected final String TEXT_308 = "sparkConfiguration.set(\"spark.hadoop.yarn.resourcemanager.scheduler.address\", ";
  protected final String TEXT_309 = ");";
  protected final String TEXT_310 = "sparkConfiguration.set(\"spark.hadoop.mapreduce.jobhistory.address\", ";
  protected final String TEXT_311 = ");";
  protected final String TEXT_312 = "sparkConfiguration.set(\"spark.hadoop.yarn.app.mapreduce.am.staging-dir\", ";
  protected final String TEXT_313 = ");";
  protected final String TEXT_314 = NL + "                    sparkConfiguration.set(\"spark.hadoop.mapreduce.map.memory.mb\", ";
  protected final String TEXT_315 = ");" + NL + "                    sparkConfiguration.set(\"spark.hadoop.mapreduce.reduce.memory.mb\", ";
  protected final String TEXT_316 = ");" + NL + "                    sparkConfiguration.set(\"spark.hadoop.yarn.app.mapreduce.am.resource.mb\", ";
  protected final String TEXT_317 = ");";
  protected final String TEXT_318 = NL + "                    sparkConfiguration.set(\"spark.hadoop.yarn.resourcemanager.principal\", ";
  protected final String TEXT_319 = ");" + NL + "                    sparkConfiguration.set(\"spark.hadoop.mapreduce.jobhistory.principal\", ";
  protected final String TEXT_320 = ");";
  protected final String TEXT_321 = NL + "                        System.setProperty(\"pname\", \"MapRLogin\");" + NL + "                        System.setProperty(\"https.protocols\", \"TLSv1.2\");" + NL + "                        System.setProperty(\"mapr.home.dir\", ";
  protected final String TEXT_322 = ");" + NL + "                        System.setProperty(\"hadoop.login\", ";
  protected final String TEXT_323 = ");";
  protected final String TEXT_324 = NL + "                            if(!isDriverCall){";
  protected final String TEXT_325 = NL + "                                org.apache.hadoop.security.UserGroupInformation.loginUserFromKeytab(";
  protected final String TEXT_326 = ", ";
  protected final String TEXT_327 = ");";
  protected final String TEXT_328 = NL + "                                sparkConfiguration.set(\"spark.yarn.keytab\", getLibJarURIPrefix() + ";
  protected final String TEXT_329 = ");" + NL + "                                sparkConfiguration.set(\"spark.yarn.principal\", ";
  protected final String TEXT_330 = ");" + NL + "                            }";
  protected final String TEXT_331 = NL + "                        com.mapr.login.client.MapRLoginHttpsClient maprLogin = new com.mapr.login.client.MapRLoginHttpsClient();" + NL + "                        maprLogin.getMapRCredentialsViaKerberos(";
  protected final String TEXT_332 = ", ";
  protected final String TEXT_333 = ");";
  protected final String TEXT_334 = NL + "                        System.setProperty(\"pname\", \"MapRLogin\");" + NL + "                        System.setProperty(\"https.protocols\", \"TLSv1.2\");" + NL + "                        System.setProperty(\"mapr.home.dir\", ";
  protected final String TEXT_335 = ");" + NL + "                        com.mapr.login.client.MapRLoginHttpsClient maprLogin = new com.mapr.login.client.MapRLoginHttpsClient();";
  protected final String TEXT_336 = NL + "                            System.setProperty(\"hadoop.login\", ";
  protected final String TEXT_337 = ");";
  protected final String TEXT_338 = NL + "                            maprLogin.setCheckUGI(false);";
  protected final String TEXT_339 = " " + NL + "\tfinal String decryptedPassword_";
  protected final String TEXT_340 = " = routines.system.PasswordEncryptUtil.decryptPassword(";
  protected final String TEXT_341 = ");";
  protected final String TEXT_342 = NL + "\tfinal String decryptedPassword_";
  protected final String TEXT_343 = " = ";
  protected final String TEXT_344 = "; ";
  protected final String TEXT_345 = NL + "                            maprLogin.getMapRCredentialsViaPassword(";
  protected final String TEXT_346 = ", ";
  protected final String TEXT_347 = ", decryptedPassword_";
  protected final String TEXT_348 = ", ";
  protected final String TEXT_349 = ", \"\");";
  protected final String TEXT_350 = NL + "                            maprLogin.getMapRCredentialsViaPassword(";
  protected final String TEXT_351 = ", ";
  protected final String TEXT_352 = ", decryptedPassword_";
  protected final String TEXT_353 = ", ";
  protected final String TEXT_354 = ");";
  protected final String TEXT_355 = NL + "                            if(!";
  protected final String TEXT_356 = ".equals(";
  protected final String TEXT_357 = ")) {" + NL + "                                throw new RuntimeException(\"The HDFS and the Spark configurations must have the same user name.\");" + NL + "                            }";
  protected final String TEXT_358 = NL + "                            if(!\"\".equals(";
  protected final String TEXT_359 = ")) {" + NL + "                                System.setProperty(\"HADOOP_USER_NAME\", ";
  protected final String TEXT_360 = ");" + NL + "                            }";
  protected final String TEXT_361 = NL + "            System.setProperty(\"hadoop.home.dir\", ";
  protected final String TEXT_362 = ");";
  protected final String TEXT_363 = NL + "                tuningConf.put(\"spark.ui.port\", ";
  protected final String TEXT_364 = ");";
  protected final String TEXT_365 = NL + "            tuningConf.put(\"spark.executor.memory\", ";
  protected final String TEXT_366 = ");";
  protected final String TEXT_367 = NL + "                tuningConf.put(\"spark.yarn.executor.memoryOverhead\", ";
  protected final String TEXT_368 = ");";
  protected final String TEXT_369 = NL + "                tuningConf.put(\"spark.driver.cores\", ";
  protected final String TEXT_370 = ");" + NL + "                tuningConf.put(\"spark.driver.memory\", ";
  protected final String TEXT_371 = ");";
  protected final String TEXT_372 = NL + "                    tuningConf.put(\"spark.yarn.am.cores\", ";
  protected final String TEXT_373 = ");" + NL + "                    tuningConf.put(\"spark.yarn.am.memory\", ";
  protected final String TEXT_374 = ");";
  protected final String TEXT_375 = NL + "                tuningConf.put(\"spark.executor.cores\", ";
  protected final String TEXT_376 = ");";
  protected final String TEXT_377 = NL + "                    tuningConf.put(\"spark.executor.instances\", ";
  protected final String TEXT_378 = ");";
  protected final String TEXT_379 = NL + "                    tuningConf.put(\"spark.dynamicAllocation.enabled\", \"true\");" + NL + "                    tuningConf.put(\"spark.shuffle.service.enabled\", \"true\");" + NL + "                    String dynInitialValue = ";
  protected final String TEXT_380 = ";" + NL + "                    tuningConf.put(\"spark.dynamicAllocation.initialExecutors\", dynInitialValue);" + NL + "                    String dynMinValue = ";
  protected final String TEXT_381 = ";" + NL + "                    tuningConf.put(\"spark.dynamicAllocation.minExecutors\", dynMinValue);";
  protected final String TEXT_382 = NL + "                        Integer iDynMaxValue = Integer.MAX_VALUE;" + NL + "                        tuningConf.put(\"spark.dynamicAllocation.maxExecutors\", new Integer(Integer.MAX_VALUE).toString());";
  protected final String TEXT_383 = NL + "                        Integer iDynMaxValue = Integer.parseInt(";
  protected final String TEXT_384 = ");" + NL + "                        tuningConf.put(\"spark.dynamicAllocation.maxExecutors\", ";
  protected final String TEXT_385 = ");";
  protected final String TEXT_386 = NL + "                    Integer iDynInitialValue = Integer.parseInt(dynInitialValue);" + NL + "                    Integer iDynMinValue = Integer.parseInt(dynMinValue);" + NL + "                    if(iDynInitialValue < iDynMinValue|| iDynInitialValue > iDynMaxValue || iDynMinValue > iDynMaxValue) {" + NL + "                        throw new RuntimeException(\"Please check your dynamicAllocation bounds, you should have min <= initial <= max\");" + NL + "                    }";
  protected final String TEXT_387 = NL + "                    tuningConf.put(\"spark.broadcast.factory\", \"org.apache.spark.broadcast.TorrentBroadcastFactory\");";
  protected final String TEXT_388 = NL + "                    tuningConf.put(\"spark.broadcast.factory\", \"org.apache.spark.broadcast.HttpBroadcastFactory\");";
  protected final String TEXT_389 = NL + "                tuningConf.put(\"spark.serializer\", ";
  protected final String TEXT_390 = ");";
  protected final String TEXT_391 = NL + "            tuningConf.put(\"spark.eventLog.enabled\",\"true\");";
  protected final String TEXT_392 = NL + "                tuningConf.put(\"spark.eventLog.compress\",\"true\");";
  protected final String TEXT_393 = NL + "            tuningConf.put(\"spark.eventLog.dir\",";
  protected final String TEXT_394 = ");" + NL + "            tuningConf.put(\"spark.yarn.historyServer.address\",";
  protected final String TEXT_395 = ");";
  protected final String TEXT_396 = NL + "            sparkConfiguration.set(\"spark.sql.warehouse.dir\", \"file:///\" +  ";
  protected final String TEXT_397 = " + \"/spark-warehouse\");";
  protected final String TEXT_398 = NL + "  ";
  protected final String TEXT_399 = NL + "    throw new java.lang.RuntimeException(\"A Spark job can't have more than 1 tS3Configuration defined in the designer.\");";
  protected final String TEXT_400 = NL + "            log.info(\"STS params: s3AccessKey=\" + ";
  protected final String TEXT_401 = " + \", assumeRole=\" +";
  protected final String TEXT_402 = " + " + NL + "                \", roleSessionName=\" + ";
  protected final String TEXT_403 = " + \", roleSessionDuration=\" + ";
  protected final String TEXT_404 = " + " + NL + "                \", externalID=\" + ";
  protected final String TEXT_405 = " + \", arn=\" + ";
  protected final String TEXT_406 = " + \", setStsRegion=\" + ";
  protected final String TEXT_407 = " +" + NL + "                \", stsRegion=\" + ";
  protected final String TEXT_408 = " + \", setsStsEndpoint=\" + ";
  protected final String TEXT_409 = " + \", stsEndpoint=\" +";
  protected final String TEXT_410 = NL + "                ";
  protected final String TEXT_411 = ");";
  protected final String TEXT_412 = NL + NL + "        if ((null != ";
  protected final String TEXT_413 = ") && !\"\".equals(";
  protected final String TEXT_414 = ")) {" + NL + "            sparkConfiguration.set(\"spark.hadoop.talend.fs.s3a.assume.role.external.id\", ";
  protected final String TEXT_415 = ");" + NL + "        }" + NL + "" + NL + "        if ((null != ";
  protected final String TEXT_416 = ") && !\"\".equals(";
  protected final String TEXT_417 = ")) {" + NL + "            sparkConfiguration.set(\"spark.hadoop.talend.fs.s3a.sts.endpoint\", ";
  protected final String TEXT_418 = ");" + NL + "        }" + NL + "\t\t\t " + NL + "        sparkConfiguration.set(\"spark.hadoop.talend.fs.s3a.access.key\", ";
  protected final String TEXT_419 = ");" + NL + "        sparkConfiguration.set(\"spark.hadoop.talend.fs.s3a.secret.key\", ";
  protected final String TEXT_420 = ");" + NL + "        sparkConfiguration.set(\"spark.hadoop.talend.fs.s3a.assume.role.arn\", ";
  protected final String TEXT_421 = ");" + NL + "        sparkConfiguration.set(\"spark.hadoop.talend.fs.s3a.assume.role.session.name\", ";
  protected final String TEXT_422 = ");" + NL + "        sparkConfiguration.set(\"spark.hadoop.talend.fs.s3a.assume.role.session.duration\", String.valueOf(";
  protected final String TEXT_423 = "*60));" + NL + "        sparkConfiguration.set(\"spark.hadoop.fs.s3a.aws.credentials.provider\",\"org.apache.hadoop.fs.s3a.HadoopSTSAssumeRoleSessionCredentialsProvider\");";
  protected final String TEXT_424 = NL + "        ";
  protected final String TEXT_425 = NL + "            tuningConf.put(";
  protected final String TEXT_426 = ", ";
  protected final String TEXT_427 = ");";
  protected final String TEXT_428 = NL + "            String driverExtraJavaOpts = tuningConf.get(\"spark.driver.extraJavaOptions\") == null ? \"\" : tuningConf.get(\"spark.driver.extraJavaOptions\");" + NL + "            String appMasterExtraJavaOpts = tuningConf.get(\"spark.yarn.am.extraJavaOptions\") == null ? \"\" : tuningConf.get(\"spark.yarn.am.extraJavaOptions\");" + NL + "            tuningConf.put(\"spark.driver.extraJavaOptions\", driverExtraJavaOpts + \" -Dhdp.version=\" + ";
  protected final String TEXT_429 = ");" + NL + "            tuningConf.put(\"spark.yarn.am.extraJavaOptions\", appMasterExtraJavaOpts + \" -Dhdp.version=\" + ";
  protected final String TEXT_430 = ");";
  protected final String TEXT_431 = NL + "        sparkConfiguration.setAll(scala.collection.JavaConversions.mapAsScalaMap(tuningConf));";
  protected final String TEXT_432 = NL + "            sparkConfiguration.set(\"spark.local.dir\", ";
  protected final String TEXT_433 = ");";
  protected final String TEXT_434 = NL + "        \tsparkConfiguration.set(\"spark.sql.catalogImplementation\", \"hive\");";
  protected final String TEXT_435 = NL + "        return sparkConfiguration;" + NL + "    }";
  protected final String TEXT_436 = NL + "        /**" + NL + "         *" + NL + "         * This method uses the Spark JobServer REST API to submit a Spark job." + NL + "         * @param conf the Spark configuration of the job to execute." + NL + "         * @return the result of the job." + NL + "         *" + NL + "         */" + NL + "        public int runSparkJobServerJob(org.apache.spark.SparkConf conf, java.util.Map<String, String> tuningConf) throws Exception {";
  protected final String TEXT_437 = NL + "                final String hdInsightPassword = routines.system.PasswordEncryptUtil.decryptPassword(";
  protected final String TEXT_438 = ");";
  protected final String TEXT_439 = NL + "                final String hdInsightPassword = ";
  protected final String TEXT_440 = ";";
  protected final String TEXT_441 = NL + "            String jobJar = \"\";" + NL + "            String[] jars = libjars.toString().split(\",\");" + NL + "            for(int i=0; i<jars.length; i++) {" + NL + "                String jar = jars[i];" + NL + "                if(jar.contains(jobName.toLowerCase())) {" + NL + "                    jobJar = jar;" + NL + "                }" + NL + "            }" + NL + "" + NL + "            java.util.Map<String, String> confMap = new java.util.HashMap<>();" + NL + "            for(scala.Tuple2 element: java.util.Arrays.asList(conf.getAll())) {" + NL + "                confMap.put((String) element._1, (String) element._2);" + NL + "            }" + NL + "            routines.system.GetJarsToRegister getJarsToRegister = new routines.system.GetJarsToRegister();" + NL + "            org.talend.bigdata.launcher.jobserver.JobServerJob instance = new org.talend.bigdata.launcher.jobserver.SparkBatchJob.Builder()" + NL + "                .withEndpoint(";
  protected final String TEXT_442 = ")" + NL + "                .withCredentials(new org.talend.bigdata.launcher.security.HDInsightCredentials(";
  protected final String TEXT_443 = ", hdInsightPassword))" + NL + "                .withJarToExecute(jobJar)" + NL + "                .withClassToExecute(\"";
  protected final String TEXT_444 = ".";
  protected final String TEXT_445 = ".";
  protected final String TEXT_446 = "\")" + NL + "                .withAppName(projectName + \"_\" + jobName + \"_\" + jobVersion + \"_\" + pid)" + NL + "                .withConf(confMap)" + NL + "                .withTuningConf(tuningConf)" + NL + "                .build();" + NL + "" + NL + "            String jobResult = instance.executeJob();" + NL + "            if(\"ERROR\".equals(jobResult)) {" + NL + "                throw instance.getException();" + NL + "            } else if(\"OK\".equals(jobResult)) {" + NL + "                return instance.getExitCode();" + NL + "            }" + NL + "            return 1;" + NL + "        }" + NL + "" + NL + "        @Override" + NL + "        public Object runJob(Object sparkContext, com.typesafe.config.Config conf) {" + NL + "            org.apache.spark.api.java.JavaSparkContext ctx = new org.apache.spark.api.java.JavaSparkContext((org.apache.spark.SparkContext) sparkContext);" + NL + "            int returnCode = 1;" + NL + "            try {" + NL + "                returnCode = run(ctx);" + NL + "            } catch (java.lang.Exception e) {" + NL + "                Thread.currentThread().stop(e);" + NL + "            }" + NL + "            return returnCode;" + NL + "        }";
  protected final String TEXT_447 = NL + "        public int runClientJob(org.apache.spark.SparkConf sparkConfiguration, java.util.List livyJobArgs) throws Exception {" + NL + "            initContext();" + NL + "            routines.system.GetJarsToRegister getJarsToRegister = new routines.system.GetJarsToRegister();";
  protected final String TEXT_448 = NL + "                final String hdInsightPassword = routines.system.PasswordEncryptUtil.decryptPassword(";
  protected final String TEXT_449 = ");";
  protected final String TEXT_450 = NL + "                final String hdInsightPassword = ";
  protected final String TEXT_451 = ";";
  protected final String TEXT_452 = NL + "                final String wasbPassword = routines.system.PasswordEncryptUtil.decryptPassword(";
  protected final String TEXT_453 = ");";
  protected final String TEXT_454 = NL + "                final String wasbPassword = ";
  protected final String TEXT_455 = ";";
  protected final String TEXT_456 = NL + NL + "            String jobJar = \"\";" + NL + "            String[] jars = libjars.toString().split(\",\");" + NL + "            for(int i=0; i<jars.length; i++) {" + NL + "                String jar = jars[i];" + NL + "                if(jar.contains(jobName.toLowerCase())) {" + NL + "                    jobJar = jar;" + NL + "                }" + NL + "            }" + NL + "" + NL + "            java.util.Map<String, String> conf = new java.util.HashMap<>();" + NL + "            for(scala.Tuple2 element: java.util.Arrays.asList(sparkConfiguration.getAll())) {" + NL + "                conf.put((String) element._1, (String) element._2);" + NL + "            }" + NL + "" + NL + "            org.talend.bigdata.launcher.fs.FileSystem azureFs = new org.talend.bigdata.launcher.fs.AzureFileSystem(\"DefaultEndpointsProtocol=https;\"" + NL + "                + \"AccountName=\"" + NL + "                + ";
  protected final String TEXT_457 = NL + "                + \";\"" + NL + "                + \"AccountKey=\" + wasbPassword, ";
  protected final String TEXT_458 = ");" + NL + "" + NL + "            org.talend.bigdata.launcher.livy.LivyJob instance = new org.talend.bigdata.launcher.livy.SparkJob.Builder()" + NL + "                .withFileSystem(azureFs)" + NL + "                .withCredentials(new org.talend.bigdata.launcher.security.HDInsightCredentials(";
  protected final String TEXT_459 = ", hdInsightPassword))" + NL + "                .withJarToExecute(jobJar)" + NL + "                .withClassToExecute(\"";
  protected final String TEXT_460 = ".";
  protected final String TEXT_461 = ".";
  protected final String TEXT_462 = "\")" + NL + "                .withLivyEndpoint(\"https://\" + ";
  protected final String TEXT_463 = " + \":\" + ";
  protected final String TEXT_464 = ")" + NL + "                .withRemoteFolder(org.talend.bigdata.launcher.utils.Utils.removeFirstSlash(";
  protected final String TEXT_465 = "))" + NL + "                .withUsername(";
  protected final String TEXT_466 = ")" + NL + "                .withLibJars(libjars.toString())" + NL + "                .withConf(conf)" + NL + "                .withArgs(livyJobArgs)" + NL + "                .withAppName(projectName + \"_\" + jobName + \"_\" + jobVersion)";
  protected final String TEXT_467 = NL + "                    .withExecutorMemory(conf.get(\"spark.executor.memory\"))" + NL + "                    .withDriverMemory(conf.get(\"spark.driver.memory\"))";
  protected final String TEXT_468 = NL + "                        .withExecutorCore(conf.get(\"spark.executor.cores\"))";
  protected final String TEXT_469 = NL + "                    .withDriverCore(conf.get(\"spark.driver.cores\"))";
  protected final String TEXT_470 = NL + "                .build();" + NL + "" + NL + "            int returnCode = instance.executeJob();" + NL + "            System.out.println(instance.getJobLog());" + NL + "            return returnCode;" + NL + "        }";
  protected final String TEXT_471 = NL + "        public int runClientJob(org.apache.spark.SparkConf sparkConfiguration, java.util.List dataprocJobArgs) throws Exception {" + NL + "            initContext();" + NL + "            routines.system.GetJarsToRegister getJarsToRegister = new routines.system.GetJarsToRegister();" + NL + "" + NL + "            String jobJar = \"\";" + NL + "            for (String jar: libjars.toString().split(\",\")) {" + NL + "                if(jar.contains(jobName.toLowerCase())) {" + NL + "                    jobJar = jar;" + NL + "                    break;" + NL + "                }" + NL + "            }" + NL + "" + NL + "            java.util.Map<String, String> conf = new java.util.HashMap<>();" + NL + "            for(scala.Tuple2 element: java.util.Arrays.asList(sparkConfiguration.getAll())) {" + NL + "                conf.put((String) element._1, (String) element._2);" + NL + "            }" + NL + "" + NL + "            org.talend.bigdata.launcher.google.dataproc.GoogleDataprocJob instance =" + NL + "                    new org.talend.bigdata.launcher.google.dataproc.DataprocSparkJob.Builder()" + NL + "                .withTalendJobName(projectName + \"_\" + jobName + \"_\" + jobVersion.replace(\".\",\"_\") + \"_\" + pid)" + NL + "                .withClusterName(";
  protected final String TEXT_472 = ")" + NL + "                .withRegion(";
  protected final String TEXT_473 = ")" + NL + "                .withProjectId(";
  protected final String TEXT_474 = ")";
  protected final String TEXT_475 = NL + "                            .withServiceAccountCredentialsPath(";
  protected final String TEXT_476 = ")";
  protected final String TEXT_477 = NL + NL + "                .withJarsBucket(";
  protected final String TEXT_478 = ")" + NL + "                .withJarToExecute(jobJar)" + NL + "                .withMainClass(\"";
  protected final String TEXT_479 = ".";
  protected final String TEXT_480 = ".";
  protected final String TEXT_481 = "\")" + NL + "                .withLibJars(libjars)" + NL + "" + NL + "                .withConf(conf)" + NL + "                .withArgs(dataprocJobArgs)" + NL + NL;
  protected final String TEXT_482 = NL + "                .withLogLevel(\"";
  protected final String TEXT_483 = "\")";
  protected final String TEXT_484 = NL + "                .build();" + NL + "" + NL + "                // Add JVM shutdown hook to send a cancel job request to the cluster" + NL + "                Runtime.getRuntime().addShutdownHook(new DataprocShutdownHook(instance));" + NL + "                // Submit the actual job" + NL + "                int returnCode = instance.executeJob();" + NL + "                System.out.println(instance.getJobLog());" + NL + "                return returnCode;" + NL + "        }" + NL + "" + NL + "        class DataprocShutdownHook extends Thread {" + NL + "                private org.talend.bigdata.launcher.google.dataproc.GoogleDataprocJob job;" + NL + "" + NL + "                public DataprocShutdownHook(org.talend.bigdata.launcher.google.dataproc.GoogleDataprocJob job) {" + NL + "                    this.job = job;" + NL + "                }" + NL + "" + NL + "                @Override" + NL + "                public void run() {";
  protected final String TEXT_485 = " log.info(\"Calling Dataproc Shutdown Hook\"); ";
  protected final String TEXT_486 = NL + "                   try {" + NL + "                    // A cancel request will be actually sent to the cluster only if the job is still ongoing" + NL + "                        job.cancelJob();" + NL + "                    } catch (Exception e) {";
  protected final String TEXT_487 = " log.error(\"Could not send a job cancel request to Dataproc : \"+e.getMessage()); ";
  protected final String TEXT_488 = NL + "                    }" + NL + "                }" + NL + "        }";
  protected final String TEXT_489 = NL + "        public int runClientJob(org.apache.spark.SparkConf sparkConfiguration, java.util.List livyJobArgs) throws Exception {" + NL + "            initContext();" + NL + "            routines.system.GetJarsToRegister getJarsToRegister = new routines.system.GetJarsToRegister();" + NL + "" + NL + "            String jobJar = \"\";" + NL + "            String[] jars = libjars.toString().split(\",\");" + NL + "            for(int i=0; i<jars.length; i++) {" + NL + "                String jar = jars[i];" + NL + "                if(jar.contains(jobName.toLowerCase())) {" + NL + "                    jobJar = jar;" + NL + "                }" + NL + "            }" + NL + "" + NL + "            java.util.Map<String, String> conf = new java.util.HashMap<>();" + NL + "            for(scala.Tuple2 element: java.util.Arrays.asList(sparkConfiguration.getAll())) {" + NL + "                conf.put((String) element._1, (String) element._2);" + NL + "            }" + NL + "" + NL + "            org.talend.bigdata.launcher.databricks.DatabricksJob instance = new org.talend.bigdata.launcher.databricks.SparkBatchJob.Builder()" + NL + "                .withAppName(projectName + \"_\" + jobName + \"_\" + jobVersion)" + NL + "                .withClassToExecute(\"";
  protected final String TEXT_490 = ".";
  protected final String TEXT_491 = ".";
  protected final String TEXT_492 = "\")" + NL + "                .withFilePath(";
  protected final String TEXT_493 = ")" + NL + "                .withJarToExecute(jobJar)" + NL + "                .withLibJars(libjars.toString())" + NL + "                .withArgs(livyJobArgs)" + NL + "                .withEndpoint(";
  protected final String TEXT_494 = ")" + NL + "                .withToken(routines.system.PasswordEncryptUtil.decryptPassword(";
  protected final String TEXT_495 = "))" + NL + "                .withClusterId(";
  protected final String TEXT_496 = ")" + NL + "                .withUserAgent(routines.system.Constant.getUserAgent(\"";
  protected final String TEXT_497 = "\"))" + NL + "                .build();" + NL + "" + NL + "            int returnCode = instance.executeJob();" + NL + "            //System.out.println(instance.getJobLog());" + NL + "            return returnCode;" + NL + "        }";
  protected final String TEXT_498 = NL + "        private int runClientJob(org.apache.spark.SparkConf sparkConfiguration, java.util.List args) throws Exception {" + NL + "            initContext();" + NL + "" + NL + "            java.util.Map<String, String> conf = new java.util.HashMap<>();" + NL + "            for(scala.Tuple2 element: java.util.Arrays.asList(sparkConfiguration.getAll())) {" + NL + "                conf.put((String) element._1, (String) element._2);" + NL + "            }" + NL + "" + NL + "\t\t\tString accessKey = ";
  protected final String TEXT_499 = ";" + NL + "            String secretKey = ";
  protected final String TEXT_500 = ";" + NL + "            String bucketName = ";
  protected final String TEXT_501 = ";" + NL + "            String bucketKey = ";
  protected final String TEXT_502 = ";" + NL + "            String region = ";
  protected final String TEXT_503 = ";" + NL + "" + NL + "            org.talend.bigdata.launcher.qubole.QuboleSparkClient.S3Account s3Account =" + NL + "            \tnew org.talend.bigdata.launcher.qubole.QuboleSparkClient.S3Account(accessKey, secretKey, bucketName, bucketKey, region);" + NL + "            " + NL + "            String talendJobName = jobName;" + NL + "            String talendJobVersion = jobVersion;" + NL + "\t\t\tjava.util.List<String> jars = java.util.Arrays.asList(libjars.split(\",\"));" + NL + "\t\t\tString master = ";
  protected final String TEXT_504 = ";" + NL + "\t\t\tString mainclass = \"";
  protected final String TEXT_505 = ".";
  protected final String TEXT_506 = ".";
  protected final String TEXT_507 = "\";" + NL + "" + NL + "            org.talend.bigdata.launcher.qubole.QuboleSparkClient.Job job =" + NL + "            \tnew org.talend.bigdata.launcher.qubole.QuboleSparkClient.Job(talendJobName, talendJobVersion, jars, master, mainclass, s3Account);" + NL + "" + NL + "            job.setDeployMode(";
  protected final String TEXT_508 = ");" + NL + "            job.setConfigs(conf);" + NL + "            job.setAppName(talendJobName + \"_\" + talendJobVersion);" + NL + "            job.setUserAgent(routines.system.Constant.getUserAgent(\"";
  protected final String TEXT_509 = "\"));";
  protected final String TEXT_510 = NL + "            \tjob.setClusterLabel(";
  protected final String TEXT_511 = ");";
  protected final String TEXT_512 = NL + NL + "            String encryptedToken = ";
  protected final String TEXT_513 = ";" + NL + "" + NL + "\t\t\ttry (org.talend.bigdata.launcher.qubole.QuboleSparkClient quboleSparkClient =" + NL + "\t\t\t\t\tnew org.talend.bigdata.launcher.qubole.QuboleSparkClient(" + NL + "\t\t\t\t\t\troutines.system.PasswordEncryptUtil.decryptPassword(encryptedToken)" + NL + "            \t\t\t\t";
  protected final String TEXT_514 = NL + "            \t\t\t\t\t, ";
  protected final String TEXT_515 = NL + "            \t\t\t\t";
  protected final String TEXT_516 = NL + "            \t\t)" + NL + "            \t) {" + NL + "\t" + NL + "\t\t\t\t";
  protected final String TEXT_517 = NL + "\t\t\t\t\tlog.info(\"waiting for job execution results ...\");" + NL + "\t\t\t\t";
  protected final String TEXT_518 = NL + "\t\t\t\tString executionResults = quboleSparkClient.execute(job);" + NL + "\t\t\t\tif (!\"\".equals(executionResults)) {" + NL + "            \t\tSystem.out.println(executionResults);" + NL + "            \t}" + NL + "            } catch (Exception e) {" + NL + "            \tthrow e;" + NL + "            }" + NL + "            return 0;" + NL + "        }";
  protected final String TEXT_519 = NL + "        public int runClientJob(org.apache.spark.SparkConf sparkConfiguration, java.util.List altusJobArgs) throws Exception {" + NL + "            initContext();" + NL + "            routines.system.GetJarsToRegister getJarsToRegister = new routines.system.GetJarsToRegister();" + NL + "" + NL + "            String jobJar = \"\";" + NL + "            for (String jar: libjars.toString().split(\",\")) {" + NL + "                if(jar.contains(jobName.toLowerCase())) {" + NL + "                    jobJar = jar;" + NL + "                    break;" + NL + "                }" + NL + "            }" + NL + "" + NL + "            java.util.Map<String, String> conf = new java.util.HashMap<>();" + NL + "            for(scala.Tuple2 element: java.util.Arrays.asList(sparkConfiguration.getAll())) {" + NL + "                conf.put((String) element._1, (String) element._2);" + NL + "            }" + NL;
  protected final String TEXT_520 = NL + "                final String decryptedPassword_";
  protected final String TEXT_521 = " = routines.system.PasswordEncryptUtil.decryptPassword(";
  protected final String TEXT_522 = ");";
  protected final String TEXT_523 = NL + "                final String decryptedPassword_";
  protected final String TEXT_524 = " = ";
  protected final String TEXT_525 = ";";
  protected final String TEXT_526 = NL + NL + "            java.text.DateFormat dateStrFormat = new java.text.SimpleDateFormat(\"yyyyMMddHHmmss\");";
  protected final String TEXT_527 = " " + NL + "            org.talend.bigdata.launcher.altus.CloudConfiguration cloudConfiguration = new org.talend.bigdata.launcher.altus.AWSConfiguration(";
  protected final String TEXT_528 = ", decryptedPassword_";
  protected final String TEXT_529 = ", ";
  protected final String TEXT_530 = ", ";
  protected final String TEXT_531 = ", ";
  protected final String TEXT_532 = ");";
  protected final String TEXT_533 = NL + "            org.talend.bigdata.launcher.altus.CloudConfiguration cloudConfiguration = new org.talend.bigdata.launcher.altus.AzureConfiguration(";
  protected final String TEXT_534 = ", ";
  protected final String TEXT_535 = ", ";
  protected final String TEXT_536 = ", ";
  protected final String TEXT_537 = ");";
  protected final String TEXT_538 = NL + "            ";
  protected final String TEXT_539 = NL + "                org.talend.bigdata.launcher.altus.AltusJob instance = new org.talend.bigdata.launcher.altus.AltusSparkJob.Builder()" + NL + "                    .withTalendJobName(projectName + \"_\" + jobName + \"_\" + jobVersion.replace(\".\",\"_\") + \"_\" + dateStrFormat.format(new java.util.Date()))" + NL + "                    .withClusterName(";
  protected final String TEXT_540 = ")" + NL + "                    .setCredentials(";
  protected final String TEXT_541 = ")" + NL + "                    .withAccessKey(";
  protected final String TEXT_542 = ")" + NL + "                    .withSecretKey(";
  protected final String TEXT_543 = ")" + NL + "                    .withPathToAltusCLI(";
  protected final String TEXT_544 = ")" + NL + "                    .withJarToExecute(jobJar)" + NL + "                    .withMainClass(\"";
  protected final String TEXT_545 = ".";
  protected final String TEXT_546 = ".";
  protected final String TEXT_547 = "\")" + NL + "                    .withLibJars(libjars)" + NL + "                    .withConf(conf)" + NL + "                    .withArgs(altusJobArgs)" + NL + "                    .withCloudConfiguration(cloudConfiguration)" + NL;
  protected final String TEXT_548 = NL + "                            .withLogLevel(\"";
  protected final String TEXT_549 = "\")";
  protected final String TEXT_550 = NL + "                    .build();";
  protected final String TEXT_551 = NL + "                org.talend.bigdata.launcher.altus.AltusJob instance = new org.talend.bigdata.launcher.altus.AltusSparkWithClusterCreationJob.Builder()" + NL + "                    .withTalendJobName(projectName + \"_\" + jobName + \"_\" + jobVersion.replace(\".\",\"_\") + \"_\" + dateStrFormat.format(new java.util.Date()))" + NL + "                    .withClusterName(";
  protected final String TEXT_552 = ")" + NL + "                    .withEnvironmentName(";
  protected final String TEXT_553 = ")" + NL + "                    .withCloudProvider(";
  protected final String TEXT_554 = ")" + NL + "                    .withDeleteAfterExecution(";
  protected final String TEXT_555 = ")" + NL + "" + NL + "                    .setCredentials(";
  protected final String TEXT_556 = ")" + NL + "                    .withAccessKey(";
  protected final String TEXT_557 = ")" + NL + "                    .withSecretKey(";
  protected final String TEXT_558 = ")" + NL + "                    .withUseCustomJson(";
  protected final String TEXT_559 = ")";
  protected final String TEXT_560 = NL + "                        .withCustomJson(";
  protected final String TEXT_561 = ")";
  protected final String TEXT_562 = NL + "                        .withInstanceType(";
  protected final String TEXT_563 = ")" + NL + "                        .withWorkderNode(";
  protected final String TEXT_564 = ")" + NL + "                        .withSshKey(";
  protected final String TEXT_565 = ")" + NL + "                        .withClouderaManagerUsername(";
  protected final String TEXT_566 = ")" + NL + "                        .withClouderaManagerPassword(";
  protected final String TEXT_567 = ")" + NL + "                        .withCustomBootstrapScript(";
  protected final String TEXT_568 = ")";
  protected final String TEXT_569 = NL + NL + "                    .withPathToAltusCLI(";
  protected final String TEXT_570 = ")" + NL + "                    .withJarToExecute(jobJar)" + NL + "                    .withMainClass(\"";
  protected final String TEXT_571 = ".";
  protected final String TEXT_572 = ".";
  protected final String TEXT_573 = "\")" + NL + "                    .withLibJars(libjars)" + NL + "                    .withConf(conf)" + NL + "                    .withArgs(altusJobArgs)" + NL + "                    .withCloudConfiguration(cloudConfiguration)" + NL;
  protected final String TEXT_574 = NL + "                            .withLogLevel(\"";
  protected final String TEXT_575 = "\")";
  protected final String TEXT_576 = NL + "                    .build();";
  protected final String TEXT_577 = NL + NL + "                // Add JVM shutdown hook to send a cancel job request to the cluster" + NL + "                Runtime.getRuntime().addShutdownHook(new AltusShutdownHook(instance));" + NL + "                // Submit the actual job" + NL + "                int returnCode = instance.executeJob();" + NL + "                System.out.println(instance.getJobLog());" + NL + "                return returnCode;" + NL + "        }" + NL + "" + NL + "        class AltusShutdownHook extends Thread {" + NL + "                private org.talend.bigdata.launcher.altus.AltusJob job;" + NL + "" + NL + "                public AltusShutdownHook(org.talend.bigdata.launcher.altus.AltusJob job) {" + NL + "                    this.job = job;" + NL + "                }" + NL + "" + NL + "                @Override" + NL + "                public void run() {";
  protected final String TEXT_578 = " log.info(\"Calling Altus Shutdown Hook\"); ";
  protected final String TEXT_579 = NL + "                   try {" + NL + "                    // A cancel request will be actually sent to the cluster only if the job is still ongoing" + NL + "                        job.cancelJob();" + NL + "                    } catch (Exception e) {";
  protected final String TEXT_580 = " log.error(\"Could not send a job cancel request to altus : \"+e.getMessage()); ";
  protected final String TEXT_581 = NL + "                    }" + NL + "                }" + NL + "        }";
  protected final String TEXT_582 = NL + "        public int runClientJob(org.apache.spark.SparkConf sparkConfiguration, java.util.List<String> args2) throws Exception {" + NL + "            initContext();" + NL + "            routines.system.GetJarsToRegister getJarsToRegister = new routines.system.GetJarsToRegister();" + NL + "" + NL + "            String jobJar = \"\";" + NL + "            String libJarUriPrefix = getLibJarURIPrefix();" + NL + "            for (String jar: libjars.toString().split(\",\")) {" + NL + "                if(jar.contains(jobName.toLowerCase())) {" + NL + "                    jobJar = (jar.startsWith(\".\") ? \"\" : libJarUriPrefix) + jar;" + NL + "                    break;" + NL + "                }" + NL + "            }" + NL + "" + NL + "            java.util.Map<String, String> conf = new java.util.HashMap<>();" + NL + "            for(scala.Tuple2 element: java.util.Arrays.asList(sparkConfiguration.getAll())) {" + NL + "                conf.put((String) element._1, (String) element._2);" + NL + "            }" + NL + "" + NL + "            System.setProperty(\"SPARK_YARN_MODE\", \"true\");" + NL + "            " + NL + "            List<String> argsList = new ArrayList<String>(java.util.Arrays.asList(" + NL + "                    \"--jar\", jobJar," + NL + "                    \"--class\", \"";
  protected final String TEXT_583 = ".";
  protected final String TEXT_584 = ".";
  protected final String TEXT_585 = "\"," + NL + "                    \"--arg\", \"-calledByAM\"" + NL + "            ));" + NL + "            for(String arg : args2) {" + NL + "            \tString[] splitArg = arg.trim().split(\"\\\\s+\");" + NL + "            \tfor(String argSplitted : splitArg) {" + NL + "            \t\targsList.add(\"--arg\");" + NL + "            \t\targsList.add(argSplitted);" + NL + "            \t}" + NL + "            }";
  protected final String TEXT_586 = NL + "                argsList.add(\"--name\");" + NL + "                argsList.add(sparkConfiguration.get(\"spark.app.name\"));";
  protected final String TEXT_587 = NL + "                    argsList.add(\"--driver-cores\");" + NL + "                    argsList.add(";
  protected final String TEXT_588 = ");" + NL + "                    argsList.add(\"--driver-memory\");" + NL + "                    argsList.add(";
  protected final String TEXT_589 = ");" + NL + "                    argsList.add(\"--executor-memory\");" + NL + "                    argsList.add(";
  protected final String TEXT_590 = ");";
  protected final String TEXT_591 = NL + "                        argsList.add(\"--num-executors\");" + NL + "                        argsList.add(";
  protected final String TEXT_592 = ");";
  protected final String TEXT_593 = NL + "                        argsList.add(\"--executor-cores\");" + NL + "                        argsList.add(";
  protected final String TEXT_594 = ";";
  protected final String TEXT_595 = NL + "                argsList.add(\"--addJars\");" + NL + "                argsList.add(removeJobFromSparkJars(sparkConfiguration.get(\"spark.jars\")));" + NL + "                " + NL + "                org.apache.spark.deploy.yarn.ClientArguments cArgs = new org.apache.spark.deploy.yarn.ClientArguments(argsList.toArray(new String[argsList.size()]), sparkConfiguration);";
  protected final String TEXT_596 = NL + "                org.apache.spark.deploy.yarn.ClientArguments cArgs = new org.apache.spark.deploy.yarn.ClientArguments(argsList.toArray(new String[argsList.size()]));";
  protected final String TEXT_597 = NL + "            org.apache.spark.deploy.yarn.Client client = new org.apache.spark.deploy.yarn.Client(cArgs, sparkConfiguration); " + NL + "" + NL + "            // submit Spark job to YARN" + NL + "            client.run();" + NL + "" + NL + "            return 0;" + NL + "        }";
  protected final String TEXT_598 = NL + "    private void setupJobWideSSLConfigurations(){";
  protected final String TEXT_599 = NL + "                System.setProperty(\"java.protocol.handler.pkgs\", \"com.sun.net.ssl.internal.www.protocol\");" + NL + "                javax.net.ssl.HttpsURLConnection.setDefaultHostnameVerifier(" + NL + "                    new javax.net.ssl.HostnameVerifier(){" + NL + "                        public boolean verify(String hostName,javax.net.ssl.SSLSession session)" + NL + "                            {" + NL + "                                return true;" + NL + "                            }" + NL + "                    }" + NL + "                );";
  protected final String TEXT_600 = NL + "            System.setProperty(\"javax.net.ssl.trustStore\", ";
  protected final String TEXT_601 = ");" + NL + "            System.setProperty(\"javax.net.ssl.trustStoreType\", \"";
  protected final String TEXT_602 = "\");";
  protected final String TEXT_603 = " " + NL + "\tfinal String decryptedPassword_";
  protected final String TEXT_604 = " = routines.system.PasswordEncryptUtil.decryptPassword(";
  protected final String TEXT_605 = ");";
  protected final String TEXT_606 = NL + "\tfinal String decryptedPassword_";
  protected final String TEXT_607 = " = ";
  protected final String TEXT_608 = "; ";
  protected final String TEXT_609 = NL + "            System.setProperty(\"javax.net.ssl.trustStorePassword\", decryptedPassword_";
  protected final String TEXT_610 = ");";
  protected final String TEXT_611 = NL + "                System.setProperty(\"javax.net.ssl.keyStore\", ";
  protected final String TEXT_612 = ");" + NL + "                System.setProperty(\"javax.net.ssl.keyStoreType\", \"";
  protected final String TEXT_613 = "\");";
  protected final String TEXT_614 = NL + "                    String decryptedPwd_";
  protected final String TEXT_615 = " = routines.system.PasswordEncryptUtil.decryptPassword(";
  protected final String TEXT_616 = ");";
  protected final String TEXT_617 = NL + "                    String decryptedPwd_";
  protected final String TEXT_618 = " = ";
  protected final String TEXT_619 = ";";
  protected final String TEXT_620 = NL + "                System.setProperty(\"javax.net.ssl.keyStorePassword\",decryptedPwd_";
  protected final String TEXT_621 = ");";
  protected final String TEXT_622 = NL + "                System.clearProperty(\"javax.net.ssl.keyStore\");" + NL + "                System.clearProperty(\"javax.net.ssl.keyStoreType\");" + NL + "                System.clearProperty(\"javax.net.ssl.keyStorePassword\");";
  protected final String TEXT_623 = NL + "            // No SSL configurations required";
  protected final String TEXT_624 = NL + "    }" + NL + "" + NL + "    private String genTempFolderForComponent(String name) {" + NL + "        java.io.File tempDir = new java.io.File(\"/tmp/\" + pid, name);" + NL + "        String tempDirPath = tempDir.getPath();" + NL + "        if (java.io.File.separatorChar != '/')" + NL + "            tempDirPath = tempDirPath.replace(java.io.File.separatorChar, '/');" + NL + "        return tempDirPath;" + NL + "    }" + NL + "" + NL + "    private void initContext(){" + NL + "        //get context" + NL + "        try{" + NL + "            //call job/subjob with an existing context, like: --context=production. if without this parameter, there will use the default context instead." + NL + "            java.io.InputStream inContext = ";
  protected final String TEXT_625 = ".class.getClassLoader().getResourceAsStream(\"";
  protected final String TEXT_626 = "/contexts/\"+contextStr+\".properties\");" + NL + "            if(isDefaultContext && inContext == null){" + NL + "            }else{" + NL + "                if(inContext!=null){" + NL + "                    //defaultProps is in order to keep the original context value" + NL + "                    defaultProps.load(inContext);" + NL + "                    inContext.close();" + NL + "                    context = new ContextProperties(defaultProps);" + NL + "                }else{" + NL + "                    //print info and job continue to run, for case: context_param is not empty." + NL + "                    System.err.println(\"Could not find the context \" + contextStr);" + NL + "                }" + NL + "            }" + NL + "" + NL + "            if(!context_param.isEmpty()){" + NL + "                context.putAll(context_param);" + NL + "            }" + NL + "            context.loadValue(context_param,null);" + NL + "            if(parentContextMap != null && !parentContextMap.isEmpty()){";
  protected final String TEXT_627 = NL + "                    if(parentContextMap.containsKey(\"";
  protected final String TEXT_628 = "\")){" + NL + "                        context.";
  protected final String TEXT_629 = " = (";
  protected final String TEXT_630 = ") parentContextMap.get(\"";
  protected final String TEXT_631 = "\");" + NL + "                    }";
  protected final String TEXT_632 = NL + "            }" + NL + "        }catch (java.io.IOException ie){" + NL + "            System.err.println(\"Could not load context \"+contextStr);" + NL + "            ie.printStackTrace();" + NL + "        }";
  protected final String TEXT_633 = NL + "                // Cloudera Navigator Code";
  protected final String TEXT_634 = NL + "                // Atlas Code";
  protected final String TEXT_635 = NL + "    }" + NL + "" + NL + "    private void setContext(Configuration conf, org.apache.spark.api.java.JavaSparkContext ctx){" + NL + "        //get context" + NL + "        //call job/subjob with an existing context, like: --context=production. if without this parameter, there will use the default context instead." + NL + "        java.net.URL inContextUrl = ";
  protected final String TEXT_636 = ".class.getClassLoader().getResource(\"";
  protected final String TEXT_637 = "/contexts/\"+contextStr+\".properties\");" + NL + "        if(isDefaultContext && inContextUrl == null){" + NL + "" + NL + "        }else{" + NL + "            if(inContextUrl!=null){" + NL + "                conf.set(ContextProperties.CONTEXT_FILE_NAME, contextStr+\".properties\");";
  protected final String TEXT_638 = NL + "                    java.io.File contextFile = new java.io.File(inContextUrl.getPath());" + NL + "                    if(contextFile.exists()) {" + NL + "                        ctx.addFile(inContextUrl.getPath());" + NL + "                    } else {" + NL + "                        java.io.InputStream contextIn = ";
  protected final String TEXT_639 = ".class.getClassLoader().getResourceAsStream(\"";
  protected final String TEXT_640 = "/contexts/\"+contextStr+\".properties\");" + NL + "                        if(contextIn != null){" + NL + "                            java.io.File tmpFile = new java.io.File(System.getProperty(\"java.io.tmpdir\") + \"/\" + jobName,  contextStr+\".properties\");" + NL + "                            java.io.OutputStream contextOut = null;" + NL + "                            try {" + NL + "                                tmpFile.getParentFile().mkdir();" + NL + "                                if(tmpFile.exists()) { tmpFile.delete(); }" + NL + "                                tmpFile.createNewFile();" + NL + "                                contextOut = new java.io.FileOutputStream(tmpFile);" + NL + "" + NL + "                                int len = -1;" + NL + "                                byte[] b = new byte[4096];" + NL + "                                while ((len = contextIn.read(b)) != -1) {" + NL + "                                    contextOut.write(b, 0, len);" + NL + "                                }" + NL + "                            } catch(java.io.IOException ioe) {" + NL + "                                ioe.printStackTrace();" + NL + "                            } finally {" + NL + "                                try {" + NL + "                                    contextIn.close();" + NL + "                                    if(contextOut != null) {" + NL + "                                        contextOut.close();" + NL + "                                    }" + NL + "                                } catch (java.io.IOException ioe) {" + NL + "                                    ioe.printStackTrace();" + NL + "                                }" + NL + "                            }" + NL + "                            ctx.addFile(tmpFile.getPath());" + NL + "                            tmpFile.deleteOnExit();" + NL + "                        }" + NL + "                    }";
  protected final String TEXT_641 = NL + NL + "            }" + NL + "        }" + NL + "" + NL + "        if(!context_param.isEmpty()){" + NL + "            for(Object contextKey : context_param.keySet()){" + NL + "                conf.set(ContextProperties.CONTEXT_PARAMS_PREFIX + contextKey, context.getProperty(contextKey.toString()));" + NL + "                conf.set(ContextProperties.CONTEXT_KEYS, conf.get(ContextProperties.CONTEXT_KEYS, \"\") + \" \" + contextKey);" + NL + "            }" + NL + "        }" + NL + "" + NL + "        if(parentContextMap != null && !parentContextMap.isEmpty()){";
  protected final String TEXT_642 = NL + "                if(parentContextMap.containsKey(\"";
  protected final String TEXT_643 = "\")){" + NL + "                    conf.set(ContextProperties.CONTEXT_PARENT_PARAMS_PREFIX + \"";
  protected final String TEXT_644 = "\", parentContextMap.get(\"";
  protected final String TEXT_645 = "\").toString());" + NL + "                    conf.set(ContextProperties.CONTEXT_PARENT_KEYS, conf.get(ContextProperties.CONTEXT_KEYS, \"\") + \" \" + \"";
  protected final String TEXT_646 = "\");" + NL + "                }";
  protected final String TEXT_647 = NL + "        }" + NL + "    }" + NL;
  protected final String TEXT_648 = NL + "        /**" + NL + "         * Takes a comma-separated list of jars and returns it after having removed the jar of the job." + NL + "         *" + NL + "         * @param sparkJars the comma-separated list of jars" + NL + "         * @return the new comma-separated list of jars" + NL + "         */" + NL + "        private static String removeJobFromSparkJars(String sparkJars) {" + NL + "            java.util.List<String> sparkJarsWithoutJob = new java.util.ArrayList<>();" + NL + "            for(String jarPath : sparkJars.split(\",\")) {" + NL + "                if(!jarPath.endsWith(jobName.toLowerCase() + \"_\" + jobVersion.replace(\".\",\"_\") + \".jar\")) {" + NL + "                    sparkJarsWithoutJob.add(jarPath);" + NL + "                }" + NL + "            }" + NL + "            return org.apache.commons.lang3.StringUtils.join(sparkJarsWithoutJob, \",\");" + NL + "        }";
  protected final String TEXT_649 = NL + NL + "    /**" + NL + "     * @return the -libjars elements prefix depending on the OS." + NL + "     */" + NL + "    private static String getLibJarURIPrefix() {" + NL + "        return System.getProperty(\"os.name\").startsWith(\"Windows\") ? \"file:///\" : \"file://\";" + NL + "    }" + NL;
  protected final String TEXT_650 = NL + "        /**" + NL + "         * Search for an argument to be present in order to know if the job has been launched by the Spark job driver. Else, it means it's been launched by a client which is not the driver." + NL + "         * This method is called in a Cloud distribution or YARN cluster context." + NL + "         *" + NL + "         * @param args the command line arguments" + NL + "         * @return a boolean that indicates whether the job has been launched by a Spark job driver." + NL + "         */" + NL + "        private static boolean isDriverCall(String[] args) {" + NL + "            List<String> argsList = java.util.Arrays.asList(args);" + NL + "            int indexDriverCallArg = argsList.indexOf(";
  protected final String TEXT_651 = " \"-calledByLivy\"";
  protected final String TEXT_652 = " \"-calledByGoogleDataproc\"";
  protected final String TEXT_653 = " \"-calledByDatabricks\"";
  protected final String TEXT_654 = " \"-calledByQubole\"";
  protected final String TEXT_655 = " \"-calledByAltus\"";
  protected final String TEXT_656 = " \"-calledByAM\" ";
  protected final String TEXT_657 = NL + "            );" + NL + "            return (indexDriverCallArg != -1);" + NL + "        }";
  protected final String TEXT_658 = NL + NL + "    private void evalParam(String arg) {" + NL + "        if (arg.startsWith(\"--resuming_logs_dir_path\")) {" + NL + "            resuming_logs_dir_path = arg.substring(25);" + NL + "        } else if (arg.startsWith(\"--resuming_checkpoint_path\")) {" + NL + "            resuming_checkpoint_path = arg.substring(27);" + NL + "        } else if (arg.startsWith(\"--parent_part_launcher\")) {" + NL + "            parent_part_launcher = arg.substring(23);" + NL + "        } else if (arg.startsWith(\"--father_pid=\")) {" + NL + "            fatherPid = arg.substring(13);" + NL + "        } else if (arg.startsWith(\"--root_pid=\")) {" + NL + "            rootPid = arg.substring(11);" + NL + "        } else if (arg.startsWith(\"--pid=\")) {" + NL + "            pid = arg.substring(6);" + NL + "        } else if (arg.startsWith(\"--context=\")) {";
  protected final String TEXT_659 = NL + "                cloudApiArgs.add(arg);";
  protected final String TEXT_660 = NL + "            contextStr = arg.substring(\"--context=\".length());" + NL + "            isDefaultContext = false;" + NL + "        } else if (arg.startsWith(\"--context_param\")) {" + NL + "            String keyValue = arg.substring(\"--context_param\".length() + 1);" + NL + "            int index = -1;" + NL + "            if (keyValue != null && (index = keyValue.indexOf('=')) > -1) {" + NL + "                context_param.put(keyValue.substring(0, index), replaceEscapeChars(keyValue.substring(index + 1)));" + NL + "            }";
  protected final String TEXT_661 = NL + "                cloudApiArgs.add(arg);";
  protected final String TEXT_662 = NL + "        } else if (arg.startsWith(\"--stat_port=\")) {" + NL + "            String portStatsStr = arg.substring(12);" + NL + "            if (portStatsStr != null && !portStatsStr.equals(\"null\")) {" + NL + "                portStats = Integer.parseInt(portStatsStr);" + NL + "            }" + NL + "        } else if (arg.startsWith(\"--client_host=\")) {" + NL + "            clientHost = arg.substring(14);" + NL + "        } else if (arg.startsWith(\"--log4jLevel=\")) {" + NL + "            log4jLevel = arg.substring(13);" + NL + "        } else if (arg.startsWith(\"--inspect\")) {" + NL + "            doInspect = Boolean.valueOf(arg.substring(\"--inspect=\".length()));" + NL + "        } else if (arg.startsWith(\"--watch\")) {" + NL + "        \twatch = true;" + NL + "        }" + NL + "    }" + NL + "" + NL + "    private void normalizeArgs(String[] args){" + NL + "        java.util.List<String> argsList = java.util.Arrays.asList(args);" + NL + "        int indexlibjars = argsList.indexOf(\"-libjars\") + 1;" + NL + "        libjars = indexlibjars == 0 ? null : argsList.get(indexlibjars);";
  protected final String TEXT_663 = " if(!isDriverCall) { ";
  protected final String TEXT_664 = NL + "            try {" + NL + "                StringBuilder sb = new StringBuilder();" + NL + "                routines.system.GetJarsToRegister getJarsToRegister = new routines.system.GetJarsToRegister();" + NL + "                boolean isFirst = true;" + NL + "                for(String libjar:libjars.split(\",\")) {" + NL + "                    if(!isFirst) {" + NL + "                        sb.append(\",\");" + NL + "                    }" + NL + "                    isFirst = false;" + NL + "                    sb.append(getJarsToRegister.replaceJarPaths(libjar));" + NL + "                }" + NL + "                libjars = sb.toString();" + NL + "            } catch (java.lang.NullPointerException e) {";
  protected final String TEXT_665 = " log.error(\"\", e); ";
  protected final String TEXT_666 = NL + "            } catch (java.lang.Exception e) {";
  protected final String TEXT_667 = " log.error(e.getMessage()); ";
  protected final String TEXT_668 = NL + "            }";
  protected final String TEXT_669 = " } ";
  protected final String TEXT_670 = NL + "    }" + NL + "" + NL + "    private final String[][] escapeChars = {" + NL + "        {\"\\\\\\\\\",\"\\\\\"},{\"\\\\n\",\"\\n\"},{\"\\\\'\",\"\\'\"},{\"\\\\r\",\"\\r\"}," + NL + "        {\"\\\\f\",\"\\f\"},{\"\\\\b\",\"\\b\"},{\"\\\\t\",\"\\t\"}" + NL + "        };" + NL + "    private String replaceEscapeChars (String keyValue) {" + NL + "" + NL + "        if (keyValue == null || (\"\").equals(keyValue.trim())) {" + NL + "            return keyValue;" + NL + "        }" + NL + "" + NL + "        StringBuilder result = new StringBuilder();" + NL + "        int currIndex = 0;" + NL + "        while (currIndex < keyValue.length()) {" + NL + "            int index = -1;" + NL + "            // judege if the left string includes escape chars" + NL + "            for (String[] strArray : escapeChars) {" + NL + "                index = keyValue.indexOf(strArray[0],currIndex);" + NL + "                if (index>=0) {" + NL + "" + NL + "                    result.append(keyValue.substring(currIndex, index + strArray[0].length()).replace(strArray[0], strArray[1]));" + NL + "                    currIndex = index + strArray[0].length();" + NL + "                    break;" + NL + "                }" + NL + "            }" + NL + "            // if the left string doesn't include escape chars, append the left into the result" + NL + "            if (index < 0) {" + NL + "                result.append(keyValue.substring(currIndex));" + NL + "                currIndex = currIndex + keyValue.length();" + NL + "            }" + NL + "        }" + NL + "" + NL + "        return result.toString();" + NL + "    }" + NL + "" + NL + "    public String getStatus() {" + NL + "        return status;" + NL + "    }" + NL + "}";
  protected final String TEXT_671 = NL;

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    
    CodeGeneratorArgument codeGenArgument = (CodeGeneratorArgument) argument;
    Vector v = (Vector) codeGenArgument.getArgument();
    IProcess process = (IProcess)v.get(0);
    List<INode> rootNodes = (List<INode>)v.get(1);
    List<INode> confNodes = new ArrayList<INode>(rootNodes);

    INode sparkConfig = (INode)v.get(2);
    confNodes.add(sparkConfig);
    org.talend.hadoop.distribution.ESparkVersion sparkVersion
            = org.talend.hadoop.distribution.spark.SparkVersionUtil.getSparkVersion(sparkConfig);

    Boolean isThereAtLeastOneMLComponent = (Boolean) v.get(4);

    String processId = process.getId();

    String cid = "Spark";
    String className = process.getName();
    boolean isTestContainer=ProcessUtils.isTestContainer(process);
    if (isTestContainer) {
        className = className + "Test";
    }
    String jobFolderName = JavaResourcesHelper.getJobFolderName(className, process.getVersion());

    String testCaseFolderName = "";
    IProcess baseProcess = ProcessUtils.getTestContainerBaseProcess(process);
    if (baseProcess != null) {
        testCaseFolderName = JavaResourcesHelper.getJobFolderName(baseProcess.getName(), baseProcess.getVersion()) + '/';
    }
    testCaseFolderName = testCaseFolderName + JavaResourcesHelper.getJobFolderName(process.getName(), process.getVersion());
    String jobClassPackage = codeGenArgument.getCurrentProjectName().toLowerCase() + '/' + testCaseFolderName;

    List<IContextParameter> params = new ArrayList<IContextParameter>();
    params=process.getContextManager().getDefaultContext().getContextParameterList();
    boolean isLog4jEnabled = ("true").equals(ElementParameterParser.getValue(process, "__LOG4J_ACTIVATE__"));
    boolean tuningProperties = "true".equals(ElementParameterParser.getValue(sparkConfig, "__ADVANCED_SETTINGS_CHECK__"));
    boolean setWebuiPort = "true".equals(ElementParameterParser.getValue(sparkConfig, "__WEB_UI_PORT_CHECK__"));
    boolean setExecutorCores = "true".equals(ElementParameterParser.getValue(sparkConfig, "__SPARK_EXECUTOR_CORES_CHECK__"));

    final String sparkStatsPollingRate = tuningProperties ?
            ElementParameterParser.getValue(process, "__SPARK_STATS_POLLING_RATE__")
            : "\"50\"";

    String sparkMode = ElementParameterParser.getValue(sparkConfig, "__SPARK_MODE__");
    String sparkDistribution = ElementParameterParser.getValue(sparkConfig, "__DISTRIBUTION__");
    String sparkDistribVersion = ElementParameterParser.getValue(sparkConfig, "__SPARK_VERSION__");

    boolean useLocalMode = "true".equals(ElementParameterParser.getValue(process, "__SPARK_LOCAL_MODE__"));
    boolean useStandaloneMode = !useLocalMode && "CLUSTER".equals(sparkMode);
    final boolean useYarnClientMode = !useLocalMode && "YARN_CLIENT".equals(sparkMode);
    boolean useYarnClusterMode = !useLocalMode && "YARN_CLUSTER".equals(sparkMode);
    boolean useYarnMode = useYarnClusterMode || useYarnClientMode;

    boolean isExecutedThroughSparkJobServer = false;
    boolean isExecutedThroughLivy = false;
    boolean isDatabricksDistribution = false;
    boolean isGoogleDataprocDistribution = false;
    boolean isQuboleDistribution = false;
    boolean isAltusDistribution = false;
    boolean isCloudDistribution = false;
    boolean useCloudLauncher = false;
	boolean useS3aSpecificProperties = false;

    boolean useMapRTicket = ElementParameterParser.getBooleanValue(sparkConfig, "__USE_MAPRTICKET__");
    String mapRTicketUsername = ElementParameterParser.getValue(sparkConfig, "__USERNAME__");
    String mapRTicketCluster = ElementParameterParser.getValue(sparkConfig, "__MAPRTICKET_CLUSTER__");
    String mapRTicketDuration = ElementParameterParser.getValue(sparkConfig, "__MAPRTICKET_DURATION__");

    boolean setMapRHomeDir = ElementParameterParser.getBooleanValue(sparkConfig, "__SET_MAPR_HOME_DIR__");
    String mapRHomeDir = ElementParameterParser.getValue(sparkConfig, "__MAPR_HOME_DIR__");

    boolean setMapRHadoopLogin = ElementParameterParser.getBooleanValue(sparkConfig, "__SET_HADOOP_LOGIN__");
    String mapRHadoopLogin = ElementParameterParser.getValue(sparkConfig, "__HADOOP_LOGIN__");

    String passwordFieldName = "";

    boolean isCustom = false;
    boolean sparkUseKrb = "true".equals(ElementParameterParser.getValue(sparkConfig, "__USE_KRB__"));
    org.talend.hadoop.distribution.component.SparkBatchComponent sparkBatchDistrib = null;
    final String studioVersion = org.talend.commons.utils.VersionUtils.getDisplayVersion();

    if(!useLocalMode) {
        try {
            sparkBatchDistrib = (org.talend.hadoop.distribution.component.SparkBatchComponent) org.talend.hadoop.distribution.DistributionFactory.buildDistribution(sparkDistribution, sparkDistribVersion);
        } catch (java.lang.Exception e) {
            e.printStackTrace();
            return "";
        }

        isCustom = sparkBatchDistrib instanceof org.talend.hadoop.distribution.custom.CustomDistribution;
        isExecutedThroughSparkJobServer = !isCustom && sparkBatchDistrib.isExecutedThroughSparkJobServer();
        useS3aSpecificProperties = !isCustom && sparkBatchDistrib.useS3AProperties();
        isExecutedThroughLivy = !isCustom && sparkBatchDistrib.isExecutedThroughLivy();
        isDatabricksDistribution = !isCustom && sparkBatchDistrib.isDatabricksDistribution();
        isQuboleDistribution = !isCustom && sparkBatchDistrib.isQuboleDistribution();
        isGoogleDataprocDistribution = !isCustom && sparkBatchDistrib.isGoogleDataprocDistribution();
        isAltusDistribution = !isCustom && sparkBatchDistrib.isAltusDistribution();
        isCloudDistribution = !isCustom && sparkBatchDistrib.isCloudDistribution();
        useCloudLauncher = !isCustom && sparkBatchDistrib.useCloudLauncher();
    } else {
        useS3aSpecificProperties = true;
    }

    // Use to define if the spark version currently used is 1.3.
    boolean isSpark13 = sparkVersion == org.talend.hadoop.distribution.ESparkVersion.SPARK_1_3;
    boolean isSpark2x = sparkVersion.compareTo(org.talend.hadoop.distribution.ESparkVersion.SPARK_2_0) >= 0;
    boolean isAboveSpark230 = sparkVersion.compareTo(org.talend.hadoop.distribution.ESparkVersion.SPARK_2_3) >= 0;

    List<Map<String, String>> sparkAdvancedProperties = (List<Map<String,String>>)ElementParameterParser.getObjectValue(sparkConfig, "__SPARK_ADVANCED_PROPERTIES__");

    java.util.List<String> jarsToRegister = null;

    boolean stats = codeGenArgument.isStatistics();

    // Kerberos variables
    boolean useKrb = false;
    boolean useKeytab = false;
    String keytabPrincipal = null;
    String keytabPath = null;

    String username = null;

    // Spark configurations for caching
    // In case we have multiple components that set different configurations, the last component in the list imposes its configurations
    List<INode> cacheConfNodes = new ArrayList<INode>();

    // Compressing RDDs is a global Spark config
    // So when at least one tCache use compression we use compression globally
    // tCacheOut nodes
    boolean cacheCompressRdd = false; // Compress the cached RDD by the tCache Component
    for (INode pNode : process.getNodesOfType("tCacheOut")) {
        cacheConfNodes.add(pNode);
        cacheCompressRdd = ("true").equals(ElementParameterParser.getValue(pNode, "__COMPRESSRDD__"));
        if(cacheCompressRdd) break;
    }

    // Compressing the RDDs is a global Spark config
    // So when at least one tReplicate use compression we use compression globally
    // tReplicate nodes
    boolean replicateCompressRdd = false; // Compress the chached RDD by the tReplicate Component
    for (INode pNode : process.getNodesOfType("tReplicate")) {
        cacheConfNodes.add(pNode);
        replicateCompressRdd = ("true").equals(ElementParameterParser.getValue(pNode, "__CACHEOUTPUT__")) && ("true").equals(ElementParameterParser.getValue(pNode, "__COMPRESSRDD__"));
        if(replicateCompressRdd) break;
    }

    boolean generatedCompressionConfig = false; // Only generate compression config once
    boolean generatedTachyonConfig = false; // Only generate tachyon config once

    // Parse nodes and setup configuration map
    for (INode pNode : cacheConfNodes) {
        boolean compressRdd = cacheCompressRdd || replicateCompressRdd;
        String compressCodec = ElementParameterParser.getValue(pNode, "__COMPRESSCODEC__");
        String storageLevel = ElementParameterParser.getValue(pNode, "__STORAGELEVEL__");
        String tachyonStoreUrl = ElementParameterParser.getValue(pNode, "__TACHYON_STORE_URL__");
        String tachyonStoreBaseDir = ElementParameterParser.getValue(pNode, "__TACHYON_STORE_BASEDIR__");

        if(!generatedTachyonConfig && storageLevel.equals("OFF_HEAP")){
            generatedTachyonConfig = true;
            Map<String, String> tachyonStoreUrlMap = new HashMap<String, String>();
            tachyonStoreUrlMap.put("PROPERTY","\"spark.tachyonStore.url\"");
            tachyonStoreUrlMap.put("VALUE",tachyonStoreUrl);
            sparkAdvancedProperties.add(tachyonStoreUrlMap);
            Map<String, String> tachyonStoreBaseDirMap = new HashMap<String, String>();
            tachyonStoreBaseDirMap.put("PROPERTY","\"spark.tachyonStore.baseDir\"");
            tachyonStoreBaseDirMap.put("VALUE",tachyonStoreBaseDir);
            sparkAdvancedProperties.add(tachyonStoreBaseDirMap);
        }

        if(!generatedCompressionConfig && compressRdd && (storageLevel.equals("MEMORY_ONLY_SER") || storageLevel.equals("MEMORY_AND_DISK_SER") || storageLevel.equals("MEMORY_AND_DISK_SER") || storageLevel.equals("MEMORY_ONLY_SER_2") || storageLevel.equals("MEMORY_AND_DISK_SER_2"))){
            generatedCompressionConfig = true;
            Map<String, String> rddCompression = new HashMap<String, String>();
            rddCompression.put("PROPERTY", "\"spark.rdd.compress\"");
            rddCompression.put("VALUE", "\"true\"");
            sparkAdvancedProperties.add(rddCompression);
            Map<String, String> rddCompressionCodec = new HashMap<String, String>();
            rddCompressionCodec.put("PROPERTY", "\"spark.io.compression.codec\"");
            rddCompressionCodec.put("VALUE", '"'+compressCodec+'"');
            sparkAdvancedProperties.add(rddCompressionCodec);
        }
    }

    
List<INode> hcmNodes = new ArrayList<INode>(process.getNodesOfType("tHadoopConfManager"));

if(hcmNodes.size() > 1) {
    
    stringBuffer.append(TEXT_1);
    
}

    stringBuffer.append(TEXT_2);
    List<INode> afsNodes = new ArrayList<INode>(process.getNodesOfType("tAzureFSConfiguration"));

    if(afsNodes.size() > 1) {
        
    stringBuffer.append(TEXT_3);
    
    }
    if(afsNodes.size() == 1) {
        INode aNode = afsNodes.get(0);
        
        // the configuration components must not be considered as a root node.
        rootNodes.remove(aNode);

        Map<String, String> asProperties = new HashMap<String, String>();
        boolean blob = ("BLOB").equalsIgnoreCase(ElementParameterParser.getValue(aNode, "__AZURE_FS__"));
        
        if (!blob) {//Data Lake
            asProperties = new HashMap<String, String>();
            asProperties.put("PROPERTY", "\"spark.hadoop.fs.adl.impl\"");
            asProperties.put("VALUE", "\"org.apache.hadoop.fs.adl.AdlFileSystem\"");
            sparkAdvancedProperties.add(asProperties);

            asProperties = new HashMap<String, String>();
            asProperties.put("PROPERTY", "\"spark.hadoop.fs.AbstractFileSystem.adl.impl\"");
            asProperties.put("VALUE", "\"org.apache.hadoop.fs.adl.Adl\"");
            sparkAdvancedProperties.add(asProperties);

            asProperties = new HashMap<String, String>();
            asProperties.put("PROPERTY", "\"spark.hadoop.dfs.adls.oauth2.access.token.provider.type\"");
            asProperties.put("VALUE", "\"ClientCredential\"");
            sparkAdvancedProperties.add(asProperties);
    
            asProperties = new HashMap<String, String>();
            asProperties.put("PROPERTY", "\"spark.hadoop.dfs.adls.oauth2.refresh.url\"");

            //decrypting token endpoint from password field
            String AUTH_TOKEN_ENDPOINT = "__AUTH_TOKEN_ENDPOINT__";
            String authTokenEndpoint = "";
            
            if (ElementParameterParser.canEncrypt(aNode, AUTH_TOKEN_ENDPOINT)) {
                authTokenEndpoint = ElementParameterParser.getEncryptedValue(aNode, AUTH_TOKEN_ENDPOINT);
                authTokenEndpoint = "routines.system.PasswordEncryptUtil.decryptPassword(" + authTokenEndpoint + ")";
            } else {
                authTokenEndpoint = ElementParameterParser.getValue(aNode, AUTH_TOKEN_ENDPOINT);
            }           

            asProperties.put("VALUE", authTokenEndpoint);
            sparkAdvancedProperties.add(asProperties);
    
            asProperties = new HashMap<String, String>();
            asProperties.put("PROPERTY", "\"spark.hadoop.dfs.adls.oauth2.client.id\"");
            asProperties.put("VALUE", ElementParameterParser.getValue(aNode, "__CLIENT_ID__"));
            sparkAdvancedProperties.add(asProperties);
    
            asProperties = new HashMap<String, String>();
            asProperties.put("PROPERTY", "\"spark.hadoop.dfs.adls.oauth2.credential\"");

            //decrypting token endpoint from password field
            String CLIENT_KEY = "__CLIENT_KEY__";
            String clientKey = "";
            
            if (ElementParameterParser.canEncrypt(aNode, CLIENT_KEY)) {
                clientKey = ElementParameterParser.getEncryptedValue(aNode, CLIENT_KEY);
                 clientKey = "routines.system.PasswordEncryptUtil.decryptPassword(" + clientKey + ")";
            } else {
                clientKey = ElementParameterParser.getValue(aNode, CLIENT_KEY);
            }           

            asProperties.put("VALUE", clientKey);
            sparkAdvancedProperties.add(asProperties);
            
        } else {//Blob
            String accountName = ElementParameterParser.getValue(aNode, "__BLOB_ACCOUNT_NAME__");
        
            asProperties = new HashMap<String, String>();
            asProperties.put("PROPERTY", "\"spark.hadoop.fs.wasbs.impl\"");
            asProperties.put("VALUE", "\"org.apache.hadoop.fs.azure.NativeAzureFileSystem\"");
            sparkAdvancedProperties.add(asProperties);
            
            asProperties = new HashMap<String, String>();
            asProperties.put("PROPERTY", "\"spark.hadoop.fs.wasb.impl\"");
            asProperties.put("VALUE", "\"org.apache.hadoop.fs.azure.NativeAzureFileSystem\"");
            sparkAdvancedProperties.add(asProperties);
            
            asProperties = new HashMap<String, String>();
            asProperties.put("PROPERTY", "\"spark.hadoop.fs.azure.account.key.\" + " + accountName + "+ \".blob.core.windows.net\"");

            //decrypting token endpoint from password field
            String ACCOUNT_KEY = "__ACCOUNT_KEY__";
            String accountKey = "";
            
            if (ElementParameterParser.canEncrypt(aNode, ACCOUNT_KEY)) {
                accountKey = ElementParameterParser.getEncryptedValue(aNode, ACCOUNT_KEY);
                accountKey = "routines.system.PasswordEncryptUtil.decryptPassword(" + accountKey + ")";
            } else {
                accountKey = ElementParameterParser.getValue(aNode, ACCOUNT_KEY);
            }           

            asProperties.put("VALUE", accountKey);
            sparkAdvancedProperties.add(asProperties);
            
            asProperties = new HashMap<String, String>();
            asProperties.put("PROPERTY", "\"spark.hadoop.fs.azure.account.keyprovider.\" + " + accountName + "+ \".blob.core.windows.net\"");
            asProperties.put("VALUE", "\"org.apache.hadoop.fs.azure.SimpleKeyProvider\"");
            sparkAdvancedProperties.add(asProperties);
            
            /* Will be functional from Hadoop version 2.9
                As of this implementation no distribution yet have a Hadoop >= 2.9
                It needs to be checked to validate its use
            */
            asProperties = new HashMap<String, String>();
            asProperties.put("PROPERTY", "\"spark.hadoop.fs.azure.user.agent.prefix\"");
            asProperties.put("VALUE", "routines.system.Constant.getUserAgent(" + "\"" + studioVersion + "\"" + ")");
            sparkAdvancedProperties.add(asProperties);

            /* It must be noted Append support in Azure Blob Storage interface DIFFERS FROM HDFS SEMANTICS. 
                Append support does not enforce single writer internally but requires applications to guarantee this semantic. 
                It becomes a responsibility of the application either to ensure single-threaded handling for a particular 
                file path, or rely on some external locking mechanism of its own. Failure to do so will result in unexpected behavior.
            */
            asProperties = new HashMap<String, String>();
            asProperties.put("PROPERTY", "\"spark.hadoop.fs.azure.enable.append.support\"");
            asProperties.put("VALUE", "\"true\"");
            sparkAdvancedProperties.add(asProperties);

            /* Rename and Delete blob operations on directories with large number of files and sub directories currently is very slow
                as these operations are done one blob at a time serially. These files and sub folders can be deleted or renamed parallel.
            */
            asProperties = new HashMap<String, String>();
            asProperties.put("PROPERTY", "\"spark.hadoop.fs.azure.delete.threads\"");
            asProperties.put("VALUE", "\"10\"");
            sparkAdvancedProperties.add(asProperties);

            asProperties = new HashMap<String, String>();
            asProperties.put("PROPERTY", "\"spark.hadoop.fs.azure.rename.threads\"");
            asProperties.put("VALUE", "\"10\"");
            sparkAdvancedProperties.add(asProperties);
        }
    }
    stringBuffer.append(TEXT_4);
    
List<INode> gsNodes = new ArrayList<INode>(process.getNodesOfType("tGSConfiguration"));

    if(gsNodes.size() > 1) {
        
    stringBuffer.append(TEXT_5);
    
    }
    if(gsNodes.size() == 1) {
        INode pNode = gsNodes.get(0);
        // the configuration components must not be considered as a root node.
        rootNodes.remove(pNode);

        Map<String, String> gsProperties = new HashMap<String, String>();
        gsProperties.put("PROPERTY", "\"spark.hadoop.fs.gs.impl\"");
        gsProperties.put("VALUE", "\"com.google.cloud.hadoop.fs.gcs.GoogleHadoopFileSystem\"");
        sparkAdvancedProperties.add(gsProperties);

        gsProperties = new HashMap<String, String>();
        gsProperties.put("PROPERTY", "\"spark.hadoop.fs.AbstractFileSystem.gs.impl\"");
        gsProperties.put("VALUE", "\"com.google.cloud.hadoop.fs.gcs.GoogleHadoopFS\"");
        sparkAdvancedProperties.add(gsProperties);

        gsProperties = new HashMap<String, String>();
        gsProperties.put("PROPERTY", "\"spark.hadoop.fs.gs.system.bucket\"");
        gsProperties.put("VALUE", ElementParameterParser.getValue(pNode, "__BUCKET_NAME__"));
        sparkAdvancedProperties.add(gsProperties);

        if(isGoogleDataprocDistribution) {
            gsProperties = new HashMap<String, String>();
            gsProperties.put("PROPERTY", "\"spark.hadoop.fs.gs.project.id\"");
            gsProperties.put("VALUE", ElementParameterParser.getValue(sparkConfig, "__GOOGLE_PROJECT_ID__"));
            sparkAdvancedProperties.add(gsProperties);
        } else {
            gsProperties = new HashMap<String, String>();
            gsProperties.put("PROPERTY", "\"spark.hadoop.fs.gs.project.id\"");
            gsProperties.put("VALUE", ElementParameterParser.getValue(pNode, "__GOOGLE_PROJECT_ID__"));
            sparkAdvancedProperties.add(gsProperties);

            gsProperties = new HashMap<String, String>();
            gsProperties.put("PROPERTY", "\"spark.hadoop.google.cloud.auth.service.account.enable\"");
            gsProperties.put("VALUE", "\"true\"");
            sparkAdvancedProperties.add(gsProperties);

            boolean useP12CredentialsFileFormat = ElementParameterParser.getBooleanValue(pNode, "__SET_P12_CREDENTIALS_FILE__");

            if(!useP12CredentialsFileFormat) {
                gsProperties = new HashMap<String, String>();
                gsProperties.put("PROPERTY", "\"spark.hadoop.google.cloud.auth.service.account.json.keyfile\"");
                gsProperties.put("VALUE", ElementParameterParser.getValue(pNode, "__PATH_TO_GOOGLE_CREDENTIALS__"));
                sparkAdvancedProperties.add(gsProperties);
            } else {
                gsProperties = new HashMap<String, String>();
                gsProperties.put("PROPERTY", "\"spark.hadoop.google.cloud.auth.service.account.email\"");
                gsProperties.put("VALUE", ElementParameterParser.getValue(pNode, "__SERVICE_ACCOUNT_ID__"));
                sparkAdvancedProperties.add(gsProperties);

                gsProperties = new HashMap<String, String>();
                gsProperties.put("PROPERTY", "\"spark.hadoop.google.cloud.auth.service.account.keyfile\"");
                gsProperties.put("VALUE", ElementParameterParser.getValue(pNode, "__SERVICE_ACCOUNT_P12_KEYFILE__"));
                sparkAdvancedProperties.add(gsProperties);
            }
        }
    }

    stringBuffer.append(TEXT_6);
    
List<INode> hiveNodes = new ArrayList<INode>(process.getNodesOfType("tHiveConfiguration"));

if ((hiveNodes.size() > 0) && !useLocalMode && ("MAPR".equals(sparkDistribution))) {
    Map<String, String> hiveProperties = null;
    hiveProperties = new HashMap<String, String>();
    hiveProperties.put("PROPERTY", "\"spark.sql.hive.metastore.sharedPrefixes\"");
    hiveProperties.put("VALUE", "\"com.mysql.jdbc,org.postgresql,com.microsoft.sqlserver,oracle.jdbc,com.mapr.fs.shim.LibraryLoader,com.mapr.security.JNISecurity,com.mapr.fs.jni\"");
    sparkAdvancedProperties.add(hiveProperties);
}
if ((hiveNodes.size() > 0) && !useLocalMode && sparkBatchDistrib.doRequireMetastoreVersionOverride()
        && (sparkBatchDistrib.getHiveMetastoreVersionForSpark() != null)) {
    String sparkHiveVersion = sparkBatchDistrib.getHiveMetastoreVersionForSpark();
    Map<String, String> hiveMetastoreVersionProperty = null;
    hiveMetastoreVersionProperty = new HashMap<String, String>();
    hiveMetastoreVersionProperty.put("PROPERTY", "\"spark.sql.hive.metastore.version\"");
    hiveMetastoreVersionProperty.put("VALUE", "\"" + sparkHiveVersion + "\"");
    sparkAdvancedProperties.add(hiveMetastoreVersionProperty);

    Map<String, String> hiveMetastoreJarsProperty = null;
    hiveMetastoreJarsProperty = new HashMap<String, String>();
    hiveMetastoreJarsProperty.put("PROPERTY", "\"spark.sql.hive.metastore.jars\"");
    hiveMetastoreJarsProperty.put("VALUE", "\"maven\"");
    sparkAdvancedProperties.add(hiveMetastoreJarsProperty);
}

    stringBuffer.append(TEXT_7);
    
List<INode> tTachyonNodes = new ArrayList<INode>(process.getNodesOfType("tTachyonConfiguration"));

if(tTachyonNodes.size() > 1) {

    stringBuffer.append(TEXT_8);
    
}
if(tTachyonNodes.size() == 1) {
    INode pNode = tTachyonNodes.get(0);
    // the configuration components must not be considered as a root node.
    rootNodes.remove(pNode);

    Map<String, String> tachyonProperties = new HashMap<String, String>();
    tachyonProperties.put("PROPERTY", "\"spark.hadoop.fs.tachyon.impl\"");
    tachyonProperties.put("VALUE", "\"tachyon.hadoop.TFS\"");
    sparkAdvancedProperties.add(tachyonProperties);

    //Set underFS username
    //May conflict with the username set HDFSConfiguration
    //The username is used when the Spark worker is not on the same node as the tachyon worker, in this case the the write goes directly
    // to the UnderFS which requires a authentication (in the case of HDFS).
    username = ElementParameterParser.getValue(pNode, "__UNDERFS_USERNAME__");

}

    stringBuffer.append(TEXT_9);
    
List<INode> hdfsNodes = new ArrayList<INode>(process.getNodesOfType("tHDFSConfiguration"));

if(hdfsNodes.size() > 1) {

    stringBuffer.append(TEXT_10);
    
}
if(hdfsNodes.size() == 1) {
    INode pNode = hdfsNodes.get(0);
    // the configuration components must not be considered as a root node.
    rootNodes.remove(pNode);

    Map<String, String> hdfsProperties = new HashMap<String, String>();
    hdfsProperties.put("PROPERTY", "\"spark.hadoop.fs.defaultFS\"");
    hdfsProperties.put("VALUE", ElementParameterParser.getValue(pNode, "__FS_DEFAULT_NAME__"));
    sparkAdvancedProperties.add(hdfsProperties);

    String hadoopDistribution = ElementParameterParser.getValue(pNode, "__DISTRIBUTION__");
    String hadoopVersion = ElementParameterParser.getValue(pNode, "__DB_VERSION__");

    org.talend.hadoop.distribution.component.HDFSComponent hdfsDistrib = null;
    try {
        hdfsDistrib = (org.talend.hadoop.distribution.component.HDFSComponent) org.talend.hadoop.distribution.DistributionFactory.buildDistribution(hadoopDistribution, hadoopVersion);
    } catch (java.lang.Exception e) {
        e.printStackTrace();
        return "";
    }

    boolean mrUseDatanodeHostname = "true".equals(ElementParameterParser.getValue(pNode, "__USE_DATANODE_HOSTNAME__")) && hdfsDistrib.doSupportUseDatanodeHostname();
    if(mrUseDatanodeHostname) {
        hdfsProperties = new HashMap<String, String>();
        hdfsProperties.put("PROPERTY", "\"spark.hadoop.dfs.client.use.datanode.hostname\"");
        hdfsProperties.put("VALUE", "\"true\"");
        sparkAdvancedProperties.add(hdfsProperties);
    }

    useKrb = "true".equals(ElementParameterParser.getValue(pNode, "__USE_KRB__")) && (hdfsDistrib.doSupportKerberos());
    if(useKrb) {
        hdfsProperties = new HashMap<String, String>();
        hdfsProperties.put("PROPERTY", "\"spark.hadoop.dfs.namenode.kerberos.principal\"");
        hdfsProperties.put("VALUE", ElementParameterParser.getValue(pNode, "__NAMENODE_PRINCIPAL__"));
        sparkAdvancedProperties.add(hdfsProperties);
        hdfsProperties = new HashMap<String, String>();
        hdfsProperties.put("PROPERTY", "\"spark.hadoop.hadoop.security.authorization\"");
        hdfsProperties.put("VALUE", "\"true\"");
        sparkAdvancedProperties.add(hdfsProperties);
        hdfsProperties = new HashMap<String, String>();
        hdfsProperties.put("PROPERTY", "\"spark.hadoop.hadoop.security.authentication\"");
        hdfsProperties.put("VALUE", "\"kerberos\"");
        sparkAdvancedProperties.add(hdfsProperties);
        useKeytab = "true".equals(ElementParameterParser.getValue(pNode, "__USE_KEYTAB__"));
        if(useKeytab) {
            keytabPrincipal = ElementParameterParser.getValue(pNode, "__PRINCIPAL__");
            keytabPath = ElementParameterParser.getValue(pNode, "__KEYTAB_PATH__");
        }
    } else {
        username = ElementParameterParser.getValue(pNode, "__USERNAME__");
    }

    List<Map<String, String>> hadoopProps = (List<Map<String,String>>)ElementParameterParser.getObjectValue(pNode, "__HADOOP_ADVANCED_PROPERTIES__");
    for(Map<String, String> item : hadoopProps){
        hdfsProperties = new HashMap<String, String>();
        hdfsProperties.put("PROPERTY", "\"spark.hadoop.\" + " + item.get("PROPERTY"));
        hdfsProperties.put("VALUE", item.get("VALUE"));
        sparkAdvancedProperties.add(hdfsProperties);
    }

    boolean useHDFSEnc = ElementParameterParser.getBooleanValue(pNode, "__USE_HDFS_ENCRYPTION__");
    String hdfsKMS = ElementParameterParser.getValue(pNode, "__HDFS_ENCRYPTION_KEY_PROVIDER__");
    if(useHDFSEnc){
        hdfsProperties = new HashMap<String, String>();
        hdfsProperties.put("PROPERTY", "\"spark.hadoop.hadoop.security.key.provider.path\"");
        hdfsProperties.put("VALUE", hdfsKMS);
        sparkAdvancedProperties.add(hdfsProperties);
        hdfsProperties = new HashMap<String, String>();
        hdfsProperties.put("PROPERTY", "\"spark.hadoop.dfs.encryption.key.provider.uri\"");
        hdfsProperties.put("VALUE", hdfsKMS);
        sparkAdvancedProperties.add(hdfsProperties);
    }

}

    stringBuffer.append(TEXT_11);
    
List<INode> hbaseNodes = new ArrayList<INode>(process.getNodesOfType("tHBaseConfiguration"));

if(hbaseNodes.size() > 1) {

    stringBuffer.append(TEXT_12);
    
}
if(hbaseNodes.size() == 1) { // for HDP 2.5 only
    INode pNode = hbaseNodes.get(0);
    // the configuration components must not be considered as a root node.
    rootNodes.remove(pNode);
    
    if("HDP_2_5".equals(ElementParameterParser.getValue(pNode, "__HBASE_VERSION__")) &&
            "true".equals(ElementParameterParser.getValue(pNode, "__SET_ZNODE_PARENT__"))){
        Map<String, String> hbaseProperties = new HashMap<String, String>();
        hbaseProperties.put("PROPERTY", "\"spark.hadoop.zookeeper.znode.parent\"");
        hbaseProperties.put("VALUE", ElementParameterParser.getValue(pNode, "__ZNODE_PARENT__"));
        sparkAdvancedProperties.add(hbaseProperties);
    }

}

    stringBuffer.append(TEXT_13);
    
List<INode> cassandraNodes = new ArrayList<INode>(process.getNodesOfType("tCassandraConfiguration"));

if(cassandraNodes.size() > 1) {

    stringBuffer.append(TEXT_14);
    
}
if(cassandraNodes.size() == 1) {
    INode pNode = cassandraNodes.get(0);
    // the configuration components must not be considered as a root node.
    rootNodes.remove(pNode);
    
    
class CassandraConfiguration_Helper{
	public Map<String, String> getProperties(INode node){
		java.util.Map<String, String> properties = new java.util.HashMap<String, String>();
		
        java.util.List<java.util.Map<String, String>> configurations = (java.util.List<java.util.Map<String, String>>)ElementParameterParser.getObjectValue(node, "__CASSANDRA_CONFIGURATION__");
        //remove some key from the configuration table, but can remove it from migration task, so ignore them on code generate stage
        java.util.List<String> ignoreConfList = new java.util.ArrayList<String>();
        ignoreConfList.add("connection_rpc_port");//"spark.cassandra.connection.rpc.port"
		ignoreConfList.add("connection_native_port");//"spark.cassandra.connection.native.port"
        java.util.Map<String, String> confMapping = new java.util.HashMap<String, String>();
        confMapping.put("connection_conf_factory","spark.cassandra.connection.conf.factory");
        confMapping.put("connection_keep_alive_ms","spark.cassandra.connection.keep_alive_ms");
        confMapping.put("connection_timeout_ms","spark.cassandra.connection.timeout_ms");
        confMapping.put("reconnection_delay_ms_min","spark.cassandra.connection.reconnection_delay_ms.min");
        confMapping.put("connection_reconnection_delay_ms_max","spark.cassandra.connection.reconnection_delay_ms.max");
        confMapping.put("connection_local_dc","spark.cassandra.connection.local_dc");
        confMapping.put("auth_conf_factory","spark.cassandra.auth.conf.factory");
        confMapping.put("query_retry_count","spark.cassandra.query.retry.count");
        confMapping.put("read_timeout_ms","spark.cassandra.read.timeout_ms");   
        confMapping.put("input_split_size","spark.cassandra.input.split.size");
        confMapping.put("input_page_row_size","spark.cassandra.input.page.row.size");
        confMapping.put("input_consistency_level","spark.cassandra.input.consistency.level");
        for(java.util.Map<String, String> conf : configurations){
            String confKey = conf.get("KEY");
            if(ignoreConfList.contains(confKey)){
            	continue;
            }
            String propertyKey = confMapping.containsKey(confKey) ? "\"" + confMapping.get(confKey) + "\"" : confKey;
            properties.put(propertyKey, conf.get("VALUE"));
        }
        String host = ElementParameterParser.getValue(node,"__HOST__");
        if(!"".equals(host)){
        	properties.put("\"spark.cassandra.connection.host\"", host);
        }
        String port = ElementParameterParser.getValue(node,"__PORT__");
        if(!"".equals(port)){
        	properties.put("\"spark.cassandra.connection.port\"", port);
        }
        boolean authentication="true".equalsIgnoreCase(ElementParameterParser.getValue(node, "__REQUIRED_AUTHENTICATION__"));
        String userName = ElementParameterParser.getValue(node, "__USERNAME__");
        String passWord = ElementParameterParser.getPasswordValue(node, "__PASSWORD__");
        if(authentication){
            properties.put("\"spark.cassandra.auth.username\"", userName);
            properties.put("\"spark.cassandra.auth.password\"", passWord);
        }  
        TSetKeystoreUtil tSetKeystoreUtil = new TSetKeystoreUtil(node);
        if(tSetKeystoreUtil.useHTTPS()){
        	properties.put("\"spark.cassandra.connection.ssl.enabled\"", "\"true\"");
        	properties.put("\"spark.cassandra.connection.ssl.trustStore.type\"", tSetKeystoreUtil.getTrustStoreType());
        	properties.put("\"spark.cassandra.connection.ssl.trustStore.path\"", tSetKeystoreUtil.getTrustStorePath());
        	properties.put("\"spark.cassandra.connection.ssl.trustStore.password\"", tSetKeystoreUtil.getTrustStorePassword());
        	if(tSetKeystoreUtil.needClientAuth()){
        		properties.put("\"spark.cassandra.connection.ssl.clientAuth.enabled\"", "\"true\"");
	        	properties.put("\"spark.cassandra.connection.ssl.keyStore.type\"", tSetKeystoreUtil.getKeyStoreType());
	        	properties.put("\"spark.cassandra.connection.ssl.keyStore.path\"", tSetKeystoreUtil.getKeyStorePath());
	        	properties.put("\"spark.cassandra.connection.ssl.keyStore.password\"", tSetKeystoreUtil.getKeyStorePassword());
        	}
        }
        return properties; 
	}
	
	public Map<String, String> getPropertiesForOutput(INode node){
		java.util.Map<String, String> properties = new java.util.HashMap<String, String>();
		
        java.util.List<java.util.Map<String, String>> configurations = (java.util.List<java.util.Map<String, String>>)ElementParameterParser.getObjectValue(node, "__CASSANDRA_CONFIGURATION__");
        java.util.Map<String, String> confMapping = new java.util.HashMap<String, String>();
        confMapping.put("output_batch_size_rows","spark.cassandra.output.batch.size.rows");
        confMapping.put("output_batch_size_bytes","spark.cassandra.output.batch.size.bytes");
        confMapping.put("output_batch_grouping_key","spark.cassandra.output.batch.grouping.key");
        confMapping.put("output_batch_buffer_size","spark.cassandra.output.batch.buffer.size");
        confMapping.put("output_concurrent_writes","spark.cassandra.output.concurrent.writes");
        confMapping.put("output_consistency_level","spark.cassandra.output.consistency.level");
        confMapping.put("output_throughput_mb_per_sec","spark.cassandra.output.throughput_mb_per_sec");
        for(java.util.Map<String, String> conf : configurations){
            String confKey = conf.get("KEY");
            String propertyKey = confMapping.containsKey(confKey) ? "\"" + confMapping.get(confKey) + "\"" : confKey;
            properties.put(propertyKey, conf.get("VALUE"));
        }
        
        return properties; 
	}
}	

    
    Map<String, String> cassandraProperties = (new CassandraConfiguration_Helper()).getProperties(pNode);
    for(String cPropKey : cassandraProperties.keySet()){
        Map<String, String> cProperty = new HashMap<String, String>();
        cProperty.put("PROPERTY", cPropKey);
        cProperty.put("VALUE", cassandraProperties.get(cPropKey));
        sparkAdvancedProperties.add(cProperty);
    }
}

    stringBuffer.append(TEXT_15);
    
List<INode> bigQueryNodes = new ArrayList<INode>(process.getNodesOfType("tBigQueryConfiguration"));

if(bigQueryNodes.size() > 1) {

    stringBuffer.append(TEXT_16);
    
}
if(bigQueryNodes.size() == 1) {
    INode pNode = bigQueryNodes.get(0);
    // the configuration components must not be considered as a root node.
    rootNodes.remove(pNode);

    // If dataproc cluster, use dataproc's project id and authenication settings
    Map<String, String> bigqueryProperties = new HashMap<String, String>();
    if(!isGoogleDataprocDistribution) {
        bigqueryProperties.put("PROPERTY", "\"spark.hadoop.google.cloud.auth.service.account.enable\"");
        bigqueryProperties.put("VALUE", "\"true\"");
        sparkAdvancedProperties.add(bigqueryProperties);

        boolean enableP12 = ElementParameterParser.getBooleanValue(pNode, "__SET_P12_CREDENTIALS_FILE__");
        if(enableP12){
            bigqueryProperties = new HashMap<String, String>();
            bigqueryProperties.put("PROPERTY", "\"spark.hadoop.google.cloud.auth.service.account.email\"");
            bigqueryProperties.put("VALUE", ElementParameterParser.getValue(pNode, "__SERVICE_ACCOUNT_ID__"));
            sparkAdvancedProperties.add(bigqueryProperties);

            bigqueryProperties = new HashMap<String, String>();
            bigqueryProperties.put("PROPERTY", "\"spark.hadoop.google.cloud.auth.service.account.keyfile\"");
            bigqueryProperties.put("VALUE", ElementParameterParser.getValue(pNode, "__SERVICE_ACCOUNT_P12_KEYFILE__"));
            sparkAdvancedProperties.add(bigqueryProperties);
        }else{
            bigqueryProperties = new HashMap<String, String>();
            bigqueryProperties.put("PROPERTY", "\"spark.hadoop.google.cloud.auth.service.account.json.keyfile\"");
            bigqueryProperties.put("VALUE", ElementParameterParser.getValue(pNode, "__PATH_TO_GOOGLE_CREDENTIALS__"));
            sparkAdvancedProperties.add(bigqueryProperties);
        }
    }
    
    String dataLocation = "\"" + ElementParameterParser.getValue(pNode, "__LOCATION__") + "\"";
    bigqueryProperties = new HashMap<String, String>();
    bigqueryProperties.put("PROPERTY", "\"spark.hadoop.mapred.bq.output.location\"");
    bigqueryProperties.put("VALUE", dataLocation);
    sparkAdvancedProperties.add(bigqueryProperties);

    bigqueryProperties = new HashMap<String, String>();
    bigqueryProperties.put("PROPERTY", "\"spark.hadoop.fs.gs.impl\"");
    bigqueryProperties.put("VALUE", "\"com.google.cloud.hadoop.fs.gcs.GoogleHadoopFileSystem\"");
    sparkAdvancedProperties.add(bigqueryProperties);

    bigqueryProperties = new HashMap<String, String>();
    bigqueryProperties.put("PROPERTY", "\"spark.hadoop.fs.AbstractFileSystem.gs.impl\"");
    bigqueryProperties.put("VALUE", "\"com.google.cloud.hadoop.fs.gcs.GoogleHadoopFS\"");
    sparkAdvancedProperties.add(bigqueryProperties);

    String googleProjectId = isGoogleDataprocDistribution ? ElementParameterParser.getValue(sparkConfig, "__GOOGLE_PROJECT_ID__") : ElementParameterParser.getValue(pNode, "__GOOGLE_PROJECT_ID__");
    bigqueryProperties = new HashMap<String, String>();
    bigqueryProperties.put("PROPERTY", "\"spark.hadoop.fs.gs.project.id\"");
    bigqueryProperties.put("VALUE", googleProjectId);
    sparkAdvancedProperties.add(bigqueryProperties);

    bigqueryProperties = new HashMap<String, String>();
    bigqueryProperties.put("PROPERTY", "\"spark.hadoop.mapred.bq.project.id\"");
    bigqueryProperties.put("VALUE", googleProjectId);
    sparkAdvancedProperties.add(bigqueryProperties);
}

    stringBuffer.append(TEXT_17);
    
    if(isThereAtLeastOneMLComponent) {

    
	// Everything related to our TalendPipelineModel used in machine learning jobs 
	// should be written there to avoid code duplication.
	
	// This file is intended to be included in both spark_footer.javajet and sparkstreaming_footer.javajet

    stringBuffer.append(TEXT_18);
    
	// Everything related to our TalendPipeline used in machine learning jobs 
	// should be written there to avoid code duplication.
	
	// This file is intended to be included in both spark_footer.javajet and sparkstreaming_footer.javajet

    stringBuffer.append(TEXT_19);
    
    }

    stringBuffer.append(TEXT_20);
    
	// Kryo registrator and all custom kryo serializers should be written there to avoid code duplication
	
	// This file is intended to be included in both spark_footer.javajet and sparkstreaming_footer.javajet

    stringBuffer.append(TEXT_21);
     
	boolean hasAvroGenericRecord = false;
	for (INode rootNode : rootNodes) {
		if(rootNode.getComponent().getName() != null && (rootNode.getComponent().getName().startsWith("tHMap") || rootNode.getComponent().getName().startsWith("tHConvert"))) {
			hasAvroGenericRecord = true;
			break;
		}
	}
	if(hasAvroGenericRecord) {

    stringBuffer.append(TEXT_22);
    
	} // end if(hasAvroGenericRecord)

    stringBuffer.append(TEXT_23);
     
		if(isThereAtLeastOneMLComponent) {
	
    stringBuffer.append(TEXT_24);
    
		}

		Iterable<String> structNameList = (Iterable<String>) v.get(3);		
		for(String structName : structNameList) {
	
    stringBuffer.append(TEXT_25);
    stringBuffer.append(structName);
    stringBuffer.append(TEXT_26);
    
		} 
	
    stringBuffer.append(TEXT_27);
     
	if(isThereAtLeastOneMLComponent) {

    stringBuffer.append(TEXT_28);
     
	} // end if(isThereAtLeastOneMLComponent)

    stringBuffer.append(TEXT_29);
     if(useCloudLauncher || useYarnClusterMode) { 
    stringBuffer.append(TEXT_30);
     } 
    stringBuffer.append(TEXT_31);
    stringBuffer.append(codeGenArgument.getContextName());
    stringBuffer.append(TEXT_32);
    if(useCloudLauncher || useYarnClusterMode){
    stringBuffer.append(TEXT_33);
    }
    stringBuffer.append(TEXT_34);
    stringBuffer.append(className );
    stringBuffer.append(TEXT_35);
    if(isLog4jEnabled){
    stringBuffer.append(TEXT_36);
    stringBuffer.append(codeGenArgument.getJobName());
    stringBuffer.append(TEXT_37);
    stringBuffer.append(codeGenArgument.getJobName());
    stringBuffer.append(TEXT_38);
    }
    stringBuffer.append(TEXT_39);
     if(isQuboleDistribution || isDatabricksDistribution || isExecutedThroughLivy || isAltusDistribution || useYarnClusterMode) { 
    stringBuffer.append(TEXT_40);
     } 
    stringBuffer.append(TEXT_41);
     if(isQuboleDistribution || isDatabricksDistribution || isExecutedThroughLivy || isAltusDistribution || useYarnClusterMode) { 
    stringBuffer.append(TEXT_42);
     } 
    stringBuffer.append(TEXT_43);
    
    if (isTestContainer) {
        List<String> instanceList =  ProcessUtils.getTestInstances(process);
        for(String instance : instanceList) {
            String context = ProcessUtils.getInstanceContext(process,instance);
            
    stringBuffer.append(TEXT_44);
    stringBuffer.append(instance);
    stringBuffer.append(TEXT_45);
    
                int assertNum = ProcessUtils.getAssertAmount(process);
                
    stringBuffer.append(TEXT_46);
    stringBuffer.append(assertNum);
    stringBuffer.append(TEXT_47);
    stringBuffer.append(className );
    stringBuffer.append(TEXT_48);
    stringBuffer.append(className );
    stringBuffer.append(TEXT_49);
    stringBuffer.append(className );
    stringBuffer.append(TEXT_50);
    stringBuffer.append(instance);
    stringBuffer.append(TEXT_51);
    stringBuffer.append(instance);
    stringBuffer.append(TEXT_52);
    stringBuffer.append(context);
    stringBuffer.append(TEXT_53);
    
                String lineSeparator = (String) java.security.AccessController.doPrivileged(
                        new sun.security.action.GetPropertyAction("line.separator"));
                for(String testDataName : ProcessUtils.getTestData(process,instance)){
                    String testData =  ProcessUtils.getTestDataValue(process, instance, testDataName);
                    if(testData==null||testData.length()<=0){
                        continue;
                    }
                    String testDataEnCodeStr = "";
                    try {
                        if (testData != null) {
                            testDataEnCodeStr = (new sun.misc.BASE64Encoder()).encode(testData.getBytes("UTF-8"));
                        }
                    } catch (java.io.UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }

                    StringBuilder testDataBase64 = new StringBuilder();
                    for(String item : testDataEnCodeStr.split(lineSeparator))
                        testDataBase64.append('"').append(item).append("\"+");
                    // Remove trailing commas.
                    if (testDataBase64.length() > 0)
                        testDataBase64.setLength(testDataBase64.length() - 1);

                    if(testDataBase64 != null && testDataBase64.length() > 0){
                        
    stringBuffer.append(TEXT_54);
    stringBuffer.append(instance);
    stringBuffer.append(TEXT_55);
    stringBuffer.append(instance);
    stringBuffer.append(TEXT_56);
    stringBuffer.append(testDataName);
    stringBuffer.append(TEXT_57);
    stringBuffer.append(testDataBase64);
    stringBuffer.append(TEXT_58);
    
                    }
                }
                
    stringBuffer.append(TEXT_59);
    stringBuffer.append(instance);
    stringBuffer.append(TEXT_60);
    stringBuffer.append(instance);
    stringBuffer.append(TEXT_61);
    stringBuffer.append(instance);
    stringBuffer.append(TEXT_62);
    stringBuffer.append(TEXT_63);
    stringBuffer.append(className );
    stringBuffer.append(TEXT_64);
    
        }
    } // isTestContainer
    
    stringBuffer.append(TEXT_65);
    if(isLog4jEnabled){
    stringBuffer.append(TEXT_66);
    stringBuffer.append(codeGenArgument.getJobName());
    stringBuffer.append(TEXT_67);
    }
    stringBuffer.append(TEXT_68);
    
        if(stats && !isCloudDistribution) {

    stringBuffer.append(TEXT_69);
    
        }

        if(username != null && !"".equals(username)) {

    stringBuffer.append(TEXT_70);
    stringBuffer.append(username);
    stringBuffer.append(TEXT_71);
    stringBuffer.append(username);
    stringBuffer.append(TEXT_72);
    
        }
        // this snippet to solve an issue in Spark Driver on MacOSX with snappy
        // you have to run with -Dorg.xerial.snappy.lib.name=libsnappyjava.jnilib

    stringBuffer.append(TEXT_73);
    
        if(sparkConfig!=null) {

    stringBuffer.append(TEXT_74);
    
                if(hcmNodes.size() == 1) {
                    INode pNode = hcmNodes.get(0);

    stringBuffer.append(TEXT_75);
    stringBuffer.append(pNode.getUniqueName());
    stringBuffer.append(TEXT_76);
    
                }

    stringBuffer.append(TEXT_77);
    
                if(isExecutedThroughSparkJobServer) {

    stringBuffer.append(TEXT_78);
    
                } else {
                    if(useCloudLauncher || useYarnClusterMode) {

    stringBuffer.append(TEXT_79);
    
                            if(!useLocalMode && sparkUseKrb && (isCustom || sparkBatchDistrib != null && sparkBatchDistrib.doSupportKerberos()) && !isSpark13) {
                                // The following part of code is needed to spawn the thread for the ticket renewal in a secured environment.
                                // Fail silently or log a warning if log4j is activated.

    stringBuffer.append(TEXT_80);
     if(isLog4jEnabled) { 
    stringBuffer.append(TEXT_81);
     } 
    stringBuffer.append(TEXT_82);
    
                            }
                            if(isDatabricksDistribution) {
                            if (isSpark2x){

    stringBuffer.append(TEXT_83);
    
                            } else {

    stringBuffer.append(TEXT_84);
    
                            }
                            } else {
							if (isSpark2x){

    stringBuffer.append(TEXT_85);
    
							} else {

    stringBuffer.append(TEXT_86);
    
							}
                            }
							if((stats || isLog4jEnabled) && !isCloudDistribution) {

    stringBuffer.append(TEXT_87);
    
   										if(isAboveSpark230) {

    stringBuffer.append(TEXT_88);
    stringBuffer.append(sparkStatsPollingRate );
    stringBuffer.append(TEXT_89);
    
   										}

    stringBuffer.append(TEXT_90);
    
   							}
							if (isSpark2x){

    stringBuffer.append(TEXT_91);
    
							} else {

    stringBuffer.append(TEXT_92);
    
							}

    stringBuffer.append(TEXT_93);
    
                    } else { // normal case no livy, no dataproc, no yarn cluster
                        if(!useLocalMode && sparkUseKrb && (isCustom || sparkBatchDistrib != null && sparkBatchDistrib.doSupportKerberos()) && !isSpark13) {
                            // The following part of code is needed to spawn the thread for the ticket renewal in a secured environment.
                            // Fail silently or log a warning if log4j is activated.

    stringBuffer.append(TEXT_94);
     if(isLog4jEnabled) { 
    stringBuffer.append(TEXT_95);
     } 
    stringBuffer.append(TEXT_96);
    
                        }
	                    if(isDatabricksDistribution) {
                        if (isSpark2x){

    stringBuffer.append(TEXT_97);
    
                        } else {

    stringBuffer.append(TEXT_98);
    
                        }
                        } else {
                        if (isSpark2x){

    stringBuffer.append(TEXT_99);
    
                        } else {

    stringBuffer.append(TEXT_100);
    
                        }
                        }
                        if((stats || isLog4jEnabled) && !isCloudDistribution) {

    stringBuffer.append(TEXT_101);
    
									if(isAboveSpark230) {

    stringBuffer.append(TEXT_102);
    stringBuffer.append(sparkStatsPollingRate );
    stringBuffer.append(TEXT_103);
    
									}

    stringBuffer.append(TEXT_104);
    
						}
                        if (isSpark2x){

    stringBuffer.append(TEXT_105);
    
						} else {

    stringBuffer.append(TEXT_106);
    
						}
                    }
                } // else part of if(isExecutedThroughSparkJobServer)

    stringBuffer.append(TEXT_107);
    
        }

    stringBuffer.append(TEXT_108);
    
class TalendLineageAPI{

    /**
    * Find the configuration node given the current node
    * 
    * @param node
    **/
    public INode findConfigurationNode(INode node){
        INode configurationNode = null;
        for (INode pNode : node.getProcess().getNodesOfType("tMRConfiguration")) {
            configurationNode = pNode;
            break;
        }
        for (INode pNode : node.getProcess().getNodesOfType("tSparkConfiguration")) {
            // spark compatibility, will not be run on Map Reduce
            configurationNode = pNode;
            break;
        }
        return configurationNode;
    }

    /**
     * Does the job require lineage generation
     */
    public boolean doRequireLineageSupport(INode node){
        INode configurationNode = findConfigurationNode(node);
        if (configurationNode != null) {
            Boolean useClouderaNavigator = ElementParameterParser.getBooleanValue(configurationNode,"__USE_CLOUDERA_NAVIGATOR__");
            Boolean useAtlas = ElementParameterParser.getBooleanValue(configurationNode,"__USE_ATLAS__");
            return (useClouderaNavigator && doRequireClouderaNavigatorSupport(configurationNode)) || (useAtlas && doRequireAtlasSupport(configurationNode));
        }
        return false;
    }

    /**
     * Does the job require Basic Atlas authentification
     */
    public boolean doRequireBasicAtlasAuthentification(INode configurationNode){
        String distribution = ElementParameterParser.getValue(configurationNode,"__DISTRIBUTION__");
        String version = ElementParameterParser.getValue(configurationNode,"__MR_VERSION__");
        // spark compatibility
        if ((version == null) || ("".equals(version))) { 
            version = ElementParameterParser.getValue(configurationNode,"__SPARK_VERSION__");
        }
        try {
            org.talend.hadoop.distribution.component.MRComponent currentDistribution = (org.talend.hadoop.distribution.component.MRComponent)org.talend.hadoop.distribution.DistributionFactory.buildDistribution(distribution, version);
            return currentDistribution.doSupportBasicAtlasAuthentification();
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Does the job require Cloudera Navigator lineage generation
     */
    public boolean doRequireClouderaNavigatorSupport(INode configurationNode){
        String distribution = ElementParameterParser.getValue(configurationNode,"__DISTRIBUTION__");
        String version = ElementParameterParser.getValue(configurationNode,"__MR_VERSION__");
        Boolean useClouderaNavigator = ElementParameterParser.getBooleanValue(configurationNode,"__USE_CLOUDERA_NAVIGATOR__");
        // spark compatibility
        if ((version == null) || ("".equals(version))) { 
            version = ElementParameterParser.getValue(configurationNode,"__SPARK_VERSION__");
        }
        try {
            org.talend.hadoop.distribution.component.MRComponent currentDistribution = (org.talend.hadoop.distribution.component.MRComponent)org.talend.hadoop.distribution.DistributionFactory.buildDistribution(distribution, version);
            return useClouderaNavigator && currentDistribution.doSupportClouderaNavigator();
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Does the job require Atlas lineage generation
     */
    public boolean doRequireAtlasSupport(INode configurationNode){
        String distribution = ElementParameterParser.getValue(configurationNode,"__DISTRIBUTION__");
        String version = ElementParameterParser.getValue(configurationNode,"__MR_VERSION__");
        Boolean useAtlas = ElementParameterParser.getBooleanValue(configurationNode,"__USE_ATLAS__");
        // spark compatibility
        if ((version == null) || ("".equals(version))) { 
           version = ElementParameterParser.getValue(configurationNode,"__SPARK_VERSION__");
        }
        try {
            org.talend.hadoop.distribution.component.MRComponent currentDistribution = (org.talend.hadoop.distribution.component.MRComponent)org.talend.hadoop.distribution.DistributionFactory.buildDistribution(distribution, version);
            return useAtlas && currentDistribution.doSupportAtlas();
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Get Cloudera Navigator API version
     */
    public int getClouderaNavigatorAPIVersion(INode configurationNode){
        String distribution = ElementParameterParser.getValue(configurationNode,"__DISTRIBUTION__");
        String version = ElementParameterParser.getValue(configurationNode,"__MR_VERSION__");
        // spark compatibility
        if ((version == null) || ("".equals(version))) { 
           version = ElementParameterParser.getValue(configurationNode,"__SPARK_VERSION__");
        }
        try {
            org.talend.hadoop.distribution.component.MRComponent currentDistribution = (org.talend.hadoop.distribution.component.MRComponent)org.talend.hadoop.distribution.DistributionFactory.buildDistribution(distribution, version);
            return currentDistribution.getClouderaNavigatorAPIVersion();
        } catch (Exception e) {
            return 8;
        }
    }

    /**
    *
    * generates a Cloudera Navigator lineage creator
    *
    */
    public void generateClouderaNavigatorLinageCreator(INode configurationNode) {
        String usernameCN = ElementParameterParser.getValue(configurationNode,"__CLOUDERA_NAVIGATOR_USERNAME__");
        String urlCN = ElementParameterParser.getValue(configurationNode,"__CLOUDERA_NAVIGATOR_URL__");
        String urlMetadataCN = ElementParameterParser.getValue(configurationNode,"__CLOUDERA_NAVIGATOR_METADATA_URL__");
        String clientUrlCN = ElementParameterParser.getValue(configurationNode,"__CLOUDERA_NAVIGATOR_CLIENT_URL__");
        Boolean useAutocommit = ElementParameterParser.getBooleanValue(configurationNode,"__CLOUDERA_NAVIGATOR_AUTOCOMMIT__");
        Boolean disableSslValidation = ElementParameterParser.getBooleanValue(configurationNode,"__CLOUDERA_NAVIGATOR_DISABLE_SSL_VALIDATION__");
        int apiVersion = getClouderaNavigatorAPIVersion(configurationNode);

        if (ElementParameterParser.canEncrypt(configurationNode, "__CLOUDERA_NAVIGATOR_PASSWORD__")) {
            
    stringBuffer.append(TEXT_109);
    stringBuffer.append(ElementParameterParser.getEncryptedValue(configurationNode, "__CLOUDERA_NAVIGATOR_PASSWORD__"));
    stringBuffer.append(TEXT_110);
    
        } else {
            
    stringBuffer.append(TEXT_111);
    stringBuffer.append( ElementParameterParser.getValue(configurationNode, "__CLOUDERA_NAVIGATOR_PASSWORD__"));
    stringBuffer.append(TEXT_112);
    
        }
        
    stringBuffer.append(TEXT_113);
    stringBuffer.append(TEXT_114);
    stringBuffer.append(clientUrlCN);
    stringBuffer.append(TEXT_115);
    stringBuffer.append(TEXT_116);
    stringBuffer.append(urlCN);
    stringBuffer.append(TEXT_117);
    stringBuffer.append(TEXT_118);
    stringBuffer.append(urlMetadataCN);
    stringBuffer.append(TEXT_119);
    stringBuffer.append(TEXT_120);
    stringBuffer.append(usernameCN);
    stringBuffer.append(TEXT_121);
    stringBuffer.append(TEXT_122);
    stringBuffer.append(useAutocommit);
    stringBuffer.append(TEXT_123);
    stringBuffer.append(TEXT_124);
    stringBuffer.append(disableSslValidation);
    stringBuffer.append(TEXT_125);
    stringBuffer.append(TEXT_126);
    stringBuffer.append(apiVersion);
    stringBuffer.append(TEXT_127);
    
    }

    /**
    *
    * generates an Atlas lineage creator
    *
    */
    public void generateAtlasLinageCreator(INode configurationNode) {
        boolean doSetAtlasApplicationProperties = ElementParameterParser.getBooleanValue(configurationNode,"__SET_ATLAS_APPLICATION_PROPERTIES__");
        if(doSetAtlasApplicationProperties){
            String atlasApplicationPropertiesFilePath = ElementParameterParser.getValue(configurationNode,"__ATLAS_APPLICATION_PROPERTIES__");
            
    stringBuffer.append(TEXT_128);
    stringBuffer.append(atlasApplicationPropertiesFilePath);
    stringBuffer.append(TEXT_129);
    
        }
        String atlasURL = ElementParameterParser.getValue(configurationNode,"__ATLAS_URL__");
        if(doRequireBasicAtlasAuthentification(configurationNode)){
            String atlasUsername = ElementParameterParser.getValue(configurationNode,"__ATLAS_USERNAME__");
            String passwordFieldName = "__ATLAS_PASSWORD__";
            if (ElementParameterParser.canEncrypt(configurationNode, passwordFieldName)) {
                
    stringBuffer.append(TEXT_130);
    stringBuffer.append(ElementParameterParser.getEncryptedValue(configurationNode, passwordFieldName));
    stringBuffer.append(TEXT_131);
    
            } else {
                
    stringBuffer.append(TEXT_132);
    stringBuffer.append( ElementParameterParser.getValue(configurationNode, passwordFieldName));
    stringBuffer.append(TEXT_133);
    
            }
            
    stringBuffer.append(TEXT_134);
    stringBuffer.append(atlasURL);
    stringBuffer.append(TEXT_135);
    stringBuffer.append(atlasUsername);
    stringBuffer.append(TEXT_136);
    
        } else {
            
    stringBuffer.append(TEXT_137);
    stringBuffer.append(atlasURL);
    stringBuffer.append(TEXT_138);
    
        }
        
    stringBuffer.append(TEXT_139);
    
    }

    /**
    * returns the dieOnError value
    */
    public Boolean getDieOnError(INode configurationNode){
        if(doRequireClouderaNavigatorSupport(configurationNode)){
            return ElementParameterParser.getBooleanValue(configurationNode,"__CLOUDERA_NAVIGATOR_DIE_ON_ERROR__");
        } else if (doRequireAtlasSupport(configurationNode)){
            return ElementParameterParser.getBooleanValue(configurationNode,"__ATLAS_DIE_ON_ERROR__");
        }
        return false;
    }

    /**
     * Generate a Map containing the columns information.
     * The map contain the column name as the key, and the column type as the value.
     */
    public void generateColumnList(IMetadataTable metadataTable, String cid) {
        
    stringBuffer.append(TEXT_140);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_141);
    
        for (IMetadataColumn column: metadataTable.getListColumns()) {
            
    stringBuffer.append(TEXT_142);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_143);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_144);
    stringBuffer.append(org.talend.core.model.metadata.types.JavaTypesManager.getTypeToGenerate(column.getTalendType(), column.isNullable()));
    stringBuffer.append(TEXT_145);
    
        }
    }

    /**
     * Generate a Map containing the columns information.
     * The map contain the column name as the key, and the column type as the value.
     * This function will generate the full output mapping of a component, in order to display any output field.
     * 
     * If the component contain output link, the map will be  generate from these links,
     * otherwise the metadata of the component will be used.
     */
    public void generateColumnListFromOutputLink(INode node, String cid) {
        
    stringBuffer.append(TEXT_146);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_147);
    
        if ((node.getOutgoingConnections() != null)
            && (node.getOutgoingConnections().size() > 0)) {
            for (IConnection connection: node.getOutgoingConnections()) {
                if (connection.getLineStyle().hasConnectionCategory(org.talend.core.model.process.IConnectionCategory.DATA)) {
                    for (IMetadataColumn column: connection.getMetadataTable().getListColumns()) {
                        
    stringBuffer.append(TEXT_148);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_149);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_150);
    stringBuffer.append(org.talend.core.model.metadata.types.JavaTypesManager.getTypeToGenerate(column.getTalendType(), column.isNullable()));
    stringBuffer.append(TEXT_151);
    
                    }
                }
            }
        } else  {
            for (IMetadataTable metadataTable: node.getMetadataList()) {
                for (IMetadataColumn column: metadataTable.getListColumns()) {
                    
    stringBuffer.append(TEXT_152);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_153);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_154);
    stringBuffer.append(org.talend.core.model.metadata.types.JavaTypesManager.getTypeToGenerate(column.getTalendType(), column.isNullable()));
    stringBuffer.append(TEXT_155);
    
                }
            }
        }
    }

    /**
     * Generate the code to call the method addDataset of rg.talend.cloudera.navigator.api.LineageCreator.
     * 
     * @param cid the cid of the current component
     * @param componentName name of the graphical component
     * @param folderPath The path to the folder containing data into HDFS
     * @param folderType The type of the folder, must be defined into com.cloudera.nav.sdk.model.entities.FileFormat
     */
    public void addDataset(String cid, String componentName, String folderPath, String folderType) {
        
    stringBuffer.append(TEXT_156);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_157);
    stringBuffer.append(componentName);
    stringBuffer.append(TEXT_158);
    stringBuffer.append(folderPath);
    stringBuffer.append(TEXT_159);
    stringBuffer.append(folderType);
    stringBuffer.append(TEXT_160);
    
    }

    /**
     * Generate list of input nodes for a given component.
     * 
     * @param node the currentNode
     */
    public void generateInputNodeList(INode node) {
        
    stringBuffer.append(TEXT_161);
    stringBuffer.append(node.getUniqueName());
    stringBuffer.append(TEXT_162);
    
        java.util.List<IConnection> inputs = (java.util.List<IConnection>)node.getIncomingConnections();
        for (IConnection connection: inputs) {
            if (connection.getLineStyle().hasConnectionCategory(org.talend.core.model.process.IConnectionCategory.DATA)) {
                
    stringBuffer.append(TEXT_163);
    stringBuffer.append(node.getUniqueName());
    stringBuffer.append(TEXT_164);
    stringBuffer.append(connection.getSource().getUniqueName());
    stringBuffer.append(TEXT_165);
    
            }
        }
    }

    /**
     * Generate list of output nodes for a given component.
     * 
     * @param node the currentNode
     */
    public void generateOutputNodeList(INode node) {
        
    stringBuffer.append(TEXT_166);
    stringBuffer.append(node.getUniqueName());
    stringBuffer.append(TEXT_167);
    
        java.util.List<IConnection> outputs = (java.util.List<IConnection>)node.getOutgoingConnections();
        for (IConnection connection: outputs) {
            if (connection.getLineStyle().hasConnectionCategory(org.talend.core.model.process.IConnectionCategory.DATA)) {
                
    stringBuffer.append(TEXT_168);
    stringBuffer.append(node.getUniqueName());
    stringBuffer.append(TEXT_169);
    stringBuffer.append(connection.getTarget().getUniqueName());
    stringBuffer.append(TEXT_170);
    
            }
        }
    }

    /**
     * Generate the code to call the method addNodeToLineage of rg.talend.cloudera.navigator.api.LineageCreator.
     * 
     * @param cid the cid of the current component
     */
    public void addNodeToLineage(String cid) {
        
    stringBuffer.append(TEXT_171);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_172);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_173);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_174);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_175);
    
    }

}

    
    TalendLineageAPI talendLineageAPI = new TalendLineageAPI();
    if (talendLineageAPI.doRequireLineageSupport(sparkConfig)) {
        
    stringBuffer.append(TEXT_176);
    
    }
    
    if(isSpark2x){
        
    stringBuffer.append(TEXT_177);
            
    } else {
        
    stringBuffer.append(TEXT_178);
    
    }
        
        // set checkpoint directory
        boolean useCheckpoint = ElementParameterParser.getBooleanValue(sparkConfig, "__USE_CHECKPOINT__");
        String checkpointDir = ElementParameterParser.getValue(sparkConfig, "__CHECKPOINT_DIR__");
        if(useCheckpoint){

    stringBuffer.append(TEXT_179);
    stringBuffer.append(checkpointDir);
    stringBuffer.append(TEXT_180);
    
        }
        if(useKrb) {
            if(((isCustom || sparkBatchDistrib != null && sparkBatchDistrib.doSupportMapRTicket()) && useMapRTicket)) {
                
    stringBuffer.append(TEXT_181);
    stringBuffer.append(setMapRHomeDir ? mapRHomeDir : "\"/opt/mapr\"" );
    stringBuffer.append(TEXT_182);
    stringBuffer.append(setMapRHadoopLogin ? mapRHadoopLogin : "\"kerberos\"");
    stringBuffer.append(TEXT_183);
    
            }
            if(useKeytab && !useYarnClusterMode) {

    stringBuffer.append(TEXT_184);
    stringBuffer.append(keytabPrincipal);
    stringBuffer.append(TEXT_185);
    stringBuffer.append(keytabPath);
    stringBuffer.append(TEXT_186);
    
            }
            if(((isCustom || sparkBatchDistrib != null && sparkBatchDistrib.doSupportMapRTicket()) && useMapRTicket)) {
                
    stringBuffer.append(TEXT_187);
    stringBuffer.append(mapRTicketCluster);
    stringBuffer.append(TEXT_188);
    stringBuffer.append(mapRTicketDuration);
    stringBuffer.append(TEXT_189);
    
            }
        }  else {
            // Mapr ticket
            if(((isCustom || sparkBatchDistrib != null && sparkBatchDistrib.doSupportMapRTicket()) && useMapRTicket)) {
                passwordFieldName = "__MAPRTICKET_PASSWORD__";
                
    stringBuffer.append(TEXT_190);
    stringBuffer.append(setMapRHomeDir ? mapRHomeDir : "\"/opt/mapr\"" );
    stringBuffer.append(TEXT_191);
    
                if (setMapRHadoopLogin) {
                    
    stringBuffer.append(TEXT_192);
    stringBuffer.append(mapRHadoopLogin);
    stringBuffer.append(TEXT_193);
    
                } else {
                    
    stringBuffer.append(TEXT_194);
    
                }
                INode node = sparkConfig;
                
    if (ElementParameterParser.canEncrypt(node, passwordFieldName)) {
    stringBuffer.append(TEXT_195);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_196);
    stringBuffer.append(ElementParameterParser.getEncryptedValue(node, passwordFieldName));
    stringBuffer.append(TEXT_197);
    } else {
    stringBuffer.append(TEXT_198);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_199);
    stringBuffer.append( ElementParameterParser.getValue(node, passwordFieldName));
    stringBuffer.append(TEXT_200);
    }
    stringBuffer.append(TEXT_201);
    
                if(sparkBatchDistrib.doSupportMaprTicketV52API()){
                    
    stringBuffer.append(TEXT_202);
    stringBuffer.append(mapRTicketCluster);
    stringBuffer.append(TEXT_203);
    stringBuffer.append(mapRTicketUsername);
    stringBuffer.append(TEXT_204);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_205);
    stringBuffer.append(mapRTicketDuration);
    stringBuffer.append(TEXT_206);
    
                } else {
                    
    stringBuffer.append(TEXT_207);
    stringBuffer.append(mapRTicketCluster);
    stringBuffer.append(TEXT_208);
    stringBuffer.append(mapRTicketUsername);
    stringBuffer.append(TEXT_209);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_210);
    stringBuffer.append(mapRTicketDuration);
    stringBuffer.append(TEXT_211);
    
                }
            }
        }

        if(stats && !isCloudDistribution && !isAboveSpark230) {

    stringBuffer.append(TEXT_212);
    
        }
        if(isLog4jEnabled && !isCloudDistribution) {

    stringBuffer.append(TEXT_213);
    
        }

    stringBuffer.append(TEXT_214);
    
        if(isLog4jEnabled) {

    stringBuffer.append(TEXT_215);
    
class BasicLogUtil{
    protected String cid  = "";
    protected org.talend.core.model.process.INode node = null;
    protected boolean log4jEnabled = false;
    private String logID = "";
    
    private BasicLogUtil(){}
    
    public BasicLogUtil(org.talend.core.model.process.INode node){
        this.node = node;
        String cidx = this.node.getUniqueName();
        if(cidx.matches("^.*?tAmazonAuroraOutput_\\d+_out$") || cidx.matches("^.*?tDBOutput_\\d+_out$")){
             cidx = cidx.substring(0,cidx.length()-4);// 4 ==> "_out".length();
        }
        this.cid = cidx;
        this.log4jEnabled = ("true").equals(org.talend.core.model.process.ElementParameterParser.getValue(this.node.getProcess(), "__LOG4J_ACTIVATE__"));
        this.log4jEnabled = this.log4jEnabled &&
                            this.node.getComponent().isLog4JEnabled() && !"JOBLET".equals(node.getComponent().getComponentType().toString());
        this.logID = this.cid;
    }
    
    public String var(String varName){
        return varName + "_" + this.cid;
    }
    public String str(String content){
        return "\"" + content + "\"";
    }
    
    public void info(String... message){
        log4j("info", message);
    }
    
    public void debug(String... message){
        log4j("debug", message);
    }
    
    public void warn(String... message){
        log4j("warn", message);
    }
    
    public void error(String... message){
        log4j("error", message);
    }
    
    public void fatal(String... message){
        log4j("fatal", message);
    }
    
    public void trace(String... message){
        log4j("trace", message);
    }
    java.util.List<String> checkableList = java.util.Arrays.asList(new String[]{"info", "debug", "trace"});     
    public void log4j(String level, String... messages){
        if(this.log4jEnabled){
            if(checkableList.contains(level)){
            
    stringBuffer.append(TEXT_216);
    stringBuffer.append(level.substring(0, 1).toUpperCase() + level.substring(1));
    stringBuffer.append(TEXT_217);
    
            }
            
    stringBuffer.append(TEXT_218);
    stringBuffer.append(level);
    stringBuffer.append(TEXT_219);
    stringBuffer.append(logID);
    stringBuffer.append(TEXT_220);
    for(String message : messages){
    stringBuffer.append(TEXT_221);
    stringBuffer.append(message);
    stringBuffer.append(TEXT_222);
    }
    stringBuffer.append(TEXT_223);
    
        }
    }
    
    public boolean isActive(){
        return this.log4jEnabled;
    }
}

class LogUtil extends BasicLogUtil{
    
    private LogUtil(){
    }
    
    public LogUtil(org.talend.core.model.process.INode node){
        super(node);
    }
    
    public void startWork(){
        debug(str("Start to work."));
    }
    
    public void endWork(){
        debug(str("Done."));
    }
    
    public void logIgnoredException(String exception){
        warn(exception);
    }
    
    public void logPrintedException(String exception){
        error(exception);
    }
    
    public void logException(String exception){
        fatal(exception);
    }
    
    public void logCompSetting(){
    
    
        if(log4jEnabled){
        
    stringBuffer.append(TEXT_224);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_225);
    stringBuffer.append(var("log4jParamters"));
    stringBuffer.append(TEXT_226);
    stringBuffer.append(TEXT_227);
    stringBuffer.append(var("log4jParamters"));
    stringBuffer.append(TEXT_228);
    
                    java.util.Set<org.talend.core.model.process.EParameterFieldType> ignoredParamsTypes = new java.util.HashSet<org.talend.core.model.process.EParameterFieldType>(); 
                    ignoredParamsTypes.addAll(
                        java.util.Arrays.asList(
                            org.talend.core.model.process.EParameterFieldType.SCHEMA_TYPE,
                            org.talend.core.model.process.EParameterFieldType.LABEL,
                            org.talend.core.model.process.EParameterFieldType.EXTERNAL,
                            org.talend.core.model.process.EParameterFieldType.MAPPING_TYPE,
                            org.talend.core.model.process.EParameterFieldType.IMAGE,
                            org.talend.core.model.process.EParameterFieldType.TNS_EDITOR,
                            org.talend.core.model.process.EParameterFieldType.WSDL2JAVA,
                            org.talend.core.model.process.EParameterFieldType.GENERATEGRAMMARCONTROLLER,
                            org.talend.core.model.process.EParameterFieldType.GENERATE_SURVIVORSHIP_RULES_CONTROLLER,
                            org.talend.core.model.process.EParameterFieldType.REFRESH_REPORTS,
                            org.talend.core.model.process.EParameterFieldType.BROWSE_REPORTS,
                            org.talend.core.model.process.EParameterFieldType.PALO_DIM_SELECTION,
                            org.talend.core.model.process.EParameterFieldType.GUESS_SCHEMA,
                            org.talend.core.model.process.EParameterFieldType.MATCH_RULE_IMEX_CONTROLLER,
                            org.talend.core.model.process.EParameterFieldType.MEMO_PERL,
                            org.talend.core.model.process.EParameterFieldType.DBTYPE_LIST,
                            org.talend.core.model.process.EParameterFieldType.VERSION,
                            org.talend.core.model.process.EParameterFieldType.TECHNICAL,
                            org.talend.core.model.process.EParameterFieldType.ICON_SELECTION,
                            org.talend.core.model.process.EParameterFieldType.JAVA_COMMAND,
                            org.talend.core.model.process.EParameterFieldType.TREE_TABLE,
                            org.talend.core.model.process.EParameterFieldType.VALIDATION_RULE_TYPE,
                            org.talend.core.model.process.EParameterFieldType.DCSCHEMA,
                            org.talend.core.model.process.EParameterFieldType.SURVIVOR_RELATION,
                            org.talend.core.model.process.EParameterFieldType.REST_RESPONSE_SCHEMA_TYPE
                            )
                    );
                    for(org.talend.core.model.process.IElementParameter ep : org.talend.core.model.utils.NodeUtil.getDisplayedParameters(node)){
                        if(!ep.isLog4JEnabled() || ignoredParamsTypes.contains(ep.getFieldType())){
                            continue;
                        }
                        String name = ep.getName();
                        if(org.talend.core.model.process.EParameterFieldType.PASSWORD.equals(ep.getFieldType())){
                            String epName = "__" + name + "__";
                            String password = "";
                            if(org.talend.core.model.process.ElementParameterParser.canEncrypt(node, epName)){
                                password = org.talend.core.model.process.ElementParameterParser.getEncryptedValue(node, epName);
                            }else{
                                String passwordValue = org.talend.core.model.process.ElementParameterParser.getValue(node, epName);
                                if (passwordValue == null || "".equals(passwordValue.trim())) {// for the value which empty
                                    passwordValue = "\"\"";
                                } 
                                password = "routines.system.PasswordEncryptUtil.encryptPassword(" + passwordValue + ")";
                            } 
                            
    stringBuffer.append(TEXT_229);
    stringBuffer.append(var("log4jParamters"));
    stringBuffer.append(TEXT_230);
    stringBuffer.append(name);
    stringBuffer.append(TEXT_231);
    stringBuffer.append(password);
    stringBuffer.append(TEXT_232);
    
                        }else{
                            String value = org.talend.core.model.utils.NodeUtil.getNormalizeParameterValue(node, ep);
                            if(value.length()>10000){
                            value = org.talend.core.model.utils.NodeUtil.replaceCRLFInMEMO_SQL(value);
                            }
                            
    stringBuffer.append(TEXT_233);
    stringBuffer.append(var("log4jParamters"));
    stringBuffer.append(TEXT_234);
    stringBuffer.append(name);
    stringBuffer.append(TEXT_235);
    stringBuffer.append(value);
    stringBuffer.append(TEXT_236);
    
                        }   
                        
    stringBuffer.append(TEXT_237);
    stringBuffer.append(var("log4jParamters"));
    stringBuffer.append(TEXT_238);
    
                    }
                    debug(var("log4jParamters")); 
                    
    stringBuffer.append(TEXT_239);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_240);
    
        } 
        
    
    } 
    
    //no use for now, because we log the data by rowStruct
    public void traceData(String rowStruct, java.util.List<org.talend.core.model.metadata.IMetadataColumn> columnList, String nbline){
        if(log4jEnabled){
        
    stringBuffer.append(TEXT_241);
    stringBuffer.append(var("log4jSb"));
    stringBuffer.append(TEXT_242);
    
            for(org.talend.core.model.metadata.IMetadataColumn column : columnList){
                org.talend.core.model.metadata.types.JavaType javaType = org.talend.core.model.metadata.types.JavaTypesManager.getJavaTypeFromId(column.getTalendType());
                String columnName = column.getLabel();
                boolean isPrimit = org.talend.core.model.metadata.types.JavaTypesManager.isJavaPrimitiveType(column.getTalendType(), column.isNullable());
                if(isPrimit){
                
    stringBuffer.append(TEXT_243);
    stringBuffer.append(var("log4jSb"));
    stringBuffer.append(TEXT_244);
    stringBuffer.append(rowStruct);
    stringBuffer.append(TEXT_245);
    stringBuffer.append(columnName);
    stringBuffer.append(TEXT_246);
    
                }else{
                
    stringBuffer.append(TEXT_247);
    stringBuffer.append(rowStruct);
    stringBuffer.append(TEXT_248);
    stringBuffer.append(columnName);
    stringBuffer.append(TEXT_249);
    stringBuffer.append(TEXT_250);
    stringBuffer.append(var("log4jSb"));
    stringBuffer.append(TEXT_251);
    stringBuffer.append(TEXT_252);
    stringBuffer.append(var("log4jSb"));
    stringBuffer.append(TEXT_253);
    stringBuffer.append(rowStruct);
    stringBuffer.append(TEXT_254);
    stringBuffer.append(columnName);
    stringBuffer.append(TEXT_255);
    
                }
                
    stringBuffer.append(TEXT_256);
    stringBuffer.append(var("log4jSb"));
    stringBuffer.append(TEXT_257);
    
            }
        }
        trace(str("Content of the record "), nbline, str(": "), var("log4jSb"));
        
    
    }
}

class LogHelper{
    
    java.util.Map<String, String> pastDict = null;
    
    public LogHelper(){
        pastDict = new java.util.HashMap<String, String>();
        pastDict.put("insert", "inserted");
        pastDict.put("update", "updated");
        pastDict.put("delete", "deleted");
        pastDict.put("upsert", "upserted");
    }   
    
    public String upperFirstChar(String data){ 
        return data.substring(0, 1).toUpperCase() + data.substring(1);
    }
    
    public String toPastTense(String data){
        return pastDict.get(data);
    }
}
LogHelper logHelper = new LogHelper();

LogUtil log = null;

    
            for (INode confNode : confNodes) {
                String componentName = confNode.getComponent().getName();
                if(componentName.endsWith("Configuration")) {

    stringBuffer.append(TEXT_258);
    stringBuffer.append(confNode.getUniqueName());
    stringBuffer.append(TEXT_259);
    
                    log = new LogUtil(confNode);
                    log.logCompSetting();
                }
            }
        }

        for (INode rootNode : rootNodes) {
            String componentName = rootNode.getComponent().getName();
            String uniqueName = rootNode.getUniqueName();
            if(rootNode.getOutgoingConnections().size() > 0 || "tJava".equals(componentName) || "tRunJob".equals(componentName) || "tMatchModel".equals(componentName) ||
              (componentName != null && componentName.startsWith("tHConvert")) || (componentName != null && componentName.startsWith("tHMap"))) {
            
    stringBuffer.append(TEXT_260);
    stringBuffer.append(TEXT_261);
    stringBuffer.append(rootNode.getUniqueName());
    stringBuffer.append(TEXT_262);
    stringBuffer.append(isSpark2x ? "ss" : "ctx");
    stringBuffer.append(TEXT_263);
    
					if(stats && !isCloudDistribution && isAboveSpark230) {

    stringBuffer.append(TEXT_264);
    stringBuffer.append(sparkStatsPollingRate );
    stringBuffer.append(TEXT_265);
    
                	}

    stringBuffer.append(TEXT_266);
    
                    if(stats && !isCloudDistribution) {

    stringBuffer.append(TEXT_267);
    
                    }
                    if (!isDatabricksDistribution) {

    stringBuffer.append(TEXT_268);
    
                    }

    stringBuffer.append(TEXT_269);
    
                // Validate result of test cases
                if (isTestContainer) {
                    
    stringBuffer.append(TEXT_270);
    
                }
            }
        }
        if (talendLineageAPI.doRequireLineageSupport(sparkConfig)) {
            List<? extends INode> graphicalNodes = process.getGraphicalNodes();
            for (INode gNode: graphicalNodes) {
                if (gNode.isActivate()) {
                    talendLineageAPI.generateColumnListFromOutputLink(gNode, gNode.getUniqueName());
                    talendLineageAPI.generateInputNodeList(gNode);
                    talendLineageAPI.generateOutputNodeList(gNode);
                    talendLineageAPI.addNodeToLineage(gNode.getUniqueName());
                }
            }
            Boolean dieOnError = talendLineageAPI.getDieOnError(sparkConfig);
            
    stringBuffer.append(TEXT_271);
    stringBuffer.append(dieOnError);
    stringBuffer.append(TEXT_272);
    
        }
        
    stringBuffer.append(TEXT_273);
    
        String master = "\"local[*]\"";
        String deployMode = null;

        if(useStandaloneMode) {
            master = ElementParameterParser.getValue(sparkConfig, "__SPARK_HOST__");
        }

        if(useYarnClientMode) {
            master = org.talend.hadoop.distribution.ESparkVersion.SPARK_2_0.compareTo(sparkVersion) > 0 ? "\"yarn-client\"" : "\"yarn\"";
            deployMode = org.talend.hadoop.distribution.ESparkVersion.SPARK_2_0.compareTo(sparkVersion) > 0 ? null : "\"client\"";
        }

        if(useYarnClusterMode) {
            master = org.talend.hadoop.distribution.ESparkVersion.SPARK_2_0.compareTo(sparkVersion) > 0 ? "\"yarn-cluster\"" : "\"yarn\"";
            deployMode = org.talend.hadoop.distribution.ESparkVersion.SPARK_2_0.compareTo(sparkVersion) > 0 ? null : "\"cluster\"";
            
    stringBuffer.append(TEXT_274);
    
            if("true".equals(ElementParameterParser.getValue(sparkConfig, "__WAIT_FOR_THE_JOB_TO_COMPLETE__"))) {
                
    stringBuffer.append(TEXT_275);
    
            } else {
                
    stringBuffer.append(TEXT_276);
    
            }
            
    stringBuffer.append(TEXT_277);
    
        }

        if(!useCloudLauncher) {

    stringBuffer.append(TEXT_278);
    stringBuffer.append(master);
    stringBuffer.append(TEXT_279);
     if(isTestContainer){ 
    stringBuffer.append(TEXT_280);
    stringBuffer.append(jobFolderName);
    stringBuffer.append(TEXT_281);
     } 
    stringBuffer.append(TEXT_282);
     if(isTestContainer){ 
    stringBuffer.append(TEXT_283);
     } 
    stringBuffer.append(TEXT_284);
    
            if (deployMode != null) {
                
    stringBuffer.append(TEXT_285);
    stringBuffer.append(deployMode);
    stringBuffer.append(TEXT_286);
     if(isLog4jEnabled) { 
    stringBuffer.append(TEXT_287);
     } 
    stringBuffer.append(TEXT_288);
    
            }
            
    stringBuffer.append(TEXT_289);
    
            // We need to look into the job command line to get the real path of the winutils.exe binary.
            String winUtilsBinaryName = useLocalMode ? "winutils-hadoop-2.6.0.exe" : sparkBatchDistrib.getWinUtilsName();
            String[] commandLine = new String[] {"<command>"};
            try {
                commandLine = ProcessorUtilities.getCommandLine("win32",true, processId, "",org.talend.designer.runprocess.IProcessor.NO_STATISTICS,org.talend.designer.runprocess.IProcessor.NO_TRACES, new String[]{});
            } catch (ProcessorException e) {
                e.printStackTrace();
            }
            java.util.List<String> jars = null;
            for (int j = 0; j < commandLine.length; j++) {
                if(commandLine[j].contains("jar")) {
                    jars = java.util.Arrays.asList(commandLine[j].split(";"));
                    break;
                }
            }
            if(jars != null) {
            	String winutilPath = null;
            	if (ProcessorUtilities.isExportConfig()) {
	            	for(int j = 0; j < jars.size(); j++) {
            			String jar = jars.get(j);
	                    if(jar.endsWith(".jar")) {
	                    	winutilPath = jar.substring(0, jar.lastIndexOf("/") + 1) + winUtilsBinaryName;
	                    	break;
	                    }
	                }
            	} else {
            		winutilPath = ProcessorUtilities.getJavaProjectLibFolderPath() + ("/" + winUtilsBinaryName);
            	}

    stringBuffer.append(TEXT_290);
    stringBuffer.append(ElementParameterParser.getValue(sparkConfig, "__SPARK_SCRATCH_DIR__"));
    stringBuffer.append(TEXT_291);
    stringBuffer.append(winutilPath);
    stringBuffer.append(TEXT_292);
    
            }

            // prepare Spark 2 jars to upload them to the cluster through spark.yarn.jars
            if (org.talend.hadoop.distribution.ESparkVersion.SPARK_2_0.compareTo(sparkVersion) <= 0
                    && sparkBatchDistrib != null && sparkBatchDistrib.getVersion() != null
                    && sparkBatchDistrib.generateSparkJarsPaths(jars) != null && !sparkBatchDistrib.generateSparkJarsPaths(jars).isEmpty()){
                
    stringBuffer.append(TEXT_293);
    stringBuffer.append(sparkBatchDistrib.generateSparkJarsPaths(jars));
    stringBuffer.append(TEXT_294);
     if(isLog4jEnabled) { 
    stringBuffer.append(TEXT_295);
     } 
    stringBuffer.append(TEXT_296);
     if(isLog4jEnabled) { 
    stringBuffer.append(TEXT_297);
     } 
    stringBuffer.append(TEXT_298);
    
            }

    stringBuffer.append(TEXT_299);
    
        }

        if(!useLocalMode && !useCloudLauncher) { // If the spark mode is not local and not executed through livy.
            boolean defineDriverHost = "true".equals(ElementParameterParser.getValue(sparkConfig, "__DEFINE_SPARK_DRIVER_HOST__"));
            if(defineDriverHost && !useYarnClusterMode) {
                String driverHost = ElementParameterParser.getValue(sparkConfig, "__SPARK_DRIVER_HOST__");
                if(driverHost != null && !"".equals(driverHost)) {

    stringBuffer.append(TEXT_300);
    stringBuffer.append(driverHost);
    stringBuffer.append(TEXT_301);
    stringBuffer.append(driverHost);
    stringBuffer.append(TEXT_302);
    
                }
            }
            if(useStandaloneMode) {
                String sparkHome = ElementParameterParser.getValue(sparkConfig, "__SPARK_HOME__");

    stringBuffer.append(TEXT_303);
    stringBuffer.append(sparkHome);
    stringBuffer.append(TEXT_304);
    
            } else if(useYarnMode) {
                // Set the YARN parameters in the SparkConf
                String resourceManager = ElementParameterParser.getValue(sparkConfig, "__RESOURCE_MANAGER__");
                boolean setResourceManagerSchedulerAddress = "true".equals(ElementParameterParser.getValue(sparkConfig, "__SET_SCHEDULER_ADDRESS__"));
                boolean setJobHistoryAddress = "true".equals(ElementParameterParser.getValue(sparkConfig, "__SET_JOBHISTORY_ADDRESS__"));
                boolean setStagingDirectory = "true".equals(ElementParameterParser.getValue(sparkConfig, "__SET_STAGING_DIRECTORY__"));
                boolean setMemory = "true".equals(ElementParameterParser.getValue(sparkConfig, "__SET_MEMORY__"));

    stringBuffer.append(TEXT_305);
    stringBuffer.append(sparkBatchDistrib.getYarnApplicationClasspath());
    stringBuffer.append(TEXT_306);
    stringBuffer.append(resourceManager);
    stringBuffer.append(TEXT_307);
    if(setResourceManagerSchedulerAddress) { 
    stringBuffer.append(TEXT_308);
    stringBuffer.append(ElementParameterParser.getValue(sparkConfig, "__RESOURCEMANAGER_SCHEDULER_ADDRESS__"));
    stringBuffer.append(TEXT_309);
     } 
    if(setJobHistoryAddress) { 
    stringBuffer.append(TEXT_310);
    stringBuffer.append(ElementParameterParser.getValue(sparkConfig, "__JOBHISTORY_ADDRESS__"));
    stringBuffer.append(TEXT_311);
     } 
    if(setStagingDirectory) { 
    stringBuffer.append(TEXT_312);
    stringBuffer.append(ElementParameterParser.getValue(sparkConfig, "__STAGING_DIRECTORY__"));
    stringBuffer.append(TEXT_313);
     } 
    if(setMemory) { 
    stringBuffer.append(TEXT_314);
    stringBuffer.append(ElementParameterParser.getValue(sparkConfig, "__MAPREDUCE_MAP_MEMORY_MB__"));
    stringBuffer.append(TEXT_315);
    stringBuffer.append(ElementParameterParser.getValue(sparkConfig, "__MAPREDUCE_REDUCE_MEMORY_MB__"));
    stringBuffer.append(TEXT_316);
    stringBuffer.append(ElementParameterParser.getValue(sparkConfig, "__YARN_APP_MAPREDUCE_AM_RESOURCE_MB__"));
    stringBuffer.append(TEXT_317);
     } 
    
                if(sparkUseKrb && (isCustom || sparkBatchDistrib.doSupportKerberos())) {

    stringBuffer.append(TEXT_318);
    stringBuffer.append(ElementParameterParser.getValue(sparkConfig, "__RESOURCEMANAGER_PRINCIPAL__"));
    stringBuffer.append(TEXT_319);
    stringBuffer.append(ElementParameterParser.getValue(sparkConfig, "__JOBHISTORY_PRINCIPAL__"));
    stringBuffer.append(TEXT_320);
    
                    if(((isCustom || sparkBatchDistrib.doSupportMapRTicket()) && useMapRTicket)) {
                        
    stringBuffer.append(TEXT_321);
    stringBuffer.append(setMapRHomeDir ? mapRHomeDir : "\"/opt/mapr\"" );
    stringBuffer.append(TEXT_322);
    stringBuffer.append(setMapRHadoopLogin ? mapRHadoopLogin : "\"kerberos\"");
    stringBuffer.append(TEXT_323);
    
                    }
                    boolean sparkUseKeytab = "true".equals(ElementParameterParser.getValue(sparkConfig, "__USE_KEYTAB__"));
                    if(sparkUseKeytab) {
                        if(useYarnClusterMode) {
                        
    stringBuffer.append(TEXT_324);
    }
    stringBuffer.append(TEXT_325);
    stringBuffer.append(ElementParameterParser.getValue(sparkConfig, "__PRINCIPAL__"));
    stringBuffer.append(TEXT_326);
    stringBuffer.append(ElementParameterParser.getValue(sparkConfig, "__KEYTAB_PATH__"));
    stringBuffer.append(TEXT_327);
    
                        if(useYarnClusterMode) {
                        
    stringBuffer.append(TEXT_328);
    stringBuffer.append(ElementParameterParser.getValue(sparkConfig, "__KEYTAB_PATH__"));
    stringBuffer.append(TEXT_329);
    stringBuffer.append(ElementParameterParser.getValue(sparkConfig, "__PRINCIPAL__"));
    stringBuffer.append(TEXT_330);
    
                        }
                    }
                    if(((isCustom || sparkBatchDistrib.doSupportMapRTicket()) && useMapRTicket)) {
                        
    stringBuffer.append(TEXT_331);
    stringBuffer.append(mapRTicketCluster);
    stringBuffer.append(TEXT_332);
    stringBuffer.append(mapRTicketDuration);
    stringBuffer.append(TEXT_333);
    
                    }
                } else if(!useCloudLauncher) {
                    // Mapr ticket
                    if(((isCustom || sparkBatchDistrib.doSupportMapRTicket()) && useMapRTicket)) {
                        passwordFieldName = "__MAPRTICKET_PASSWORD__";
                        
    stringBuffer.append(TEXT_334);
    stringBuffer.append(setMapRHomeDir ? mapRHomeDir : "\"/opt/mapr\"" );
    stringBuffer.append(TEXT_335);
    
                        if (setMapRHadoopLogin) {
                            
    stringBuffer.append(TEXT_336);
    stringBuffer.append(mapRHadoopLogin);
    stringBuffer.append(TEXT_337);
    
                        } else {
                            
    stringBuffer.append(TEXT_338);
    
                        }
                        INode node = sparkConfig;
                        
    if (ElementParameterParser.canEncrypt(node, passwordFieldName)) {
    stringBuffer.append(TEXT_339);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_340);
    stringBuffer.append(ElementParameterParser.getEncryptedValue(node, passwordFieldName));
    stringBuffer.append(TEXT_341);
    } else {
    stringBuffer.append(TEXT_342);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_343);
    stringBuffer.append( ElementParameterParser.getValue(node, passwordFieldName));
    stringBuffer.append(TEXT_344);
    }
    
                        if(sparkBatchDistrib.doSupportMaprTicketV52API()){
                            
    stringBuffer.append(TEXT_345);
    stringBuffer.append(mapRTicketCluster);
    stringBuffer.append(TEXT_346);
    stringBuffer.append(mapRTicketUsername);
    stringBuffer.append(TEXT_347);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_348);
    stringBuffer.append(mapRTicketDuration);
    stringBuffer.append(TEXT_349);
    
                        } else {
                            
    stringBuffer.append(TEXT_350);
    stringBuffer.append(mapRTicketCluster);
    stringBuffer.append(TEXT_351);
    stringBuffer.append(mapRTicketUsername);
    stringBuffer.append(TEXT_352);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_353);
    stringBuffer.append(mapRTicketDuration);
    stringBuffer.append(TEXT_354);
    
                        }
                    }
                    String sparkUsername = ElementParameterParser.getValue(sparkConfig, "__USERNAME__");
                    if(sparkUsername != null) {
                        if(username != null) {

    stringBuffer.append(TEXT_355);
    stringBuffer.append(sparkUsername);
    stringBuffer.append(TEXT_356);
    stringBuffer.append(username);
    stringBuffer.append(TEXT_357);
    
                        }
                        if(!"".equals(sparkUsername)) {

    stringBuffer.append(TEXT_358);
    stringBuffer.append(sparkUsername);
    stringBuffer.append(TEXT_359);
    stringBuffer.append(sparkUsername);
    stringBuffer.append(TEXT_360);
    
                        }
                    }
                }
            }
        } // End of: If the spark mode is not local.

        boolean defineHadoopHomeDir = "true".equals(ElementParameterParser.getValue(sparkConfig, "__DEFINE_HADOOP_HOME_DIR__"));
        if(defineHadoopHomeDir && !useCloudLauncher) {
        
    stringBuffer.append(TEXT_361);
    stringBuffer.append(ElementParameterParser.getValue(sparkConfig, "__HADOOP_HOME_DIR__"));
    stringBuffer.append(TEXT_362);
    
        }
        
        if(tuningProperties) {
            // Set Web UI port
            if(setWebuiPort) {

    stringBuffer.append(TEXT_363);
    stringBuffer.append(ElementParameterParser.getValue(sparkConfig, "__WEB_UI_PORT__"));
    stringBuffer.append(TEXT_364);
    
            }

    stringBuffer.append(TEXT_365);
    stringBuffer.append(ElementParameterParser.getValue(sparkConfig, "__SPARK_EXECUTOR_MEM__"));
    stringBuffer.append(TEXT_366);
    
            // executor memory overhead is a YARN only parameter
            boolean setExecutorMemoryOverhead = "true".equals(ElementParameterParser.getValue(sparkConfig, "__SET_SPARK_EXECUTOR_MEM_OVERHEAD__"));
            if(setExecutorMemoryOverhead && (useYarnClientMode || useYarnClusterMode)) {

    stringBuffer.append(TEXT_367);
    stringBuffer.append(ElementParameterParser.getValue(sparkConfig, "__SPARK_EXECUTOR_MEM_OVERHEAD__"));
    stringBuffer.append(TEXT_368);
    
            }

            if(useStandaloneMode || useYarnClusterMode) {

    stringBuffer.append(TEXT_369);
    stringBuffer.append(ElementParameterParser.getValue(sparkConfig, "__SPARK_DRIVER_CORES__"));
    stringBuffer.append(TEXT_370);
    stringBuffer.append(ElementParameterParser.getValue(sparkConfig, "__SPARK_DRIVER_MEM__"));
    stringBuffer.append(TEXT_371);
    
            } else if(useYarnClientMode) {
                // application master tuning properties
                boolean setApplicationMasterTuningProperties = "true".equals(ElementParameterParser.getValue(sparkConfig, "__SPARK_YARN_AM_SETTINGS_CHECK__"));
                if(setApplicationMasterTuningProperties) {

    stringBuffer.append(TEXT_372);
    stringBuffer.append(ElementParameterParser.getValue(sparkConfig, "__SPARK_YARN_AM_CORES__"));
    stringBuffer.append(TEXT_373);
    stringBuffer.append(ElementParameterParser.getValue(sparkConfig, "__SPARK_YARN_AM_MEM__"));
    stringBuffer.append(TEXT_374);
    
                }
            }
            if(setExecutorCores && !useLocalMode) {

    stringBuffer.append(TEXT_375);
    stringBuffer.append(ElementParameterParser.getValue(sparkConfig, "__SPARK_EXECUTOR_CORES__"));
    stringBuffer.append(TEXT_376);
    
            }

            if(useYarnMode) {
                String allocationMode = ElementParameterParser.getValue(sparkConfig, "__SPARK_YARN_ALLOC_TYPE__");
                // we do nothing in AUTO mode
                if("FIXED".equalsIgnoreCase(allocationMode)) {

    stringBuffer.append(TEXT_377);
    stringBuffer.append(ElementParameterParser.getValue(sparkConfig, "__SPARK_EXECUTOR_INSTANCES__"));
    stringBuffer.append(TEXT_378);
    
                } else if("DYNAMIC".equalsIgnoreCase(allocationMode) && (isCustom  || sparkBatchDistrib != null && sparkBatchDistrib.doSupportDynamicMemoryAllocation())) {

    stringBuffer.append(TEXT_379);
    stringBuffer.append(ElementParameterParser.getValue(sparkConfig, "__SPARK_YARN_DYN_INIT__"));
    stringBuffer.append(TEXT_380);
    stringBuffer.append(ElementParameterParser.getValue(sparkConfig, "__SPARK_YARN_DYN_MIN__"));
    stringBuffer.append(TEXT_381);
    
                    String dynMaxValue = ElementParameterParser.getValue(sparkConfig, "__SPARK_YARN_DYN_MAX__");
                    if(dynMaxValue.contains("MAX")) {

    stringBuffer.append(TEXT_382);
    
                    }
                    else {

    stringBuffer.append(TEXT_383);
    stringBuffer.append(dynMaxValue);
    stringBuffer.append(TEXT_384);
    stringBuffer.append(dynMaxValue);
    stringBuffer.append(TEXT_385);
    
                    }

    stringBuffer.append(TEXT_386);
    
                }
            }

            // The broadcast factory is unused in Spark 2.
            if (org.talend.hadoop.distribution.ESparkVersion.SPARK_2_0.compareTo(sparkVersion) > 0) {
                String broadcastFactory = ElementParameterParser.getValue(sparkConfig, "__SPARK_BROADCAST_FACTORY__");
                // we do nothing in auto mode
                if("TORRENT".equalsIgnoreCase(broadcastFactory)) {

    stringBuffer.append(TEXT_387);
    
                } else if ("HTTP".equalsIgnoreCase(broadcastFactory)) {

    stringBuffer.append(TEXT_388);
    
                }
            }

            boolean customizeSparkSerializer = "true".equals(ElementParameterParser.getValue(sparkConfig, "__CUSTOMIZE_SPARK_SERIALIZER__"));
            if(customizeSparkSerializer) {

    stringBuffer.append(TEXT_389);
    stringBuffer.append(ElementParameterParser.getValue(sparkConfig, "__SPARK_SERIALIZER__"));
    stringBuffer.append(TEXT_390);
    
            }
        } // end set of tuning properties

        // SPARK LOGGING / HISTORY parameters
        // These parameters are valied for YARN and mesos
        boolean enableSparkEventLogging = "true".equals(ElementParameterParser.getValue(sparkConfig, "__ENABLE_SPARK_EVENT_LOGGING__"));
        if(enableSparkEventLogging && (useYarnClientMode || useYarnClusterMode)){

    stringBuffer.append(TEXT_391);
    
            boolean compressSparkEventLogs = "true".equals(ElementParameterParser.getValue(sparkConfig, "__COMPRESS_SPARK_EVENT_LOGS__"));
            if(compressSparkEventLogs){

    stringBuffer.append(TEXT_392);
    
            }

    stringBuffer.append(TEXT_393);
    stringBuffer.append(ElementParameterParser.getValue(sparkConfig, "__SPARK_EVENT_LOG_DIR__"));
    stringBuffer.append(TEXT_394);
    stringBuffer.append(ElementParameterParser.getValue(sparkConfig, "__SPARKHISTORY_ADDRESS__"));
    stringBuffer.append(TEXT_395);
    
        }

        if(useLocalMode) {
            // Scratch dir must be absolute. System.getProperty("user.dir")
            
    stringBuffer.append(TEXT_396);
    stringBuffer.append(ElementParameterParser.getValue(sparkConfig, "__SPARK_SCRATCH_DIR__"));
    stringBuffer.append(TEXT_397);
    
        }

    stringBuffer.append(TEXT_398);
     
List<INode> s3Nodes = new ArrayList<INode>(process.getNodesOfType("tS3Configuration"));

if(s3Nodes.size() > 1) {

    stringBuffer.append(TEXT_399);
    
}
if(s3Nodes.size() == 1) {
    INode pNode = s3Nodes.get(0);
    // the configuration components must not be considered as a root node.
    rootNodes.remove(pNode);

    Map<String, String> s3Properties = null;
    String s3FileSystem = "\"org.apache.hadoop.fs.s3native.NativeS3FileSystem\"";

    Boolean useS3a = ElementParameterParser.getBooleanValue(pNode, "__USE_S3A__");
    if (useS3a) {
        s3FileSystem = "\"org.apache.hadoop.fs.s3a.S3AFileSystem\"";
        s3Properties = new HashMap<String, String>();
        s3Properties.put("PROPERTY", "\"spark.hadoop.fs.s3n.impl\"");
        s3Properties.put("VALUE", s3FileSystem);
        sparkAdvancedProperties.add(s3Properties);
        if (!isAltusDistribution) {
            s3Properties = new HashMap<String, String>();
            s3Properties.put("PROPERTY", "\"spark.hadoop.fs.s3a.user.agent.prefix\"");
            s3Properties.put("VALUE", "routines.system.Constant.getUserAgent(" + "\"" + studioVersion + "\"" + ")");
            sparkAdvancedProperties.add(s3Properties);
        }
    }

    s3Properties = new HashMap<String, String>();
    s3Properties.put("PROPERTY", "\"spark.hadoop.fs.s3.impl\"");
    s3Properties.put("VALUE", s3FileSystem);
    sparkAdvancedProperties.add(s3Properties);
    s3Properties = new HashMap<String, String>();
    s3Properties.put("PROPERTY", "\"spark.hadoop.fs.s3n.impl\"");
    s3Properties.put("VALUE", s3FileSystem);
    sparkAdvancedProperties.add(s3Properties);
    s3Properties = new HashMap<String, String>();
    s3Properties.put("PROPERTY", "\"spark.hadoop.fs.s3.awsAccessKeyId\"");
    s3Properties.put("VALUE", ElementParameterParser.getValue(pNode, "__ACCESS_KEY__"));
    sparkAdvancedProperties.add(s3Properties);
    s3Properties = new HashMap<String, String>();
    s3Properties.put("PROPERTY", "\"spark.hadoop.fs.s3n.awsAccessKeyId\"");
    s3Properties.put("VALUE", ElementParameterParser.getValue(pNode, "__ACCESS_KEY__"));
    sparkAdvancedProperties.add(s3Properties);
    s3Properties = new HashMap<String, String>();
    s3Properties.put("PROPERTY", "\"spark.hadoop.fs.s3.awsSecretAccessKey\"");
    passwordFieldName = "__SECRET_KEY__";
    String password = "";
    if (ElementParameterParser.canEncrypt(pNode, passwordFieldName)) {
        password = ElementParameterParser.getEncryptedValue(pNode, passwordFieldName);
        password = "routines.system.PasswordEncryptUtil.decryptPassword(" + password + ")";
    } else {
        password = ElementParameterParser.getValue(pNode, passwordFieldName);
    }
    s3Properties.put("VALUE", password);
    sparkAdvancedProperties.add(s3Properties);
    s3Properties = new HashMap<String, String>();
    s3Properties.put("PROPERTY", "\"spark.hadoop.fs.s3n.awsSecretAccessKey\"");
    s3Properties.put("VALUE", password);
    sparkAdvancedProperties.add(s3Properties);

    if (useS3a) {
		if (useS3aSpecificProperties) {
			s3Properties = new HashMap<String, String>();
            s3Properties.put("PROPERTY", "\"spark.hadoop.fs.s3a.access.key\"");
            s3Properties.put("VALUE", ElementParameterParser.getValue(pNode, "__ACCESS_KEY__"));
            sparkAdvancedProperties.add(s3Properties);
            s3Properties = new HashMap<String, String>();
            s3Properties.put("PROPERTY", "\"spark.hadoop.fs.s3a.secret.key\"");
            s3Properties.put("VALUE", password);
            sparkAdvancedProperties.add(s3Properties);
		} else {
            s3Properties = new HashMap<String, String>();
            s3Properties.put("PROPERTY", "\"spark.hadoop.fs.s3a.awsAccessKeyId\"");
            s3Properties.put("VALUE", ElementParameterParser.getValue(pNode, "__ACCESS_KEY__"));
            sparkAdvancedProperties.add(s3Properties);
            s3Properties = new HashMap<String, String>();
            s3Properties.put("PROPERTY", "\"spark.hadoop.fs.s3a.awsSecretAccessKey\"");
            s3Properties.put("VALUE", password);
            sparkAdvancedProperties.add(s3Properties);
		}
    }

    // Set endpoint
    Boolean setEndpoint = ElementParameterParser.getBooleanValue(pNode, "__SET_ENDPOINT__");
    Boolean setRegion = ElementParameterParser.getBooleanValue(pNode, "__SET_REGION__");
    if (setEndpoint || setRegion) {
        org.talend.hadoop.distribution.component.HadoopComponent s3Distrib = null;

        if(!useLocalMode) {
            try {
                s3Distrib = (org.talend.hadoop.distribution.component.HadoopComponent) org.talend.hadoop.distribution.DistributionFactory.buildDistribution(
                        sparkDistribution, sparkDistribVersion);
            } catch (java.lang.Exception e) {
                e.printStackTrace();
                return "";
            }
        }
        if (((s3Distrib != null) && (s3Distrib.doSupportS3V4())) || (useLocalMode)) {
            String endpoint = ElementParameterParser.getValue(pNode, "__ENDPOINT__");
            if (setRegion) {
                endpoint = ElementParameterParser.getValue(pNode, "__REGION__");
            }
            s3Properties = new HashMap<String, String>();
            s3Properties.put("PROPERTY", "\"spark.hadoop.fs.s3.endpoint\"");
            s3Properties.put("VALUE", endpoint);
            sparkAdvancedProperties.add(s3Properties);

            s3Properties = new HashMap<String, String>();
            s3Properties.put("PROPERTY", "\"spark.hadoop.fs.s3n.endpoint\"");
            s3Properties.put("VALUE", endpoint);
            sparkAdvancedProperties.add(s3Properties);

            s3Properties = new HashMap<String, String>();
            s3Properties.put("PROPERTY", "\"hadoop.fs.s3.endpoint\"");
            s3Properties.put("VALUE", endpoint);
            sparkAdvancedProperties.add(s3Properties);

            s3Properties = new HashMap<String, String>();
            s3Properties.put("PROPERTY", "\"hadoop.fs.s3n.endpoint\"");
            s3Properties.put("VALUE", endpoint);
            sparkAdvancedProperties.add(s3Properties);

            if (useS3a) {
                s3Properties = new HashMap<String, String>();
                s3Properties.put("PROPERTY", "\"spark.hadoop.fs.s3a.endpoint\"");
                s3Properties.put("VALUE", endpoint);
                sparkAdvancedProperties.add(s3Properties);

                s3Properties = new HashMap<String, String>();
                s3Properties.put("PROPERTY", "\"hadoop.fs.s3a.endpoint\"");
                s3Properties.put("VALUE", endpoint);
                sparkAdvancedProperties.add(s3Properties);
            }
        }
    }

    String s3AccessKey = ElementParameterParser.getValue(pNode, "__ACCESS_KEY__");
    String s3SecretKey = ElementParameterParser.getPasswordValue(pNode, "__SECRET_KEY__");
    Boolean assumeRole = ElementParameterParser.getBooleanValue(pNode, "__ASSUME_ROLE__");
    String externalId = ElementParameterParser.getValue(pNode, "__EXTERNAL_ID__");
    String roleArn = ElementParameterParser.getValue(pNode, "__ARN__");
    String roleSessionName = ElementParameterParser.getValue(pNode, "__ROLE_SESSION_NAME__");
    String sessionDuration = ElementParameterParser.getValue(pNode, "__SESSION_DURATION__");

    Boolean setStsRegion = ElementParameterParser.getBooleanValue(pNode, "__SET_STS_REGION__");
    String stsRegion = ElementParameterParser.getValue(pNode, "__STS_REGION__");

    Boolean setStsEndpoint = ElementParameterParser.getBooleanValue(pNode, "__SET_STS_ENDPOINT__");
    String stsEndpoint = ElementParameterParser.getValue(pNode, "__STS_ENDPOINT__");
	     
    String resultingStsEndpoint = null;
	
    if (setStsRegion) {
        resultingStsEndpoint = stsRegion;
    }
    else if (setStsEndpoint) {
        resultingStsEndpoint = stsEndpoint;
    }

    if (useS3a && assumeRole) {
    
    if(isLog4jEnabled){
    stringBuffer.append(TEXT_400);
    stringBuffer.append(s3AccessKey);
    stringBuffer.append(TEXT_401);
    stringBuffer.append(assumeRole);
    stringBuffer.append(TEXT_402);
    stringBuffer.append(roleSessionName);
    stringBuffer.append(TEXT_403);
    stringBuffer.append(sessionDuration);
    stringBuffer.append(TEXT_404);
    stringBuffer.append(externalId);
    stringBuffer.append(TEXT_405);
    stringBuffer.append(roleArn);
    stringBuffer.append(TEXT_406);
    stringBuffer.append(setStsRegion);
    stringBuffer.append(TEXT_407);
    stringBuffer.append(stsRegion);
    stringBuffer.append(TEXT_408);
    stringBuffer.append(setStsEndpoint);
    stringBuffer.append(TEXT_409);
    stringBuffer.append(TEXT_410);
    stringBuffer.append(setStsEndpoint);
    stringBuffer.append(TEXT_411);
    }
    stringBuffer.append(TEXT_412);
    stringBuffer.append(externalId);
    stringBuffer.append(TEXT_413);
    stringBuffer.append(externalId);
    stringBuffer.append(TEXT_414);
    stringBuffer.append(externalId);
    stringBuffer.append(TEXT_415);
    stringBuffer.append(resultingStsEndpoint);
    stringBuffer.append(TEXT_416);
    stringBuffer.append(resultingStsEndpoint);
    stringBuffer.append(TEXT_417);
    stringBuffer.append(resultingStsEndpoint);
    stringBuffer.append(TEXT_418);
    stringBuffer.append(s3AccessKey);
    stringBuffer.append(TEXT_419);
    stringBuffer.append(s3SecretKey);
    stringBuffer.append(TEXT_420);
    stringBuffer.append(roleArn);
    stringBuffer.append(TEXT_421);
    stringBuffer.append(roleSessionName);
    stringBuffer.append(TEXT_422);
    stringBuffer.append(sessionDuration);
    stringBuffer.append(TEXT_423);
    
    }
}

    stringBuffer.append(TEXT_424);
    
        // Advanced properties
        for(Map<String, String> property : sparkAdvancedProperties){

    stringBuffer.append(TEXT_425);
    stringBuffer.append(property.get("PROPERTY"));
    stringBuffer.append(TEXT_426);
    stringBuffer.append(property.get("VALUE"));
    stringBuffer.append(TEXT_427);
    
        }

        // Set hdp.version for driver & application master
        boolean setHdpVersion = ElementParameterParser.getBooleanValue(sparkConfig, "__SET_HDP_VERSION__");
        String hdpVersion = ElementParameterParser.getValue(sparkConfig, "__HDP_VERSION__");
        if(setHdpVersion && hdpVersion != null){

    stringBuffer.append(TEXT_428);
    stringBuffer.append(hdpVersion);
    stringBuffer.append(TEXT_429);
    stringBuffer.append(hdpVersion);
    stringBuffer.append(TEXT_430);
    
            }

    stringBuffer.append(TEXT_431);
     if(!useCloudLauncher) { 
    stringBuffer.append(TEXT_432);
    stringBuffer.append(ElementParameterParser.getValue(sparkConfig, "__SPARK_SCRATCH_DIR__"));
    stringBuffer.append(TEXT_433);
     }
        for (INode pNode : process.getNodesOfType("tSqlRow")) {
        	if(!((java.util.List<java.util.Map<String, String>>) ElementParameterParser.getObjectValue(pNode, "__TEMP_SQL_UDF_FUNCTIONS__")).isEmpty()) {
        
    stringBuffer.append(TEXT_434);
    
        	break;
        	}
        }
        
    stringBuffer.append(TEXT_435);
    



    // This part of code is generated only if the used distribution execute Spark jobs through the Spark Job Server. ONly used by HD Insight currently.
    if(isExecutedThroughSparkJobServer) {

    stringBuffer.append(TEXT_436);
    
            passwordFieldName = "__HDINSIGHT_PASSWORD__";
            if (ElementParameterParser.canEncrypt(sparkConfig, passwordFieldName)) {

    stringBuffer.append(TEXT_437);
    stringBuffer.append(ElementParameterParser.getEncryptedValue(sparkConfig, passwordFieldName));
    stringBuffer.append(TEXT_438);
    
            } else {

    stringBuffer.append(TEXT_439);
    stringBuffer.append( ElementParameterParser.getValue(sparkConfig, passwordFieldName));
    stringBuffer.append(TEXT_440);
    
            }

    stringBuffer.append(TEXT_441);
    stringBuffer.append(ElementParameterParser.getValue(sparkConfig, "__HDINSIGHT_ENDPOINT__"));
    stringBuffer.append(TEXT_442);
    stringBuffer.append(ElementParameterParser.getValue(sparkConfig, "__HDINSIGHT_USERNAME__"));
    stringBuffer.append(TEXT_443);
    stringBuffer.append(codeGenArgument.getCurrentProjectName().toLowerCase());
    stringBuffer.append(TEXT_444);
    stringBuffer.append(JavaResourcesHelper.getJobFolderName(className, process.getVersion()));
    stringBuffer.append(TEXT_445);
    stringBuffer.append(className);
    stringBuffer.append(TEXT_446);
    
    }

    if(isExecutedThroughLivy) {

    stringBuffer.append(TEXT_447);
    
            passwordFieldName = "__HDINSIGHT_PASSWORD__";
            if (ElementParameterParser.canEncrypt(sparkConfig, passwordFieldName)) {

    stringBuffer.append(TEXT_448);
    stringBuffer.append(ElementParameterParser.getEncryptedValue(sparkConfig, passwordFieldName));
    stringBuffer.append(TEXT_449);
    
            } else {

    stringBuffer.append(TEXT_450);
    stringBuffer.append( ElementParameterParser.getValue(sparkConfig, passwordFieldName));
    stringBuffer.append(TEXT_451);
    
            }

            passwordFieldName = "__WASB_PASSWORD__";
            if (ElementParameterParser.canEncrypt(sparkConfig, passwordFieldName)) {

    stringBuffer.append(TEXT_452);
    stringBuffer.append(ElementParameterParser.getEncryptedValue(sparkConfig, passwordFieldName));
    stringBuffer.append(TEXT_453);
    
            } else {

    stringBuffer.append(TEXT_454);
    stringBuffer.append( ElementParameterParser.getValue(sparkConfig, passwordFieldName));
    stringBuffer.append(TEXT_455);
    
            }

    stringBuffer.append(TEXT_456);
    stringBuffer.append(ElementParameterParser.getValue(sparkConfig, "__WASB_USERNAME__"));
    stringBuffer.append(TEXT_457);
    stringBuffer.append(ElementParameterParser.getValue(sparkConfig, "__WASB_CONTAINER__"));
    stringBuffer.append(TEXT_458);
    stringBuffer.append(ElementParameterParser.getValue(sparkConfig, "__HDINSIGHT_USERNAME__"));
    stringBuffer.append(TEXT_459);
    stringBuffer.append(codeGenArgument.getCurrentProjectName().toLowerCase());
    stringBuffer.append(TEXT_460);
    stringBuffer.append(JavaResourcesHelper.getJobFolderName(className, process.getVersion()));
    stringBuffer.append(TEXT_461);
    stringBuffer.append(className);
    stringBuffer.append(TEXT_462);
    stringBuffer.append(ElementParameterParser.getValue(sparkConfig, "__LIVY_HOST__"));
    stringBuffer.append(TEXT_463);
    stringBuffer.append(ElementParameterParser.getValue(sparkConfig, "__LIVY_PORT__"));
    stringBuffer.append(TEXT_464);
    stringBuffer.append(ElementParameterParser.getValue(sparkConfig, "__REMOTE_FOLDER__"));
    stringBuffer.append(TEXT_465);
    stringBuffer.append(ElementParameterParser.getValue(sparkConfig, "__LIVY_USERNAME__"));
    stringBuffer.append(TEXT_466);
    
                if(tuningProperties) {

    stringBuffer.append(TEXT_467);
    
                    if(setExecutorCores && !useLocalMode) {

    stringBuffer.append(TEXT_468);
    
                    }

    stringBuffer.append(TEXT_469);
    
                }

    stringBuffer.append(TEXT_470);
    
    } // end of livy mode
    else if(isGoogleDataprocDistribution) {

    stringBuffer.append(TEXT_471);
    stringBuffer.append(ElementParameterParser.getValue(sparkConfig, "__GOOGLE_CLUSTER_ID__"));
    stringBuffer.append(TEXT_472);
    stringBuffer.append(ElementParameterParser.getValue(sparkConfig, "__GOOGLE_REGION__"));
    stringBuffer.append(TEXT_473);
    stringBuffer.append(ElementParameterParser.getValue(sparkConfig, "__GOOGLE_PROJECT_ID__"));
    stringBuffer.append(TEXT_474);
    
                        if(ElementParameterParser.getBooleanValue(sparkConfig, "__DEFINE_PATH_TO_GOOGLE_CREDENTIALS__")) {

    stringBuffer.append(TEXT_475);
    stringBuffer.append(ElementParameterParser.getValue(sparkConfig, "__PATH_TO_GOOGLE_CREDENTIALS__"));
    stringBuffer.append(TEXT_476);
    
                        }

    stringBuffer.append(TEXT_477);
    stringBuffer.append(ElementParameterParser.getValue(sparkConfig, "__GOOGLE_JARS_BUCKET__"));
    stringBuffer.append(TEXT_478);
    stringBuffer.append(codeGenArgument.getCurrentProjectName().toLowerCase());
    stringBuffer.append(TEXT_479);
    stringBuffer.append(JavaResourcesHelper.getJobFolderName(className, process.getVersion()));
    stringBuffer.append(TEXT_480);
    stringBuffer.append(className);
    stringBuffer.append(TEXT_481);
    
            boolean customLogLevel  = "true".equals(ElementParameterParser.getValue(process, "__LOG4J_RUN_ACTIVATE__"));
            if(customLogLevel){
            String runLevel = ElementParameterParser.getValue(process, "__LOG4J_RUN_LEVEL__").toUpperCase();
            
    stringBuffer.append(TEXT_482);
    stringBuffer.append(runLevel);
    stringBuffer.append(TEXT_483);
    }
    stringBuffer.append(TEXT_484);
    if(isLog4jEnabled) {
    stringBuffer.append(TEXT_485);
    }
    stringBuffer.append(TEXT_486);
    if(isLog4jEnabled) {
    stringBuffer.append(TEXT_487);
    }
    stringBuffer.append(TEXT_488);
    
    } // end of dataproc
    else if(isDatabricksDistribution) {

    stringBuffer.append(TEXT_489);
    stringBuffer.append(codeGenArgument.getCurrentProjectName().toLowerCase());
    stringBuffer.append(TEXT_490);
    stringBuffer.append(JavaResourcesHelper.getJobFolderName(className, process.getVersion()));
    stringBuffer.append(TEXT_491);
    stringBuffer.append(className);
    stringBuffer.append(TEXT_492);
    stringBuffer.append(ElementParameterParser.getValue(sparkConfig, "__DATABRICKS_DBFS_DEP_FOLDER__"));
    stringBuffer.append(TEXT_493);
    stringBuffer.append(ElementParameterParser.getValue(sparkConfig, "__DATABRICKS_ENDPOINT__"));
    stringBuffer.append(TEXT_494);
    stringBuffer.append(ElementParameterParser.getEncryptedValue(sparkConfig, "__DATABRICKS_TOKEN__"));
    stringBuffer.append(TEXT_495);
    stringBuffer.append(ElementParameterParser.getValue(sparkConfig, "__DATABRICKS_CLUSTER_ID__"));
    stringBuffer.append(TEXT_496);
    stringBuffer.append(studioVersion);
    stringBuffer.append(TEXT_497);
    
    } // end of databricks
    else if (isQuboleDistribution) {
    
    stringBuffer.append(TEXT_498);
    stringBuffer.append(ElementParameterParser.getValue(sparkConfig, "__QUBOLE_S3_ACCESS_KEY__"));
    stringBuffer.append(TEXT_499);
    stringBuffer.append(ElementParameterParser.getPasswordValue(sparkConfig, "__QUBOLE_S3_SECRET_KEY__"));
    stringBuffer.append(TEXT_500);
    stringBuffer.append(ElementParameterParser.getValue(sparkConfig, "__QUBOLE_S3_BUCKET_NAME__"));
    stringBuffer.append(TEXT_501);
    stringBuffer.append(ElementParameterParser.getValue(sparkConfig, "__QUBOLE_S3_BUCKET_KEY__"));
    stringBuffer.append(TEXT_502);
    stringBuffer.append(ElementParameterParser.getValue(sparkConfig, "__QUBOLE_S3_REGION__"));
    stringBuffer.append(TEXT_503);
    stringBuffer.append(master);
    stringBuffer.append(TEXT_504);
    stringBuffer.append(codeGenArgument.getCurrentProjectName().toLowerCase());
    stringBuffer.append(TEXT_505);
    stringBuffer.append(JavaResourcesHelper.getJobFolderName(className, process.getVersion()));
    stringBuffer.append(TEXT_506);
    stringBuffer.append(className);
    stringBuffer.append(TEXT_507);
    stringBuffer.append(deployMode);
    stringBuffer.append(TEXT_508);
    stringBuffer.append(studioVersion);
    stringBuffer.append(TEXT_509);
     if (ElementParameterParser.getBooleanValue(sparkConfig, "__QUBOLE_CLUSTER__")) {
    stringBuffer.append(TEXT_510);
    stringBuffer.append(ElementParameterParser.getValue(sparkConfig, "__QUBOLE_CLUSTER_LABEL__"));
    stringBuffer.append(TEXT_511);
     } 
    stringBuffer.append(TEXT_512);
    stringBuffer.append(ElementParameterParser.getEncryptedValue(sparkConfig, "__QUBOLE_API_TOKEN__"));
    stringBuffer.append(TEXT_513);
     if (ElementParameterParser.getBooleanValue(sparkConfig, "__QUBOLE_ENDPOINT__")) {
    stringBuffer.append(TEXT_514);
    stringBuffer.append(ElementParameterParser.getValue(sparkConfig, "__QUBOLE_ENDPOINT_URL__"));
    stringBuffer.append(TEXT_515);
     } 
    stringBuffer.append(TEXT_516);
    if(isLog4jEnabled) {
    stringBuffer.append(TEXT_517);
    }
    stringBuffer.append(TEXT_518);
    
    } // end of qubole
    else if(isAltusDistribution) {
        
    stringBuffer.append(TEXT_519);
     if ("\"AWS\"".equals(ElementParameterParser.getValue(sparkConfig, "__ALTUS_CLOUD_PROVIDER__"))) {
            	//Sync with name rule of Job server.
            	passwordFieldName = "__ALTUS_S3_SECRET_KEY__";
            } else {
            	passwordFieldName = "__ALTUS_AZURE_APP_KEY__";
            }
            
    if (ElementParameterParser.canEncrypt(sparkConfig, passwordFieldName)) {
    stringBuffer.append(TEXT_520);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_521);
    stringBuffer.append(ElementParameterParser.getEncryptedValue(sparkConfig, passwordFieldName));
    stringBuffer.append(TEXT_522);
    } else {
    stringBuffer.append(TEXT_523);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_524);
    stringBuffer.append( ElementParameterParser.getValue(sparkConfig, passwordFieldName));
    stringBuffer.append(TEXT_525);
    }
    stringBuffer.append(TEXT_526);
     if ("\"AWS\"".equals(ElementParameterParser.getValue(sparkConfig, "__ALTUS_CLOUD_PROVIDER__"))) {
    stringBuffer.append(TEXT_527);
    stringBuffer.append(ElementParameterParser.getValue(sparkConfig, "__ALTUS_S3_ACCESS_KEY__"));
    stringBuffer.append(TEXT_528);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_529);
    stringBuffer.append(ElementParameterParser.getValue(sparkConfig, "__ALTUS_S3_REGION__"));
    stringBuffer.append(TEXT_530);
    stringBuffer.append(ElementParameterParser.getValue(sparkConfig, "__ALTUS_BUCKET_NAME__"));
    stringBuffer.append(TEXT_531);
    stringBuffer.append(ElementParameterParser.getValue(sparkConfig, "__ALTUS_JARS_BUCKET__"));
    stringBuffer.append(TEXT_532);
     } else {
			String adlsAccountFQDN = ElementParameterParser.getValue(sparkConfig, "__ALTUS_AZURE_ADLS_ACCOUNT_FQDN__");
			String adlsAppId = ElementParameterParser.getValue(sparkConfig, "__ALTUS_AZURE_APP_ID__");
			String adlsAuthTokenEndPoint = "";
			if (ElementParameterParser.canEncrypt(sparkConfig, "__ALTUS_AZURE_TOKEN_ENDPOINT__")) {
                adlsAuthTokenEndPoint = ElementParameterParser.getEncryptedValue(sparkConfig, "__ALTUS_AZURE_TOKEN_ENDPOINT__");
                adlsAuthTokenEndPoint = "routines.system.PasswordEncryptUtil.decryptPassword(" + adlsAuthTokenEndPoint + ")";
            } else {
                adlsAuthTokenEndPoint = ElementParameterParser.getValue(sparkConfig, "__ALTUS_AZURE_TOKEN_ENDPOINT__");
            }           
			String adlsClientKey = "";
			if (ElementParameterParser.canEncrypt(sparkConfig, "__ALTUS_AZURE_APP_KEY__")) {
                adlsClientKey = ElementParameterParser.getEncryptedValue(sparkConfig, "__ALTUS_AZURE_APP_KEY__");
                adlsClientKey = "routines.system.PasswordEncryptUtil.decryptPassword(" + adlsClientKey + ")";
            } else {
                adlsClientKey = ElementParameterParser.getValue(sparkConfig, "__ALTUS_AZURE_APP_KEY__");
            }           
            
    stringBuffer.append(TEXT_533);
    stringBuffer.append(adlsAccountFQDN);
    stringBuffer.append(TEXT_534);
    stringBuffer.append(adlsAppId);
    stringBuffer.append(TEXT_535);
    stringBuffer.append(adlsAuthTokenEndPoint);
    stringBuffer.append(TEXT_536);
    stringBuffer.append(adlsClientKey);
    stringBuffer.append(TEXT_537);
     }
    stringBuffer.append(TEXT_538);
     if (ElementParameterParser.getBooleanValue(sparkConfig, "__ALTUS_REUSE_CLUSTER__")) {
    stringBuffer.append(TEXT_539);
    stringBuffer.append(ElementParameterParser.getValue(sparkConfig, "__ALTUS_CLUSTER_NAME__"));
    stringBuffer.append(TEXT_540);
    stringBuffer.append(ElementParameterParser.getBooleanValue(sparkConfig, "__ALTUS_SET_CREDENTIALS__"));
    stringBuffer.append(TEXT_541);
    stringBuffer.append(ElementParameterParser.getValue(sparkConfig, "__ALTUS_ACCESS_KEY__"));
    stringBuffer.append(TEXT_542);
    stringBuffer.append(ElementParameterParser.getValue(sparkConfig, "__ALTUS_SECRET_KEY__"));
    stringBuffer.append(TEXT_543);
    stringBuffer.append(ElementParameterParser.getValue(sparkConfig, "__ALTUS_CLI_PATH__"));
    stringBuffer.append(TEXT_544);
    stringBuffer.append(codeGenArgument.getCurrentProjectName().toLowerCase());
    stringBuffer.append(TEXT_545);
    stringBuffer.append(JavaResourcesHelper.getJobFolderName(className, process.getVersion()));
    stringBuffer.append(TEXT_546);
    stringBuffer.append(className);
    stringBuffer.append(TEXT_547);
    
                    boolean customLogLevel  = "true".equals(ElementParameterParser.getValue(process, "__LOG4J_RUN_ACTIVATE__"));
                    if(customLogLevel){
                        String runLevel = ElementParameterParser.getValue(process, "__LOG4J_RUN_LEVEL__").toUpperCase();
                        
    stringBuffer.append(TEXT_548);
    stringBuffer.append(runLevel);
    stringBuffer.append(TEXT_549);
    }
    stringBuffer.append(TEXT_550);
     } else { 
    stringBuffer.append(TEXT_551);
    stringBuffer.append(ElementParameterParser.getValue(sparkConfig, "__ALTUS_CLUSTER_NAME__"));
    stringBuffer.append(TEXT_552);
    stringBuffer.append(ElementParameterParser.getValue(sparkConfig, "__ALTUS_ENVIRONMENT_NAME__"));
    stringBuffer.append(TEXT_553);
    stringBuffer.append(ElementParameterParser.getValue(sparkConfig, "__ALTUS_CLOUD_PROVIDER__"));
    stringBuffer.append(TEXT_554);
    stringBuffer.append(ElementParameterParser.getBooleanValue(sparkConfig, "__ALTUS_DELETE_AFTER_EXECUTION__"));
    stringBuffer.append(TEXT_555);
    stringBuffer.append(ElementParameterParser.getBooleanValue(sparkConfig, "__ALTUS_SET_CREDENTIALS__"));
    stringBuffer.append(TEXT_556);
    stringBuffer.append(ElementParameterParser.getValue(sparkConfig, "__ALTUS_ACCESS_KEY__"));
    stringBuffer.append(TEXT_557);
    stringBuffer.append(ElementParameterParser.getValue(sparkConfig, "__ALTUS_SECRET_KEY__"));
    stringBuffer.append(TEXT_558);
    stringBuffer.append(ElementParameterParser.getBooleanValue(sparkConfig, "__ALTUS_USE_CUSTOM_JSON__"));
    stringBuffer.append(TEXT_559);
    
                    if (ElementParameterParser.getBooleanValue(sparkConfig, "__ALTUS_USE_CUSTOM_JSON__")) {
                        String lineSeparator = (String) java.security.AccessController.doPrivileged(
                                new sun.security.action.GetPropertyAction("line.separator"));
                        String altusJson = ElementParameterParser.getValue(sparkConfig, "__ALTUS_CUSTOM_JSON__");
                        StringBuilder altusJsonFormatted = new StringBuilder();
                        for(String item : altusJson.split(lineSeparator))
                            altusJsonFormatted.append("\"").append(item.replace("\"", "\\\"")).append("\" +");
                        // Remove trailing plus.
                        if (altusJsonFormatted.length() > 0)
                            altusJsonFormatted.setLength(altusJsonFormatted.length() - 1);
                        
    stringBuffer.append(TEXT_560);
    stringBuffer.append(altusJsonFormatted);
    stringBuffer.append(TEXT_561);
     } else { 
						String instanceTypeField;
						String customBoostrapScript = ElementParameterParser.getValue(sparkConfig, "__ALTUS_CUSTOM_BOOTSTRAP_SCRIPT__");
						if (customBoostrapScript == null || customBoostrapScript.isEmpty()) {
							customBoostrapScript = "\"\"";
						}
						if ("\"AWS\"".equals(ElementParameterParser.getValue(sparkConfig, "__ALTUS_CLOUD_PROVIDER__"))){
							instanceTypeField = "__ALTUS_INSTANCE_TYPE__";
						}else {
							instanceTypeField = "__ALTUS_AZURE_INSTANCE_TYPE__";
						}
                    
    stringBuffer.append(TEXT_562);
    stringBuffer.append(ElementParameterParser.getValue(sparkConfig, instanceTypeField));
    stringBuffer.append(TEXT_563);
    stringBuffer.append(ElementParameterParser.getValue(sparkConfig, "__ALTUS_WORKER_NODE__"));
    stringBuffer.append(TEXT_564);
    stringBuffer.append(ElementParameterParser.getValue(sparkConfig, "__ALTUS_SSH_KEY__"));
    stringBuffer.append(TEXT_565);
    stringBuffer.append(ElementParameterParser.getValue(sparkConfig, "__ALTUS_CLOUDERA_MANAGER_USERNAME__"));
    stringBuffer.append(TEXT_566);
    stringBuffer.append(ElementParameterParser.getValue(sparkConfig, "__ALTUS_CLOUDERA_MANAGER_PASSWORD__"));
    stringBuffer.append(TEXT_567);
    stringBuffer.append(customBoostrapScript);
    stringBuffer.append(TEXT_568);
     } 
    stringBuffer.append(TEXT_569);
    stringBuffer.append(ElementParameterParser.getValue(sparkConfig, "__ALTUS_CLI_PATH__"));
    stringBuffer.append(TEXT_570);
    stringBuffer.append(codeGenArgument.getCurrentProjectName().toLowerCase());
    stringBuffer.append(TEXT_571);
    stringBuffer.append(JavaResourcesHelper.getJobFolderName(className, process.getVersion()));
    stringBuffer.append(TEXT_572);
    stringBuffer.append(className);
    stringBuffer.append(TEXT_573);
    
                    boolean customLogLevel  = "true".equals(ElementParameterParser.getValue(process, "__LOG4J_RUN_ACTIVATE__"));
                    if(customLogLevel){
                        String runLevel = ElementParameterParser.getValue(process, "__LOG4J_RUN_LEVEL__").toUpperCase();
                        
    stringBuffer.append(TEXT_574);
    stringBuffer.append(runLevel);
    stringBuffer.append(TEXT_575);
    }
    stringBuffer.append(TEXT_576);
    }
    stringBuffer.append(TEXT_577);
    if(isLog4jEnabled) {
    stringBuffer.append(TEXT_578);
    }
    stringBuffer.append(TEXT_579);
    if(isLog4jEnabled) {
    stringBuffer.append(TEXT_580);
    }
    stringBuffer.append(TEXT_581);
    
    } // end of altus
    else if(useYarnClusterMode) { // Yarn cluster, but neither livy, nor Altus, nor dataproc 

    stringBuffer.append(TEXT_582);
    stringBuffer.append(codeGenArgument.getCurrentProjectName().toLowerCase());
    stringBuffer.append(TEXT_583);
    stringBuffer.append(JavaResourcesHelper.getJobFolderName(className, process.getVersion()));
    stringBuffer.append(TEXT_584);
    stringBuffer.append(className);
    stringBuffer.append(TEXT_585);
    
            if(org.talend.hadoop.distribution.ESparkVersion.SPARK_2_0.compareTo(sparkVersion) > 0) {

    stringBuffer.append(TEXT_586);
    
                if (tuningProperties) {
                    
    stringBuffer.append(TEXT_587);
    stringBuffer.append(ElementParameterParser.getValue(sparkConfig, "__SPARK_DRIVER_CORES__"));
    stringBuffer.append(TEXT_588);
    stringBuffer.append(ElementParameterParser.getValue(sparkConfig, "__SPARK_DRIVER_MEM__"));
    stringBuffer.append(TEXT_589);
    stringBuffer.append(ElementParameterParser.getValue(sparkConfig, "__SPARK_EXECUTOR_MEM__"));
    stringBuffer.append(TEXT_590);
    
                    String allocationMode = ElementParameterParser.getValue(sparkConfig, "__SPARK_YARN_ALLOC_TYPE__");
                    if("FIXED".equalsIgnoreCase(allocationMode)) {
                        
    stringBuffer.append(TEXT_591);
    stringBuffer.append(ElementParameterParser.getValue(sparkConfig, "__SPARK_EXECUTOR_INSTANCES__"));
    stringBuffer.append(TEXT_592);
    
                    }
                    if(setExecutorCores && !useLocalMode) {
                        
    stringBuffer.append(TEXT_593);
    stringBuffer.append(ElementParameterParser.getValue(sparkConfig, "__SPARK_EXECUTOR_CORES__"));
    stringBuffer.append(TEXT_594);
    
                    }
                }
                
    stringBuffer.append(TEXT_595);
    
            } else {

    stringBuffer.append(TEXT_596);
    
            }

    stringBuffer.append(TEXT_597);
    
    } // end of YARN cluster mode

    // job wide SSL configurations
    // Find the tSetKeystore node
    List<INode> tSetKeystoreNodes = new ArrayList<INode>();
    for (INode pNode : process.getNodesOfType("tSetKeystore")) {
        tSetKeystoreNodes.add(pNode);
    }
    // We don't want to generate the job wide SSL configurations unless there's a tMongoDBConfiguration
    // to avoid breaking existing component specific SSL configurations
    // YOu can add other components beside tMongoDBConfiguration
    // Find the tMongoDBConfiguration node
    List<INode> tMongoDBConfigurationSSLNodes = new ArrayList<INode>();
    for (INode pNode : process.getNodesOfType("tMongoDBConfiguration")) {
        boolean usesSSL = ElementParameterParser.getBooleanValue(pNode, "__USE_SSL__");
        if(usesSSL) tMongoDBConfigurationSSLNodes.add(pNode);
    }

    stringBuffer.append(TEXT_598);
    
        // Generate job wide ssl configuration when :
        if(tSetKeystoreNodes.size() > 0){
            // Use the configurations of the first keystore
            INode node = tSetKeystoreNodes.get(0);
            String trustStoreType = ElementParameterParser.getValue(node,"__TRUSTSTORE_TYPE__");
            String trustStorePath = ElementParameterParser.getValue(node,"__SSL_TRUSTSERVER_TRUSTSTORE__");
            String trustStorePwd = ElementParameterParser.getValue(node,"__SSL_TRUSTSERVER_PASSWORD__");
            boolean needClientAuth = "true".equals(ElementParameterParser.getValue(node,"__NEED_CLIENT_AUTH__"));
            String keyStoreType = ElementParameterParser.getValue(node,"__KEYSTORE_TYPE__");
            String keyStorePath = ElementParameterParser.getValue(node,"__SSL_KEYSTORE__");
            String keyStorePwd = ElementParameterParser.getValue(node,"__SSL_KEYSTORE_PASSWORD__");
            boolean verifyName = ("true").equals(ElementParameterParser.getValue(node,"__VERIFY_NAME__"));

            if(!verifyName){

    stringBuffer.append(TEXT_599);
    
            }

    stringBuffer.append(TEXT_600);
    stringBuffer.append(trustStorePath );
    stringBuffer.append(TEXT_601);
    stringBuffer.append(trustStoreType );
    stringBuffer.append(TEXT_602);
    
            passwordFieldName = "__SSL_TRUSTSERVER_PASSWORD__";

    if (ElementParameterParser.canEncrypt(node, passwordFieldName)) {
    stringBuffer.append(TEXT_603);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_604);
    stringBuffer.append(ElementParameterParser.getEncryptedValue(node, passwordFieldName));
    stringBuffer.append(TEXT_605);
    } else {
    stringBuffer.append(TEXT_606);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_607);
    stringBuffer.append( ElementParameterParser.getValue(node, passwordFieldName));
    stringBuffer.append(TEXT_608);
    }
    stringBuffer.append(TEXT_609);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_610);
    
            if(needClientAuth){

    stringBuffer.append(TEXT_611);
    stringBuffer.append(keyStorePath );
    stringBuffer.append(TEXT_612);
    stringBuffer.append(keyStoreType);
    stringBuffer.append(TEXT_613);
    
                passwordFieldName = "__SSL_KEYSTORE_PASSWORD__";
                if (ElementParameterParser.canEncrypt(node, passwordFieldName)) {

    stringBuffer.append(TEXT_614);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_615);
    stringBuffer.append(ElementParameterParser.getEncryptedValue(node, passwordFieldName));
    stringBuffer.append(TEXT_616);
    
                } else {

    stringBuffer.append(TEXT_617);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_618);
    stringBuffer.append( ElementParameterParser.getValue(node, passwordFieldName));
    stringBuffer.append(TEXT_619);
    
                }

    stringBuffer.append(TEXT_620);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_621);
    
            }else{

    stringBuffer.append(TEXT_622);
    
            }
        } else {
            // Do not any Generate SSL configurations
            
    stringBuffer.append(TEXT_623);
    
        }

    stringBuffer.append(TEXT_624);
    stringBuffer.append(className);
    stringBuffer.append(TEXT_625);
    stringBuffer.append(jobClassPackage);
    stringBuffer.append(TEXT_626);
    
                for(IContextParameter ctxParam :params){
                    String typeToGenerate = "String";
                    if(ctxParam.getType().equals("id_List Of Value") || ctxParam.getType().equals("id_File") || ctxParam.getType().equals("id_Directory")){
                        typeToGenerate = "String";
                    }else{
                        typeToGenerate = JavaTypesManager.getTypeToGenerate(ctxParam.getType(),true);
                    }
                    
    stringBuffer.append(TEXT_627);
    stringBuffer.append(ctxParam.getName());
    stringBuffer.append(TEXT_628);
    stringBuffer.append(ctxParam.getName());
    stringBuffer.append(TEXT_629);
    stringBuffer.append(typeToGenerate );
    stringBuffer.append(TEXT_630);
    stringBuffer.append(ctxParam.getName());
    stringBuffer.append(TEXT_631);
    
                }
                
    stringBuffer.append(TEXT_632);
    
        if (talendLineageAPI.doRequireLineageSupport(sparkConfig)) {
            if(talendLineageAPI.doRequireClouderaNavigatorSupport(sparkConfig)){
                
    stringBuffer.append(TEXT_633);
    
                talendLineageAPI.generateClouderaNavigatorLinageCreator(sparkConfig);
            } else if(talendLineageAPI.doRequireAtlasSupport(sparkConfig)){
                
    stringBuffer.append(TEXT_634);
    
                talendLineageAPI.generateAtlasLinageCreator(sparkConfig);
            }
        }
        
    stringBuffer.append(TEXT_635);
    stringBuffer.append(className);
    stringBuffer.append(TEXT_636);
    stringBuffer.append(jobClassPackage);
    stringBuffer.append(TEXT_637);
    
                if(!useLocalMode) {

    stringBuffer.append(TEXT_638);
    stringBuffer.append(className);
    stringBuffer.append(TEXT_639);
    stringBuffer.append(jobClassPackage);
    stringBuffer.append(TEXT_640);
    
                }

    stringBuffer.append(TEXT_641);
    
            for(IContextParameter ctxParam : params){
            
    stringBuffer.append(TEXT_642);
    stringBuffer.append(ctxParam.getName());
    stringBuffer.append(TEXT_643);
    stringBuffer.append(ctxParam.getName());
    stringBuffer.append(TEXT_644);
    stringBuffer.append(ctxParam.getName());
    stringBuffer.append(TEXT_645);
    stringBuffer.append(ctxParam.getName());
    stringBuffer.append(TEXT_646);
    
            }
            
    stringBuffer.append(TEXT_647);
    
    if(useYarnClusterMode) {

    stringBuffer.append(TEXT_648);
    
    }

    stringBuffer.append(TEXT_649);
    
    if(isQuboleDistribution || isDatabricksDistribution || isExecutedThroughLivy || isGoogleDataprocDistribution || isAltusDistribution || useYarnClusterMode ) {

    stringBuffer.append(TEXT_650);
     if (isExecutedThroughLivy) { 
    stringBuffer.append(TEXT_651);
     } else if (isGoogleDataprocDistribution) { 
    stringBuffer.append(TEXT_652);
     } else if (isDatabricksDistribution) { 
    stringBuffer.append(TEXT_653);
     } else if (isQuboleDistribution) { 
    stringBuffer.append(TEXT_654);
     } else if (isAltusDistribution) { 
    stringBuffer.append(TEXT_655);
     } else if (useYarnClusterMode) { 
    stringBuffer.append(TEXT_656);
     } 
    stringBuffer.append(TEXT_657);
    
    }

    stringBuffer.append(TEXT_658);
     if(useCloudLauncher || useYarnClusterMode) { 
    stringBuffer.append(TEXT_659);
     } 
    stringBuffer.append(TEXT_660);
     if(useCloudLauncher || useYarnClusterMode) { 
    stringBuffer.append(TEXT_661);
     } 
    stringBuffer.append(TEXT_662);
     if(useCloudLauncher || useYarnClusterMode) { 
    stringBuffer.append(TEXT_663);
     } 
     if(useCloudLauncher || useYarnClusterMode ||  isExecutedThroughSparkJobServer) { 
    stringBuffer.append(TEXT_664);
    if(isLog4jEnabled) {
    stringBuffer.append(TEXT_665);
    }
    stringBuffer.append(TEXT_666);
    if(isLog4jEnabled) {
    stringBuffer.append(TEXT_667);
    }
    stringBuffer.append(TEXT_668);
     } 
     if(useCloudLauncher || useYarnClusterMode) { 
    stringBuffer.append(TEXT_669);
     } 
    stringBuffer.append(TEXT_670);
    stringBuffer.append(TEXT_671);
    return stringBuffer.toString();
  }
}
