package org.talend.designer.codegen.translators.dataquality.matching;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.talend.core.model.metadata.IMetadataColumn;
import org.talend.core.model.metadata.IMetadataTable;
import org.talend.core.model.metadata.types.JavaType;
import org.talend.core.model.metadata.types.JavaTypesManager;
import org.talend.core.model.process.ElementParameterParser;
import org.talend.core.model.process.IBigDataNode;
import org.talend.core.model.process.IConnection;
import org.talend.core.model.process.IConnectionCategory;
import org.talend.core.model.process.INode;
import org.talend.designer.common.BigDataCodeGeneratorArgument;
import org.talend.designer.common.tsqlrow.TSqlRowUtil;

public class TMatchPredictSparkcodeJava
{
  protected static String nl;
  public static synchronized TMatchPredictSparkcodeJava create(String lineSeparator)
  {
    nl = lineSeparator;
    TMatchPredictSparkcodeJava result = new TMatchPredictSparkcodeJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "    public static class ";
  protected final String TEXT_2 = "_FromRowTo";
  protected final String TEXT_3 = " implements org.apache.spark.api.java.function.Function<org.apache.spark.sql.Row, ";
  protected final String TEXT_4 = "> {" + NL + "        " + NL + "        public ";
  protected final String TEXT_5 = " call(org.apache.spark.sql.Row row) {";
  protected final String TEXT_6 = NL + "            ";
  protected final String TEXT_7 = " result = new ";
  protected final String TEXT_8 = "();" + NL + "            org.apache.spark.sql.types.StructField[] structFields = row.schema().fields();" + NL + "            for (int i = 0; i < structFields.length; i++) {" + NL + "                org.apache.avro.Schema.Field avroField = ";
  protected final String TEXT_9 = ".getClassSchema().getField(structFields[i].name());" + NL + "                if (avroField != null){" + NL + "                    result.put(avroField.pos(), row.get(i));" + NL + "                }" + NL + "            }" + NL + "            return result;" + NL + "        }" + NL + "    }";
  protected final String TEXT_10 = NL + "        public static class ";
  protected final String TEXT_11 = "_FromRowTo";
  protected final String TEXT_12 = " implements org.apache.spark.api.java.function.Function<org.apache.spark.sql.Row, ";
  protected final String TEXT_13 = "> {" + NL + "    " + NL + "            public ";
  protected final String TEXT_14 = " call(org.apache.spark.sql.Row row) {";
  protected final String TEXT_15 = NL + "                ";
  protected final String TEXT_16 = " result = new ";
  protected final String TEXT_17 = "();" + NL + "                org.apache.spark.sql.types.StructField[] structFields = row.schema().fields();" + NL + "                for (int i = 0; i < structFields.length; i++) {" + NL + "                    org.apache.avro.Schema.Field avroField = ";
  protected final String TEXT_18 = ".getClassSchema().getField(structFields[i].name());" + NL + "                    if (avroField != null){" + NL + "                        result.put(avroField.pos(), row.get(i));" + NL + "                    }" + NL + "                }" + NL + "                return result;" + NL + "            }" + NL + "        }";
  protected final String TEXT_19 = NL + "        public static class ";
  protected final String TEXT_20 = "_FromRowTo";
  protected final String TEXT_21 = " implements org.apache.spark.api.java.function.Function<org.apache.spark.sql.Row, ";
  protected final String TEXT_22 = "> {" + NL + "            " + NL + "            public ";
  protected final String TEXT_23 = " call(org.apache.spark.sql.Row row) {";
  protected final String TEXT_24 = NL + "                ";
  protected final String TEXT_25 = " result = new ";
  protected final String TEXT_26 = "();" + NL + "                org.apache.spark.sql.types.StructField[] structFields = row.schema().fields();" + NL + "                for (int i = 0; i < structFields.length; i++) {" + NL + "                    org.apache.avro.Schema.Field avroField = ";
  protected final String TEXT_27 = ".getClassSchema().getField(structFields[i].name());" + NL + "                    if (avroField != null){" + NL + "                        result.put(avroField.pos(), row.get(i));" + NL + "                    }" + NL + "                }" + NL + "                return result;" + NL + "            }" + NL + "        }" + NL;
  protected final String TEXT_28 = NL + "            public static class ";
  protected final String TEXT_29 = "_From";
  protected final String TEXT_30 = "To";
  protected final String TEXT_31 = " implements org.apache.spark.api.java.function.Function<";
  protected final String TEXT_32 = ", ";
  protected final String TEXT_33 = "> {" + NL + "" + NL + "                public ";
  protected final String TEXT_34 = " call(";
  protected final String TEXT_35 = " input) {";
  protected final String TEXT_36 = NL + "                    ";
  protected final String TEXT_37 = " result = new ";
  protected final String TEXT_38 = "();";
  protected final String TEXT_39 = NL + "                            if(input.";
  protected final String TEXT_40 = " != null) {" + NL + "                                result.";
  protected final String TEXT_41 = " = new java.sql.";
  protected final String TEXT_42 = "(input.";
  protected final String TEXT_43 = ".getTime());" + NL + "                            } else {" + NL + "                                result.";
  protected final String TEXT_44 = " = null;" + NL + "                            }";
  protected final String TEXT_45 = NL + "                            result.";
  protected final String TEXT_46 = " = input.";
  protected final String TEXT_47 = ";";
  protected final String TEXT_48 = NL + "                     return result;" + NL + "                }" + NL + "            }";

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    
//Parse the inputs to this javajet generator.
final BigDataCodeGeneratorArgument codeGenArgument = (BigDataCodeGeneratorArgument) argument;
final INode node = (INode)codeGenArgument.getArgument();
final String cid = node.getUniqueName();
final boolean isLog4jEnabled = ("true").equals(ElementParameterParser.getValue(node.getProcess(), "__LOG4J_ACTIVATE__"));

final boolean isSpark1 = org.talend.hadoop.distribution.ESparkVersion.SPARK_2_0.compareTo(codeGenArgument.getSparkVersion()) > 0;

final String dataframeClass = isSpark1
    ? "org.apache.spark.sql.DataFrame"
    : "org.apache.spark.sql.Dataset<org.apache.spark.sql.Row>";

TSqlRowUtil tSqlRowUtil = new TSqlRowUtil(node);
String validateError = tSqlRowUtil.validate(true, true);
if (validateError != null) {
    // Cause the job compilation to explicitly fail if there is a problem.
    return "throw new JobConfigurationError(\"" + validateError +"\");";
}

    
//IO Connections
IConnection incomingConnection = null;
if (node.getIncomingConnections() != null) {
 for (IConnection in : node.getIncomingConnections()) {
     if (in.getLineStyle().hasConnectionCategory(IConnectionCategory.DATA)) {
         incomingConnection = in;
         break;
     }
 }
}
String incomingConnectionName = incomingConnection.getName();
String incomingStructName = codeGenArgument.getRecordStructName(incomingConnection);

IConnection suspectConnection = null;
boolean hasSuspectConnection = false;
List<? extends IConnection> outgoingSuspectConnections = node.getOutgoingConnections("SUSPECT");
if (outgoingSuspectConnections.size() > 0) {
    suspectConnection = outgoingSuspectConnections.get(0);
}
if(suspectConnection != null){
    hasSuspectConnection = true;
}

IConnection exactConnection = null;
boolean hasExactConnection = false;
List<? extends IConnection> outgoingExactConnections = node.getOutgoingConnections("EXACT");
if (outgoingExactConnections.size() > 0) {
    exactConnection = outgoingExactConnections.get(0);
}
if(exactConnection != null){
    hasExactConnection = true;
}

IConnection uniqueConnection = null;
boolean hasUniqueConnection = false;
List<? extends IConnection> outgoingUniqueConnections = node.getOutgoingConnections("UNIQUE");
if (outgoingUniqueConnections.size() > 0) {
    uniqueConnection = outgoingUniqueConnections.get(0);
}
if(uniqueConnection != null){
    hasUniqueConnection = true;
}

IConnection suspectSamplingConnection = null;
boolean hasSuspectSamplingConnection = false;
List<? extends IConnection> outgoingSuspectSamplingConnections = node.getOutgoingConnections("SUSPECT_SAMPLING");
if (outgoingSuspectSamplingConnections.size() > 0) {
    suspectSamplingConnection = outgoingSuspectSamplingConnections.get(0);
}
if(suspectSamplingConnection != null){
    hasSuspectSamplingConnection = true;
}


    
List<IMetadataTable> metadatas = node.getMetadataList();
if ((metadatas == null) || (metadatas.size() == 0))
    return "" ;
IMetadataTable metadata = metadatas.get(0);
if (metadata == null)
    return "";
List<IMetadataColumn> columns = metadata.getListColumns();

boolean needPairing = "UNPAIRED".equals(ElementParameterParser.getValue(node, "__INPUT_TYPE__"));

final boolean useTimestampForDatesInDataframes = ElementParameterParser.getBooleanValue(node, "__DATE_TO_TIMESTAMP_DF_TYPE_SUBSTITUTION__");

try {
    

    if(hasSuspectConnection){
        String suspectOutConnectionName = suspectConnection.getName();
        String suspectOutStructName = codeGenArgument.getRecordStructName(suspectConnection);
    
    stringBuffer.append(TEXT_1);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_2);
    stringBuffer.append(suspectOutStructName);
    stringBuffer.append(TEXT_3);
    stringBuffer.append(suspectOutStructName);
    stringBuffer.append(TEXT_4);
    stringBuffer.append(suspectOutStructName);
    stringBuffer.append(TEXT_5);
    stringBuffer.append(TEXT_6);
    stringBuffer.append(suspectOutStructName);
    stringBuffer.append(TEXT_7);
    stringBuffer.append(suspectOutStructName);
    stringBuffer.append(TEXT_8);
    stringBuffer.append(suspectOutStructName);
    stringBuffer.append(TEXT_9);
    
    }//end if(hasSuspectConnection)
    
    if(hasUniqueConnection){
        String uniqueOutConnectionName = uniqueConnection.getName();
        String uniqueOutStructName = codeGenArgument.getRecordStructName(uniqueConnection);

    stringBuffer.append(TEXT_10);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_11);
    stringBuffer.append(uniqueOutStructName);
    stringBuffer.append(TEXT_12);
    stringBuffer.append(uniqueOutStructName);
    stringBuffer.append(TEXT_13);
    stringBuffer.append(uniqueOutStructName);
    stringBuffer.append(TEXT_14);
    stringBuffer.append(TEXT_15);
    stringBuffer.append(uniqueOutStructName);
    stringBuffer.append(TEXT_16);
    stringBuffer.append(uniqueOutStructName);
    stringBuffer.append(TEXT_17);
    stringBuffer.append(uniqueOutStructName);
    stringBuffer.append(TEXT_18);
    
    }//end if(hasUniqueConnection)
    
    if(hasExactConnection){ 
        String exactOutConnectionName = exactConnection.getName();
        String exactOutStructName = codeGenArgument.getRecordStructName(exactConnection);

    stringBuffer.append(TEXT_19);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_20);
    stringBuffer.append(exactOutStructName);
    stringBuffer.append(TEXT_21);
    stringBuffer.append(exactOutStructName);
    stringBuffer.append(TEXT_22);
    stringBuffer.append(exactOutStructName);
    stringBuffer.append(TEXT_23);
    stringBuffer.append(TEXT_24);
    stringBuffer.append(exactOutStructName);
    stringBuffer.append(TEXT_25);
    stringBuffer.append(exactOutStructName);
    stringBuffer.append(TEXT_26);
    stringBuffer.append(exactOutStructName);
    stringBuffer.append(TEXT_27);
    
    }//end if(hasExactConnection)
    
        // If the incoming rowStruct contains a Date field (always typed as java.util.Date),
        // we must generate a new structure which replaces these java.util.Date instances by
        // java.sql.Date or java.sql.Timestamp instances.
       org.talend.designer.bigdata.avro.AvroRecordStructGenerator avroRecordStructGenerator =
           (org.talend.designer.bigdata.avro.AvroRecordStructGenerator) codeGenArgument.getRecordStructGenerator();

       // Some of the incoming connections might share the same schema (and then the same rowXStruct). We must generate the below code only once by schema (if necessary).
       java.util.Set<String> knownStructNames = new java.util.HashSet();

       if(tSqlRowUtil.containsDateFields(incomingConnection) && !knownStructNames.contains(incomingStructName)) {
            String suggestedDfStructName = "DF_"+incomingStructName;
            String dfStructName = avroRecordStructGenerator.generateRecordStructForDataFrame(suggestedDfStructName, incomingStructName, useTimestampForDatesInDataframes);
            knownStructNames.add(incomingStructName);

    stringBuffer.append(TEXT_28);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_29);
    stringBuffer.append(incomingStructName);
    stringBuffer.append(TEXT_30);
    stringBuffer.append(dfStructName);
    stringBuffer.append(TEXT_31);
    stringBuffer.append(incomingStructName);
    stringBuffer.append(TEXT_32);
    stringBuffer.append(dfStructName);
    stringBuffer.append(TEXT_33);
    stringBuffer.append(dfStructName);
    stringBuffer.append(TEXT_34);
    stringBuffer.append(incomingStructName);
    stringBuffer.append(TEXT_35);
    stringBuffer.append(TEXT_36);
    stringBuffer.append(dfStructName);
    stringBuffer.append(TEXT_37);
    stringBuffer.append(dfStructName);
    stringBuffer.append(TEXT_38);
    
                    for(IMetadataColumn column : columns) {
                        if(tSqlRowUtil.isDateField(column)) {

    stringBuffer.append(TEXT_39);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_40);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_41);
    stringBuffer.append(useTimestampForDatesInDataframes ? "Timestamp" : "Date");
    stringBuffer.append(TEXT_42);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_43);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_44);
    
                        } else {

    stringBuffer.append(TEXT_45);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_46);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_47);
    
                        }
                     } // end for(IMetadataColumn column : columns)

    stringBuffer.append(TEXT_48);
    
        } // end if(tSqlRowUtil.containsDateFields(incomingConnection) && !knownStructNames.contains(incomingStructName))
} catch (java.lang.Exception e) {
    // Do not generate sparkcode part : a clean exception has to be generated within sparkconfig part
}

    return stringBuffer.toString();
  }
}
