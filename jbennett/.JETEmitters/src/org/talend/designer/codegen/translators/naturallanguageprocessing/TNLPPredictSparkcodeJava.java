package org.talend.designer.codegen.translators.naturallanguageprocessing;

import org.talend.core.model.process.INode;
import org.talend.core.model.process.ElementParameterParser;
import org.talend.core.model.metadata.IMetadataTable;
import org.talend.core.model.metadata.IMetadataColumn;
import org.talend.core.model.process.IConnection;
import org.talend.core.model.process.EConnectionType;
import org.talend.designer.common.BigDataCodeGeneratorArgument;
import org.talend.core.model.metadata.types.JavaTypesManager;
import org.talend.core.model.process.IConnectionCategory;
import java.util.List;
import java.util.Map;
import java.util.ArrayList;

public class TNLPPredictSparkcodeJava
{
  protected static String nl;
  public static synchronized TNLPPredictSparkcodeJava create(String lineSeparator)
  {
    nl = lineSeparator;
    TNLPPredictSparkcodeJava result = new TNLPPredictSparkcodeJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "static final int ";
  protected final String TEXT_2 = "outSchemaSize = ";
  protected final String TEXT_3 = ";" + NL + "public static class ";
  protected final String TEXT_4 = "getTextTokenFunction implements org.apache.spark.api.java.function.PairFunction<";
  protected final String TEXT_5 = ", String, List<String>>{" + NL + "    @Override" + NL + "    public scala.Tuple2<String, List<String>> call(";
  protected final String TEXT_6 = " row){" + NL + "        String line = (String)row.get(\"";
  protected final String TEXT_7 = "\");" + NL + "        String tokens = (String)row.get(\"";
  protected final String TEXT_8 = "\");" + NL + "        List<String> tlist = new java.util.LinkedList<String>(java.util.Arrays.asList(tokens.split(\"\\t\")));" + NL + "        tlist.removeAll(java.util.Collections.singleton(\"\"));" + NL + "        return new scala.Tuple2<String, List<String>>(line, tlist);" + NL + "    }" + NL + "}" + NL + "" + NL + "public static class ";
  protected final String TEXT_9 = "getOtherFeatureFunction implements org.apache.spark.api.java.function.Function<";
  protected final String TEXT_10 = ", List<String>>{" + NL + "        " + NL + "    private List<String> featureColumn = new java.util.ArrayList<String>();" + NL + "" + NL + "    public ";
  protected final String TEXT_11 = "getOtherFeatureFunction(List<String> cols){" + NL + "        featureColumn = cols;" + NL + "    }" + NL + "" + NL + "    @Override" + NL + "    public List<String> call(";
  protected final String TEXT_12 = " row){" + NL + "        List<String> features = new java.util.ArrayList<String>();" + NL + "        if(featureColumn.size() != 0){" + NL + "            List<String[]> cols = new java.util.ArrayList<String[]>();" + NL + "            for(String c : featureColumn){" + NL + "                cols.add(((String)row.get(c)).split(\"\\t\"));" + NL + "            }" + NL + "            for(int i=0; i<cols.get(0).length; i++){" + NL + "                StringBuilder f = new StringBuilder(\"\");" + NL + "                for(int j=0; j<cols.size(); j++){" + NL + "                    f.append(cols.get(j)[i]+\"\\t\");" + NL + "                }" + NL + "                features.add(f.toString());" + NL + "            }" + NL + "        }" + NL + "        return features;" + NL + "    }" + NL + "}" + NL + "" + NL + "public static class ";
  protected final String TEXT_13 = "constructRowStruct implements org.apache.spark.api.java.function.Function<scala.Tuple2<";
  protected final String TEXT_14 = ", scala.Tuple2<String, String>>, ";
  protected final String TEXT_15 = ">{" + NL + "" + NL + "    @Override" + NL + "    public ";
  protected final String TEXT_16 = " call(scala.Tuple2<";
  protected final String TEXT_17 = ", scala.Tuple2<String, String>> pair){";
  protected final String TEXT_18 = NL + "        ";
  protected final String TEXT_19 = " output = new ";
  protected final String TEXT_20 = "();";
  protected final String TEXT_21 = NL + "            output.put(";
  protected final String TEXT_22 = ", pair._1().get(";
  protected final String TEXT_23 = "));";
  protected final String TEXT_24 = NL + "        output.put(";
  protected final String TEXT_25 = "outSchemaSize-2, pair._2()._1());" + NL + "        output.put(";
  protected final String TEXT_26 = "outSchemaSize-1, pair._2()._2());" + NL + "        return output;" + NL + "    }" + NL + "}";

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
     
final BigDataCodeGeneratorArgument codeGenArgument = (BigDataCodeGeneratorArgument) argument;
final INode node = (INode)codeGenArgument.getArgument();
final String cid = node.getUniqueName();

java.util.regex.Pattern p = java.util.regex.Pattern.compile("<PERSON>|<\\/PERSON>");
List<? extends IConnection> inConns = node.getIncomingConnections(EConnectionType.FLOW_MAIN);
List<IMetadataColumn> inputColumns = null;
IMetadataTable inputMetadataTable = null;
String inConnName = "";
String inConnTypeName = "";
String outConnName = "";
IMetadataTable outputMetadataTable = null;
java.util.List<IMetadataColumn> outputColumns = null;
List< ? extends IConnection> outConns = node.getOutgoingConnections();
String outConnTypeName = codeGenArgument.getRecordStructName(outConns.get(0));
if (inConns.size() > 0){
    inConnName = inConns.get(0).getName();
    inConnTypeName = codeGenArgument.getRecordStructName(inConns.get(0));
    inputMetadataTable = inConns.get(0).getMetadataTable();
    inputColumns = inputMetadataTable.getListColumns();
}
if (outConns.size() == 1){
    outConnName = outConns.get(0).getName();
    outputMetadataTable = outConns.get(0).getMetadataTable();
    outConnTypeName = codeGenArgument.getRecordStructName(outConns.get(0));
    outputColumns = outputMetadataTable.getListColumns();
}
final String textColumn = (String)ElementParameterParser.getValue(node, "__TEXT__");
final String tokenColumn = (String)ElementParameterParser.getValue(node, "__TOKEN__");

    stringBuffer.append(TEXT_1);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_2);
    stringBuffer.append(outputColumns.size());
    stringBuffer.append(TEXT_3);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_4);
    stringBuffer.append(inConnTypeName);
    stringBuffer.append(TEXT_5);
    stringBuffer.append(inConnTypeName);
    stringBuffer.append(TEXT_6);
    stringBuffer.append(textColumn);
    stringBuffer.append(TEXT_7);
    stringBuffer.append(tokenColumn);
    stringBuffer.append(TEXT_8);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_9);
    stringBuffer.append(inConnTypeName);
    stringBuffer.append(TEXT_10);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_11);
    stringBuffer.append(inConnTypeName);
    stringBuffer.append(TEXT_12);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_13);
    stringBuffer.append(inConnTypeName);
    stringBuffer.append(TEXT_14);
    stringBuffer.append(outConnTypeName);
    stringBuffer.append(TEXT_15);
    stringBuffer.append(outConnTypeName);
    stringBuffer.append(TEXT_16);
    stringBuffer.append(inConnTypeName);
    stringBuffer.append(TEXT_17);
    stringBuffer.append(TEXT_18);
    stringBuffer.append(outConnTypeName);
    stringBuffer.append(TEXT_19);
    stringBuffer.append(outConnTypeName);
    stringBuffer.append(TEXT_20);
    
        for(int i = 0; i < inputColumns.size(); i++) {
        
    stringBuffer.append(TEXT_21);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_22);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_23);
    
          }
        
    stringBuffer.append(TEXT_24);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_25);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_26);
    return stringBuffer.toString();
  }
}
