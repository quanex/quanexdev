package org.talend.designer.codegen.translators.dataquality.matching;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.talend.core.model.metadata.IMetadataColumn;
import org.talend.core.model.metadata.IMetadataTable;
import org.talend.core.model.metadata.types.JavaType;
import org.talend.core.model.metadata.types.JavaTypesManager;
import org.talend.core.model.process.ElementParameterParser;
import org.talend.core.model.process.IBigDataNode;
import org.talend.core.model.process.IConnection;
import org.talend.core.model.process.IConnectionCategory;
import org.talend.core.model.process.INode;
import org.talend.core.model.utils.NodeUtil;
import org.talend.designer.common.BigDataCodeGeneratorArgument;
import org.talend.designer.common.tmodelencoder.TModelEncoderUtil;
import org.talend.designer.common.tsqlrow.TSqlRowUtil;

public class TMatchIndexSparkconfigJava
{
  protected static String nl;
  public static synchronized TMatchIndexSparkconfigJava create(String lineSeparator)
  {
    nl = lineSeparator;
    TMatchIndexSparkconfigJava result = new TMatchIndexSparkconfigJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "        " + NL + "        // SparkSQL will use the default TimeZone to handle dates (see org.apache.spark.sql.catalyst.util.DateTimeUtils)." + NL + "        // To avoid inconsistencies with others components in the Studio, ensure that UTC is set as the default TimeZone." + NL + "        java.util.TimeZone.setDefault(java.util.TimeZone.getTimeZone(\"UTC\"));" + NL + "        " + NL + "        // java.util.Date -> java.sql.Timestamp conversions to be compliant with Spark SQL before creating our DataFrame" + NL + "        org.apache.spark.api.java.JavaRDD<";
  protected final String TEXT_2 = "> ";
  protected final String TEXT_3 = " = rdd_";
  protected final String TEXT_4 = ".map(new ";
  protected final String TEXT_5 = "_From";
  protected final String TEXT_6 = "To";
  protected final String TEXT_7 = "());";
  protected final String TEXT_8 = NL + NL + "//Convert the incoming RDD to DataFrame" + NL + "org.apache.spark.sql.SQLContext sqlContext_";
  protected final String TEXT_9 = " = new org.apache.spark.sql.SQLContext(ctx);" + NL + "    " + NL + "org.apache.spark.sql.Dataset<org.apache.spark.sql.Row> ";
  protected final String TEXT_10 = " = " + NL + "    sqlContext_";
  protected final String TEXT_11 = ".createDataFrame(";
  protected final String TEXT_12 = ", ";
  protected final String TEXT_13 = ".class);" + NL + "" + NL + "" + NL + "// Pairing model";
  protected final String TEXT_14 = NL + "    java.net.URI currentURI_";
  protected final String TEXT_15 = "_config = FileSystem.getDefaultUri(ctx.hadoopConfiguration());" + NL + "    FileSystem.setDefaultUri(ctx.hadoopConfiguration(), new java.net.URI(";
  protected final String TEXT_16 = "));" + NL + "    fs = FileSystem.get(ctx.hadoopConfiguration());";
  protected final String TEXT_17 = NL + "com.esotericsoftware.kryo.Kryo kryo_";
  protected final String TEXT_18 = " = new com.esotericsoftware.kryo.Kryo();" + NL + "TalendPipelineModelSerializer talendPipelineModelSerializer_";
  protected final String TEXT_19 = " = new TalendPipelineModelSerializer();" + NL + "com.esotericsoftware.kryo.io.Input pairingModelInput_";
  protected final String TEXT_20 = " = new com.esotericsoftware.kryo.io.Input(fs.open(new org.apache.hadoop.fs.Path(";
  protected final String TEXT_21 = " + \"/blocking/\")));" + NL + "TalendPipelineModel blockingModel_";
  protected final String TEXT_22 = " = kryo_";
  protected final String TEXT_23 = ".readObject(pairingModelInput_";
  protected final String TEXT_24 = ", TalendPipelineModel.class, talendPipelineModelSerializer_";
  protected final String TEXT_25 = ");" + NL + "org.apache.spark.ml.PipelineModel pairingPM_";
  protected final String TEXT_26 = " = blockingModel_";
  protected final String TEXT_27 = ".getPipelineModel();";
  protected final String TEXT_28 = NL + "    FileSystem.setDefaultUri(ctx.hadoopConfiguration(), currentURI_";
  protected final String TEXT_29 = "_config);" + NL + "    fs = FileSystem.get(ctx.hadoopConfiguration());";
  protected final String TEXT_30 = NL + NL + "org.talend.dataquality.reconciliation.indexing.api.MatchIndex.create(";
  protected final String TEXT_31 = NL + "        ";
  protected final String TEXT_32 = ",";
  protected final String TEXT_33 = NL + "        ";
  protected final String TEXT_34 = ",";
  protected final String TEXT_35 = NL + "        ";
  protected final String TEXT_36 = ",";
  protected final String TEXT_37 = NL + "        ";
  protected final String TEXT_38 = ",";
  protected final String TEXT_39 = NL + "        ";
  protected final String TEXT_40 = "," + NL + "        pairingPM_";
  protected final String TEXT_41 = ",";
  protected final String TEXT_42 = NL + "        ";
  protected final String TEXT_43 = ",";
  protected final String TEXT_44 = NL + "        ";
  protected final String TEXT_45 = ",";
  protected final String TEXT_46 = NL + "        ";
  protected final String TEXT_47 = NL + "        " + NL + ");";
  protected final String TEXT_48 = NL;

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    
// Parse the inputs to this javajet generator.
final BigDataCodeGeneratorArgument codeGenArgument = (BigDataCodeGeneratorArgument) argument;
final INode node = (INode) codeGenArgument.getArgument();
final IBigDataNode bigDataNode = (IBigDataNode) codeGenArgument.getArgument();
final String cid = node.getUniqueName();

    
//IO Connections
TModelEncoderUtil tModelEncoderUtil = new TModelEncoderUtil(node);
IConnection incomingConnection = tModelEncoderUtil.getIncomingConnection();
String incomingConnectionName = incomingConnection.getName();
String incomingStructName = codeGenArgument.getRecordStructName(incomingConnection);

//The name of the List containing all transformations
String stagesName = "stages_" + cid;
//The name of the input DataFrame
String dfInName = "dfIn_" + cid;

//Component params
/*********** Parameters ************/
String nodeES = ElementParameterParser.getValue(node,"__NODES__");
String[] splitNodeES = nodeES.split(":");
if (splitNodeES.length!=2){
    return "throw new IllegalArgumentException(\"" + splitNodeES +"\");";
}
String host = splitNodeES[0]+"\"";
String port = splitNodeES[1].substring(0,splitNodeES[1].length()-1);
String indexName = ElementParameterParser.getValue(node,"__INDEX__");
Boolean flagReset = ElementParameterParser.getBooleanValue(node, "__RESET_INDEX__");
String pairingModelFolder = ElementParameterParser.getValue(node,"__PAIRING_HDFS_FOLDER__");
boolean useConfigurationComponent = "true".equals(ElementParameterParser.getValue(node, "__DEFINE_STORAGE_CONFIGURATION__"));
String uriPrefix = "\"\"";
if(useConfigurationComponent) {
    uriPrefix = org.talend.designer.spark.generator.storage.SparkStorageUtils.getURIPrefix(node);
    pairingModelFolder = uriPrefix + " + " + pairingModelFolder;
}
String maxStorageThreshold = ElementParameterParser.getValue(node,"__MAX_STORAGE_THRESHOLD__");
String maxWaitSecPerBulk = ElementParameterParser.getValue(node,"__MAX_WAIT_SECONDS_PER_BULK__");
String maxBulkSize = ElementParameterParser.getValue(node,"__MAX_BULK_SIZE__");


String rddName, structName;
TSqlRowUtil tSqlRowUtil= new TSqlRowUtil(node);
if(tSqlRowUtil.containsDateFields(incomingConnection)) {
    // Additional map to convert from java.util.Date to java.sql.Timestamp
        String newRddName = "tmp_rdd_"+incomingConnectionName;
        String newStructName = "DF_"+incomingStructName+"AvroRecord";

    stringBuffer.append(TEXT_1);
    stringBuffer.append(newStructName);
    stringBuffer.append(TEXT_2);
    stringBuffer.append(newRddName);
    stringBuffer.append(TEXT_3);
    stringBuffer.append(incomingConnectionName);
    stringBuffer.append(TEXT_4);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_5);
    stringBuffer.append(incomingStructName);
    stringBuffer.append(TEXT_6);
    stringBuffer.append(newStructName);
    stringBuffer.append(TEXT_7);
    
        rddName = newRddName;
        structName = newStructName;
} else {
        // No need for additional map
        rddName = "rdd_"+incomingConnectionName;
        structName = incomingStructName;
}

    stringBuffer.append(TEXT_8);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_9);
    stringBuffer.append(dfInName);
    stringBuffer.append(TEXT_10);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_11);
    stringBuffer.append(rddName);
    stringBuffer.append(TEXT_12);
    stringBuffer.append(structName);
    stringBuffer.append(TEXT_13);
    
if(!"\"\"".equals(uriPrefix)) {

    stringBuffer.append(TEXT_14);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_15);
    stringBuffer.append(uriPrefix);
    stringBuffer.append(TEXT_16);
    
}

    stringBuffer.append(TEXT_17);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_18);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_19);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_20);
    stringBuffer.append(pairingModelFolder);
    stringBuffer.append(TEXT_21);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_22);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_23);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_24);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_25);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_26);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_27);
    
if(!"\"\"".equals(uriPrefix)) {
    
    stringBuffer.append(TEXT_28);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_29);
    
}

    stringBuffer.append(TEXT_30);
    stringBuffer.append(TEXT_31);
    stringBuffer.append(dfInName);
    stringBuffer.append(TEXT_32);
    stringBuffer.append(TEXT_33);
    stringBuffer.append(host);
    stringBuffer.append(TEXT_34);
    stringBuffer.append(TEXT_35);
    stringBuffer.append(port);
    stringBuffer.append(TEXT_36);
    stringBuffer.append(TEXT_37);
    stringBuffer.append(indexName);
    stringBuffer.append(TEXT_38);
    stringBuffer.append(TEXT_39);
    stringBuffer.append(maxStorageThreshold);
    stringBuffer.append(TEXT_40);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_41);
    stringBuffer.append(TEXT_42);
    stringBuffer.append(maxWaitSecPerBulk);
    stringBuffer.append(TEXT_43);
    stringBuffer.append(TEXT_44);
    stringBuffer.append(maxBulkSize);
    stringBuffer.append(TEXT_45);
    stringBuffer.append(TEXT_46);
    stringBuffer.append(flagReset);
    stringBuffer.append(TEXT_47);
    stringBuffer.append(TEXT_48);
    return stringBuffer.toString();
  }
}
