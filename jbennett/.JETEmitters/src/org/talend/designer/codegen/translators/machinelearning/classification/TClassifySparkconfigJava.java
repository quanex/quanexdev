package org.talend.designer.codegen.translators.machinelearning.classification;

import java.util.List;
import org.talend.core.model.metadata.IMetadataColumn;
import org.talend.core.model.metadata.IMetadataTable;
import org.talend.core.model.metadata.types.JavaType;
import org.talend.core.model.metadata.types.JavaTypesManager;
import org.talend.core.model.process.ElementParameterParser;
import org.talend.core.model.process.IBigDataNode;
import org.talend.core.model.process.IConnection;
import org.talend.core.model.process.IConnectionCategory;
import org.talend.core.model.process.INode;
import org.talend.core.model.utils.NodeUtil;
import org.talend.designer.common.BigDataCodeGeneratorArgument;

public class TClassifySparkconfigJava
{
  protected static String nl;
  public static synchronized TClassifySparkconfigJava create(String lineSeparator)
  {
    nl = lineSeparator;
    TClassifySparkconfigJava result = new TClassifySparkconfigJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "org.apache.spark.api.java.JavaRDD<";
  protected final String TEXT_2 = "> rdd_";
  protected final String TEXT_3 = ";" + NL + "{";
  protected final String TEXT_4 = NL + "            java.net.URI currentURI_";
  protected final String TEXT_5 = "_config = FileSystem.getDefaultUri(ctx.hadoopConfiguration());" + NL + "            FileSystem.setDefaultUri(ctx.hadoopConfiguration(), new java.net.URI(";
  protected final String TEXT_6 = "));" + NL + "            fs = FileSystem.get(ctx.hadoopConfiguration());";
  protected final String TEXT_7 = NL + "        com.esotericsoftware.kryo.Kryo kryo_";
  protected final String TEXT_8 = " = new com.esotericsoftware.kryo.Kryo();" + NL + "        com.esotericsoftware.kryo.io.Input featuresInput_";
  protected final String TEXT_9 = " = new com.esotericsoftware.kryo.io.Input(fs.open(new org.apache.hadoop.fs.Path(";
  protected final String TEXT_10 = " + \"/model/\")));" + NL + "        TalendPipelineModelSerializer talendPipelineModelSerializer_";
  protected final String TEXT_11 = " = new TalendPipelineModelSerializer();" + NL + "        TalendPipelineModel featuresTalendPipelineModel_";
  protected final String TEXT_12 = " = kryo_";
  protected final String TEXT_13 = ".readObject(featuresInput_";
  protected final String TEXT_14 = ", TalendPipelineModel.class, talendPipelineModelSerializer_";
  protected final String TEXT_15 = ");" + NL + "" + NL + "        java.util.Map<String, String> featuresParamsMap_";
  protected final String TEXT_16 = " = featuresTalendPipelineModel_";
  protected final String TEXT_17 = ".getParams();" + NL + "" + NL + "        org.apache.spark.ml.PipelineModel pipelineModel_";
  protected final String TEXT_18 = " = featuresTalendPipelineModel_";
  protected final String TEXT_19 = ".getPipelineModel();";
  protected final String TEXT_20 = NL + "        org.apache.spark.ml.PipelineModel pipelineModel_";
  protected final String TEXT_21 = ";" + NL + "        {" + NL + "            Object model = globalMap.get(\"";
  protected final String TEXT_22 = "_MODEL\");" + NL + "            if (model instanceof org.apache.spark.ml.PipelineModel) {" + NL + "                pipelineModel_";
  protected final String TEXT_23 = " = (org.apache.spark.ml.PipelineModel) model;" + NL + "            } else {" + NL + "                throw new Exception(\"Unsupported model type: \" + model.getClass());" + NL + "            }" + NL + "        }";
  protected final String TEXT_24 = NL + NL + "    // Convert incoming RDD to DataFrame" + NL + "    org.apache.spark.sql.SQLContext sqlContext_";
  protected final String TEXT_25 = " = new org.apache.spark.sql.SQLContext(ctx);";
  protected final String TEXT_26 = NL + "    ";
  protected final String TEXT_27 = " df_";
  protected final String TEXT_28 = " = sqlContext_";
  protected final String TEXT_29 = ".createDataFrame(rdd_";
  protected final String TEXT_30 = ", ";
  protected final String TEXT_31 = ".class);";
  protected final String TEXT_32 = NL + "    ";
  protected final String TEXT_33 = " results = pipelineModel_";
  protected final String TEXT_34 = ".transform(df_";
  protected final String TEXT_35 = ");" + NL;
  protected final String TEXT_36 = NL + "        org.apache.spark.ml.feature.StringIndexerModel sim = (org.apache.spark.ml.feature.StringIndexerModel) pipelineModel_";
  protected final String TEXT_37 = ".stages()[1];" + NL;
  protected final String TEXT_38 = NL + "            org.apache.spark.rdd.RDD<";
  protected final String TEXT_39 = "> rdd = results.map(" + NL + "                    new StringIndexerInverseFunction_";
  protected final String TEXT_40 = "(sim)," + NL + "                    scala.reflect.ClassManifestFactory.fromClass(";
  protected final String TEXT_41 = ".class));" + NL + "            rdd_";
  protected final String TEXT_42 = " = org.apache.spark.api.java.JavaRDD.fromRDD(rdd, rdd.org$apache$spark$rdd$RDD$$evidence$1);";
  protected final String TEXT_43 = NL + "            rdd_";
  protected final String TEXT_44 = " = results.map(new StringIndexerInverseFunction_";
  protected final String TEXT_45 = "(sim)," + NL + "                    org.apache.spark.sql.Encoders.bean(";
  protected final String TEXT_46 = ".class)).toJavaRDD();";
  protected final String TEXT_47 = NL + "        // TODO";
  protected final String TEXT_48 = NL + "}" + NL;
  protected final String TEXT_49 = NL;

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    
//Parse the inputs to this javajet generator.
final BigDataCodeGeneratorArgument codeGenArgument = (BigDataCodeGeneratorArgument) argument;
final INode node = (INode)codeGenArgument.getArgument();
final String cid = node.getUniqueName();
final boolean isLog4jEnabled = ("true").equals(ElementParameterParser.getValue(node.getProcess(), "__LOG4J_ACTIVATE__"));
boolean useConfigurationComponent = "true".equals(ElementParameterParser.getValue(node, "__DEFINE_STORAGE_CONFIGURATION__"));

final String dataframeClass = org.talend.hadoop.distribution.ESparkVersion.SPARK_2_0.compareTo(codeGenArgument.getSparkVersion()) > 0
    ? "org.apache.spark.sql.DataFrame"
    : "org.apache.spark.sql.Dataset<org.apache.spark.sql.Row>";

    
List<IMetadataTable> metadatas = node.getMetadataList();
if ((metadatas == null) || (metadatas.size() == 0))
    return "" ;
IMetadataTable metadata = metadatas.get(0);
if (metadata == null)
    return "";
List<IMetadataColumn> columns = metadata.getListColumns();
if (columns == null)
    return "";
List<? extends IConnection> conns = node.getIncomingConnections();
if (conns == null || conns.size() == 0 || !conns.get(0).getLineStyle().hasConnectionCategory(IConnectionCategory.DATA))
    return "";
IConnection inConn = conns.get(0);
conns = node.getOutgoingConnections();
if (conns == null || conns.size() == 0 || !conns.get(0).getLineStyle().hasConnectionCategory(IConnectionCategory.DATA))
    return "";
IConnection outConn = conns.get(0);

String inRowStruct = codeGenArgument.getRecordStructName(inConn);
String inConnName = inConn.getName();

String outRowStruct = codeGenArgument.getRecordStructName(outConn);
String outConnName = outConn.getName();


// This is set to true if the output label is not double format.
// TODO: check if the labelColumn is double and provide a unique labelColumn.
boolean needsLabelIndexer = true;
String labelColumn = "label";

Boolean savedOnDisk = ElementParameterParser.getBooleanValue(node, "__SAVE_ON_DISK__");
String hdfsFolder = ElementParameterParser.getValue(node,"__HDFS_FOLDER__");
String uriPrefix = "\"\"";
if(useConfigurationComponent) {
    uriPrefix = org.talend.designer.spark.generator.storage.SparkStorageUtils.getURIPrefix(node);
    hdfsFolder = uriPrefix + " + " + hdfsFolder;
}

    stringBuffer.append(TEXT_1);
    stringBuffer.append(outRowStruct);
    stringBuffer.append(TEXT_2);
    stringBuffer.append(outConnName);
    stringBuffer.append(TEXT_3);
    
    // Get the pipeline model.
    if (savedOnDisk) {
        if(!"\"\"".equals(uriPrefix)) {
            
    stringBuffer.append(TEXT_4);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_5);
    stringBuffer.append(uriPrefix);
    stringBuffer.append(TEXT_6);
    
        }
        
    stringBuffer.append(TEXT_7);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_8);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_9);
    stringBuffer.append(hdfsFolder);
    stringBuffer.append(TEXT_10);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_11);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_12);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_13);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_14);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_15);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_16);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_17);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_18);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_19);
    
    } else {
        String modelCid = ElementParameterParser.getValue(node, "__MODEL_LOCATION__");
        
    stringBuffer.append(TEXT_20);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_21);
    stringBuffer.append(modelCid);
    stringBuffer.append(TEXT_22);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_23);
    
    }
    
    stringBuffer.append(TEXT_24);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_25);
    stringBuffer.append(TEXT_26);
    stringBuffer.append(dataframeClass);
    stringBuffer.append(TEXT_27);
    stringBuffer.append(inConnName);
    stringBuffer.append(TEXT_28);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_29);
    stringBuffer.append(inConnName);
    stringBuffer.append(TEXT_30);
    stringBuffer.append(inRowStruct);
    stringBuffer.append(TEXT_31);
    stringBuffer.append(TEXT_32);
    stringBuffer.append(dataframeClass);
    stringBuffer.append(TEXT_33);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_34);
    stringBuffer.append(inConnName);
    stringBuffer.append(TEXT_35);
    

    if (needsLabelIndexer) {
        
    stringBuffer.append(TEXT_36);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_37);
    
        if (org.talend.hadoop.distribution.ESparkVersion.SPARK_2_0.compareTo(codeGenArgument.getSparkVersion()) > 0) {
            
    stringBuffer.append(TEXT_38);
    stringBuffer.append(outRowStruct);
    stringBuffer.append(TEXT_39);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_40);
    stringBuffer.append(outRowStruct);
    stringBuffer.append(TEXT_41);
    stringBuffer.append(outConnName);
    stringBuffer.append(TEXT_42);
    
        } else {
            
    stringBuffer.append(TEXT_43);
    stringBuffer.append(outConnName);
    stringBuffer.append(TEXT_44);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_45);
    stringBuffer.append(outRowStruct);
    stringBuffer.append(TEXT_46);
    
        }
    } else {
        
    stringBuffer.append(TEXT_47);
    
    }
    
    stringBuffer.append(TEXT_48);
    stringBuffer.append(TEXT_49);
    return stringBuffer.toString();
  }
}
