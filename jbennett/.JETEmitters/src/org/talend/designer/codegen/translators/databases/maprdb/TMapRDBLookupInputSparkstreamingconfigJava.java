package org.talend.designer.codegen.translators.databases.maprdb;

public class TMapRDBLookupInputSparkstreamingconfigJava
{
  protected static String nl;
  public static synchronized TMapRDBLookupInputSparkstreamingconfigJava create(String lineSeparator)
  {
    nl = lineSeparator;
    TMapRDBLookupInputSparkstreamingconfigJava result = new TMapRDBLookupInputSparkstreamingconfigJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    return stringBuffer.toString();
  }
}
