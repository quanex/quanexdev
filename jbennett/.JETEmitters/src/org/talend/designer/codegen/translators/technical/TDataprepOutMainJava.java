package org.talend.designer.codegen.translators.technical;

import org.talend.core.model.process.INode;
import org.talend.designer.codegen.config.CodeGeneratorArgument;
import org.talend.core.model.process.ElementParameterParser;
import org.talend.core.model.metadata.IMetadataTable;
import org.talend.core.model.metadata.IMetadataColumn;
import org.talend.core.model.process.IConnection;
import org.talend.core.model.process.IConnectionCategory;
import java.util.List;

public class TDataprepOutMainJava
{
  protected static String nl;
  public static synchronized TDataprepOutMainJava create(String lineSeparator)
  {
    nl = lineSeparator;
    TDataprepOutMainJava result = new TDataprepOutMainJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "";
  protected final String TEXT_2 = NL;
  protected final String TEXT_3 = NL + "    recordMap_";
  protected final String TEXT_4 = ".put(" + NL + "        \"";
  protected final String TEXT_5 = "\"," + NL + "    \troutines.system.FormatterUtils.format(";
  protected final String TEXT_6 = ".";
  protected final String TEXT_7 = ",";
  protected final String TEXT_8 = ")" + NL + "    );";
  protected final String TEXT_9 = NL + NL + "incommingRecordsWriter_";
  protected final String TEXT_10 = ".write(objectMapper_";
  protected final String TEXT_11 = ".writeValueAsString(recordMap_";
  protected final String TEXT_12 = "));" + NL + "incommingRecordsWriter_";
  protected final String TEXT_13 = ".write(System.lineSeparator());" + NL + "recordMap_";
  protected final String TEXT_14 = ".clear();";

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    stringBuffer.append(TEXT_1);
    
CodeGeneratorArgument codeGenArgument = (CodeGeneratorArgument) argument;
INode node = (INode)codeGenArgument.getArgument();
String cid = node.getUniqueName();
List<? extends IConnection> incomingConnections = node.getIncomingConnections();

if((incomingConnections==null)&&(incomingConnections.isEmpty())) {
	return stringBuffer.toString();
}

IConnection inputConn = null;
for(IConnection incomingConnection : incomingConnections) {
	if(incomingConnection.getLineStyle().hasConnectionCategory(IConnectionCategory.DATA)) {
		inputConn = incomingConnection;
	}
}

if(inputConn==null) {
	return stringBuffer.toString();
}

List<IMetadataTable> metadatas = node.getMetadataList();
if ((metadatas==null)||(metadatas.isEmpty())) {
	return stringBuffer.toString();
}

IMetadataTable metadataOfNode = metadatas.get(0);

if(metadataOfNode == null) {
	return stringBuffer.toString();
}

IMetadataTable metadataOfInput = inputConn.getMetadataTable();

if(metadataOfInput == null) {
	return stringBuffer.toString();
}

    stringBuffer.append(TEXT_2);
    
int index = 0;
for(IMetadataColumn column : metadataOfInput.getListColumns()){
	String pattern = column.getPattern() == null || column.getPattern().trim().length() == 0 ? null : column.getPattern();

    stringBuffer.append(TEXT_3);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_4);
    stringBuffer.append(String.format("%04d", index++));
    stringBuffer.append(TEXT_5);
    stringBuffer.append(inputConn.getName() );
    stringBuffer.append(TEXT_6);
    stringBuffer.append(column.getLabel() );
    stringBuffer.append(TEXT_7);
    stringBuffer.append(pattern);
    stringBuffer.append(TEXT_8);
     } 
    stringBuffer.append(TEXT_9);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_10);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_11);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_12);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_13);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_14);
    return stringBuffer.toString();
  }
}
