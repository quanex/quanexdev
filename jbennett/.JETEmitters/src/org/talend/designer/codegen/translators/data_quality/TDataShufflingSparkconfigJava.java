package org.talend.designer.codegen.translators.data_quality;

import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Iterator;
import org.talend.core.model.metadata.IMetadataColumn;
import org.talend.core.model.metadata.IMetadataTable;
import org.talend.core.model.metadata.types.JavaType;
import org.talend.core.model.metadata.types.JavaTypesManager;
import org.talend.core.model.process.ElementParameterParser;
import org.talend.core.model.process.IBigDataNode;
import org.talend.core.model.process.IConnection;
import org.talend.core.model.process.IConnectionCategory;
import org.talend.core.model.process.INode;
import org.talend.core.model.utils.NodeUtil;
import org.talend.designer.common.BigDataCodeGeneratorArgument;
import org.talend.designer.common.tmodelencoder.TModelEncoderUtil;
import org.talend.designer.common.tsqlrow.TSqlRowUtil;

public class TDataShufflingSparkconfigJava
{
  protected static String nl;
  public static synchronized TDataShufflingSparkconfigJava create(String lineSeparator)
  {
    nl = lineSeparator;
    TDataShufflingSparkconfigJava result = new TDataShufflingSparkconfigJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "    ";
  protected final String TEXT_2 = NL + "        // SparkSQL will use the default TimeZone to handle dates (see org.apache.spark.sql.catalyst.util.DateTimeUtils)." + NL + "        // To avoid inconsistencies with others components in the Studio, ensure that UTC is set as the default TimeZone." + NL + "        java.util.TimeZone.setDefault(java.util.TimeZone.getTimeZone(\"UTC\"));" + NL + "        " + NL + "        // java.util.Date -> java.sql.Date or java.sql.Timestamp conversions to be compliant with Spark SQL before creating our DataFrame" + NL + "        org.apache.spark.api.java.JavaRDD<";
  protected final String TEXT_3 = "> ";
  protected final String TEXT_4 = " = rdd_";
  protected final String TEXT_5 = ".map(new ";
  protected final String TEXT_6 = "_From";
  protected final String TEXT_7 = "To";
  protected final String TEXT_8 = "());";
  protected final String TEXT_9 = NL + NL + "    //convert the incoming RDD to DataFrame" + NL + "\torg.apache.spark.sql.SQLContext sqlContext_";
  protected final String TEXT_10 = " = new org.apache.spark.sql.SQLContext(ctx);" + NL + "\t" + NL + "\t";
  protected final String TEXT_11 = " df_";
  protected final String TEXT_12 = "_";
  protected final String TEXT_13 = " = sqlContext_";
  protected final String TEXT_14 = ".createDataFrame(";
  protected final String TEXT_15 = ", ";
  protected final String TEXT_16 = ".class);" + NL + "\t" + NL + "\tList<List<String>> shufflingColumns_";
  protected final String TEXT_17 = " = new java.util.ArrayList<List<String>>();" + NL + "\t";
  protected final String TEXT_18 = NL + "\t\t" + NL + "\t\tshufflingColumns_";
  protected final String TEXT_19 = ".get(";
  protected final String TEXT_20 = ").add(\"";
  protected final String TEXT_21 = "\");" + NL + "\t";
  protected final String TEXT_22 = NL + "\t\tList<String> newList_";
  protected final String TEXT_23 = " = new java.util.ArrayList<String>();" + NL + "\t\tnewList_";
  protected final String TEXT_24 = ".add(\"";
  protected final String TEXT_25 = "\");" + NL + "\t\tshufflingColumns_";
  protected final String TEXT_26 = ".add(newList_";
  protected final String TEXT_27 = ");" + NL + "\t\t\t\t\t\t" + NL + "\t";
  protected final String TEXT_28 = NL + "\t\t" + NL + "\t\t// start the partition columns" + NL + "\t\tList<String> partitionColumns_";
  protected final String TEXT_29 = " = new java.util.ArrayList<String>();" + NL + "\t";
  protected final String TEXT_30 = NL + "\t\tpartitionColumns_";
  protected final String TEXT_31 = " = null;" + NL + "\t";
  protected final String TEXT_32 = NL + "\t\tpartitionColumns_";
  protected final String TEXT_33 = ".add(\"";
  protected final String TEXT_34 = "\");" + NL + "\t";
  protected final String TEXT_35 = NL + "\torg.talend.dataquality.datamasking.shuffling.ShuffleColumns shuffleColumn_";
  protected final String TEXT_36 = " =  org.talend.dataquality.datamasking.shuffling.ShuffleColumns.apply(shufflingColumns_";
  protected final String TEXT_37 = ", partitionColumns_";
  protected final String TEXT_38 = ");" + NL + "\t";
  protected final String TEXT_39 = NL + "    shuffleColumn_";
  protected final String TEXT_40 = ".setSeed(";
  protected final String TEXT_41 = ");";
  protected final String TEXT_42 = NL + "\t" + NL + "\t" + NL + "\t";
  protected final String TEXT_43 = " rdf_";
  protected final String TEXT_44 = "_";
  protected final String TEXT_45 = " =shuffleColumn_";
  protected final String TEXT_46 = ".shuffle(df_";
  protected final String TEXT_47 = "_";
  protected final String TEXT_48 = ");" + NL + "\t// Convert outgoing DataFrame to RDD" + NL + "    org.apache.spark.api.java.JavaRDD<";
  protected final String TEXT_49 = "> rdd_";
  protected final String TEXT_50 = " = " + NL + "            rdf_";
  protected final String TEXT_51 = "_";
  protected final String TEXT_52 = ".toJavaRDD().map(new ";
  protected final String TEXT_53 = "_FromRowTo";
  protected final String TEXT_54 = "());" + NL + "\t";
  protected final String TEXT_55 = NL;

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    stringBuffer.append(TEXT_1);
    
// Parse the inputs to this javajet generator.
final BigDataCodeGeneratorArgument codeGenArgument = (BigDataCodeGeneratorArgument) argument;
final INode node = (INode) codeGenArgument.getArgument();
final IBigDataNode bigDataNode = (IBigDataNode) codeGenArgument.getArgument();
final String cid = node.getUniqueName();
final String dataframeClass = org.talend.hadoop.distribution.ESparkVersion.SPARK_2_0.compareTo(codeGenArgument.getSparkVersion()) > 0
    ? "org.apache.spark.sql.DataFrame"
    : "org.apache.spark.sql.Dataset<org.apache.spark.sql.Row>";

//IO Connections
TModelEncoderUtil tModelEncoderUtil = new TModelEncoderUtil(node);
IConnection incomingConnection = tModelEncoderUtil.getIncomingConnection();
String incomingConnectionName = incomingConnection.getName();
String incomingStructName = codeGenArgument.getRecordStructName(incomingConnection);
String outStructName = codeGenArgument.getRecordStructName(tModelEncoderUtil.getOutgoingConnection());

//Component params
List<Map<String, String>> shufflingTable = (List<Map<String,String>>)ElementParameterParser.getObjectValue(node, "__SHUFFLING_COLUMNS__");
List<Map<String, String>> partitionTable = (List<Map<String,String>>)ElementParameterParser.getObjectValue(node, "__PARTITIONING_COLUMNS__");
String randomSeedString = ElementParameterParser.getValue(node, "__RANDOM_SEED__");
TSqlRowUtil tSqlRowUtil= new TSqlRowUtil(node);
String rddName, structName;
if(tSqlRowUtil.containsDateFields(incomingConnection)) {
        // Additional map to convert from java.util.Date to java.sql.Date or java.sql.Timestamp
        String newRddName = "tmp_rdd_"+incomingConnectionName;
        String newStructName = "DF_"+incomingStructName+"AvroRecord";
    
    stringBuffer.append(TEXT_2);
    stringBuffer.append(newStructName);
    stringBuffer.append(TEXT_3);
    stringBuffer.append(newRddName);
    stringBuffer.append(TEXT_4);
    stringBuffer.append(incomingConnectionName);
    stringBuffer.append(TEXT_5);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_6);
    stringBuffer.append(incomingStructName);
    stringBuffer.append(TEXT_7);
    stringBuffer.append(newStructName);
    stringBuffer.append(TEXT_8);
    
        rddName = newRddName;
        structName = newStructName;
    } else {
        // No need for additional map
        rddName = "rdd_"+incomingConnectionName;
        structName = incomingStructName;
    }
    
    stringBuffer.append(TEXT_9);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_10);
    stringBuffer.append(dataframeClass);
    stringBuffer.append(TEXT_11);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_12);
    stringBuffer.append(incomingConnectionName);
    stringBuffer.append(TEXT_13);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_14);
    stringBuffer.append(rddName);
    stringBuffer.append(TEXT_15);
    stringBuffer.append(structName);
    stringBuffer.append(TEXT_16);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_17);
    
		List<Integer> groupids = new ArrayList<Integer>();
		int i = 0;
		for (Iterator<Map<String, String>> iterator = shufflingTable.iterator(); iterator.hasNext();) {//start for iterator
			i++;
			Map<String, String> map = iterator.next();
			if (!map.get("GROUP_ID").toString().isEmpty()) { // start if 1
				int groupid = Integer.parseInt((String) map.get("GROUP_ID"));
				if(groupid != 0){//start if id
					if (groupids.contains(groupid)) { //start if 2
	
    stringBuffer.append(TEXT_18);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_19);
    stringBuffer.append(groupids.indexOf(groupid));
    stringBuffer.append(TEXT_20);
    stringBuffer.append(map.get("COLUMN"));
    stringBuffer.append(TEXT_21);
    
					}// end if 2
					else{ //start else 1
						groupids.add(groupid);
						String item = map.get("COLUMN").toString();
	
    stringBuffer.append(TEXT_22);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_23);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_24);
    stringBuffer.append(item);
    stringBuffer.append(TEXT_25);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_26);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_27);
    
					}//end else
				}//end if id 
			}//end if 1
		}// end for iterator
	
    stringBuffer.append(TEXT_28);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_29);
    
		if (partitionTable == null || partitionTable.isEmpty()) { // start if
		
	
    stringBuffer.append(TEXT_30);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_31);
    
		}// end if
		else{//start else
			for (Iterator<Map<String, String>> iterator = partitionTable.iterator(); iterator.hasNext();) {// start iterator
				Map<String, String> map = iterator.next();
	
    stringBuffer.append(TEXT_32);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_33);
    stringBuffer.append(map.get("PARTITIONING_COLUMN"));
    stringBuffer.append(TEXT_34);
    
			}// end iterator
		}//end else
	
    stringBuffer.append(TEXT_35);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_36);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_37);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_38);
    
         if(randomSeedString.length() > 0 && !"\"\"".equals(randomSeedString)) {
    
    stringBuffer.append(TEXT_39);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_40);
    stringBuffer.append(randomSeedString);
    stringBuffer.append(TEXT_41);
    
         }
    
    stringBuffer.append(TEXT_42);
    stringBuffer.append(dataframeClass);
    stringBuffer.append(TEXT_43);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_44);
    stringBuffer.append(incomingConnectionName);
    stringBuffer.append(TEXT_45);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_46);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_47);
    stringBuffer.append(incomingConnectionName);
    stringBuffer.append(TEXT_48);
    stringBuffer.append(outStructName);
    stringBuffer.append(TEXT_49);
    stringBuffer.append(tModelEncoderUtil.getOutgoingConnection().getName());
    stringBuffer.append(TEXT_50);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_51);
    stringBuffer.append(incomingConnectionName);
    stringBuffer.append(TEXT_52);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_53);
    stringBuffer.append(outStructName);
    stringBuffer.append(TEXT_54);
    stringBuffer.append(TEXT_55);
    return stringBuffer.toString();
  }
}
