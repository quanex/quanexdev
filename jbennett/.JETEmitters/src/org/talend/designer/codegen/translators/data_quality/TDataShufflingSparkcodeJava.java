package org.talend.designer.codegen.translators.data_quality;

import org.talend.core.model.process.INode;
import org.talend.core.model.process.IConnection;
import org.talend.core.model.metadata.IMetadataColumn;
import org.talend.core.model.process.ElementParameterParser;
import org.talend.designer.common.BigDataCodeGeneratorArgument;
import org.talend.designer.common.tsqlrow.TSqlRowUtil;
import org.talend.designer.common.tmodelencoder.TModelEncoderUtil;

public class TDataShufflingSparkcodeJava
{
  protected static String nl;
  public static synchronized TDataShufflingSparkcodeJava create(String lineSeparator)
  {
    nl = lineSeparator;
    TDataShufflingSparkcodeJava result = new TDataShufflingSparkcodeJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "";
  protected final String TEXT_2 = " " + NL + "    \tlog.warn(\"Component does not have Date/Timestamp checkbox, will use Timestamp as default\");";
  protected final String TEXT_3 = " " + NL + "    \tSystem.err.println(\"Component does not have Date/Timestamp checkbox, will use Timestamp as default\");";
  protected final String TEXT_4 = NL + "public static class ";
  protected final String TEXT_5 = "_FromRowTo";
  protected final String TEXT_6 = " implements org.apache.spark.api.java.function.Function<org.apache.spark.sql.Row, ";
  protected final String TEXT_7 = "> {" + NL + "" + NL + "    public ";
  protected final String TEXT_8 = " call(org.apache.spark.sql.Row row) {";
  protected final String TEXT_9 = NL + "        ";
  protected final String TEXT_10 = " result = new ";
  protected final String TEXT_11 = "();" + NL + "        org.apache.spark.sql.types.StructField[] structFields = row.schema().fields();" + NL + "        for (int i = 0; i < structFields.length; i++) {" + NL + "            org.apache.avro.Schema.Field avroField = ";
  protected final String TEXT_12 = ".getClassSchema().getField(structFields[i].name());" + NL + "            if (avroField != null){" + NL + "                result.put(avroField.pos(), row.get(i));" + NL + "            }" + NL + "        }" + NL + "        return result;" + NL + "    }" + NL + "}";
  protected final String TEXT_13 = NL + NL + "\t\tpublic static class ";
  protected final String TEXT_14 = "_From";
  protected final String TEXT_15 = "To";
  protected final String TEXT_16 = " implements org.apache.spark.api.java.function.Function<";
  protected final String TEXT_17 = ", ";
  protected final String TEXT_18 = "> {" + NL + "" + NL + "\t\t\tpublic ";
  protected final String TEXT_19 = " call(";
  protected final String TEXT_20 = " input) {" + NL + "\t\t\t\t";
  protected final String TEXT_21 = " result = new ";
  protected final String TEXT_22 = "();";
  protected final String TEXT_23 = NL + "\t\t\t\t\t\tif(input.";
  protected final String TEXT_24 = " != null) {" + NL + "\t\t\t\t\t\t\tresult.";
  protected final String TEXT_25 = " = new java.sql.";
  protected final String TEXT_26 = "(input.";
  protected final String TEXT_27 = ".getTime());" + NL + "\t\t\t\t\t\t} else {" + NL + "\t\t\t\t\t\t\tresult.";
  protected final String TEXT_28 = " = null;" + NL + "\t\t\t\t\t\t}";
  protected final String TEXT_29 = NL + "\t\t\t\t\tresult.";
  protected final String TEXT_30 = " = input.";
  protected final String TEXT_31 = ";";
  protected final String TEXT_32 = NL + "\t\t\t\treturn result;" + NL + "\t\t\t}" + NL + "\t\t}";
  protected final String TEXT_33 = NL;

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    stringBuffer.append(TEXT_1);
    
// Parse the inputs to this javajet generator.
final BigDataCodeGeneratorArgument codeGenArgument = (BigDataCodeGeneratorArgument)argument;
final INode node = (INode)codeGenArgument.getArgument();
final String cid = node.getUniqueName();

final String dataframeClass = org.talend.hadoop.distribution.ESparkVersion.SPARK_2_0.compareTo(codeGenArgument.getSparkVersion()) > 0
    ? "org.apache.spark.sql.DataFrame"
    : "org.apache.spark.sql.Dataset<org.apache.spark.sql.Row>";

    
TSqlRowUtil tSqlRowUtil = new TSqlRowUtil(node);
String validateError = tSqlRowUtil.validate(true, true);
if (validateError != null) {
    // Cause the job compilation to explicitly fail if there is a problem.
    return "throw new JobConfigurationError(\"" + validateError +"\");";
}

boolean isLog4jEnabled = ("true").equals(ElementParameterParser.getValue(node.getProcess(), "__LOG4J_ACTIVATE__"));

boolean useTimestampForDatesInDataframes = true;
try{
    useTimestampForDatesInDataframes = ElementParameterParser.getBooleanValue(node, "__DATE_TO_TIMESTAMP_DF_TYPE_SUBSTITUTION__");
} catch(Exception e){
    if (isLog4jEnabled) { 
    
    stringBuffer.append(TEXT_2);
     
    } else { 
    
    stringBuffer.append(TEXT_3);
     
    } 
}

String outStructName = codeGenArgument.getRecordStructName(tSqlRowUtil.getOutgoingConnection());

    stringBuffer.append(TEXT_4);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_5);
    stringBuffer.append(outStructName);
    stringBuffer.append(TEXT_6);
    stringBuffer.append(outStructName);
    stringBuffer.append(TEXT_7);
    stringBuffer.append(outStructName);
    stringBuffer.append(TEXT_8);
    stringBuffer.append(TEXT_9);
    stringBuffer.append(outStructName);
    stringBuffer.append(TEXT_10);
    stringBuffer.append(outStructName);
    stringBuffer.append(TEXT_11);
    stringBuffer.append(outStructName);
    stringBuffer.append(TEXT_12);
    
// If the incoming rowStruct contains a Date field (always typed as java.util.Date),
// we must generate a new structure which replaces these java.util.Date instances by
// java.sql.Date or java.sql.Timestamp instances.

org.talend.designer.bigdata.avro.AvroRecordStructGenerator avroRecordStructGenerator = (org.talend.designer.bigdata.avro.AvroRecordStructGenerator) codeGenArgument.getRecordStructGenerator();

// Some of the incoming connections might share the same schema (and then the same rowXStruct). We must generate the below code only once by schema (if necessary).
java.util.Set<String> knownStructNames = new java.util.HashSet();

for(IConnection incomingConnection : tSqlRowUtil.getIncomingConnections()) {
	String originalStructName = codeGenArgument.getRecordStructName(incomingConnection);
	if(tSqlRowUtil.containsDateFields(incomingConnection) && !knownStructNames.contains(originalStructName)) {
		java.util.List<IMetadataColumn> columns = tSqlRowUtil.getColumns(incomingConnection);
		String suggestedDfStructName = "DF_"+originalStructName;
		String dfStructName = avroRecordStructGenerator.generateRecordStructForDataFrame(suggestedDfStructName, originalStructName, useTimestampForDatesInDataframes);
		knownStructNames.add(originalStructName);

    stringBuffer.append(TEXT_13);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_14);
    stringBuffer.append(originalStructName);
    stringBuffer.append(TEXT_15);
    stringBuffer.append(dfStructName);
    stringBuffer.append(TEXT_16);
    stringBuffer.append(originalStructName);
    stringBuffer.append(TEXT_17);
    stringBuffer.append(dfStructName);
    stringBuffer.append(TEXT_18);
    stringBuffer.append(dfStructName);
    stringBuffer.append(TEXT_19);
    stringBuffer.append(originalStructName);
    stringBuffer.append(TEXT_20);
    stringBuffer.append(dfStructName);
    stringBuffer.append(TEXT_21);
    stringBuffer.append(dfStructName);
    stringBuffer.append(TEXT_22);
    
				for(IMetadataColumn column : columns) {
					if(tSqlRowUtil.isDateField(column)) {

    stringBuffer.append(TEXT_23);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_24);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_25);
    stringBuffer.append(useTimestampForDatesInDataframes ? "Timestamp" : "Date");
    stringBuffer.append(TEXT_26);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_27);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_28);
    
				} else {

    stringBuffer.append(TEXT_29);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_30);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_31);
    
				}
			} // end for(IMetadataColumn column : columns)

    stringBuffer.append(TEXT_32);
    
	} // end if(tSqlRowUtil.containsDateFields(incomingConnection) && !knownStructNames.contains(originalStructName))
} // end for(IConnection incomingConnection : tSqlRowUtil.getIncomingConnections())

    stringBuffer.append(TEXT_33);
    return stringBuffer.toString();
  }
}
