package org.talend.designer.codegen.translators.technical;

import org.talend.core.model.process.INode;
import org.talend.designer.codegen.config.CodeGeneratorArgument;
import org.talend.core.model.process.ElementParameterParser;
import org.talend.core.model.process.IConnection;
import org.talend.core.model.process.IConnectionCategory;
import java.util.List;

public class TDataprepInEndJava
{
  protected static String nl;
  public static synchronized TDataprepInEndJava create(String lineSeparator)
  {
    nl = lineSeparator;
    TDataprepInEndJava result = new TDataprepInEndJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "";
  protected final String TEXT_2 = NL + NL + "\t\t}" + NL + "\t  \t" + NL + "\t} catch (InterruptedException e) {\t\t\t" + NL + "\t\te.printStackTrace();" + NL + "\t}" + NL + "  \t" + NL + "  }// end of the main while(true)" + NL + " }catch(Exception e){" + NL + " \tSystem.err.println(\"Unexpected error: \"+ e.getLocalizedMessage());\t" + NL + " }";
  protected final String TEXT_3 = NL;

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    stringBuffer.append(TEXT_1);
    
CodeGeneratorArgument codeGenArgument = (CodeGeneratorArgument) argument;
INode node = (INode)codeGenArgument.getArgument();
String cid = node.getUniqueName();

List<? extends IConnection> outConnections = node.getOutgoingConnections();

if((outConnections==null)&&(outConnections.isEmpty())) {
	return stringBuffer.toString();
}

boolean isOutFlowConnValid = false;
for(IConnection outConnection : outConnections) {
	if(outConnection.getLineStyle().hasConnectionCategory(IConnectionCategory.MAIN)) {
		isOutFlowConnValid = true;
	}
}

if(!isOutFlowConnValid) {
	return stringBuffer.toString();
}

String uname = ElementParameterParser.getValue(node, "__UNAME__");

    stringBuffer.append(TEXT_2);
    stringBuffer.append(TEXT_3);
    return stringBuffer.toString();
  }
}
