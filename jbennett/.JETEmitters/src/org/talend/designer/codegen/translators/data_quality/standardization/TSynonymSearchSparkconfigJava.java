package org.talend.designer.codegen.translators.data_quality.standardization;

import org.talend.core.model.metadata.IMetadataColumn;
import org.talend.core.model.metadata.IMetadataTable;
import org.talend.core.model.metadata.types.JavaType;
import org.talend.core.model.metadata.types.JavaTypesManager;
import org.talend.core.model.process.ElementParameterParser;
import org.talend.core.model.process.IBigDataNode;
import org.talend.core.model.process.IConnection;
import org.talend.core.model.process.IConnectionCategory;
import org.talend.core.model.process.INode;
import org.talend.core.model.utils.NodeUtil;
import org.talend.designer.common.BigDataCodeGeneratorArgument;

public class TSynonymSearchSparkconfigJava
{
  protected static String nl;
  public static synchronized TSynonymSearchSparkconfigJava create(String lineSeparator)
  {
    nl = lineSeparator;
    TSynonymSearchSparkconfigJava result = new TSynonymSearchSparkconfigJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "            public static class ";
  protected final String TEXT_2 = " implements ";
  protected final String TEXT_3 = " {" + NL + "                private ContextProperties context = new ContextProperties();";
  protected final String TEXT_4 = NL + NL + "                //private ContextProperties context = null;" + NL + "" + NL + "                public ";
  protected final String TEXT_5 = "(JobConf job) {" + NL + "                    this.context = new ContextProperties(job);";
  protected final String TEXT_6 = NL + "                }" + NL + "" + NL + "\t            public ";
  protected final String TEXT_7 = " ";
  protected final String TEXT_8 = "(";
  protected final String TEXT_9 = ") ";
  protected final String TEXT_10 = " {" + NL + "\t            \t";
  protected final String TEXT_11 = NL + "\t            \t";
  protected final String TEXT_12 = NL + "\t                ";
  protected final String TEXT_13 = NL + "\t                return ";
  protected final String TEXT_14 = ";" + NL + "\t            }" + NL + "\t        }" + NL + "\t\t";
  protected final String TEXT_15 = NL + "            public static class ";
  protected final String TEXT_16 = " implements ";
  protected final String TEXT_17 = " {";
  protected final String TEXT_18 = NL + NL + "                private ContextProperties context = null;" + NL + "" + NL + "                public ";
  protected final String TEXT_19 = "(JobConf job) {" + NL + "                    this.context = new ContextProperties(job);" + NL + "                }" + NL + "" + NL + "                public ";
  protected final String TEXT_20 = " ";
  protected final String TEXT_21 = "(";
  protected final String TEXT_22 = ") ";
  protected final String TEXT_23 = " {" + NL + "                \t";
  protected final String TEXT_24 = NL + "\t                 \treturn ";
  protected final String TEXT_25 = ";";
  protected final String TEXT_26 = NL + "                }" + NL + "            }";
  protected final String TEXT_27 = NL + "            public static class ";
  protected final String TEXT_28 = " implements ";
  protected final String TEXT_29 = " {";
  protected final String TEXT_30 = NL + NL + "                private ContextProperties context = null;" + NL + "                private java.util.function.Function<org.apache.avro.generic.IndexedRecord, org.apache.avro.generic.IndexedRecord> function = null;" + NL + "" + NL + "                public ";
  protected final String TEXT_31 = "(JobConf job, java.util.function.Function<org.apache.avro.generic.IndexedRecord, org.apache.avro.generic.IndexedRecord> function) {" + NL + "                    this.context = new ContextProperties(job);" + NL + "                    this.function = function;" + NL + "                }" + NL + "" + NL + "                public ";
  protected final String TEXT_32 = " ";
  protected final String TEXT_33 = "(";
  protected final String TEXT_34 = ") ";
  protected final String TEXT_35 = " {";
  protected final String TEXT_36 = NL + "                    ";
  protected final String TEXT_37 = NL + "                    ";
  protected final String TEXT_38 = NL + "                    return ";
  protected final String TEXT_39 = ";" + NL + "                }" + NL + "            }";
  protected final String TEXT_40 = NL;
  protected final String TEXT_41 = NL + "            // No sparkcode generated for unnecessary ";
  protected final String TEXT_42 = NL;
  protected final String TEXT_43 = NL + "            public static class ";
  protected final String TEXT_44 = "TrueFilter implements org.apache.spark.api.java.function.Function<scala.Tuple2<Boolean, org.apache.avro.specific.SpecificRecordBase>, Boolean> {" + NL + "" + NL + "                public Boolean call(scala.Tuple2<Boolean, org.apache.avro.specific.SpecificRecordBase> arg0)" + NL + "                        throws Exception {" + NL + "                    return arg0._1;" + NL + "                }" + NL + "            }" + NL + "" + NL + "            public static class ";
  protected final String TEXT_45 = "FalseFilter implements org.apache.spark.api.java.function.Function<scala.Tuple2<Boolean, org.apache.avro.specific.SpecificRecordBase>, Boolean> {" + NL + "" + NL + "                public Boolean call(scala.Tuple2<Boolean, org.apache.avro.specific.SpecificRecordBase> arg0)" + NL + "                        throws Exception {" + NL + "                    return !arg0._1;" + NL + "                }" + NL + "            }" + NL + "" + NL + "            public static class ";
  protected final String TEXT_46 = "ToNullWritableMain implements ";
  protected final String TEXT_47 = " {" + NL + "" + NL + "                private ContextProperties context = null;" + NL + "" + NL + "                public ";
  protected final String TEXT_48 = "ToNullWritableMain(JobConf job) {" + NL + "                    this.context = new ContextProperties(job);" + NL + "                }" + NL + "" + NL + "                public ";
  protected final String TEXT_49 = " call(" + NL + "                        scala.Tuple2<Boolean, org.apache.avro.specific.SpecificRecordBase> data){";
  protected final String TEXT_50 = NL + "                    ";
  protected final String TEXT_51 = " ";
  protected final String TEXT_52 = " = (";
  protected final String TEXT_53 = ")data._2;" + NL + "                    return ";
  protected final String TEXT_54 = ";" + NL + "                }" + NL + "            }" + NL + "" + NL + "            public static class ";
  protected final String TEXT_55 = "ToNullWritableReject implements ";
  protected final String TEXT_56 = " {" + NL + "" + NL + "                private ContextProperties context = null;" + NL + "" + NL + "                public ";
  protected final String TEXT_57 = "ToNullWritableReject(JobConf job) {" + NL + "                    this.context = new ContextProperties(job);" + NL + "                }" + NL + "" + NL + "                public ";
  protected final String TEXT_58 = " call(" + NL + "                        scala.Tuple2<Boolean, org.apache.avro.specific.SpecificRecordBase> data){";
  protected final String TEXT_59 = NL + "                        ";
  protected final String TEXT_60 = " ";
  protected final String TEXT_61 = " = (";
  protected final String TEXT_62 = ")data._2;" + NL + "                    return ";
  protected final String TEXT_63 = ";" + NL + "                }" + NL + "            }";
  protected final String TEXT_64 = NL + "            // No sparkconfig generated for unnecessary ";
  protected final String TEXT_65 = NL;
  protected final String TEXT_66 = NL + "            // Extract data." + NL;
  protected final String TEXT_67 = NL + "            ";
  protected final String TEXT_68 = "<Boolean, org.apache.avro.specific.SpecificRecordBase> temporary_rdd_";
  protected final String TEXT_69 = " = rdd_";
  protected final String TEXT_70 = ".";
  protected final String TEXT_71 = "(new ";
  protected final String TEXT_72 = "(job));" + NL + "" + NL + "            // Main flow" + NL;
  protected final String TEXT_73 = NL + "            ";
  protected final String TEXT_74 = " rdd_";
  protected final String TEXT_75 = " = temporary_rdd_";
  protected final String TEXT_76 = NL + "                  .filter(new ";
  protected final String TEXT_77 = "TrueFilter())" + NL + "                    .";
  protected final String TEXT_78 = "(new ";
  protected final String TEXT_79 = "ToNullWritableMain(job));" + NL + "" + NL + "            // Reject flow";
  protected final String TEXT_80 = NL + "            ";
  protected final String TEXT_81 = " rdd_";
  protected final String TEXT_82 = " = temporary_rdd_";
  protected final String TEXT_83 = NL + "                    .filter(new ";
  protected final String TEXT_84 = "FalseFilter())" + NL + "                    .";
  protected final String TEXT_85 = "(new ";
  protected final String TEXT_86 = "ToNullWritableReject(job));";
  protected final String TEXT_87 = NL + "            ";
  protected final String TEXT_88 = " rdd_";
  protected final String TEXT_89 = " = rdd_";
  protected final String TEXT_90 = ".";
  protected final String TEXT_91 = "(new ";
  protected final String TEXT_92 = "(job));";
  protected final String TEXT_93 = NL + "        org.talend.dataquality.standardization.record.SynonymRecordSearcher recSearch_";
  protected final String TEXT_94 = " = null;" + NL + "        int uniqueIndexNum_";
  protected final String TEXT_95 = ";" + NL + "        String gid_";
  protected final String TEXT_96 = " = null;" + NL + "        String[] inputs_";
  protected final String TEXT_97 = " = null;" + NL + "        org.talend.dataquality.standardization.index.Error error_";
  protected final String TEXT_98 = " = new org.talend.dataquality.standardization.index.Error();" + NL;
  protected final String TEXT_99 = NL + NL + "        if(recSearch_";
  protected final String TEXT_100 = " == null){" + NL + "           recSearch_";
  protected final String TEXT_101 = " = new org.talend.dataquality.standardization.record.SynonymRecordSearcher(";
  protected final String TEXT_102 = ");";
  protected final String TEXT_103 = NL + "                   org.talend.dataquality.standardization.index.SynonymIndexSearcher searcher";
  protected final String TEXT_104 = "_";
  protected final String TEXT_105 = " = new org.talend.dataquality.standardization.index.SynonymIndexSearcher(";
  protected final String TEXT_106 = ");" + NL + "                   searcher";
  protected final String TEXT_107 = "_";
  protected final String TEXT_108 = ".setSearchMode(org.talend.dataquality.standardization.index.SynonymIndexSearcher.SynonymSearchMode.";
  protected final String TEXT_109 = ");" + NL + "                   searcher";
  protected final String TEXT_110 = "_";
  protected final String TEXT_111 = ".setMaxEdits(";
  protected final String TEXT_112 = ");" + NL + "                   searcher";
  protected final String TEXT_113 = "_";
  protected final String TEXT_114 = ".setSlop(";
  protected final String TEXT_115 = ");" + NL + "                   searcher";
  protected final String TEXT_116 = "_";
  protected final String TEXT_117 = ".setTopDocLimit(";
  protected final String TEXT_118 = "); " + NL + "                   searcher";
  protected final String TEXT_119 = "_";
  protected final String TEXT_120 = ".setMatchingThreshold(";
  protected final String TEXT_121 = ");   " + NL + "                   recSearch_";
  protected final String TEXT_122 = ".addSearcher(searcher";
  protected final String TEXT_123 = "_";
  protected final String TEXT_124 = ", ";
  protected final String TEXT_125 = ");" + NL + "                       ";
  protected final String TEXT_126 = NL + "           uniqueIndexNum_";
  protected final String TEXT_127 = " = ";
  protected final String TEXT_128 = ";" + NL + "        }";
  protected final String TEXT_129 = NL + "        // if no output flow, without code generated." + NL + "        if (";
  protected final String TEXT_130 = " == null && ";
  protected final String TEXT_131 = " == null)" + NL + "            return";
  protected final String TEXT_132 = ";" + NL + "            " + NL + "        inputs_";
  protected final String TEXT_133 = " = new String[uniqueIndexNum_";
  protected final String TEXT_134 = "];" + NL + "        ";
  protected final String TEXT_135 = NL + "                inputs_";
  protected final String TEXT_136 = "[";
  protected final String TEXT_137 = "] = ";
  protected final String TEXT_138 = ".";
  protected final String TEXT_139 = ";";
  protected final String TEXT_140 = NL + "        java.util.List<org.talend.dataquality.standardization.record.OutputRecord> output_";
  protected final String TEXT_141 = " = null;" + NL + "        " + NL + "        try {" + NL + "            output_";
  protected final String TEXT_142 = " = recSearch_";
  protected final String TEXT_143 = ".search(";
  protected final String TEXT_144 = ", inputs_";
  protected final String TEXT_145 = ");" + NL + "        } catch (NullPointerException e) {" + NL + "            error_";
  protected final String TEXT_146 = ".set(false, \"Parse exception, field value is NULL\");" + NL + "        } catch (java.io.IOException e) {" + NL + "            error_";
  protected final String TEXT_147 = ".set(false, \"IO exception\");" + NL + "        }" + NL + "        boolean forceIn_";
  protected final String TEXT_148 = " = false;" + NL + "    " + NL + "        if (!error_";
  protected final String TEXT_149 = ".getStatus()) {" + NL + "           forceIn_";
  protected final String TEXT_150 = " = true;" + NL + "           ";
  protected final String TEXT_151 = NL + "               if (";
  protected final String TEXT_152 = " == null) {";
  protected final String TEXT_153 = NL + "                   ";
  protected final String TEXT_154 = " = new ";
  protected final String TEXT_155 = "();" + NL + "               }" + NL + "                            ";
  protected final String TEXT_156 = NL + "               ";
  protected final String TEXT_157 = NL + "               ";
  protected final String TEXT_158 = ".error_message = error_";
  protected final String TEXT_159 = ".getMessage();";
  protected final String TEXT_160 = NL + "               ";
  protected final String TEXT_161 = " = null;";
  protected final String TEXT_162 = NL + "        }" + NL + "        error_";
  protected final String TEXT_163 = ".reset();" + NL + "        " + NL + "        if (output_";
  protected final String TEXT_164 = " != null && !output_";
  protected final String TEXT_165 = ".isEmpty()) gid_";
  protected final String TEXT_166 = "=java.util.UUID.randomUUID().toString();" + NL + "        " + NL + "        for (int i_";
  protected final String TEXT_167 = " = 0; (output_";
  protected final String TEXT_168 = " != null && i_";
  protected final String TEXT_169 = " < output_";
  protected final String TEXT_170 = ".size()) || forceIn_";
  protected final String TEXT_171 = ";) {" + NL + "            org.talend.dataquality.standardization.record.OutputRecord outputRecord_";
  protected final String TEXT_172 = ";" + NL + "            " + NL + "            if (forceIn_";
  protected final String TEXT_173 = ") {" + NL + "                outputRecord_";
  protected final String TEXT_174 = " = null;" + NL + "                forceIn_";
  protected final String TEXT_175 = " = false;" + NL + "            } else {" + NL + "                outputRecord_";
  protected final String TEXT_176 = " = output_";
  protected final String TEXT_177 = ".get(i_";
  protected final String TEXT_178 = ");";
  protected final String TEXT_179 = " ";
  protected final String TEXT_180 = " = null;";
  protected final String TEXT_181 = NL + "                i_";
  protected final String TEXT_182 = "++;" + NL + "            } " + NL + "" + NL + "            //main connection case" + NL + "            if (org.talend.dataquality.parser.util.RecognitionError.getStatus()) {";
  protected final String TEXT_183 = NL + "                  if (outputRecord_";
  protected final String TEXT_184 = " != null) {" + NL + "                     if (";
  protected final String TEXT_185 = " == null) {";
  protected final String TEXT_186 = NL + "                         ";
  protected final String TEXT_187 = " = new ";
  protected final String TEXT_188 = "();" + NL + "                     }" + NL + "                     ";
  protected final String TEXT_189 = NL + "                             ";
  protected final String TEXT_190 = ".";
  protected final String TEXT_191 = " = outputRecord_";
  protected final String TEXT_192 = ".getRecord(";
  protected final String TEXT_193 = ");";
  protected final String TEXT_194 = NL + "                     " + NL + "                     // if the field exists in input schema, set the value with input.";
  protected final String TEXT_195 = NL + "                     ";
  protected final String TEXT_196 = NL + "                     ";
  protected final String TEXT_197 = NL + "                     ";
  protected final String TEXT_198 = ".GID = gid_";
  protected final String TEXT_199 = ";";
  protected final String TEXT_200 = NL + "                     ";
  protected final String TEXT_201 = ".GRP_SIZE = output_";
  protected final String TEXT_202 = ".size();";
  protected final String TEXT_203 = NL + "                     ";
  protected final String TEXT_204 = ".SCORE = outputRecord_";
  protected final String TEXT_205 = ".getScore();";
  protected final String TEXT_206 = NL + "                     ";
  protected final String TEXT_207 = ".SCORES = outputRecord_";
  protected final String TEXT_208 = ".getScores();";
  protected final String TEXT_209 = NL + "                     ";
  protected final String TEXT_210 = ".NB_MATCHED_FIELDS = outputRecord_";
  protected final String TEXT_211 = ".getNbMatch();" + NL + "                     " + NL + "                     // Emit the parsed structure on the main output.";
  protected final String TEXT_212 = NL + "                     ";
  protected final String TEXT_213 = NL + "                     " + NL + "                     //generate main code when component is not muti";
  protected final String TEXT_214 = NL + "                     ";
  protected final String TEXT_215 = NL + "                     " + NL + "                  }";
  protected final String TEXT_216 = NL + "                  //reject connection case" + NL + "             } else {";
  protected final String TEXT_217 = NL + "                    if (";
  protected final String TEXT_218 = " == null) {";
  protected final String TEXT_219 = NL + "                        ";
  protected final String TEXT_220 = " = new ";
  protected final String TEXT_221 = "();" + NL + "                    }" + NL;
  protected final String TEXT_222 = NL + "                    ";
  protected final String TEXT_223 = ".error_message = org.talend.dataquality.parser.util.RecognitionError.getMessage();";
  protected final String TEXT_224 = NL + "                    ";
  protected final String TEXT_225 = NL;
  protected final String TEXT_226 = NL + "                    ";
  protected final String TEXT_227 = NL + "                    //111111111111";
  protected final String TEXT_228 = NL + "            }" + NL + "        } // for" + NL + "            " + NL + "        for (int index_";
  protected final String TEXT_229 = " = 0 ; index_";
  protected final String TEXT_230 = " < uniqueIndexNum_";
  protected final String TEXT_231 = "; index_";
  protected final String TEXT_232 = "++) {" + NL + "           recSearch_";
  protected final String TEXT_233 = ".getSearcher(index_";
  protected final String TEXT_234 = ").close();" + NL + "        }" + NL + "        recSearch_";
  protected final String TEXT_235 = " = null;" + NL;
  protected final String TEXT_236 = NL + "            ";
  protected final String TEXT_237 = NL + "            // Die immediately on errors." + NL + "            throw new IOException(";
  protected final String TEXT_238 = ");";
  protected final String TEXT_239 = NL + "                // Ignore errors processing this row and move to the next.";
  protected final String TEXT_240 = NL + "                // Send error rows to the reject output.";
  protected final String TEXT_241 = NL + "                ";
  protected final String TEXT_242 = NL + "                ";
  protected final String TEXT_243 = NL + "                ";

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    
// Parse the inputs to this javajet generator.
final BigDataCodeGeneratorArgument codeGenArgument = (BigDataCodeGeneratorArgument) argument;
final INode node = (INode) codeGenArgument.getArgument();
final IBigDataNode bigDataNode = (IBigDataNode) codeGenArgument.getArgument();
final String cid = node.getUniqueName();

    
	/**
	 * Code generated for component with single output
 	 */
    class SOFunctionGenerator extends org.talend.designer.spark.generator.FunctionGenerator{

    	SOFunctionGenerator(org.talend.designer.common.TransformerBase transformer) {
            super(transformer);
        }

    	SOFunctionGenerator(org.talend.designer.common.TransformerBase transformer, org.talend.designer.spark.generator.SparkFunction sparkFunction) {
    		super(transformer, sparkFunction);
    	}

    	@Override
        public void generate(){
		
    stringBuffer.append(TEXT_1);
    stringBuffer.append(this.sparkFunction.getClassName(cid));
    stringBuffer.append(TEXT_2);
    stringBuffer.append(this.sparkFunction.getCodeFunctionImplementation(getOutValueClass(), getInValueClass()));
    stringBuffer.append(TEXT_3);
    
                this.transformer.generateHelperClasses(true);
                this.transformer.generateTransformContextDeclaration();
                
    stringBuffer.append(TEXT_4);
    stringBuffer.append(this.sparkFunction.getClassName(cid));
    stringBuffer.append(TEXT_5);
    
                    this.transformer.generateTransformContextDeclarationInConstructor();
                    
    stringBuffer.append(TEXT_6);
    stringBuffer.append(this.sparkFunction.getCodeFunctionReturnedType(this.outValueClass.toString()));
    stringBuffer.append(TEXT_7);
    stringBuffer.append(this.sparkFunction.getCodeImplementedMethod());
    stringBuffer.append(TEXT_8);
    stringBuffer.append(this.sparkFunction.getCodeFunctionArgument(getInValueClass()));
    stringBuffer.append(TEXT_9);
    stringBuffer.append(this.sparkFunction.getCodeThrowException());
    stringBuffer.append(TEXT_10);
    stringBuffer.append(this.sparkFunction.getMethodHeader(this.outValueClass, this.outValue, this.inValueClass, this.inValue));
    stringBuffer.append(TEXT_11);
    stringBuffer.append(this.sparkFunction.getCodeKeyMapping(getInValue()));
    stringBuffer.append(TEXT_12);
    
	                this.transformer.generateTransformContextInitialization();
	                this.transformer.generateTransform(true);
	                
    stringBuffer.append(TEXT_13);
    stringBuffer.append(this.sparkFunction.getCodeFunctionReturn(this.getOutValue(), this.getOutValueClass()));
    stringBuffer.append(TEXT_14);
    
        }
    }

	/**
	 * Code generated for component with multiple outputs
 	 */
    class MOFunctionGenerator extends org.talend.designer.spark.generator.FunctionGenerator{

        /** The single connection to pass along the output chain of Mappers
         *  instead of sending to a dedicated collector via MultipleOutputs. */
        String connectionToChain = null;

        MOFunctionGenerator(org.talend.designer.common.TransformerBase transformer) {
            super(transformer);
            defaultOutKeyClass = "Boolean";
        }

    	MOFunctionGenerator(org.talend.designer.common.TransformerBase transformer, org.talend.designer.spark.generator.SparkFunction sparkFunction) {
    		super(transformer, sparkFunction);
            defaultOutKeyClass = "Boolean";
    	}

    	@Override
        public void generate(){
        
    stringBuffer.append(TEXT_15);
    stringBuffer.append(this.sparkFunction.getClassName(cid));
    stringBuffer.append(TEXT_16);
    stringBuffer.append(this.sparkFunction.getCodeFunctionImplementationOutputFixedType(getInValueClass(), "Boolean", "org.apache.avro.specific.SpecificRecordBase"));
    stringBuffer.append(TEXT_17);
    
                this.transformer.generateHelperClasses(true);
                this.transformer.generateTransformContextDeclaration();
                
    stringBuffer.append(TEXT_18);
    stringBuffer.append(this.sparkFunction.getClassName(cid));
    stringBuffer.append(TEXT_19);
    stringBuffer.append(this.sparkFunction.getCodeFunctionReturnedTypeFixedType((String)this.outKeyClass, "org.apache.avro.specific.SpecificRecordBase"));
    stringBuffer.append(TEXT_20);
    stringBuffer.append(this.sparkFunction.getCodeImplementedMethod());
    stringBuffer.append(TEXT_21);
    stringBuffer.append(this.sparkFunction.getCodeFunctionArgument(getInValueClass()));
    stringBuffer.append(TEXT_22);
    stringBuffer.append(this.sparkFunction.getCodeThrowException());
    stringBuffer.append(TEXT_23);
    stringBuffer.append(this.sparkFunction.getMethodHeader(this.outValueClass, this.outValue, this.inValueClass, this.inValue));
    
                    this.transformer.generateTransformContextInitialization();
                    this.transformer.generateTransform(true);
	                if(this.sparkFunction.getCodeFunctionReturn()!=null) {
                
    stringBuffer.append(TEXT_24);
    stringBuffer.append(this.sparkFunction.getCodeFunctionReturn());
    stringBuffer.append(TEXT_25);
    
	            	}
                
    stringBuffer.append(TEXT_26);
    
        }
    }
    
    /**
     * Code generated for tDataprepRun component
     */
    class DataprepFunctionGenerator extends org.talend.designer.spark.generator.FunctionGenerator {

        DataprepFunctionGenerator(org.talend.designer.common.TransformerBase transformer) {
            super(transformer);
        }

        DataprepFunctionGenerator(org.talend.designer.common.TransformerBase transformer, org.talend.designer.spark.generator.SparkFunction sparkFunction) {
            super(transformer, sparkFunction);
        }

        @Override
        public void generate(){
        
    stringBuffer.append(TEXT_27);
    stringBuffer.append(this.sparkFunction.getClassName(cid));
    stringBuffer.append(TEXT_28);
    stringBuffer.append(this.sparkFunction.getCodeFunctionImplementation(getOutValueClass(), getInValueClass()));
    stringBuffer.append(TEXT_29);
    
                this.transformer.generateHelperClasses(true);
                this.transformer.generateTransformContextDeclaration();
                
    stringBuffer.append(TEXT_30);
    stringBuffer.append(this.sparkFunction.getClassName(cid));
    stringBuffer.append(TEXT_31);
    stringBuffer.append(this.sparkFunction.getCodeFunctionReturnedType(this.outValueClass.toString()));
    stringBuffer.append(TEXT_32);
    stringBuffer.append(this.sparkFunction.getCodeImplementedMethod());
    stringBuffer.append(TEXT_33);
    stringBuffer.append(this.sparkFunction.getCodeFunctionArgument(getInValueClass()));
    stringBuffer.append(TEXT_34);
    stringBuffer.append(this.sparkFunction.getCodeThrowException());
    stringBuffer.append(TEXT_35);
    stringBuffer.append(TEXT_36);
    stringBuffer.append(this.sparkFunction.getMethodHeader(this.outValueClass, this.outValue, this.inValueClass, this.inValue));
    stringBuffer.append(TEXT_37);
    stringBuffer.append(this.sparkFunction.getCodeKeyMapping(getInValue()));
    
                        this.transformer.generateTransformContextInitialization();
                        this.transformer.generateTransform(true);
                     
    stringBuffer.append(TEXT_38);
    stringBuffer.append(this.sparkFunction.getCodeFunctionReturn(this.getOutValue(), this.getOutValueClass()));
    stringBuffer.append(TEXT_39);
    
        }
    }

    stringBuffer.append(TEXT_40);
    

/**
 * A common, reusable utility that generates code in the context of a spark
 * component, for reading and writing to a spark RDD.
 */
class SparkRowTransformUtil extends org.talend.designer.common.CommonRowTransformUtil {

    private boolean isMultiOutput = false;

    private org.talend.designer.spark.generator.SparkFunction sparkFunction = null;

    private org.talend.designer.spark.generator.FunctionGenerator functionGenerator = null;

    public SparkRowTransformUtil() {

    }
    
    public SparkRowTransformUtil(org.talend.designer.spark.generator.SparkFunction sparkFunction) {
        this.sparkFunction = sparkFunction;
    }
    
    public void setFunctionGenerator(org.talend.designer.spark.generator.FunctionGenerator functionGenerator) {
        this.functionGenerator = functionGenerator;
    }

    public void setMultiOutput(boolean multiOutput) {
        isMultiOutput = multiOutput;
    }

    public String getCodeToGetInField(String columnName) {
        return functionGenerator.getInValue() + "." + columnName;
    }

    public String getInValue() {
        return functionGenerator.getInValue();
    }

    public String getOutValue() {
        return functionGenerator.getOutValue();
    }

    public String getInValueClass() {
        return functionGenerator.getInValueClass();
    }

    public String getOutValueClass() {
        return functionGenerator.getOutValueClass();
    }

    public String getCodeToGetOutField(boolean isReject, String columnName) {
        return functionGenerator.getOutValue(isReject ? "reject" : "main") + "." + columnName;
    }

    public String getCodeToInitOut(boolean isReject, Iterable<IMetadataColumn> columns) {
        if(!isReject && this.sparkFunction!=null && !isMultiOutput) {
            return this.sparkFunction.getCodeToInitOut(functionGenerator.getOutValue("main"), functionGenerator.getOutValueClass("main"));
        } else {
            return "";
        }
    }

    // Method to avoid using getCodeToInitOut that calls sparkFunction.getCodeToInitOut which creates unnecessary objects
    // Check getCodeToAddToOutput in SparkFunction and its implementation in FlatMapToPairFunction
    public String getCodeToAddToOutput(boolean isReject, Iterable<IMetadataColumn> columns) {
        if(this.sparkFunction!=null && !isMultiOutput) {
            return this.sparkFunction.getCodeToAddToOutput(false, false, functionGenerator.getOutValue(isReject ? "reject" : "main"), functionGenerator.getOutValueClass(isReject ? "reject" : "main"));
        }else if(this.sparkFunction!=null && isMultiOutput){
            if(isReject){
                return this.sparkFunction.getCodeToAddToOutput(true, false, functionGenerator.getOutValue("reject"), functionGenerator.getOutValueClass("reject"));
            }else{
                return this.sparkFunction.getCodeToAddToOutput(true, true, functionGenerator.getOutValue("main"), functionGenerator.getOutValueClass("main"));
            }
        }else {
            return "";
        }
    }

    public String getCodeToSetOutField(boolean isReject, String columnName, String codeValue) {
        return functionGenerator.getOutValue(isReject ? "reject" : "main") + "." + columnName + " = " + codeValue + ";";
    }

    public String getCodeToSetOutField(boolean isReject, String columnName, String codeValue, String operator) {
        return functionGenerator.getOutValue(isReject ? "reject" : "main") + "." + columnName + " " + operator + " " + codeValue + ";";
    }

    public String getCodeToEmit(boolean isReject) {
        if (this.sparkFunction != null && isMultiOutput) {
            if (isReject) {
                return this.sparkFunction.getCodeToEmit(false, functionGenerator.getOutValue("reject"), functionGenerator.getOutValueClass("reject"));
            } else {
                return this.sparkFunction.getCodeToEmit(true, functionGenerator.getOutValue("main"), functionGenerator.getOutValueClass("main"));
            }
        } else {
            if (isReject) {
                return this.sparkFunction.getCodeToInitOut(functionGenerator.getOutValue("reject"), functionGenerator.getOutValueClass("reject"));
            } else {
                return "";
            }
        }
    }

    public void generateSparkCode(final org.talend.designer.common.TransformerBase transformer, final org.talend.designer.spark.generator.SparkFunction sparkFunction) {
        if (transformer.isMultiOutput()) {
            setMultiOutput(true);
        }
        if (transformer.isUnnecessary()) {
            
    stringBuffer.append(TEXT_41);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_42);
    
            return;
        }

        if (transformer.isMultiOutput()) {
            org.talend.designer.spark.generator.SparkFunction localSparkFunction = null;
            if ((sparkFunction instanceof org.talend.designer.spark.generator.FlatMapFunction)
                    || (sparkFunction instanceof org.talend.designer.spark.generator.FlatMapToPairFunction)) {
                localSparkFunction = new org.talend.designer.spark.generator.FlatMapToPairFunction(
                        sparkFunction.isInputPair(),
                        codeGenArgument.getSparkVersion(),
                        sparkFunction.getKeyList());
            } else {
                localSparkFunction = new org.talend.designer.spark.generator.MapToPairFunction(
                        sparkFunction.isInputPair(),
                        sparkFunction.getKeyList());
            }

            org.talend.designer.spark.generator.SparkFunction extractSparkFunction = null;
            if ((sparkFunction instanceof org.talend.designer.spark.generator.FlatMapFunction)
                    || (sparkFunction instanceof org.talend.designer.spark.generator.MapFunction)) {
                extractSparkFunction = new org.talend.designer.spark.generator.MapFunction(
                        sparkFunction.isInputPair(),
                        sparkFunction.getKeyList());
            } else {
                extractSparkFunction = new org.talend.designer.spark.generator.MapToPairFunction(
                        sparkFunction.isInputPair(),
                        sparkFunction.getKeyList());
            }
            this.sparkFunction = localSparkFunction;

            // The multi-output condition is slightly different, and the
            // MapperHelper is configured with internal names for the
            // connections.
            java.util.HashMap<String, String> names = new java.util.HashMap<String, String>();
            names.put("main", transformer.getOutConnMainName());
            names.put("reject", transformer.getOutConnRejectName());

            // Refactoring FunctionGenerator to java so we have to instaniate a MO or SO here
            functionGenerator = new MOFunctionGenerator(transformer, localSparkFunction);
            functionGenerator.init(node, cid, null, transformer.getInConnName(), null, names);

            
    stringBuffer.append(TEXT_43);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_44);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_45);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_46);
    stringBuffer.append(extractSparkFunction.getCodeFunctionImplementationInputFixedType(transformer.getOutConnMainTypeName(), "Boolean", "org.apache.avro.specific.SpecificRecordBase"));
    stringBuffer.append(TEXT_47);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_48);
    stringBuffer.append(extractSparkFunction.getCodeFunctionReturnedType(transformer.getOutConnMainTypeName()));
    stringBuffer.append(TEXT_49);
    stringBuffer.append(TEXT_50);
    stringBuffer.append(transformer.getOutConnMainTypeName());
    stringBuffer.append(TEXT_51);
    stringBuffer.append(transformer.getOutConnMainName());
    stringBuffer.append(TEXT_52);
    stringBuffer.append(transformer.getOutConnMainTypeName());
    stringBuffer.append(TEXT_53);
    stringBuffer.append(extractSparkFunction.getCodeFunctionReturn(transformer.getOutConnMainName(), transformer.getOutConnMainTypeName()));
    stringBuffer.append(TEXT_54);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_55);
    stringBuffer.append(extractSparkFunction.getCodeFunctionImplementationInputFixedType(transformer.getOutConnRejectTypeName(), "Boolean", "org.apache.avro.specific.SpecificRecordBase"));
    stringBuffer.append(TEXT_56);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_57);
    stringBuffer.append(extractSparkFunction.getCodeFunctionReturnedType(transformer.getOutConnRejectTypeName()));
    stringBuffer.append(TEXT_58);
    stringBuffer.append(TEXT_59);
    stringBuffer.append(transformer.getOutConnRejectTypeName());
    stringBuffer.append(TEXT_60);
    stringBuffer.append(transformer.getOutConnRejectName());
    stringBuffer.append(TEXT_61);
    stringBuffer.append(transformer.getOutConnRejectTypeName());
    stringBuffer.append(TEXT_62);
    stringBuffer.append(extractSparkFunction.getCodeFunctionReturn(transformer.getOutConnRejectName(), transformer.getOutConnRejectTypeName()));
    stringBuffer.append(TEXT_63);
    
        } else {
            // Refactoring FunctionGenerator to java so we have to instaniate a MO or SO here
            functionGenerator = new SOFunctionGenerator(transformer, sparkFunction);

            functionGenerator.init(node, cid, null, transformer.getInConnName(), null,
                    transformer.getOutConnMainName() != null
                        ? transformer.getOutConnMainName()
                                : transformer.getOutConnRejectName());
        }
        functionGenerator.generate();
    }

    public void generateSparkConfig(final org.talend.designer.common.TransformerBase transformer, final org.talend.designer.spark.generator.SparkFunction sparkFunction) {
        if (transformer.isUnnecessary()) {
            
    stringBuffer.append(TEXT_64);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_65);
    
            return;
        }

        if (transformer.isMultiOutput()) {
            String localFunctionType = "mapToPair";
            if ((sparkFunction instanceof org.talend.designer.spark.generator.FlatMapFunction)
                    || (sparkFunction instanceof org.talend.designer.spark.generator.FlatMapToPairFunction)) {
                localFunctionType = "flatMapToPair";
            }

            String extractFunctionType = "mapToPair";
            if ((sparkFunction instanceof org.talend.designer.spark.generator.FlatMapFunction)
                    || (sparkFunction instanceof org.talend.designer.spark.generator.MapFunction)) {
                extractFunctionType = "map";
            }
            
    stringBuffer.append(TEXT_66);
    stringBuffer.append(TEXT_67);
    stringBuffer.append(sparkFunction.isStreaming() ?"org.apache.spark.streaming.api.java.JavaPairDStream":"org.apache.spark.api.java.JavaPairRDD");
    stringBuffer.append(TEXT_68);
    stringBuffer.append(transformer.getOutConnMainName());
    stringBuffer.append(TEXT_69);
    stringBuffer.append(transformer.getInConnName());
    stringBuffer.append(TEXT_70);
    stringBuffer.append(localFunctionType);
    stringBuffer.append(TEXT_71);
    stringBuffer.append(sparkFunction.getClassName(cid));
    stringBuffer.append(TEXT_72);
    stringBuffer.append(TEXT_73);
    stringBuffer.append(sparkFunction.getConfigReturnedType(transformer.getOutConnMainTypeName()));
    stringBuffer.append(TEXT_74);
    stringBuffer.append(transformer.getOutConnMainName());
    stringBuffer.append(TEXT_75);
    stringBuffer.append(transformer.getOutConnMainName());
    stringBuffer.append(TEXT_76);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_77);
    stringBuffer.append(extractFunctionType);
    stringBuffer.append(TEXT_78);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_79);
    stringBuffer.append(TEXT_80);
    stringBuffer.append(sparkFunction.getConfigReturnedType(transformer.getOutConnRejectTypeName()));
    stringBuffer.append(TEXT_81);
    stringBuffer.append(transformer.getOutConnRejectName());
    stringBuffer.append(TEXT_82);
    stringBuffer.append(transformer.getOutConnMainName());
    stringBuffer.append(TEXT_83);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_84);
    stringBuffer.append(extractFunctionType);
    stringBuffer.append(TEXT_85);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_86);
    
        } else {
            functionGenerator = new SOFunctionGenerator(transformer, sparkFunction);

            functionGenerator.init(node, cid, null, transformer.getInConnName(), null,
                    transformer.getOutConnMainName() != null
                        ? transformer.getOutConnMainName()
                                : transformer.getOutConnRejectName());
            
    stringBuffer.append(TEXT_87);
    stringBuffer.append(sparkFunction.getConfigReturnedType(transformer.getOutConnMainName() != null ? transformer.getOutConnMainTypeName() : transformer.getOutConnRejectTypeName()));
    stringBuffer.append(TEXT_88);
    stringBuffer.append(transformer.getOutConnMainName() != null ? transformer.getOutConnMainName() : transformer.getOutConnRejectName());
    stringBuffer.append(TEXT_89);
    stringBuffer.append(transformer.getInConnName());
    stringBuffer.append(TEXT_90);
    stringBuffer.append(sparkFunction.getConfigTransformation());
    stringBuffer.append(TEXT_91);
    stringBuffer.append(sparkFunction.getClassName(cid));
    stringBuffer.append(TEXT_92);
    
        }
    }
}


    

/**
 * Contains common processing for tSynonymSearch code generation.  This is
 * used in MapReduce and Storm components.
 *
 * The following imports must occur before importing this file:
 * @ include file="@{org.talend.designer.components.mrprovider}/resources/utils/common_codegen_util.javajet"
 */
class TSynonymSearchUtil extends org.talend.designer.common.TransformerBase {

    // TODO: what happens when these are input column names?
    final private static String REJECT_MSG = "errorMessage";
    final private static String REJECT_CODE = "errorCode";
    final private static String REJECT_FIELD = "inputLine";

    final private java.util.List<java.util.Map<String, String>> listToSearch = (java.util.List<java.util.Map<String, String>>)ElementParameterParser.getObjectValue(node, "__SEARCH_INDEXS__");

    java.util.List<? extends IConnection> outConns = node.getOutgoingSortedConnections();

    /** The outgoing connection contains the column {@link #REJECT_FIELD}.
     *  If we import a job from a DI job, this column will be missing */
    final private Boolean containsRejectField;

    /** The list of columns that should be copied directly from the input to
     *  the output schema (where they have the same column names). */
    final private Iterable<IMetadataColumn> copiedInColumns;

    /** Columns in the output schema that are not copied directly from the
     *  input schema (excluding reject fields). */
    final private java.util.List<IMetadataColumn> newOutColumns;

    /** TODO: Used in DI, tied to CHECK_NUM */
    final private boolean validateDatesStrict = false;
    final private boolean validateNumberOfMatchedGroups = false;

    public TSynonymSearchUtil(INode node,
            org.talend.designer.common.BigDataCodeGeneratorArgument argument,
            org.talend.designer.common.CommonRowTransformUtil rowTransformUtil) {
        super(node, argument, rowTransformUtil, "FLOW", "REJECT");

        containsRejectField = hasOutputColumn(true, REJECT_FIELD);

        if (null != getInConn() && null != getOutConnMain()) {
            copiedInColumns = getColumnsUnion(getInColumns(), getOutColumnsMain());
            newOutColumns = getColumnsDiff(getOutColumnsMain(), getInColumns());
        } else if (null != getInConn() && null != getOutConnReject()) {
            copiedInColumns = getColumnsUnion(getInColumns(), getOutColumnsReject());
            // If only the reject output is used, the new columns are those that
            // are not part of the main schema (ignore the REJECT_XXX columns).
            Iterable<IMetadataColumn> mainCols = getColumnsDiff(
                    getOutColumnsReject(), getColumns(getOutColumnsReject(),
                            REJECT_FIELD, REJECT_CODE, REJECT_MSG));
            newOutColumns = getColumnsDiff(mainCols, getInColumns());
        } else {
            copiedInColumns = null;
            newOutColumns = null;
        }
        
    }

    public void generateTransformContextDeclaration() {
        
    stringBuffer.append(TEXT_93);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_94);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_95);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_96);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_97);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_98);
    
    }

    public void generateTransformContextInitialization(){
        
    stringBuffer.append(TEXT_99);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_100);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_101);
    stringBuffer.append(listToSearch.size());
    stringBuffer.append(TEXT_102);
     
           java.util.List<String> uniqueInputName = new java.util.ArrayList<String>();
           int i = 0;
           
           for (java.util.Map<String, String> toSearch : listToSearch) {
               String inputName = toSearch.get("PRE_COLUMN");
               String indexPath = toSearch.get("INDEX_PATH");
               String searchMode = toSearch.get("SEARCH_MODE");
               String matchingThreshold = toSearch.get("SCORE_THRESHOLD");
               //String minimumSimilarity = toSearch.get("MINIMUM_SIMILARITY");
               String maxEdits = toSearch.get("MAX_EDITS_FOR_FUZZY_MATCH");
               String slop = toSearch.get("SLOP_FOR_PARTIAL_MATCH");
               int limit = Integer.parseInt(toSearch.get("NUM_TOPS"));
               
               // only use the previous index if duplicate
               if (!uniqueInputName.contains(inputName)) {
                   uniqueInputName.add(inputName);
                   
    stringBuffer.append(TEXT_103);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_104);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_105);
    stringBuffer.append(indexPath);
    stringBuffer.append(TEXT_106);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_107);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_108);
    stringBuffer.append(searchMode);
    stringBuffer.append(TEXT_109);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_110);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_111);
    stringBuffer.append(maxEdits);
    stringBuffer.append(TEXT_112);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_113);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_114);
    stringBuffer.append(slop);
    stringBuffer.append(TEXT_115);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_116);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_117);
    stringBuffer.append(limit);
    stringBuffer.append(TEXT_118);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_119);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_120);
    stringBuffer.append(matchingThreshold);
    stringBuffer.append(TEXT_121);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_122);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_123);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_124);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_125);
    
                   i++;
               }
           }
           
    stringBuffer.append(TEXT_126);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_127);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_128);
    
    }

    public void generateTransform() {
        generateTransform(false);
    }

    /**
     * Generates code that performs the tranformation from one input string
     * to many output strings, or to a reject flow.
     */
    public void generateTransform(boolean hasAReturnedType) {
        
    stringBuffer.append(TEXT_129);
    stringBuffer.append(getOutConnMainName());
    stringBuffer.append(TEXT_130);
    stringBuffer.append(getOutConnRejectName());
    stringBuffer.append(TEXT_131);
    stringBuffer.append(hasAReturnedType?" null":"");
    stringBuffer.append(TEXT_132);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_133);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_134);
    
        String resultLimit = ElementParameterParser.getValue(node, "__LIMIT__"); 

        java.util.List<String> inputFieldsName = new java.util.ArrayList<String>();
        for (IMetadataColumn preColumn : getInColumns()) {
		         inputFieldsName.add(preColumn.getLabel());
	      }

        java.util.List<String> uniqueInputName = new java.util.ArrayList<String>();
        java.util.List<String> uniqueOutputName = new java.util.ArrayList<String>();
        int i = 0;
        
        for (java.util.Map<String, String> toSearch : listToSearch) {
            String inputName = toSearch.get("PRE_COLUMN");
            String outputName = toSearch.get("COLUMN");
            
            if (!uniqueInputName.contains(inputName)){
                uniqueInputName.add(inputName);
                uniqueOutputName.add(outputName);
                
    stringBuffer.append(TEXT_135);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_136);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_137);
    stringBuffer.append(getInConnName());
    stringBuffer.append(TEXT_138);
    stringBuffer.append(inputName);
    stringBuffer.append(TEXT_139);
    
                i++;
            }
        }
    stringBuffer.append(TEXT_140);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_141);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_142);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_143);
    stringBuffer.append(resultLimit);
    stringBuffer.append(TEXT_144);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_145);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_146);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_147);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_148);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_149);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_150);
    if (getOutConnRejectName() != null) {
    stringBuffer.append(TEXT_151);
    stringBuffer.append(getOutConnRejectName());
    stringBuffer.append(TEXT_152);
    stringBuffer.append(TEXT_153);
    stringBuffer.append(getOutConnRejectName());
    stringBuffer.append(TEXT_154);
    stringBuffer.append(getOutConnRejectTypeName());
    stringBuffer.append(TEXT_155);
    stringBuffer.append(TEXT_156);
    stringBuffer.append(getRowTransform().getCodeToCopyFields(true, copiedInColumns));
    stringBuffer.append(TEXT_157);
    stringBuffer.append(getOutConnRejectName());
    stringBuffer.append(TEXT_158);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_159);
    
           }
           
           if (getOutConnMainName() != null) {
           
    stringBuffer.append(TEXT_160);
    stringBuffer.append(getOutConnMainName());
    stringBuffer.append(TEXT_161);
    }
    stringBuffer.append(TEXT_162);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_163);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_164);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_165);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_166);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_167);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_168);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_169);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_170);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_171);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_172);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_173);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_174);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_175);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_176);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_177);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_178);
    if (getOutConnRejectName() != null) {
    stringBuffer.append(TEXT_179);
    stringBuffer.append(getOutConnRejectName());
    stringBuffer.append(TEXT_180);
    }
    stringBuffer.append(TEXT_181);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_182);
    if (getOutConnMain() != null) {
    stringBuffer.append(TEXT_183);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_184);
    stringBuffer.append(getOutConnMainName());
    stringBuffer.append(TEXT_185);
    stringBuffer.append(TEXT_186);
    stringBuffer.append(getOutConnMainName());
    stringBuffer.append(TEXT_187);
    stringBuffer.append(getOutConnMainTypeName());
    stringBuffer.append(TEXT_188);
    
                     for (IMetadataColumn column : getOutColumnsMain()) {
                         String columnName = column.getLabel();
                         
					               // if the field do NOT exist in input schema, set the value with input.
					               if (uniqueOutputName.contains(columnName)) { 
                             // if the field exists in reference list, set the value with search result.
                             int index = uniqueOutputName.indexOf(columnName);
                             
    stringBuffer.append(TEXT_189);
    stringBuffer.append(getOutConnMainName());
    stringBuffer.append(TEXT_190);
    stringBuffer.append(columnName);
    stringBuffer.append(TEXT_191);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_192);
    stringBuffer.append(index);
    stringBuffer.append(TEXT_193);
    
                         }
                     }
                     
    stringBuffer.append(TEXT_194);
    stringBuffer.append(TEXT_195);
    stringBuffer.append(getRowTransform().getCodeToCopyFields(false, copiedInColumns));
    stringBuffer.append(TEXT_196);
    stringBuffer.append(TEXT_197);
    stringBuffer.append(getOutConnMainName());
    stringBuffer.append(TEXT_198);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_199);
    stringBuffer.append(TEXT_200);
    stringBuffer.append(getOutConnMainName());
    stringBuffer.append(TEXT_201);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_202);
    stringBuffer.append(TEXT_203);
    stringBuffer.append(getOutConnMainName());
    stringBuffer.append(TEXT_204);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_205);
    stringBuffer.append(TEXT_206);
    stringBuffer.append(getOutConnMainName());
    stringBuffer.append(TEXT_207);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_208);
    stringBuffer.append(TEXT_209);
    stringBuffer.append(getOutConnMainName());
    stringBuffer.append(TEXT_210);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_211);
    stringBuffer.append(TEXT_212);
    stringBuffer.append(getRowTransform().getCodeToEmit(false));
    stringBuffer.append(TEXT_213);
    stringBuffer.append(TEXT_214);
    stringBuffer.append(getRowTransform().getCodeToInitOut(null == getOutConnMain(), newOutColumns));
    stringBuffer.append(TEXT_215);
     }
    stringBuffer.append(TEXT_216);
    if (getOutConnReject() != null) {
    stringBuffer.append(TEXT_217);
    stringBuffer.append(getOutConnRejectName());
    stringBuffer.append(TEXT_218);
    stringBuffer.append(TEXT_219);
    stringBuffer.append(getOutConnRejectName());
    stringBuffer.append(TEXT_220);
    stringBuffer.append(getOutConnRejectTypeName());
    stringBuffer.append(TEXT_221);
    stringBuffer.append(TEXT_222);
    stringBuffer.append(getOutConnRejectName());
    stringBuffer.append(TEXT_223);
    stringBuffer.append(TEXT_224);
    stringBuffer.append(getRowTransform().getCodeToCopyFields(true, copiedInColumns));
    stringBuffer.append(TEXT_225);
    stringBuffer.append(TEXT_226);
    stringBuffer.append(getRowTransform().getCodeToEmit(true));
    stringBuffer.append(TEXT_227);
     }
    stringBuffer.append(TEXT_228);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_229);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_230);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_231);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_232);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_233);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_234);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_235);
    
    }


    /**
     * Generates code in the transform context to create reject output.
     *
     * @param die if this reject output should kill the job.  Normally, this is
     *    tied to a dieOnError parameter for the component, but can be
     *    explicitly set to false for non-fatal reject output.
     * @param codeException a variable in the transform scope that contains the
     *    variable with an exception.  If null, this will be constructed from
     *    the codeRejectMsg.
     * @param codeRejectMsg the error message to output with the reject output.
     */
    private void generateTransformReject(boolean die, String codeException, String codeRejectMsg) {
        if (codeRejectMsg == null) {
            codeRejectMsg = "\"" + cid + " - \" + " + codeException
                    + ".getMessage()";
            // Note: in DI, the error message can have the line number  appended
            // to it: " - Line: " + tos_count_nodeUniqueName()
        }

        if (codeException == null) {
            codeException = codeRejectMsg;
        }

        // If there are multiple outputs, then copy all of the new columns from
        // the original output to the error output.
        if (isMultiOutput()) {
            
    stringBuffer.append(TEXT_236);
    stringBuffer.append(getRowTransform().getCodeToCopyOutMainToReject(newOutColumns));
    
        }

        if (die) {
            
    stringBuffer.append(TEXT_237);
    stringBuffer.append(codeException);
    stringBuffer.append(TEXT_238);
    
        } else {
            if (null == getOutConnReject()) {
                
    stringBuffer.append(TEXT_239);
    
                generateLogMessage(codeRejectMsg);
            } else {
                
    stringBuffer.append(TEXT_240);
    stringBuffer.append(TEXT_241);
    stringBuffer.append(getRowTransform().getCodeToSetOutField(true, REJECT_MSG,
                        codeRejectMsg) );
    stringBuffer.append(TEXT_242);
    stringBuffer.append(getRowTransform().getCodeToCopyFields(true, copiedInColumns));
    stringBuffer.append(TEXT_243);
    stringBuffer.append(getRowTransform().getCodeToEmit(true));
    
            }
        }
        return;
    }

}

    
org.talend.designer.spark.generator.SparkFunction sparkFunction = null;
String requiredInputType = bigDataNode.getRequiredInputType();
String requiredOutputType = bigDataNode.getRequiredOutputType();
String incomingType = bigDataNode.getIncomingType();
String outgoingType = bigDataNode.getOutgoingType();
boolean inputIsPair = requiredInputType != null ? "KEYVALUE".equals(requiredInputType) : "KEYVALUE".equals(incomingType);

String type = requiredOutputType == null ? outgoingType : requiredOutputType;
if("KEYVALUE".equals(type)) {
    sparkFunction = new org.talend.designer.spark.generator.FlatMapToPairFunction(inputIsPair, codeGenArgument.getSparkVersion());
} else if("VALUE".equals(type)) {
    sparkFunction = new org.talend.designer.spark.generator.FlatMapFunction(inputIsPair, codeGenArgument.getSparkVersion());
}

boolean isNodeInBatchMode = org.talend.designer.common.tmap.LookupUtil.isNodeInBatchMode(node);
if("SPARKSTREAMING".equals(node.getComponent().getType()) && !isNodeInBatchMode) {
    sparkFunction.setStreaming(true);
}

final SparkRowTransformUtil sparkTransformUtil = new SparkRowTransformUtil(sparkFunction);
final TSynonymSearchUtil tSynonymSearchUtil = new TSynonymSearchUtil(
        node, codeGenArgument, sparkTransformUtil);

sparkTransformUtil.generateSparkConfig(tSynonymSearchUtil, sparkFunction);

    return stringBuffer.toString();
  }
}
