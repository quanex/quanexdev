package org.talend.designer.codegen.translators.processing;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.talend.core.model.metadata.IMetadataColumn;
import org.talend.core.model.metadata.IMetadataTable;
import org.talend.core.model.metadata.types.JavaType;
import org.talend.core.model.metadata.types.JavaTypesManager;
import org.talend.core.model.process.ElementParameterParser;
import org.talend.core.model.process.IBigDataNode;
import org.talend.core.model.process.IConnection;
import org.talend.core.model.process.IConnectionCategory;
import org.talend.core.model.process.INode;
import org.talend.designer.common.BigDataCodeGeneratorArgument;

public class TTopSparkstreamingcodeJava
{
  protected static String nl;
  public static synchronized TTopSparkstreamingcodeJava create(String lineSeparator)
  {
    nl = lineSeparator;
    TTopSparkstreamingcodeJava result = new TTopSparkstreamingcodeJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "\t";
  protected final String TEXT_2 = NL + "            public static class ";
  protected final String TEXT_3 = " implements ";
  protected final String TEXT_4 = " {" + NL + "                private ContextProperties context = new ContextProperties();";
  protected final String TEXT_5 = NL + NL + "                //private ContextProperties context = null;" + NL + "" + NL + "                public ";
  protected final String TEXT_6 = "(JobConf job) {" + NL + "                    this.context = new ContextProperties(job);";
  protected final String TEXT_7 = NL + "                }" + NL + "" + NL + "\t            public ";
  protected final String TEXT_8 = " ";
  protected final String TEXT_9 = "(";
  protected final String TEXT_10 = ") ";
  protected final String TEXT_11 = " {" + NL + "\t            \t";
  protected final String TEXT_12 = NL + "\t            \t";
  protected final String TEXT_13 = NL + "\t                ";
  protected final String TEXT_14 = NL + "\t                return ";
  protected final String TEXT_15 = ";" + NL + "\t            }" + NL + "\t        }" + NL + "\t\t";
  protected final String TEXT_16 = NL + "            public static class ";
  protected final String TEXT_17 = " implements ";
  protected final String TEXT_18 = " {";
  protected final String TEXT_19 = NL + NL + "                private ContextProperties context = null;" + NL + "" + NL + "                public ";
  protected final String TEXT_20 = "(JobConf job) {" + NL + "                    this.context = new ContextProperties(job);" + NL + "                }" + NL + "" + NL + "                public ";
  protected final String TEXT_21 = " ";
  protected final String TEXT_22 = "(";
  protected final String TEXT_23 = ") ";
  protected final String TEXT_24 = " {" + NL + "                \t";
  protected final String TEXT_25 = NL + "\t                 \treturn ";
  protected final String TEXT_26 = ";";
  protected final String TEXT_27 = NL + "                }" + NL + "            }";
  protected final String TEXT_28 = NL + "            public static class ";
  protected final String TEXT_29 = " implements ";
  protected final String TEXT_30 = " {";
  protected final String TEXT_31 = NL + NL + "                private ContextProperties context = null;" + NL + "                private java.util.function.Function<org.apache.avro.generic.IndexedRecord, org.apache.avro.generic.IndexedRecord> function = null;" + NL + "" + NL + "                public ";
  protected final String TEXT_32 = "(JobConf job, java.util.function.Function<org.apache.avro.generic.IndexedRecord, org.apache.avro.generic.IndexedRecord> function) {" + NL + "                    this.context = new ContextProperties(job);" + NL + "                    this.function = function;" + NL + "                }" + NL + "" + NL + "                public ";
  protected final String TEXT_33 = " ";
  protected final String TEXT_34 = "(";
  protected final String TEXT_35 = ") ";
  protected final String TEXT_36 = " {";
  protected final String TEXT_37 = NL + "                    ";
  protected final String TEXT_38 = NL + "                    ";
  protected final String TEXT_39 = NL + "                    return ";
  protected final String TEXT_40 = ";" + NL + "                }" + NL + "            }";
  protected final String TEXT_41 = NL;
  protected final String TEXT_42 = NL + "            // No sparkcode generated for unnecessary ";
  protected final String TEXT_43 = NL;
  protected final String TEXT_44 = NL + "            // No sparkconfig generated for unnecessary ";
  protected final String TEXT_45 = NL;
  protected final String TEXT_46 = NL + "        ";
  protected final String TEXT_47 = " rdd_";
  protected final String TEXT_48 = " =" + NL + "            ctx.parallelizePairs(" + NL + "                    rdd_";
  protected final String TEXT_49 = ".";
  protected final String TEXT_50 = "(";
  protected final String TEXT_51 = "," + NL + "                        new ";
  protected final String TEXT_52 = "(job))," + NL + "                1); // One partition to keep the ordering.";
  protected final String TEXT_53 = NL + "            return 1;";
  protected final String TEXT_54 = NL + "            return -1;";
  protected final String TEXT_55 = NL + "            return -1;";
  protected final String TEXT_56 = NL + "            return 1;";
  protected final String TEXT_57 = NL + "            if (";
  protected final String TEXT_58 = " == null && ";
  protected final String TEXT_59 = " != null) {";
  protected final String TEXT_60 = NL + "            } else if (";
  protected final String TEXT_61 = " != null && ";
  protected final String TEXT_62 = " == null) {";
  protected final String TEXT_63 = NL + "            } else if (";
  protected final String TEXT_64 = " == null && ";
  protected final String TEXT_65 = " == null) {" + NL + "                //ignore" + NL + "            } else {";
  protected final String TEXT_66 = NL + "            if (";
  protected final String TEXT_67 = " != ";
  protected final String TEXT_68 = ") {" + NL + "                if (";
  protected final String TEXT_69 = ") {";
  protected final String TEXT_70 = NL + "                } else {";
  protected final String TEXT_71 = NL + "                }" + NL + "            }";
  protected final String TEXT_72 = NL + "            if (";
  protected final String TEXT_73 = " > ";
  protected final String TEXT_74 = ") {";
  protected final String TEXT_75 = NL + "            } else if (";
  protected final String TEXT_76 = " < ";
  protected final String TEXT_77 = ") {";
  protected final String TEXT_78 = NL + "            }";
  protected final String TEXT_79 = NL + "            int comp_";
  protected final String TEXT_80 = " = ";
  protected final String TEXT_81 = ".compareTo(";
  protected final String TEXT_82 = ");" + NL + "            if (comp_";
  protected final String TEXT_83 = " != 0) {" + NL + "                if (comp_";
  protected final String TEXT_84 = " > 0) {";
  protected final String TEXT_85 = NL + "                } else {";
  protected final String TEXT_86 = NL + "                }" + NL + "            }";
  protected final String TEXT_87 = NL + "            if (";
  protected final String TEXT_88 = " - ";
  protected final String TEXT_89 = " != 0) {" + NL + "                if (";
  protected final String TEXT_90 = " - ";
  protected final String TEXT_91 = " > 0) {";
  protected final String TEXT_92 = NL + "                } else {";
  protected final String TEXT_93 = NL + "                }" + NL + "            }";
  protected final String TEXT_94 = NL + "                int cmp_";
  protected final String TEXT_95 = " = FormatterUtils.format_DateInUTC(";
  protected final String TEXT_96 = ", ";
  protected final String TEXT_97 = ").compareTo(FormatterUtils.format_DateInUTC(";
  protected final String TEXT_98 = ", ";
  protected final String TEXT_99 = "));" + NL + "                if (cmp_";
  protected final String TEXT_100 = " > 0) {";
  protected final String TEXT_101 = NL + "                } else if (cmp_";
  protected final String TEXT_102 = " < 0) {";
  protected final String TEXT_103 = NL + "                }";
  protected final String TEXT_104 = NL + "                if (!";
  protected final String TEXT_105 = ".equals(";
  protected final String TEXT_106 = ")) {" + NL + "                    if (";
  protected final String TEXT_107 = ".compareTo(";
  protected final String TEXT_108 = ") > 0) {";
  protected final String TEXT_109 = NL + "                    } else {";
  protected final String TEXT_110 = NL + "                    }" + NL + "                }";
  protected final String TEXT_111 = NL + "                int cmp_";
  protected final String TEXT_112 = " = String.valueOf(";
  protected final String TEXT_113 = ").compareTo(String.valueOf(";
  protected final String TEXT_114 = "));" + NL + "                if (cmp_";
  protected final String TEXT_115 = " > 0) {";
  protected final String TEXT_116 = NL + "                } else if (cmp_";
  protected final String TEXT_117 = " < 0) {";
  protected final String TEXT_118 = NL + "                }";
  protected final String TEXT_119 = NL + "                if (";
  protected final String TEXT_120 = " > ";
  protected final String TEXT_121 = ") {";
  protected final String TEXT_122 = NL + "                } else if (";
  protected final String TEXT_123 = " < ";
  protected final String TEXT_124 = ") {";
  protected final String TEXT_125 = NL + "                }";
  protected final String TEXT_126 = NL + "                int cmp_";
  protected final String TEXT_127 = " = String.valueOf(";
  protected final String TEXT_128 = ").compareTo(String.valueOf(";
  protected final String TEXT_129 = "));" + NL + "                if (cmp_";
  protected final String TEXT_130 = " > 0) {";
  protected final String TEXT_131 = NL + "                } else if (cmp_";
  protected final String TEXT_132 = " < 0) {";
  protected final String TEXT_133 = NL + "                }";
  protected final String TEXT_134 = NL + "                if (";
  protected final String TEXT_135 = " > ";
  protected final String TEXT_136 = ") {";
  protected final String TEXT_137 = NL + "                } else if (";
  protected final String TEXT_138 = " < ";
  protected final String TEXT_139 = ") {";
  protected final String TEXT_140 = NL + "                }";
  protected final String TEXT_141 = NL + "                int cmp_";
  protected final String TEXT_142 = " = String.valueOf(";
  protected final String TEXT_143 = ").compareTo(String.valueOf(";
  protected final String TEXT_144 = "));";
  protected final String TEXT_145 = NL + "                int cmp_";
  protected final String TEXT_146 = " = ";
  protected final String TEXT_147 = ".compareTo(";
  protected final String TEXT_148 = ");";
  protected final String TEXT_149 = NL + "                if (cmp_";
  protected final String TEXT_150 = " > 0) {";
  protected final String TEXT_151 = NL + "                } else if (cmp_";
  protected final String TEXT_152 = " < 0) {";
  protected final String TEXT_153 = NL + "                }";
  protected final String TEXT_154 = NL + "                int cmp_";
  protected final String TEXT_155 = " = String.valueOf(";
  protected final String TEXT_156 = ").compareTo(String.valueOf(";
  protected final String TEXT_157 = "));" + NL + "                if (cmp_";
  protected final String TEXT_158 = " > 0) {";
  protected final String TEXT_159 = NL + "                } else if (cmp_";
  protected final String TEXT_160 = " < 0) {";
  protected final String TEXT_161 = NL + "                }";
  protected final String TEXT_162 = NL + "                if (";
  protected final String TEXT_163 = " > ";
  protected final String TEXT_164 = ") {";
  protected final String TEXT_165 = NL + "                } else if (";
  protected final String TEXT_166 = " < ";
  protected final String TEXT_167 = ") {";
  protected final String TEXT_168 = NL + "                }";
  protected final String TEXT_169 = NL + "                int cmp_";
  protected final String TEXT_170 = " = String.valueOf(";
  protected final String TEXT_171 = ").compareTo(String.valueOf(";
  protected final String TEXT_172 = "));" + NL + "                if (cmp_";
  protected final String TEXT_173 = " > 0) {";
  protected final String TEXT_174 = NL + "                } else if (cmp_";
  protected final String TEXT_175 = " < 0) {";
  protected final String TEXT_176 = NL + "                }";
  protected final String TEXT_177 = NL + "                if (";
  protected final String TEXT_178 = " > ";
  protected final String TEXT_179 = ") {";
  protected final String TEXT_180 = NL + "                } else if (";
  protected final String TEXT_181 = " < ";
  protected final String TEXT_182 = ") {";
  protected final String TEXT_183 = NL + "                }";
  protected final String TEXT_184 = NL + "                int cmp_";
  protected final String TEXT_185 = " = String.valueOf(";
  protected final String TEXT_186 = ").compareTo(String.valueOf(";
  protected final String TEXT_187 = "));" + NL + "                if (cmp_";
  protected final String TEXT_188 = " > 0) {";
  protected final String TEXT_189 = NL + "                } else if (cmp_";
  protected final String TEXT_190 = " < 0) {";
  protected final String TEXT_191 = NL + "                }";
  protected final String TEXT_192 = NL + "                if (";
  protected final String TEXT_193 = " > ";
  protected final String TEXT_194 = ") {";
  protected final String TEXT_195 = NL + "                } else if (";
  protected final String TEXT_196 = " < ";
  protected final String TEXT_197 = ") {";
  protected final String TEXT_198 = NL + "                }";
  protected final String TEXT_199 = NL + "            int comp_";
  protected final String TEXT_200 = " = ";
  protected final String TEXT_201 = ".compareTo(";
  protected final String TEXT_202 = ");" + NL + "            if (comp_";
  protected final String TEXT_203 = " != 0) {" + NL + "                if (comp_";
  protected final String TEXT_204 = " > 0) {";
  protected final String TEXT_205 = NL + "                } else {";
  protected final String TEXT_206 = NL + "                }" + NL + "" + NL + "            }";
  protected final String TEXT_207 = NL + "            throw new JobConfigurationError(\"The ";
  protected final String TEXT_208 = " type is not supported: column--";
  protected final String TEXT_209 = "\");";
  protected final String TEXT_210 = NL + "            }";
  protected final String TEXT_211 = NL + "\t";
  protected final String TEXT_212 = NL + "            public static class ";
  protected final String TEXT_213 = " implements ";
  protected final String TEXT_214 = " {" + NL + "                private ContextProperties context = new ContextProperties();";
  protected final String TEXT_215 = NL + NL + "                //private ContextProperties context = null;" + NL + "" + NL + "                public ";
  protected final String TEXT_216 = "(JobConf job) {" + NL + "                    this.context = new ContextProperties(job);";
  protected final String TEXT_217 = NL + "                }" + NL + "" + NL + "\t            public ";
  protected final String TEXT_218 = " ";
  protected final String TEXT_219 = "(";
  protected final String TEXT_220 = ") ";
  protected final String TEXT_221 = " {" + NL + "\t            \t";
  protected final String TEXT_222 = NL + "\t            \t";
  protected final String TEXT_223 = NL + "\t                ";
  protected final String TEXT_224 = NL + "\t                return ";
  protected final String TEXT_225 = ";" + NL + "\t            }" + NL + "\t        }" + NL + "\t\t";
  protected final String TEXT_226 = NL + "            public static class ";
  protected final String TEXT_227 = " implements ";
  protected final String TEXT_228 = " {";
  protected final String TEXT_229 = NL + NL + "                private ContextProperties context = null;" + NL + "" + NL + "                public ";
  protected final String TEXT_230 = "(JobConf job) {" + NL + "                    this.context = new ContextProperties(job);" + NL + "                }" + NL + "" + NL + "                public ";
  protected final String TEXT_231 = " ";
  protected final String TEXT_232 = "(";
  protected final String TEXT_233 = ") ";
  protected final String TEXT_234 = " {" + NL + "                \t";
  protected final String TEXT_235 = NL + "\t                 \treturn ";
  protected final String TEXT_236 = ";";
  protected final String TEXT_237 = NL + "                }" + NL + "            }";
  protected final String TEXT_238 = NL + "            public static class ";
  protected final String TEXT_239 = " implements ";
  protected final String TEXT_240 = " {";
  protected final String TEXT_241 = NL + NL + "                private ContextProperties context = null;" + NL + "                private java.util.function.Function<org.apache.avro.generic.IndexedRecord, org.apache.avro.generic.IndexedRecord> function = null;" + NL + "" + NL + "                public ";
  protected final String TEXT_242 = "(JobConf job, java.util.function.Function<org.apache.avro.generic.IndexedRecord, org.apache.avro.generic.IndexedRecord> function) {" + NL + "                    this.context = new ContextProperties(job);" + NL + "                    this.function = function;" + NL + "                }" + NL + "" + NL + "                public ";
  protected final String TEXT_243 = " ";
  protected final String TEXT_244 = "(";
  protected final String TEXT_245 = ") ";
  protected final String TEXT_246 = " {";
  protected final String TEXT_247 = NL + "                    ";
  protected final String TEXT_248 = NL + "                    ";
  protected final String TEXT_249 = NL + "                    return ";
  protected final String TEXT_250 = ";" + NL + "                }" + NL + "            }";
  protected final String TEXT_251 = NL;
  protected final String TEXT_252 = NL + "            // No sparkcode generated for unnecessary ";
  protected final String TEXT_253 = NL + "        public static class StreamingTop_";
  protected final String TEXT_254 = " implements org.apache.spark.api.java.function.Function<";
  protected final String TEXT_255 = ", ";
  protected final String TEXT_256 = "> {" + NL + "            transient private org.apache.spark.api.java.JavaSparkContext ctx = null;" + NL + "            private Integer top = 5;" + NL + "" + NL + "            public StreamingTop_";
  protected final String TEXT_257 = "(Integer top) {" + NL + "                this.top = top;" + NL + "            }" + NL + "" + NL + "            public ";
  protected final String TEXT_258 = " call(";
  protected final String TEXT_259 = " temporaryRdd)" + NL + "                    throws Exception {" + NL + "                if (ctx == null) {" + NL + "                    ctx = new org.apache.spark.api.java.JavaSparkContext(temporaryRdd.context());" + NL + "                }" + NL + "                return ctx.parallelizePairs(" + NL + "                        temporaryRdd.";
  protected final String TEXT_260 = "(top," + NL + "                                new ";
  protected final String TEXT_261 = "(new JobConf())));" + NL + "            }" + NL + "        }";
  protected final String TEXT_262 = NL + "            // No sparkconfig generated for unnecessary ";
  protected final String TEXT_263 = NL + "            ";
  protected final String TEXT_264 = " rdd_";
  protected final String TEXT_265 = " =" + NL + "                    rdd_";
  protected final String TEXT_266 = ".transformToPair(new StreamingTop_";
  protected final String TEXT_267 = "(";
  protected final String TEXT_268 = "));";
  protected final String TEXT_269 = NL + "            return 1;";
  protected final String TEXT_270 = NL + "            return -1;";
  protected final String TEXT_271 = NL + "            return -1;";
  protected final String TEXT_272 = NL + "            return 1;";
  protected final String TEXT_273 = NL + "            if (";
  protected final String TEXT_274 = " == null && ";
  protected final String TEXT_275 = " != null) {";
  protected final String TEXT_276 = NL + "            } else if (";
  protected final String TEXT_277 = " != null && ";
  protected final String TEXT_278 = " == null) {";
  protected final String TEXT_279 = NL + "            } else if (";
  protected final String TEXT_280 = " == null && ";
  protected final String TEXT_281 = " == null) {" + NL + "                //ignore" + NL + "            } else {";
  protected final String TEXT_282 = NL + "            if (";
  protected final String TEXT_283 = " != ";
  protected final String TEXT_284 = ") {" + NL + "                if (";
  protected final String TEXT_285 = ") {";
  protected final String TEXT_286 = NL + "                } else {";
  protected final String TEXT_287 = NL + "                }" + NL + "            }";
  protected final String TEXT_288 = NL + "            if (";
  protected final String TEXT_289 = " > ";
  protected final String TEXT_290 = ") {";
  protected final String TEXT_291 = NL + "            } else if (";
  protected final String TEXT_292 = " < ";
  protected final String TEXT_293 = ") {";
  protected final String TEXT_294 = NL + "            }";
  protected final String TEXT_295 = NL + "            int comp_";
  protected final String TEXT_296 = " = ";
  protected final String TEXT_297 = ".compareTo(";
  protected final String TEXT_298 = ");" + NL + "            if (comp_";
  protected final String TEXT_299 = " != 0) {" + NL + "                if (comp_";
  protected final String TEXT_300 = " > 0) {";
  protected final String TEXT_301 = NL + "                } else {";
  protected final String TEXT_302 = NL + "                }" + NL + "            }";
  protected final String TEXT_303 = NL + "            if (";
  protected final String TEXT_304 = " - ";
  protected final String TEXT_305 = " != 0) {" + NL + "                if (";
  protected final String TEXT_306 = " - ";
  protected final String TEXT_307 = " > 0) {";
  protected final String TEXT_308 = NL + "                } else {";
  protected final String TEXT_309 = NL + "                }" + NL + "            }";
  protected final String TEXT_310 = NL + "                int cmp_";
  protected final String TEXT_311 = " = FormatterUtils.format_DateInUTC(";
  protected final String TEXT_312 = ", ";
  protected final String TEXT_313 = ").compareTo(FormatterUtils.format_DateInUTC(";
  protected final String TEXT_314 = ", ";
  protected final String TEXT_315 = "));" + NL + "                if (cmp_";
  protected final String TEXT_316 = " > 0) {";
  protected final String TEXT_317 = NL + "                } else if (cmp_";
  protected final String TEXT_318 = " < 0) {";
  protected final String TEXT_319 = NL + "                }";
  protected final String TEXT_320 = NL + "                if (!";
  protected final String TEXT_321 = ".equals(";
  protected final String TEXT_322 = ")) {" + NL + "                    if (";
  protected final String TEXT_323 = ".compareTo(";
  protected final String TEXT_324 = ") > 0) {";
  protected final String TEXT_325 = NL + "                    } else {";
  protected final String TEXT_326 = NL + "                    }" + NL + "                }";
  protected final String TEXT_327 = NL + "                int cmp_";
  protected final String TEXT_328 = " = String.valueOf(";
  protected final String TEXT_329 = ").compareTo(String.valueOf(";
  protected final String TEXT_330 = "));" + NL + "                if (cmp_";
  protected final String TEXT_331 = " > 0) {";
  protected final String TEXT_332 = NL + "                } else if (cmp_";
  protected final String TEXT_333 = " < 0) {";
  protected final String TEXT_334 = NL + "                }";
  protected final String TEXT_335 = NL + "                if (";
  protected final String TEXT_336 = " > ";
  protected final String TEXT_337 = ") {";
  protected final String TEXT_338 = NL + "                } else if (";
  protected final String TEXT_339 = " < ";
  protected final String TEXT_340 = ") {";
  protected final String TEXT_341 = NL + "                }";
  protected final String TEXT_342 = NL + "                int cmp_";
  protected final String TEXT_343 = " = String.valueOf(";
  protected final String TEXT_344 = ").compareTo(String.valueOf(";
  protected final String TEXT_345 = "));" + NL + "                if (cmp_";
  protected final String TEXT_346 = " > 0) {";
  protected final String TEXT_347 = NL + "                } else if (cmp_";
  protected final String TEXT_348 = " < 0) {";
  protected final String TEXT_349 = NL + "                }";
  protected final String TEXT_350 = NL + "                if (";
  protected final String TEXT_351 = " > ";
  protected final String TEXT_352 = ") {";
  protected final String TEXT_353 = NL + "                } else if (";
  protected final String TEXT_354 = " < ";
  protected final String TEXT_355 = ") {";
  protected final String TEXT_356 = NL + "                }";
  protected final String TEXT_357 = NL + "                int cmp_";
  protected final String TEXT_358 = " = String.valueOf(";
  protected final String TEXT_359 = ").compareTo(String.valueOf(";
  protected final String TEXT_360 = "));";
  protected final String TEXT_361 = NL + "                int cmp_";
  protected final String TEXT_362 = " = ";
  protected final String TEXT_363 = ".compareTo(";
  protected final String TEXT_364 = ");";
  protected final String TEXT_365 = NL + "                if (cmp_";
  protected final String TEXT_366 = " > 0) {";
  protected final String TEXT_367 = NL + "                } else if (cmp_";
  protected final String TEXT_368 = " < 0) {";
  protected final String TEXT_369 = NL + "                }";
  protected final String TEXT_370 = NL + "                int cmp_";
  protected final String TEXT_371 = " = String.valueOf(";
  protected final String TEXT_372 = ").compareTo(String.valueOf(";
  protected final String TEXT_373 = "));" + NL + "                if (cmp_";
  protected final String TEXT_374 = " > 0) {";
  protected final String TEXT_375 = NL + "                } else if (cmp_";
  protected final String TEXT_376 = " < 0) {";
  protected final String TEXT_377 = NL + "                }";
  protected final String TEXT_378 = NL + "                if (";
  protected final String TEXT_379 = " > ";
  protected final String TEXT_380 = ") {";
  protected final String TEXT_381 = NL + "                } else if (";
  protected final String TEXT_382 = " < ";
  protected final String TEXT_383 = ") {";
  protected final String TEXT_384 = NL + "                }";
  protected final String TEXT_385 = NL + "                int cmp_";
  protected final String TEXT_386 = " = String.valueOf(";
  protected final String TEXT_387 = ").compareTo(String.valueOf(";
  protected final String TEXT_388 = "));" + NL + "                if (cmp_";
  protected final String TEXT_389 = " > 0) {";
  protected final String TEXT_390 = NL + "                } else if (cmp_";
  protected final String TEXT_391 = " < 0) {";
  protected final String TEXT_392 = NL + "                }";
  protected final String TEXT_393 = NL + "                if (";
  protected final String TEXT_394 = " > ";
  protected final String TEXT_395 = ") {";
  protected final String TEXT_396 = NL + "                } else if (";
  protected final String TEXT_397 = " < ";
  protected final String TEXT_398 = ") {";
  protected final String TEXT_399 = NL + "                }";
  protected final String TEXT_400 = NL + "                int cmp_";
  protected final String TEXT_401 = " = String.valueOf(";
  protected final String TEXT_402 = ").compareTo(String.valueOf(";
  protected final String TEXT_403 = "));" + NL + "                if (cmp_";
  protected final String TEXT_404 = " > 0) {";
  protected final String TEXT_405 = NL + "                } else if (cmp_";
  protected final String TEXT_406 = " < 0) {";
  protected final String TEXT_407 = NL + "                }";
  protected final String TEXT_408 = NL + "                if (";
  protected final String TEXT_409 = " > ";
  protected final String TEXT_410 = ") {";
  protected final String TEXT_411 = NL + "                } else if (";
  protected final String TEXT_412 = " < ";
  protected final String TEXT_413 = ") {";
  protected final String TEXT_414 = NL + "                }";
  protected final String TEXT_415 = NL + "            int comp_";
  protected final String TEXT_416 = " = ";
  protected final String TEXT_417 = ".compareTo(";
  protected final String TEXT_418 = ");" + NL + "            if (comp_";
  protected final String TEXT_419 = " != 0) {" + NL + "                if (comp_";
  protected final String TEXT_420 = " > 0) {";
  protected final String TEXT_421 = NL + "                } else {";
  protected final String TEXT_422 = NL + "                }" + NL + "" + NL + "            }";
  protected final String TEXT_423 = NL + "            throw new JobConfigurationError(\"The ";
  protected final String TEXT_424 = " type is not supported: column--";
  protected final String TEXT_425 = "\");";
  protected final String TEXT_426 = NL + "            }";
  protected final String TEXT_427 = NL + "\t";

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    
if(org.talend.designer.common.tmap.LookupUtil.isNodeInBatchMode((INode) ((BigDataCodeGeneratorArgument) argument).getArgument())) {

    stringBuffer.append(TEXT_1);
    

final BigDataCodeGeneratorArgument codeGenArgument = (BigDataCodeGeneratorArgument) argument;
final INode node = (INode) codeGenArgument.getArgument();
final IBigDataNode bigDataNode = (IBigDataNode) codeGenArgument.getArgument();
final String cid = node.getUniqueName();

    
	/**
	 * Code generated for component with single output
 	 */
    class SOFunctionGenerator extends org.talend.designer.spark.generator.FunctionGenerator{

    	SOFunctionGenerator(org.talend.designer.common.TransformerBase transformer) {
            super(transformer);
        }

    	SOFunctionGenerator(org.talend.designer.common.TransformerBase transformer, org.talend.designer.spark.generator.SparkFunction sparkFunction) {
    		super(transformer, sparkFunction);
    	}

    	@Override
        public void generate(){
		
    stringBuffer.append(TEXT_2);
    stringBuffer.append(this.sparkFunction.getClassName(cid));
    stringBuffer.append(TEXT_3);
    stringBuffer.append(this.sparkFunction.getCodeFunctionImplementation(getOutValueClass(), getInValueClass()));
    stringBuffer.append(TEXT_4);
    
                this.transformer.generateHelperClasses(true);
                this.transformer.generateTransformContextDeclaration();
                
    stringBuffer.append(TEXT_5);
    stringBuffer.append(this.sparkFunction.getClassName(cid));
    stringBuffer.append(TEXT_6);
    
                    this.transformer.generateTransformContextDeclarationInConstructor();
                    
    stringBuffer.append(TEXT_7);
    stringBuffer.append(this.sparkFunction.getCodeFunctionReturnedType(this.outValueClass.toString()));
    stringBuffer.append(TEXT_8);
    stringBuffer.append(this.sparkFunction.getCodeImplementedMethod());
    stringBuffer.append(TEXT_9);
    stringBuffer.append(this.sparkFunction.getCodeFunctionArgument(getInValueClass()));
    stringBuffer.append(TEXT_10);
    stringBuffer.append(this.sparkFunction.getCodeThrowException());
    stringBuffer.append(TEXT_11);
    stringBuffer.append(this.sparkFunction.getMethodHeader(this.outValueClass, this.outValue, this.inValueClass, this.inValue));
    stringBuffer.append(TEXT_12);
    stringBuffer.append(this.sparkFunction.getCodeKeyMapping(getInValue()));
    stringBuffer.append(TEXT_13);
    
	                this.transformer.generateTransformContextInitialization();
	                this.transformer.generateTransform(true);
	                
    stringBuffer.append(TEXT_14);
    stringBuffer.append(this.sparkFunction.getCodeFunctionReturn(this.getOutValue(), this.getOutValueClass()));
    stringBuffer.append(TEXT_15);
    
        }
    }

	/**
	 * Code generated for component with multiple outputs
 	 */
    class MOFunctionGenerator extends org.talend.designer.spark.generator.FunctionGenerator{

        /** The single connection to pass along the output chain of Mappers
         *  instead of sending to a dedicated collector via MultipleOutputs. */
        String connectionToChain = null;

        MOFunctionGenerator(org.talend.designer.common.TransformerBase transformer) {
            super(transformer);
            defaultOutKeyClass = "Boolean";
        }

    	MOFunctionGenerator(org.talend.designer.common.TransformerBase transformer, org.talend.designer.spark.generator.SparkFunction sparkFunction) {
    		super(transformer, sparkFunction);
            defaultOutKeyClass = "Boolean";
    	}

    	@Override
        public void generate(){
        
    stringBuffer.append(TEXT_16);
    stringBuffer.append(this.sparkFunction.getClassName(cid));
    stringBuffer.append(TEXT_17);
    stringBuffer.append(this.sparkFunction.getCodeFunctionImplementationOutputFixedType(getInValueClass(), "Boolean", "org.apache.avro.specific.SpecificRecordBase"));
    stringBuffer.append(TEXT_18);
    
                this.transformer.generateHelperClasses(true);
                this.transformer.generateTransformContextDeclaration();
                
    stringBuffer.append(TEXT_19);
    stringBuffer.append(this.sparkFunction.getClassName(cid));
    stringBuffer.append(TEXT_20);
    stringBuffer.append(this.sparkFunction.getCodeFunctionReturnedTypeFixedType((String)this.outKeyClass, "org.apache.avro.specific.SpecificRecordBase"));
    stringBuffer.append(TEXT_21);
    stringBuffer.append(this.sparkFunction.getCodeImplementedMethod());
    stringBuffer.append(TEXT_22);
    stringBuffer.append(this.sparkFunction.getCodeFunctionArgument(getInValueClass()));
    stringBuffer.append(TEXT_23);
    stringBuffer.append(this.sparkFunction.getCodeThrowException());
    stringBuffer.append(TEXT_24);
    stringBuffer.append(this.sparkFunction.getMethodHeader(this.outValueClass, this.outValue, this.inValueClass, this.inValue));
    
                    this.transformer.generateTransformContextInitialization();
                    this.transformer.generateTransform(true);
	                if(this.sparkFunction.getCodeFunctionReturn()!=null) {
                
    stringBuffer.append(TEXT_25);
    stringBuffer.append(this.sparkFunction.getCodeFunctionReturn());
    stringBuffer.append(TEXT_26);
    
	            	}
                
    stringBuffer.append(TEXT_27);
    
        }
    }
    
    /**
     * Code generated for tDataprepRun component
     */
    class DataprepFunctionGenerator extends org.talend.designer.spark.generator.FunctionGenerator {

        DataprepFunctionGenerator(org.talend.designer.common.TransformerBase transformer) {
            super(transformer);
        }

        DataprepFunctionGenerator(org.talend.designer.common.TransformerBase transformer, org.talend.designer.spark.generator.SparkFunction sparkFunction) {
            super(transformer, sparkFunction);
        }

        @Override
        public void generate(){
        
    stringBuffer.append(TEXT_28);
    stringBuffer.append(this.sparkFunction.getClassName(cid));
    stringBuffer.append(TEXT_29);
    stringBuffer.append(this.sparkFunction.getCodeFunctionImplementation(getOutValueClass(), getInValueClass()));
    stringBuffer.append(TEXT_30);
    
                this.transformer.generateHelperClasses(true);
                this.transformer.generateTransformContextDeclaration();
                
    stringBuffer.append(TEXT_31);
    stringBuffer.append(this.sparkFunction.getClassName(cid));
    stringBuffer.append(TEXT_32);
    stringBuffer.append(this.sparkFunction.getCodeFunctionReturnedType(this.outValueClass.toString()));
    stringBuffer.append(TEXT_33);
    stringBuffer.append(this.sparkFunction.getCodeImplementedMethod());
    stringBuffer.append(TEXT_34);
    stringBuffer.append(this.sparkFunction.getCodeFunctionArgument(getInValueClass()));
    stringBuffer.append(TEXT_35);
    stringBuffer.append(this.sparkFunction.getCodeThrowException());
    stringBuffer.append(TEXT_36);
    stringBuffer.append(TEXT_37);
    stringBuffer.append(this.sparkFunction.getMethodHeader(this.outValueClass, this.outValue, this.inValueClass, this.inValue));
    stringBuffer.append(TEXT_38);
    stringBuffer.append(this.sparkFunction.getCodeKeyMapping(getInValue()));
    
                        this.transformer.generateTransformContextInitialization();
                        this.transformer.generateTransform(true);
                     
    stringBuffer.append(TEXT_39);
    stringBuffer.append(this.sparkFunction.getCodeFunctionReturn(this.getOutValue(), this.getOutValueClass()));
    stringBuffer.append(TEXT_40);
    
        }
    }

    stringBuffer.append(TEXT_41);
    

/**
 * A common, reusable utility that generates code in the context of a spark
 * component, for reading and writing to a spark RDD.
 */
class SparkRowTransformUtil extends org.talend.designer.common.CommonRowTransformUtil {

    private boolean isMultiOutput = false;

    private org.talend.designer.spark.generator.SparkFunction sparkFunction = null;
    

    private org.talend.designer.spark.generator.FunctionGenerator functionGenerator = null;

    public SparkRowTransformUtil() {

    }

    public SparkRowTransformUtil(org.talend.designer.spark.generator.SparkFunction sparkFunction) {
        this.sparkFunction = sparkFunction;
    }

    public void setMultiOutput(boolean multiOutput) {
        isMultiOutput = multiOutput;
    }

    public String getCodeToGetInField(String columnName) {
        return functionGenerator.getInValue() + "." + columnName;
    }

    public String getInValue() {
        return functionGenerator.getInValue();
    }

    public String getOutValue() {
        return functionGenerator.getOutValue();
    }

    public String getInValueClass() {
        return functionGenerator.getInValueClass();
    }

    public String getOutValueClass() {
        return functionGenerator.getOutValueClass();
    }

    public String getCodeToGetOutField(boolean isReject, String columnName) {
        return functionGenerator.getOutValue(isReject ? "reject" : "main") + "." + columnName;
    }

    public String getCodeToInitOut(boolean isReject, Iterable<IMetadataColumn> columns) {
        if(this.sparkFunction!=null && !isMultiOutput) {
            return this.sparkFunction.getCodeToInitOut(functionGenerator.getOutValue(isReject ? "reject" : "main"), functionGenerator.getOutValueClass(isReject ? "reject" : "main"));
        } else {
            return "";
        }
    }

    public String getCodeToSetOutField(boolean isReject, String columnName, String codeValue) {
        return functionGenerator.getOutValue(isReject ? "reject" : "main") + "." + columnName + " = " + codeValue + ";";
    }

    public String getCodeToSetOutField(boolean isReject, String columnName, String codeValue, String operator) {
        return functionGenerator.getOutValue(isReject ? "reject" : "main") + "." + columnName + " " + operator + " " + codeValue + ";";
    }

    public String getCodeToEmit(boolean isReject) {
        if (this.sparkFunction!=null && isMultiOutput) {
            if (isReject) {
                return this.sparkFunction.getCodeToEmit(false, functionGenerator.getOutValue("reject"), functionGenerator.getOutValueClass("reject"));
            } else {
                return this.sparkFunction.getCodeToEmit(true, functionGenerator.getOutValue("main"), functionGenerator.getOutValueClass("main"));
            }
        } else {
            return "";
        }
    }

    public void generateSparkCode(final org.talend.designer.common.TransformerBase transformer, final org.talend.designer.spark.generator.SparkFunction sparkFunction) {
        if (transformer.isMultiOutput()) {
            setMultiOutput(true);
        }
        if (transformer.isUnnecessary()) {
            
    stringBuffer.append(TEXT_42);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_43);
    
            return;
        }

        // Refactoring FunctionGenerator to java so we have to instaniate a MO or SO here
        functionGenerator = new SOFunctionGenerator(transformer, sparkFunction);

        functionGenerator.init(node, cid, null, transformer.getInConnName(), null, 
                transformer.getOutConnMainName() != null
                    ? transformer.getOutConnMainName()
                            : transformer.getOutConnRejectName());

        functionGenerator.generate();
    }

    public void generateSparkConfig(final org.talend.designer.common.TransformerBase transformer, final org.talend.designer.spark.generator.SparkFunction sparkFunction, String topX) {
        if (transformer.isUnnecessary()) {
            
    stringBuffer.append(TEXT_44);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_45);
    
            return;
        }

        functionGenerator = new SOFunctionGenerator(transformer, sparkFunction);

        functionGenerator.init(node, cid, null, transformer.getInConnName(), null, 
                transformer.getOutConnMainName() != null
                    ? transformer.getOutConnMainName()
                            : transformer.getOutConnRejectName());
        
    stringBuffer.append(TEXT_46);
    stringBuffer.append(sparkFunction.getConfigReturnedType(functionGenerator.getOutValueClass()));
    stringBuffer.append(TEXT_47);
    stringBuffer.append(transformer.getOutConnMainName());
    stringBuffer.append(TEXT_48);
    stringBuffer.append(transformer.getInConnName());
    stringBuffer.append(TEXT_49);
    stringBuffer.append(sparkFunction.getConfigTransformation());
    stringBuffer.append(TEXT_50);
    stringBuffer.append(topX);
    stringBuffer.append(TEXT_51);
    stringBuffer.append(sparkFunction.getClassName(cid));
    stringBuffer.append(TEXT_52);
    
    }
}


    

/**
 * Contains common processing for tSortRow code generation.
 */
class TSortRowUtil extends org.talend.designer.common.TransformerBase {

    /** The list of columns that should be copied directly from the input to
     *  the output schema (where they have the same column names). */
    final private Iterable<IMetadataColumn> copiedInColumns;

    /** When generating a comparator, the code accessor for the left side. */
    private final String codeVarData1;

    /** When generating a comparator, the code accessor for the right side. */
    private final String codeVarData2;

    java.util.List<java.util.Map<String, String>> criterias = (java.util.List<java.util.Map<String,String>>)ElementParameterParser.getObjectValue(node, "__CRITERIA__");
    java.util.List<String> listCols = new java.util.ArrayList<String>();

    /** Contains ONLY those columns that are ascending. */
    java.util.Set<String> criteriasAscendingColumns = new java.util.HashSet<String>();

    java.util.Map<String, Integer> criteriasSortType = new java.util.HashMap<String, Integer>();
    java.util.Map<String, Boolean> sortTypes = new java.util.HashMap<String, Boolean>();

    final java.util.Map<String, java.util.List<org.talend.core.model.metadata.IMetadataColumn>> keyList;

    final Integer SORT_NUM = 0;
    final Integer SORT_ALPHA = 1;
    final Integer SORT_DATE = 2;

    public TSortRowUtil(INode node,
            org.talend.designer.common.BigDataCodeGeneratorArgument argument,
            org.talend.designer.common.CommonRowTransformUtil rowTransformUtil) {
        this(node, argument, rowTransformUtil, "data1", "data2", false);
    }

    /**
     * @param invert if true, changes all ascending columns to descending
     *        columns and vice versa.
     */
    protected TSortRowUtil(INode node,
            org.talend.designer.common.BigDataCodeGeneratorArgument argument,
            org.talend.designer.common.CommonRowTransformUtil rowTransformUtil,
            String codeVarData1, String codeVarData2, boolean invert) {
        super(node, argument, rowTransformUtil, "FLOW", "REJECT");

        keyList = ((IBigDataNode) node).getKeyList();
        this.codeVarData1 = codeVarData1;
        this.codeVarData2 = codeVarData2;

        if (null != getInConn() && null != getOutConnMain()) {
            copiedInColumns = org.talend.designer.common.TransformerBaseUtil.getColumnsUnion(getInColumns(), getOutColumnsMain());
        } else {
            copiedInColumns = null;
        }

        for(int i = 0; i < criterias.size(); i++) {
            java.util.Map<String, String> line = criterias.get(i);
            String colname = line.get("COLNAME");
            if (listCols.contains(colname)) {
                continue;//skip dipulicate
            }
            listCols.add(colname);

            if ("asc".equals(line.get("ORDER")) == !invert) {
                criteriasAscendingColumns.add(colname);
            }

            if (("num").equals(line.get("SORT"))) {
                criteriasSortType.put(colname, SORT_NUM);
                sortTypes.put(colname, true);
            } else if (("alpha").equals(line.get("SORT"))) {
                sortTypes.put(colname, false);
            } else {
                criteriasSortType.put(colname, SORT_DATE);
                sortTypes.put(colname, true);
            }
        }
    }

    public void generateTransformContextDeclaration() {
        // Nothing here
    }

    /**
     * Generates code that performs the tranformation from one input string
     * to many output strings, or to a reject flow.
     */
    public void generateTransform() {
        generateTransform(true);
    }

    /**
     * Generates code that performs the tranformation from one input string
     * to many output strings, or to a reject flow. The boolean parameter is
     * used to define whether the transform method return type is void or something
     * else.
     */
    public void generateTransform(boolean hasAReturnedType) {
        int index = 0;
        for(String col:listCols) {
            for(IMetadataColumn column : copiedInColumns) {
                if (col.equals(column.getLabel())) {
                    generateComparatorSnippetForColumn(column, index,
                        org.talend.designer.spark.generator.utils.SparkFunctionUtil.getKeyValueAccessor(
                            keyList, "BOTH", codeVarData1, index),
                        org.talend.designer.spark.generator.utils.SparkFunctionUtil.getKeyValueAccessor(
                            keyList, "BOTH", codeVarData2, index));
                    index++;
                    break;
                }
            }
        }
    }

    /**
     * Generates code in the transform context to create reject output.
     *
     * @param die if this reject output should kill the job.  Normally, this is
     *    tied to a dieOnError parameter for the component, but can be
     *    explicitly set to false for non-fatal reject output.
     * @param codeException a variable in the transform scope that contains the
     *    variable with an exception.  If null, this will be constructed from
     *    the codeRejectMsg.
     * @param codeRejectMsg the error message to output with the reject output.
     */
    private void generateTransformReject(boolean die, String codeException, String codeRejectMsg) {

    }

    protected void greater(String columnName) {
        if (criteriasAscendingColumns.contains(columnName)) {
            
    stringBuffer.append(TEXT_53);
    
        } else {
            
    stringBuffer.append(TEXT_54);
    
        }
    }

    protected void lesser(String columnName) {
        if (criteriasAscendingColumns.contains(columnName)) {
            
    stringBuffer.append(TEXT_55);
    
        } else {
            
    stringBuffer.append(TEXT_56);
    
        }
    }

    private void generateComparatorSnippetForColumn(IMetadataColumn column,
                int i, String codeData1, String codeData2) {
        boolean nullable = !JavaTypesManager.isJavaPrimitiveType(column.getTalendType(), column.isNullable());
        String typeToGenerate = JavaTypesManager.getTypeToGenerate(column.getTalendType(), column.isNullable());
        String pattern = column.getPattern() == null || column.getPattern().trim().length() == 0 ? null : column.getPattern();
        String columnName = column.getLabel();

        // For nullable columns, we always generate a comparison that sorts
        // nulls before non-null.
        if (nullable) {
            
    stringBuffer.append(TEXT_57);
    stringBuffer.append(codeData1);
    stringBuffer.append(TEXT_58);
    stringBuffer.append(codeData2);
    stringBuffer.append(TEXT_59);
    lesser(columnName);
    stringBuffer.append(TEXT_60);
    stringBuffer.append(codeData1);
    stringBuffer.append(TEXT_61);
    stringBuffer.append(codeData2);
    stringBuffer.append(TEXT_62);
    greater(columnName);
    stringBuffer.append(TEXT_63);
    stringBuffer.append(codeData1);
    stringBuffer.append(TEXT_64);
    stringBuffer.append(codeData2);
    stringBuffer.append(TEXT_65);
    
            // Note that the nullable else case is left open here and closed
            // at the end.
        }

        if (typeToGenerate.equalsIgnoreCase("Boolean")) {
            
    stringBuffer.append(TEXT_66);
    stringBuffer.append(codeData1);
    stringBuffer.append(TEXT_67);
    stringBuffer.append(codeData2);
    stringBuffer.append(TEXT_68);
    stringBuffer.append(codeData1);
    stringBuffer.append(TEXT_69);
    greater(columnName);
    stringBuffer.append(TEXT_70);
    lesser(columnName);
    stringBuffer.append(TEXT_71);
    
        } else if (typeToGenerate.equalsIgnoreCase("Byte")) {
            
    stringBuffer.append(TEXT_72);
    stringBuffer.append(codeData1);
    stringBuffer.append(TEXT_73);
    stringBuffer.append(codeData2);
    stringBuffer.append(TEXT_74);
    greater(columnName);
    stringBuffer.append(TEXT_75);
    stringBuffer.append(codeData1);
    stringBuffer.append(TEXT_76);
    stringBuffer.append(codeData2);
    stringBuffer.append(TEXT_77);
    lesser(columnName);
    stringBuffer.append(TEXT_78);
    
        } else if (typeToGenerate.equals("byte[]")) {
            
    stringBuffer.append(TEXT_79);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_80);
    stringBuffer.append(codeData1);
    stringBuffer.append(TEXT_81);
    stringBuffer.append(codeData2);
    stringBuffer.append(TEXT_82);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_83);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_84);
    greater(columnName);
    stringBuffer.append(TEXT_85);
    lesser(columnName);
    stringBuffer.append(TEXT_86);
    
        } else if (typeToGenerate.equalsIgnoreCase("Char") || typeToGenerate.equalsIgnoreCase("Character")) {
            
    stringBuffer.append(TEXT_87);
    stringBuffer.append(codeData1);
    stringBuffer.append(TEXT_88);
    stringBuffer.append(codeData2);
    stringBuffer.append(TEXT_89);
    stringBuffer.append(codeData1);
    stringBuffer.append(TEXT_90);
    stringBuffer.append(codeData2);
    stringBuffer.append(TEXT_91);
    greater(columnName);
    stringBuffer.append(TEXT_92);
    lesser(columnName);
    stringBuffer.append(TEXT_93);
    
        } else if (typeToGenerate.equals("java.util.Date")) {
            if (!sortTypes.get(columnName)) {
                
    stringBuffer.append(TEXT_94);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_95);
    stringBuffer.append(codeData1);
    stringBuffer.append(TEXT_96);
    stringBuffer.append(pattern);
    stringBuffer.append(TEXT_97);
    stringBuffer.append(codeData2);
    stringBuffer.append(TEXT_98);
    stringBuffer.append(pattern);
    stringBuffer.append(TEXT_99);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_100);
    greater(columnName);
    stringBuffer.append(TEXT_101);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_102);
    lesser(columnName);
    stringBuffer.append(TEXT_103);
    
            } else {
                
    stringBuffer.append(TEXT_104);
    stringBuffer.append(codeData1);
    stringBuffer.append(TEXT_105);
    stringBuffer.append(codeData2);
    stringBuffer.append(TEXT_106);
    stringBuffer.append(codeData1);
    stringBuffer.append(TEXT_107);
    stringBuffer.append(codeData2);
    stringBuffer.append(TEXT_108);
    greater(columnName);
    stringBuffer.append(TEXT_109);
    lesser(columnName);
    stringBuffer.append(TEXT_110);
    
            }
        } else if (typeToGenerate.equalsIgnoreCase("Double")) {
            if (!sortTypes.get(columnName)) {
                
    stringBuffer.append(TEXT_111);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_112);
    stringBuffer.append(codeData1);
    stringBuffer.append(TEXT_113);
    stringBuffer.append(codeData2);
    stringBuffer.append(TEXT_114);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_115);
    greater(columnName);
    stringBuffer.append(TEXT_116);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_117);
    lesser(columnName);
    stringBuffer.append(TEXT_118);
    
            } else {
                
    stringBuffer.append(TEXT_119);
    stringBuffer.append(codeData1);
    stringBuffer.append(TEXT_120);
    stringBuffer.append(codeData2);
    stringBuffer.append(TEXT_121);
    greater(columnName);
    stringBuffer.append(TEXT_122);
    stringBuffer.append(codeData1);
    stringBuffer.append(TEXT_123);
    stringBuffer.append(codeData2);
    stringBuffer.append(TEXT_124);
    lesser(columnName);
    stringBuffer.append(TEXT_125);
    
            }
        } else if (typeToGenerate.equalsIgnoreCase("Float")) {
            if (!sortTypes.get(columnName)) {
                
    stringBuffer.append(TEXT_126);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_127);
    stringBuffer.append(codeData1);
    stringBuffer.append(TEXT_128);
    stringBuffer.append(codeData2);
    stringBuffer.append(TEXT_129);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_130);
    greater(columnName);
    stringBuffer.append(TEXT_131);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_132);
    lesser(columnName);
    stringBuffer.append(TEXT_133);
    
            } else {
                
    stringBuffer.append(TEXT_134);
    stringBuffer.append(codeData1);
    stringBuffer.append(TEXT_135);
    stringBuffer.append(codeData2);
    stringBuffer.append(TEXT_136);
    greater(columnName);
    stringBuffer.append(TEXT_137);
    stringBuffer.append(codeData1);
    stringBuffer.append(TEXT_138);
    stringBuffer.append(codeData2);
    stringBuffer.append(TEXT_139);
    lesser(columnName);
    stringBuffer.append(TEXT_140);
    
            }
        } else if (typeToGenerate.equals("BigDecimal")) {
            if (!sortTypes.get(columnName)) {
                
    stringBuffer.append(TEXT_141);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_142);
    stringBuffer.append(codeData1);
    stringBuffer.append(TEXT_143);
    stringBuffer.append(codeData2);
    stringBuffer.append(TEXT_144);
    
            } else {
                
    stringBuffer.append(TEXT_145);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_146);
    stringBuffer.append(codeData1);
    stringBuffer.append(TEXT_147);
    stringBuffer.append(codeData2);
    stringBuffer.append(TEXT_148);
    }
    stringBuffer.append(TEXT_149);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_150);
    greater(columnName);
    stringBuffer.append(TEXT_151);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_152);
    lesser(columnName);
    stringBuffer.append(TEXT_153);
    
        } else if (typeToGenerate.equalsIgnoreCase("Integer") || typeToGenerate.equalsIgnoreCase("int")) {
            if (!sortTypes.get(columnName)) {
                
    stringBuffer.append(TEXT_154);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_155);
    stringBuffer.append(codeData1);
    stringBuffer.append(TEXT_156);
    stringBuffer.append(codeData2);
    stringBuffer.append(TEXT_157);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_158);
    greater(columnName);
    stringBuffer.append(TEXT_159);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_160);
    lesser(columnName);
    stringBuffer.append(TEXT_161);
    
            } else {
                
    stringBuffer.append(TEXT_162);
    stringBuffer.append(codeData1);
    stringBuffer.append(TEXT_163);
    stringBuffer.append(codeData2);
    stringBuffer.append(TEXT_164);
    greater(columnName);
    stringBuffer.append(TEXT_165);
    stringBuffer.append(codeData1);
    stringBuffer.append(TEXT_166);
    stringBuffer.append(codeData2);
    stringBuffer.append(TEXT_167);
    lesser(columnName);
    stringBuffer.append(TEXT_168);
    
            }
        } else if (typeToGenerate.equalsIgnoreCase("Long")) {
            if (!sortTypes.get(columnName)) {
                
    stringBuffer.append(TEXT_169);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_170);
    stringBuffer.append(codeData1);
    stringBuffer.append(TEXT_171);
    stringBuffer.append(codeData2);
    stringBuffer.append(TEXT_172);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_173);
    greater(columnName);
    stringBuffer.append(TEXT_174);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_175);
    lesser(columnName);
    stringBuffer.append(TEXT_176);
    
            } else {
                
    stringBuffer.append(TEXT_177);
    stringBuffer.append(codeData1);
    stringBuffer.append(TEXT_178);
    stringBuffer.append(codeData2);
    stringBuffer.append(TEXT_179);
    greater(columnName);
    stringBuffer.append(TEXT_180);
    stringBuffer.append(codeData1);
    stringBuffer.append(TEXT_181);
    stringBuffer.append(codeData2);
    stringBuffer.append(TEXT_182);
    lesser(columnName);
    stringBuffer.append(TEXT_183);
    
            }
        } else if (typeToGenerate.equalsIgnoreCase("Short")) {
            if (!sortTypes.get(columnName)) {
                
    stringBuffer.append(TEXT_184);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_185);
    stringBuffer.append(codeData1);
    stringBuffer.append(TEXT_186);
    stringBuffer.append(codeData2);
    stringBuffer.append(TEXT_187);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_188);
    greater(columnName);
    stringBuffer.append(TEXT_189);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_190);
    lesser(columnName);
    stringBuffer.append(TEXT_191);
    
            } else {
                
    stringBuffer.append(TEXT_192);
    stringBuffer.append(codeData1);
    stringBuffer.append(TEXT_193);
    stringBuffer.append(codeData2);
    stringBuffer.append(TEXT_194);
    greater(columnName);
    stringBuffer.append(TEXT_195);
    stringBuffer.append(codeData1);
    stringBuffer.append(TEXT_196);
    stringBuffer.append(codeData2);
    stringBuffer.append(TEXT_197);
    lesser(columnName);
    stringBuffer.append(TEXT_198);
    
            }
        } else if (typeToGenerate.equals("String")) {
            
    stringBuffer.append(TEXT_199);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_200);
    stringBuffer.append(codeData1);
    stringBuffer.append(TEXT_201);
    stringBuffer.append(codeData2);
    stringBuffer.append(TEXT_202);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_203);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_204);
    greater(columnName);
    stringBuffer.append(TEXT_205);
    lesser(columnName);
    stringBuffer.append(TEXT_206);
    
        } else if (typeToGenerate.equals("Object")
                || typeToGenerate.equals("List")
                || typeToGenerate.equals("Document")
                || typeToGenerate.equals("Dynamic")) {
            
    stringBuffer.append(TEXT_207);
    stringBuffer.append(typeToGenerate);
    stringBuffer.append(TEXT_208);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_209);
    
        }

        // Close the open else statement for nullable columns.
        if (nullable) {
            
    stringBuffer.append(TEXT_210);
    
        }
    }
}

    

/**
 * Contains common processing for tTop code generation.
 */
class TTopUtil extends TSortRowUtil {

    public TTopUtil(INode node,
            org.talend.designer.common.BigDataCodeGeneratorArgument argument,
            org.talend.designer.common.CommonRowTransformUtil rowTransformUtil) {
        super(node, argument, rowTransformUtil, "data1._1", "data2._1", true);
    }
}

    
final SparkRowTransformUtil sparkTransformUtil = new SparkRowTransformUtil();
final TTopUtil tTopUtil = new TTopUtil(node, codeGenArgument, sparkTransformUtil);

java.util.Map<String, java.util.List<org.talend.core.model.metadata.IMetadataColumn>> keyList = bigDataNode.getKeyList();
org.talend.designer.spark.generator.SparkFunction sparkFunction = new org.talend.designer.spark.generator.TopFunction(false, keyList);

sparkTransformUtil.generateSparkCode(tTopUtil, sparkFunction);

    
} else {
	final BigDataCodeGeneratorArgument codeGenArgument = (BigDataCodeGeneratorArgument) argument;
	final INode node = (INode) codeGenArgument.getArgument();
	final IBigDataNode bigDataNode = (IBigDataNode) codeGenArgument.getArgument();
	final String cid = node.getUniqueName();
	
    stringBuffer.append(TEXT_211);
    
	/**
	 * Code generated for component with single output
 	 */
    class SOFunctionGenerator extends org.talend.designer.spark.generator.FunctionGenerator{

    	SOFunctionGenerator(org.talend.designer.common.TransformerBase transformer) {
            super(transformer);
        }

    	SOFunctionGenerator(org.talend.designer.common.TransformerBase transformer, org.talend.designer.spark.generator.SparkFunction sparkFunction) {
    		super(transformer, sparkFunction);
    	}

    	@Override
        public void generate(){
		
    stringBuffer.append(TEXT_212);
    stringBuffer.append(this.sparkFunction.getClassName(cid));
    stringBuffer.append(TEXT_213);
    stringBuffer.append(this.sparkFunction.getCodeFunctionImplementation(getOutValueClass(), getInValueClass()));
    stringBuffer.append(TEXT_214);
    
                this.transformer.generateHelperClasses(true);
                this.transformer.generateTransformContextDeclaration();
                
    stringBuffer.append(TEXT_215);
    stringBuffer.append(this.sparkFunction.getClassName(cid));
    stringBuffer.append(TEXT_216);
    
                    this.transformer.generateTransformContextDeclarationInConstructor();
                    
    stringBuffer.append(TEXT_217);
    stringBuffer.append(this.sparkFunction.getCodeFunctionReturnedType(this.outValueClass.toString()));
    stringBuffer.append(TEXT_218);
    stringBuffer.append(this.sparkFunction.getCodeImplementedMethod());
    stringBuffer.append(TEXT_219);
    stringBuffer.append(this.sparkFunction.getCodeFunctionArgument(getInValueClass()));
    stringBuffer.append(TEXT_220);
    stringBuffer.append(this.sparkFunction.getCodeThrowException());
    stringBuffer.append(TEXT_221);
    stringBuffer.append(this.sparkFunction.getMethodHeader(this.outValueClass, this.outValue, this.inValueClass, this.inValue));
    stringBuffer.append(TEXT_222);
    stringBuffer.append(this.sparkFunction.getCodeKeyMapping(getInValue()));
    stringBuffer.append(TEXT_223);
    
	                this.transformer.generateTransformContextInitialization();
	                this.transformer.generateTransform(true);
	                
    stringBuffer.append(TEXT_224);
    stringBuffer.append(this.sparkFunction.getCodeFunctionReturn(this.getOutValue(), this.getOutValueClass()));
    stringBuffer.append(TEXT_225);
    
        }
    }

	/**
	 * Code generated for component with multiple outputs
 	 */
    class MOFunctionGenerator extends org.talend.designer.spark.generator.FunctionGenerator{

        /** The single connection to pass along the output chain of Mappers
         *  instead of sending to a dedicated collector via MultipleOutputs. */
        String connectionToChain = null;

        MOFunctionGenerator(org.talend.designer.common.TransformerBase transformer) {
            super(transformer);
            defaultOutKeyClass = "Boolean";
        }

    	MOFunctionGenerator(org.talend.designer.common.TransformerBase transformer, org.talend.designer.spark.generator.SparkFunction sparkFunction) {
    		super(transformer, sparkFunction);
            defaultOutKeyClass = "Boolean";
    	}

    	@Override
        public void generate(){
        
    stringBuffer.append(TEXT_226);
    stringBuffer.append(this.sparkFunction.getClassName(cid));
    stringBuffer.append(TEXT_227);
    stringBuffer.append(this.sparkFunction.getCodeFunctionImplementationOutputFixedType(getInValueClass(), "Boolean", "org.apache.avro.specific.SpecificRecordBase"));
    stringBuffer.append(TEXT_228);
    
                this.transformer.generateHelperClasses(true);
                this.transformer.generateTransformContextDeclaration();
                
    stringBuffer.append(TEXT_229);
    stringBuffer.append(this.sparkFunction.getClassName(cid));
    stringBuffer.append(TEXT_230);
    stringBuffer.append(this.sparkFunction.getCodeFunctionReturnedTypeFixedType((String)this.outKeyClass, "org.apache.avro.specific.SpecificRecordBase"));
    stringBuffer.append(TEXT_231);
    stringBuffer.append(this.sparkFunction.getCodeImplementedMethod());
    stringBuffer.append(TEXT_232);
    stringBuffer.append(this.sparkFunction.getCodeFunctionArgument(getInValueClass()));
    stringBuffer.append(TEXT_233);
    stringBuffer.append(this.sparkFunction.getCodeThrowException());
    stringBuffer.append(TEXT_234);
    stringBuffer.append(this.sparkFunction.getMethodHeader(this.outValueClass, this.outValue, this.inValueClass, this.inValue));
    
                    this.transformer.generateTransformContextInitialization();
                    this.transformer.generateTransform(true);
	                if(this.sparkFunction.getCodeFunctionReturn()!=null) {
                
    stringBuffer.append(TEXT_235);
    stringBuffer.append(this.sparkFunction.getCodeFunctionReturn());
    stringBuffer.append(TEXT_236);
    
	            	}
                
    stringBuffer.append(TEXT_237);
    
        }
    }
    
    /**
     * Code generated for tDataprepRun component
     */
    class DataprepFunctionGenerator extends org.talend.designer.spark.generator.FunctionGenerator {

        DataprepFunctionGenerator(org.talend.designer.common.TransformerBase transformer) {
            super(transformer);
        }

        DataprepFunctionGenerator(org.talend.designer.common.TransformerBase transformer, org.talend.designer.spark.generator.SparkFunction sparkFunction) {
            super(transformer, sparkFunction);
        }

        @Override
        public void generate(){
        
    stringBuffer.append(TEXT_238);
    stringBuffer.append(this.sparkFunction.getClassName(cid));
    stringBuffer.append(TEXT_239);
    stringBuffer.append(this.sparkFunction.getCodeFunctionImplementation(getOutValueClass(), getInValueClass()));
    stringBuffer.append(TEXT_240);
    
                this.transformer.generateHelperClasses(true);
                this.transformer.generateTransformContextDeclaration();
                
    stringBuffer.append(TEXT_241);
    stringBuffer.append(this.sparkFunction.getClassName(cid));
    stringBuffer.append(TEXT_242);
    stringBuffer.append(this.sparkFunction.getCodeFunctionReturnedType(this.outValueClass.toString()));
    stringBuffer.append(TEXT_243);
    stringBuffer.append(this.sparkFunction.getCodeImplementedMethod());
    stringBuffer.append(TEXT_244);
    stringBuffer.append(this.sparkFunction.getCodeFunctionArgument(getInValueClass()));
    stringBuffer.append(TEXT_245);
    stringBuffer.append(this.sparkFunction.getCodeThrowException());
    stringBuffer.append(TEXT_246);
    stringBuffer.append(TEXT_247);
    stringBuffer.append(this.sparkFunction.getMethodHeader(this.outValueClass, this.outValue, this.inValueClass, this.inValue));
    stringBuffer.append(TEXT_248);
    stringBuffer.append(this.sparkFunction.getCodeKeyMapping(getInValue()));
    
                        this.transformer.generateTransformContextInitialization();
                        this.transformer.generateTransform(true);
                     
    stringBuffer.append(TEXT_249);
    stringBuffer.append(this.sparkFunction.getCodeFunctionReturn(this.getOutValue(), this.getOutValueClass()));
    stringBuffer.append(TEXT_250);
    
        }
    }

    stringBuffer.append(TEXT_251);
    

/**
 * A common, reusable utility that generates code in the context of a spark
 * component, for reading and writing to a spark RDD.
 */
class SparkRowTransformUtil extends org.talend.designer.common.CommonRowTransformUtil {

    private boolean isMultiOutput = false;

    private org.talend.designer.spark.generator.SparkFunction sparkFunction = null;
    

    private org.talend.designer.spark.generator.FunctionGenerator functionGenerator = null;

    public SparkRowTransformUtil() {

    }

    public SparkRowTransformUtil(org.talend.designer.spark.generator.SparkFunction sparkFunction) {
        this.sparkFunction = sparkFunction;
    }

    public void setMultiOutput(boolean multiOutput) {
        isMultiOutput = multiOutput;
    }

    public String getCodeToGetInField(String columnName) {
        return functionGenerator.getInValue() + "." + columnName;
    }

    public String getInValue() {
        return functionGenerator.getInValue();
    }

    public String getOutValue() {
        return functionGenerator.getOutValue();
    }

    public String getInValueClass() {
        return functionGenerator.getInValueClass();
    }

    public String getOutValueClass() {
        return functionGenerator.getOutValueClass();
    }

    public String getCodeToGetOutField(boolean isReject, String columnName) {
        return functionGenerator.getOutValue(isReject ? "reject" : "main") + "." + columnName;
    }

    public String getCodeToInitOut(boolean isReject, Iterable<IMetadataColumn> columns) {
        if(this.sparkFunction!=null && !isMultiOutput) {
            return this.sparkFunction.getCodeToInitOut(functionGenerator.getOutValue(isReject ? "reject" : "main"), functionGenerator.getOutValueClass(isReject ? "reject" : "main"));
        } else {
            return "";
        }
    }

    public String getCodeToSetOutField(boolean isReject, String columnName, String codeValue) {
        return functionGenerator.getOutValue(isReject ? "reject" : "main") + "." + columnName + " = " + codeValue + ";";
    }

    public String getCodeToSetOutField(boolean isReject, String columnName, String codeValue, String operator) {
        return functionGenerator.getOutValue(isReject ? "reject" : "main") + "." + columnName + " " + operator + " " + codeValue + ";";
    }

    public String getCodeToEmit(boolean isReject) {
        if (this.sparkFunction!=null && isMultiOutput) {
            if (isReject) {
                return this.sparkFunction.getCodeToEmit(false, functionGenerator.getOutValue("reject"), functionGenerator.getOutValueClass("reject"));
            } else {
                return this.sparkFunction.getCodeToEmit(true, functionGenerator.getOutValue("main"), functionGenerator.getOutValueClass("main"));
            }
        } else {
            return "";
        }
    }

    public void generateSparkCode(final org.talend.designer.common.TransformerBase transformer, final org.talend.designer.spark.generator.SparkFunction sparkFunction) {
        if (transformer.isMultiOutput()) {
            setMultiOutput(true);
        }
        if (transformer.isUnnecessary()) {
            
    stringBuffer.append(TEXT_252);
    stringBuffer.append(cid);
    
            return;
        }


        // Refactoring FunctionGenerator to java so we have to instaniate a MO or SO here
        functionGenerator = new SOFunctionGenerator(transformer, sparkFunction);

        functionGenerator.init(node, cid, null, transformer.getInConnName(), null, 
                transformer.getOutConnMainName() != null
                    ? transformer.getOutConnMainName()
                            : transformer.getOutConnRejectName());

        functionGenerator.generate();


        String internalType = ((org.talend.designer.spark.streaming.generator.TopStreamingFunction)sparkFunction).getConfigInternalType(functionGenerator.getOutValueClass());
        
    stringBuffer.append(TEXT_253);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_254);
    stringBuffer.append(internalType);
    stringBuffer.append(TEXT_255);
    stringBuffer.append(internalType);
    stringBuffer.append(TEXT_256);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_257);
    stringBuffer.append(internalType);
    stringBuffer.append(TEXT_258);
    stringBuffer.append(internalType);
    stringBuffer.append(TEXT_259);
    stringBuffer.append(sparkFunction.getConfigTransformation());
    stringBuffer.append(TEXT_260);
    stringBuffer.append(sparkFunction.getClassName(cid));
    stringBuffer.append(TEXT_261);
    
    }

    public void generateSparkConfig(final org.talend.designer.common.TransformerBase transformer, final org.talend.designer.spark.generator.SparkFunction sparkFunction, String topX) {
        if (transformer.isUnnecessary()) {
            
    stringBuffer.append(TEXT_262);
    stringBuffer.append(cid);
    
            return;
        }

        functionGenerator = new SOFunctionGenerator(transformer, sparkFunction);

        functionGenerator.init(node, cid, null, transformer.getInConnName(), null, 
                transformer.getOutConnMainName() != null
                    ? transformer.getOutConnMainName()
                            : transformer.getOutConnRejectName());
            String returnedType = sparkFunction.getConfigReturnedType(functionGenerator.getOutValueClass());
            
    stringBuffer.append(TEXT_263);
    stringBuffer.append(returnedType);
    stringBuffer.append(TEXT_264);
    stringBuffer.append(transformer.getOutConnMainName());
    stringBuffer.append(TEXT_265);
    stringBuffer.append(transformer.getInConnName());
    stringBuffer.append(TEXT_266);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_267);
    stringBuffer.append(topX);
    stringBuffer.append(TEXT_268);
    
    }
}


    

/**
 * Contains common processing for tSortRow code generation.
 */
class TSortRowUtil extends org.talend.designer.common.TransformerBase {

    /** The list of columns that should be copied directly from the input to
     *  the output schema (where they have the same column names). */
    final private Iterable<IMetadataColumn> copiedInColumns;

    /** When generating a comparator, the code accessor for the left side. */
    private final String codeVarData1;

    /** When generating a comparator, the code accessor for the right side. */
    private final String codeVarData2;

    java.util.List<java.util.Map<String, String>> criterias = (java.util.List<java.util.Map<String,String>>)ElementParameterParser.getObjectValue(node, "__CRITERIA__");
    java.util.List<String> listCols = new java.util.ArrayList<String>();

    /** Contains ONLY those columns that are ascending. */
    java.util.Set<String> criteriasAscendingColumns = new java.util.HashSet<String>();

    java.util.Map<String, Integer> criteriasSortType = new java.util.HashMap<String, Integer>();
    java.util.Map<String, Boolean> sortTypes = new java.util.HashMap<String, Boolean>();

    final java.util.Map<String, java.util.List<org.talend.core.model.metadata.IMetadataColumn>> keyList;

    final Integer SORT_NUM = 0;
    final Integer SORT_ALPHA = 1;
    final Integer SORT_DATE = 2;

    public TSortRowUtil(INode node,
            org.talend.designer.common.BigDataCodeGeneratorArgument argument,
            org.talend.designer.common.CommonRowTransformUtil rowTransformUtil) {
        this(node, argument, rowTransformUtil, "data1", "data2", false);
    }

    /**
     * @param invert if true, changes all ascending columns to descending
     *        columns and vice versa.
     */
    protected TSortRowUtil(INode node,
            org.talend.designer.common.BigDataCodeGeneratorArgument argument,
            org.talend.designer.common.CommonRowTransformUtil rowTransformUtil,
            String codeVarData1, String codeVarData2, boolean invert) {
        super(node, argument, rowTransformUtil, "FLOW", "REJECT");

        keyList = ((IBigDataNode) node).getKeyList();
        this.codeVarData1 = codeVarData1;
        this.codeVarData2 = codeVarData2;

        if (null != getInConn() && null != getOutConnMain()) {
            copiedInColumns = org.talend.designer.common.TransformerBaseUtil.getColumnsUnion(getInColumns(), getOutColumnsMain());
        } else {
            copiedInColumns = null;
        }

        for(int i = 0; i < criterias.size(); i++) {
            java.util.Map<String, String> line = criterias.get(i);
            String colname = line.get("COLNAME");
            if (listCols.contains(colname)) {
                continue;//skip dipulicate
            }
            listCols.add(colname);

            if ("asc".equals(line.get("ORDER")) == !invert) {
                criteriasAscendingColumns.add(colname);
            }

            if (("num").equals(line.get("SORT"))) {
                criteriasSortType.put(colname, SORT_NUM);
                sortTypes.put(colname, true);
            } else if (("alpha").equals(line.get("SORT"))) {
                sortTypes.put(colname, false);
            } else {
                criteriasSortType.put(colname, SORT_DATE);
                sortTypes.put(colname, true);
            }
        }
    }

    public void generateTransformContextDeclaration() {
        // Nothing here
    }

    /**
     * Generates code that performs the tranformation from one input string
     * to many output strings, or to a reject flow.
     */
    public void generateTransform() {
        generateTransform(true);
    }

    /**
     * Generates code that performs the tranformation from one input string
     * to many output strings, or to a reject flow. The boolean parameter is
     * used to define whether the transform method return type is void or something
     * else.
     */
    public void generateTransform(boolean hasAReturnedType) {
        int index = 0;
        for(String col:listCols) {
            for(IMetadataColumn column : copiedInColumns) {
                if (col.equals(column.getLabel())) {
                    generateComparatorSnippetForColumn(column, index,
                        org.talend.designer.spark.generator.utils.SparkFunctionUtil.getKeyValueAccessor(
                            keyList, "BOTH", codeVarData1, index),
                        org.talend.designer.spark.generator.utils.SparkFunctionUtil.getKeyValueAccessor(
                            keyList, "BOTH", codeVarData2, index));
                    index++;
                    break;
                }
            }
        }
    }

    /**
     * Generates code in the transform context to create reject output.
     *
     * @param die if this reject output should kill the job.  Normally, this is
     *    tied to a dieOnError parameter for the component, but can be
     *    explicitly set to false for non-fatal reject output.
     * @param codeException a variable in the transform scope that contains the
     *    variable with an exception.  If null, this will be constructed from
     *    the codeRejectMsg.
     * @param codeRejectMsg the error message to output with the reject output.
     */
    private void generateTransformReject(boolean die, String codeException, String codeRejectMsg) {

    }

    protected void greater(String columnName) {
        if (criteriasAscendingColumns.contains(columnName)) {
            
    stringBuffer.append(TEXT_269);
    
        } else {
            
    stringBuffer.append(TEXT_270);
    
        }
    }

    protected void lesser(String columnName) {
        if (criteriasAscendingColumns.contains(columnName)) {
            
    stringBuffer.append(TEXT_271);
    
        } else {
            
    stringBuffer.append(TEXT_272);
    
        }
    }

    private void generateComparatorSnippetForColumn(IMetadataColumn column,
                int i, String codeData1, String codeData2) {
        boolean nullable = !JavaTypesManager.isJavaPrimitiveType(column.getTalendType(), column.isNullable());
        String typeToGenerate = JavaTypesManager.getTypeToGenerate(column.getTalendType(), column.isNullable());
        String pattern = column.getPattern() == null || column.getPattern().trim().length() == 0 ? null : column.getPattern();
        String columnName = column.getLabel();

        // For nullable columns, we always generate a comparison that sorts
        // nulls before non-null.
        if (nullable) {
            
    stringBuffer.append(TEXT_273);
    stringBuffer.append(codeData1);
    stringBuffer.append(TEXT_274);
    stringBuffer.append(codeData2);
    stringBuffer.append(TEXT_275);
    lesser(columnName);
    stringBuffer.append(TEXT_276);
    stringBuffer.append(codeData1);
    stringBuffer.append(TEXT_277);
    stringBuffer.append(codeData2);
    stringBuffer.append(TEXT_278);
    greater(columnName);
    stringBuffer.append(TEXT_279);
    stringBuffer.append(codeData1);
    stringBuffer.append(TEXT_280);
    stringBuffer.append(codeData2);
    stringBuffer.append(TEXT_281);
    
            // Note that the nullable else case is left open here and closed
            // at the end.
        }

        if (typeToGenerate.equalsIgnoreCase("Boolean")) {
            
    stringBuffer.append(TEXT_282);
    stringBuffer.append(codeData1);
    stringBuffer.append(TEXT_283);
    stringBuffer.append(codeData2);
    stringBuffer.append(TEXT_284);
    stringBuffer.append(codeData1);
    stringBuffer.append(TEXT_285);
    greater(columnName);
    stringBuffer.append(TEXT_286);
    lesser(columnName);
    stringBuffer.append(TEXT_287);
    
        } else if (typeToGenerate.equalsIgnoreCase("Byte")) {
            
    stringBuffer.append(TEXT_288);
    stringBuffer.append(codeData1);
    stringBuffer.append(TEXT_289);
    stringBuffer.append(codeData2);
    stringBuffer.append(TEXT_290);
    greater(columnName);
    stringBuffer.append(TEXT_291);
    stringBuffer.append(codeData1);
    stringBuffer.append(TEXT_292);
    stringBuffer.append(codeData2);
    stringBuffer.append(TEXT_293);
    lesser(columnName);
    stringBuffer.append(TEXT_294);
    
        } else if (typeToGenerate.equals("byte[]")) {
            
    stringBuffer.append(TEXT_295);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_296);
    stringBuffer.append(codeData1);
    stringBuffer.append(TEXT_297);
    stringBuffer.append(codeData2);
    stringBuffer.append(TEXT_298);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_299);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_300);
    greater(columnName);
    stringBuffer.append(TEXT_301);
    lesser(columnName);
    stringBuffer.append(TEXT_302);
    
        } else if (typeToGenerate.equalsIgnoreCase("Char") || typeToGenerate.equalsIgnoreCase("Character")) {
            
    stringBuffer.append(TEXT_303);
    stringBuffer.append(codeData1);
    stringBuffer.append(TEXT_304);
    stringBuffer.append(codeData2);
    stringBuffer.append(TEXT_305);
    stringBuffer.append(codeData1);
    stringBuffer.append(TEXT_306);
    stringBuffer.append(codeData2);
    stringBuffer.append(TEXT_307);
    greater(columnName);
    stringBuffer.append(TEXT_308);
    lesser(columnName);
    stringBuffer.append(TEXT_309);
    
        } else if (typeToGenerate.equals("java.util.Date")) {
            if (!sortTypes.get(columnName)) {
                
    stringBuffer.append(TEXT_310);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_311);
    stringBuffer.append(codeData1);
    stringBuffer.append(TEXT_312);
    stringBuffer.append(pattern);
    stringBuffer.append(TEXT_313);
    stringBuffer.append(codeData2);
    stringBuffer.append(TEXT_314);
    stringBuffer.append(pattern);
    stringBuffer.append(TEXT_315);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_316);
    greater(columnName);
    stringBuffer.append(TEXT_317);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_318);
    lesser(columnName);
    stringBuffer.append(TEXT_319);
    
            } else {
                
    stringBuffer.append(TEXT_320);
    stringBuffer.append(codeData1);
    stringBuffer.append(TEXT_321);
    stringBuffer.append(codeData2);
    stringBuffer.append(TEXT_322);
    stringBuffer.append(codeData1);
    stringBuffer.append(TEXT_323);
    stringBuffer.append(codeData2);
    stringBuffer.append(TEXT_324);
    greater(columnName);
    stringBuffer.append(TEXT_325);
    lesser(columnName);
    stringBuffer.append(TEXT_326);
    
            }
        } else if (typeToGenerate.equalsIgnoreCase("Double")) {
            if (!sortTypes.get(columnName)) {
                
    stringBuffer.append(TEXT_327);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_328);
    stringBuffer.append(codeData1);
    stringBuffer.append(TEXT_329);
    stringBuffer.append(codeData2);
    stringBuffer.append(TEXT_330);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_331);
    greater(columnName);
    stringBuffer.append(TEXT_332);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_333);
    lesser(columnName);
    stringBuffer.append(TEXT_334);
    
            } else {
                
    stringBuffer.append(TEXT_335);
    stringBuffer.append(codeData1);
    stringBuffer.append(TEXT_336);
    stringBuffer.append(codeData2);
    stringBuffer.append(TEXT_337);
    greater(columnName);
    stringBuffer.append(TEXT_338);
    stringBuffer.append(codeData1);
    stringBuffer.append(TEXT_339);
    stringBuffer.append(codeData2);
    stringBuffer.append(TEXT_340);
    lesser(columnName);
    stringBuffer.append(TEXT_341);
    
            }
        } else if (typeToGenerate.equalsIgnoreCase("Float")) {
            if (!sortTypes.get(columnName)) {
                
    stringBuffer.append(TEXT_342);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_343);
    stringBuffer.append(codeData1);
    stringBuffer.append(TEXT_344);
    stringBuffer.append(codeData2);
    stringBuffer.append(TEXT_345);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_346);
    greater(columnName);
    stringBuffer.append(TEXT_347);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_348);
    lesser(columnName);
    stringBuffer.append(TEXT_349);
    
            } else {
                
    stringBuffer.append(TEXT_350);
    stringBuffer.append(codeData1);
    stringBuffer.append(TEXT_351);
    stringBuffer.append(codeData2);
    stringBuffer.append(TEXT_352);
    greater(columnName);
    stringBuffer.append(TEXT_353);
    stringBuffer.append(codeData1);
    stringBuffer.append(TEXT_354);
    stringBuffer.append(codeData2);
    stringBuffer.append(TEXT_355);
    lesser(columnName);
    stringBuffer.append(TEXT_356);
    
            }
        } else if (typeToGenerate.equals("BigDecimal")) {
            if (!sortTypes.get(columnName)) {
                
    stringBuffer.append(TEXT_357);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_358);
    stringBuffer.append(codeData1);
    stringBuffer.append(TEXT_359);
    stringBuffer.append(codeData2);
    stringBuffer.append(TEXT_360);
    
            } else {
                
    stringBuffer.append(TEXT_361);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_362);
    stringBuffer.append(codeData1);
    stringBuffer.append(TEXT_363);
    stringBuffer.append(codeData2);
    stringBuffer.append(TEXT_364);
    }
    stringBuffer.append(TEXT_365);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_366);
    greater(columnName);
    stringBuffer.append(TEXT_367);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_368);
    lesser(columnName);
    stringBuffer.append(TEXT_369);
    
        } else if (typeToGenerate.equalsIgnoreCase("Integer") || typeToGenerate.equalsIgnoreCase("int")) {
            if (!sortTypes.get(columnName)) {
                
    stringBuffer.append(TEXT_370);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_371);
    stringBuffer.append(codeData1);
    stringBuffer.append(TEXT_372);
    stringBuffer.append(codeData2);
    stringBuffer.append(TEXT_373);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_374);
    greater(columnName);
    stringBuffer.append(TEXT_375);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_376);
    lesser(columnName);
    stringBuffer.append(TEXT_377);
    
            } else {
                
    stringBuffer.append(TEXT_378);
    stringBuffer.append(codeData1);
    stringBuffer.append(TEXT_379);
    stringBuffer.append(codeData2);
    stringBuffer.append(TEXT_380);
    greater(columnName);
    stringBuffer.append(TEXT_381);
    stringBuffer.append(codeData1);
    stringBuffer.append(TEXT_382);
    stringBuffer.append(codeData2);
    stringBuffer.append(TEXT_383);
    lesser(columnName);
    stringBuffer.append(TEXT_384);
    
            }
        } else if (typeToGenerate.equalsIgnoreCase("Long")) {
            if (!sortTypes.get(columnName)) {
                
    stringBuffer.append(TEXT_385);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_386);
    stringBuffer.append(codeData1);
    stringBuffer.append(TEXT_387);
    stringBuffer.append(codeData2);
    stringBuffer.append(TEXT_388);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_389);
    greater(columnName);
    stringBuffer.append(TEXT_390);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_391);
    lesser(columnName);
    stringBuffer.append(TEXT_392);
    
            } else {
                
    stringBuffer.append(TEXT_393);
    stringBuffer.append(codeData1);
    stringBuffer.append(TEXT_394);
    stringBuffer.append(codeData2);
    stringBuffer.append(TEXT_395);
    greater(columnName);
    stringBuffer.append(TEXT_396);
    stringBuffer.append(codeData1);
    stringBuffer.append(TEXT_397);
    stringBuffer.append(codeData2);
    stringBuffer.append(TEXT_398);
    lesser(columnName);
    stringBuffer.append(TEXT_399);
    
            }
        } else if (typeToGenerate.equalsIgnoreCase("Short")) {
            if (!sortTypes.get(columnName)) {
                
    stringBuffer.append(TEXT_400);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_401);
    stringBuffer.append(codeData1);
    stringBuffer.append(TEXT_402);
    stringBuffer.append(codeData2);
    stringBuffer.append(TEXT_403);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_404);
    greater(columnName);
    stringBuffer.append(TEXT_405);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_406);
    lesser(columnName);
    stringBuffer.append(TEXT_407);
    
            } else {
                
    stringBuffer.append(TEXT_408);
    stringBuffer.append(codeData1);
    stringBuffer.append(TEXT_409);
    stringBuffer.append(codeData2);
    stringBuffer.append(TEXT_410);
    greater(columnName);
    stringBuffer.append(TEXT_411);
    stringBuffer.append(codeData1);
    stringBuffer.append(TEXT_412);
    stringBuffer.append(codeData2);
    stringBuffer.append(TEXT_413);
    lesser(columnName);
    stringBuffer.append(TEXT_414);
    
            }
        } else if (typeToGenerate.equals("String")) {
            
    stringBuffer.append(TEXT_415);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_416);
    stringBuffer.append(codeData1);
    stringBuffer.append(TEXT_417);
    stringBuffer.append(codeData2);
    stringBuffer.append(TEXT_418);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_419);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_420);
    greater(columnName);
    stringBuffer.append(TEXT_421);
    lesser(columnName);
    stringBuffer.append(TEXT_422);
    
        } else if (typeToGenerate.equals("Object")
                || typeToGenerate.equals("List")
                || typeToGenerate.equals("Document")
                || typeToGenerate.equals("Dynamic")) {
            
    stringBuffer.append(TEXT_423);
    stringBuffer.append(typeToGenerate);
    stringBuffer.append(TEXT_424);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_425);
    
        }

        // Close the open else statement for nullable columns.
        if (nullable) {
            
    stringBuffer.append(TEXT_426);
    
        }
    }
}

    

/**
 * Contains common processing for tTop code generation.
 */
class TTopUtil extends TSortRowUtil {

    public TTopUtil(INode node,
            org.talend.designer.common.BigDataCodeGeneratorArgument argument,
            org.talend.designer.common.CommonRowTransformUtil rowTransformUtil) {
        super(node, argument, rowTransformUtil, "data1._1", "data2._1", true);
    }
}

    stringBuffer.append(TEXT_427);
    
	final SparkRowTransformUtil sparkTransformUtil = new SparkRowTransformUtil();
	final TTopUtil tTopUtil = new TTopUtil(node, codeGenArgument, sparkTransformUtil);

	java.util.Map<String, java.util.List<org.talend.core.model.metadata.IMetadataColumn>> keyList = bigDataNode.getKeyList();
	org.talend.designer.spark.streaming.generator.TopStreamingFunction sparkFunction = new org.talend.designer.spark.streaming.generator.TopStreamingFunction(false, keyList);

	sparkTransformUtil.generateSparkCode(tTopUtil, sparkFunction);
}

    return stringBuffer.toString();
  }
}
