package org.talend.designer.codegen.translators.dataquality.matching;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.talend.core.model.metadata.IMetadataColumn;
import org.talend.core.model.metadata.IMetadataTable;
import org.talend.core.model.metadata.types.JavaType;
import org.talend.core.model.metadata.types.JavaTypesManager;
import org.talend.core.model.process.ElementParameterParser;
import org.talend.core.model.process.IBigDataNode;
import org.talend.core.model.process.IConnection;
import org.talend.core.model.process.IConnectionCategory;
import org.talend.core.model.process.INode;
import org.talend.designer.common.BigDataCodeGeneratorArgument;
import org.talend.designer.common.tsqlrow.TSqlRowUtil;

public class TMatchIndexPredictSparkcodeJava
{
  protected static String nl;
  public static synchronized TMatchIndexPredictSparkcodeJava create(String lineSeparator)
  {
    nl = lineSeparator;
    TMatchIndexPredictSparkcodeJava result = new TMatchIndexPredictSparkcodeJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "    public static class ";
  protected final String TEXT_2 = "_FromRowTo";
  protected final String TEXT_3 = " implements org.apache.spark.api.java.function.Function<org.apache.spark.sql.Row, ";
  protected final String TEXT_4 = "> {" + NL + "        " + NL + "        public ";
  protected final String TEXT_5 = " call(org.apache.spark.sql.Row row) {";
  protected final String TEXT_6 = NL + "            ";
  protected final String TEXT_7 = " result = new ";
  protected final String TEXT_8 = "();" + NL + "            org.apache.spark.sql.types.StructField[] structFields = row.schema().fields();" + NL + "            for (int i = 0; i < structFields.length; i++) {" + NL + "                org.apache.avro.Schema.Field avroField = ";
  protected final String TEXT_9 = ".getClassSchema().getField(structFields[i].name());" + NL + "                if (avroField != null){" + NL + "                    result.put(avroField.pos(), row.get(i));" + NL + "                }" + NL + "            }" + NL + "            return result;" + NL + "        }" + NL + "    }";
  protected final String TEXT_10 = NL + "        public static class ";
  protected final String TEXT_11 = "_FromRowTo";
  protected final String TEXT_12 = " implements org.apache.spark.api.java.function.Function<org.apache.spark.sql.Row, ";
  protected final String TEXT_13 = "> {" + NL + "            " + NL + "            public ";
  protected final String TEXT_14 = " call(org.apache.spark.sql.Row row) {";
  protected final String TEXT_15 = NL + "                ";
  protected final String TEXT_16 = " result = new ";
  protected final String TEXT_17 = "();" + NL + "                org.apache.spark.sql.types.StructField[] structFields = row.schema().fields();" + NL + "                for (int i = 0; i < structFields.length; i++) {" + NL + "                    org.apache.avro.Schema.Field avroField = ";
  protected final String TEXT_18 = ".getClassSchema().getField(structFields[i].name());" + NL + "                    if (avroField != null){" + NL + "                        result.put(avroField.pos(), row.get(i));" + NL + "                    }" + NL + "                }" + NL + "                return result;" + NL + "            }" + NL + "        }" + NL;
  protected final String TEXT_19 = NL + NL + "        public static class ";
  protected final String TEXT_20 = "_From";
  protected final String TEXT_21 = "To";
  protected final String TEXT_22 = " implements org.apache.spark.api.java.function.Function<";
  protected final String TEXT_23 = ", ";
  protected final String TEXT_24 = "> {" + NL + "" + NL + "            public ";
  protected final String TEXT_25 = " call(";
  protected final String TEXT_26 = " input) {";
  protected final String TEXT_27 = NL + "                ";
  protected final String TEXT_28 = " result = new ";
  protected final String TEXT_29 = "();";
  protected final String TEXT_30 = NL + "                        if(input.";
  protected final String TEXT_31 = " != null) {" + NL + "                            result.";
  protected final String TEXT_32 = " = new java.sql.Timestamp(input.";
  protected final String TEXT_33 = ".getTime());" + NL + "                        } else {" + NL + "                            result.";
  protected final String TEXT_34 = " = null;" + NL + "                        }";
  protected final String TEXT_35 = NL + "                    result.";
  protected final String TEXT_36 = " = input.";
  protected final String TEXT_37 = ";";
  protected final String TEXT_38 = NL + "                return result;" + NL + "            }" + NL + "        }";

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    
//Parse the inputs to this javajet generator.
final BigDataCodeGeneratorArgument codeGenArgument = (BigDataCodeGeneratorArgument) argument;
final INode node = (INode)codeGenArgument.getArgument();
final String cid = node.getUniqueName();
final boolean isLog4jEnabled = ("true").equals(ElementParameterParser.getValue(node.getProcess(), "__LOG4J_ACTIVATE__"));
boolean useConfigurationComponent = "true".equals(ElementParameterParser.getValue(node, "__DEFINE_STORAGE_CONFIGURATION__"));

TSqlRowUtil tSqlRowUtil = new TSqlRowUtil(node);
String validateError = tSqlRowUtil.validate(true, true);
if (validateError != null) {
    // Cause the job compilation to explicitly fail if there is a problem.
    return "throw new JobConfigurationError(\"" + validateError +"\");";
}

    
//IO Connections
IConnection incomingConnection = null;
if (node.getIncomingConnections() != null) {
 for (IConnection in : node.getIncomingConnections()) {
     if (in.getLineStyle().hasConnectionCategory(IConnectionCategory.DATA)) {
         incomingConnection = in;
         break;
     }
 }
}
String incomingConnectionName = incomingConnection.getName();
String incomingStructName = codeGenArgument.getRecordStructName(incomingConnection);

IConnection suspectConnection = null;
boolean hasSuspectConnection = false;
List<? extends IConnection> outgoingSuspectConnections = node.getOutgoingConnections("SUSPECT");
if (outgoingSuspectConnections.size() > 0) {
    suspectConnection = outgoingSuspectConnections.get(0);
}
if(suspectConnection != null){
    hasSuspectConnection = true;
}

IConnection noMatchConnection = null;
boolean hasnoMatchConnection = false;
List<? extends IConnection> outgoingnoMatchConnections = node.getOutgoingConnections("NOMATCH");
if (outgoingnoMatchConnections.size() > 0) {
    noMatchConnection = outgoingnoMatchConnections.get(0);
}
if(noMatchConnection != null){
    hasnoMatchConnection = true;
}

    
List<IMetadataTable> metadatas = node.getMetadataList();
if ((metadatas == null) || (metadatas.size() == 0))
    return "" ;
IMetadataTable metadata = metadatas.get(0);
if (metadata == null)
    return "";
List<IMetadataColumn> columns = metadata.getListColumns();


try {
    if(hasSuspectConnection){
        String suspectOutConnectionName = suspectConnection.getName();
        String suspectOutStructName = codeGenArgument.getRecordStructName(suspectConnection);
    
    stringBuffer.append(TEXT_1);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_2);
    stringBuffer.append(suspectOutStructName);
    stringBuffer.append(TEXT_3);
    stringBuffer.append(suspectOutStructName);
    stringBuffer.append(TEXT_4);
    stringBuffer.append(suspectOutStructName);
    stringBuffer.append(TEXT_5);
    stringBuffer.append(TEXT_6);
    stringBuffer.append(suspectOutStructName);
    stringBuffer.append(TEXT_7);
    stringBuffer.append(suspectOutStructName);
    stringBuffer.append(TEXT_8);
    stringBuffer.append(suspectOutStructName);
    stringBuffer.append(TEXT_9);
    
    }//end if(hasSuspectConnection)

    if(hasnoMatchConnection){ 
        String exactOutConnectionName = noMatchConnection.getName();
        String exactOutStructName = codeGenArgument.getRecordStructName(noMatchConnection);

    stringBuffer.append(TEXT_10);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_11);
    stringBuffer.append(exactOutStructName);
    stringBuffer.append(TEXT_12);
    stringBuffer.append(exactOutStructName);
    stringBuffer.append(TEXT_13);
    stringBuffer.append(exactOutStructName);
    stringBuffer.append(TEXT_14);
    stringBuffer.append(TEXT_15);
    stringBuffer.append(exactOutStructName);
    stringBuffer.append(TEXT_16);
    stringBuffer.append(exactOutStructName);
    stringBuffer.append(TEXT_17);
    stringBuffer.append(exactOutStructName);
    stringBuffer.append(TEXT_18);
    
    }//end if(hasnoMatchConnection)

// If the incoming rowStruct contains a Date field (always typed as java.util.Date),
// we must generate a new structure which replaces these java.util.Date instances by
// java.sql.Timestamp instances since DataFrames only support java.sql.Timestamp.

org.talend.designer.bigdata.avro.AvroRecordStructGenerator avroRecordStructGenerator = (org.talend.designer.bigdata.avro.AvroRecordStructGenerator) codeGenArgument.getRecordStructGenerator();

// Some of the incoming connections might share the same schema (and then the same rowXStruct). We must generate the below code only once by schema (if necessary).
java.util.Set<String> knownStructNames = new java.util.HashSet();

    if(tSqlRowUtil.containsDateFields(incomingConnection) && !knownStructNames.contains(incomingStructName)) {
        String suggestedDfStructName = "DF_"+incomingStructName;
        String dfStructName = avroRecordStructGenerator.generateRecordStructForDataFrame(suggestedDfStructName, incomingStructName);
        knownStructNames.add(incomingStructName);

    stringBuffer.append(TEXT_19);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_20);
    stringBuffer.append(incomingStructName);
    stringBuffer.append(TEXT_21);
    stringBuffer.append(dfStructName);
    stringBuffer.append(TEXT_22);
    stringBuffer.append(incomingStructName);
    stringBuffer.append(TEXT_23);
    stringBuffer.append(dfStructName);
    stringBuffer.append(TEXT_24);
    stringBuffer.append(dfStructName);
    stringBuffer.append(TEXT_25);
    stringBuffer.append(incomingStructName);
    stringBuffer.append(TEXT_26);
    stringBuffer.append(TEXT_27);
    stringBuffer.append(dfStructName);
    stringBuffer.append(TEXT_28);
    stringBuffer.append(dfStructName);
    stringBuffer.append(TEXT_29);
    
                for(IMetadataColumn column : columns) {
                    if(tSqlRowUtil.isDateField(column)) {

    stringBuffer.append(TEXT_30);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_31);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_32);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_33);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_34);
    
                } else {

    stringBuffer.append(TEXT_35);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_36);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_37);
    
                }
            } // end for(IMetadataColumn column : columns)

    stringBuffer.append(TEXT_38);
    
    }


} catch (java.lang.Exception e) {
    // Do not generate sparkcode part : a clean exception has to be generated within sparkconfig part
}

    return stringBuffer.toString();
  }
}
