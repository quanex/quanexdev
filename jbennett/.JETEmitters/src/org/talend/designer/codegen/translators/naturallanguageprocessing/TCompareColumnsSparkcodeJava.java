package org.talend.designer.codegen.translators.naturallanguageprocessing;

import org.talend.core.model.process.INode;
import org.talend.core.model.process.ElementParameterParser;
import org.talend.core.model.metadata.IMetadataTable;
import org.talend.core.model.metadata.IMetadataColumn;
import org.talend.core.model.process.IConnection;
import org.talend.core.model.process.EConnectionType;
import org.talend.designer.common.BigDataCodeGeneratorArgument;
import org.talend.core.model.metadata.types.JavaTypesManager;
import org.talend.core.model.process.IConnectionCategory;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.ArrayList;
import java.lang.StringBuilder;

public class TCompareColumnsSparkcodeJava
{
  protected static String nl;
  public static synchronized TCompareColumnsSparkcodeJava create(String lineSeparator)
  {
    nl = lineSeparator;
    TCompareColumnsSparkcodeJava result = new TCompareColumnsSparkcodeJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "static final int ";
  protected final String TEXT_2 = "outSchemaSize = ";
  protected final String TEXT_3 = ";" + NL + "static final int ";
  protected final String TEXT_4 = "inSchemaSize = ";
  protected final String TEXT_5 = ";" + NL + "static final java.util.Set<String> ";
  protected final String TEXT_6 = "nicknameSet = org.talend.dataquality.nlp.FeatureConstructor.createNickNameSet(\"nicknames.txt\");" + NL + "static final java.util.Map<String, java.util.Set<String>> ";
  protected final String TEXT_7 = "nicknameTable = org.talend.dataquality.nlp.FeatureConstructor.createNickNameTable(\"nicknames.txt\", ";
  protected final String TEXT_8 = "nicknameSet);" + NL + "" + NL + "" + NL + "public static class ";
  protected final String TEXT_9 = "compare implements org.apache.spark.api.java.function.Function<";
  protected final String TEXT_10 = ", ";
  protected final String TEXT_11 = ">{" + NL + "    private List<String> opts;" + NL + "    public ";
  protected final String TEXT_12 = "compare(List<String> opts){" + NL + "        this.opts = opts;" + NL + "    }" + NL + "" + NL + "    @Override" + NL + "    public ";
  protected final String TEXT_13 = " call(";
  protected final String TEXT_14 = " row){";
  protected final String TEXT_15 = NL + "        ";
  protected final String TEXT_16 = " output = new ";
  protected final String TEXT_17 = "();" + NL + "        for(String opt : opts){" + NL + "            int outputIdx = Integer.parseInt(opt.split(\"\\t\")[3]);// 0, main col 1, ref col 2, algo 3, output col" + NL + "            String operation = opt.split(\"\\t\")[2];" + NL + "            int mainIdx = Integer.parseInt(opt.split(\"\\t\")[0]);" + NL + "            int refIdx = Integer.parseInt(opt.split(\"\\t\")[1]);" + NL + "            String token = (String)row.get(mainIdx);" + NL + "            if(operation.equals(\"SIMILARITY*\")){" + NL + "                String refAll = (String)row.get(refIdx);" + NL + "                String[] ref = refAll.split(\"\\t\");" + NL + "                String similarity = \"\";" + NL + "                String similarTokens = \"\";" + NL + "                if(token!=null &&!token.equals(\"\")){" + NL + "                    double maxSim = -1;" + NL + "                    String tokenSim = \"\";" + NL + "                    for(String refer : ref){" + NL + "                        java.util.Set<String> nicknames = org.talend.dataquality.nlp.FeatureConstructor.searchNickName(refer, ";
  protected final String TEXT_18 = "nicknameSet, ";
  protected final String TEXT_19 = "nicknameTable);" + NL + "                        for(String nickname : nicknames){" + NL + "                            double sim = org.apache.commons.lang3.StringUtils.getJaroWinklerDistance(token, nickname);" + NL + "                            if(sim > maxSim){" + NL + "                                maxSim = sim;" + NL + "                                tokenSim = nickname;" + NL + "                            }" + NL + "                        }" + NL + "                    }" + NL + "                    similarity = String.format(\"%.2f\",maxSim);" + NL + "                    similarTokens = tokenSim;" + NL + "                }else{" + NL + "                    similarity = \"\";" + NL + "                    similarTokens = \"\";" + NL + "                }" + NL + "                //System.out.println(outputIdx);" + NL + "                output.put(outputIdx, similarity);" + NL + "                output.put(outputIdx + 1, similarTokens);" + NL + "            }else if(operation.equals(\"FIRSTLETTER*\")){" + NL + "                String refAll = (String)row.get(refIdx);" + NL + "                if(token!=null &&token.length() > 0 &&refAll!=null && refAll.length() > 0){" + NL + "                    if(Character.toLowerCase(token.charAt(0)) == Character.toLowerCase(refAll.charAt(0))){" + NL + "                        output.put(outputIdx, \"T\");" + NL + "                    }else{" + NL + "                        output.put(outputIdx, \"F\");" + NL + "                    }" + NL + "                }else{" + NL + "                    output.put(outputIdx, \"\");" + NL + "                }" + NL + "            }else if(operation.equals(\"SUBSTRING*\")){" + NL + "                String refAll = (String)row.get(refIdx);" + NL + "                if(token!=null &&token.length() > 0 &&refAll!=null && refAll.length() > 0){" + NL + "                    if(refAll.toLowerCase().contains(token.toLowerCase())){" + NL + "                        output.put(outputIdx, \"T\");" + NL + "                    }else{" + NL + "                        output.put(outputIdx, \"F\");" + NL + "                    }" + NL + "                }else{" + NL + "                    output.put(outputIdx, \"\");" + NL + "                }" + NL + "            }" + NL + "        }";
  protected final String TEXT_20 = NL + "            output.put(";
  protected final String TEXT_21 = ", row.get(";
  protected final String TEXT_22 = "));";
  protected final String TEXT_23 = NL + "        return output;" + NL + "    }" + NL + "}";

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
     
final BigDataCodeGeneratorArgument codeGenArgument = (BigDataCodeGeneratorArgument) argument;
final INode node = (INode)codeGenArgument.getArgument();
final String cid = node.getUniqueName();
List<? extends IConnection> inConns = node.getIncomingConnections(EConnectionType.FLOW_MAIN);
List< ? extends IConnection> outConns = node.getOutgoingConnections();
IMetadataTable inputMetadataTable = null;
IMetadataTable outputMetadataTable = null;
String inConnName = "";
String inConnTypeName = "";
String outConnName = "";
String outConnTypeName = "";
List<IMetadataColumn> outputColumns = null;
List<IMetadataColumn> inputColumns = null;
StringBuilder pipeline = new StringBuilder("");
if (inConns.size() == 1){
    inConnName = inConns.get(0).getName();
    inputMetadataTable = inConns.get(0).getMetadataTable();
    inConnTypeName = codeGenArgument.getRecordStructName(inConns.get(0));
    inputColumns = inputMetadataTable.getListColumns();
}
if (outConns.size() == 1){
    outConnName = outConns.get(0).getName();
    outputMetadataTable = outConns.get(0).getMetadataTable();
    outConnTypeName = codeGenArgument.getRecordStructName(outConns.get(0));
    outputColumns = outputMetadataTable.getListColumns();
}

    stringBuffer.append(TEXT_1);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_2);
    stringBuffer.append(outputColumns.size());
    stringBuffer.append(TEXT_3);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_4);
    stringBuffer.append(inputColumns.size());
    stringBuffer.append(TEXT_5);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_6);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_7);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_8);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_9);
    stringBuffer.append(inConnTypeName);
    stringBuffer.append(TEXT_10);
    stringBuffer.append(outConnTypeName);
    stringBuffer.append(TEXT_11);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_12);
    stringBuffer.append(outConnTypeName);
    stringBuffer.append(TEXT_13);
    stringBuffer.append(inConnTypeName);
    stringBuffer.append(TEXT_14);
    stringBuffer.append(TEXT_15);
    stringBuffer.append(outConnTypeName);
    stringBuffer.append(TEXT_16);
    stringBuffer.append(outConnTypeName);
    stringBuffer.append(TEXT_17);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_18);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_19);
    
        for(int i = 0; i < inputColumns.size(); i++) {
        
    stringBuffer.append(TEXT_20);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_21);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_22);
    
          }
        
    stringBuffer.append(TEXT_23);
    return stringBuffer.toString();
  }
}
