package org.talend.designer.codegen.translators.dataquality.matching;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.talend.core.model.metadata.IMetadataColumn;
import org.talend.core.model.metadata.IMetadataTable;
import org.talend.core.model.metadata.types.JavaType;
import org.talend.core.model.metadata.types.JavaTypesManager;
import org.talend.core.model.process.ElementParameterParser;
import org.talend.core.model.process.IBigDataNode;
import org.talend.core.model.process.IConnection;
import org.talend.core.model.process.IConnectionCategory;
import org.talend.core.model.process.INode;
import org.talend.core.model.utils.NodeUtil;
import org.talend.designer.common.BigDataCodeGeneratorArgument;
import org.talend.designer.common.tmodelencoder.TModelEncoderUtil;
import org.talend.designer.common.tsqlrow.TSqlRowUtil;

public class TMatchIndexPredictSparkconfigJava
{
  protected static String nl;
  public static synchronized TMatchIndexPredictSparkconfigJava create(String lineSeparator)
  {
    nl = lineSeparator;
    TMatchIndexPredictSparkconfigJava result = new TMatchIndexPredictSparkconfigJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "    java.net.URI currentURI_";
  protected final String TEXT_2 = "_config = FileSystem.getDefaultUri(ctx.hadoopConfiguration());" + NL + "    FileSystem.setDefaultUri(ctx.hadoopConfiguration(), new java.net.URI(";
  protected final String TEXT_3 = "));" + NL + "    fs = FileSystem.get(ctx.hadoopConfiguration());";
  protected final String TEXT_4 = NL + NL + "com.esotericsoftware.kryo.Kryo kryo_";
  protected final String TEXT_5 = " = new com.esotericsoftware.kryo.Kryo();" + NL + "TalendPipelineModelSerializer talendPipelineModelSerializer_";
  protected final String TEXT_6 = " = new TalendPipelineModelSerializer();" + NL + "    com.esotericsoftware.kryo.io.Input pairingModelInput_";
  protected final String TEXT_7 = " = new com.esotericsoftware.kryo.io.Input(fs.open(new org.apache.hadoop.fs.Path(";
  protected final String TEXT_8 = " + \"/blocking/\")));" + NL + "    TalendPipelineModel blockingModel_";
  protected final String TEXT_9 = " = kryo_";
  protected final String TEXT_10 = ".readObject(pairingModelInput_";
  protected final String TEXT_11 = ", TalendPipelineModel.class, talendPipelineModelSerializer_";
  protected final String TEXT_12 = ");" + NL + "    org.apache.spark.ml.PipelineModel pairingPM_";
  protected final String TEXT_13 = " = blockingModel_";
  protected final String TEXT_14 = ".getPipelineModel();" + NL;
  protected final String TEXT_15 = NL + "    // from file system." + NL + "    com.esotericsoftware.kryo.io.Input featuresInput_";
  protected final String TEXT_16 = " = new com.esotericsoftware.kryo.io.Input(fs.open(new org.apache.hadoop.fs.Path(";
  protected final String TEXT_17 = " + \"/model/\")));" + NL + "    TalendPipelineModel mlModel_";
  protected final String TEXT_18 = " = kryo_";
  protected final String TEXT_19 = ".readObject(featuresInput_";
  protected final String TEXT_20 = ", TalendPipelineModel.class, talendPipelineModelSerializer_";
  protected final String TEXT_21 = ");" + NL + "    org.apache.spark.ml.PipelineModel mlPM_";
  protected final String TEXT_22 = " = mlModel_";
  protected final String TEXT_23 = ".getPipelineModel();";
  protected final String TEXT_24 = NL + "    // from current job." + NL + "    org.apache.spark.ml.PipelineModel mlPM_";
  protected final String TEXT_25 = ";" + NL + "    {" + NL + "        Object model = globalMap.get(\"";
  protected final String TEXT_26 = "_MODEL\");" + NL + "        if (model instanceof org.apache.spark.ml.PipelineModel) {" + NL + "            mlPM_";
  protected final String TEXT_27 = " = (org.apache.spark.ml.PipelineModel) model;" + NL + "        } else {" + NL + "            throw new Exception(\"Unsupported model type: \" + model.getClass());" + NL + "        }" + NL + "    }";
  protected final String TEXT_28 = NL + "        // SparkSQL will use the default TimeZone to handle dates (see org.apache.spark.sql.catalyst.util.DateTimeUtils)." + NL + "        // To avoid inconsistencies with others components in the Studio, ensure that UTC is set as the default TimeZone." + NL + "        java.util.TimeZone.setDefault(java.util.TimeZone.getTimeZone(\"UTC\"));" + NL + "        " + NL + "        // java.util.Date -> java.sql.Timestamp conversions to be compliant with Spark SQL before creating our DataFrame" + NL + "        org.apache.spark.api.java.JavaRDD<";
  protected final String TEXT_29 = "> ";
  protected final String TEXT_30 = " = rdd_";
  protected final String TEXT_31 = ".map(new ";
  protected final String TEXT_32 = "_From";
  protected final String TEXT_33 = "To";
  protected final String TEXT_34 = "());";
  protected final String TEXT_35 = NL + "    //Convert the incoming RDD to DataFrame" + NL + "    org.apache.spark.sql.SQLContext sqlContext_";
  protected final String TEXT_36 = " = new org.apache.spark.sql.SQLContext(ctx);" + NL + "\t" + NL + "    org.apache.spark.sql.Dataset<org.apache.spark.sql.Row> ";
  protected final String TEXT_37 = " = " + NL + "\t        sqlContext_";
  protected final String TEXT_38 = ".createDataFrame(";
  protected final String TEXT_39 = ", ";
  protected final String TEXT_40 = ".class);" + NL + "" + NL + "" + NL + "scala.Tuple2<org.apache.spark.sql.Dataset<org.apache.spark.sql.Row>,scala.Option<org.apache.spark.sql.Dataset<org.apache.spark.sql.Row>>>" + NL + " matchResult_";
  protected final String TEXT_41 = " = " + NL + "        org.talend.dataquality.reconciliation.indexing.api.MatchIndexPredict.search(";
  protected final String TEXT_42 = NL + "        ";
  protected final String TEXT_43 = ",";
  protected final String TEXT_44 = NL + "        ";
  protected final String TEXT_45 = ", ";
  protected final String TEXT_46 = NL + "        ";
  protected final String TEXT_47 = ", ";
  protected final String TEXT_48 = NL + "        ";
  protected final String TEXT_49 = "," + NL + "        pairingPM_";
  protected final String TEXT_50 = "," + NL + "        mlPM_";
  protected final String TEXT_51 = ",";
  protected final String TEXT_52 = NL + "        ";
  protected final String TEXT_53 = ",";
  protected final String TEXT_54 = NL + "        ";
  protected final String TEXT_55 = ",";
  protected final String TEXT_56 = NL + "        ";
  protected final String TEXT_57 = ",";
  protected final String TEXT_58 = NL + "        ";
  protected final String TEXT_59 = ");";
  protected final String TEXT_60 = NL + "// Convert no-match outgoing DataFrame to RDD" + NL + "org.apache.spark.api.java.JavaRDD<";
  protected final String TEXT_61 = "> rdd_";
  protected final String TEXT_62 = " =" + NL + "    matchResult_";
  protected final String TEXT_63 = "._2.get().toJavaRDD().map(new ";
  protected final String TEXT_64 = "_FromRowTo";
  protected final String TEXT_65 = "());";
  protected final String TEXT_66 = NL;
  protected final String TEXT_67 = NL + "// Convert suspects outgoing DataFrame to RDD" + NL + "org.apache.spark.api.java.JavaRDD<";
  protected final String TEXT_68 = "> rdd_";
  protected final String TEXT_69 = " =" + NL + "    matchResult_";
  protected final String TEXT_70 = "._1.toJavaRDD().map(new ";
  protected final String TEXT_71 = "_FromRowTo";
  protected final String TEXT_72 = "());";
  protected final String TEXT_73 = NL + NL + NL;
  protected final String TEXT_74 = NL;

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    
//Parse the inputs to this javajet generator.
final BigDataCodeGeneratorArgument codeGenArgument = (BigDataCodeGeneratorArgument) argument;
final INode node = (INode)codeGenArgument.getArgument();
final String cid = node.getUniqueName();
final boolean isLog4jEnabled = ("true").equals(ElementParameterParser.getValue(node.getProcess(), "__LOG4J_ACTIVATE__"));
boolean useConfigurationComponent = "true".equals(ElementParameterParser.getValue(node, "__DEFINE_STORAGE_CONFIGURATION__"));

TSqlRowUtil tSqlRowUtil = new TSqlRowUtil(node);
String validateError = tSqlRowUtil.validate(true, true);
if (validateError != null) {
    // Cause the job compilation to explicitly fail if there is a problem.
    return "throw new JobConfigurationError(\"" + validateError +"\");";
}

    
//IO Connections
IConnection incomingConnection = null;
if (node.getIncomingConnections() != null) {
 for (IConnection in : node.getIncomingConnections()) {
     if (in.getLineStyle().hasConnectionCategory(IConnectionCategory.DATA)) {
         incomingConnection = in;
         break;
     }
 }
}
String incomingConnectionName = incomingConnection.getName();
String incomingStructName = codeGenArgument.getRecordStructName(incomingConnection);

IConnection suspectConnection = null;
boolean hasSuspectConnection = false;
List<? extends IConnection> outgoingSuspectConnections = node.getOutgoingConnections("SUSPECT");
if (outgoingSuspectConnections.size() > 0) {
    suspectConnection = outgoingSuspectConnections.get(0);
}
if(suspectConnection != null){
    hasSuspectConnection = true;
}

IConnection noMatchConnection = null;
boolean hasnoMatchConnection = false;
List<? extends IConnection> outgoingnoMatchConnections = node.getOutgoingConnections("NOMATCH");
if (outgoingnoMatchConnections.size() > 0) {
    noMatchConnection = outgoingnoMatchConnections.get(0);
}
if(noMatchConnection != null){
    hasnoMatchConnection = true;
}

    
/*********** Parameters ************/
String nodeES = ElementParameterParser.getValue(node,"__NODES__");
String[] splitNodeES = nodeES.split(":");
if (splitNodeES.length!=2){
	return "throw new IllegalArgumentException(\"" + splitNodeES +"\");";
}
String host = splitNodeES[0]+"\"";
String port = splitNodeES[1].substring(0,splitNodeES[1].length()-1);
String indexName = ElementParameterParser.getValue(node,"__INDEX__");
// Pairing model
String pairingModelFolder = ElementParameterParser.getValue(node,"__PAIRING_HDFS_FOLDER__");
// Matching model
String mlModelFolder = ElementParameterParser.getValue(node,"__ML_HDFS_FOLDER__");
String uriPrefix = "\"\"";
if(useConfigurationComponent) {
    uriPrefix = org.talend.designer.spark.generator.storage.SparkStorageUtils.getURIPrefix(node);
    pairingModelFolder = uriPrefix + " + " + pairingModelFolder;
    mlModelFolder = uriPrefix + " + " + mlModelFolder;
}
if(!"\"\"".equals(uriPrefix)) {
    
    stringBuffer.append(TEXT_1);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_2);
    stringBuffer.append(uriPrefix);
    stringBuffer.append(TEXT_3);
    
}

String maxWaitSecPerBulk = ElementParameterParser.getValue(node,"__MAX_WAIT_SECONDS_PER_BULK__");
String maxBulkSize = ElementParameterParser.getValue(node,"__MAX_BULK_SIZE__");

    stringBuffer.append(TEXT_4);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_5);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_6);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_7);
    stringBuffer.append(pairingModelFolder);
    stringBuffer.append(TEXT_8);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_9);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_10);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_11);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_12);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_13);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_14);
    
//Get the ml pipeline model,
Boolean savedOnDisk = ElementParameterParser.getValue(node, "__ML_MODEL_LOCATION__").equals("FILE_SYSTEM")? true : false;
if (savedOnDisk) {
    
    stringBuffer.append(TEXT_15);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_16);
    stringBuffer.append(mlModelFolder);
    stringBuffer.append(TEXT_17);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_18);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_19);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_20);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_21);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_22);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_23);
    
} else {
    String mlModelCid = ElementParameterParser.getValue(node, "__ML_MODEL_NAME__");
    
    stringBuffer.append(TEXT_24);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_25);
    stringBuffer.append(mlModelCid);
    stringBuffer.append(TEXT_26);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_27);
    
}
String noMatchLabel = ElementParameterParser.getValue(node,"__NO_MATCH_LABEL__");


String rddName, structName;
if(tSqlRowUtil.containsDateFields(incomingConnection)) {
    // Additional map to convert from java.util.Date to java.sql.Timestamp
        String newRddName = "tmp_rdd_"+incomingConnectionName;
        String newStructName = "DF_"+incomingStructName+"AvroRecord";

    stringBuffer.append(TEXT_28);
    stringBuffer.append(newStructName);
    stringBuffer.append(TEXT_29);
    stringBuffer.append(newRddName);
    stringBuffer.append(TEXT_30);
    stringBuffer.append(incomingConnectionName);
    stringBuffer.append(TEXT_31);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_32);
    stringBuffer.append(incomingStructName);
    stringBuffer.append(TEXT_33);
    stringBuffer.append(newStructName);
    stringBuffer.append(TEXT_34);
    
        rddName = newRddName;
        structName = newStructName;
} else {
        // No need for additional map
        rddName = "rdd_"+incomingConnectionName;
        structName = incomingStructName;
}

    stringBuffer.append(TEXT_35);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_36);
    stringBuffer.append(incomingConnectionName);
    stringBuffer.append(TEXT_37);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_38);
    stringBuffer.append(rddName);
    stringBuffer.append(TEXT_39);
    stringBuffer.append(structName);
    stringBuffer.append(TEXT_40);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_41);
    stringBuffer.append(TEXT_42);
    stringBuffer.append(incomingConnectionName);
    stringBuffer.append(TEXT_43);
    stringBuffer.append(TEXT_44);
    stringBuffer.append(host);
    stringBuffer.append(TEXT_45);
    stringBuffer.append(TEXT_46);
    stringBuffer.append(port);
    stringBuffer.append(TEXT_47);
    stringBuffer.append(TEXT_48);
    stringBuffer.append(indexName);
    stringBuffer.append(TEXT_49);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_50);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_51);
    stringBuffer.append(TEXT_52);
    stringBuffer.append(hasnoMatchConnection);
    stringBuffer.append(TEXT_53);
    stringBuffer.append(TEXT_54);
    stringBuffer.append(noMatchLabel);
    stringBuffer.append(TEXT_55);
    stringBuffer.append(TEXT_56);
    stringBuffer.append(maxWaitSecPerBulk);
    stringBuffer.append(TEXT_57);
    stringBuffer.append(TEXT_58);
    stringBuffer.append(maxBulkSize);
    stringBuffer.append(TEXT_59);
    
if(hasnoMatchConnection){
    String noMatchOutConnectionName = noMatchConnection.getName();
    String noMatchOutStructName = codeGenArgument.getRecordStructName(noMatchConnection);

    stringBuffer.append(TEXT_60);
    stringBuffer.append(noMatchOutStructName);
    stringBuffer.append(TEXT_61);
    stringBuffer.append(noMatchOutConnectionName);
    stringBuffer.append(TEXT_62);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_63);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_64);
    stringBuffer.append(noMatchOutStructName);
    stringBuffer.append(TEXT_65);
    
}

    stringBuffer.append(TEXT_66);
    
if(hasSuspectConnection){
    String suspectOutConnectionName = suspectConnection.getName();
    String suspectOutStructName = codeGenArgument.getRecordStructName(suspectConnection);

    stringBuffer.append(TEXT_67);
    stringBuffer.append(suspectOutStructName);
    stringBuffer.append(TEXT_68);
    stringBuffer.append(suspectOutConnectionName);
    stringBuffer.append(TEXT_69);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_70);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_71);
    stringBuffer.append(suspectOutStructName);
    stringBuffer.append(TEXT_72);
    
}

    stringBuffer.append(TEXT_73);
    stringBuffer.append(TEXT_74);
    return stringBuffer.toString();
  }
}
