package org.talend.designer.codegen.translators.naturallanguageprocessing;

import org.talend.core.model.process.INode;
import org.talend.core.model.process.ElementParameterParser;
import org.talend.core.model.metadata.IMetadataTable;
import org.talend.core.model.metadata.IMetadataColumn;
import org.talend.core.model.process.IConnection;
import org.talend.core.model.process.EConnectionType;
import org.talend.designer.common.BigDataCodeGeneratorArgument;
import org.talend.core.model.metadata.types.JavaTypesManager;
import org.talend.core.model.process.IConnectionCategory;
import java.util.List;
import java.util.Map;
import java.util.ArrayList;

public class TNLPPredictSparkconfigJava
{
  protected static String nl;
  public static synchronized TNLPPredictSparkconfigJava create(String lineSeparator)
  {
    nl = lineSeparator;
    TNLPPredictSparkconfigJava result = new TNLPPredictSparkconfigJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "final List<String> featureColumn_";
  protected final String TEXT_2 = " = new java.util.ArrayList<String>();";
  protected final String TEXT_3 = NL + "    featureColumn_";
  protected final String TEXT_4 = ".add(\"";
  protected final String TEXT_5 = "\");";
  protected final String TEXT_6 = NL + NL + "com.esotericsoftware.kryo.Kryo kryo_";
  protected final String TEXT_7 = " = new com.esotericsoftware.kryo.Kryo();" + NL + "kryo_";
  protected final String TEXT_8 = ".setInstantiatorStrategy(new org.objenesis.strategy.StdInstantiatorStrategy());" + NL + "//if use Folder ( not use file), need to add the default file name " + NL + "    String path_";
  protected final String TEXT_9 = " = ";
  protected final String TEXT_10 = ";" + NL + "\tif(!";
  protected final String TEXT_11 = "){" + NL + "    \tpath_";
  protected final String TEXT_12 = " = path_";
  protected final String TEXT_13 = " + \"/model\";" + NL + "\t}" + NL + "com.esotericsoftware.kryo.io.Input modelInput_";
  protected final String TEXT_14 = " = new com.esotericsoftware.kryo.io.Input(fs.open(new org.apache.hadoop.fs.Path(path_";
  protected final String TEXT_15 = ")));" + NL + "org.talend.dataquality.nlp.NLPProcessing.Model model_";
  protected final String TEXT_16 = " = kryo_";
  protected final String TEXT_17 = ".readObject(modelInput_";
  protected final String TEXT_18 = ", org.talend.dataquality.nlp.NLPProcessing.Model.class);" + NL + "modelInput_";
  protected final String TEXT_19 = ".close();" + NL + "org.talend.dataquality.nlp.FeatureConstructor fc_";
  protected final String TEXT_20 = " = new org.talend.dataquality.nlp.FeatureConstructor(model_";
  protected final String TEXT_21 = ".getToolKit(), model_";
  protected final String TEXT_22 = ".getPipeline())" + NL + ".setPredecessorOccurenceThredhold(100)" + NL + ".setPositionRelativeCompensation(10)" + NL + ".setMostFrequentPercent(0.05);" + NL + "org.apache.spark.api.java.JavaPairRDD<String, List<String>> textTokenPairRDD_";
  protected final String TEXT_23 = " = rdd_";
  protected final String TEXT_24 = ".mapToPair(new ";
  protected final String TEXT_25 = "getTextTokenFunction());" + NL + "org.apache.spark.api.java.JavaRDD<List<String>> otherFeatureRDD_";
  protected final String TEXT_26 = " = rdd_";
  protected final String TEXT_27 = ".map(new ";
  protected final String TEXT_28 = "getOtherFeatureFunction(featureColumn_";
  protected final String TEXT_29 = "));" + NL + "org.talend.dataquality.nlp.NLPProcessing nlp_";
  protected final String TEXT_30 = " = new org.talend.dataquality.nlp.NLPProcessing(fc_";
  protected final String TEXT_31 = ", textTokenPairRDD_";
  protected final String TEXT_32 = ".values(), otherFeatureRDD_";
  protected final String TEXT_33 = ", textTokenPairRDD_";
  protected final String TEXT_34 = ".keys());" + NL + "org.apache.spark.api.java.JavaRDD<";
  protected final String TEXT_35 = "> rdd_";
  protected final String TEXT_36 = " = rdd_";
  protected final String TEXT_37 = ".zip(nlp_";
  protected final String TEXT_38 = ".predict(model_";
  protected final String TEXT_39 = ")).map(new ";
  protected final String TEXT_40 = "constructRowStruct());";

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
     
final BigDataCodeGeneratorArgument codeGenArgument = (BigDataCodeGeneratorArgument) argument;
final INode node = (INode)codeGenArgument.getArgument();
final String cid = node.getUniqueName();
List<? extends IConnection> inConns = node.getIncomingConnections();
List< ? extends IConnection> outConns = node.getOutgoingConnections();
IMetadataTable inputMetadataTable = null;
IMetadataTable outputMetadataTable = null;
String inConnName = "";
String inConnTypeName = "";
String outConnName = "";
String outConnTypeName = "";
List<IMetadataColumn> outputColumns = null;
List<IMetadataColumn> inputColumns = null;
StringBuilder pipeline = new StringBuilder("");
if (inConns.size() == 1){
    inConnName = inConns.get(0).getName();
    inputMetadataTable = inConns.get(0).getMetadataTable();
    inConnTypeName = codeGenArgument.getRecordStructName(inConns.get(0));
    inputColumns = inputMetadataTable.getListColumns();
}
if (outConns.size() == 1){
    outConnName = outConns.get(0).getName();
    outputMetadataTable = outConns.get(0).getMetadataTable();
    outConnTypeName = codeGenArgument.getRecordStructName(outConns.get(0));
    outputColumns = outputMetadataTable.getListColumns();
}
boolean useModelInFile = "true".equals(ElementParameterParser.getValue(node, "__USE_FILE__"));
String hdfsFolder = ElementParameterParser.getValue(node,"__MODEL_PATH__");
boolean useConfigurationComponent = "true".equals(ElementParameterParser.getValue(node, "__DEFINE_STORAGE_CONFIGURATION__"));
if(useConfigurationComponent) {
    String uriPrefix = org.talend.designer.spark.generator.storage.SparkStorageUtils.getURIPrefix(node);
    hdfsFolder = uriPrefix + " + " + hdfsFolder;
}
List<Map<String, String>> otherFeatureSchema = (Boolean)ElementParameterParser.getObjectValue(node, "__IF_OTHER_FEATURE__") ? (ArrayList<Map<String, String>>)ElementParameterParser.getObjectValue(node, "__OTHER_FEATURE_SCHEMA__") : new ArrayList<Map<String, String>>();
List<String> otherFeatureColumn = new ArrayList<String>();
for(Map<String, String> line : otherFeatureSchema){
    otherFeatureColumn.add(line.get("OTHER_FEATURE_COLUMN"));
}

    stringBuffer.append(TEXT_1);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_2);
    
for(String c : otherFeatureColumn){

    stringBuffer.append(TEXT_3);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_4);
    stringBuffer.append(c);
    stringBuffer.append(TEXT_5);
    
}

    stringBuffer.append(TEXT_6);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_7);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_8);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_9);
    stringBuffer.append(hdfsFolder);
    stringBuffer.append(TEXT_10);
    stringBuffer.append(useModelInFile);
    stringBuffer.append(TEXT_11);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_12);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_13);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_14);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_15);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_16);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_17);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_18);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_19);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_20);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_21);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_22);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_23);
    stringBuffer.append(inConnName);
    stringBuffer.append(TEXT_24);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_25);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_26);
    stringBuffer.append(inConnName);
    stringBuffer.append(TEXT_27);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_28);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_29);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_30);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_31);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_32);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_33);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_34);
    stringBuffer.append(outConnTypeName);
    stringBuffer.append(TEXT_35);
    stringBuffer.append(outConnName);
    stringBuffer.append(TEXT_36);
    stringBuffer.append(inConnName);
    stringBuffer.append(TEXT_37);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_38);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_39);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_40);
    return stringBuffer.toString();
  }
}
