package org.talend.designer.codegen.translators.naturallanguageprocessing;

import org.talend.core.model.process.INode;
import org.talend.core.model.process.ElementParameterParser;
import org.talend.core.model.metadata.IMetadataTable;
import org.talend.core.model.metadata.IMetadataColumn;
import org.talend.core.model.process.IConnection;
import org.talend.core.model.process.EConnectionType;
import org.talend.designer.common.BigDataCodeGeneratorArgument;
import org.talend.core.model.metadata.types.JavaTypesManager;
import org.talend.core.model.process.IConnectionCategory;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.ArrayList;
import java.lang.StringBuilder;

public class TNLPModelSparkcodeJava
{
  protected static String nl;
  public static synchronized TNLPModelSparkcodeJava create(String lineSeparator)
  {
    nl = lineSeparator;
    TNLPModelSparkcodeJava result = new TNLPModelSparkcodeJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = NL + "public static class ";
  protected final String TEXT_2 = "getTokenLabelFunction implements org.apache.spark.api.java.function.PairFunction<List<";
  protected final String TEXT_3 = ">, List<String>, List<String>>{" + NL + "    @Override" + NL + "    public scala.Tuple2<List<String>,List<String>> call(List<";
  protected final String TEXT_4 = "> rows){" + NL + "        List<String> tokens = new java.util.ArrayList<String>();" + NL + "        List<String> labels = new java.util.ArrayList<String>();" + NL + "        for(int i = 0;i<rows.size();i++){" + NL + "            tokens.add((String)rows.get(i).get(\"";
  protected final String TEXT_5 = "\"));" + NL + "            labels.add((String)rows.get(i).get(\"";
  protected final String TEXT_6 = "\"));" + NL + "        }" + NL + "        return new scala.Tuple2<>(tokens, labels);" + NL + "    }" + NL + "}" + NL + "" + NL + "public static class ";
  protected final String TEXT_7 = "getFeatureFunction implements org.apache.spark.api.java.function.Function<List<";
  protected final String TEXT_8 = ">, List<String>>{" + NL + "    private List<String> cols;" + NL + "    public ";
  protected final String TEXT_9 = "getFeatureFunction(List<String> cols){" + NL + "        this.cols = cols;" + NL + "    }" + NL + "" + NL + "    @Override" + NL + "    public List<String> call(List<";
  protected final String TEXT_10 = "> rows){" + NL + "        List<String> otherFeature = new java.util.ArrayList<String>();" + NL + "        for(int i = 0;i<rows.size();i++){" + NL + "            StringBuilder f = new StringBuilder(\"\");" + NL + "            for(String col : this.cols){" + NL + "                f.append((String)rows.get(i).get(col)+\"\\t\");" + NL + "            }" + NL + "            otherFeature.add(f.toString());" + NL + "        }" + NL + "        return otherFeature;" + NL + "    }" + NL + "}";

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
     
final BigDataCodeGeneratorArgument codeGenArgument = (BigDataCodeGeneratorArgument) argument;
final INode node = (INode)codeGenArgument.getArgument();
final String cid = node.getUniqueName();

java.util.regex.Pattern p = java.util.regex.Pattern.compile("<PERSON>|<\\/PERSON>");
List<? extends IConnection> inConns = node.getIncomingConnections(EConnectionType.FLOW_MAIN);
List<IMetadataColumn> inputColumns = null;
IMetadataTable inputMetadataTable = null;
String inConnName = "";
String inConnTypeName = "";
if (inConns.size() > 0){
    inConnName = inConns.get(0).getName();
    inConnTypeName = codeGenArgument.getRecordStructName(inConns.get(0));
    inputMetadataTable = inConns.get(0).getMetadataTable();
    inputColumns = inputMetadataTable.getListColumns();
}

    stringBuffer.append(TEXT_1);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_2);
    stringBuffer.append(inConnTypeName);
    stringBuffer.append(TEXT_3);
    stringBuffer.append(inConnTypeName);
    stringBuffer.append(TEXT_4);
    stringBuffer.append(inputColumns.get(0).getLabel());
    stringBuffer.append(TEXT_5);
    stringBuffer.append(inputColumns.get(inputColumns.size()-1).getLabel());
    stringBuffer.append(TEXT_6);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_7);
    stringBuffer.append(inConnTypeName);
    stringBuffer.append(TEXT_8);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_9);
    stringBuffer.append(inConnTypeName);
    stringBuffer.append(TEXT_10);
    return stringBuffer.toString();
  }
}
