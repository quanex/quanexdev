package org.talend.designer.codegen.translators.elt.map.sap;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.talend.core.model.metadata.IMetadataColumn;
import org.talend.core.model.metadata.IMetadataTable;
import org.talend.core.model.metadata.types.JavaType;
import org.talend.core.model.metadata.types.JavaTypesManager;
import org.talend.core.model.process.ElementParameterParser;
import org.talend.core.model.process.IConnection;
import org.talend.core.model.process.IConnectionCategory;
import org.talend.core.model.process.INode;
import org.talend.designer.codegen.config.CodeGeneratorArgument;
import org.talend.core.model.utils.TalendTextUtils;
import org.talend.designer.sapelt.SAPMapComponent;
import org.talend.designer.sapelt.model.emf.sapelt.SapEltData;
import org.talend.designer.sapelt.model.emf.sapelt.InputTable;
import org.talend.designer.sapelt.model.emf.sapelt.OutputTable;
import org.talend.designer.sapelt.language.ScriptGenerationManager;
import org.talend.designer.sapelt.model.emf.sapelt.TableEntry;
import org.talend.designer.sapelt.expressionutils.DataMapExpressionParser;
import org.talend.designer.sapelt.language.SapLanguage;
import org.talend.designer.sapelt.model.tableentry.TableEntryLocation;

public class TELTSAPMapBeginJava
{
  protected static String nl;
  public static synchronized TELTSAPMapBeginJava create(String lineSeparator)
  {
    nl = lineSeparator;
    TELTSAPMapBeginJava result = new TELTSAPMapBeginJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "";
  protected final String TEXT_2 = NL + "\t" + NL + "" + NL + "\t";
  protected final String TEXT_3 = "\t    " + NL + "\t\torg.talend.sap.ISAPConnection connection_";
  protected final String TEXT_4 = " = (org.talend.sap.ISAPConnection)globalMap.get(\"conn_";
  protected final String TEXT_5 = "\");\t";
  protected final String TEXT_6 = NL + "\t\t\t\tif(connection_";
  protected final String TEXT_7 = " == null){" + NL + "\t\t\t\t\tconnection_";
  protected final String TEXT_8 = " = ((org.talend.sap.impl.SAPConnectionFactory)(org.talend.sap.impl.SAPConnectionFactory.getInstance())).createConnection(";
  protected final String TEXT_9 = ");" + NL + "\t\t\t\t}";
  protected final String TEXT_10 = NL + "\t";
  protected final String TEXT_11 = NL + "\t\torg.talend.sap.ISAPConnection connection_";
  protected final String TEXT_12 = " = null;";
  protected final String TEXT_13 = NL + "\t\t\t\tconnection_";
  protected final String TEXT_14 = " = ((org.talend.sap.impl.SAPConnectionFactory)(org.talend.sap.impl.SAPConnectionFactory.getInstance())).createConnection(";
  protected final String TEXT_15 = ");";
  protected final String TEXT_16 = NL + "\t\t\tif (connection_";
  protected final String TEXT_17 = " == null) {//}";
  protected final String TEXT_18 = NL + "\t\t";
  protected final String TEXT_19 = NL + NL + "\tclass PropertyUil_";
  protected final String TEXT_20 = " {" + NL + "\t\t" + NL + "        void validateAndSet(java.util.Properties p, String key, Object value) {" + NL + "        \tif(value==null) {" + NL + "        \t\tSystem.err.println(\"WARN : will ignore the property : \" + key + \" as setting the null value.\"); " + NL + "        \t\treturn;" + NL + "        \t}" + NL + "        \t" + NL + "        \tp.setProperty(key, String.valueOf(value));" + NL + "        }" + NL + "        " + NL + "\t}" + NL + "\t" + NL + "\tPropertyUil_";
  protected final String TEXT_21 = " pu_";
  protected final String TEXT_22 = " = new PropertyUil_";
  protected final String TEXT_23 = "();" + NL + "" + NL + "\t";
  protected final String TEXT_24 = " " + NL + "\tfinal String decryptedPassword_";
  protected final String TEXT_25 = " = routines.system.PasswordEncryptUtil.decryptPassword(";
  protected final String TEXT_26 = ");";
  protected final String TEXT_27 = NL + "\tfinal String decryptedPassword_";
  protected final String TEXT_28 = " = ";
  protected final String TEXT_29 = "; ";
  protected final String TEXT_30 = NL + "\tjava.util.Properties properties_";
  protected final String TEXT_31 = " = new java.util.Properties();" + NL + "    pu_";
  protected final String TEXT_32 = ".validateAndSet(properties_";
  protected final String TEXT_33 = ", org.talend.sap.ISAPConnection.PROP_CLIENT, ";
  protected final String TEXT_34 = ");" + NL + "    pu_";
  protected final String TEXT_35 = ".validateAndSet(properties_";
  protected final String TEXT_36 = ", org.talend.sap.ISAPConnection.PROP_USER, ";
  protected final String TEXT_37 = ");" + NL + "    pu_";
  protected final String TEXT_38 = ".validateAndSet(properties_";
  protected final String TEXT_39 = ", org.talend.sap.ISAPConnection.PROP_PASSWORD, decryptedPassword_";
  protected final String TEXT_40 = ");" + NL + "    pu_";
  protected final String TEXT_41 = ".validateAndSet(properties_";
  protected final String TEXT_42 = ", org.talend.sap.ISAPConnection.PROP_LANGUAGE, ";
  protected final String TEXT_43 = ");" + NL + "    ";
  protected final String TEXT_44 = NL + "    pu_";
  protected final String TEXT_45 = ".validateAndSet(properties_";
  protected final String TEXT_46 = ", org.talend.sap.ISAPConnection.PROP_APPLICATION_SERVER_HOST, ";
  protected final String TEXT_47 = ");" + NL + "    pu_";
  protected final String TEXT_48 = ".validateAndSet(properties_";
  protected final String TEXT_49 = ", org.talend.sap.ISAPConnection.PROP_SYSTEM_NUMBER, ";
  protected final String TEXT_50 = ");";
  protected final String TEXT_51 = NL + "    pu_";
  protected final String TEXT_52 = ".validateAndSet(properties_";
  protected final String TEXT_53 = ", \"jco.client.mshost\", ";
  protected final String TEXT_54 = ");" + NL + "    pu_";
  protected final String TEXT_55 = ".validateAndSet(properties_";
  protected final String TEXT_56 = ", \"jco.client.r3name\", ";
  protected final String TEXT_57 = ");" + NL + "    pu_";
  protected final String TEXT_58 = ".validateAndSet(properties_";
  protected final String TEXT_59 = ", \"jco.client.group\", ";
  protected final String TEXT_60 = ");";
  protected final String TEXT_61 = NL + "    " + NL + "\t";
  protected final String TEXT_62 = NL + "\t\tpu_";
  protected final String TEXT_63 = ".validateAndSet(properties_";
  protected final String TEXT_64 = ", ";
  protected final String TEXT_65 = " ,";
  protected final String TEXT_66 = ");" + NL + "\t\t";
  protected final String TEXT_67 = NL + "        " + NL + "    \tconnection_";
  protected final String TEXT_68 = " = org.talend.sap.impl.SAPConnectionFactory.getInstance().createConnection(properties_";
  protected final String TEXT_69 = ");";
  protected final String TEXT_70 = NL + "\t\t\t//{" + NL + "\t\t\t}";
  protected final String TEXT_71 = NL + "\torg.talend.sap.service.ISAPTableDataService dataService_";
  protected final String TEXT_72 = " = connection_";
  protected final String TEXT_73 = ".getTableDataService();" + NL + "\torg.talend.sap.model.table.ISAPTableJoin join_info_";
  protected final String TEXT_74 = " = dataService_";
  protected final String TEXT_75 = ".createJoin();" + NL + "\tjoin_info_";
  protected final String TEXT_76 = ".fetchSize(";
  protected final String TEXT_77 = ");" + NL + "\tjoin_info_";
  protected final String TEXT_78 = ".from(\"";
  protected final String TEXT_79 = "\");" + NL + "\tjoin_info_";
  protected final String TEXT_80 = ".where(\"";
  protected final String TEXT_81 = "\");" + NL + "\t" + NL + "\t";
  protected final String TEXT_82 = " " + NL + "\tfinal String decrypted_ftp_pwd_";
  protected final String TEXT_83 = " = routines.system.PasswordEncryptUtil.decryptPassword(";
  protected final String TEXT_84 = ");";
  protected final String TEXT_85 = NL + "\tfinal String decrypted_ftp_pwd_";
  protected final String TEXT_86 = " = ";
  protected final String TEXT_87 = "; ";
  protected final String TEXT_88 = NL + "\tjoin_info_";
  protected final String TEXT_89 = ".ftp(org.talend.sap.model.FTP_MODE.";
  protected final String TEXT_90 = ", ";
  protected final String TEXT_91 = ", ";
  protected final String TEXT_92 = ", ";
  protected final String TEXT_93 = ", decrypted_ftp_pwd_";
  protected final String TEXT_94 = ");";
  protected final String TEXT_95 = NL + "\t\tjoin_info_";
  protected final String TEXT_96 = ".addField(\"";
  protected final String TEXT_97 = "\", \"";
  protected final String TEXT_98 = "\", \"";
  protected final String TEXT_99 = "\", 0, ";
  protected final String TEXT_100 = ", org.talend.sap.model.SAPType.";
  protected final String TEXT_101 = ");";
  protected final String TEXT_102 = NL + "\tboolean flag_";
  protected final String TEXT_103 = " = true;" + NL + "\tif(flag_";
  protected final String TEXT_104 = ") {" + NL + "\t\tthrow new RuntimeException(\"";
  protected final String TEXT_105 = "\");" + NL + "\t}";
  protected final String TEXT_106 = NL + "\ttry {" + NL + "\t\torg.talend.sap.model.table.ISAPBatchData data_";
  protected final String TEXT_107 = " = join_info_";
  protected final String TEXT_108 = ".batch(";
  protected final String TEXT_109 = ", ";
  protected final String TEXT_110 = ");" + NL + "\t\t" + NL + "\t\tresourceMap.put(\"data_";
  protected final String TEXT_111 = "\", data_";
  protected final String TEXT_112 = "); " + NL + "\t\t" + NL + "\t \twhile(data_";
  protected final String TEXT_113 = ".nextRow()) {//}";
  protected final String TEXT_114 = NL + "\t\t\t";
  protected final String TEXT_115 = ".";
  protected final String TEXT_116 = " = data_";
  protected final String TEXT_117 = ".getString(";
  protected final String TEXT_118 = ");";
  protected final String TEXT_119 = NL + "\t\t\t";
  protected final String TEXT_120 = ".";
  protected final String TEXT_121 = " = data_";
  protected final String TEXT_122 = ".getInteger(";
  protected final String TEXT_123 = ");";
  protected final String TEXT_124 = NL + "\t\t\t";
  protected final String TEXT_125 = ".";
  protected final String TEXT_126 = " = data_";
  protected final String TEXT_127 = ".getShort(";
  protected final String TEXT_128 = ");";
  protected final String TEXT_129 = NL + "\t\t\t";
  protected final String TEXT_130 = ".";
  protected final String TEXT_131 = " = data_";
  protected final String TEXT_132 = ".getLong(";
  protected final String TEXT_133 = ");";
  protected final String TEXT_134 = NL + "\t\t\t";
  protected final String TEXT_135 = ".";
  protected final String TEXT_136 = " = data_";
  protected final String TEXT_137 = ".getDate(";
  protected final String TEXT_138 = ");";
  protected final String TEXT_139 = NL + "\t\t\t";
  protected final String TEXT_140 = ".";
  protected final String TEXT_141 = " = data_";
  protected final String TEXT_142 = ".getByte(";
  protected final String TEXT_143 = ");";
  protected final String TEXT_144 = NL + "\t\t\t";
  protected final String TEXT_145 = ".";
  protected final String TEXT_146 = " = data_";
  protected final String TEXT_147 = ".getRaw(";
  protected final String TEXT_148 = ");";
  protected final String TEXT_149 = NL + "\t\t\t";
  protected final String TEXT_150 = ".";
  protected final String TEXT_151 = " = data_";
  protected final String TEXT_152 = ".getDouble(";
  protected final String TEXT_153 = ");";
  protected final String TEXT_154 = NL + "\t\t\t";
  protected final String TEXT_155 = ".";
  protected final String TEXT_156 = " = data_";
  protected final String TEXT_157 = ".getFloat(";
  protected final String TEXT_158 = ");";
  protected final String TEXT_159 = NL + "\t\t\tjava.math.BigInteger bi_";
  protected final String TEXT_160 = "_";
  protected final String TEXT_161 = " = data_";
  protected final String TEXT_162 = ".getBigInteger(";
  protected final String TEXT_163 = ");" + NL + "\t\t\t";
  protected final String TEXT_164 = ".";
  protected final String TEXT_165 = " = bi_";
  protected final String TEXT_166 = "_";
  protected final String TEXT_167 = "==null ? null : new java.math.BigDecimal(bi_";
  protected final String TEXT_168 = "_";
  protected final String TEXT_169 = ");";
  protected final String TEXT_170 = NL + "\t\t\t";
  protected final String TEXT_171 = ".";
  protected final String TEXT_172 = " = data_";
  protected final String TEXT_173 = ".getBigDecimal(";
  protected final String TEXT_174 = ");";
  protected final String TEXT_175 = NL + "\t\t\t";
  protected final String TEXT_176 = ".";
  protected final String TEXT_177 = " = data_";
  protected final String TEXT_178 = ".getBigInteger(";
  protected final String TEXT_179 = ");\t\t";
  protected final String TEXT_180 = NL + "\t\t\t";
  protected final String TEXT_181 = ".";
  protected final String TEXT_182 = " = data_";
  protected final String TEXT_183 = ".getString(";
  protected final String TEXT_184 = ");";
  protected final String TEXT_185 = NL + "\t\t\t";
  protected final String TEXT_186 = ".";
  protected final String TEXT_187 = " = ParserUtils.parseTo_";
  protected final String TEXT_188 = "(data_";
  protected final String TEXT_189 = ".getString(";
  protected final String TEXT_190 = "));";

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    stringBuffer.append(TEXT_1);
    
	CodeGeneratorArgument codeGenArgument = (CodeGeneratorArgument) argument;
	
	SAPMapComponent node = (SAPMapComponent) codeGenArgument.getArgument();
	SapEltData sapdata = node.getExternalEmfData();
	List<InputTable> inputTables = sapdata.getInputTables();
	List<OutputTable> outputTables = sapdata.getOutputTables();
	
	ScriptGenerationManager gm = node.getGenerationManager();
	
	String cid = node.getUniqueName();
	
	List<? extends IConnection> outputConnections = node.getOutgoingSortedConnections();
	if((outputConnections == null) || (outputConnections.size() == 0)) {
		return "";
	}
	IConnection outputConnection = outputConnections.get(0);
	
	if(!outputConnection.getLineStyle().hasConnectionCategory(IConnectionCategory.DATA)) {
		return "";
	}
	
	OutputTable outputTable = null;
	
	if(outputTables==null || outputTables.isEmpty()) {
		return "";
	}
	
	for(OutputTable ot : outputTables) {
		if(ot.getName().equals(outputConnection.getUniqueName())) {
			outputTable = ot;
		}
	}
	
	if(outputTable == null) {
		return "";
	}
	
	List<TableEntry> tableEntries = outputTable.getTableEntries();
	
	if(tableEntries == null || tableEntries.isEmpty()) {
		return "";
	}
	
	Map<String, TableEntry> entryMap = new HashMap<String, TableEntry>();
	for(TableEntry entry : tableEntries) {
		entryMap.put(entry.getName(), entry);
	}
	
	IMetadataTable outputMetadata = outputConnection.getMetadataTable();
	if(outputMetadata == null) {
		return "";
	}
	
	List<IMetadataColumn> columnList = outputMetadata.getListColumns();
	
	if(columnList == null || columnList.isEmpty()) {
		return "";
	}
	
	String client = ElementParameterParser.getValue(node, "__CLIENT__");
	String userid = ElementParameterParser.getValue(node, "__USERID__");
	String password = ElementParameterParser.getValue(node, "__PASSWORD__");
	String language = ElementParameterParser.getValue(node, "__LANGUAGE__");
	String hostname = ElementParameterParser.getValue(node, "__HOSTNAME__");
	String systemnumber = ElementParameterParser.getValue(node, "__SYSTEMNUMBER__");
	
	String systemId = ElementParameterParser.getValue(node,"__SYSTEMID__");
	String groupName = ElementParameterParser.getValue(node,"__GROUPNAME__");
	
	String serverType = ElementParameterParser.getValue(node,"__SERVERTYPE__");
	
	String file_protocol = ElementParameterParser.getValue(node,"__FILE_PROTOCOL__");
	
	List<Map<String, String>> sapProps = (List<Map<String,String>>)ElementParameterParser.getObjectValue(node, "__SAP_PROPERTIES__");
	
	String passwordFieldName = "__PASSWORD__";
	
    boolean useExistingConn = ("true").equals(ElementParameterParser.getValue(node,"__USE_EXISTING_CONNECTION__"));
	String connection = ElementParameterParser.getValue(node,"__CONNECTION__");

    stringBuffer.append(TEXT_2);
    if(useExistingConn){
    stringBuffer.append(TEXT_3);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_4);
    stringBuffer.append(connection );
    stringBuffer.append(TEXT_5);
    	
		INode connectionNode = null; 
		for (INode processNode : node.getProcess().getGeneratingNodes()) { 
			if(connection.equals(processNode.getUniqueName())) { 
				connectionNode = processNode; 
				break; 
			} 
		} 
		boolean specify_alias = "true".equals(ElementParameterParser.getValue(connectionNode, "__SPECIFY_DATASOURCE_ALIAS__"));
		if(specify_alias){
			String alias = ElementParameterParser.getValue(connectionNode, "__SAP_DATASOURCE_ALIAS__");
			if(null != alias && !("".equals(alias))){

    stringBuffer.append(TEXT_6);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_7);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_8);
    stringBuffer.append(alias);
    stringBuffer.append(TEXT_9);
    
			}
		}

    stringBuffer.append(TEXT_10);
    }else{
    stringBuffer.append(TEXT_11);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_12);
    
		boolean specify_alias = "true".equals(ElementParameterParser.getValue(node, "__SPECIFY_DATASOURCE_ALIAS__"));
		if(specify_alias){
			String alias = ElementParameterParser.getValue(node, "__SAP_DATASOURCE_ALIAS__");
			if(null != alias && !("".equals(alias))){

    stringBuffer.append(TEXT_13);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_14);
    stringBuffer.append(alias);
    stringBuffer.append(TEXT_15);
    
			}

    stringBuffer.append(TEXT_16);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_17);
    
		}

    stringBuffer.append(TEXT_18);
    stringBuffer.append(TEXT_19);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_20);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_21);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_22);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_23);
    if (ElementParameterParser.canEncrypt(node, passwordFieldName)) {
    stringBuffer.append(TEXT_24);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_25);
    stringBuffer.append(ElementParameterParser.getEncryptedValue(node, passwordFieldName));
    stringBuffer.append(TEXT_26);
    } else {
    stringBuffer.append(TEXT_27);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_28);
    stringBuffer.append( ElementParameterParser.getValue(node, passwordFieldName));
    stringBuffer.append(TEXT_29);
    }
    stringBuffer.append(TEXT_30);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_31);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_32);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_33);
    stringBuffer.append(client);
    stringBuffer.append(TEXT_34);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_35);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_36);
    stringBuffer.append(userid);
    stringBuffer.append(TEXT_37);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_38);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_39);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_40);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_41);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_42);
    stringBuffer.append(language);
    stringBuffer.append(TEXT_43);
    if("ApplicationServer".equals(serverType)){
    stringBuffer.append(TEXT_44);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_45);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_46);
    stringBuffer.append(hostname);
    stringBuffer.append(TEXT_47);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_48);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_49);
    stringBuffer.append(systemnumber);
    stringBuffer.append(TEXT_50);
    }else{
    stringBuffer.append(TEXT_51);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_52);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_53);
    stringBuffer.append(hostname);
    stringBuffer.append(TEXT_54);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_55);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_56);
    stringBuffer.append(systemId);
    stringBuffer.append(TEXT_57);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_58);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_59);
    stringBuffer.append(groupName);
    stringBuffer.append(TEXT_60);
    }
    stringBuffer.append(TEXT_61);
    
    if(sapProps!=null) {
		for(Map<String, String> item : sapProps){
		
    stringBuffer.append(TEXT_62);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_63);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_64);
    stringBuffer.append(item.get("PROPERTY") );
    stringBuffer.append(TEXT_65);
    stringBuffer.append(item.get("VALUE") );
    stringBuffer.append(TEXT_66);
     
		}
    }
	
    stringBuffer.append(TEXT_67);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_68);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_69);
    
		if(specify_alias){

    stringBuffer.append(TEXT_70);
    
		}
	}
	
	String batch_size = ElementParameterParser.getValue(node, "__BATCH_SIZE__");
	
	String ftp_host = ElementParameterParser.getValue(node, "__FTP_HOST__");
	String ftp_port = ElementParameterParser.getValue(node, "__FTP_PORT__");
	String ftp_user = ElementParameterParser.getValue(node, "__FTP_USER__");
	
	String ftp_pass_field_name = "__FTP_PASS__";
	
	String ftp_dir = ElementParameterParser.getValue(node, "__FTP_DIR__");
	String generated_filename_prefix = ElementParameterParser.getValue(node, "__GENERATED_FILENAME_PREFIX__");

    stringBuffer.append(TEXT_71);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_72);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_73);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_74);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_75);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_76);
    stringBuffer.append(batch_size);
    stringBuffer.append(TEXT_77);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_78);
    stringBuffer.append(gm.getFromStatment(outputTable.getName()));
    stringBuffer.append(TEXT_79);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_80);
    stringBuffer.append(gm.getWhereClause(outputTable.getName()));
    stringBuffer.append(TEXT_81);
    if (ElementParameterParser.canEncrypt(node, ftp_pass_field_name)) {
    stringBuffer.append(TEXT_82);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_83);
    stringBuffer.append(ElementParameterParser.getEncryptedValue(node, ftp_pass_field_name));
    stringBuffer.append(TEXT_84);
    } else {
    stringBuffer.append(TEXT_85);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_86);
    stringBuffer.append( ElementParameterParser.getValue(node, ftp_pass_field_name));
    stringBuffer.append(TEXT_87);
    }
    stringBuffer.append(TEXT_88);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_89);
    stringBuffer.append(file_protocol);
    stringBuffer.append(TEXT_90);
    stringBuffer.append(ftp_host);
    stringBuffer.append(TEXT_91);
    stringBuffer.append(ftp_port);
    stringBuffer.append(TEXT_92);
    stringBuffer.append(ftp_user);
    stringBuffer.append(TEXT_93);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_94);
    
	DataMapExpressionParser dataMapExpressionParser = new DataMapExpressionParser(new SapLanguage());
	
	String errorMessage = null;
	for(int i=0;i<columnList.size();i++) {
		IMetadataColumn column = columnList.get(i);
		
		String columnName = column.getLabel();
		
		TableEntry entry = entryMap.get(columnName);
		
		if(entry == null) {
			errorMessage = "talend schema is not the same with the output table in the TELTSAPMap, please check";
			break;
		}
		
		String expression = entry.getExpression();
		
		if(expression == null) {
			errorMessage = "miss the expression the output table in the TELTSAPMap, please check";
			break;
		}
		
		String sapType = column.getType();
		if((sapType == null) || "".equals(sapType)) {
			sapType = "STRING";
		}
		
		Integer length = column.getLength();
		if(length == null) {
			length = 0;
		}
		
		TableEntryLocation[] locations = dataMapExpressionParser.parseTableEntryLocations(expression);
		if(locations==null || locations.length!=1) {
			errorMessage = "some expression is wrong in the output table, please check";
			break;
		}
		
		TableEntryLocation location = locations[0];
		String alias = gm.getAlias(outputTable.getName(), i);
		alias = (alias == null ? location.columnName : alias);

    stringBuffer.append(TEXT_95);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_96);
    stringBuffer.append(location.tableName);
    stringBuffer.append(TEXT_97);
    stringBuffer.append(location.columnName);
    stringBuffer.append(TEXT_98);
    stringBuffer.append(alias);
    stringBuffer.append(TEXT_99);
    stringBuffer.append(length);
    stringBuffer.append(TEXT_100);
    stringBuffer.append(sapType);
    stringBuffer.append(TEXT_101);
    
	}
	
	if(errorMessage!=null) {

    stringBuffer.append(TEXT_102);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_103);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_104);
    stringBuffer.append(errorMessage);
    stringBuffer.append(TEXT_105);
    
	}
	

    stringBuffer.append(TEXT_106);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_107);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_108);
    stringBuffer.append(ftp_dir);
    stringBuffer.append(TEXT_109);
    stringBuffer.append(generated_filename_prefix);
    stringBuffer.append(TEXT_110);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_111);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_112);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_113);
    
	for(int i=0;i<columnList.size();i++) {
		IMetadataColumn column = columnList.get(i);
	    String typeToGenerate = JavaTypesManager.getTypeToGenerate(column.getTalendType(),column.isNullable());
	    JavaType javaType = JavaTypesManager.getJavaTypeFromId(column.getTalendType());
	    String dbType = column.getType();
	    
		if(javaType == JavaTypesManager.STRING) {

    stringBuffer.append(TEXT_114);
    stringBuffer.append(outputConnection.getName() );
    stringBuffer.append(TEXT_115);
    stringBuffer.append(column.getLabel() );
    stringBuffer.append(TEXT_116);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_117);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_118);
    
		} else if(javaType == JavaTypesManager.INTEGER) {

    stringBuffer.append(TEXT_119);
    stringBuffer.append(outputConnection.getName() );
    stringBuffer.append(TEXT_120);
    stringBuffer.append(column.getLabel() );
    stringBuffer.append(TEXT_121);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_122);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_123);
    
		} else if(javaType == JavaTypesManager.SHORT) {

    stringBuffer.append(TEXT_124);
    stringBuffer.append(outputConnection.getName() );
    stringBuffer.append(TEXT_125);
    stringBuffer.append(column.getLabel() );
    stringBuffer.append(TEXT_126);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_127);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_128);
    
		} else if(javaType == JavaTypesManager.LONG) {

    stringBuffer.append(TEXT_129);
    stringBuffer.append(outputConnection.getName() );
    stringBuffer.append(TEXT_130);
    stringBuffer.append(column.getLabel() );
    stringBuffer.append(TEXT_131);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_132);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_133);
    
		} else if(javaType == JavaTypesManager.DATE) {

    stringBuffer.append(TEXT_134);
    stringBuffer.append(outputConnection.getName() );
    stringBuffer.append(TEXT_135);
    stringBuffer.append(column.getLabel() );
    stringBuffer.append(TEXT_136);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_137);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_138);
    
		} else if(javaType == JavaTypesManager.BYTE) {

    stringBuffer.append(TEXT_139);
    stringBuffer.append(outputConnection.getName() );
    stringBuffer.append(TEXT_140);
    stringBuffer.append(column.getLabel() );
    stringBuffer.append(TEXT_141);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_142);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_143);
    
		} else if(javaType == JavaTypesManager.BYTE_ARRAY) {

    stringBuffer.append(TEXT_144);
    stringBuffer.append(outputConnection.getName() );
    stringBuffer.append(TEXT_145);
    stringBuffer.append(column.getLabel() );
    stringBuffer.append(TEXT_146);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_147);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_148);
    
		} else if(javaType == JavaTypesManager.DOUBLE) {

    stringBuffer.append(TEXT_149);
    stringBuffer.append(outputConnection.getName() );
    stringBuffer.append(TEXT_150);
    stringBuffer.append(column.getLabel() );
    stringBuffer.append(TEXT_151);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_152);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_153);
    
		} else if(javaType == JavaTypesManager.FLOAT) {

    stringBuffer.append(TEXT_154);
    stringBuffer.append(outputConnection.getName() );
    stringBuffer.append(TEXT_155);
    stringBuffer.append(column.getLabel() );
    stringBuffer.append(TEXT_156);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_157);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_158);
    
		} else if(javaType == JavaTypesManager.BIGDECIMAL) {
			if("BIG_INTEGER".equals(dbType)) {

    stringBuffer.append(TEXT_159);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_160);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_161);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_162);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_163);
    stringBuffer.append(outputConnection.getName() );
    stringBuffer.append(TEXT_164);
    stringBuffer.append(column.getLabel() );
    stringBuffer.append(TEXT_165);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_166);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_167);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_168);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_169);
    
			} else {

    stringBuffer.append(TEXT_170);
    stringBuffer.append(outputConnection.getName() );
    stringBuffer.append(TEXT_171);
    stringBuffer.append(column.getLabel() );
    stringBuffer.append(TEXT_172);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_173);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_174);
    
			}
		} else if(javaType == JavaTypesManager.OBJECT && "BIG_INTEGER".equals(dbType)) {

    stringBuffer.append(TEXT_175);
    stringBuffer.append(outputConnection.getName() );
    stringBuffer.append(TEXT_176);
    stringBuffer.append(column.getLabel() );
    stringBuffer.append(TEXT_177);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_178);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_179);
    
		} else if(javaType == JavaTypesManager.OBJECT) {

    stringBuffer.append(TEXT_180);
    stringBuffer.append(outputConnection.getName() );
    stringBuffer.append(TEXT_181);
    stringBuffer.append(column.getLabel() );
    stringBuffer.append(TEXT_182);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_183);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_184);
    
		} else {

    stringBuffer.append(TEXT_185);
    stringBuffer.append(outputConnection.getName() );
    stringBuffer.append(TEXT_186);
    stringBuffer.append(column.getLabel() );
    stringBuffer.append(TEXT_187);
    stringBuffer.append(typeToGenerate);
    stringBuffer.append(TEXT_188);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_189);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_190);
    
		}
	}

    return stringBuffer.toString();
  }
}
