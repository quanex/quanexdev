package org.talend.designer.codegen.translators.databases.bigquery;

import org.talend.core.model.metadata.IMetadataColumn;
import org.talend.core.model.metadata.IMetadataTable;
import org.talend.core.model.process.ElementParameterParser;
import org.talend.core.model.process.IConnection;
import org.talend.core.model.process.IConnection;
import org.talend.core.model.process.INode;
import org.talend.designer.common.BigDataCodeGeneratorArgument;
import org.talend.designer.common.tsqlrow.TSqlRowUtil;
import org.talend.designer.spark.generator.storage.MongoDBSparkStorage;

public class TBigQueryOutputSparkconfigJava
{
  protected static String nl;
  public static synchronized TBigQueryOutputSparkconfigJava create(String lineSeparator)
  {
    nl = lineSeparator;
    TBigQueryOutputSparkconfigJava result = new TBigQueryOutputSparkconfigJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "\tif(true) {" + NL + "\t\tthrow new RuntimeException(\"";
  protected final String TEXT_2 = " component needs a tBigQueryConfiguration to be defined within the job.\");\t" + NL + "\t}";
  protected final String TEXT_3 = NL + "    \t";
  protected final String TEXT_4 = NL + "\t\t    org.apache.hadoop.conf.Configuration config_";
  protected final String TEXT_5 = " = ctx.sparkContext().hadoopConfiguration();" + NL + "\t\t";
  protected final String TEXT_6 = NL + "\t\t    org.apache.hadoop.conf.Configuration config_";
  protected final String TEXT_7 = " = ctx.hadoopConfiguration();" + NL + "\t\t";
  protected final String TEXT_8 = NL + "    \t" + NL + "    \t" + NL + "    \t";
  protected final String TEXT_9 = "    " + NL + "    \t" + NL + "    \tString schemaStr_";
  protected final String TEXT_10 = " = \"";
  protected final String TEXT_11 = "\";" + NL + "    \tString projectId_";
  protected final String TEXT_12 = " = config_";
  protected final String TEXT_13 = ".get(\"mapred.bq.project.id\");" + NL + "    " + NL + "    \t";
  protected final String TEXT_14 = NL + "\t\t\tcom.google.cloud.hadoop.io.bigquery.BigQueryConfiguration.configureBigQueryOutput(config_";
  protected final String TEXT_15 = ", projectId_";
  protected final String TEXT_16 = ", ";
  protected final String TEXT_17 = ", ";
  protected final String TEXT_18 = ", schemaStr_";
  protected final String TEXT_19 = ");" + NL + "\t    \tconfig_";
  protected final String TEXT_20 = ".set(\"mapreduce.job.outputformat.class\", com.google.cloud.hadoop.io.bigquery.BigQueryOutputFormat.class.getName());" + NL + "\t\t";
  protected final String TEXT_21 = NL + "\t    \tcom.google.api.services.bigquery.model.TableSchema tableSchema_";
  protected final String TEXT_22 = " = new com.google.api.services.bigquery.model.TableSchema();" + NL + "\t    \ttableSchema_";
  protected final String TEXT_23 = ".setFields(com.google.cloud.hadoop.io.bigquery.BigQueryUtils.getSchemaFromString(schemaStr_";
  protected final String TEXT_24 = "));" + NL + "\t    \tcom.google.cloud.hadoop.io.bigquery.output.BigQueryOutputConfiguration.configure(config_";
  protected final String TEXT_25 = ", " + NL + "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tprojectId_";
  protected final String TEXT_26 = ", " + NL + "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
  protected final String TEXT_27 = ", " + NL + "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
  protected final String TEXT_28 = ", " + NL + "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\ttableSchema_";
  protected final String TEXT_29 = ", " + NL + "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
  protected final String TEXT_30 = " + java.util.UUID.randomUUID(), " + NL + "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tcom.google.cloud.hadoop.io.bigquery.BigQueryFileFormat.NEWLINE_DELIMITED_JSON, " + NL + "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\torg.apache.hadoop.mapreduce.lib.output.TextOutputFormat.class);" + NL + "\t\t\t" + NL + "\t\t\tconfig_";
  protected final String TEXT_31 = ".set(\"mapreduce.job.outputformat.class\", com.google.cloud.hadoop.io.bigquery.output.IndirectBigQueryOutputFormat.class.getName());" + NL + "\t\t\t";
  protected final String TEXT_32 = NL + "\t\t\t";
  protected final String TEXT_33 = NL + "\t\t\t\tconfig_";
  protected final String TEXT_34 = ".set(com.google.cloud.hadoop.io.bigquery.BigQueryConfiguration.OUTPUT_TABLE_WRITE_DISPOSITION_KEY, \"WRITE_TRUNCATE\");" + NL + "\t\t\t";
  protected final String TEXT_35 = "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t" + NL + "\t\t\t";
  protected final String TEXT_36 = NL + "\t\t";
  protected final String TEXT_37 = NL + "    " + NL + "    \t";
  protected final String TEXT_38 = NL + "\t\t    org.apache.spark.streaming.api.java.JavaPairDStream<Object, com.google.gson.JsonObject> gsonDS_";
  protected final String TEXT_39 = " = ";
  protected final String TEXT_40 = ".mapToPair(new ";
  protected final String TEXT_41 = "_From";
  protected final String TEXT_42 = "ToGson(job));" + NL + "\t\t";
  protected final String TEXT_43 = NL + "\t\t    org.apache.spark.api.java.JavaPairRDD<Object, com.google.gson.JsonObject> gsonRDD_";
  protected final String TEXT_44 = " = ";
  protected final String TEXT_45 = ".mapToPair(new ";
  protected final String TEXT_46 = "_From";
  protected final String TEXT_47 = "ToGson(job));" + NL + "\t\t";
  protected final String TEXT_48 = NL + "\t\t    gsonDS_";
  protected final String TEXT_49 = ".foreachRDD(new ";
  protected final String TEXT_50 = "_ForeachRDDOutput(config_";
  protected final String TEXT_51 = "));" + NL + "\t\t";
  protected final String TEXT_52 = NL + "\t\t    gsonRDD_";
  protected final String TEXT_53 = ".saveAsNewAPIHadoopDataset(config_";
  protected final String TEXT_54 = ");" + NL + "\t\t";
  protected final String TEXT_55 = NL;

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    
BigDataCodeGeneratorArgument codeGenArgument = (BigDataCodeGeneratorArgument) argument;
INode node = (INode)codeGenArgument.getArgument();
String cid = node.getUniqueName();

TSqlRowUtil tSqlRowUtil = new TSqlRowUtil(node);
String validateError = tSqlRowUtil.validate(true, false);
if (validateError != null) {
    // Cause the job compilation to explicitly fail if there is a problem.
    return "throw new JobConfigurationError(\"" + validateError +"\");";
}

IConnection componentIncomingConnection = tSqlRowUtil.getIncomingConnections().get(0);
String inStructName = codeGenArgument.getRecordStructName(componentIncomingConnection);
String inRddName = "rdd_"+componentIncomingConnection.getName();

String dataset = ElementParameterParser.getValue(node,"__BIGQUERY_DATASET__");
String table = ElementParameterParser.getValue(node,"__BIGQUERY_TABLE__");

java.util.List<? extends INode> configurationNodes = node.getProcess().getNodesOfType("tBigQueryConfiguration");
INode configurationNode = null;
if (configurationNodes != null && configurationNodes.size() > 0) {
    configurationNode = configurationNodes.get(0);
} else {

    stringBuffer.append(TEXT_1);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_2);
    
}
String bqGCSTempPath = ElementParameterParser.getValue(configurationNode, "__EXECUTE_GCP_STORAGE_TEMP_PATH__");

java.util.List<IMetadataTable> metadatas = node.getMetadataList();

if(metadatas != null && metadatas.size() > 0){
    IMetadataTable metadata = metadatas.get(0);
    if(metadata != null){
    
    stringBuffer.append(TEXT_3);
    
    	if("SPARKSTREAMING".equals(node.getComponent().getType())) {
		
    stringBuffer.append(TEXT_4);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_5);
    
		} else {
		
    stringBuffer.append(TEXT_6);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_7);
    
		}
		
    stringBuffer.append(TEXT_8);
    
    	java.util.List<IMetadataColumn> columnList = metadata.getListColumns();
        int sizeColumns = columnList.size();
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for (int i = 0; i < sizeColumns; i++) {
            IMetadataColumn column = columnList.get(i);
            boolean isNullable = column.isNullable();
            String dbType = column.getType();
            sb.append("{");
            sb.append("\\\"name\\\" : \\\"" + column.getOriginalDbColumnName() + "\\\",");
            
            boolean isArray = false;
            if("ARRAY".equals(dbType)){
            	isArray = true;
            }
            
            sb.append("\\\"type\\\" : \\\"" + (isArray ? "STRING" : dbType) + "\\\",");
            sb.append("\\\"mode\\\" : \\\"" + (isArray ? "REPEATED" : (isNullable ? "NULLABLE" : "REQUIRED")) + "\\\"");
            sb.append("}");
            if(i + 1 != sizeColumns){
            	sb.append(",");
            }
        }
        sb.append("]");
        
    stringBuffer.append(TEXT_9);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_10);
    stringBuffer.append(sb.toString());
    stringBuffer.append(TEXT_11);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_12);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_13);
    
    	if("SPARKSTREAMING".equals(node.getComponent().getType())) {
		
    stringBuffer.append(TEXT_14);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_15);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_16);
    stringBuffer.append(dataset);
    stringBuffer.append(TEXT_17);
    stringBuffer.append(table);
    stringBuffer.append(TEXT_18);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_19);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_20);
    
		}else{
		
    stringBuffer.append(TEXT_21);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_22);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_23);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_24);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_25);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_26);
    stringBuffer.append(dataset);
    stringBuffer.append(TEXT_27);
    stringBuffer.append(table);
    stringBuffer.append(TEXT_28);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_29);
    stringBuffer.append(bqGCSTempPath);
    stringBuffer.append(TEXT_30);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_31);
    String tableOperation = ElementParameterParser.getValue(node,"__TABLE_OPERATION__"); 
    stringBuffer.append(TEXT_32);
    if("TRUNCATE".equals(tableOperation)){
    stringBuffer.append(TEXT_33);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_34);
    }else{//CREATE IF NEEDED and APPEND, which is default operation
    stringBuffer.append(TEXT_35);
    }
    stringBuffer.append(TEXT_36);
    
		}
		
    stringBuffer.append(TEXT_37);
    
    	if("SPARKSTREAMING".equals(node.getComponent().getType())) {
		
    stringBuffer.append(TEXT_38);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_39);
    stringBuffer.append(inRddName);
    stringBuffer.append(TEXT_40);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_41);
    stringBuffer.append(inStructName);
    stringBuffer.append(TEXT_42);
    
		} else {
		
    stringBuffer.append(TEXT_43);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_44);
    stringBuffer.append(inRddName);
    stringBuffer.append(TEXT_45);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_46);
    stringBuffer.append(inStructName);
    stringBuffer.append(TEXT_47);
    
		}
		
		if("SPARKSTREAMING".equals(node.getComponent().getType())) {
		
    stringBuffer.append(TEXT_48);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_49);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_50);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_51);
    
		}else{
	    
    stringBuffer.append(TEXT_52);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_53);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_54);
    
		}
    }
	// No Schema
} else {
}

    stringBuffer.append(TEXT_55);
    return stringBuffer.toString();
  }
}
