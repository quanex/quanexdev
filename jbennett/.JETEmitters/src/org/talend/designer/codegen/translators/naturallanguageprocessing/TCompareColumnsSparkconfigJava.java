package org.talend.designer.codegen.translators.naturallanguageprocessing;

import org.talend.core.model.process.INode;
import org.talend.core.model.process.ElementParameterParser;
import org.talend.core.model.metadata.IMetadataTable;
import org.talend.core.model.metadata.IMetadataColumn;
import org.talend.core.model.process.IConnection;
import org.talend.core.model.process.EConnectionType;
import org.talend.designer.common.BigDataCodeGeneratorArgument;
import org.talend.core.model.metadata.types.JavaTypesManager;
import org.talend.core.model.process.IConnectionCategory;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.HashSet;

public class TCompareColumnsSparkconfigJava
{
  protected static String nl;
  public static synchronized TCompareColumnsSparkconfigJava create(String lineSeparator)
  {
    nl = lineSeparator;
    TCompareColumnsSparkconfigJava result = new TCompareColumnsSparkconfigJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "List<String> ";
  protected final String TEXT_2 = "opts = new java.util.ArrayList<String>();";
  protected final String TEXT_3 = NL + "    ";
  protected final String TEXT_4 = "opts.add(\"";
  protected final String TEXT_5 = "\");";
  protected final String TEXT_6 = NL + "org.apache.spark.api.java.JavaRDD<";
  protected final String TEXT_7 = "> rdd_";
  protected final String TEXT_8 = " = rdd_";
  protected final String TEXT_9 = ".map(new ";
  protected final String TEXT_10 = "compare(";
  protected final String TEXT_11 = "opts));";

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
     
final BigDataCodeGeneratorArgument codeGenArgument = (BigDataCodeGeneratorArgument) argument;
final INode node = (INode)codeGenArgument.getArgument();
final String cid = node.getUniqueName();
List<? extends IConnection> inConns = node.getIncomingConnections();
List< ? extends IConnection> outConns = node.getOutgoingConnections();
IMetadataTable inputMetadataTable = null;
IMetadataTable outputMetadataTable = null;
String inConnName = "";
String inConnTypeName = "";
String outConnName = "";
String outConnTypeName = "";
List<IMetadataColumn> outputColumns = null;
List<IMetadataColumn> inputColumns = null;
StringBuilder pipeline = new StringBuilder("");
if (inConns.size() == 1){
    inConnName = inConns.get(0).getUniqueName();
    inputMetadataTable = inConns.get(0).getMetadataTable();
    inConnTypeName = codeGenArgument.getRecordStructName(inConns.get(0));
    inputColumns = inputMetadataTable.getListColumns();
}
if (outConns.size() == 1){
    outConnName = outConns.get(0).getName();
    outputMetadataTable = outConns.get(0).getMetadataTable();
    outConnTypeName = codeGenArgument.getRecordStructName(outConns.get(0));
    outputColumns = outputMetadataTable.getListColumns();
}
List<Map<String, String>> conf = (List<Map<String, String>>) ElementParameterParser.getObjectValue(node, "__COMPARE_COLUMNS__");
List<String> operations = new ArrayList<String>();
List<String> cols = new ArrayList<String>();
for(IMetadataColumn c : outputColumns){
    cols.add(c.getLabel());
}
for(Map<String, String> map : conf){
    operations.add(cols.indexOf(map.get("MAIN_COLUMN"))+"\t"+cols.indexOf(map.get("REF_COLUMN"))+"\t"+map.get("ALGO_LIST")+"\t"+cols.indexOf(map.get("OUTPUT_COLUMN")));
}
int numInput = inputColumns.size();

    stringBuffer.append(TEXT_1);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_2);
    
for(String opt : operations){

    stringBuffer.append(TEXT_3);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_4);
    stringBuffer.append(opt);
    stringBuffer.append(TEXT_5);
    
}

    stringBuffer.append(TEXT_6);
    stringBuffer.append(outConnTypeName);
    stringBuffer.append(TEXT_7);
    stringBuffer.append(outConnName);
    stringBuffer.append(TEXT_8);
    stringBuffer.append(inConnName);
    stringBuffer.append(TEXT_9);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_10);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_11);
    return stringBuffer.toString();
  }
}
