package org.talend.designer.codegen.translators.messaging.maprstreams;

import java.util.Map.Entry;
import org.talend.core.model.process.INode;
import org.talend.designer.common.BigDataCodeGeneratorArgument;
import org.talend.designer.common.tmaprstreamsinput.TMapRStreamsInputUtil;
import org.talend.hadoop.distribution.kafka.SparkStreamingKafkaVersion;

public class TMapRStreamsInputSparkstreamingconfigJava
{
  protected static String nl;
  public static synchronized TMapRStreamsInputSparkstreamingconfigJava create(String lineSeparator)
  {
    nl = lineSeparator;
    TMapRStreamsInputSparkstreamingconfigJava result = new TMapRStreamsInputSparkstreamingconfigJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "";
  protected final String TEXT_2 = NL + "\t";
  protected final String TEXT_3 = NL;
  protected final String TEXT_4 = NL;
  protected final String TEXT_5 = NL + "\t\tjava.util.Map<String, String> ";
  protected final String TEXT_6 = "_properties = new java.util.HashMap<String, String>();" + NL + "\t\t";
  protected final String TEXT_7 = "_properties.put(\"bootstrap.servers\", \"ignored by MapR Streams\");";
  protected final String TEXT_8 = NL + "\t\t\t";
  protected final String TEXT_9 = "_properties.put(";
  protected final String TEXT_10 = ", ";
  protected final String TEXT_11 = ");";
  protected final String TEXT_12 = NL + "\t\t";
  protected final String TEXT_13 = "_properties.put(org.apache.kafka.clients.consumer.ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, \"";
  protected final String TEXT_14 = "\");" + NL + "\t\t";
  protected final String TEXT_15 = "_properties.put(\"serializer.encoding\", ";
  protected final String TEXT_16 = ");" + NL + "\t\t";
  protected final String TEXT_17 = "_properties.put(org.apache.kafka.clients.consumer.ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, ";
  protected final String TEXT_18 = "_KeyDeserializer.class.getName());" + NL + "\t\t";
  protected final String TEXT_19 = "_properties.put(org.apache.kafka.clients.consumer.ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, ";
  protected final String TEXT_20 = "_ValueDeserializer.class.getName());";
  protected final String TEXT_21 = NL + "\t\tjava.util.Set<String> ";
  protected final String TEXT_22 = "_topics = new java.util.HashSet<String>();";
  protected final String TEXT_23 = NL + "\t\t\tif(true){" + NL + "\t\t\t\tthrow new Exception(\"A stream/topic must be provided.\");" + NL + "\t\t\t}";
  protected final String TEXT_24 = NL + "\t\t\t\t";
  protected final String TEXT_25 = "_topics.add(";
  protected final String TEXT_26 = ");";
  protected final String TEXT_27 = NL;
  protected final String TEXT_28 = NL + "org.apache.spark.streaming.api.java.JavaPairDStream<NullWritable, ";
  protected final String TEXT_29 = "> rdd_";
  protected final String TEXT_30 = " = org.apache.spark.streaming.kafka.v09.KafkaUtils.createDirectStream(ctx, NullWritable.class, ";
  protected final String TEXT_31 = ".class, ";
  protected final String TEXT_32 = "_properties, ";
  protected final String TEXT_33 = "_topics);" + NL + NL + NL;
  protected final String TEXT_34 = NL + "\t";
  protected final String TEXT_35 = NL;
  protected final String TEXT_36 = NL;
  protected final String TEXT_37 = NL + "\t\tjava.util.Map<String, Object> ";
  protected final String TEXT_38 = "_properties = new java.util.HashMap<String, Object>();";
  protected final String TEXT_39 = NL + "\t\t\t";
  protected final String TEXT_40 = "_properties.put(";
  protected final String TEXT_41 = ", ";
  protected final String TEXT_42 = ");";
  protected final String TEXT_43 = NL + "\t\t";
  protected final String TEXT_44 = "_properties.put(org.apache.kafka.clients.consumer.ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, \"";
  protected final String TEXT_45 = "\");" + NL + "\t\t";
  protected final String TEXT_46 = "_properties.put(\"serializer.encoding\", ";
  protected final String TEXT_47 = ");" + NL + "\t\t";
  protected final String TEXT_48 = "_properties.put(org.apache.kafka.clients.consumer.ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, ";
  protected final String TEXT_49 = "_KeyDeserializer.class);" + NL + "\t\t";
  protected final String TEXT_50 = "_properties.put(org.apache.kafka.clients.consumer.ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, ";
  protected final String TEXT_51 = "_ValueDeserializer.class);";
  protected final String TEXT_52 = NL + "\t\tjava.util.List<String> ";
  protected final String TEXT_53 = "_topics = new java.util.ArrayList<String>();";
  protected final String TEXT_54 = NL + "\t\t\tif(true){" + NL + "\t\t\t\tthrow new Exception(\"A stream/topic must be provided.\");" + NL + "\t\t\t}";
  protected final String TEXT_55 = NL + "\t\t\t\t";
  protected final String TEXT_56 = "_topics.add(";
  protected final String TEXT_57 = ");";
  protected final String TEXT_58 = NL;
  protected final String TEXT_59 = NL + NL + "org.apache.spark.streaming.api.java.JavaDStream<org.apache.kafka.clients.consumer.ConsumerRecord<NullWritable, ";
  protected final String TEXT_60 = ">> rdd_kafka_";
  protected final String TEXT_61 = " = org.apache.spark.streaming.kafka09.KafkaUtils.createDirectStream(" + NL + "\tctx, " + NL + "\torg.apache.spark.streaming.kafka09.LocationStrategies.PreferConsistent(), " + NL + "\torg.apache.spark.streaming.kafka09.ConsumerStrategies.<NullWritable, ";
  protected final String TEXT_62 = ">Subscribe(";
  protected final String TEXT_63 = "_topics, ";
  protected final String TEXT_64 = "_properties)" + NL + ");" + NL + "" + NL + "org.apache.spark.streaming.api.java.JavaPairDStream<NullWritable, ";
  protected final String TEXT_65 = "> rdd_";
  protected final String TEXT_66 = " = rdd_kafka_";
  protected final String TEXT_67 = ".mapToPair(new ";
  protected final String TEXT_68 = "_Function());" + NL + NL + NL + NL;
  protected final String TEXT_69 = NL + NL + NL;
  protected final String TEXT_70 = NL;

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    stringBuffer.append(TEXT_1);
    
BigDataCodeGeneratorArgument codeGenArgument = (BigDataCodeGeneratorArgument) argument;
INode node = (INode)codeGenArgument.getArgument();
String cid = node.getUniqueName();

TMapRStreamsInputUtil tMapRStreamsInputUtil = new TMapRStreamsInputUtil(node);
SparkStreamingKafkaVersion sparkStreamingKafkaVersion = tMapRStreamsInputUtil.getSparkStreamingKafkaVersion();

if (SparkStreamingKafkaVersion.MAPR_5X0_KAFKA.equals(sparkStreamingKafkaVersion)) {
	// Special case only for some MapR distributions 

    stringBuffer.append(TEXT_2);
    stringBuffer.append(TEXT_3);
    stringBuffer.append(TEXT_4);
    
/**
* This is a Javajet helper class meant to mutualize code generation for tMapRStreamsXXXInput components.
* These components share several parameters :
* - the consumer properties
* - the list of topics to read from
*
* Each tMapRStreamsXXXInput_sparkstreamingconfig.javajet should at least import this file to avoid
* code duplication.
*/

final class TMapRStreamsInputHelper {

	private TMapRStreamsInputUtil tMapRStreamsInputUtil;
	
	public TMapRStreamsInputHelper(TMapRStreamsInputUtil util){
		tMapRStreamsInputUtil = util;
	}

	public void generateProperties(String cid, String outStructName) {

    stringBuffer.append(TEXT_5);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_6);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_7);
    
		for(Entry<String, String> property : tMapRStreamsInputUtil.getConsumerProperties().entrySet()) {

    stringBuffer.append(TEXT_8);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_9);
    stringBuffer.append(property.getKey());
    stringBuffer.append(TEXT_10);
    stringBuffer.append(property.getValue());
    stringBuffer.append(TEXT_11);
    
		} // end for

    stringBuffer.append(TEXT_12);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_13);
    stringBuffer.append(tMapRStreamsInputUtil.getAutoOffsetReset());
    stringBuffer.append(TEXT_14);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_15);
    stringBuffer.append(tMapRStreamsInputUtil.getEncoding());
    stringBuffer.append(TEXT_16);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_17);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_18);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_19);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_20);
    
	} // end generateProperties
	
	public void generateTopics(String cid) {

    stringBuffer.append(TEXT_21);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_22);
    
		if(tMapRStreamsInputUtil.getTopics().isEmpty()){

    stringBuffer.append(TEXT_23);
    
		}else {
			for(String kafkaTopic : tMapRStreamsInputUtil.getTopics()) {

    stringBuffer.append(TEXT_24);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_25);
    stringBuffer.append(kafkaTopic);
    stringBuffer.append(TEXT_26);
    
			} // end for
		} // end else
	} // end generateTopics
	
} // end class TKafkaInputHelper

    stringBuffer.append(TEXT_27);
    
String outStructName = codeGenArgument.getRecordStructName(tMapRStreamsInputUtil.getOutgoingConnection());
TMapRStreamsInputHelper tMapRStreamsInputHelper = new TMapRStreamsInputHelper(tMapRStreamsInputUtil);
tMapRStreamsInputHelper.generateProperties(cid, outStructName);
tMapRStreamsInputHelper.generateTopics(cid);

    stringBuffer.append(TEXT_28);
    stringBuffer.append(outStructName);
    stringBuffer.append(TEXT_29);
    stringBuffer.append(tMapRStreamsInputUtil.getOutgoingConnection().getName());
    stringBuffer.append(TEXT_30);
    stringBuffer.append(outStructName);
    stringBuffer.append(TEXT_31);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_32);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_33);
    
} else if (SparkStreamingKafkaVersion.MAPR_600_KAFKA.equals(sparkStreamingKafkaVersion)) {
	// Special case only for some MapR distributions 

    stringBuffer.append(TEXT_34);
    stringBuffer.append(TEXT_35);
    stringBuffer.append(TEXT_36);
    
/**
* This is a Javajet helper class meant to mutualize code generation for tMapRStreamsXXXInput components.
* These components share several parameters :
* - the consumer properties
* - the list of topics to read from
*
* Each tMapRStreamsXXXInput_sparkstreamingconfig.javajet should at least import this file to avoid
* code duplication.
*/

final class TMapRStreamsInputHelper {

	private TMapRStreamsInputUtil tMapRStreamsInputUtil;
	
	public TMapRStreamsInputHelper(TMapRStreamsInputUtil util){
		tMapRStreamsInputUtil = util;
	}

	public void generateProperties(String cid, String outStructName) {

    stringBuffer.append(TEXT_37);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_38);
    
		for(Entry<String, String> property : tMapRStreamsInputUtil.getConsumerProperties().entrySet()) {

    stringBuffer.append(TEXT_39);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_40);
    stringBuffer.append(property.getKey());
    stringBuffer.append(TEXT_41);
    stringBuffer.append(property.getValue());
    stringBuffer.append(TEXT_42);
    
		} // end for

    stringBuffer.append(TEXT_43);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_44);
    stringBuffer.append(tMapRStreamsInputUtil.getAutoOffsetReset());
    stringBuffer.append(TEXT_45);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_46);
    stringBuffer.append(tMapRStreamsInputUtil.getEncoding());
    stringBuffer.append(TEXT_47);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_48);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_49);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_50);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_51);
    
	} // end generateProperties
	
	public void generateTopics(String cid) {

    stringBuffer.append(TEXT_52);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_53);
    
		if(tMapRStreamsInputUtil.getTopics().isEmpty()){

    stringBuffer.append(TEXT_54);
    
		}else {
			for(String kafkaTopic : tMapRStreamsInputUtil.getTopics()) {

    stringBuffer.append(TEXT_55);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_56);
    stringBuffer.append(kafkaTopic);
    stringBuffer.append(TEXT_57);
    
			} // end for
		} // end else
	} // end generateTopics
	
} // end class TKafkaInputHelper

    stringBuffer.append(TEXT_58);
    
String outStructName = codeGenArgument.getRecordStructName(tMapRStreamsInputUtil.getOutgoingConnection());
TMapRStreamsInputHelper tMapRStreamsInputHelper = new TMapRStreamsInputHelper(tMapRStreamsInputUtil);
tMapRStreamsInputHelper.generateProperties(cid, outStructName);
tMapRStreamsInputHelper.generateTopics(cid);

    stringBuffer.append(TEXT_59);
    stringBuffer.append(outStructName);
    stringBuffer.append(TEXT_60);
    stringBuffer.append(tMapRStreamsInputUtil.getOutgoingConnection().getName());
    stringBuffer.append(TEXT_61);
    stringBuffer.append(outStructName);
    stringBuffer.append(TEXT_62);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_63);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_64);
    stringBuffer.append(outStructName);
    stringBuffer.append(TEXT_65);
    stringBuffer.append(tMapRStreamsInputUtil.getOutgoingConnection().getName());
    stringBuffer.append(TEXT_66);
    stringBuffer.append(tMapRStreamsInputUtil.getOutgoingConnection().getName());
    stringBuffer.append(TEXT_67);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_68);
    
}

    stringBuffer.append(TEXT_69);
    stringBuffer.append(TEXT_70);
    return stringBuffer.toString();
  }
}
