package org.talend.designer.codegen.translators.machinelearning.clustering;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.talend.core.model.metadata.IMetadataColumn;
import org.talend.core.model.metadata.IMetadataTable;
import org.talend.core.model.metadata.types.JavaType;
import org.talend.core.model.metadata.types.JavaTypesManager;
import org.talend.core.model.process.ElementParameterParser;
import org.talend.core.model.process.IBigDataNode;
import org.talend.core.model.process.IConnection;
import org.talend.core.model.process.IConnectionCategory;
import org.talend.core.model.process.INode;
import org.talend.designer.common.BigDataCodeGeneratorArgument;

public class TPredictClusterSparkstreamingcodeJava
{
  protected static String nl;
  public static synchronized TPredictClusterSparkstreamingcodeJava create(String lineSeparator)
  {
    nl = lineSeparator;
    TPredictClusterSparkstreamingcodeJava result = new TPredictClusterSparkstreamingcodeJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "public static class ";
  protected final String TEXT_2 = " extends ";
  protected final String TEXT_3 = " {" + NL + "    public ";
  protected final String TEXT_4 = " temporaryVector;" + NL + "}" + NL + "public static class GetPrediction_";
  protected final String TEXT_5 = NL + "        implements org.apache.spark.api.java.function.Function<";
  protected final String TEXT_6 = ", ";
  protected final String TEXT_7 = "> {" + NL + "" + NL + "    org.apache.spark.mllib.clustering.KMeansModel currentModel;" + NL + "" + NL + "    public GetPrediction_";
  protected final String TEXT_8 = "(" + NL + "            org.apache.spark.mllib.clustering.KMeansModel currentModel) {" + NL + "        this.currentModel = currentModel;" + NL + "    }" + NL + "" + NL + "    public ";
  protected final String TEXT_9 = " call(";
  protected final String TEXT_10 = " input) {";
  protected final String TEXT_11 = NL + "        ";
  protected final String TEXT_12 = " output = new ";
  protected final String TEXT_13 = "();";
  protected final String TEXT_14 = NL + "                output.";
  protected final String TEXT_15 = " = input.";
  protected final String TEXT_16 = ";";
  protected final String TEXT_17 = NL + "        output.label = currentModel.predict(input.temporaryVector);" + NL + "        return output;" + NL + "    }" + NL + "}" + NL + "" + NL + "public static class GetEncodedStruct_";
  protected final String TEXT_18 = " implements org.apache.spark.api.java.function.Function<org.apache.spark.sql.Row, ";
  protected final String TEXT_19 = "> {" + NL + "" + NL + "    String vectorName;" + NL + "" + NL + "    public GetEncodedStruct_";
  protected final String TEXT_20 = "(String vectorName) {" + NL + "        this.vectorName = vectorName;" + NL + "    }" + NL + "" + NL + "    public ";
  protected final String TEXT_21 = " call(org.apache.spark.sql.Row input) {";
  protected final String TEXT_22 = NL + "        ";
  protected final String TEXT_23 = " output = new ";
  protected final String TEXT_24 = "();" + NL + "        try {" + NL + "        \t";
  protected final String TEXT_25 = NL + "    \t\toutput.temporaryVector = (";
  protected final String TEXT_26 = ") input.get(input.fieldIndex(this.vectorName));" + NL + "\t    \t";
  protected final String TEXT_27 = NL + "\t    \t\toutput.temporaryVector = org.apache.spark.mllib.linalg.Vectors.fromML((org.apache.spark.ml.linalg.Vector) input.get(input.fieldIndex(this.vectorName)));" + NL + "\t    \t";
  protected final String TEXT_28 = NL + "        } catch (java.lang.ClassCastException e) {" + NL + "            // nothing, return null" + NL + "        }";
  protected final String TEXT_29 = NL + "                output.";
  protected final String TEXT_30 = " = (";
  protected final String TEXT_31 = ") input.get(input.fieldIndex(\"";
  protected final String TEXT_32 = "\"));";
  protected final String TEXT_33 = NL + "        return output;" + NL + "    }" + NL + "}" + NL + "" + NL + "public static class GenerateVector_";
  protected final String TEXT_34 = NL + "    implements org.apache.spark.api.java.function.Function<org.apache.spark.api.java.JavaRDD<";
  protected final String TEXT_35 = ">," + NL + "            org.apache.spark.api.java.JavaRDD<";
  protected final String TEXT_36 = ">> {" + NL + "" + NL + "    private String vectorName = \"\";" + NL + "    private org.apache.spark.ml.PipelineModel pipelineModel;" + NL + "    private org.apache.spark.sql.SQLContext context;" + NL + "" + NL + "    public GenerateVector_";
  protected final String TEXT_37 = " (String vectorName, org.apache.spark.ml.PipelineModel pipelineModel, org.apache.spark.sql.SQLContext context) {" + NL + "        this.vectorName = vectorName;" + NL + "        this.pipelineModel = pipelineModel;" + NL + "        this.context = context;" + NL + "    }" + NL + "" + NL + "    @Override" + NL + "    public org.apache.spark.api.java.JavaRDD<";
  protected final String TEXT_38 = "> call(org.apache.spark.api.java.JavaRDD<";
  protected final String TEXT_39 = "> inputRDD) throws Exception {";
  protected final String TEXT_40 = NL + "        ";
  protected final String TEXT_41 = " inputDataFrame = context" + NL + "              .createDataFrame(inputRDD, ";
  protected final String TEXT_42 = ".class);";
  protected final String TEXT_43 = NL + "        ";
  protected final String TEXT_44 = " outputDataFrame = pipelineModel" + NL + "              .transform(inputDataFrame);" + NL + "" + NL + "        return outputDataFrame.toJavaRDD().map(new GetEncodedStruct_";
  protected final String TEXT_45 = "(vectorName));" + NL + "    }" + NL + "}" + NL + "" + NL + "" + NL + "" + NL + "public static class GetKeyVectorStruct_";
  protected final String TEXT_46 = NL + "        implements" + NL + "        org.apache.spark.api.java.function.PairFunction<org.apache.spark.sql.Row, ";
  protected final String TEXT_47 = ", ";
  protected final String TEXT_48 = "> {" + NL + "" + NL + "    String vectorName;" + NL + "" + NL + "    public GetKeyVectorStruct_";
  protected final String TEXT_49 = "(String vectorName) {" + NL + "        this.vectorName = vectorName;" + NL + "    }" + NL + "" + NL + "    public scala.Tuple2<";
  protected final String TEXT_50 = ", ";
  protected final String TEXT_51 = "> call(org.apache.spark.sql.Row input) {";
  protected final String TEXT_52 = NL + "        ";
  protected final String TEXT_53 = " output = new ";
  protected final String TEXT_54 = "();";
  protected final String TEXT_55 = NL + "        ";
  protected final String TEXT_56 = " outputVector  = null;" + NL + "        try {" + NL + "            outputVector = (";
  protected final String TEXT_57 = ") input" + NL + "                    .get(input.fieldIndex(this.vectorName));" + NL + "        } catch (java.lang.ClassCastException e) {" + NL + "            // nothing, return null" + NL + "        }";
  protected final String TEXT_58 = NL + "                output.";
  protected final String TEXT_59 = " = (";
  protected final String TEXT_60 = ") input.get(input.fieldIndex(\"";
  protected final String TEXT_61 = "\"));";
  protected final String TEXT_62 = NL + "        return new scala.Tuple2(output, outputVector);" + NL + "    }" + NL + "}" + NL + NL;
  protected final String TEXT_63 = NL + "    public static class GenerateKeyVector_";
  protected final String TEXT_64 = NL + "        implements org.apache.spark.api.java.function.Function<org.apache.spark.api.java.JavaRDD<";
  protected final String TEXT_65 = ">," + NL + "                    org.apache.spark.api.java.JavaPairRDD<";
  protected final String TEXT_66 = ", ";
  protected final String TEXT_67 = ">> {" + NL + "" + NL + "        private String vectorName = \"\";" + NL + "        private org.apache.spark.ml.PipelineModel pipelineModel;" + NL + "        private org.apache.spark.sql.SQLContext context;" + NL + "" + NL + "        public GenerateKeyVector_";
  protected final String TEXT_68 = " (String vectorName, org.apache.spark.ml.PipelineModel pipelineModel, org.apache.spark.sql.SQLContext context) {" + NL + "            this.vectorName = vectorName;" + NL + "            this.pipelineModel = pipelineModel;" + NL + "            this.context = context;" + NL + "        }" + NL + "" + NL + "        @Override" + NL + "        public org.apache.spark.api.java.JavaPairRDD<";
  protected final String TEXT_69 = ", ";
  protected final String TEXT_70 = "> call(org.apache.spark.api.java.JavaRDD<";
  protected final String TEXT_71 = "> inputRDD) throws Exception {";
  protected final String TEXT_72 = NL + "            ";
  protected final String TEXT_73 = " inputDataFrame = context" + NL + "                  .createDataFrame(inputRDD, ";
  protected final String TEXT_74 = ".class);";
  protected final String TEXT_75 = NL + "            ";
  protected final String TEXT_76 = " outputDataFrame = pipelineModel" + NL + "                .transform(inputDataFrame);" + NL + "            return outputDataFrame.toJavaRDD().mapToPair(new GetKeyVectorStruct_";
  protected final String TEXT_77 = "(vectorName));" + NL + "        }" + NL + "    }";
  protected final String TEXT_78 = NL + "    public static class GenerateKeyVector_";
  protected final String TEXT_79 = NL + "        implements org.apache.spark.api.java.function.Function<org.apache.spark.api.java.JavaRDD<";
  protected final String TEXT_80 = ">," + NL + "                    org.apache.spark.api.java.JavaPairRDD<";
  protected final String TEXT_81 = ", ";
  protected final String TEXT_82 = ">> {" + NL + "" + NL + "        private String vectorName = \"\";" + NL + "        private org.apache.spark.ml.Pipeline pipeline;" + NL + "        private org.apache.spark.sql.SQLContext context;" + NL + "" + NL + "        public GenerateKeyVector_";
  protected final String TEXT_83 = " (String vectorName, org.apache.spark.ml.Pipeline pipeline, org.apache.spark.sql.SQLContext context) {" + NL + "            this.vectorName = vectorName;" + NL + "            this.pipeline = pipeline;" + NL + "            this.context = context;" + NL + "        }" + NL + "" + NL + "        @Override" + NL + "        public org.apache.spark.api.java.JavaPairRDD<";
  protected final String TEXT_84 = ", ";
  protected final String TEXT_85 = "> call(org.apache.spark.api.java.JavaRDD<";
  protected final String TEXT_86 = "> inputRDD) throws Exception {";
  protected final String TEXT_87 = NL + "            ";
  protected final String TEXT_88 = " inputDataFrame = context" + NL + "                  .createDataFrame(inputRDD, ";
  protected final String TEXT_89 = ".class);";
  protected final String TEXT_90 = NL + "            ";
  protected final String TEXT_91 = " outputDataFrame = pipeline" + NL + "                .fit(inputDataFrame)" + NL + "                .transform(inputDataFrame);" + NL + "            return outputDataFrame.toJavaRDD().mapToPair(new GetKeyVectorStruct_";
  protected final String TEXT_92 = "(vectorName));" + NL + "        }" + NL + "    }";
  protected final String TEXT_93 = NL + NL + "public static class GetStreamingPrediction_";
  protected final String TEXT_94 = NL + "        implements org.apache.spark.api.java.function.Function<scala.Tuple2<";
  protected final String TEXT_95 = ", Integer>, ";
  protected final String TEXT_96 = "> {" + NL + "" + NL + "    public ";
  protected final String TEXT_97 = " call(scala.Tuple2<";
  protected final String TEXT_98 = ", Integer> inputTuple) {";
  protected final String TEXT_99 = NL + "        ";
  protected final String TEXT_100 = " input = inputTuple._1();" + NL + "        Integer label = inputTuple._2();";
  protected final String TEXT_101 = NL + "        ";
  protected final String TEXT_102 = " output = new ";
  protected final String TEXT_103 = "();";
  protected final String TEXT_104 = NL + "                output.";
  protected final String TEXT_105 = " = input.";
  protected final String TEXT_106 = ";";
  protected final String TEXT_107 = NL + "        output.label = label;" + NL + "        return output;" + NL + "    }" + NL + "}";

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    
//Parse the inputs to this javajet generator.
final BigDataCodeGeneratorArgument codeGenArgument = (BigDataCodeGeneratorArgument) argument;
final INode node = (INode)codeGenArgument.getArgument();
final String cid = node.getUniqueName();
final boolean isLog4jEnabled = ("true").equals(ElementParameterParser.getValue(node.getProcess(), "__LOG4J_ACTIVATE__"));

final String dataframeClass = org.talend.hadoop.distribution.ESparkVersion.SPARK_2_0.compareTo(codeGenArgument.getSparkVersion()) > 0
    ? "org.apache.spark.sql.DataFrame"
    : "org.apache.spark.sql.Dataset<org.apache.spark.sql.Row>";
final String vectorClass = "org.apache.spark.mllib.linalg.Vector";

    
IMetadataTable metadata = null;
IConnection conn = null;
List<IMetadataColumn> columns = null;
List<IMetadataTable> metadatas = node.getMetadataList();
if((metadatas!=null) && (metadatas.size() > 0)){
    metadata = metadatas.get(0);
    if(metadata != null){
        columns = metadata.getListColumns();
    }
}
List<? extends IConnection> conns = node.getIncomingConnections();
if(conns != null && conns.size() > 0 && conns.get(0).getLineStyle().hasConnectionCategory(IConnectionCategory.DATA)){
    conn = conns.get(0);
}

List<? extends IConnection> outConns = node.getOutgoingConnections();
IConnection outConn = null;
if(outConns != null && outConns.size() > 0 && outConns.get(0).getLineStyle().hasConnectionCategory(IConnectionCategory.DATA)){
    outConn = outConns.get(0);
}

if(columns == null || columns.isEmpty() || conn == null || outConn == null){
    return "";
}

String inRowStruct = codeGenArgument.getRecordStructName(conn);
String inRowStructEncoded = conn.getName() + "Encoded";
String connName = conn.getName();

String outRowStruct = codeGenArgument.getRecordStructName(outConn);
String outConnName = outConn.getName();


    stringBuffer.append(TEXT_1);
    stringBuffer.append(inRowStructEncoded);
    stringBuffer.append(TEXT_2);
    stringBuffer.append(inRowStruct);
    stringBuffer.append(TEXT_3);
    stringBuffer.append(vectorClass);
    stringBuffer.append(TEXT_4);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_5);
    stringBuffer.append(inRowStructEncoded);
    stringBuffer.append(TEXT_6);
    stringBuffer.append(outRowStruct);
    stringBuffer.append(TEXT_7);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_8);
    stringBuffer.append(outRowStruct);
    stringBuffer.append(TEXT_9);
    stringBuffer.append(inRowStructEncoded);
    stringBuffer.append(TEXT_10);
    stringBuffer.append(TEXT_11);
    stringBuffer.append(outRowStruct);
    stringBuffer.append(TEXT_12);
    stringBuffer.append(outRowStruct);
    stringBuffer.append(TEXT_13);
    
        for (IMetadataColumn column: columns) {
            if (!"label".equals(column.getLabel())) {
                
    stringBuffer.append(TEXT_14);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_15);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_16);
    
            }
        }
        
    stringBuffer.append(TEXT_17);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_18);
    stringBuffer.append(inRowStructEncoded);
    stringBuffer.append(TEXT_19);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_20);
    stringBuffer.append(inRowStructEncoded);
    stringBuffer.append(TEXT_21);
    stringBuffer.append(TEXT_22);
    stringBuffer.append(inRowStructEncoded);
    stringBuffer.append(TEXT_23);
    stringBuffer.append(inRowStructEncoded);
    stringBuffer.append(TEXT_24);
     if (org.talend.hadoop.distribution.ESparkVersion.SPARK_2_0.compareTo(codeGenArgument.getSparkVersion()) > 0) {
    stringBuffer.append(TEXT_25);
    stringBuffer.append(vectorClass);
    stringBuffer.append(TEXT_26);
     } else { 
    stringBuffer.append(TEXT_27);
     } 
    stringBuffer.append(TEXT_28);
    
        for (IMetadataColumn column: columns) {
            if (!"label".equals(column.getLabel())) {
                
    stringBuffer.append(TEXT_29);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_30);
    stringBuffer.append(JavaTypesManager.getTypeToGenerate(column.getTalendType(),
                                column.isNullable()));
    stringBuffer.append(TEXT_31);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_32);
    
            }
        }
        
    stringBuffer.append(TEXT_33);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_34);
    stringBuffer.append(inRowStruct);
    stringBuffer.append(TEXT_35);
    stringBuffer.append(inRowStructEncoded);
    stringBuffer.append(TEXT_36);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_37);
    stringBuffer.append(inRowStructEncoded);
    stringBuffer.append(TEXT_38);
    stringBuffer.append(inRowStruct);
    stringBuffer.append(TEXT_39);
    stringBuffer.append(TEXT_40);
    stringBuffer.append(dataframeClass);
    stringBuffer.append(TEXT_41);
    stringBuffer.append(inRowStruct);
    stringBuffer.append(TEXT_42);
    stringBuffer.append(TEXT_43);
    stringBuffer.append(dataframeClass);
    stringBuffer.append(TEXT_44);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_45);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_46);
    stringBuffer.append(inRowStruct);
    stringBuffer.append(TEXT_47);
    stringBuffer.append(vectorClass);
    stringBuffer.append(TEXT_48);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_49);
    stringBuffer.append(inRowStruct);
    stringBuffer.append(TEXT_50);
    stringBuffer.append(vectorClass);
    stringBuffer.append(TEXT_51);
    stringBuffer.append(TEXT_52);
    stringBuffer.append(inRowStruct);
    stringBuffer.append(TEXT_53);
    stringBuffer.append(inRowStruct);
    stringBuffer.append(TEXT_54);
    stringBuffer.append(TEXT_55);
    stringBuffer.append(vectorClass);
    stringBuffer.append(TEXT_56);
    stringBuffer.append(vectorClass);
    stringBuffer.append(TEXT_57);
    
        for (IMetadataColumn column: columns) {
            if (!"label".equals(column.getLabel())) {
                
    stringBuffer.append(TEXT_58);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_59);
    stringBuffer.append(JavaTypesManager.getTypeToGenerate(column.getTalendType(),
                                column.isNullable()));
    stringBuffer.append(TEXT_60);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_61);
    
            }
        }
        
    stringBuffer.append(TEXT_62);
    

String modelLocation = ElementParameterParser.getValue(node, "__MODEL_LOCATION__");
Boolean inputAsPipelineModel = false;
for(INode targetNode : node.getProcess().getGeneratingNodes()){
    if (targetNode.getUniqueName().equals(modelLocation)) {
        // If the tKmeansStrModel load the pipeline from disk, it will save a PipelineModel.
        // Otherwise (the default case), it will be a Pipeline.
        // This is because this pipeline was read from HDFS and can be a complexe pipeline computed with a batch KMeans
        inputAsPipelineModel = ElementParameterParser.getBooleanValue(targetNode, "__REUSE_PIPELINE__")
                && ElementParameterParser.getBooleanValue(targetNode, "__LOAD_FROM_DISK__");
        break;
    }
}

if (inputAsPipelineModel) {
    
    stringBuffer.append(TEXT_63);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_64);
    stringBuffer.append(inRowStruct);
    stringBuffer.append(TEXT_65);
    stringBuffer.append(inRowStruct);
    stringBuffer.append(TEXT_66);
    stringBuffer.append(vectorClass);
    stringBuffer.append(TEXT_67);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_68);
    stringBuffer.append(inRowStruct);
    stringBuffer.append(TEXT_69);
    stringBuffer.append(vectorClass);
    stringBuffer.append(TEXT_70);
    stringBuffer.append(inRowStruct);
    stringBuffer.append(TEXT_71);
    stringBuffer.append(TEXT_72);
    stringBuffer.append(dataframeClass);
    stringBuffer.append(TEXT_73);
    stringBuffer.append(inRowStruct);
    stringBuffer.append(TEXT_74);
    stringBuffer.append(TEXT_75);
    stringBuffer.append(dataframeClass);
    stringBuffer.append(TEXT_76);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_77);
    
} else {
    
    stringBuffer.append(TEXT_78);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_79);
    stringBuffer.append(inRowStruct);
    stringBuffer.append(TEXT_80);
    stringBuffer.append(inRowStruct);
    stringBuffer.append(TEXT_81);
    stringBuffer.append(vectorClass);
    stringBuffer.append(TEXT_82);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_83);
    stringBuffer.append(inRowStruct);
    stringBuffer.append(TEXT_84);
    stringBuffer.append(vectorClass);
    stringBuffer.append(TEXT_85);
    stringBuffer.append(inRowStruct);
    stringBuffer.append(TEXT_86);
    stringBuffer.append(TEXT_87);
    stringBuffer.append(dataframeClass);
    stringBuffer.append(TEXT_88);
    stringBuffer.append(inRowStruct);
    stringBuffer.append(TEXT_89);
    stringBuffer.append(TEXT_90);
    stringBuffer.append(dataframeClass);
    stringBuffer.append(TEXT_91);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_92);
    
}

    stringBuffer.append(TEXT_93);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_94);
    stringBuffer.append(inRowStruct);
    stringBuffer.append(TEXT_95);
    stringBuffer.append(outRowStruct);
    stringBuffer.append(TEXT_96);
    stringBuffer.append(outRowStruct);
    stringBuffer.append(TEXT_97);
    stringBuffer.append(inRowStruct);
    stringBuffer.append(TEXT_98);
    stringBuffer.append(TEXT_99);
    stringBuffer.append(inRowStruct);
    stringBuffer.append(TEXT_100);
    stringBuffer.append(TEXT_101);
    stringBuffer.append(outRowStruct);
    stringBuffer.append(TEXT_102);
    stringBuffer.append(outRowStruct);
    stringBuffer.append(TEXT_103);
    
        for (IMetadataColumn column: columns) {
            if (!"label".equals(column.getLabel())) {
                
    stringBuffer.append(TEXT_104);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_105);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_106);
    
            }
        }
        
    stringBuffer.append(TEXT_107);
    return stringBuffer.toString();
  }
}
