package org.talend.designer.codegen.translators.data_quality;

import org.talend.core.model.metadata.IMetadataColumn;
import org.talend.core.model.metadata.IMetadataTable;
import org.talend.core.model.metadata.types.JavaType;
import org.talend.core.model.metadata.types.JavaTypesManager;
import org.talend.core.model.process.EConnectionType;
import org.talend.core.model.process.ElementParameterParser;
import org.talend.core.model.process.IBigDataNode;
import org.talend.core.model.process.IConnection;
import org.talend.core.model.process.IConnectionCategory;
import org.talend.core.model.process.INode;
import org.talend.designer.common.BigDataCodeGeneratorArgument;

public class TPatternMaskingSparkconfigJava
{
  protected static String nl;
  public static synchronized TPatternMaskingSparkconfigJava create(String lineSeparator)
  {
    nl = lineSeparator;
    TPatternMaskingSparkconfigJava result = new TPatternMaskingSparkconfigJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "    ";
  protected final String TEXT_2 = NL + "            public static class ";
  protected final String TEXT_3 = " implements ";
  protected final String TEXT_4 = " {" + NL + "                private ContextProperties context = new ContextProperties();";
  protected final String TEXT_5 = NL + NL + "                //private ContextProperties context = null;" + NL + "" + NL + "                public ";
  protected final String TEXT_6 = "(JobConf job) {" + NL + "                    this.context = new ContextProperties(job);";
  protected final String TEXT_7 = NL + "                }" + NL + "" + NL + "\t            public ";
  protected final String TEXT_8 = " ";
  protected final String TEXT_9 = "(";
  protected final String TEXT_10 = ") ";
  protected final String TEXT_11 = " {" + NL + "\t            \t";
  protected final String TEXT_12 = NL + "\t            \t";
  protected final String TEXT_13 = NL + "\t                ";
  protected final String TEXT_14 = NL + "\t                return ";
  protected final String TEXT_15 = ";" + NL + "\t            }" + NL + "\t        }" + NL + "\t\t";
  protected final String TEXT_16 = NL + "            public static class ";
  protected final String TEXT_17 = " implements ";
  protected final String TEXT_18 = " {";
  protected final String TEXT_19 = NL + NL + "                private ContextProperties context = null;" + NL + "" + NL + "                public ";
  protected final String TEXT_20 = "(JobConf job) {" + NL + "                    this.context = new ContextProperties(job);" + NL + "                }" + NL + "" + NL + "                public ";
  protected final String TEXT_21 = " ";
  protected final String TEXT_22 = "(";
  protected final String TEXT_23 = ") ";
  protected final String TEXT_24 = " {" + NL + "                \t";
  protected final String TEXT_25 = NL + "\t                 \treturn ";
  protected final String TEXT_26 = ";";
  protected final String TEXT_27 = NL + "                }" + NL + "            }";
  protected final String TEXT_28 = NL + "            public static class ";
  protected final String TEXT_29 = " implements ";
  protected final String TEXT_30 = " {";
  protected final String TEXT_31 = NL + NL + "                private ContextProperties context = null;" + NL + "                private java.util.function.Function<org.apache.avro.generic.IndexedRecord, org.apache.avro.generic.IndexedRecord> function = null;" + NL + "" + NL + "                public ";
  protected final String TEXT_32 = "(JobConf job, java.util.function.Function<org.apache.avro.generic.IndexedRecord, org.apache.avro.generic.IndexedRecord> function) {" + NL + "                    this.context = new ContextProperties(job);" + NL + "                    this.function = function;" + NL + "                }" + NL + "" + NL + "                public ";
  protected final String TEXT_33 = " ";
  protected final String TEXT_34 = "(";
  protected final String TEXT_35 = ") ";
  protected final String TEXT_36 = " {";
  protected final String TEXT_37 = NL + "                    ";
  protected final String TEXT_38 = NL + "                    ";
  protected final String TEXT_39 = NL + "                    return ";
  protected final String TEXT_40 = ";" + NL + "                }" + NL + "            }";
  protected final String TEXT_41 = NL;
  protected final String TEXT_42 = NL + "            // No sparkcode generated for unnecessary ";
  protected final String TEXT_43 = NL;
  protected final String TEXT_44 = NL + "            public static class ";
  protected final String TEXT_45 = "TrueFilter implements org.apache.spark.api.java.function.Function<scala.Tuple2<Boolean, org.apache.avro.specific.SpecificRecordBase>, Boolean> {" + NL + "" + NL + "                public Boolean call(scala.Tuple2<Boolean, org.apache.avro.specific.SpecificRecordBase> arg0)" + NL + "                        throws Exception {" + NL + "                    return arg0._1;" + NL + "                }" + NL + "            }" + NL + "" + NL + "            public static class ";
  protected final String TEXT_46 = "FalseFilter implements org.apache.spark.api.java.function.Function<scala.Tuple2<Boolean, org.apache.avro.specific.SpecificRecordBase>, Boolean> {" + NL + "" + NL + "                public Boolean call(scala.Tuple2<Boolean, org.apache.avro.specific.SpecificRecordBase> arg0)" + NL + "                        throws Exception {" + NL + "                    return !arg0._1;" + NL + "                }" + NL + "            }" + NL + "" + NL + "            public static class ";
  protected final String TEXT_47 = "ToNullWritableMain implements ";
  protected final String TEXT_48 = " {" + NL + "" + NL + "                private ContextProperties context = null;" + NL + "" + NL + "                public ";
  protected final String TEXT_49 = "ToNullWritableMain(JobConf job) {" + NL + "                    this.context = new ContextProperties(job);" + NL + "                }" + NL + "" + NL + "                public ";
  protected final String TEXT_50 = " call(" + NL + "                        scala.Tuple2<Boolean, org.apache.avro.specific.SpecificRecordBase> data){";
  protected final String TEXT_51 = NL + "                    ";
  protected final String TEXT_52 = " ";
  protected final String TEXT_53 = " = (";
  protected final String TEXT_54 = ")data._2;" + NL + "                    return ";
  protected final String TEXT_55 = ";" + NL + "                }" + NL + "            }" + NL + "" + NL + "            public static class ";
  protected final String TEXT_56 = "ToNullWritableReject implements ";
  protected final String TEXT_57 = " {" + NL + "" + NL + "                private ContextProperties context = null;" + NL + "" + NL + "                public ";
  protected final String TEXT_58 = "ToNullWritableReject(JobConf job) {" + NL + "                    this.context = new ContextProperties(job);" + NL + "                }" + NL + "" + NL + "                public ";
  protected final String TEXT_59 = " call(" + NL + "                        scala.Tuple2<Boolean, org.apache.avro.specific.SpecificRecordBase> data){";
  protected final String TEXT_60 = NL + "                        ";
  protected final String TEXT_61 = " ";
  protected final String TEXT_62 = " = (";
  protected final String TEXT_63 = ")data._2;" + NL + "                    return ";
  protected final String TEXT_64 = ";" + NL + "                }" + NL + "            }";
  protected final String TEXT_65 = NL + "            // No sparkconfig generated for unnecessary ";
  protected final String TEXT_66 = NL;
  protected final String TEXT_67 = NL + "            // Extract data." + NL;
  protected final String TEXT_68 = NL + "            ";
  protected final String TEXT_69 = "<Boolean, org.apache.avro.specific.SpecificRecordBase> temporary_rdd_";
  protected final String TEXT_70 = " = rdd_";
  protected final String TEXT_71 = ".";
  protected final String TEXT_72 = "(new ";
  protected final String TEXT_73 = "(job));" + NL + "" + NL + "            // Main flow" + NL;
  protected final String TEXT_74 = NL + "            ";
  protected final String TEXT_75 = " rdd_";
  protected final String TEXT_76 = " = temporary_rdd_";
  protected final String TEXT_77 = NL + "                  .filter(new ";
  protected final String TEXT_78 = "TrueFilter())" + NL + "                    .";
  protected final String TEXT_79 = "(new ";
  protected final String TEXT_80 = "ToNullWritableMain(job));" + NL + "" + NL + "            // Reject flow";
  protected final String TEXT_81 = NL + "            ";
  protected final String TEXT_82 = " rdd_";
  protected final String TEXT_83 = " = temporary_rdd_";
  protected final String TEXT_84 = NL + "                    .filter(new ";
  protected final String TEXT_85 = "FalseFilter())" + NL + "                    .";
  protected final String TEXT_86 = "(new ";
  protected final String TEXT_87 = "ToNullWritableReject(job));";
  protected final String TEXT_88 = NL + "            ";
  protected final String TEXT_89 = " rdd_";
  protected final String TEXT_90 = " = rdd_";
  protected final String TEXT_91 = ".";
  protected final String TEXT_92 = "(new ";
  protected final String TEXT_93 = "(job));";
  protected final String TEXT_94 = NL;
  protected final String TEXT_95 = NL + NL + "    final org.talend.dataquality.datamasking.TypeTester typeTester_";
  protected final String TEXT_96 = " = new org.talend.dataquality.datamasking.TypeTester();" + NL + "" + NL + "    final java.util.Random rnd_";
  protected final String TEXT_97 = " = new java.util.Random(";
  protected final String TEXT_98 = NL + "                Long.valueOf(";
  protected final String TEXT_99 = ")";
  protected final String TEXT_100 = NL + ");";
  protected final String TEXT_101 = NL + "    java.util.List<org.talend.dataquality.datamasking.generic.FieldDefinition> fieldList_";
  protected final String TEXT_102 = "_";
  protected final String TEXT_103 = " = new java.util.ArrayList<>();";
  protected final String TEXT_104 = "   " + NL + "                       String filePath_";
  protected final String TEXT_105 = "_";
  protected final String TEXT_106 = " = ";
  protected final String TEXT_107 = ";" + NL + "                       filePath_";
  protected final String TEXT_108 = "_";
  protected final String TEXT_109 = " = org.apache.spark.SparkFiles.get(filePath_";
  protected final String TEXT_110 = "_";
  protected final String TEXT_111 = ".substring(filePath_";
  protected final String TEXT_112 = "_";
  protected final String TEXT_113 = ".lastIndexOf(\"/\") + 1));" + NL + "                       System.out.println(\">>> \"+ filePath_";
  protected final String TEXT_114 = "_";
  protected final String TEXT_115 = ");" + NL + "    \t           \t\tfieldList_";
  protected final String TEXT_116 = "_";
  protected final String TEXT_117 = ".add(new org.talend.dataquality.datamasking.generic.FieldDefinition(" + NL + "                           \"";
  protected final String TEXT_118 = "\", filePath_";
  protected final String TEXT_119 = "_";
  protected final String TEXT_120 = ", ";
  protected final String TEXT_121 = "));";
  protected final String TEXT_122 = "           \t\t           \t\t" + NL + "    \t           fieldList_";
  protected final String TEXT_123 = "_";
  protected final String TEXT_124 = ".add(new org.talend.dataquality.datamasking.generic.FieldDefinition(" + NL + "                           \"";
  protected final String TEXT_125 = "\", ";
  protected final String TEXT_126 = ", ";
  protected final String TEXT_127 = "));";
  protected final String TEXT_128 = NL + "        final org.talend.dataquality.datamasking.generic.BijectiveSubstitutionFunction fn_";
  protected final String TEXT_129 = "_";
  protected final String TEXT_130 = " = new org.talend.dataquality.datamasking.generic.BijectiveSubstitutionFunction(fieldList_";
  protected final String TEXT_131 = "_";
  protected final String TEXT_132 = ");" + NL + "" + NL + "        fn_";
  protected final String TEXT_133 = "_";
  protected final String TEXT_134 = ".parse(null, ";
  protected final String TEXT_135 = ", rnd_";
  protected final String TEXT_136 = ");" + NL + "        fn_";
  protected final String TEXT_137 = "_";
  protected final String TEXT_138 = ".setKeepEmpty(";
  protected final String TEXT_139 = ");" + NL;
  protected final String TEXT_140 = NL + NL + "    org.talend.dataquality.datamasking.DataMasker<";
  protected final String TEXT_141 = "Struct, ";
  protected final String TEXT_142 = "Struct> duplicator_";
  protected final String TEXT_143 = NL + "                    = new org.talend.dataquality.datamasking.DataMasker<";
  protected final String TEXT_144 = "Struct, ";
  protected final String TEXT_145 = "Struct>(){" + NL + "    @Override" + NL + "    protected ";
  protected final String TEXT_146 = "Struct generateOutput(";
  protected final String TEXT_147 = "Struct originalStruct, boolean isOriginal)" + NL + "    {";
  protected final String TEXT_148 = NL + "        ";
  protected final String TEXT_149 = "Struct tmpStruct = new ";
  protected final String TEXT_150 = "Struct();" + NL;
  protected final String TEXT_151 = "      " + NL + "                tmpStruct.";
  protected final String TEXT_152 = " = originalStruct.";
  protected final String TEXT_153 = ";";
  protected final String TEXT_154 = NL + "        if (isOriginal){" + NL + "            tmpStruct.ORIGINAL_MARK = true;" + NL + "        }" + NL + "        else{" + NL + "            modifyOutput(tmpStruct);            " + NL + "            tmpStruct.ORIGINAL_MARK = false;" + NL + "        }" + NL + "        " + NL + "        return tmpStruct;" + NL + "    }" + NL + "" + NL + "    private void modifyOutput(";
  protected final String TEXT_155 = "Struct ";
  protected final String TEXT_156 = "){    " + NL;
  protected final String TEXT_157 = NL + "        \t    Object tmpValue_";
  protected final String TEXT_158 = "_";
  protected final String TEXT_159 = " = null;" + NL + "                    tmpValue_";
  protected final String TEXT_160 = "_";
  protected final String TEXT_161 = " = fn_";
  protected final String TEXT_162 = "_";
  protected final String TEXT_163 = ".generateMaskedRow(";
  protected final String TEXT_164 = ".";
  protected final String TEXT_165 = ");" + NL + "                    ";
  protected final String TEXT_166 = NL + "                    if(tmpValue_";
  protected final String TEXT_167 = "_";
  protected final String TEXT_168 = " == null){";
  protected final String TEXT_169 = NL + "                        ";
  protected final String TEXT_170 = ".";
  protected final String TEXT_171 = " = null;" + NL + "                    }" + NL + "                    else{";
  protected final String TEXT_172 = "    ";
  protected final String TEXT_173 = NL + "                                ";
  protected final String TEXT_174 = ".";
  protected final String TEXT_175 = " = String.valueOf(tmpValue_";
  protected final String TEXT_176 = "_";
  protected final String TEXT_177 = ");";
  protected final String TEXT_178 = NL + "                               if(tmpValue_";
  protected final String TEXT_179 = "_";
  protected final String TEXT_180 = " instanceof java.util.Date){";
  protected final String TEXT_181 = NL + "                                    ";
  protected final String TEXT_182 = ".";
  protected final String TEXT_183 = " = (java.util.Date)tmpValue_";
  protected final String TEXT_184 = "_";
  protected final String TEXT_185 = ";" + NL + "                                }" + NL + "                                else{";
  protected final String TEXT_186 = NL + "                                    ";
  protected final String TEXT_187 = ".";
  protected final String TEXT_188 = " = ParserUtils.parseTo_Date(tmpValue_";
  protected final String TEXT_189 = "_";
  protected final String TEXT_190 = ".toString(), ";
  protected final String TEXT_191 = ");" + NL + "                                }";
  protected final String TEXT_192 = NL + "                                ";
  protected final String TEXT_193 = ".";
  protected final String TEXT_194 = " = tmpValue_";
  protected final String TEXT_195 = "_";
  protected final String TEXT_196 = ".toString().getBytes();";
  protected final String TEXT_197 = "                                  ";
  protected final String TEXT_198 = NL + "                            ";
  protected final String TEXT_199 = ".";
  protected final String TEXT_200 = " = ParserUtils.parseTo_";
  protected final String TEXT_201 = "(String.valueOf(tmpValue_";
  protected final String TEXT_202 = "_";
  protected final String TEXT_203 = "));";
  protected final String TEXT_204 = " ";
  protected final String TEXT_205 = NL + "                    }";
  protected final String TEXT_206 = NL + "    }" + NL + "};" + NL + "    List<";
  protected final String TEXT_207 = "Struct> ";
  protected final String TEXT_208 = "ReslutList = duplicator_";
  protected final String TEXT_209 = ".process(";
  protected final String TEXT_210 = ", ";
  protected final String TEXT_211 = ");" + NL + "" + NL + "" + NL + "    // Emit the parsed structure on the main output.";
  protected final String TEXT_212 = NL + "    ";
  protected final String TEXT_213 = NL + "        " + NL + "    for (";
  protected final String TEXT_214 = "Struct tmpStructMask_";
  protected final String TEXT_215 = " : ";
  protected final String TEXT_216 = "ReslutList){";
  protected final String TEXT_217 = NL + "        ";
  protected final String TEXT_218 = " = tmpStructMask_";
  protected final String TEXT_219 = ";" + NL;
  protected final String TEXT_220 = NL + "        ";
  protected final String TEXT_221 = NL + "    }";
  protected final String TEXT_222 = NL + "            ctx.sparkContext().sc().addFile(";
  protected final String TEXT_223 = ", true);";
  protected final String TEXT_224 = NL + "            ctx.sc().addFile(";
  protected final String TEXT_225 = ", true);";
  protected final String TEXT_226 = NL + "        ";
  protected final String TEXT_227 = NL + "        ";

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    stringBuffer.append(TEXT_1);
    
//Parse the inputs to this javajet generator.
final BigDataCodeGeneratorArgument codeGenArgument = (BigDataCodeGeneratorArgument) argument;
final INode node = (INode) codeGenArgument.getArgument();
final IBigDataNode bigDataNode = (IBigDataNode) codeGenArgument.getArgument();
final String cid = node.getUniqueName();

    
	/**
	 * Code generated for component with single output
 	 */
    class SOFunctionGenerator extends org.talend.designer.spark.generator.FunctionGenerator{

    	SOFunctionGenerator(org.talend.designer.common.TransformerBase transformer) {
            super(transformer);
        }

    	SOFunctionGenerator(org.talend.designer.common.TransformerBase transformer, org.talend.designer.spark.generator.SparkFunction sparkFunction) {
    		super(transformer, sparkFunction);
    	}

    	@Override
        public void generate(){
		
    stringBuffer.append(TEXT_2);
    stringBuffer.append(this.sparkFunction.getClassName(cid));
    stringBuffer.append(TEXT_3);
    stringBuffer.append(this.sparkFunction.getCodeFunctionImplementation(getOutValueClass(), getInValueClass()));
    stringBuffer.append(TEXT_4);
    
                this.transformer.generateHelperClasses(true);
                this.transformer.generateTransformContextDeclaration();
                
    stringBuffer.append(TEXT_5);
    stringBuffer.append(this.sparkFunction.getClassName(cid));
    stringBuffer.append(TEXT_6);
    
                    this.transformer.generateTransformContextDeclarationInConstructor();
                    
    stringBuffer.append(TEXT_7);
    stringBuffer.append(this.sparkFunction.getCodeFunctionReturnedType(this.outValueClass.toString()));
    stringBuffer.append(TEXT_8);
    stringBuffer.append(this.sparkFunction.getCodeImplementedMethod());
    stringBuffer.append(TEXT_9);
    stringBuffer.append(this.sparkFunction.getCodeFunctionArgument(getInValueClass()));
    stringBuffer.append(TEXT_10);
    stringBuffer.append(this.sparkFunction.getCodeThrowException());
    stringBuffer.append(TEXT_11);
    stringBuffer.append(this.sparkFunction.getMethodHeader(this.outValueClass, this.outValue, this.inValueClass, this.inValue));
    stringBuffer.append(TEXT_12);
    stringBuffer.append(this.sparkFunction.getCodeKeyMapping(getInValue()));
    stringBuffer.append(TEXT_13);
    
	                this.transformer.generateTransformContextInitialization();
	                this.transformer.generateTransform(true);
	                
    stringBuffer.append(TEXT_14);
    stringBuffer.append(this.sparkFunction.getCodeFunctionReturn(this.getOutValue(), this.getOutValueClass()));
    stringBuffer.append(TEXT_15);
    
        }
    }

	/**
	 * Code generated for component with multiple outputs
 	 */
    class MOFunctionGenerator extends org.talend.designer.spark.generator.FunctionGenerator{

        /** The single connection to pass along the output chain of Mappers
         *  instead of sending to a dedicated collector via MultipleOutputs. */
        String connectionToChain = null;

        MOFunctionGenerator(org.talend.designer.common.TransformerBase transformer) {
            super(transformer);
            defaultOutKeyClass = "Boolean";
        }

    	MOFunctionGenerator(org.talend.designer.common.TransformerBase transformer, org.talend.designer.spark.generator.SparkFunction sparkFunction) {
    		super(transformer, sparkFunction);
            defaultOutKeyClass = "Boolean";
    	}

    	@Override
        public void generate(){
        
    stringBuffer.append(TEXT_16);
    stringBuffer.append(this.sparkFunction.getClassName(cid));
    stringBuffer.append(TEXT_17);
    stringBuffer.append(this.sparkFunction.getCodeFunctionImplementationOutputFixedType(getInValueClass(), "Boolean", "org.apache.avro.specific.SpecificRecordBase"));
    stringBuffer.append(TEXT_18);
    
                this.transformer.generateHelperClasses(true);
                this.transformer.generateTransformContextDeclaration();
                
    stringBuffer.append(TEXT_19);
    stringBuffer.append(this.sparkFunction.getClassName(cid));
    stringBuffer.append(TEXT_20);
    stringBuffer.append(this.sparkFunction.getCodeFunctionReturnedTypeFixedType((String)this.outKeyClass, "org.apache.avro.specific.SpecificRecordBase"));
    stringBuffer.append(TEXT_21);
    stringBuffer.append(this.sparkFunction.getCodeImplementedMethod());
    stringBuffer.append(TEXT_22);
    stringBuffer.append(this.sparkFunction.getCodeFunctionArgument(getInValueClass()));
    stringBuffer.append(TEXT_23);
    stringBuffer.append(this.sparkFunction.getCodeThrowException());
    stringBuffer.append(TEXT_24);
    stringBuffer.append(this.sparkFunction.getMethodHeader(this.outValueClass, this.outValue, this.inValueClass, this.inValue));
    
                    this.transformer.generateTransformContextInitialization();
                    this.transformer.generateTransform(true);
	                if(this.sparkFunction.getCodeFunctionReturn()!=null) {
                
    stringBuffer.append(TEXT_25);
    stringBuffer.append(this.sparkFunction.getCodeFunctionReturn());
    stringBuffer.append(TEXT_26);
    
	            	}
                
    stringBuffer.append(TEXT_27);
    
        }
    }
    
    /**
     * Code generated for tDataprepRun component
     */
    class DataprepFunctionGenerator extends org.talend.designer.spark.generator.FunctionGenerator {

        DataprepFunctionGenerator(org.talend.designer.common.TransformerBase transformer) {
            super(transformer);
        }

        DataprepFunctionGenerator(org.talend.designer.common.TransformerBase transformer, org.talend.designer.spark.generator.SparkFunction sparkFunction) {
            super(transformer, sparkFunction);
        }

        @Override
        public void generate(){
        
    stringBuffer.append(TEXT_28);
    stringBuffer.append(this.sparkFunction.getClassName(cid));
    stringBuffer.append(TEXT_29);
    stringBuffer.append(this.sparkFunction.getCodeFunctionImplementation(getOutValueClass(), getInValueClass()));
    stringBuffer.append(TEXT_30);
    
                this.transformer.generateHelperClasses(true);
                this.transformer.generateTransformContextDeclaration();
                
    stringBuffer.append(TEXT_31);
    stringBuffer.append(this.sparkFunction.getClassName(cid));
    stringBuffer.append(TEXT_32);
    stringBuffer.append(this.sparkFunction.getCodeFunctionReturnedType(this.outValueClass.toString()));
    stringBuffer.append(TEXT_33);
    stringBuffer.append(this.sparkFunction.getCodeImplementedMethod());
    stringBuffer.append(TEXT_34);
    stringBuffer.append(this.sparkFunction.getCodeFunctionArgument(getInValueClass()));
    stringBuffer.append(TEXT_35);
    stringBuffer.append(this.sparkFunction.getCodeThrowException());
    stringBuffer.append(TEXT_36);
    stringBuffer.append(TEXT_37);
    stringBuffer.append(this.sparkFunction.getMethodHeader(this.outValueClass, this.outValue, this.inValueClass, this.inValue));
    stringBuffer.append(TEXT_38);
    stringBuffer.append(this.sparkFunction.getCodeKeyMapping(getInValue()));
    
                        this.transformer.generateTransformContextInitialization();
                        this.transformer.generateTransform(true);
                     
    stringBuffer.append(TEXT_39);
    stringBuffer.append(this.sparkFunction.getCodeFunctionReturn(this.getOutValue(), this.getOutValueClass()));
    stringBuffer.append(TEXT_40);
    
        }
    }

    stringBuffer.append(TEXT_41);
    

/**
 * A common, reusable utility that generates code in the context of a spark
 * component, for reading and writing to a spark RDD.
 */
class SparkRowTransformUtil extends org.talend.designer.common.CommonRowTransformUtil {

    private boolean isMultiOutput = false;

    private org.talend.designer.spark.generator.SparkFunction sparkFunction = null;

    private org.talend.designer.spark.generator.FunctionGenerator functionGenerator = null;

    public SparkRowTransformUtil() {

    }
    
    public SparkRowTransformUtil(org.talend.designer.spark.generator.SparkFunction sparkFunction) {
        this.sparkFunction = sparkFunction;
    }
    
    public void setFunctionGenerator(org.talend.designer.spark.generator.FunctionGenerator functionGenerator) {
        this.functionGenerator = functionGenerator;
    }

    public void setMultiOutput(boolean multiOutput) {
        isMultiOutput = multiOutput;
    }

    public String getCodeToGetInField(String columnName) {
        return functionGenerator.getInValue() + "." + columnName;
    }

    public String getInValue() {
        return functionGenerator.getInValue();
    }

    public String getOutValue() {
        return functionGenerator.getOutValue();
    }

    public String getInValueClass() {
        return functionGenerator.getInValueClass();
    }

    public String getOutValueClass() {
        return functionGenerator.getOutValueClass();
    }

    public String getCodeToGetOutField(boolean isReject, String columnName) {
        return functionGenerator.getOutValue(isReject ? "reject" : "main") + "." + columnName;
    }

    public String getCodeToInitOut(boolean isReject, Iterable<IMetadataColumn> columns) {
        if(!isReject && this.sparkFunction!=null && !isMultiOutput) {
            return this.sparkFunction.getCodeToInitOut(functionGenerator.getOutValue("main"), functionGenerator.getOutValueClass("main"));
        } else {
            return "";
        }
    }

    // Method to avoid using getCodeToInitOut that calls sparkFunction.getCodeToInitOut which creates unnecessary objects
    // Check getCodeToAddToOutput in SparkFunction and its implementation in FlatMapToPairFunction
    public String getCodeToAddToOutput(boolean isReject, Iterable<IMetadataColumn> columns) {
        if(this.sparkFunction!=null && !isMultiOutput) {
            return this.sparkFunction.getCodeToAddToOutput(false, false, functionGenerator.getOutValue(isReject ? "reject" : "main"), functionGenerator.getOutValueClass(isReject ? "reject" : "main"));
        }else if(this.sparkFunction!=null && isMultiOutput){
            if(isReject){
                return this.sparkFunction.getCodeToAddToOutput(true, false, functionGenerator.getOutValue("reject"), functionGenerator.getOutValueClass("reject"));
            }else{
                return this.sparkFunction.getCodeToAddToOutput(true, true, functionGenerator.getOutValue("main"), functionGenerator.getOutValueClass("main"));
            }
        }else {
            return "";
        }
    }

    public String getCodeToSetOutField(boolean isReject, String columnName, String codeValue) {
        return functionGenerator.getOutValue(isReject ? "reject" : "main") + "." + columnName + " = " + codeValue + ";";
    }

    public String getCodeToSetOutField(boolean isReject, String columnName, String codeValue, String operator) {
        return functionGenerator.getOutValue(isReject ? "reject" : "main") + "." + columnName + " " + operator + " " + codeValue + ";";
    }

    public String getCodeToEmit(boolean isReject) {
        if (this.sparkFunction != null && isMultiOutput) {
            if (isReject) {
                return this.sparkFunction.getCodeToEmit(false, functionGenerator.getOutValue("reject"), functionGenerator.getOutValueClass("reject"));
            } else {
                return this.sparkFunction.getCodeToEmit(true, functionGenerator.getOutValue("main"), functionGenerator.getOutValueClass("main"));
            }
        } else {
            if (isReject) {
                return this.sparkFunction.getCodeToInitOut(functionGenerator.getOutValue("reject"), functionGenerator.getOutValueClass("reject"));
            } else {
                return "";
            }
        }
    }

    public void generateSparkCode(final org.talend.designer.common.TransformerBase transformer, final org.talend.designer.spark.generator.SparkFunction sparkFunction) {
        if (transformer.isMultiOutput()) {
            setMultiOutput(true);
        }
        if (transformer.isUnnecessary()) {
            
    stringBuffer.append(TEXT_42);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_43);
    
            return;
        }

        if (transformer.isMultiOutput()) {
            org.talend.designer.spark.generator.SparkFunction localSparkFunction = null;
            if ((sparkFunction instanceof org.talend.designer.spark.generator.FlatMapFunction)
                    || (sparkFunction instanceof org.talend.designer.spark.generator.FlatMapToPairFunction)) {
                localSparkFunction = new org.talend.designer.spark.generator.FlatMapToPairFunction(
                        sparkFunction.isInputPair(),
                        codeGenArgument.getSparkVersion(),
                        sparkFunction.getKeyList());
            } else {
                localSparkFunction = new org.talend.designer.spark.generator.MapToPairFunction(
                        sparkFunction.isInputPair(),
                        sparkFunction.getKeyList());
            }

            org.talend.designer.spark.generator.SparkFunction extractSparkFunction = null;
            if ((sparkFunction instanceof org.talend.designer.spark.generator.FlatMapFunction)
                    || (sparkFunction instanceof org.talend.designer.spark.generator.MapFunction)) {
                extractSparkFunction = new org.talend.designer.spark.generator.MapFunction(
                        sparkFunction.isInputPair(),
                        sparkFunction.getKeyList());
            } else {
                extractSparkFunction = new org.talend.designer.spark.generator.MapToPairFunction(
                        sparkFunction.isInputPair(),
                        sparkFunction.getKeyList());
            }
            this.sparkFunction = localSparkFunction;

            // The multi-output condition is slightly different, and the
            // MapperHelper is configured with internal names for the
            // connections.
            java.util.HashMap<String, String> names = new java.util.HashMap<String, String>();
            names.put("main", transformer.getOutConnMainName());
            names.put("reject", transformer.getOutConnRejectName());

            // Refactoring FunctionGenerator to java so we have to instaniate a MO or SO here
            functionGenerator = new MOFunctionGenerator(transformer, localSparkFunction);
            functionGenerator.init(node, cid, null, transformer.getInConnName(), null, names);

            
    stringBuffer.append(TEXT_44);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_45);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_46);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_47);
    stringBuffer.append(extractSparkFunction.getCodeFunctionImplementationInputFixedType(transformer.getOutConnMainTypeName(), "Boolean", "org.apache.avro.specific.SpecificRecordBase"));
    stringBuffer.append(TEXT_48);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_49);
    stringBuffer.append(extractSparkFunction.getCodeFunctionReturnedType(transformer.getOutConnMainTypeName()));
    stringBuffer.append(TEXT_50);
    stringBuffer.append(TEXT_51);
    stringBuffer.append(transformer.getOutConnMainTypeName());
    stringBuffer.append(TEXT_52);
    stringBuffer.append(transformer.getOutConnMainName());
    stringBuffer.append(TEXT_53);
    stringBuffer.append(transformer.getOutConnMainTypeName());
    stringBuffer.append(TEXT_54);
    stringBuffer.append(extractSparkFunction.getCodeFunctionReturn(transformer.getOutConnMainName(), transformer.getOutConnMainTypeName()));
    stringBuffer.append(TEXT_55);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_56);
    stringBuffer.append(extractSparkFunction.getCodeFunctionImplementationInputFixedType(transformer.getOutConnRejectTypeName(), "Boolean", "org.apache.avro.specific.SpecificRecordBase"));
    stringBuffer.append(TEXT_57);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_58);
    stringBuffer.append(extractSparkFunction.getCodeFunctionReturnedType(transformer.getOutConnRejectTypeName()));
    stringBuffer.append(TEXT_59);
    stringBuffer.append(TEXT_60);
    stringBuffer.append(transformer.getOutConnRejectTypeName());
    stringBuffer.append(TEXT_61);
    stringBuffer.append(transformer.getOutConnRejectName());
    stringBuffer.append(TEXT_62);
    stringBuffer.append(transformer.getOutConnRejectTypeName());
    stringBuffer.append(TEXT_63);
    stringBuffer.append(extractSparkFunction.getCodeFunctionReturn(transformer.getOutConnRejectName(), transformer.getOutConnRejectTypeName()));
    stringBuffer.append(TEXT_64);
    
        } else {
            // Refactoring FunctionGenerator to java so we have to instaniate a MO or SO here
            functionGenerator = new SOFunctionGenerator(transformer, sparkFunction);

            functionGenerator.init(node, cid, null, transformer.getInConnName(), null,
                    transformer.getOutConnMainName() != null
                        ? transformer.getOutConnMainName()
                                : transformer.getOutConnRejectName());
        }
        functionGenerator.generate();
    }

    public void generateSparkConfig(final org.talend.designer.common.TransformerBase transformer, final org.talend.designer.spark.generator.SparkFunction sparkFunction) {
        if (transformer.isUnnecessary()) {
            
    stringBuffer.append(TEXT_65);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_66);
    
            return;
        }

        if (transformer.isMultiOutput()) {
            String localFunctionType = "mapToPair";
            if ((sparkFunction instanceof org.talend.designer.spark.generator.FlatMapFunction)
                    || (sparkFunction instanceof org.talend.designer.spark.generator.FlatMapToPairFunction)) {
                localFunctionType = "flatMapToPair";
            }

            String extractFunctionType = "mapToPair";
            if ((sparkFunction instanceof org.talend.designer.spark.generator.FlatMapFunction)
                    || (sparkFunction instanceof org.talend.designer.spark.generator.MapFunction)) {
                extractFunctionType = "map";
            }
            
    stringBuffer.append(TEXT_67);
    stringBuffer.append(TEXT_68);
    stringBuffer.append(sparkFunction.isStreaming() ?"org.apache.spark.streaming.api.java.JavaPairDStream":"org.apache.spark.api.java.JavaPairRDD");
    stringBuffer.append(TEXT_69);
    stringBuffer.append(transformer.getOutConnMainName());
    stringBuffer.append(TEXT_70);
    stringBuffer.append(transformer.getInConnName());
    stringBuffer.append(TEXT_71);
    stringBuffer.append(localFunctionType);
    stringBuffer.append(TEXT_72);
    stringBuffer.append(sparkFunction.getClassName(cid));
    stringBuffer.append(TEXT_73);
    stringBuffer.append(TEXT_74);
    stringBuffer.append(sparkFunction.getConfigReturnedType(transformer.getOutConnMainTypeName()));
    stringBuffer.append(TEXT_75);
    stringBuffer.append(transformer.getOutConnMainName());
    stringBuffer.append(TEXT_76);
    stringBuffer.append(transformer.getOutConnMainName());
    stringBuffer.append(TEXT_77);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_78);
    stringBuffer.append(extractFunctionType);
    stringBuffer.append(TEXT_79);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_80);
    stringBuffer.append(TEXT_81);
    stringBuffer.append(sparkFunction.getConfigReturnedType(transformer.getOutConnRejectTypeName()));
    stringBuffer.append(TEXT_82);
    stringBuffer.append(transformer.getOutConnRejectName());
    stringBuffer.append(TEXT_83);
    stringBuffer.append(transformer.getOutConnMainName());
    stringBuffer.append(TEXT_84);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_85);
    stringBuffer.append(extractFunctionType);
    stringBuffer.append(TEXT_86);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_87);
    
        } else {
            functionGenerator = new SOFunctionGenerator(transformer, sparkFunction);

            functionGenerator.init(node, cid, null, transformer.getInConnName(), null,
                    transformer.getOutConnMainName() != null
                        ? transformer.getOutConnMainName()
                                : transformer.getOutConnRejectName());
            
    stringBuffer.append(TEXT_88);
    stringBuffer.append(sparkFunction.getConfigReturnedType(transformer.getOutConnMainName() != null ? transformer.getOutConnMainTypeName() : transformer.getOutConnRejectTypeName()));
    stringBuffer.append(TEXT_89);
    stringBuffer.append(transformer.getOutConnMainName() != null ? transformer.getOutConnMainName() : transformer.getOutConnRejectName());
    stringBuffer.append(TEXT_90);
    stringBuffer.append(transformer.getInConnName());
    stringBuffer.append(TEXT_91);
    stringBuffer.append(sparkFunction.getConfigTransformation());
    stringBuffer.append(TEXT_92);
    stringBuffer.append(sparkFunction.getClassName(cid));
    stringBuffer.append(TEXT_93);
    
        }
    }
}


    stringBuffer.append(TEXT_94);
    

/**
 * Contains common processing for tPatternMasking code generation.  This is
 * used in MapReduce and Storm components.
 *
 * The following imports must occur before importing this file:
 * @ include file="@{org.talend.designer.components.mrprovider}/resources/utils/common_codegen_util.javajet"
 */
class TPatternMaskingUtil extends org.talend.designer.common.TransformerBase {

    java.util.List<? extends IConnection> outConns = node.getOutgoingSortedConnections();
    //Find the tSparkConfiguration and define which Spark version is currently used.
    final java.util.List<? extends INode> sparkConfigs = node.getProcess().getNodesOfType("tSparkConfiguration");
    boolean useLocalMode = false;
    final private String incomingConnName;
    final private String outgoingConnName;

    /** The list of columns that should be copied directly from the input to
     *  the output schema (where they have the same column names). */
    final private Iterable<IMetadataColumn> copiedInColumns;

    /** Columns in the output schema that are not copied directly from the
     *  input schema (excluding reject fields). */
    final private java.util.List<IMetadataColumn> newOutColumns;
    final private java.util.List<IMetadataColumn> inputColumns;
    
    final private java.util.Set<String> columnsToMask =  new java.util.HashSet<String>();
    final private java.util.List<java.util.Map<String, String>> ruleTableList = (java.util.List<java.util.Map<String, String>>)ElementParameterParser.getObjectValue(node, "__RULE_TABLE__");

    final private String randomSeedString = ElementParameterParser.getValue(node, "__RANDOM_SEED__");
    final private boolean keepNull = ("true").equals(ElementParameterParser.getValue(node, "__KEEP_NULL__"));
    final private boolean keepEmpty = ("true").equals(ElementParameterParser.getValue(node, "__KEEP_EMPTY__"));
    final private boolean keep = ("true").equals(ElementParameterParser.getValue(node, "__OUTPUT_ORIGINAL__"));


    public TPatternMaskingUtil(INode node,
            org.talend.designer.common.BigDataCodeGeneratorArgument argument,
            org.talend.designer.common.CommonRowTransformUtil rowTransformUtil) {
        super(node, argument, rowTransformUtil, "FLOW", "REJECT");

        if (null != getInConn() && null != getOutConnMain()) {
            copiedInColumns = getColumnsUnion(getInColumns(), getOutColumnsMain());
            newOutColumns = getColumnsDiff(getOutColumnsMain(), getInColumns());
            inputColumns=getInConn().getMetadataTable().getListColumns() ;
        } else {
            copiedInColumns = null;
            newOutColumns = null;
            inputColumns=new java.util.ArrayList<IMetadataColumn>();
        }
        incomingConnName=getInConn().getName();
        outgoingConnName=getOutConnMainName();

		for(java.util.Map<String, String> columnModifMap : ruleTableList) {
			columnsToMask.add(columnModifMap.get("COLUMN_TO_MASK").toLowerCase());
		}
    }

    public void generateTransformContextInitialization(){

    stringBuffer.append(TEXT_95);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_96);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_97);
    
             if(randomSeedString.length() > 0 && !"\"\"".equals(randomSeedString)) {
        
    stringBuffer.append(TEXT_98);
    stringBuffer.append(randomSeedString);
    stringBuffer.append(TEXT_99);
    
             }
        
    stringBuffer.append(TEXT_100);
    
    }

    public void generateTransform() {
        generateTransform(false);
    }

    /**
     * Generates code that performs the tranformation from one input string
     * to many output strings, or to a reject flow.
     */
    public void generateTransform(boolean hasAReturnedType) {

    for(int i = 0; i < inputColumns.size(); i++) {

        IMetadataColumn column = inputColumns.get(i);

        if(columnsToMask.contains(column.getLabel().toLowerCase())){

    stringBuffer.append(TEXT_101);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_102);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_103);
    
            for(java.util.Map<String, String> columnModifMap : ruleTableList) {

		        String columnToMask = columnModifMap.get("COLUMN_TO_MASK");
                if (columnToMask.equalsIgnoreCase(column.getLabel())){
	      	       String fieldType = columnModifMap.get("FIELD_TYPE");
                   String values = null;
		           if (fieldType.equals("ENUMERATION"))
		              values = columnModifMap.get("VALUES");
		
		           String interval = null;
		           if (fieldType.equals("INTERVAL"))
			          interval = columnModifMap.get("INTERVAL");
		           if (fieldType.equals("DATEPATTERN"))
			          interval = columnModifMap.get("DATE_INTERVAL");
                   
		           String sKeepFormat = columnModifMap.get("KEEP_FORMAT");
                   String typeToGenerate = JavaTypesManager.getTypeToGenerate(column.getTalendType(), true);//TDQ-11328: fix compil err for type "int"(not nullable)
                   JavaType javaType = JavaTypesManager.getJavaTypeFromId(column.getTalendType());
                   
                   if (fieldType.equals("ENUMERATION_FROM_FILE")) {
                       values = columnModifMap.get("PATH");
                    
    stringBuffer.append(TEXT_104);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_105);
    stringBuffer.append(columnToMask);
    stringBuffer.append(TEXT_106);
    stringBuffer.append(values);
    stringBuffer.append(TEXT_107);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_108);
    stringBuffer.append(columnToMask);
    stringBuffer.append(TEXT_109);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_110);
    stringBuffer.append(columnToMask);
    stringBuffer.append(TEXT_111);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_112);
    stringBuffer.append(columnToMask);
    stringBuffer.append(TEXT_113);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_114);
    stringBuffer.append(columnToMask);
    stringBuffer.append(TEXT_115);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_116);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_117);
    stringBuffer.append(fieldType);
    stringBuffer.append(TEXT_118);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_119);
    stringBuffer.append(columnToMask);
    stringBuffer.append(TEXT_120);
    stringBuffer.append(interval);
    stringBuffer.append(TEXT_121);
    
                    }else{

    stringBuffer.append(TEXT_122);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_123);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_124);
    stringBuffer.append(fieldType);
    stringBuffer.append(TEXT_125);
    stringBuffer.append(values);
    stringBuffer.append(TEXT_126);
    stringBuffer.append(interval);
    stringBuffer.append(TEXT_127);
    }
               }
           } // for

    stringBuffer.append(TEXT_128);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_129);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_130);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_131);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_132);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_133);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_134);
    stringBuffer.append(keepNull);
    stringBuffer.append(TEXT_135);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_136);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_137);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_138);
    stringBuffer.append(keepEmpty);
    stringBuffer.append(TEXT_139);
    
        } // if

    
    } // for inputColumns

    stringBuffer.append(TEXT_140);
    stringBuffer.append(incomingConnName);
    stringBuffer.append(TEXT_141);
    stringBuffer.append(outgoingConnName);
    stringBuffer.append(TEXT_142);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_143);
    stringBuffer.append(incomingConnName);
    stringBuffer.append(TEXT_144);
    stringBuffer.append(outgoingConnName);
    stringBuffer.append(TEXT_145);
    stringBuffer.append(outgoingConnName);
    stringBuffer.append(TEXT_146);
    stringBuffer.append(incomingConnName);
    stringBuffer.append(TEXT_147);
    stringBuffer.append(TEXT_148);
    stringBuffer.append(outgoingConnName);
    stringBuffer.append(TEXT_149);
    stringBuffer.append(outgoingConnName);
    stringBuffer.append(TEXT_150);
     
            for(int i = 0; i < inputColumns.size(); i++){
                IMetadataColumn column = inputColumns.get(i);
        
    stringBuffer.append(TEXT_151);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_152);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_153);
    
            }
        
    stringBuffer.append(TEXT_154);
    stringBuffer.append(outgoingConnName);
    stringBuffer.append(TEXT_155);
    stringBuffer.append(outgoingConnName);
    stringBuffer.append(TEXT_156);
     
            for(int i = 0; i < inputColumns.size(); i++){
                IMetadataColumn column = inputColumns.get(i);
	        if(columnsToMask.contains(column.getLabel().toLowerCase())){ 
                    String typeToGenerate = JavaTypesManager.getTypeToGenerate(column.getTalendType(), column.isNullable());
                    JavaType javaType = JavaTypesManager.getJavaTypeFromId(column.getTalendType());
    
    stringBuffer.append(TEXT_157);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_158);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_159);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_160);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_161);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_162);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_163);
    stringBuffer.append(outgoingConnName);
    stringBuffer.append(TEXT_164);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_165);
    if (column.isNullable()) {
    stringBuffer.append(TEXT_166);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_167);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_168);
    stringBuffer.append(TEXT_169);
    stringBuffer.append(outgoingConnName);
    stringBuffer.append(TEXT_170);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_171);
    }
    stringBuffer.append(TEXT_172);
                              
                            if(javaType == JavaTypesManager.STRING || javaType == JavaTypesManager.OBJECT){
                        
    stringBuffer.append(TEXT_173);
    stringBuffer.append(outgoingConnName);
    stringBuffer.append(TEXT_174);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_175);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_176);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_177);
    
                            }
                            else if(javaType == JavaTypesManager.DATE){
                                String patternValue = column.getPattern() == null || column.getPattern().trim().length() == 0 ? null : column.getPattern();
                        
    stringBuffer.append(TEXT_178);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_179);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_180);
    stringBuffer.append(TEXT_181);
    stringBuffer.append(outgoingConnName);
    stringBuffer.append(TEXT_182);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_183);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_184);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_185);
    stringBuffer.append(TEXT_186);
    stringBuffer.append(outgoingConnName);
    stringBuffer.append(TEXT_187);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_188);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_189);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_190);
    stringBuffer.append(patternValue);
    stringBuffer.append(TEXT_191);
    
                            }
                            else if(javaType == JavaTypesManager.BYTE_ARRAY){
                        
    stringBuffer.append(TEXT_192);
    stringBuffer.append(outgoingConnName);
    stringBuffer.append(TEXT_193);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_194);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_195);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_196);
    
                            }
                            else{
                        
    stringBuffer.append(TEXT_197);
    stringBuffer.append(TEXT_198);
    stringBuffer.append(outgoingConnName);
    stringBuffer.append(TEXT_199);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_200);
    stringBuffer.append( typeToGenerate );
    stringBuffer.append(TEXT_201);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_202);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_203);
    
                            } 
                        
    stringBuffer.append(TEXT_204);
    if(column.isNullable()){
    stringBuffer.append(TEXT_205);
    }
    
                } // if
            } // for
    
    stringBuffer.append(TEXT_206);
    stringBuffer.append(outgoingConnName);
    stringBuffer.append(TEXT_207);
    stringBuffer.append(outgoingConnName);
    stringBuffer.append(TEXT_208);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_209);
    stringBuffer.append(incomingConnName);
    stringBuffer.append(TEXT_210);
    stringBuffer.append(keep);
    stringBuffer.append(TEXT_211);
    stringBuffer.append(TEXT_212);
    stringBuffer.append(getRowTransform().getCodeToEmit(false));
    stringBuffer.append(TEXT_213);
    stringBuffer.append(outgoingConnName);
    stringBuffer.append(TEXT_214);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_215);
    stringBuffer.append(outgoingConnName);
    stringBuffer.append(TEXT_216);
    stringBuffer.append(TEXT_217);
    stringBuffer.append(outgoingConnName);
    stringBuffer.append(TEXT_218);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_219);
    stringBuffer.append(TEXT_220);
    stringBuffer.append(getRowTransform().getCodeToInitOut(null == getOutConnMain(), newOutColumns));
    stringBuffer.append(TEXT_221);
    
    }
}

    
org.talend.designer.spark.generator.SparkFunction sparkFunction = null;
String requiredInputType = bigDataNode.getRequiredInputType();
String requiredOutputType = bigDataNode.getRequiredOutputType();
String incomingType = bigDataNode.getIncomingType();
String outgoingType = bigDataNode.getOutgoingType();
boolean inputIsPair = requiredInputType != null ? "KEYVALUE".equals(requiredInputType) : "KEYVALUE".equals(incomingType);

String type = requiredOutputType == null ? outgoingType : requiredOutputType;
if("KEYVALUE".equals(type)) {
 sparkFunction = new org.talend.designer.spark.generator.FlatMapToPairFunction(inputIsPair, codeGenArgument.getSparkVersion());
} else if("VALUE".equals(type)) {
 sparkFunction = new org.talend.designer.spark.generator.FlatMapFunction(inputIsPair, codeGenArgument.getSparkVersion());
}

boolean isNodeInBatchMode = org.talend.designer.common.tmap.LookupUtil.isNodeInBatchMode(node);
if("SPARKSTREAMING".equals(node.getComponent().getType()) && !isNodeInBatchMode) {
 sparkFunction.setStreaming(true);
}

final SparkRowTransformUtil sparkTransformUtil = new SparkRowTransformUtil(sparkFunction);
final TPatternMaskingUtil tPatternMaskingUtil = new TPatternMaskingUtil(
     node, codeGenArgument, sparkTransformUtil);
     
sparkTransformUtil.generateSparkConfig(tPatternMaskingUtil, sparkFunction);

java.util.List<java.util.Map<String, String>> rules = (java.util.List<java.util.Map<String, String>>)ElementParameterParser.getObjectValue(node, "__RULE_TABLE__");
for (java.util.Map<String, String> rule : rules) {
    String ruleType = rule.get("FIELD_TYPE");
    String pathValue = rule.get("PATH");
    if("ENUMERATION_FROM_FILE".equals(ruleType)){
    
    
        if("SPARKSTREAMING".equals(node.getComponent().getType()) && !isNodeInBatchMode) {
        
    stringBuffer.append(TEXT_222);
    stringBuffer.append(pathValue);
    stringBuffer.append(TEXT_223);
    
        } else {
        
    stringBuffer.append(TEXT_224);
    stringBuffer.append(pathValue);
    stringBuffer.append(TEXT_225);
    
        }
    }
}

    stringBuffer.append(TEXT_226);
    stringBuffer.append(TEXT_227);
    return stringBuffer.toString();
  }
}
