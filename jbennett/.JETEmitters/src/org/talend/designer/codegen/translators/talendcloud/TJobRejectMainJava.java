package org.talend.designer.codegen.translators.talendcloud;

import java.util.List;
import org.talend.core.model.process.INode;
import org.talend.core.model.process.IConnection;
import org.talend.core.model.process.IConnectionCategory;
import org.talend.core.model.metadata.IMetadataTable;
import org.talend.core.model.metadata.IMetadataColumn;
import org.talend.designer.codegen.config.CodeGeneratorArgument;

public class TJobRejectMainJava
{
  protected static String nl;
  public static synchronized TJobRejectMainJava create(String lineSeparator)
  {
    nl = lineSeparator;
    TJobRejectMainJava result = new TJobRejectMainJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "                    rejectWriter.begin();";
  protected final String TEXT_2 = NL + "                        rejectWriter.write(\"";
  protected final String TEXT_3 = "\", ";
  protected final String TEXT_4 = ".";
  protected final String TEXT_5 = ");";
  protected final String TEXT_6 = NL + "                    rejectWriter.end();";
  protected final String TEXT_7 = NL;

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    
CodeGeneratorArgument codeGenArgument = (CodeGeneratorArgument) argument;
INode node = (INode) codeGenArgument.getArgument();
String cid = node.getUniqueName();

for (IConnection conn : node.getIncomingConnections()) {
	if (conn.getLineStyle().hasConnectionCategory(IConnectionCategory.DATA)) {
		List<IMetadataTable> metadatas = node.getMetadataList();
		if (null != metadatas && 0 < metadatas.size()) {
			IMetadataTable metadata = metadatas.get(0);
			if (null != metadata) {
				List<IMetadataColumn> metadataColumns = metadata.getListColumns();

    stringBuffer.append(TEXT_1);
                      for (IMetadataColumn column : metadataColumns) {
                        String name = column.getLabel(); 
    stringBuffer.append(TEXT_2);
    stringBuffer.append(name);
    stringBuffer.append(TEXT_3);
    stringBuffer.append(conn.getName());
    stringBuffer.append(TEXT_4);
    stringBuffer.append(name);
    stringBuffer.append(TEXT_5);
                      } 
    stringBuffer.append(TEXT_6);
    
				break;
			}
		}
	}
}

    stringBuffer.append(TEXT_7);
    return stringBuffer.toString();
  }
}
