package org.talend.designer.codegen.translators.messaging.pubsub;

import java.util.Map.Entry;
import org.talend.core.model.process.ElementParameterParser;
import org.talend.core.model.process.IConnection;
import org.talend.core.model.process.IConnectionCategory;
import org.talend.core.model.process.INode;
import org.talend.designer.common.BigDataCodeGeneratorArgument;
import org.talend.designer.common.tgooglecloudconfiguration.TGoogleCloudConfigurationUtil;

public class TPubSubOutputSparkstreamingconfigJava
{
  protected static String nl;
  public static synchronized TPubSubOutputSparkstreamingconfigJava create(String lineSeparator)
  {
    nl = lineSeparator;
    TPubSubOutputSparkstreamingconfigJava result = new TPubSubOutputSparkstreamingconfigJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "";
  protected final String TEXT_2 = NL;
  protected final String TEXT_3 = NL + "    ";
  protected final String TEXT_4 = "_ForeachPartition.createIfNotExistsTopic(context);";
  protected final String TEXT_5 = NL + "rdd_";
  protected final String TEXT_6 = ".foreachRDD( new ";
  protected final String TEXT_7 = "_ForeachRDD(job));";
  protected final String TEXT_8 = NL;

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    stringBuffer.append(TEXT_1);
    
// Parse the inputs to this javajet generator.
final BigDataCodeGeneratorArgument codeGenArgument = (BigDataCodeGeneratorArgument) argument;
final INode node = (INode)codeGenArgument.getArgument();
final String cid = node.getUniqueName();

IConnection inConn = null;
if (node.getIncomingConnections() != null) {
    for (IConnection in : node.getIncomingConnections()) {
        if (in.getLineStyle().hasConnectionCategory(IConnectionCategory.DATA)) {
            inConn = in;
            break;
        }
    }
}
if (inConn == null) {
    return "";
}

    stringBuffer.append(TEXT_2);
    
if ("CREATE_IF_NOT_EXISTS".equals(ElementParameterParser.getValue(node, "__TOPIC_OPERATION__"))) {
    
    stringBuffer.append(TEXT_3);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_4);
    
}

    stringBuffer.append(TEXT_5);
    stringBuffer.append(inConn.getName());
    stringBuffer.append(TEXT_6);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_7);
    stringBuffer.append(TEXT_8);
    return stringBuffer.toString();
  }
}
