package org.talend.designer.codegen.translators.mapreduce.input;

import java.util.List;
import org.talend.core.model.metadata.IMetadataColumn;
import org.talend.core.model.metadata.IMetadataTable;
import org.talend.core.model.metadata.types.JavaType;
import org.talend.core.model.metadata.types.JavaTypesManager;
import org.talend.core.model.process.EConnectionType;
import org.talend.core.model.process.ElementParameterParser;
import org.talend.core.model.process.IBigDataNode;
import org.talend.core.model.process.IConnection;
import org.talend.core.model.process.IConnectionCategory;
import org.talend.core.model.process.INode;
import org.talend.designer.common.BigDataCodeGeneratorArgument;

public class TInputFormatAvroSparkstreamingconfigJava
{
  protected static String nl;
  public static synchronized TInputFormatAvroSparkstreamingconfigJava create(String lineSeparator)
  {
    nl = lineSeparator;
    TInputFormatAvroSparkstreamingconfigJava result = new TInputFormatAvroSparkstreamingconfigJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "                org.apache.spark.api.java.JavaPairRDD<NullWritable, ";
  protected final String TEXT_2 = "> rdd_";
  protected final String TEXT_3 = " =" + NL + "                        ctx.sparkContext().hadoopRDD(" + NL + "                                job,";
  protected final String TEXT_4 = NL + "                                ";
  protected final String TEXT_5 = "StructInputFormat.class," + NL + "                                NullWritable.class,";
  protected final String TEXT_6 = NL + "                                ";
  protected final String TEXT_7 = ".class,";
  protected final String TEXT_8 = NL + "                                ";
  protected final String TEXT_9 = ");";
  protected final String TEXT_10 = NL + "                org.apache.spark.api.java.JavaPairRDD<NullWritable, ";
  protected final String TEXT_11 = "> rdd_";
  protected final String TEXT_12 = " =" + NL + "                        ctx.sparkContext().hadoopRDD(" + NL + "                                job,";
  protected final String TEXT_13 = NL + "                                ";
  protected final String TEXT_14 = "StructInputFormat.class," + NL + "                                NullWritable.class,";
  protected final String TEXT_15 = NL + "                                ";
  protected final String TEXT_16 = ".class);";

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    
BigDataCodeGeneratorArgument codeGenArgument = (BigDataCodeGeneratorArgument) argument;
INode node = (INode)codeGenArgument.getArgument();
String cid = node.getUniqueName();

    
List<IMetadataTable> metadatas = node.getMetadataList();

if (metadatas != null && metadatas.size() > 0) {
    IMetadataTable metadata = metadatas.get(0);
    if(metadata != null){
        List< ? extends IConnection> connections = node.getOutgoingConnections();
        if ((connections != null) && (connections.size() > 0)) {
            IConnection connection = connections.get(0);
            String connName = connection.getName();
            String recordStructName = codeGenArgument.getRecordStructName(connection);
            boolean useMinPartitions = ElementParameterParser.getBooleanValue(node,"__USE_MIN_PARTITIONS__");
            if (useMinPartitions) {
                String minPartitions = ElementParameterParser.getValue(node,"__MIN_PARTITIONS__");
                
    stringBuffer.append(TEXT_1);
    stringBuffer.append(recordStructName);
    stringBuffer.append(TEXT_2);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_3);
    stringBuffer.append(TEXT_4);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_5);
    stringBuffer.append(TEXT_6);
    stringBuffer.append(recordStructName);
    stringBuffer.append(TEXT_7);
    stringBuffer.append(TEXT_8);
    stringBuffer.append(minPartitions);
    stringBuffer.append(TEXT_9);
    
            } else {
                
    stringBuffer.append(TEXT_10);
    stringBuffer.append(recordStructName);
    stringBuffer.append(TEXT_11);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_12);
    stringBuffer.append(TEXT_13);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_14);
    stringBuffer.append(TEXT_15);
    stringBuffer.append(recordStructName);
    stringBuffer.append(TEXT_16);
    
            }
        }
    }
}

    return stringBuffer.toString();
  }
}
