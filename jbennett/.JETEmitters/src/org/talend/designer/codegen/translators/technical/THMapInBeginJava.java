package org.talend.designer.codegen.translators.technical;

import java.util.Set;
import org.talend.transform.component.thmap.MapperComponent;
import org.talend.core.model.metadata.IMetadataTable;
import org.talend.core.model.metadata.IMetadataColumn;
import org.talend.core.model.metadata.types.JavaTypesManager;
import org.talend.core.model.process.IConnection;
import org.talend.core.model.process.IConnectionCategory;
import org.talend.core.model.process.INode;
import org.talend.core.model.process.ElementParameterParser;
import org.talend.core.model.utils.TalendTextUtils;
import org.talend.designer.codegen.config.CodeGeneratorArgument;

public class THMapInBeginJava
{
  protected static String nl;
  public static synchronized THMapInBeginJava create(String lineSeparator)
  {
    nl = lineSeparator;
    THMapInBeginJava result = new THMapInBeginJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "    //THMAPIN_BEGIN tHMap: ";
  protected final String TEXT_2 = NL + "\tint nb_line_";
  protected final String TEXT_3 = " = 0;";
  protected final String TEXT_4 = NL + "    String executorId_";
  protected final String TEXT_5 = " = Thread.currentThread().getId()+\"_";
  protected final String TEXT_6 = "_\"+\"mapExecutor\";" + NL + "    org.talend.transform.runtime.common.MapExecutor mapExec_";
  protected final String TEXT_7 = " = (org.talend.transform.runtime.common.MapExecutor) ";
  protected final String TEXT_8 = ".this.globalMap.get(executorId_";
  protected final String TEXT_9 = ");" + NL + "    if (mapExec_";
  protected final String TEXT_10 = " == null) {" + NL + "      mapExec_";
  protected final String TEXT_11 = " = org.talend.transform.runtime.common.MapExecutorFactory.get();";
  protected final String TEXT_12 = NL + "      ";
  protected final String TEXT_13 = ".this.globalMap.put(executorId_";
  protected final String TEXT_14 = ", mapExec_";
  protected final String TEXT_15 = ");" + NL + "  " + NL + "      mapExec_";
  protected final String TEXT_16 = ".setLoggingLevel(\"";
  protected final String TEXT_17 = "\");" + NL + "      mapExec_";
  protected final String TEXT_18 = ".setExceptionThreshold(org.talend.transform.runtime.common.SeverityLevel.HIGHEST);" + NL + "    }" + NL + "" + NL + "    java.util.Map<String, Object> ecProps_";
  protected final String TEXT_19 = " = new java.util.HashMap<String, Object>();";
  protected final String TEXT_20 = NL + "    ";
  protected final String TEXT_21 = ".synchronizeContext();" + NL + "" + NL + "    java.util.Enumeration<?> propertyNames_";
  protected final String TEXT_22 = " = ";
  protected final String TEXT_23 = ".propertyNames();" + NL + "    while (propertyNames_";
  protected final String TEXT_24 = ".hasMoreElements()) {" + NL + "        String key_";
  protected final String TEXT_25 = " = (String) propertyNames_";
  protected final String TEXT_26 = ".nextElement();" + NL + "        Object value_";
  protected final String TEXT_27 = " = (Object) ";
  protected final String TEXT_28 = ".get(key_";
  protected final String TEXT_29 = ");" + NL + "        ecProps_";
  protected final String TEXT_30 = ".put(\"context.\"+key_";
  protected final String TEXT_31 = ", value_";
  protected final String TEXT_32 = ");" + NL + "    }" + NL + "     mapExec_";
  protected final String TEXT_33 = ".setExecutionProperties(\"";
  protected final String TEXT_34 = "\", ecProps_";
  protected final String TEXT_35 = ");" + NL;
  protected final String TEXT_36 = NL + "    String mapPath = (String) context.get(\"";
  protected final String TEXT_37 = "\");" + NL + "    mapExec_";
  protected final String TEXT_38 = ".setUp(\"";
  protected final String TEXT_39 = "\", new org.talend.transform.runtime.common.MapLocation(\"";
  protected final String TEXT_40 = "\", mapPath));";
  protected final String TEXT_41 = NL + "    mapExec_";
  protected final String TEXT_42 = ".setUp(\"";
  protected final String TEXT_43 = "\", new org.talend.transform.runtime.common.MapLocation(\"";
  protected final String TEXT_44 = "\", \"";
  protected final String TEXT_45 = "\"));";
  protected final String TEXT_46 = NL + "    java.util.Map<String, javax.xml.transform.Source> sources_";
  protected final String TEXT_47 = " = new java.util.LinkedHashMap<>();";
  protected final String TEXT_48 = NL + "    //Setting one source only" + NL + "    mapExec_";
  protected final String TEXT_49 = ".setObjectSource(\"";
  protected final String TEXT_50 = "\", list_";
  protected final String TEXT_51 = ");";
  protected final String TEXT_52 = NL + "   //Source for Connection:  ";
  protected final String TEXT_53 = NL + "   javax.xml.transform.Source connectionSource_";
  protected final String TEXT_54 = "_";
  protected final String TEXT_55 = " = null;";
  protected final String TEXT_56 = NL + "   java.util.Map<";
  protected final String TEXT_57 = "Struct, ";
  protected final String TEXT_58 = "Struct> tHash_";
  protected final String TEXT_59 = " = (java.util.Map<";
  protected final String TEXT_60 = "Struct, ";
  protected final String TEXT_61 = "Struct>)globalMap.get(\"tHash_";
  protected final String TEXT_62 = "\");";
  protected final String TEXT_63 = NL + "   ";
  protected final String TEXT_64 = "Struct ";
  protected final String TEXT_65 = " = null;" + NL + "   if (tHash_";
  protected final String TEXT_66 = ".values()!=null" + NL + "      && tHash_";
  protected final String TEXT_67 = ".values().iterator().hasNext())";
  protected final String TEXT_68 = NL + "      ";
  protected final String TEXT_69 = " = tHash_";
  protected final String TEXT_70 = ".values().iterator().next();";
  protected final String TEXT_71 = NL + "    //Source for ";
  protected final String TEXT_72 = " ";
  protected final String TEXT_73 = NL + "    javax.xml.transform.stream.StreamSource ss_";
  protected final String TEXT_74 = "_";
  protected final String TEXT_75 = " = new javax.xml.transform.stream.StreamSource();" + NL + "    if (";
  protected final String TEXT_76 = "!=null&&";
  protected final String TEXT_77 = ".";
  protected final String TEXT_78 = "!=null) {" + NL + "        ss_";
  protected final String TEXT_79 = "_";
  protected final String TEXT_80 = ".setReader(new java.io.StringReader(";
  protected final String TEXT_81 = ".";
  protected final String TEXT_82 = "));" + NL + "    } else {" + NL + "        ss_";
  protected final String TEXT_83 = "_";
  protected final String TEXT_84 = ".setReader(new java.io.StringReader(\"\"));" + NL + "    }" + NL + "    connectionSource_";
  protected final String TEXT_85 = "_";
  protected final String TEXT_86 = " = ss_";
  protected final String TEXT_87 = "_";
  protected final String TEXT_88 = ";";
  protected final String TEXT_89 = NL + "    org.dom4j.io.DocumentSource ds_";
  protected final String TEXT_90 = "_";
  protected final String TEXT_91 = " = null;" + NL + "    if (";
  protected final String TEXT_92 = "!=null&&";
  protected final String TEXT_93 = ".";
  protected final String TEXT_94 = "!=null){" + NL + "        ds_";
  protected final String TEXT_95 = "_";
  protected final String TEXT_96 = " = new org.dom4j.io.DocumentSource(((routines.system.Document)";
  protected final String TEXT_97 = ".";
  protected final String TEXT_98 = ").getDocument());" + NL + "    } else {" + NL + "        ds_";
  protected final String TEXT_99 = "_";
  protected final String TEXT_100 = " = new org.dom4j.io.DocumentSource(new routines.system.Document().getDocument());" + NL + "    }" + NL + "    connectionSource_";
  protected final String TEXT_101 = "_";
  protected final String TEXT_102 = " = ds_";
  protected final String TEXT_103 = "_";
  protected final String TEXT_104 = ";";
  protected final String TEXT_105 = NL + "    javax.xml.transform.stream.StreamSource ss_";
  protected final String TEXT_106 = "_";
  protected final String TEXT_107 = " = new javax.xml.transform.stream.StreamSource();" + NL + "    ss_";
  protected final String TEXT_108 = "_";
  protected final String TEXT_109 = ".setInputStream(new java.io.ByteArrayInputStream((byte[])";
  protected final String TEXT_110 = ".";
  protected final String TEXT_111 = "));" + NL + "    connectionSource_";
  protected final String TEXT_112 = "_";
  protected final String TEXT_113 = " = ss_";
  protected final String TEXT_114 = "_";
  protected final String TEXT_115 = ";";
  protected final String TEXT_116 = NL + "    if (";
  protected final String TEXT_117 = ".";
  protected final String TEXT_118 = " instanceof java.io.InputStream) {" + NL + "         javax.xml.transform.stream.StreamSource ss_";
  protected final String TEXT_119 = "_";
  protected final String TEXT_120 = " = new javax.xml.transform.stream.StreamSource();" + NL + "         ss_";
  protected final String TEXT_121 = "_";
  protected final String TEXT_122 = ".setInputStream((java.io.InputStream)";
  protected final String TEXT_123 = ".";
  protected final String TEXT_124 = ");" + NL + "         connectionSource_";
  protected final String TEXT_125 = "_";
  protected final String TEXT_126 = " = ss_";
  protected final String TEXT_127 = "_";
  protected final String TEXT_128 = ";" + NL + "    } else if (";
  protected final String TEXT_129 = ".";
  protected final String TEXT_130 = " instanceof String) {" + NL + "        javax.xml.transform.stream.StreamSource ss_";
  protected final String TEXT_131 = "_";
  protected final String TEXT_132 = " = new javax.xml.transform.stream.StreamSource();" + NL + "        ss_";
  protected final String TEXT_133 = "_";
  protected final String TEXT_134 = ".setReader(new java.io.StringReader((String)";
  protected final String TEXT_135 = ".";
  protected final String TEXT_136 = "));" + NL + "        connectionSource_";
  protected final String TEXT_137 = "_";
  protected final String TEXT_138 = " = ss_";
  protected final String TEXT_139 = "_";
  protected final String TEXT_140 = ";" + NL + "    } else if (";
  protected final String TEXT_141 = ".";
  protected final String TEXT_142 = " instanceof byte[]) {" + NL + "        javax.xml.transform.stream.StreamSource ss_";
  protected final String TEXT_143 = "_";
  protected final String TEXT_144 = " = new javax.xml.transform.stream.StreamSource();" + NL + "        ss_";
  protected final String TEXT_145 = "_";
  protected final String TEXT_146 = ".setInputStream(new java.io.ByteArrayInputStream((byte[])";
  protected final String TEXT_147 = ".";
  protected final String TEXT_148 = "));" + NL + "        connectionSource_";
  protected final String TEXT_149 = "_";
  protected final String TEXT_150 = " = ss_";
  protected final String TEXT_151 = "_";
  protected final String TEXT_152 = ";" + NL + "    } else if (";
  protected final String TEXT_153 = ".";
  protected final String TEXT_154 = " instanceof routines.system.Document) {" + NL + "        org.dom4j.io.DocumentSource ds_";
  protected final String TEXT_155 = "_";
  protected final String TEXT_156 = " = new org.dom4j.io.DocumentSource(((routines.system.Document)";
  protected final String TEXT_157 = ".";
  protected final String TEXT_158 = ").getDocument());" + NL + "        connectionSource_";
  protected final String TEXT_159 = "_";
  protected final String TEXT_160 = " =ds_";
  protected final String TEXT_161 = "_";
  protected final String TEXT_162 = ";" + NL + "    }";
  protected final String TEXT_163 = NL + "    connectionSource_";
  protected final String TEXT_164 = "_";
  protected final String TEXT_165 = " = new org.talend.transform.io.ObjectSource(list_";
  protected final String TEXT_166 = ");";
  protected final String TEXT_167 = NL + "   java.util.Map<";
  protected final String TEXT_168 = "Struct, ";
  protected final String TEXT_169 = "Struct> tHash_";
  protected final String TEXT_170 = " = (java.util.Map<";
  protected final String TEXT_171 = "Struct, ";
  protected final String TEXT_172 = "Struct>)globalMap.get(\"tHash_";
  protected final String TEXT_173 = "\");" + NL + "   java.util.List<java.util.Map<String, Object>> list_";
  protected final String TEXT_174 = "_";
  protected final String TEXT_175 = " = new java.util.ArrayList<>();" + NL + "   for (";
  protected final String TEXT_176 = "Struct connStruct : tHash_";
  protected final String TEXT_177 = ".values()) {";
  protected final String TEXT_178 = NL + "     ";
  protected final String TEXT_179 = "Struct ";
  protected final String TEXT_180 = " = connStruct;";
  protected final String TEXT_181 = NL + "    java.util.Map<String, Object> map_";
  protected final String TEXT_182 = "_";
  protected final String TEXT_183 = " = new java.util.HashMap<>();";
  protected final String TEXT_184 = NL + "    map_";
  protected final String TEXT_185 = "_";
  protected final String TEXT_186 = ".put(\"";
  protected final String TEXT_187 = "\", ";
  protected final String TEXT_188 = ".";
  protected final String TEXT_189 = ");";
  protected final String TEXT_190 = NL + "    list_";
  protected final String TEXT_191 = "_";
  protected final String TEXT_192 = ".add(map_";
  protected final String TEXT_193 = "_";
  protected final String TEXT_194 = ");" + NL + "   }" + NL + "   connectionSource_";
  protected final String TEXT_195 = "_";
  protected final String TEXT_196 = " = new org.talend.transform.io.ObjectSource(list_";
  protected final String TEXT_197 = "_";
  protected final String TEXT_198 = ");";
  protected final String TEXT_199 = NL + "    sources_";
  protected final String TEXT_200 = ".put(\"";
  protected final String TEXT_201 = "\", connectionSource_";
  protected final String TEXT_202 = "_";
  protected final String TEXT_203 = ");";
  protected final String TEXT_204 = NL + "   //Setting a single connection of payload-type" + NL + "    mapExec_";
  protected final String TEXT_205 = ".setSource(\"";
  protected final String TEXT_206 = "\", connectionSource_";
  protected final String TEXT_207 = "_";
  protected final String TEXT_208 = ");";
  protected final String TEXT_209 = NL + "   //Setting multiple sources" + NL + "    mapExec_";
  protected final String TEXT_210 = ".setSources(\"";
  protected final String TEXT_211 = "\", sources_";
  protected final String TEXT_212 = ");";
  protected final String TEXT_213 = NL;
  protected final String TEXT_214 = NL;
  protected final String TEXT_215 = "    " + NL + "    java.util.Map<String, javax.xml.transform.Result> output_";
  protected final String TEXT_216 = " = new java.util.HashMap<>();";
  protected final String TEXT_217 = "  " + NL + "    javax.xml.transform.Result output_";
  protected final String TEXT_218 = " = null;";
  protected final String TEXT_219 = "  " + NL + "   //Result for thMap Connection \"";
  protected final String TEXT_220 = "\" : ObjectResult" + NL + "\t java.util.List<java.util.Map<String, Object>> outList_";
  protected final String TEXT_221 = "_";
  protected final String TEXT_222 = " = new java.util.ArrayList<>();" + NL + "\t org.talend.transform.io.ObjectResult result_";
  protected final String TEXT_223 = "_";
  protected final String TEXT_224 = " = new org.talend.transform.io.ObjectResult(outList_";
  protected final String TEXT_225 = "_";
  protected final String TEXT_226 = ");";
  protected final String TEXT_227 = NL + "\t//Result is of single column string" + NL + "    javax.xml.transform.stream.StreamResult result_";
  protected final String TEXT_228 = "_";
  protected final String TEXT_229 = " = new javax.xml.transform.stream.StreamResult();" + NL + "    java.io.StringWriter sw_";
  protected final String TEXT_230 = "_";
  protected final String TEXT_231 = " = new java.io.StringWriter();" + NL + "    ((javax.xml.transform.stream.StreamResult)result_";
  protected final String TEXT_232 = "_";
  protected final String TEXT_233 = ").setWriter(sw_";
  protected final String TEXT_234 = "_";
  protected final String TEXT_235 = ");";
  protected final String TEXT_236 = NL + "\t//Result is of single column byte array" + NL + "    javax.xml.transform.stream.StreamResult result_";
  protected final String TEXT_237 = "_";
  protected final String TEXT_238 = " = new javax.xml.transform.stream.StreamResult();" + NL + "    java.io.ByteArrayOutputStream bas_";
  protected final String TEXT_239 = "_";
  protected final String TEXT_240 = " = new java.io.ByteArrayOutputStream();" + NL + "    ((javax.xml.transform.stream.StreamResult)result_";
  protected final String TEXT_241 = "_";
  protected final String TEXT_242 = ").setOutputStream(bas_";
  protected final String TEXT_243 = "_";
  protected final String TEXT_244 = ");    ";
  protected final String TEXT_245 = NL + "    //Result is of single column document" + NL + "    org.dom4j.io.DocumentResult result_";
  protected final String TEXT_246 = "_";
  protected final String TEXT_247 = " = new org.dom4j.io.DocumentResult();";
  protected final String TEXT_248 = NL + "   //Add to map (";
  protected final String TEXT_249 = " record_path, result) " + NL + "\toutput_";
  protected final String TEXT_250 = ".put(\"";
  protected final String TEXT_251 = "\",result_";
  protected final String TEXT_252 = "_";
  protected final String TEXT_253 = ");";
  protected final String TEXT_254 = NL + "   //Set the single result" + NL + "   output_";
  protected final String TEXT_255 = " = result_";
  protected final String TEXT_256 = "_";
  protected final String TEXT_257 = ";" + NL + "   mapExec_";
  protected final String TEXT_258 = ".setResult(\"";
  protected final String TEXT_259 = "\",output_";
  protected final String TEXT_260 = ");";
  protected final String TEXT_261 = "  " + NL + "   //Setting multiple/wrapper results" + NL + "\tmapExec_";
  protected final String TEXT_262 = ".setResults(\"";
  protected final String TEXT_263 = "\", output_";
  protected final String TEXT_264 = "); ";
  protected final String TEXT_265 = NL + "    ";
  protected final String TEXT_266 = ".this.globalMap.put(Thread.currentThread().getId()+\"_";
  protected final String TEXT_267 = "_\"+\"outputResult\", output_";
  protected final String TEXT_268 = ");";
  protected final String TEXT_269 = NL;
  protected final String TEXT_270 = NL + "    // Runs the map when the InputStream is accessed";
  protected final String TEXT_271 = NL + "    ";
  protected final String TEXT_272 = ".this.globalMap.put(Thread" + NL + "            .currentThread().getId()+\"_";
  protected final String TEXT_273 = "_\"+\"outputResult\", mapExec_";
  protected final String TEXT_274 = ".executeToStream(\"";
  protected final String TEXT_275 = "\"));";
  protected final String TEXT_276 = NL + "    org.talend.transform.runtime.common.MapExecutionStatus results_";
  protected final String TEXT_277 = " = mapExec_";
  protected final String TEXT_278 = ".execute(\"";
  protected final String TEXT_279 = "\");" + NL + "    mapExec_";
  protected final String TEXT_280 = ".freeExecutionResources(\"";
  protected final String TEXT_281 = "\");";
  protected final String TEXT_282 = NL + "    ";
  protected final String TEXT_283 = ".this.globalMap.put(\"";
  protected final String TEXT_284 = "_\"+\"EXECUTION_STATUS\",results_";
  protected final String TEXT_285 = ");";
  protected final String TEXT_286 = NL + "    ";
  protected final String TEXT_287 = ".this.globalMap.put(\"";
  protected final String TEXT_288 = "_\"+\"EXECUTION_SEVERITY\",results_";
  protected final String TEXT_289 = ".getHighestSeverityLevel().getNumValue());" + NL + "    if (results_";
  protected final String TEXT_290 = ".getHighestSeverityLevel().isGreaterOrEqualsTo(";
  protected final String TEXT_291 = ")) {" + NL + "        throw new TalendException(new java.lang.Exception(String.valueOf(results_";
  protected final String TEXT_292 = ")),currentComponent,globalMap);" + NL + "    }";
  protected final String TEXT_293 = NL + NL + "    if (results_";
  protected final String TEXT_294 = ".getHighestSeverityLevel().isGreaterThan(org.talend.transform.runtime.common.SeverityLevel.INFO)) {" + NL + "        System.err.println(results_";
  protected final String TEXT_295 = ");" + NL + "    }";

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    
    //THMAPIN_BEGIN
    CodeGeneratorArgument codeGenArgument = (CodeGeneratorArgument) argument;
    INode node = (INode)codeGenArgument.getArgument();
    String processName = org.talend.core.model.utils.JavaResourcesHelper.getSimpleClassName(node.getProcess());
    String this_cid = ElementParameterParser.getValue(node, "__UNIQUE_NAME__");
    String tHMap_id = this_cid.replace("_THMAP_IN", "");
    String cid = tHMap_id;
  
    // To not conflict with the included asMap in _runmap below
    boolean asMap_ = "true".equals(ElementParameterParser.getValue(node, "__AS_MAP__"));
    boolean sourceAsMap = "true".equals(ElementParameterParser.getValue(node, "__SOURCE_AS_MAP__"));
    
    //Filter out non FLOW_MAIN/ROUTE type connections
    INode tHMapNode = MapperComponent.getThMapNode(node);    

    stringBuffer.append(TEXT_1);
    stringBuffer.append(this_cid);
    
	if (node.isDesignSubjobStartNode()) {
	// if the thMapIn virtual component is the first node, it means that thMapOut virtual component is not activated.
	// In this case thMapIn is responsible for initializing nb_line counter.

    stringBuffer.append(TEXT_2);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_3);
    
	} // node.isDesignSubjobStartNode()

    
    if (sourceAsMap) {

    
    // Fills the dependency property of the component
    org.talend.transform.components.ExportDependencyProvider edp = new org.talend.transform.components.ExportDependencyProvider();
    edp.createTDMArchives(tHMapNode);
    MapperComponent mapperComponent = ((MapperComponent)tHMapNode.getExternalNode());

    String mapPath = ElementParameterParser.getValue(node, "__HMAP_PATH__");
    String mapVarPath = ElementParameterParser.getValue(node, "__HMAP_VAR_PATH__");

    org.talend.transform.component.thmap.MapInfo mapInfo = mapperComponent.getMapInfo(mapPath, mapVarPath);
    String mainProjectName = mapInfo.getMainProjectName();
    String relativeMapPath = mapInfo.getRelativeMapPath();

    String log = ElementParameterParser.getValue(node, "__LOG__");

    boolean asInputstream = "true".equals(ElementParameterParser.getValue(node, "__AS_INPUTSTREAM__"));

    String devWorkspace = ElementParameterParser.getValue(node.getProcess(), "__COMP_DEFAULT_FILE_DIR__");
    devWorkspace = TalendTextUtils.addQuotes(devWorkspace);
    String devInstall = ElementParameterParser.getValue(node.getProcess(), "__PRODUCT_ROOT_DIR__");
    devInstall = TalendTextUtils.addQuotes(devInstall);

    Integer exceptionLevel = Integer.parseInt(ElementParameterParser.getValue(node, "__EXCEPTION__"));

    //Use the main job's node to find the incoming connections that have the declared <connection>Struct classes
    java.util.List<? extends IConnection> mainInConnections = ((MapperComponent)node.getExternalNode()).getNonVirtualMainIncomingConnections();

    stringBuffer.append(TEXT_4);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_5);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_6);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_7);
    stringBuffer.append(processName);
    stringBuffer.append(TEXT_8);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_9);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_10);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_11);
    stringBuffer.append(TEXT_12);
    stringBuffer.append(processName);
    stringBuffer.append(TEXT_13);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_14);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_15);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_16);
    stringBuffer.append(log);
    stringBuffer.append(TEXT_17);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_18);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_19);
    
    String localContext = "context";

    stringBuffer.append(TEXT_20);
    stringBuffer.append(localContext);
    stringBuffer.append(TEXT_21);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_22);
    stringBuffer.append(localContext);
    stringBuffer.append(TEXT_23);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_24);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_25);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_26);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_27);
    stringBuffer.append(localContext);
    stringBuffer.append(TEXT_28);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_29);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_30);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_31);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_32);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_33);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_34);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_35);
    
	String mapVarPathWithoutQuotes = TalendTextUtils.removeQuotes(mapVarPath);
    if (mapVarPathWithoutQuotes.startsWith("context.")) {
        String contextMapVar = mapVarPathWithoutQuotes.substring(mapVarPathWithoutQuotes.indexOf(".") + 1);

    stringBuffer.append(TEXT_36);
    stringBuffer.append(contextMapVar);
    stringBuffer.append(TEXT_37);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_38);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_39);
    stringBuffer.append(mainProjectName);
    stringBuffer.append(TEXT_40);
    
    } else {

    stringBuffer.append(TEXT_41);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_42);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_43);
    stringBuffer.append(mainProjectName);
    stringBuffer.append(TEXT_44);
    stringBuffer.append(relativeMapPath);
    stringBuffer.append(TEXT_45);
    
  }  

    
    if (mainInConnections!=null && mainInConnections.size() >= 1) {
      boolean requiresMultipleSources = mapperComponent.requiresMultipleSources();

    stringBuffer.append(TEXT_46);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_47);
    
      for (IConnection incomingConn : mainInConnections) {
        //In the event that the connection name in the main job and joblet are not the same, this is the connection which has a struct declaration
        String mainConnectionName = incomingConn.getUniqueName();
        //Find the corresponding connection if the thMap is in a joblet
        IConnection thMapInConnection = org.talend.transform.components.utils.ComponentUtils.findInConnectionFromTransformNode(tHMapNode, node, incomingConn); //Use this to find the record for the mapExecutor
        String recordName = thMapInConnection.getUniqueName();
        if (mapperComponent.isSingleSchemaTypeInputConnection(thMapInConnection)) {
          //Single connection schema-type; non-wrapped

    stringBuffer.append(TEXT_48);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_49);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_50);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_51);
    
        } else { //Create the Source accordingly

    
   //Create the corresponding source for the connection

    stringBuffer.append(TEXT_52);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_53);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_54);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_55);
               if (!mapperComponent.isIncomingHashMap(thMapInConnection)) {
                 if (!incomingConn.getLineStyle().hasConnectionCategory(IConnectionCategory.MAIN)) {

    stringBuffer.append(TEXT_56);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_57);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_58);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_59);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_60);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_61);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_62);
    stringBuffer.append(TEXT_63);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_64);
    stringBuffer.append(incomingConn.getName());
    stringBuffer.append(TEXT_65);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_66);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_67);
    stringBuffer.append(TEXT_68);
    stringBuffer.append(incomingConn.getName());
    stringBuffer.append(TEXT_69);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_70);
    
                 } //End for Lookup Connection
                 IMetadataColumn inputCol = incomingConn.getMetadataTable().getListColumns().get(0);
                 if (JavaTypesManager.STRING.getId().equals(inputCol.getTalendType())) {

    stringBuffer.append(TEXT_71);
    stringBuffer.append(node.getUniqueName());
    stringBuffer.append(TEXT_72);
    stringBuffer.append(incomingConn.getName() );
    stringBuffer.append(TEXT_73);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_74);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_75);
    stringBuffer.append(incomingConn.getName() );
    stringBuffer.append(TEXT_76);
    stringBuffer.append(incomingConn.getName() );
    stringBuffer.append(TEXT_77);
    stringBuffer.append(inputCol.getLabel() );
    stringBuffer.append(TEXT_78);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_79);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_80);
    stringBuffer.append(incomingConn.getName() );
    stringBuffer.append(TEXT_81);
    stringBuffer.append(inputCol.getLabel() );
    stringBuffer.append(TEXT_82);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_83);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_84);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_85);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_86);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_87);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_88);
    
                 } else if ("id_Document".equals(inputCol.getTalendType())) {

    stringBuffer.append(TEXT_89);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_90);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_91);
    stringBuffer.append(incomingConn.getName() );
    stringBuffer.append(TEXT_92);
    stringBuffer.append(incomingConn.getName() );
    stringBuffer.append(TEXT_93);
    stringBuffer.append(inputCol.getLabel() );
    stringBuffer.append(TEXT_94);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_95);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_96);
    stringBuffer.append(incomingConn.getName() );
    stringBuffer.append(TEXT_97);
    stringBuffer.append(inputCol.getLabel() );
    stringBuffer.append(TEXT_98);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_99);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_100);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_101);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_102);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_103);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_104);
    
                 } else if ("id_byte[]".equals(inputCol.getTalendType())) {

    stringBuffer.append(TEXT_105);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_106);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_107);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_108);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_109);
    stringBuffer.append(incomingConn.getName());
    stringBuffer.append(TEXT_110);
    stringBuffer.append(inputCol.getLabel());
    stringBuffer.append(TEXT_111);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_112);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_113);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_114);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_115);
    
                 } else {

    stringBuffer.append(TEXT_116);
    stringBuffer.append(incomingConn.getName() );
    stringBuffer.append(TEXT_117);
    stringBuffer.append(inputCol.getLabel() );
    stringBuffer.append(TEXT_118);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_119);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_120);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_121);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_122);
    stringBuffer.append(incomingConn.getName() );
    stringBuffer.append(TEXT_123);
    stringBuffer.append(inputCol.getLabel() );
    stringBuffer.append(TEXT_124);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_125);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_126);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_127);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_128);
    stringBuffer.append(incomingConn.getName() );
    stringBuffer.append(TEXT_129);
    stringBuffer.append(inputCol.getLabel() );
    stringBuffer.append(TEXT_130);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_131);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_132);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_133);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_134);
    stringBuffer.append(incomingConn.getName() );
    stringBuffer.append(TEXT_135);
    stringBuffer.append(inputCol.getLabel() );
    stringBuffer.append(TEXT_136);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_137);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_138);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_139);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_140);
    stringBuffer.append(incomingConn.getName() );
    stringBuffer.append(TEXT_141);
    stringBuffer.append(inputCol.getLabel() );
    stringBuffer.append(TEXT_142);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_143);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_144);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_145);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_146);
    stringBuffer.append(incomingConn.getName() );
    stringBuffer.append(TEXT_147);
    stringBuffer.append(inputCol.getLabel() );
    stringBuffer.append(TEXT_148);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_149);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_150);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_151);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_152);
    stringBuffer.append(incomingConn.getName() );
    stringBuffer.append(TEXT_153);
    stringBuffer.append(inputCol.getLabel() );
    stringBuffer.append(TEXT_154);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_155);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_156);
    stringBuffer.append(incomingConn.getName() );
    stringBuffer.append(TEXT_157);
    stringBuffer.append(inputCol.getLabel() );
    stringBuffer.append(TEXT_158);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_159);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_160);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_161);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_162);
    
                 }//End if-else types
               } else { //End if !isIncomingHashMap
                   if (incomingConn.getLineStyle().hasConnectionCategory(IConnectionCategory.MAIN)) {
                      //The source data is read first(for virtual components)

    stringBuffer.append(TEXT_163);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_164);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_165);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_166);
                     } else if (incomingConn.getLineStyle().hasConnectionCategory(IConnectionCategory.DATA)) {

    stringBuffer.append(TEXT_167);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_168);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_169);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_170);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_171);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_172);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_173);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_174);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_175);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_176);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_177);
    stringBuffer.append(TEXT_178);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_179);
    stringBuffer.append(incomingConn.getName());
    stringBuffer.append(TEXT_180);
    
    // Populate a TDM HashMap using a DI rowStruct
    String incomingConnName = incomingConn.getUniqueName();

    stringBuffer.append(TEXT_181);
    stringBuffer.append(incomingConnName);
    stringBuffer.append(TEXT_182);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_183);
    
                IMetadataTable inputMetadataTable = incomingConn.getMetadataTable();
                for (IMetadataColumn inputCol : inputMetadataTable.getListColumns()) {

    stringBuffer.append(TEXT_184);
    stringBuffer.append(incomingConnName);
    stringBuffer.append(TEXT_185);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_186);
    stringBuffer.append(inputCol.getLabel() );
    stringBuffer.append(TEXT_187);
    stringBuffer.append(incomingConn.getName());
    stringBuffer.append(TEXT_188);
    stringBuffer.append(inputCol.getLabel() );
    stringBuffer.append(TEXT_189);
    
                } // for (IMetadataColumn

    stringBuffer.append(TEXT_190);
    stringBuffer.append(incomingConnName);
    stringBuffer.append(TEXT_191);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_192);
    stringBuffer.append(incomingConnName);
    stringBuffer.append(TEXT_193);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_194);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_195);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_196);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_197);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_198);
    
                   } //End if MAIN or DATA
               } //End else metadataColumns.size()>1

    
          if (requiresMultipleSources) { //MultiSource structure
              String recordPath = mapperComponent.findInputConnectionRecordPath(thMapInConnection);

    stringBuffer.append(TEXT_199);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_200);
    stringBuffer.append(recordPath);
    stringBuffer.append(TEXT_201);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_202);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_203);
    
          } else { //Single-connection / payloadtype

    stringBuffer.append(TEXT_204);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_205);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_206);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_207);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_208);
    
          } //End if Multirep structure
        } //End else if isSingleSchemaTypeInputConnection
      } //End forIConnection
      if (requiresMultipleSources) {

    stringBuffer.append(TEXT_209);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_210);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_211);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_212);
    
      }
    } //End mainInConnections!=null

    stringBuffer.append(TEXT_213);
    stringBuffer.append(TEXT_214);
    
	//Note: Create the result using the logic here if it is not of type inputStream
    boolean asInputstream_ = "true".equals(ElementParameterParser.getValue(node, "__AS_INPUTSTREAM__"));
    java.util.List<? extends IConnection> mainOutConnections_setResults = ((MapperComponent)node.getExternalNode()).getNonVirtualMainOutgoingConnections();
    boolean requires_setResults = mapperComponent.requiresMultipleResults();    
    boolean requiresMultipleResults = mapperComponent.requiresMultipleResults();

      
    if (requires_setResults) {  //The output is a wrapper structure

    stringBuffer.append(TEXT_215);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_216);
      
	 } else if (!asInputstream_ && mainOutConnections_setResults.size()==1){ //single connection

    stringBuffer.append(TEXT_217);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_218);
    	
    } //end if-requires_setResults
    for (IConnection outConnection : mainOutConnections_setResults) {
        //In the event that the connection name in the main job and joblet are not the same, this is the connection which has a struct declaration
        String mainConnectionName = outConnection.getUniqueName();
        //Find the corresponding connection if the thMap is in a joblet
        IConnection thMapOutConnection = org.talend.transform.components.utils.ComponentUtils.findOutConnectionFromTransformNode(tHMapNode, node, outConnection); //Use this to find the record for the mapExecutor
        if (!asInputstream_) { //Create the Result accordingly
	         String defaultDataType = mapperComponent.getOutputConnectionDataType(outConnection);
	         if ("MAP".equals(defaultDataType)) { //Schema-type connection 

    stringBuffer.append(TEXT_219);
    stringBuffer.append(mainConnectionName);
    stringBuffer.append(TEXT_220);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_221);
    stringBuffer.append(outConnection.getName());
    stringBuffer.append(TEXT_222);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_223);
    stringBuffer.append(outConnection.getName());
    stringBuffer.append(TEXT_224);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_225);
    stringBuffer.append(outConnection.getName());
    stringBuffer.append(TEXT_226);
        
            } else if ((JavaTypesManager.STRING.getId().equals(defaultDataType))) {

    stringBuffer.append(TEXT_227);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_228);
    stringBuffer.append(outConnection.getName());
    stringBuffer.append(TEXT_229);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_230);
    stringBuffer.append(outConnection.getName());
    stringBuffer.append(TEXT_231);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_232);
    stringBuffer.append(outConnection.getName());
    stringBuffer.append(TEXT_233);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_234);
    stringBuffer.append(outConnection.getName());
    stringBuffer.append(TEXT_235);
    
            } else if ("id_byte[]".equals(defaultDataType)) {

    stringBuffer.append(TEXT_236);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_237);
    stringBuffer.append(outConnection.getName());
    stringBuffer.append(TEXT_238);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_239);
    stringBuffer.append(outConnection.getName());
    stringBuffer.append(TEXT_240);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_241);
    stringBuffer.append(outConnection.getName());
    stringBuffer.append(TEXT_242);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_243);
    stringBuffer.append(outConnection.getName());
    stringBuffer.append(TEXT_244);
    
            } else if ("id_Document".equals(defaultDataType)) {

    stringBuffer.append(TEXT_245);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_246);
    stringBuffer.append(outConnection.getName());
    stringBuffer.append(TEXT_247);
    
            } // end if-defaultDataType
            if (requires_setResults) {
               String recordPath = mapperComponent.findOutputConnectionRecordPath(thMapOutConnection); 

    stringBuffer.append(TEXT_248);
    stringBuffer.append(outConnection.getName());
    stringBuffer.append(TEXT_249);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_250);
    stringBuffer.append(recordPath);
    stringBuffer.append(TEXT_251);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_252);
    stringBuffer.append(outConnection.getName());
    stringBuffer.append(TEXT_253);
    	  
	         } else if (!asInputstream_ && mainOutConnections_setResults.size()==1){ //single connection only 

    stringBuffer.append(TEXT_254);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_255);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_256);
    stringBuffer.append(outConnection.getName());
    stringBuffer.append(TEXT_257);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_258);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_259);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_260);
    
            } //End if-requires_setResults

        
        } //end if !asInputstream_
    } //end for loop
    if (requires_setResults) { 

    stringBuffer.append(TEXT_261);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_262);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_263);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_264);
      
	} if (!asInputstream_ && mainOutConnections_setResults.size()>=1) {

    stringBuffer.append(TEXT_265);
    stringBuffer.append(processName);
    stringBuffer.append(TEXT_266);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_267);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_268);
      
    } //end if !asInputstream_

    stringBuffer.append(TEXT_269);
    
    if (asInputstream) {

    stringBuffer.append(TEXT_270);
    stringBuffer.append(TEXT_271);
    stringBuffer.append( processName );
    stringBuffer.append(TEXT_272);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_273);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_274);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_275);
    
    } else {

    stringBuffer.append(TEXT_276);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_277);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_278);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_279);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_280);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_281);
    stringBuffer.append(TEXT_282);
    stringBuffer.append( processName );
    stringBuffer.append(TEXT_283);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_284);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_285);
    stringBuffer.append(TEXT_286);
    stringBuffer.append( processName );
    stringBuffer.append(TEXT_287);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_288);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_289);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_290);
    stringBuffer.append(exceptionLevel);
    stringBuffer.append(TEXT_291);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_292);
    
        if(!org.talend.designer.runprocess.ProcessorUtilities.isExportConfig()){

    stringBuffer.append(TEXT_293);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_294);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_295);
    
        } // (!org.talend.designer.runprocess
    } // if (asInputstream)

    
    }

    return stringBuffer.toString();
  }
}
