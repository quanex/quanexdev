package org.talend.designer.codegen.translators.data_quality;

import org.talend.core.model.process.INode;
import org.talend.designer.codegen.config.CodeGeneratorArgument;
import org.talend.core.model.process.ElementParameterParser;

public class TJapaneseTokenizeBeginJava
{
  protected static String nl;
  public static synchronized TJapaneseTokenizeBeginJava create(String lineSeparator)
  {
    nl = lineSeparator;
    TJapaneseTokenizeBeginJava result = new TJapaneseTokenizeBeginJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "int nb_line_";
  protected final String TEXT_2 = " = 0;" + NL + "" + NL + "org.talend.dataquality.jp.tokenization.TextTokenizer textTokenizer_";
  protected final String TEXT_3 = " = " + NL + "\torg.talend.dataquality.jp.tokenization.TextTokenizer.getInstance();";

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    
	CodeGeneratorArgument codeGenArgument = (CodeGeneratorArgument) argument;
	INode node = (INode)codeGenArgument.getArgument();
	String cid = node.getUniqueName();

    stringBuffer.append(TEXT_1);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_2);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_3);
    return stringBuffer.toString();
  }
}
