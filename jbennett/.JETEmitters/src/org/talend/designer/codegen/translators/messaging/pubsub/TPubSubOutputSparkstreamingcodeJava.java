package org.talend.designer.codegen.translators.messaging.pubsub;

import java.util.List;
import org.talend.core.model.metadata.IMetadataColumn;
import org.talend.core.model.metadata.IMetadataTable;
import org.talend.core.model.metadata.types.JavaType;
import org.talend.core.model.metadata.types.JavaType;
import org.talend.core.model.metadata.types.JavaTypesManager;
import org.talend.core.model.metadata.types.JavaTypesManager;
import org.talend.core.model.process.EConnectionType;
import org.talend.core.model.process.ElementParameterParser;
import org.talend.core.model.process.IConnection;
import org.talend.core.model.process.IConnectionCategory;
import org.talend.core.model.process.INode;
import org.talend.designer.common.BigDataCodeGeneratorArgument;
import org.talend.designer.common.tgooglecloudconfiguration.TGoogleCloudConfigurationUtil;
import org.talend.designer.spark.generator.storage.PubSubSparkStorage;

public class TPubSubOutputSparkstreamingcodeJava
{
  protected static String nl;
  public static synchronized TPubSubOutputSparkstreamingcodeJava create(String lineSeparator)
  {
    nl = lineSeparator;
    TPubSubOutputSparkstreamingcodeJava result = new TPubSubOutputSparkstreamingcodeJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "";
  protected final String TEXT_2 = NL + NL + "public static class ";
  protected final String TEXT_3 = " {" + NL + "\tprivate static volatile org.apache.commons.pool2.ObjectPool<";
  protected final String TEXT_4 = "> pool;" + NL + "\t" + NL + "\tprivate ";
  protected final String TEXT_5 = "() {" + NL + "\t    // empty" + NL + "\t}" + NL + "" + NL + "\tpublic static org.apache.commons.pool2.ObjectPool<";
  protected final String TEXT_6 = "> get(ContextProperties context) {" + NL + "\t\tif(pool == null) {" + NL + "\t\t\tsynchronized(";
  protected final String TEXT_7 = ".class) {" + NL + "\t\t\t\tif(pool == null) {" + NL + "\t\t\t\t\tpool = createPool(context);" + NL + "\t\t\t\t}" + NL + "\t\t\t}" + NL + "\t\t}" + NL + "\t   return pool;" + NL + "\t}" + NL + "" + NL + "\tprivate static org.apache.commons.pool2.ObjectPool<";
  protected final String TEXT_8 = "> createPool(ContextProperties context) {" + NL + "\t    org.apache.commons.pool2.impl.GenericObjectPoolConfig config = new org.apache.commons.pool2.impl.GenericObjectPoolConfig();" + NL + "\t    config.setMaxTotal(";
  protected final String TEXT_9 = ");" + NL + "\t    config.setMaxWaitMillis(";
  protected final String TEXT_10 = ");" + NL + "\t    config.setMinIdle(";
  protected final String TEXT_11 = ");" + NL + "\t    config.setMaxIdle(";
  protected final String TEXT_12 = ");" + NL + "\t    config.setBlockWhenExhausted(true);";
  protected final String TEXT_13 = NL + "\t\t\tconfig.setTimeBetweenEvictionRunsMillis(";
  protected final String TEXT_14 = ");" + NL + "\t\t\tconfig.setMinEvictableIdleTimeMillis(";
  protected final String TEXT_15 = ");" + NL + "\t\t\tconfig.setSoftMinEvictableIdleTimeMillis(";
  protected final String TEXT_16 = ");";
  protected final String TEXT_17 = NL + "\t    return new org.apache.commons.pool2.impl.GenericObjectPool<";
  protected final String TEXT_18 = ">(new ";
  protected final String TEXT_19 = "(context), config);" + NL + "\t}" + NL + "}" + NL + "" + NL + "public static class ";
  protected final String TEXT_20 = " extends org.apache.commons.pool2.BasePooledObjectFactory<";
  protected final String TEXT_21 = "> {" + NL + "" + NL + "    private ContextProperties context;" + NL + "" + NL + "    public ";
  protected final String TEXT_22 = "(ContextProperties context) {" + NL + "        this.context = context;" + NL + "    }" + NL + "" + NL + "    @Override" + NL + "    public ";
  protected final String TEXT_23 = " create() throws java.lang.Exception {" + NL + "        return ";
  protected final String TEXT_24 = "_ForeachPartition.createClient(context);" + NL + "    }" + NL + "" + NL + "    @Override" + NL + "    public org.apache.commons.pool2.PooledObject<";
  protected final String TEXT_25 = "> wrap(";
  protected final String TEXT_26 = " publisher) {" + NL + "        return new org.apache.commons.pool2.impl.DefaultPooledObject<";
  protected final String TEXT_27 = ">(publisher);" + NL + "    }" + NL + "}" + NL + "" + NL + "public static class ";
  protected final String TEXT_28 = "_ForeachRDD<KEY> implements ";
  protected final String TEXT_29 = " {" + NL + "" + NL + "    private ContextProperties context;" + NL + "" + NL + "    public ";
  protected final String TEXT_30 = "_ForeachRDD(JobConf job){" + NL + "        this.context = new ContextProperties(job);" + NL + "    }" + NL + "" + NL + "    public ";
  protected final String TEXT_31 = " call(org.apache.spark.api.java.JavaPairRDD<KEY,";
  protected final String TEXT_32 = "> rdd) throws java.lang.Exception {" + NL + "        rdd.foreachPartition(new ";
  protected final String TEXT_33 = "_ForeachPartition<KEY>(context));";
  protected final String TEXT_34 = NL + "        ";
  protected final String TEXT_35 = NL + "    }" + NL + "}" + NL + "" + NL + "public static class ";
  protected final String TEXT_36 = "_ForeachPartition<KEY> implements org.apache.spark.api.java.function.VoidFunction<java.util.Iterator<scala.Tuple2<KEY,";
  protected final String TEXT_37 = ">>> {" + NL + "" + NL + "    private ContextProperties context;" + NL + "" + NL + "    private final String topic;" + NL + "" + NL + "    public ";
  protected final String TEXT_38 = "_ForeachPartition(ContextProperties context) {" + NL + "        this.context = context;" + NL + "        this.topic = \"projects/\" + ";
  protected final String TEXT_39 = " + \"/topics/\" + ";
  protected final String TEXT_40 = ";" + NL + "    }" + NL + "" + NL + "    private static com.google.api.client.auth.oauth2.Credential createCredential(ContextProperties context) {" + NL + "        org.apache.spark.streaming.pubsub.SparkGCPCredentials.Builder builder =" + NL + "                new org.apache.spark.streaming.pubsub.SparkGCPCredentials.Builder();" + NL;
  protected final String TEXT_41 = NL + "            builder = builder.p12ServiceAccount(";
  protected final String TEXT_42 = ",";
  protected final String TEXT_43 = NL + "                    ";
  protected final String TEXT_44 = ");";
  protected final String TEXT_45 = NL + "            builder = builder.jsonServiceAccount(";
  protected final String TEXT_46 = ");";
  protected final String TEXT_47 = NL + NL + "        return builder.build().provider();" + NL + "    }" + NL + "" + NL + "    private static com.google.api.services.pubsub.Pubsub createClient(ContextProperties context)" + NL + "            throws java.security.GeneralSecurityException, java.io.IOException {" + NL + "        return new com.google.api.services.pubsub.Pubsub.Builder(" + NL + "                com.google.api.client.googleapis.javanet.GoogleNetHttpTransport.newTrustedTransport()," + NL + "                com.google.api.client.json.jackson2.JacksonFactory.getDefaultInstance()," + NL + "                new com.google.cloud.hadoop.util.RetryHttpInitializer(createCredential(context), jobName)" + NL + "                ).setApplicationName(jobName).build();" + NL + "    }" + NL;
  protected final String TEXT_48 = NL + "    public static void createIfNotExistsTopic(ContextProperties context)" + NL + "            throws java.security.GeneralSecurityException, java.io.IOException {" + NL + "        String topic = \"projects/\" + ";
  protected final String TEXT_49 = " + \"/topics/\" + ";
  protected final String TEXT_50 = ";" + NL + "        com.google.api.services.pubsub.Pubsub client = createClient(context);" + NL + "        try {" + NL + "            client.projects().topics().create(topic," + NL + "                        new com.google.api.services.pubsub.model.Topic().setName(topic)" + NL + "                    ).execute();" + NL + "        } catch (com.google.api.client.googleapis.json.GoogleJsonResponseException e) {" + NL + "            // 409 indicates that the topic already exists (which can be ignored)." + NL + "            // Any other error code means the topic creation failed and is unrecoverable." + NL + "            if (e.getDetails().getCode() != 409) {" + NL + "              throw new IOException(e);" + NL + "            }" + NL + "        }" + NL + "    }";
  protected final String TEXT_51 = NL + NL + "    public void call(java.util.Iterator<scala.Tuple2<KEY,";
  protected final String TEXT_52 = ">> iterator) throws java.lang.Exception {";
  protected final String TEXT_53 = NL + "        ";
  protected final String TEXT_54 = " client = ";
  protected final String TEXT_55 = ".get(context).borrowObject();" + NL + "" + NL + "        scala.Tuple2<KEY,";
  protected final String TEXT_56 = "> tuple;" + NL + "        try {" + NL + "            java.util.List<com.google.api.services.pubsub.model.PubsubMessage> messages = new java.util.ArrayList<>();" + NL + "            while (iterator.hasNext()) {" + NL + "                tuple = iterator.next();" + NL + "                if (tuple._2() != null && tuple._2().";
  protected final String TEXT_57 = " != null) {";
  protected final String TEXT_58 = NL + "                        messages.add(new com.google.api.services.pubsub.model.PubsubMessage()" + NL + "                                .encodeData(tuple._2().";
  protected final String TEXT_59 = ".array()));";
  protected final String TEXT_60 = NL + "                        messages.add(new com.google.api.services.pubsub.model.PubsubMessage()" + NL + "                                .encodeData(tuple._2().";
  protected final String TEXT_61 = ".getBytes(\"UTF-8\")));";
  protected final String TEXT_62 = NL + "                        throw new throw new JobConfigurationError(\"Unsupported first column type: ";
  protected final String TEXT_63 = "\");";
  protected final String TEXT_64 = NL + "                }" + NL + "            }" + NL + "            com.google.api.services.pubsub.model.PublishRequest pr =" + NL + "                    new com.google.api.services.pubsub.model.PublishRequest();" + NL + "            pr.setMessages(messages);" + NL + "            client.projects().topics().publish(topic, pr).execute();" + NL + "        } finally {" + NL + "            if (client != null) {";
  protected final String TEXT_65 = NL + "                ";
  protected final String TEXT_66 = ".get(context).returnObject(client);" + NL + "            }" + NL + "        }" + NL + "    }" + NL + "}";
  protected final String TEXT_67 = NL;

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    
// Parse the inputs to this javajet generator.
final BigDataCodeGeneratorArgument codeGenArgument = (BigDataCodeGeneratorArgument) argument;
final INode node = (INode)codeGenArgument.getArgument();
final PubSubSparkStorage storage = new PubSubSparkStorage(node);
final TGoogleCloudConfigurationUtil gcpConfigUtil = new TGoogleCloudConfigurationUtil(node);
if (gcpConfigUtil.getValidateError() != null) {
    // Cause the job compilation to explicitly fail if there is a problem.
    return "throw new JobConfigurationError(\"" + gcpConfigUtil.getValidateError() +"\");";
}

final String cid = node.getUniqueName();

final String topicName = ElementParameterParser.getValue(node, "__PUBSUB_TOPIC__");
IConnection inConn = null;
if (node.getIncomingConnections() != null) {
    for (IConnection in : node.getIncomingConnections()) {
        if (in.getLineStyle().hasConnectionCategory(IConnectionCategory.DATA)) {
            inConn = in;
            break;
        }
    }
}
if (inConn == null) {
    return "";
}

final String inStructName = codeGenArgument.getRecordStructName(inConn);
IMetadataColumn inConnCol0 = inConn.getMetadataTable().getListColumns().get(0);

final String foreachClass;
final String foreachReturnClass;
final String foreachReturn;
if (org.talend.hadoop.distribution.ESparkVersion.SPARK_2_0.compareTo(codeGenArgument.getSparkVersion()) > 0) {
    foreachClass = "org.apache.spark.api.java.function.Function<org.apache.spark.api.java.JavaPairRDD<KEY, " + inStructName + ">, Void>";
    foreachReturnClass = "Void";
    foreachReturn = "return null;";
} else {
    foreachClass = "org.apache.spark.api.java.function.VoidFunction<org.apache.spark.api.java.JavaPairRDD<KEY," + inStructName + ">>";
    foreachReturnClass = "void";
    foreachReturn = "";
}


    stringBuffer.append(TEXT_1);
    
	// Connection pool code generation.
	
	// Warning : the object factory class has to be generated elsewhere, since the factory implementation depends of
	// the actual pooling object we want to store in this pool.
	
	// This file should be included in every single Spark and Spark Streaming component sparkcode.javajet
	// which requires to create connections to an external system without relying on a dedicated Spark connector. 

    stringBuffer.append(TEXT_2);
    stringBuffer.append(storage.getPoolClassName());
    stringBuffer.append(TEXT_3);
    stringBuffer.append(storage.getPooledObjectClassName());
    stringBuffer.append(TEXT_4);
    stringBuffer.append(storage.getPoolClassName());
    stringBuffer.append(TEXT_5);
    stringBuffer.append(storage.getPooledObjectClassName());
    stringBuffer.append(TEXT_6);
    stringBuffer.append(storage.getPoolClassName());
    stringBuffer.append(TEXT_7);
    stringBuffer.append(storage.getPooledObjectClassName());
    stringBuffer.append(TEXT_8);
    stringBuffer.append(storage.getPoolMaxTotal());
    stringBuffer.append(TEXT_9);
    stringBuffer.append(storage.getPoolMaxWait());
    stringBuffer.append(TEXT_10);
    stringBuffer.append(storage.getPoolMinIdle());
    stringBuffer.append(TEXT_11);
    stringBuffer.append(storage.getPoolMaxIdle());
    stringBuffer.append(TEXT_12);
    
		if(storage.isPoolUseEviction()) {

    stringBuffer.append(TEXT_13);
    stringBuffer.append(storage.getPoolTimeBetweenEviction());
    stringBuffer.append(TEXT_14);
    stringBuffer.append(storage.getPoolEvictionMinIdleTime());
    stringBuffer.append(TEXT_15);
    stringBuffer.append(storage.getPoolEvictionSoftMinIdleTime());
    stringBuffer.append(TEXT_16);
    
		}

    stringBuffer.append(TEXT_17);
    stringBuffer.append(storage.getPooledObjectClassName());
    stringBuffer.append(TEXT_18);
    stringBuffer.append(storage.getFactoryClassName());
    stringBuffer.append(TEXT_19);
    stringBuffer.append(storage.getFactoryClassName());
    stringBuffer.append(TEXT_20);
    stringBuffer.append(storage.getPooledObjectClassName());
    stringBuffer.append(TEXT_21);
    stringBuffer.append(storage.getFactoryClassName());
    stringBuffer.append(TEXT_22);
    stringBuffer.append(storage.getPooledObjectClassName());
    stringBuffer.append(TEXT_23);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_24);
    stringBuffer.append(storage.getPooledObjectClassName());
    stringBuffer.append(TEXT_25);
    stringBuffer.append(storage.getPooledObjectClassName());
    stringBuffer.append(TEXT_26);
    stringBuffer.append(storage.getPooledObjectClassName());
    stringBuffer.append(TEXT_27);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_28);
    stringBuffer.append(foreachClass);
    stringBuffer.append(TEXT_29);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_30);
    stringBuffer.append(foreachReturnClass);
    stringBuffer.append(TEXT_31);
    stringBuffer.append(inStructName);
    stringBuffer.append(TEXT_32);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_33);
    stringBuffer.append(TEXT_34);
    stringBuffer.append(foreachReturn);
    stringBuffer.append(TEXT_35);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_36);
    stringBuffer.append(inStructName);
    stringBuffer.append(TEXT_37);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_38);
    stringBuffer.append(gcpConfigUtil.getProjectName());
    stringBuffer.append(TEXT_39);
    stringBuffer.append(topicName);
    stringBuffer.append(TEXT_40);
    
        if (gcpConfigUtil.isSpecifyingCredentialsP12()) {
            
    stringBuffer.append(TEXT_41);
    stringBuffer.append(gcpConfigUtil.getGcpAuthenticationPath());
    stringBuffer.append(TEXT_42);
    stringBuffer.append(TEXT_43);
    stringBuffer.append(gcpConfigUtil.getGcpServiceAccountId());
    stringBuffer.append(TEXT_44);
    
        } else if (gcpConfigUtil.isSpecifyingCredentialsJson()) {
            
    stringBuffer.append(TEXT_45);
    stringBuffer.append(gcpConfigUtil.getGcpAuthenticationPath());
    stringBuffer.append(TEXT_46);
    
        }
        
    stringBuffer.append(TEXT_47);
    
if ("CREATE_IF_NOT_EXISTS".equals(ElementParameterParser.getValue(node, "__TOPIC_OPERATION__"))) {
    
    stringBuffer.append(TEXT_48);
    stringBuffer.append(gcpConfigUtil.getProjectName());
    stringBuffer.append(TEXT_49);
    stringBuffer.append(topicName);
    stringBuffer.append(TEXT_50);
    
}

    stringBuffer.append(TEXT_51);
    stringBuffer.append(inStructName);
    stringBuffer.append(TEXT_52);
    stringBuffer.append(TEXT_53);
    stringBuffer.append(storage.getPooledObjectClassName());
    stringBuffer.append(TEXT_54);
    stringBuffer.append(storage.getPoolClassName());
    stringBuffer.append(TEXT_55);
    stringBuffer.append(inStructName);
    stringBuffer.append(TEXT_56);
    stringBuffer.append(inConnCol0.getLabel());
    stringBuffer.append(TEXT_57);
    
                    JavaType javaType = JavaTypesManager.getJavaTypeFromId(inConnCol0.getTalendType());
                    if (javaType == JavaTypesManager.BYTE_ARRAY) {
                        
    stringBuffer.append(TEXT_58);
    stringBuffer.append(inConnCol0.getLabel());
    stringBuffer.append(TEXT_59);
    
                    } else if (javaType == JavaTypesManager.STRING) {
                        
    stringBuffer.append(TEXT_60);
    stringBuffer.append(inConnCol0.getLabel());
    stringBuffer.append(TEXT_61);
    
                    } else {
                        
    stringBuffer.append(TEXT_62);
    stringBuffer.append(inConnCol0.getTalendType());
    stringBuffer.append(TEXT_63);
    
                    }
                    
    stringBuffer.append(TEXT_64);
    stringBuffer.append(TEXT_65);
    stringBuffer.append(storage.getPoolClassName());
    stringBuffer.append(TEXT_66);
    stringBuffer.append(TEXT_67);
    return stringBuffer.toString();
  }
}
