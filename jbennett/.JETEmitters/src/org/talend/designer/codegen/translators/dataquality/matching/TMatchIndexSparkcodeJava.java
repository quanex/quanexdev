package org.talend.designer.codegen.translators.dataquality.matching;

import org.talend.core.model.process.INode;
import org.talend.core.model.process.IConnection;
import org.talend.core.model.metadata.IMetadataColumn;
import org.talend.designer.common.BigDataCodeGeneratorArgument;
import org.talend.designer.common.tsqlrow.TSqlRowUtil;

public class TMatchIndexSparkcodeJava
{
  protected static String nl;
  public static synchronized TMatchIndexSparkcodeJava create(String lineSeparator)
  {
    nl = lineSeparator;
    TMatchIndexSparkcodeJava result = new TMatchIndexSparkcodeJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = NL + "        public static class ";
  protected final String TEXT_2 = "_From";
  protected final String TEXT_3 = "To";
  protected final String TEXT_4 = " implements org.apache.spark.api.java.function.Function<";
  protected final String TEXT_5 = ", ";
  protected final String TEXT_6 = "> {" + NL + "" + NL + "            public ";
  protected final String TEXT_7 = " call(";
  protected final String TEXT_8 = " input) {";
  protected final String TEXT_9 = NL + "                ";
  protected final String TEXT_10 = " result = new ";
  protected final String TEXT_11 = "();";
  protected final String TEXT_12 = NL + "                        if(input.";
  protected final String TEXT_13 = " != null) {" + NL + "                            result.";
  protected final String TEXT_14 = " = new java.sql.Timestamp(input.";
  protected final String TEXT_15 = ".getTime());" + NL + "                        } else {" + NL + "                            result.";
  protected final String TEXT_16 = " = null;" + NL + "                        }";
  protected final String TEXT_17 = NL + "                    result.";
  protected final String TEXT_18 = " = input.";
  protected final String TEXT_19 = ";";
  protected final String TEXT_20 = NL + "                return result;" + NL + "            }" + NL + "        }";
  protected final String TEXT_21 = NL;

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    
// Parse the inputs to this javajet generator.
final BigDataCodeGeneratorArgument codeGenArgument = (BigDataCodeGeneratorArgument)argument;
final INode node = (INode)codeGenArgument.getArgument();
final String cid = node.getUniqueName();

final String dataframeClass = org.talend.hadoop.distribution.ESparkVersion.SPARK_2_0.compareTo(codeGenArgument.getSparkVersion()) > 0
    ? "org.apache.spark.sql.DataFrame"
    : "org.apache.spark.sql.Dataset<org.apache.spark.sql.Row>";

    
TSqlRowUtil tSqlRowUtil = new TSqlRowUtil(node);
String validateError = tSqlRowUtil.validate(true, false);
if (validateError != null) {
    // Cause the job compilation to explicitly fail if there is a problem.
    return "throw new JobConfigurationError(\"" + validateError +"\");";
}

// If the incoming rowStruct contains a Date field (always typed as java.util.Date),
// we must generate a new structure which replaces these java.util.Date instances by
// java.sql.Timestamp instances since DataFrames only support java.sql.Timestamp.

org.talend.designer.bigdata.avro.AvroRecordStructGenerator avroRecordStructGenerator = (org.talend.designer.bigdata.avro.AvroRecordStructGenerator) codeGenArgument.getRecordStructGenerator();

// Some of the incoming connections might share the same schema (and then the same rowXStruct). We must generate the below code only once by schema (if necessary).
java.util.Set<String> knownStructNames = new java.util.HashSet();

for(IConnection incomingConnection : tSqlRowUtil.getIncomingConnections()) {
    String originalStructName = codeGenArgument.getRecordStructName(incomingConnection);
    if(tSqlRowUtil.containsDateFields(incomingConnection) && !knownStructNames.contains(originalStructName)) {
        java.util.List<IMetadataColumn> columns = tSqlRowUtil.getColumns(incomingConnection);
        String suggestedDfStructName = "DF_"+originalStructName;
        String dfStructName = avroRecordStructGenerator.generateRecordStructForDataFrame(suggestedDfStructName, originalStructName);
        knownStructNames.add(originalStructName);

    stringBuffer.append(TEXT_1);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_2);
    stringBuffer.append(originalStructName);
    stringBuffer.append(TEXT_3);
    stringBuffer.append(dfStructName);
    stringBuffer.append(TEXT_4);
    stringBuffer.append(originalStructName);
    stringBuffer.append(TEXT_5);
    stringBuffer.append(dfStructName);
    stringBuffer.append(TEXT_6);
    stringBuffer.append(dfStructName);
    stringBuffer.append(TEXT_7);
    stringBuffer.append(originalStructName);
    stringBuffer.append(TEXT_8);
    stringBuffer.append(TEXT_9);
    stringBuffer.append(dfStructName);
    stringBuffer.append(TEXT_10);
    stringBuffer.append(dfStructName);
    stringBuffer.append(TEXT_11);
    
                for(IMetadataColumn column : columns) {
                    if(tSqlRowUtil.isDateField(column)) {

    stringBuffer.append(TEXT_12);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_13);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_14);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_15);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_16);
    
                } else {

    stringBuffer.append(TEXT_17);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_18);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_19);
    
                }
            } // end for(IMetadataColumn column : columns)

    stringBuffer.append(TEXT_20);
    
    } // end if(tSqlRowUtil.containsDateFields(incomingConnection) && !knownStructNames.contains(originalStructName))
} // end for(IConnection incomingConnection : tSqlRowUtil.getIncomingConnections())

    stringBuffer.append(TEXT_21);
    return stringBuffer.toString();
  }
}
