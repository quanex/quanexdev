package org.talend.designer.codegen.translators.databases.hive;

import java.util.List;
import org.talend.core.model.metadata.IMetadataColumn;
import org.talend.core.model.metadata.IMetadataTable;
import org.talend.core.model.metadata.types.JavaType;
import org.talend.core.model.metadata.types.JavaTypesManager;
import org.talend.core.model.process.ElementParameterParser;
import org.talend.core.model.process.IConnection;
import org.talend.core.model.process.INode;
import org.talend.designer.common.BigDataCodeGeneratorArgument;
import org.talend.designer.common.tsqlrow.TSqlRowUtil;
import java.util.Map;
import java.util.List;
import java.util.StringJoiner;

public class THiveOutputSparkstreamingcodeJava
{
  protected static String nl;
  public static synchronized THiveOutputSparkstreamingcodeJava create(String lineSeparator)
  {
    nl = lineSeparator;
    THiveOutputSparkstreamingcodeJava result = new THiveOutputSparkstreamingcodeJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = NL + "public static class ";
  protected final String TEXT_2 = "_From";
  protected final String TEXT_3 = "To";
  protected final String TEXT_4 = " implements org.apache.spark.api.java.function.Function<";
  protected final String TEXT_5 = ", ";
  protected final String TEXT_6 = "> {" + NL + "" + NL + "    public ";
  protected final String TEXT_7 = " call(";
  protected final String TEXT_8 = " input) {";
  protected final String TEXT_9 = NL + "        ";
  protected final String TEXT_10 = " result = new ";
  protected final String TEXT_11 = "();";
  protected final String TEXT_12 = NL + "                    if(input.";
  protected final String TEXT_13 = " != null) {" + NL + "                        result.";
  protected final String TEXT_14 = " = new java.sql.";
  protected final String TEXT_15 = "(input.";
  protected final String TEXT_16 = ".getTime());" + NL + "                    } else {" + NL + "                        result.";
  protected final String TEXT_17 = " = null;" + NL + "                    }";
  protected final String TEXT_18 = NL + "        result.";
  protected final String TEXT_19 = " = input.";
  protected final String TEXT_20 = ";";
  protected final String TEXT_21 = NL + "        return result;" + NL + "    }" + NL + "}";
  protected final String TEXT_22 = NL + "    public static class ";
  protected final String TEXT_23 = "_";
  protected final String TEXT_24 = "ToRow implements org.apache.spark.api.java.function.Function<";
  protected final String TEXT_25 = ", org.apache.spark.sql.Row> {" + NL + "        public org.apache.spark.sql.Row call(";
  protected final String TEXT_26 = " input) {" + NL + "            return org.apache.spark.sql.RowFactory.create(";
  protected final String TEXT_27 = ", ";
  protected final String TEXT_28 = NL + "                    input.";
  protected final String TEXT_29 = NL + "            );" + NL + "        }" + NL + "    }";
  protected final String TEXT_30 = NL + NL + "    static class ";
  protected final String TEXT_31 = "_JavaHiveContextSingleton {" + NL + "      static private transient org.apache.spark.sql.hive.HiveContext instance = null;" + NL + "      static public org.apache.spark.sql.hive.HiveContext getInstance(org.apache.spark.SparkContext sparkContext, ContextProperties context) {" + NL + "        if (instance == null) {";
  protected final String TEXT_32 = NL + "                System.setProperty(\"hive.metastore.uris\", \"thrift://\" + ";
  protected final String TEXT_33 = " + \":\" + ";
  protected final String TEXT_34 = " + \"/\");";
  protected final String TEXT_35 = NL + "                System.setProperty(\"hive.metastore.uris\", ";
  protected final String TEXT_36 = ");";
  protected final String TEXT_37 = NL + "                System.setProperty(\"hive.metastore.sasl.enabled\", \"true\");" + NL + "                System.setProperty(\"hive.security.authorization.enabled\", \"false\");" + NL + "                System.setProperty(\"hive.metastore.kerberos.principal\", ";
  protected final String TEXT_38 = ");" + NL + "                System.setProperty(\"hive.metastore.execute.setugi\", \"true\");";
  protected final String TEXT_39 = NL + "              System.setProperty(\"fs.defaultFS\", ";
  protected final String TEXT_40 = ");";
  protected final String TEXT_41 = NL + NL + "          instance = new org.apache.spark.sql.hive.HiveContext(sparkContext);" + NL + "        }" + NL + "        return instance;" + NL + "      }" + NL + "    }" + NL;
  protected final String TEXT_42 = NL + "    public static class ";
  protected final String TEXT_43 = "_ForeachRDDOutput implements ";
  protected final String TEXT_44 = " {" + NL + "" + NL + "        private ContextProperties context;" + NL + "" + NL + "        public ";
  protected final String TEXT_45 = "_ForeachRDDOutput(JobConf job){" + NL + "            this.context = new ContextProperties(job);" + NL + "       }" + NL + "" + NL + "        @Override" + NL + "        public ";
  protected final String TEXT_46 = " call(";
  protected final String TEXT_47 = " temporaryRdd) throws Exception {" + NL + "" + NL + "            org.apache.spark.sql.hive.HiveContext hiveContext_";
  protected final String TEXT_48 = " = ";
  protected final String TEXT_49 = "_JavaHiveContextSingleton.getInstance(temporaryRdd.context(), this.context);" + NL;
  protected final String TEXT_50 = NL + "                ";
  protected final String TEXT_51 = " df_";
  protected final String TEXT_52 = " = hiveContext_";
  protected final String TEXT_53 = ".createDataFrame(temporaryRdd, ";
  protected final String TEXT_54 = ".class);";
  protected final String TEXT_55 = NL + "                org.apache.spark.sql.types.StructType st_";
  protected final String TEXT_56 = " = (org.apache.spark.sql.types.StructType) org.apache.spark.sql.catalyst.JavaTypeInference.inferDataType(";
  protected final String TEXT_57 = ".class)._1();" + NL + "                org.apache.spark.sql.types.StructField[] fields_";
  protected final String TEXT_58 = " = st_";
  protected final String TEXT_59 = ".fields();";
  protected final String TEXT_60 = NL + "                                Integer precision_";
  protected final String TEXT_61 = " = ";
  protected final String TEXT_62 = ";" + NL + "                                if(precision_";
  protected final String TEXT_63 = " > org.apache.spark.sql.types.DecimalType.MAX_PRECISION()) {";
  protected final String TEXT_64 = NL + "                                    log.warn(\"Decimal precision (\" + precision_";
  protected final String TEXT_65 = " + \") cannot be greater than MAX_PRECISION (\" + org.apache.spark.sql.types.DecimalType.MAX_PRECISION() + \"). Decimal precision is set to (\" + org.apache.spark.sql.types.DecimalType.MAX_PRECISION() + \").\");";
  protected final String TEXT_66 = " " + NL + "                                    precision_";
  protected final String TEXT_67 = " = org.apache.spark.sql.types.DecimalType.MAX_PRECISION();" + NL + "                                }" + NL + "                                Integer scale_";
  protected final String TEXT_68 = " = ";
  protected final String TEXT_69 = ";" + NL + "                                if(scale_";
  protected final String TEXT_70 = " != null) {" + NL + "                                    if(scale_";
  protected final String TEXT_71 = " > precision_";
  protected final String TEXT_72 = ") {";
  protected final String TEXT_73 = NL + "                                        log.warn(\"Decimal scale (\" + scale_";
  protected final String TEXT_74 = " + \") cannot be greater than precision (\" + precision_";
  protected final String TEXT_75 = " + \"). Decimal scale is set to (\" + precision_";
  protected final String TEXT_76 = " + \").\");";
  protected final String TEXT_77 = " " + NL + "                                        scale_";
  protected final String TEXT_78 = " = precision_";
  protected final String TEXT_79 = ";" + NL + "                                    }" + NL + "                                    fields_";
  protected final String TEXT_80 = "[";
  protected final String TEXT_81 = "] = org.apache.spark.sql.types.DataTypes.createStructField(fields_";
  protected final String TEXT_82 = "[";
  protected final String TEXT_83 = "].name(), org.apache.spark.sql.types.DataTypes.createDecimalType(precision_";
  protected final String TEXT_84 = ", scale_";
  protected final String TEXT_85 = "), fields_";
  protected final String TEXT_86 = "[";
  protected final String TEXT_87 = "].nullable());" + NL + "                                } else {" + NL + "                                    scale_";
  protected final String TEXT_88 = " = (";
  protected final String TEXT_89 = " > precision_";
  protected final String TEXT_90 = ") ? precision_";
  protected final String TEXT_91 = " : ";
  protected final String TEXT_92 = ";";
  protected final String TEXT_93 = NL + "                                    log.warn(\"Decimal scale is set to (\" + scale_";
  protected final String TEXT_94 = " + \").\");";
  protected final String TEXT_95 = " " + NL + "                                    fields_";
  protected final String TEXT_96 = "[";
  protected final String TEXT_97 = "] = org.apache.spark.sql.types.DataTypes.createStructField(fields_";
  protected final String TEXT_98 = "[";
  protected final String TEXT_99 = "].name(), org.apache.spark.sql.types.DataTypes.createDecimalType(precision_";
  protected final String TEXT_100 = ", scale_";
  protected final String TEXT_101 = "), fields_";
  protected final String TEXT_102 = "[";
  protected final String TEXT_103 = "].nullable());" + NL + "                                }";
  protected final String TEXT_104 = NL + "                                Integer scale_";
  protected final String TEXT_105 = " = ";
  protected final String TEXT_106 = ";" + NL + "                                if(scale_";
  protected final String TEXT_107 = " != null) {" + NL + "                                    if(scale_";
  protected final String TEXT_108 = " > org.apache.spark.sql.types.DecimalType.MAX_SCALE()) {";
  protected final String TEXT_109 = NL + "                                        log.warn(\"Decimal scale (\" + scale_";
  protected final String TEXT_110 = " + \") cannot be greater than MAX_SCALE (\" + org.apache.spark.sql.types.DecimalType.MAX_SCALE() + \"). Decimal scale is set to (\" + org.apache.spark.sql.types.DecimalType.MAX_SCALE() + \").\");";
  protected final String TEXT_111 = " " + NL + "                                        scale_";
  protected final String TEXT_112 = " = org.apache.spark.sql.types.DecimalType.MAX_SCALE();" + NL + "                                    }" + NL + "                                    fields_";
  protected final String TEXT_113 = "[";
  protected final String TEXT_114 = "] = org.apache.spark.sql.types.DataTypes.createStructField(fields_";
  protected final String TEXT_115 = "[";
  protected final String TEXT_116 = "].name(), org.apache.spark.sql.types.DataTypes.createDecimalType(org.apache.spark.sql.types.DecimalType.MAX_PRECISION(), scale_";
  protected final String TEXT_117 = "), fields_";
  protected final String TEXT_118 = "[";
  protected final String TEXT_119 = "].nullable());" + NL + "                                } else {";
  protected final String TEXT_120 = NL + "                                    log.warn(\"Decimal precision is set to (\" + org.apache.spark.sql.types.DecimalType.MAX_PRECISION() + \") and scale is set to (\" + ";
  protected final String TEXT_121 = " + \").\");";
  protected final String TEXT_122 = " " + NL + "                                    fields_";
  protected final String TEXT_123 = "[";
  protected final String TEXT_124 = "] = org.apache.spark.sql.types.DataTypes.createStructField(fields_";
  protected final String TEXT_125 = "[";
  protected final String TEXT_126 = "].name(), org.apache.spark.sql.types.DataTypes.createDecimalType(org.apache.spark.sql.types.DecimalType.MAX_PRECISION(), ";
  protected final String TEXT_127 = "), fields_";
  protected final String TEXT_128 = "[";
  protected final String TEXT_129 = "].nullable());" + NL + "                                }";
  protected final String TEXT_130 = NL + "                org.apache.spark.api.java.JavaRDD<org.apache.spark.sql.Row> rddRow = temporaryRdd.map(new ";
  protected final String TEXT_131 = "_";
  protected final String TEXT_132 = "ToRow());";
  protected final String TEXT_133 = NL + "                ";
  protected final String TEXT_134 = " df_";
  protected final String TEXT_135 = " = hiveContext_";
  protected final String TEXT_136 = ".createDataFrame(rddRow, org.apache.spark.sql.types.DataTypes.createStructType(fields_";
  protected final String TEXT_137 = "));" + NL + "                ";
  protected final String TEXT_138 = NL + "                    // Ensure that the columns order is consistent with the component schema" + NL + "                    java.util.List<org.apache.spark.sql.Column> columns_";
  protected final String TEXT_139 = " = new java.util.ArrayList<org.apache.spark.sql.Column>();";
  protected final String TEXT_140 = NL + "                        columns_";
  protected final String TEXT_141 = ".add(df_";
  protected final String TEXT_142 = ".col(\"";
  protected final String TEXT_143 = "\"));";
  protected final String TEXT_144 = NL + "                    df_";
  protected final String TEXT_145 = " = df_";
  protected final String TEXT_146 = ".select(scala.collection.JavaConversions.asScalaBuffer(columns_";
  protected final String TEXT_147 = ").seq());";
  protected final String TEXT_148 = NL + "\t            \torg.apache.spark.sql.SparkSession sparkSession = org.apache.spark.sql.SparkSession.builder().sparkContext(temporaryRdd.context()).getOrCreate();" + NL + "\t            \torg.apache.spark.sql.catalog.Catalog catalog = sparkSession.catalog();" + NL + "\t            \tString databaseUri = catalog.getDatabase(";
  protected final String TEXT_149 = ").locationUri();" + NL + "\t            ";
  protected final String TEXT_150 = NL + "                hiveContext_";
  protected final String TEXT_151 = ".sql(\"USE \" + ";
  protected final String TEXT_152 = ");";
  protected final String TEXT_153 = NL + "                    df_";
  protected final String TEXT_154 = ".saveAsTable(";
  protected final String TEXT_155 = ", \"";
  protected final String TEXT_156 = "\", org.apache.spark.sql.SaveMode.";
  protected final String TEXT_157 = ");";
  protected final String TEXT_158 = NL + "                       hiveContext_";
  protected final String TEXT_159 = ".setConf(\"hive.exec.dynamic.partition\", \"true\");" + NL + "                       hiveContext_";
  protected final String TEXT_160 = ".setConf(\"hive.exec.dynamic.partition.mode\", \"nonstrict\");";
  protected final String TEXT_161 = NL + "                    df_";
  protected final String TEXT_162 = ".write()" + NL + "                            .mode(org.apache.spark.sql.SaveMode.";
  protected final String TEXT_163 = ")";
  protected final String TEXT_164 = NL + "                            .format(\"";
  protected final String TEXT_165 = "\")";
  protected final String TEXT_166 = NL + "                            .option(\"path\", databaseUri + \"/\" + ";
  protected final String TEXT_167 = ")";
  protected final String TEXT_168 = NL + "                            .partitionBy(";
  protected final String TEXT_169 = ")";
  protected final String TEXT_170 = NL + "                            .insertInto(";
  protected final String TEXT_171 = ");";
  protected final String TEXT_172 = NL + "                            .saveAsTable(";
  protected final String TEXT_173 = ");";
  protected final String TEXT_174 = NL;
  protected final String TEXT_175 = NL + "                df_";
  protected final String TEXT_176 = ".write().format(\"orc\").mode(org.apache.spark.sql.SaveMode.";
  protected final String TEXT_177 = ")" + NL + "                    .save(";
  protected final String TEXT_178 = " + ";
  protected final String TEXT_179 = ");";
  protected final String TEXT_180 = NL + "            ";
  protected final String TEXT_181 = NL + "        }" + NL + "" + NL + "    }";

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    
// Parse the inputs to this javajet generator.
final BigDataCodeGeneratorArgument codeGenArgument = (BigDataCodeGeneratorArgument)argument;
final INode node = (INode)codeGenArgument.getArgument();
final String cid = node.getUniqueName();

final String dataframeClass = org.talend.hadoop.distribution.ESparkVersion.SPARK_2_0.compareTo(codeGenArgument.getSparkVersion()) > 0
    ? "org.apache.spark.sql.DataFrame"
    : "org.apache.spark.sql.Dataset<org.apache.spark.sql.Row>";

    
List<IMetadataTable> metadatas = node.getMetadataList();

TSqlRowUtil tSqlRowUtil = new TSqlRowUtil(node);
String validateError = tSqlRowUtil.validate(true, false);
if (validateError != null) {
    // Cause the job compilation to explicitly fail if there is a problem.
    return "throw new JobConfigurationError(\"" + validateError +"\");";
}

final boolean useTimestampForDatesInDataframes = ElementParameterParser.getBooleanValue(node, "__DATE_TO_TIMESTAMP_DF_TYPE_SUBSTITUTION__");

// Find the tSparkConfiguration and define which Spark version is currently used. It will define the generation mode: RDD with InputFormat or native Dataframe API.
final java.util.List<? extends INode> sparkConfigs = node.getProcess().getNodesOfType("tSparkConfiguration");
INode sparkConfig = null;
if(sparkConfigs != null && sparkConfigs.size() > 0) {
    sparkConfig = sparkConfigs.get(0);
}

boolean useLocalMode = false;
org.talend.hadoop.distribution.component.SparkBatchComponent sparkBatchDistrib = null;
if(sparkConfig != null) {
    String sparkDistribution = ElementParameterParser.getValue(sparkConfig, "__DISTRIBUTION__");
    String sparkVersion = ElementParameterParser.getValue(sparkConfig, "__SPARK_VERSION__");

    useLocalMode = "true".equals(ElementParameterParser.getValue(sparkConfig, "__SPARK_LOCAL_MODE__"));
    if(!useLocalMode) {
        try {
            sparkBatchDistrib = (org.talend.hadoop.distribution.component.SparkBatchComponent) org.talend.hadoop.distribution.DistributionFactory.buildDistribution(sparkDistribution, sparkVersion);
        } catch (java.lang.Exception e) {
            e.printStackTrace();
            return "";
        }
    }
}

String saveMode = ElementParameterParser.getValue(node, "__SAVEMODE__");
String saveFormat = ElementParameterParser.getValue(node, "__TABLEFORMAT__");
String outputSource = ElementParameterParser.getValue(node, "__OUTPUT_SOURCE__");

// When true, lets Dataframe re-order the columns by alphabetical order.
// This has been added for retrocompatibility with previous Studio versions.
boolean sortColumnsAlphabetically = ElementParameterParser.getBooleanValue(node, "__SORT_COLUMNS_ALPHABETICALLY__");

// Get Hive & HDFS configurations
String hiveConfiguration = ElementParameterParser.getValue(node, "__HIVE_STORAGE_CONFIGURATION__");
String hdfsConfiguration = ElementParameterParser.getValue(node, "__HDFS_STORAGE_CONFIGURATION__");
INode hiveConfigurationNode = null;
INode hdfsConfigurationNode = null;
boolean isHiveMetastoreHaEnabled = false;
String hiveThriftMetaStoreHost = null;
String hiveThriftMetaStorePort = null;
String hiveMetastoreUris = null;
String hdfsNamenodeURI = null;
boolean useKrb = false;
String hivePrincipal = null;

// Get Hive partitioning parameters
Boolean hivePartitionsEnabled = ElementParameterParser.getBooleanValue(node, "__HIVE_PARTITIONS_ENABLED__");
List<Map<String, String>> hivePartitionsKeys = null;
StringJoiner hivePartitionsKeysSJ = new StringJoiner(",");

if (hivePartitionsEnabled) {
    hivePartitionsKeys = (List<Map<String,String>>) ElementParameterParser.getObjectValue(node, "__HIVE_PARTITIONS_KEYS__");
    for (Map<String, String> parameterRow: hivePartitionsKeys) {
        hivePartitionsKeysSJ.add(String.format("\"%1$s\"",parameterRow.get("KEY_COLUMN")));
    }
}

for (INode pNode1 : node.getProcess().getNodesOfType("tHiveConfiguration")) {
    if(hiveConfiguration!=null && hiveConfiguration.equals(pNode1.getUniqueName())) {
        hiveConfigurationNode = pNode1;
        isHiveMetastoreHaEnabled = "true".equals(ElementParameterParser.getValue(hiveConfigurationNode, "__ENABLE_HIVE_HA__"));
        if(!isHiveMetastoreHaEnabled) {
            hiveThriftMetaStoreHost = ElementParameterParser.getValue(hiveConfigurationNode, "__HOST__");
            hiveThriftMetaStorePort = ElementParameterParser.getValue(hiveConfigurationNode, "__PORT__");
        } else {
            hiveMetastoreUris = ElementParameterParser.getValue(hiveConfigurationNode, "__HIVE_METASTORE_URIS__");
        }
        useKrb = "true".equals(ElementParameterParser.getValue(hiveConfigurationNode, "__USE_KRB__"));
        hivePrincipal = ElementParameterParser.getValue(hiveConfigurationNode, "__HIVE_PRINCIPAL__");
        break;
    }
}

if (useLocalMode || (sparkBatchDistrib != null && !sparkBatchDistrib.useCloudLauncher())) {
    for (INode pNode2 : node.getProcess().getNodesOfType("tHDFSConfiguration")) {
        if(hdfsConfiguration!=null && hdfsConfiguration.equals(pNode2.getUniqueName())) {
            hdfsConfigurationNode = pNode2;
            hdfsNamenodeURI = ElementParameterParser.getValue(hdfsConfigurationNode, "__FS_DEFAULT_NAME__");
            break;
        }
    }
}

IConnection componentIncomingConnection = tSqlRowUtil.getIncomingConnections().get(0);
String inStructName = codeGenArgument.getRecordStructName(componentIncomingConnection);
String structName = inStructName;
boolean containsDate = false; 

// Change the structName if there's a Date column
for(IConnection incomingConnection : tSqlRowUtil.getIncomingConnections()) {
    if(tSqlRowUtil.containsDateFields(incomingConnection)) {
        containsDate = true;
        break;
    }
}

if (containsDate) {
        structName = "DF_"+inStructName+"AvroRecord";
}

// If the incoming rowStruct contains a Date field (always typed as java.util.Date),
// we must generate a new structure which replaces these java.util.Date instances by
// java.sql.Date or java.sql.Timestamp instances.
org.talend.designer.bigdata.avro.AvroRecordStructGenerator avroRecordStructGenerator = (org.talend.designer.bigdata.avro.AvroRecordStructGenerator) codeGenArgument.getRecordStructGenerator();

for(IConnection incomingConnection : tSqlRowUtil.getIncomingConnections()) {
    java.util.List<IMetadataColumn> columns = tSqlRowUtil.getColumns(incomingConnection);
    String originalStructName = codeGenArgument.getRecordStructName(incomingConnection);
    if(tSqlRowUtil.containsDateFields(incomingConnection)) {
        String suggestedDfStructName = "DF_"+originalStructName;
        String dfStructName = avroRecordStructGenerator.generateRecordStructForDataFrame(suggestedDfStructName, originalStructName, useTimestampForDatesInDataframes);

    stringBuffer.append(TEXT_1);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_2);
    stringBuffer.append(originalStructName);
    stringBuffer.append(TEXT_3);
    stringBuffer.append(dfStructName);
    stringBuffer.append(TEXT_4);
    stringBuffer.append(originalStructName);
    stringBuffer.append(TEXT_5);
    stringBuffer.append(dfStructName);
    stringBuffer.append(TEXT_6);
    stringBuffer.append(dfStructName);
    stringBuffer.append(TEXT_7);
    stringBuffer.append(originalStructName);
    stringBuffer.append(TEXT_8);
    stringBuffer.append(TEXT_9);
    stringBuffer.append(dfStructName);
    stringBuffer.append(TEXT_10);
    stringBuffer.append(dfStructName);
    stringBuffer.append(TEXT_11);
    
        for(IMetadataColumn column : columns) {
            if(tSqlRowUtil.isDateField(column)) {

    stringBuffer.append(TEXT_12);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_13);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_14);
    stringBuffer.append(useTimestampForDatesInDataframes ? "Timestamp" : "Date");
    stringBuffer.append(TEXT_15);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_16);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_17);
    
            } else {

    stringBuffer.append(TEXT_18);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_19);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_20);
    
            }
        } // end for(IMetadataColumn column : columns)

    stringBuffer.append(TEXT_21);
    
    } // end if(tSqlRowUtil.containsDateFields(incomingConnection))

    stringBuffer.append(TEXT_22);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_23);
    stringBuffer.append(originalStructName);
    stringBuffer.append(TEXT_24);
    stringBuffer.append(structName);
    stringBuffer.append(TEXT_25);
    stringBuffer.append(structName);
    stringBuffer.append(TEXT_26);
     
                boolean isFirst = true;
                java.util.List<IMetadataColumn> columnsCopy = new java.util.ArrayList<IMetadataColumn>(columns);
                java.util.Collections.sort(columnsCopy, new java.util.Comparator<IMetadataColumn>() {
                    @Override
                    public int compare(IMetadataColumn c1, IMetadataColumn c2) {
                        return c1.getLabel().compareTo(c2.getLabel());
                    }
                });
                for(IMetadataColumn column : columnsCopy) {
                    if(!isFirst) {
                        
    stringBuffer.append(TEXT_27);
    
                    }
                    isFirst = false; 

    stringBuffer.append(TEXT_28);
    stringBuffer.append(column.getLabel());
     
                } 

    stringBuffer.append(TEXT_29);
    
} // end for(IConnection incomingConnection : tSqlRowUtil.getIncomingConnections())


    stringBuffer.append(TEXT_30);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_31);
    if(!isHiveMetastoreHaEnabled) {
    stringBuffer.append(TEXT_32);
    stringBuffer.append(hiveThriftMetaStoreHost);
    stringBuffer.append(TEXT_33);
    stringBuffer.append(hiveThriftMetaStorePort);
    stringBuffer.append(TEXT_34);
    } else {
    stringBuffer.append(TEXT_35);
    stringBuffer.append(hiveMetastoreUris);
    stringBuffer.append(TEXT_36);
    }
    
            if(useKrb){
                
    stringBuffer.append(TEXT_37);
    stringBuffer.append(hivePrincipal);
    stringBuffer.append(TEXT_38);
    
            }

     if(hdfsNamenodeURI != null) { 
    stringBuffer.append(TEXT_39);
    stringBuffer.append(hdfsNamenodeURI);
    stringBuffer.append(TEXT_40);
     } 
    stringBuffer.append(TEXT_41);
    
{ // Start ForeachRDD helper function
    // The signature of foreachRDD has changed in Spark 2.0
    org.talend.designer.spark.generator.utils.ForeachRDDUtil foreachUtil =
            org.talend.designer.spark.generator.utils.ForeachRDDUtil.createFunctionJavaRDD(
                    codeGenArgument.getSparkVersion(), structName);
    
    stringBuffer.append(TEXT_42);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_43);
    stringBuffer.append(foreachUtil.getFunctionInterface());
    stringBuffer.append(TEXT_44);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_45);
    stringBuffer.append(foreachUtil.getCallReturnType());
    stringBuffer.append(TEXT_46);
    stringBuffer.append(foreachUtil.getCallArgumentType());
    stringBuffer.append(TEXT_47);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_48);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_49);
    
            if(org.talend.hadoop.distribution.ESparkVersion.SPARK_1_3.compareTo(codeGenArgument.getSparkVersion()) >= 0){
                
    stringBuffer.append(TEXT_50);
    stringBuffer.append(dataframeClass);
    stringBuffer.append(TEXT_51);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_52);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_53);
    stringBuffer.append(structName);
    stringBuffer.append(TEXT_54);
    
            }
            // Spark > 1.3 uses the Java JavaTypeInference to change Big Decimal precision
            else {

    stringBuffer.append(TEXT_55);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_56);
    stringBuffer.append(structName);
    stringBuffer.append(TEXT_57);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_58);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_59);
    
                if ((metadatas!=null) && (metadatas.size() > 0)) {
                    IMetadataTable metadata = metadatas.get(0);
                    java.util.List<IMetadataColumn> columnsCopy = new java.util.ArrayList<IMetadataColumn>(metadata.getListColumns());
                    java.util.Collections.sort(columnsCopy, new java.util.Comparator<IMetadataColumn>() {
                        @Override
                        public int compare(IMetadataColumn c1, IMetadataColumn c2) {
                            return c1.getLabel().compareTo(c2.getLabel());
                        }
                    });
                    int nbColumns = columnsCopy.size();
                    for(int i=0; i<nbColumns; i++) {
                        IMetadataColumn column = columnsCopy.get(i);
                        String columnName = column.getLabel();
                        String typeToGenerate = JavaTypesManager.getTypeToGenerate(column.getTalendType(), column.isNullable());
                        JavaType javaType = JavaTypesManager.getJavaTypeFromId(column.getTalendType());
                        
                        if(javaType == JavaTypesManager.BIGDECIMAL) {
                            Integer precision = column.getLength();
                            Integer scale = column.getPrecision();
                            final int defaultScale = 18;
                            
                            boolean isLog4jEnabled = ("true").equals(ElementParameterParser.getValue(node.getProcess(), "__LOG4J_ACTIVATE__"));
                        
                            if(precision != null) {
                            
    stringBuffer.append(TEXT_60);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_61);
    stringBuffer.append(precision);
    stringBuffer.append(TEXT_62);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_63);
     if(isLog4jEnabled) { 
    stringBuffer.append(TEXT_64);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_65);
     } 
    stringBuffer.append(TEXT_66);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_67);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_68);
    stringBuffer.append(scale);
    stringBuffer.append(TEXT_69);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_70);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_71);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_72);
     if(isLog4jEnabled) { 
    stringBuffer.append(TEXT_73);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_74);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_75);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_76);
     } 
    stringBuffer.append(TEXT_77);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_78);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_79);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_80);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_81);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_82);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_83);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_84);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_85);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_86);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_87);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_88);
    stringBuffer.append(defaultScale);
    stringBuffer.append(TEXT_89);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_90);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_91);
    stringBuffer.append(defaultScale);
    stringBuffer.append(TEXT_92);
     if(isLog4jEnabled) { 
    stringBuffer.append(TEXT_93);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_94);
     } 
    stringBuffer.append(TEXT_95);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_96);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_97);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_98);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_99);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_100);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_101);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_102);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_103);
    
                            } else {
                            
    stringBuffer.append(TEXT_104);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_105);
    stringBuffer.append(scale);
    stringBuffer.append(TEXT_106);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_107);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_108);
     if(isLog4jEnabled) { 
    stringBuffer.append(TEXT_109);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_110);
     } 
    stringBuffer.append(TEXT_111);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_112);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_113);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_114);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_115);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_116);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_117);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_118);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_119);
     if(isLog4jEnabled) { 
    stringBuffer.append(TEXT_120);
    stringBuffer.append(defaultScale);
    stringBuffer.append(TEXT_121);
     } 
    stringBuffer.append(TEXT_122);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_123);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_124);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_125);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_126);
    stringBuffer.append(defaultScale);
    stringBuffer.append(TEXT_127);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_128);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_129);
    
                            }
                        }
                    }
                }
    
    stringBuffer.append(TEXT_130);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_131);
    stringBuffer.append(inStructName);
    stringBuffer.append(TEXT_132);
    stringBuffer.append(TEXT_133);
    stringBuffer.append(dataframeClass);
    stringBuffer.append(TEXT_134);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_135);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_136);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_137);
    
            }
            if(!sortColumnsAlphabetically) {
            
    stringBuffer.append(TEXT_138);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_139);
    
                    for(IMetadataColumn column : tSqlRowUtil.getColumns(componentIncomingConnection)) {
                    
    stringBuffer.append(TEXT_140);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_141);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_142);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_143);
    
                    }
                    
    stringBuffer.append(TEXT_144);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_145);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_146);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_147);
    
            }
            if(outputSource.equals("HIVE_TABLE")){
                String hiveDatabaseName = ElementParameterParser.getValue(node, "__HIVE_DATABASE_NAME__");
                String hiveTableName = ElementParameterParser.getValue(node, "__HIVE_TABLE_NAME__");
                boolean needDatabaseLocationUri = org.talend.hadoop.distribution.ESparkVersion.SPARK_2_2.compareTo(codeGenArgument.getSparkVersion()) <= 0;
                if (needDatabaseLocationUri){
                
    stringBuffer.append(TEXT_148);
    stringBuffer.append(hiveDatabaseName);
    stringBuffer.append(TEXT_149);
    
	            }
                
    stringBuffer.append(TEXT_150);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_151);
    stringBuffer.append(hiveDatabaseName);
    stringBuffer.append(TEXT_152);
    
                // Versions < 1.5 - no write() method or experimental - no partitioning
                if (org.talend.hadoop.distribution.ESparkVersion.SPARK_1_5.compareTo(codeGenArgument.getSparkVersion()) > 0) {
                    
    stringBuffer.append(TEXT_153);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_154);
    stringBuffer.append(hiveTableName);
    stringBuffer.append(TEXT_155);
    stringBuffer.append(saveFormat);
    stringBuffer.append(TEXT_156);
    stringBuffer.append(saveMode);
    stringBuffer.append(TEXT_157);
    
                } else {
                    // Versions >= 1.5 - write() method available - partitioning
                    
                    // Manually enable nonstrict partition mode when appending data
                    if (hivePartitionsEnabled && "Append".equals(saveMode)) {
                    
    stringBuffer.append(TEXT_158);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_159);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_160);
    
                    }
                    
    stringBuffer.append(TEXT_161);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_162);
    stringBuffer.append(saveMode);
    stringBuffer.append(TEXT_163);
     if(org.talend.hadoop.distribution.ESparkVersion.SPARK_2_1.compareTo(codeGenArgument.getSparkVersion()) < 0 || !"Append".equals(saveMode)) { 
    stringBuffer.append(TEXT_164);
    stringBuffer.append(saveFormat);
    stringBuffer.append(TEXT_165);
     if (needDatabaseLocationUri && (org.talend.hadoop.distribution.ESparkVersion.SPARK_2_1.compareTo(codeGenArgument.getSparkVersion()) < 0 || !"Append".equals(saveMode))) {
    stringBuffer.append(TEXT_166);
    stringBuffer.append(hiveTableName);
    stringBuffer.append(TEXT_167);
     }} 
     if (hivePartitionsEnabled) { 
    stringBuffer.append(TEXT_168);
    stringBuffer.append(hivePartitionsKeysSJ.toString());
    stringBuffer.append(TEXT_169);
     } 
     if(org.talend.hadoop.distribution.ESparkVersion.SPARK_2_1.compareTo(codeGenArgument.getSparkVersion()) >= 0 && "Append".equals(saveMode)) { 
    stringBuffer.append(TEXT_170);
    stringBuffer.append(hiveTableName);
    stringBuffer.append(TEXT_171);
     } else { 
    stringBuffer.append(TEXT_172);
    stringBuffer.append(hiveTableName);
    stringBuffer.append(TEXT_173);
     } 
    stringBuffer.append(TEXT_174);
    
                }
            }else if (outputSource.equals("ORC_FILE")){
                String outputFolder = ElementParameterParser.getValue(node, "__OUTPUT_FOLDER__");
                
    stringBuffer.append(TEXT_175);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_176);
    stringBuffer.append(saveMode);
    stringBuffer.append(TEXT_177);
    stringBuffer.append((hdfsNamenodeURI != null) ? hdfsNamenodeURI : "\"\"");
    stringBuffer.append(TEXT_178);
    stringBuffer.append(outputFolder);
    stringBuffer.append(TEXT_179);
    
            }
            
    stringBuffer.append(TEXT_180);
    stringBuffer.append(foreachUtil.getCallReturnCode());
    stringBuffer.append(TEXT_181);
    
} // End ForeachRDD helper function

    return stringBuffer.toString();
  }
}
