package org.talend.designer.codegen.translators.data_quality;

import org.talend.core.model.process.INode;
import org.talend.core.model.process.ElementParameterParser;
import org.talend.designer.codegen.config.CodeGeneratorArgument;
import org.talend.core.model.metadata.IMetadataTable;
import org.talend.core.model.metadata.IMetadataColumn;
import org.talend.core.model.process.IConnection;
import org.talend.core.model.process.IConnectionCategory;
import org.talend.core.model.metadata.types.JavaTypesManager;
import org.talend.core.model.metadata.types.JavaType;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.ArrayList;
import org.talend.core.model.utils.NodeUtil;
import org.talend.core.model.process.EConnectionType;

public class TDataShufflingOutMainJava
{
  protected static String nl;
  public static synchronized TDataShufflingOutMainJava create(String lineSeparator)
  {
    nl = lineSeparator;
    TDataShufflingOutMainJava result = new TDataShufflingOutMainJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "";
  protected final String TEXT_2 = NL + "\tList<Object> rowValue_";
  protected final String TEXT_3 = " =  new java.util.ArrayList<Object>();" + NL + "\t" + NL + "\t";
  protected final String TEXT_4 = NL + "\t" + NL + "\trowValue_";
  protected final String TEXT_5 = ".add(";
  protected final String TEXT_6 = ".";
  protected final String TEXT_7 = ");" + NL + "  \t";
  protected final String TEXT_8 = NL + "  \tshufflingService_";
  protected final String TEXT_9 = ".addOneRow(rowValue_";
  protected final String TEXT_10 = ");" + NL + "" + NL + "  \tnb_line_";
  protected final String TEXT_11 = "++;";

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    stringBuffer.append(TEXT_1);
    
    CodeGeneratorArgument codeGenArgument = (CodeGeneratorArgument) argument;
    INode node = (INode)codeGenArgument.getArgument();
    String cid = ElementParameterParser.getValue(node, "__DESTINATION__");
    
    INode virtualTargetNode = node.getOutgoingConnections(EConnectionType.ON_COMPONENT_OK).get(0).getTarget();
    String virtualTargetCid = virtualTargetNode.getUniqueName();

    String incomingConnName = null;    
	IMetadataTable inputMetadataTable = null;
	java.util.List<IMetadataColumn> inputColumns = null;

    List<? extends IConnection> incomingConnections = node.getIncomingConnections();
	if (incomingConnections != null && !incomingConnections.isEmpty()) {	
		for (IConnection conn : incomingConnections) {
			if (conn.getLineStyle().hasConnectionCategory(IConnectionCategory.DATA)) {
				incomingConnName = conn.getName();				
				inputMetadataTable = conn.getMetadataTable();
				inputColumns = inputMetadataTable.getListColumns();
				break;
			}
		}
	}  
	
  
    stringBuffer.append(TEXT_2);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_3);
    
	for(int i = 0; i < inputColumns.size(); i++){// start for
	IMetadataColumn column = inputColumns.get(i);
	
    stringBuffer.append(TEXT_4);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_5);
    stringBuffer.append(incomingConnName);
    stringBuffer.append(TEXT_6);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_7);
     		
  	}//end for
  	
    stringBuffer.append(TEXT_8);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_9);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_10);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_11);
    return stringBuffer.toString();
  }
}
