package org.talend.designer.codegen.translators.data_quality;

import org.talend.core.model.process.INode;
import org.talend.core.model.process.ElementParameterParser;
import org.talend.designer.codegen.config.CodeGeneratorArgument;
import org.talend.core.model.metadata.IMetadataTable;
import org.talend.core.model.metadata.IMetadataColumn;
import org.talend.core.model.process.IConnection;
import org.talend.core.model.process.IConnectionCategory;
import org.talend.core.model.metadata.types.JavaTypesManager;
import org.talend.core.model.metadata.types.JavaType;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashSet;
import java.util.Set;

public class TPatternMaskingBeginJava
{
  protected static String nl;
  public static synchronized TPatternMaskingBeginJava create(String lineSeparator)
  {
    nl = lineSeparator;
    TPatternMaskingBeginJava result = new TPatternMaskingBeginJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "";
  protected final String TEXT_2 = NL + NL + "final org.talend.dataquality.datamasking.TypeTester typeTester_";
  protected final String TEXT_3 = " = new org.talend.dataquality.datamasking.TypeTester();" + NL + "final java.util.Random rnd_";
  protected final String TEXT_4 = " = new java.util.Random" + NL + "(";
  protected final String TEXT_5 = NL + "                Long.valueOf(";
  protected final String TEXT_6 = ")";
  protected final String TEXT_7 = NL + ");" + NL;
  protected final String TEXT_8 = NL + "\t    List<org.talend.dataquality.datamasking.generic.FieldDefinition> fieldList_";
  protected final String TEXT_9 = "_";
  protected final String TEXT_10 = " = new java.util.ArrayList<>();";
  protected final String TEXT_11 = "           \t\t           \t\t" + NL + "           \t   \tfieldList_";
  protected final String TEXT_12 = "_";
  protected final String TEXT_13 = ".add(new org.talend.dataquality.datamasking.generic.FieldDefinition(" + NL + "                           \"";
  protected final String TEXT_14 = "\", ";
  protected final String TEXT_15 = ", ";
  protected final String TEXT_16 = "));" + NL + "\t\t";
  protected final String TEXT_17 = NL + "final org.talend.dataquality.datamasking.generic.BijectiveSubstitutionFunction fn_";
  protected final String TEXT_18 = "_";
  protected final String TEXT_19 = " = new org.talend.dataquality.datamasking.generic.BijectiveSubstitutionFunction(fieldList_";
  protected final String TEXT_20 = "_";
  protected final String TEXT_21 = ");" + NL + "" + NL + "fn_";
  protected final String TEXT_22 = "_";
  protected final String TEXT_23 = ".parse(null, ";
  protected final String TEXT_24 = ", rnd_";
  protected final String TEXT_25 = ");" + NL + "fn_";
  protected final String TEXT_26 = "_";
  protected final String TEXT_27 = ".setKeepEmpty(";
  protected final String TEXT_28 = ");" + NL;
  protected final String TEXT_29 = NL;
  protected final String TEXT_30 = NL + NL + NL + "org.talend.dataquality.datamasking.DataMasker<";
  protected final String TEXT_31 = "Struct, ";
  protected final String TEXT_32 = "Struct> duplicator_";
  protected final String TEXT_33 = NL + "                    = new org.talend.dataquality.datamasking.DataMasker<";
  protected final String TEXT_34 = "Struct, ";
  protected final String TEXT_35 = "Struct>()" + NL + "{" + NL + "    @Override" + NL + "    protected ";
  protected final String TEXT_36 = "Struct generateOutput(";
  protected final String TEXT_37 = "Struct originalStruct, boolean isOriginal)" + NL + "    {";
  protected final String TEXT_38 = NL + "        ";
  protected final String TEXT_39 = "Struct tmpStruct = new ";
  protected final String TEXT_40 = "Struct();" + NL;
  protected final String TEXT_41 = "      " + NL + "                tmpStruct.";
  protected final String TEXT_42 = " = originalStruct.";
  protected final String TEXT_43 = ";";
  protected final String TEXT_44 = NL + NL + "        if (isOriginal)" + NL + "        {" + NL + "            tmpStruct.ORIGINAL_MARK = true;" + NL + "        }" + NL + "        else" + NL + "        {" + NL + "            modifyOutput(tmpStruct);            " + NL + "            tmpStruct.ORIGINAL_MARK = false;" + NL + "        }" + NL + "        " + NL + "        return tmpStruct;" + NL + "    }" + NL + "" + NL + "    private void modifyOutput(";
  protected final String TEXT_45 = "Struct ";
  protected final String TEXT_46 = ") " + NL + "    {    " + NL;
  protected final String TEXT_47 = NL + "        \t    Object tmpValue_";
  protected final String TEXT_48 = "_";
  protected final String TEXT_49 = " = null;" + NL + "                    tmpValue_";
  protected final String TEXT_50 = "_";
  protected final String TEXT_51 = " = fn_";
  protected final String TEXT_52 = "_";
  protected final String TEXT_53 = ".generateMaskedRow(";
  protected final String TEXT_54 = ".";
  protected final String TEXT_55 = ");" + NL + "                    ";
  protected final String TEXT_56 = NL + "                    if(tmpValue_";
  protected final String TEXT_57 = "_";
  protected final String TEXT_58 = " == null)" + NL + "                    {";
  protected final String TEXT_59 = NL + "                        ";
  protected final String TEXT_60 = ".";
  protected final String TEXT_61 = " = null;" + NL + "                    }" + NL + "                    else" + NL + "                    {";
  protected final String TEXT_62 = "    " + NL + "                    ";
  protected final String TEXT_63 = NL + "                                ";
  protected final String TEXT_64 = ".";
  protected final String TEXT_65 = " = String.valueOf(tmpValue_";
  protected final String TEXT_66 = "_";
  protected final String TEXT_67 = ");";
  protected final String TEXT_68 = NL + "                               if(tmpValue_";
  protected final String TEXT_69 = "_";
  protected final String TEXT_70 = " instanceof java.util.Date)" + NL + "                                {";
  protected final String TEXT_71 = NL + "                                    ";
  protected final String TEXT_72 = ".";
  protected final String TEXT_73 = " = (java.util.Date)tmpValue_";
  protected final String TEXT_74 = "_";
  protected final String TEXT_75 = ";" + NL + "                                }" + NL + "                                else" + NL + "                                {";
  protected final String TEXT_76 = NL + "                                    ";
  protected final String TEXT_77 = ".";
  protected final String TEXT_78 = " = ParserUtils.parseTo_Date(tmpValue_";
  protected final String TEXT_79 = "_";
  protected final String TEXT_80 = ".toString(), ";
  protected final String TEXT_81 = ");" + NL + "                                }";
  protected final String TEXT_82 = NL + "                                ";
  protected final String TEXT_83 = ".";
  protected final String TEXT_84 = " = tmpValue_";
  protected final String TEXT_85 = "_";
  protected final String TEXT_86 = ".toString().getBytes();";
  protected final String TEXT_87 = "                                  ";
  protected final String TEXT_88 = NL + "                            ";
  protected final String TEXT_89 = ".";
  protected final String TEXT_90 = " = ParserUtils.parseTo_";
  protected final String TEXT_91 = "(String.valueOf(tmpValue_";
  protected final String TEXT_92 = "_";
  protected final String TEXT_93 = "));";
  protected final String TEXT_94 = " ";
  protected final String TEXT_95 = NL + "                    }";
  protected final String TEXT_96 = NL + "    }" + NL + "};";
  protected final String TEXT_97 = NL;

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    stringBuffer.append(TEXT_1);
    
    CodeGeneratorArgument codeGenArgument = (CodeGeneratorArgument) argument;
    INode node = (INode)codeGenArgument.getArgument();
    String cid = node.getUniqueName();

    Set<String> columnsToMask =  new HashSet<String>();
    List<Map<String, String>> ruleTableList = (List<Map<String, String>>)ElementParameterParser.getObjectValue(node, "__RULE_TABLE__");
for(Map<String, String> columnModifMap : ruleTableList) {
columnsToMask.add(columnModifMap.get("COLUMN_TO_MASK").toLowerCase());
}

    String randomSeedString = ElementParameterParser.getValue(node, "__RANDOM_SEED__");
    boolean keepNull = ("true").equals(ElementParameterParser.getValue(node, "__KEEP_NULL__"));
    boolean keepEmpty = ("true").equals(ElementParameterParser.getValue(node, "__KEEP_EMPTY__"));

    String incomingConnName = null;
    IMetadataTable inputMetadataTable = null;
    java.util.List<IMetadataColumn> inputColumns = null;
    List< ? extends IConnection> incomingConnections = node.getIncomingConnections();

    String outgoingConnName = null;
    IMetadataTable outputMetadataTable = null;
    java.util.List<IMetadataColumn> outputColumns = null;
    List< ? extends IConnection> outgoingConnections = node.getOutgoingConnections();

    if (incomingConnections != null && !incomingConnections.isEmpty()) {    
        for (IConnection conn : incomingConnections) {
            if (conn.getLineStyle().hasConnectionCategory(IConnectionCategory.DATA)) {
                INode preNode = conn.getSource();
                while("tLogRow".equals(preNode.getComponent().getName())) {     
                    List<? extends IConnection> preincomingConnections = preNode.getIncomingConnections();
                    if (preincomingConnections != null && !preincomingConnections.isEmpty()) {
                       for (IConnection preNodeConn : preincomingConnections) {
                          conn=preNodeConn;
                          preNode=preNodeConn.getSource();
                          break;
                       }
                    } else {
                       break;
                    }
                }
                incomingConnName = conn.getName();              
                inputMetadataTable = conn.getMetadataTable();
                inputColumns = inputMetadataTable.getListColumns();
                break;
            }
        }
    }  
    if (outgoingConnections != null && !outgoingConnections.isEmpty())
    {    
        for (IConnection conn : outgoingConnections)
        {
            if (conn.getLineStyle().hasConnectionCategory(IConnectionCategory.DATA))
            {
                outgoingConnName = conn.getName();              
                outputMetadataTable = conn.getMetadataTable();
                outputColumns = outputMetadataTable.getListColumns();
                break;
            }
        }
    }  

    stringBuffer.append(TEXT_2);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_3);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_4);
    
             if(randomSeedString.length() > 0 && !"\"\"".equals(randomSeedString)) {
        
    stringBuffer.append(TEXT_5);
    stringBuffer.append(randomSeedString);
    stringBuffer.append(TEXT_6);
    
             }
        
    stringBuffer.append(TEXT_7);
    
    for(int i = 0; i < inputColumns.size(); i++) {

        IMetadataColumn column = inputColumns.get(i);

        if(columnsToMask.contains(column.getLabel().toLowerCase())){

    stringBuffer.append(TEXT_8);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_9);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_10);
    
            for(Map<String, String> columnModifMap : ruleTableList) {

		String columnToMask = columnModifMap.get("COLUMN_TO_MASK");
                if (columnToMask.equalsIgnoreCase(column.getLabel())){
	      	   String fieldType = columnModifMap.get("FIELD_TYPE");
                   String values = null;
		   if (fieldType.equals("ENUMERATION"))
			values = columnModifMap.get("VALUES");
		   if (fieldType.equals("ENUMERATION_FROM_FILE"))
			values = columnModifMap.get("PATH");
		   String interval = null;
		   if (fieldType.equals("INTERVAL"))
			interval = columnModifMap.get("INTERVAL");
		   if (fieldType.equals("DATEPATTERN"))
			interval = columnModifMap.get("DATE_INTERVAL");
                   String sKeepFormat = columnModifMap.get("KEEP_FORMAT");
                
                   String typeToGenerate = JavaTypesManager.getTypeToGenerate(column.getTalendType(), true);//TDQ-11328: fix compil err for type "int"(not nullable)
                   JavaType javaType = JavaTypesManager.getJavaTypeFromId(column.getTalendType());

    stringBuffer.append(TEXT_11);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_12);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_13);
    stringBuffer.append(fieldType);
    stringBuffer.append(TEXT_14);
    stringBuffer.append(values);
    stringBuffer.append(TEXT_15);
    stringBuffer.append(interval);
    stringBuffer.append(TEXT_16);
    

               }
           } // for


    stringBuffer.append(TEXT_17);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_18);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_19);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_20);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_21);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_22);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_23);
    stringBuffer.append(keepNull);
    stringBuffer.append(TEXT_24);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_25);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_26);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_27);
    stringBuffer.append(keepEmpty);
    stringBuffer.append(TEXT_28);
    

        } // if

    stringBuffer.append(TEXT_29);
    

    } // for inputColumns

    stringBuffer.append(TEXT_30);
    stringBuffer.append(incomingConnName);
    stringBuffer.append(TEXT_31);
    stringBuffer.append(outgoingConnName);
    stringBuffer.append(TEXT_32);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_33);
    stringBuffer.append(incomingConnName);
    stringBuffer.append(TEXT_34);
    stringBuffer.append(outgoingConnName);
    stringBuffer.append(TEXT_35);
    stringBuffer.append(outgoingConnName);
    stringBuffer.append(TEXT_36);
    stringBuffer.append(incomingConnName);
    stringBuffer.append(TEXT_37);
    stringBuffer.append(TEXT_38);
    stringBuffer.append(outgoingConnName);
    stringBuffer.append(TEXT_39);
    stringBuffer.append(outgoingConnName);
    stringBuffer.append(TEXT_40);
     
            for(int i = 0; i < inputColumns.size(); i++)
            {
                IMetadataColumn column = inputColumns.get(i);

        
    stringBuffer.append(TEXT_41);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_42);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_43);
    
            }
        
    stringBuffer.append(TEXT_44);
    stringBuffer.append(outgoingConnName);
    stringBuffer.append(TEXT_45);
    stringBuffer.append(outgoingConnName);
    stringBuffer.append(TEXT_46);
     
            for(int i = 0; i < inputColumns.size(); i++)
            {
                IMetadataColumn column = inputColumns.get(i);
	        if(columnsToMask.contains(column.getLabel().toLowerCase()))
                { 
                    String typeToGenerate = JavaTypesManager.getTypeToGenerate(column.getTalendType(), column.isNullable());
                    JavaType javaType = JavaTypesManager.getJavaTypeFromId(column.getTalendType());
    
    stringBuffer.append(TEXT_47);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_48);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_49);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_50);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_51);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_52);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_53);
    stringBuffer.append(outgoingConnName);
    stringBuffer.append(TEXT_54);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_55);
    if (column.isNullable()) {
    stringBuffer.append(TEXT_56);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_57);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_58);
    stringBuffer.append(TEXT_59);
    stringBuffer.append(outgoingConnName);
    stringBuffer.append(TEXT_60);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_61);
    }
    stringBuffer.append(TEXT_62);
                              
                            if(javaType == JavaTypesManager.STRING || javaType == JavaTypesManager.OBJECT)
                            {
                        
    stringBuffer.append(TEXT_63);
    stringBuffer.append(outgoingConnName);
    stringBuffer.append(TEXT_64);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_65);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_66);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_67);
    
                            }
                            else if(javaType == JavaTypesManager.DATE)
                            {
                                String patternValue = column.getPattern() == null || column.getPattern().trim().length() == 0 ? null : column.getPattern();
                        
    stringBuffer.append(TEXT_68);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_69);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_70);
    stringBuffer.append(TEXT_71);
    stringBuffer.append(outgoingConnName);
    stringBuffer.append(TEXT_72);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_73);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_74);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_75);
    stringBuffer.append(TEXT_76);
    stringBuffer.append(outgoingConnName);
    stringBuffer.append(TEXT_77);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_78);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_79);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_80);
    stringBuffer.append(patternValue);
    stringBuffer.append(TEXT_81);
    
                            }
                            else if(javaType == JavaTypesManager.BYTE_ARRAY)
                            {
                        
    stringBuffer.append(TEXT_82);
    stringBuffer.append(outgoingConnName);
    stringBuffer.append(TEXT_83);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_84);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_85);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_86);
    
                            }
                            else
                            {
                        
    stringBuffer.append(TEXT_87);
    stringBuffer.append(TEXT_88);
    stringBuffer.append(outgoingConnName);
    stringBuffer.append(TEXT_89);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_90);
    stringBuffer.append( typeToGenerate );
    stringBuffer.append(TEXT_91);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_92);
    stringBuffer.append(column.getLabel());
    stringBuffer.append(TEXT_93);
    
                            } 
                        
    stringBuffer.append(TEXT_94);
    if(column.isNullable()){
    stringBuffer.append(TEXT_95);
    }
    
                } // if
            } // for
    
    stringBuffer.append(TEXT_96);
    stringBuffer.append(TEXT_97);
    return stringBuffer.toString();
  }
}
