package org.talend.designer.codegen.translators.processing.datamapper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.talend.transform.components.spark.TDMExternalNodeProvider;
import org.talend.core.model.metadata.IMetadataColumn;
import org.talend.core.model.metadata.IMetadataTable;
import org.talend.core.model.metadata.MetadataTalendType;
import org.talend.core.model.metadata.types.JavaType;
import org.talend.core.model.metadata.types.JavaTypesManager;
import org.talend.core.model.process.EConnectionType;
import org.talend.core.model.process.ElementParameterParser;
import org.talend.core.model.process.IBigDataNode;
import org.talend.core.model.process.IConnection;
import org.talend.core.model.process.IConnectionCategory;
import org.talend.core.model.process.INode;
import org.talend.core.model.utils.NodeUtil;
import org.talend.core.model.utils.TalendTextUtils;
import org.talend.designer.common.BigDataCodeGeneratorArgument;
import org.talend.transform.dataflow.common.THMapRecordParms;
import org.talend.transform.components.spark.TDMExternalNodeProvider;
import org.talend.transform.components.spark.thmaprecord.THMapRecordComponent;
import org.talend.transform.components.spark.codegen.THMapUtil;
import org.talend.transform.components.spark.utils.TDMUtils;

public class THMapRecordSparkstreamingconfigJava
{
  protected static String nl;
  public static synchronized THMapRecordSparkstreamingconfigJava create(String lineSeparator)
  {
    nl = lineSeparator;
    THMapRecordSparkstreamingconfigJava result = new THMapRecordSparkstreamingconfigJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "// start code generation";
  protected final String TEXT_2 = NL + "\t// HDFS storage setup" + NL + "    java.net.URI currentURI_";
  protected final String TEXT_3 = "_config = FileSystem.getDefaultUri(ctx.sparkContext().hadoopConfiguration());" + NL + "    FileSystem.setDefaultUri(ctx.sparkContext().hadoopConfiguration(), new java.net.URI(";
  protected final String TEXT_4 = "));" + NL + "    fs = FileSystem.get(ctx.sparkContext().hadoopConfiguration());";
  protected final String TEXT_5 = NL + NL + NL + NL + NL + NL + "// Set up a Spark DataFlow." + NL + "org.talend.bigdata.dataflow.spark.streaming.SparkStreamingDataFlowContext ";
  protected final String TEXT_6 = "_sdfContext = new org.talend.bigdata.dataflow.spark.streaming.SparkStreamingDataFlowContext.Builder().withStreamingContext(ctx).build();" + NL + "org.talend.bigdata.dataflow.spark.streaming.SparkStreamingDataFlow ";
  protected final String TEXT_7 = "_sdf = new org.talend.bigdata.dataflow.spark.streaming.SparkStreamingDataFlow(";
  protected final String TEXT_8 = "_sdfContext);" + NL + "" + NL + "// Set up and run the component." + NL + "org.talend.transform.dataflow.thmaprecord.THMapRecord ";
  protected final String TEXT_9 = "_aThmap                = new org.talend.transform.dataflow.thmaprecord.THMapRecord();" + NL + "org.talend.transform.dataflow.thmaprecord.THMapRecordSpecBuilder ";
  protected final String TEXT_10 = "_thmapSB    = ";
  protected final String TEXT_11 = "_aThmap.createSpecBuilder();";
  protected final String TEXT_12 = NL;
  protected final String TEXT_13 = "_thmapSB.setId(\"";
  protected final String TEXT_14 = "\");" + NL;
  protected final String TEXT_15 = NL;
  protected final String TEXT_16 = "_thmapSB.setMap(\"";
  protected final String TEXT_17 = "\");                     ";
  protected final String TEXT_18 = NL;
  protected final String TEXT_19 = "_thmapSB.setStorageLevel(\"";
  protected final String TEXT_20 = "\");";
  protected final String TEXT_21 = NL;
  protected final String TEXT_22 = "_thmapSB.setDieOnError(\"";
  protected final String TEXT_23 = "\");";
  protected final String TEXT_24 = NL;
  protected final String TEXT_25 = "_thmapSB.setDisableInputAsPayload(\"";
  protected final String TEXT_26 = "\");";
  protected final String TEXT_27 = NL;
  protected final String TEXT_28 = "_thmapSB.setDisableOutputAsPayload(\"";
  protected final String TEXT_29 = "\");";
  protected final String TEXT_30 = NL;
  protected final String TEXT_31 = "_thmapSB.setMapRequiresMultipleResults(";
  protected final String TEXT_32 = ");" + NL + "" + NL + "// INPUT RDD";
  protected final String TEXT_33 = NL + "    ";
  protected final String TEXT_34 = "_sdf.put(\"";
  protected final String TEXT_35 = "\", rdd_";
  protected final String TEXT_36 = ");";
  protected final String TEXT_37 = NL + "    ";
  protected final String TEXT_38 = "_thmapSB.addInput(\"";
  protected final String TEXT_39 = "\", rdd_";
  protected final String TEXT_40 = ");";
  protected final String TEXT_41 = NL + NL + "// OUTPUT SCHEMA";
  protected final String TEXT_42 = NL + "    ";
  protected final String TEXT_43 = "_thmapSB.addOutput(" + NL + "        \"";
  protected final String TEXT_44 = "\", " + NL + "        \"";
  protected final String TEXT_45 = "\",";
  protected final String TEXT_46 = NL + "        ";
  protected final String TEXT_47 = ".getClassSchema()" + NL + "        );";
  protected final String TEXT_48 = NL;
  protected final String TEXT_49 = " ";
  protected final String TEXT_50 = NL;
  protected final String TEXT_51 = ".synchronizeContext();" + NL + "    ";
  protected final String TEXT_52 = NL;
  protected final String TEXT_53 = "_aThmap.createDataFlowBuilder(";
  protected final String TEXT_54 = "_thmapSB.build()).build(";
  protected final String TEXT_55 = "_sdf);" + NL + "" + NL + "" + NL + "// OUTPUT RDD";
  protected final String TEXT_56 = NL + "    ";
  protected final String TEXT_57 = "<NullWritable, ";
  protected final String TEXT_58 = "> " + NL + "    rdd_";
  protected final String TEXT_59 = " = ";
  protected final String TEXT_60 = NL + "    ";
  protected final String TEXT_61 = "_sdf.";
  protected final String TEXT_62 = "(\"";
  protected final String TEXT_63 = "\");";
  protected final String TEXT_64 = NL;

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    stringBuffer.append(TEXT_1);
    
// Parse the inputs to this javajet generator.
final BigDataCodeGeneratorArgument gen_code_gen_arg = (BigDataCodeGeneratorArgument) argument;
final INode gen_node                                = (INode) gen_code_gen_arg.getArgument();
final String gen_cid                                = gen_node.getUniqueName();
final THMapUtil gen_thmap_util                      = new THMapUtil(gen_node);

String gen_storage  = ElementParameterParser.getValue(gen_node,"__STORAGE_LEVEL__");
String gen_input    = ElementParameterParser.getValue(gen_node,"__INPUT__");
String gen_output   = ElementParameterParser.getValue(gen_node,"__OUTPUT__");
String gen_tdm_map  = TDMUtils.getMapProjectPath(gen_node, ElementParameterParser.getValue(gen_node,"__MAP__"), false);
String gen_dieOnError = ElementParameterParser.getValue(gen_node,"__DIE_ON_ERROR__");
String gen_disableInputAsPayload = ElementParameterParser.getValue(gen_node,"__DISABLE_INPUT_AS_PAYLOAD__");
String gen_disableOutputAsPayload = ElementParameterParser.getValue(gen_node,"__DISABLE_OUTPUT_AS_PAYLOAD__");

TDMExternalNodeProvider gen_node_provider   = (TDMExternalNodeProvider) gen_node.getExternalNode();
THMapRecordComponent gen_tdm_component  	= (THMapRecordComponent) gen_node_provider.getCurrentTDMNode();
THMapRecordParms gen_tdm_params             = gen_tdm_component.getAndValidate(ElementParameterParser.getValue(gen_node,"__PARAMS__"));
boolean gen_requiresMultipleResults = gen_tdm_component.requiresMultipleResults();

String gen_spark_dataset_class  = gen_thmap_util.getSparkDatasetClassName();
String gen_spark_dataset_method = gen_thmap_util.getSparkGetDatasetMethodName();

String gen_storage_uri_prefix = "";
// Used for Spark only for now.
if("true".equals(ElementParameterParser.getValue(gen_node, "__DEFINE_STORAGE_CONFIGURATION__"))) { // use configuration component
    gen_storage_uri_prefix = org.talend.designer.spark.generator.storage.SparkStorageUtils.getURIPrefix(gen_node);
    gen_input = gen_storage_uri_prefix + " + " + gen_input;
    gen_output = gen_storage_uri_prefix + " + " + gen_output;
}
if(gen_storage_uri_prefix.length() > 0) {

    stringBuffer.append(TEXT_2);
    stringBuffer.append(gen_cid);
    stringBuffer.append(TEXT_3);
    stringBuffer.append(gen_storage_uri_prefix);
    stringBuffer.append(TEXT_4);
    
} // end if

    stringBuffer.append(TEXT_5);
    stringBuffer.append(gen_cid);
    stringBuffer.append(TEXT_6);
    stringBuffer.append(gen_cid);
    stringBuffer.append(TEXT_7);
    stringBuffer.append(gen_cid);
    stringBuffer.append(TEXT_8);
    stringBuffer.append(gen_cid);
    stringBuffer.append(TEXT_9);
    stringBuffer.append(gen_cid);
    stringBuffer.append(TEXT_10);
    stringBuffer.append(gen_cid);
    stringBuffer.append(TEXT_11);
    stringBuffer.append(TEXT_12);
    stringBuffer.append(gen_cid);
    stringBuffer.append(TEXT_13);
    stringBuffer.append(gen_cid);
    stringBuffer.append(TEXT_14);
    stringBuffer.append(TEXT_15);
    stringBuffer.append(gen_cid);
    stringBuffer.append(TEXT_16);
    stringBuffer.append(gen_tdm_map);
    stringBuffer.append(TEXT_17);
    stringBuffer.append(TEXT_18);
    stringBuffer.append(gen_cid);
    stringBuffer.append(TEXT_19);
    stringBuffer.append(gen_storage);
    stringBuffer.append(TEXT_20);
    stringBuffer.append(TEXT_21);
    stringBuffer.append(gen_cid);
    stringBuffer.append(TEXT_22);
    stringBuffer.append(gen_dieOnError);
    stringBuffer.append(TEXT_23);
    stringBuffer.append(TEXT_24);
    stringBuffer.append(gen_cid);
    stringBuffer.append(TEXT_25);
    stringBuffer.append(gen_disableInputAsPayload);
    stringBuffer.append(TEXT_26);
    stringBuffer.append(TEXT_27);
    stringBuffer.append(gen_cid);
    stringBuffer.append(TEXT_28);
    stringBuffer.append(gen_disableOutputAsPayload);
    stringBuffer.append(TEXT_29);
    stringBuffer.append(TEXT_30);
    stringBuffer.append(gen_cid);
    stringBuffer.append(TEXT_31);
    stringBuffer.append(gen_requiresMultipleResults);
    stringBuffer.append(TEXT_32);
    
// [DEB] for
    for (org.talend.core.model.process.IConnection gen_connection : gen_thmap_util.getIncomingConnections()) {      // set DStream to each input connection

    stringBuffer.append(TEXT_33);
    stringBuffer.append(gen_cid);
    stringBuffer.append(TEXT_34);
    stringBuffer.append(gen_connection.getName());
    stringBuffer.append(TEXT_35);
    stringBuffer.append(gen_connection.getName());
    stringBuffer.append(TEXT_36);
    stringBuffer.append(TEXT_37);
    stringBuffer.append(gen_cid);
    stringBuffer.append(TEXT_38);
    stringBuffer.append(gen_connection.getName());
    stringBuffer.append(TEXT_39);
    stringBuffer.append(gen_connection.getName());
    stringBuffer.append(TEXT_40);
    
    }
// [END] for

    stringBuffer.append(TEXT_41);
    
// [DEB] for
    for (org.talend.core.model.process.IConnection gen_connection : gen_thmap_util.getOutgoingConnections()) {      // set RDD to each output connection
      IConnection thMapRecordConnection = gen_thmap_util.getTdmOutputConnection(gen_connection);
      IConnection tdmOutputConnection = thMapRecordConnection!=null?thMapRecordConnection:gen_connection;

    stringBuffer.append(TEXT_42);
    stringBuffer.append(gen_cid);
    stringBuffer.append(TEXT_43);
    stringBuffer.append(tdmOutputConnection.getName());
    stringBuffer.append(TEXT_44);
    stringBuffer.append(gen_tdm_params.getConnectionId2ElementPath().get(tdmOutputConnection.getName()));
    stringBuffer.append(TEXT_45);
    stringBuffer.append(TEXT_46);
    stringBuffer.append(gen_code_gen_arg.getRecordStructName(gen_connection.getName()));
    stringBuffer.append(TEXT_47);
    
    }
// [END] for

    stringBuffer.append(TEXT_48);
     
    String localContext = "context";

    stringBuffer.append(TEXT_49);
    stringBuffer.append(TEXT_50);
    stringBuffer.append(localContext);
    stringBuffer.append(TEXT_51);
    stringBuffer.append(TEXT_52);
    stringBuffer.append(gen_cid);
    stringBuffer.append(TEXT_53);
    stringBuffer.append(gen_cid);
    stringBuffer.append(TEXT_54);
    stringBuffer.append(gen_cid);
    stringBuffer.append(TEXT_55);
    
// [DEB] for
    // For every gen_output gen_connection, initialize the gen_output RDD.
    for (org.talend.core.model.process.IConnection gen_connection : gen_thmap_util.getOutgoingConnections()) {  // get RDD to each output connection
     IConnection thMapRecordConnection = gen_thmap_util.getTdmOutputConnection(gen_connection);
     IConnection tdmOutputConnection = thMapRecordConnection!=null?thMapRecordConnection:gen_connection;

    stringBuffer.append(TEXT_56);
    stringBuffer.append(gen_spark_dataset_class);
    stringBuffer.append(TEXT_57);
    stringBuffer.append(gen_code_gen_arg.getRecordStructName(gen_connection.getName()));
    stringBuffer.append(TEXT_58);
    stringBuffer.append(gen_connection.getName());
    stringBuffer.append(TEXT_59);
    stringBuffer.append(TEXT_60);
    stringBuffer.append(gen_cid);
    stringBuffer.append(TEXT_61);
    stringBuffer.append(gen_spark_dataset_method);
    stringBuffer.append(TEXT_62);
    stringBuffer.append(tdmOutputConnection.getName());
    stringBuffer.append(TEXT_63);
    
    }
// [END] for

    stringBuffer.append(TEXT_64);
    return stringBuffer.toString();
  }
}
