package org.talend.designer.codegen.translators.messaging.pubsub;

import java.util.Map.Entry;
import org.talend.core.model.process.ElementParameterParser;
import org.talend.core.model.process.IConnection;
import org.talend.core.model.process.INode;
import org.talend.designer.common.BigDataCodeGeneratorArgument;
import org.talend.designer.common.tgooglecloudconfiguration.TGoogleCloudConfigurationUtil;

public class TPubSubInputSparkstreamingconfigJava
{
  protected static String nl;
  public static synchronized TPubSubInputSparkstreamingconfigJava create(String lineSeparator)
  {
    nl = lineSeparator;
    TPubSubInputSparkstreamingconfigJava result = new TPubSubInputSparkstreamingconfigJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = NL + "org.apache.spark.streaming.pubsub.SparkGCPCredentials.Builder builder =" + NL + "        new org.apache.spark.streaming.pubsub.SparkGCPCredentials.Builder();" + NL;
  protected final String TEXT_2 = NL + "    builder = builder.p12ServiceAccount(";
  protected final String TEXT_3 = ",";
  protected final String TEXT_4 = NL + "            ";
  protected final String TEXT_5 = ");";
  protected final String TEXT_6 = NL + "    builder = builder.jsonServiceAccount(";
  protected final String TEXT_7 = ");";
  protected final String TEXT_8 = NL + NL + "org.apache.spark.streaming.api.java.JavaReceiverInputDStream<org.apache.spark.streaming.pubsub.SparkPubsubMessage> dStream_";
  protected final String TEXT_9 = " = null;" + NL;
  protected final String TEXT_10 = NL + "\t// to be replaced by constructor that uses topic" + NL + "             dStream_";
  protected final String TEXT_11 = " =  org.apache.spark.streaming.pubsub.PubsubUtils.createStream(" + NL + "                ctx, ";
  protected final String TEXT_12 = ", ";
  protected final String TEXT_13 = ", ";
  protected final String TEXT_14 = "," + NL + "                builder.build()," + NL + "                org.apache.spark.storage.StorageLevel.";
  protected final String TEXT_15 = "());";
  protected final String TEXT_16 = NL + "\t   dStream_";
  protected final String TEXT_17 = " =  org.apache.spark.streaming.pubsub.PubsubUtils.createStream(" + NL + "                ctx, ";
  protected final String TEXT_18 = ", ";
  protected final String TEXT_19 = "," + NL + "                builder.build()," + NL + "                org.apache.spark.storage.StorageLevel.";
  protected final String TEXT_20 = "());";
  protected final String TEXT_21 = NL + NL + "org.apache.spark.streaming.api.java.JavaDStream<";
  protected final String TEXT_22 = "> rdd_";
  protected final String TEXT_23 = " = dStream_";
  protected final String TEXT_24 = ".map(new ";
  protected final String TEXT_25 = "_MapToOutputStruct());" + NL + NL + NL + NL + NL + NL + NL;
  protected final String TEXT_26 = NL;

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    
BigDataCodeGeneratorArgument codeGenArgument = (BigDataCodeGeneratorArgument) argument;
INode node = (INode)codeGenArgument.getArgument();
final TGoogleCloudConfigurationUtil gcpConfigUtil = new TGoogleCloudConfigurationUtil(node);
if (gcpConfigUtil.getValidateError() != null) {
    // Cause the job compilation to explicitly fail if there is a problem.
    return "throw new JobConfigurationError(\"" + gcpConfigUtil.getValidateError() +"\");";
}

String cid = node.getUniqueName();
IConnection conn = node.getOutgoingConnections().get(0);
String connectionTypeName = codeGenArgument.getRecordStructName(conn);
String connName = conn.getUniqueName();

String topicName = ElementParameterParser.getValue(node, "__PUBSUB_TOPIC__");
String subscriptionName = ElementParameterParser.getValue(node, "__PUBSUB_SUBSCRIPTION_NAME__");

String storageLevel = ElementParameterParser.getValue(node, "__STORAGELEVEL__");

boolean createSubscriptionFromTopic = true;
if (topicName.replaceAll("\"", "").length() == 0) {
		createSubscriptionFromTopic = false;
}
if (subscriptionName.replaceAll("\"", "").length() == 0) {
	  	subscriptionName = "\"subscription_" + java.util.UUID.randomUUID()+"\"";
}


    stringBuffer.append(TEXT_1);
    
if (gcpConfigUtil.isSpecifyingCredentialsP12()) {
    
    stringBuffer.append(TEXT_2);
    stringBuffer.append(gcpConfigUtil.getGcpAuthenticationPath());
    stringBuffer.append(TEXT_3);
    stringBuffer.append(TEXT_4);
    stringBuffer.append(gcpConfigUtil.getGcpServiceAccountId());
    stringBuffer.append(TEXT_5);
    
} else if (gcpConfigUtil.isSpecifyingCredentialsJson()) {
    
    stringBuffer.append(TEXT_6);
    stringBuffer.append(gcpConfigUtil.getGcpAuthenticationPath());
    stringBuffer.append(TEXT_7);
    
}

    stringBuffer.append(TEXT_8);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_9);
    
 if (createSubscriptionFromTopic) {

    stringBuffer.append(TEXT_10);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_11);
    stringBuffer.append(gcpConfigUtil.getProjectName());
    stringBuffer.append(TEXT_12);
    stringBuffer.append(topicName);
    stringBuffer.append(TEXT_13);
    stringBuffer.append(subscriptionName);
    stringBuffer.append(TEXT_14);
    stringBuffer.append(storageLevel);
    stringBuffer.append(TEXT_15);
     
 } else {
 
    stringBuffer.append(TEXT_16);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_17);
    stringBuffer.append(gcpConfigUtil.getProjectName());
    stringBuffer.append(TEXT_18);
    stringBuffer.append(subscriptionName);
    stringBuffer.append(TEXT_19);
    stringBuffer.append(storageLevel);
    stringBuffer.append(TEXT_20);
    
}

    stringBuffer.append(TEXT_21);
    stringBuffer.append(connectionTypeName);
    stringBuffer.append(TEXT_22);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_23);
    stringBuffer.append(connName);
    stringBuffer.append(TEXT_24);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_25);
    stringBuffer.append(TEXT_26);
    return stringBuffer.toString();
  }
}
