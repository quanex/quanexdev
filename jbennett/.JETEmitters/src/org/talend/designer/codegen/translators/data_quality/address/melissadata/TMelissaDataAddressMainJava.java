package org.talend.designer.codegen.translators.data_quality.address.melissadata;

import org.talend.core.model.process.INode;
import org.talend.core.model.metadata.IMetadataTable;
import org.talend.designer.codegen.config.CodeGeneratorArgument;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import org.talend.core.model.process.ElementParameterParser;
import org.talend.core.model.process.IConnection;
import org.talend.core.model.metadata.IMetadataColumn;
import org.talend.core.model.process.IConnectionCategory;

public class TMelissaDataAddressMainJava
{
  protected static String nl;
  public static synchronized TMelissaDataAddressMainJava create(String lineSeparator)
  {
    nl = lineSeparator;
    TMelissaDataAddressMainJava result = new TMelissaDataAddressMainJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "\t\t\t\t\tao_";
  protected final String TEXT_2 = ".ClearProperties();            " + NL + "\t\t\t\t\t" + NL + "                    if(useRightfielder";
  protected final String TEXT_3 = " && rightfielder_exists_";
  protected final String TEXT_4 = "){";
  protected final String TEXT_5 = "                    " + NL + "                        rightfielder_";
  protected final String TEXT_6 = ".Parse(";
  protected final String TEXT_7 = ".";
  protected final String TEXT_8 = " != null ? ";
  protected final String TEXT_9 = ".";
  protected final String TEXT_10 = ":\"\");" + NL + "                        ao_";
  protected final String TEXT_11 = ".SetCompany(rightfielder_";
  protected final String TEXT_12 = ".GetCompany());" + NL + "                        ao_";
  protected final String TEXT_13 = ".SetAddress(rightfielder_";
  protected final String TEXT_14 = ".GetAddress());" + NL + "                        ao_";
  protected final String TEXT_15 = ".SetAddress2(rightfielder_";
  protected final String TEXT_16 = ".GetAddress2());" + NL + "                        ao_";
  protected final String TEXT_17 = ".SetCity(rightfielder_";
  protected final String TEXT_18 = ".GetCity());" + NL + "                        ao_";
  protected final String TEXT_19 = ".SetState(rightfielder_";
  protected final String TEXT_20 = ".GetState());" + NL + "                        ao_";
  protected final String TEXT_21 = ".SetZip(rightfielder_";
  protected final String TEXT_22 = ".GetPostalCode());" + NL + "                    } else {";
  protected final String TEXT_23 = "                " + NL + "                          ao_";
  protected final String TEXT_24 = ".Set";
  protected final String TEXT_25 = "(";
  protected final String TEXT_26 = ".";
  protected final String TEXT_27 = " != null ? ";
  protected final String TEXT_28 = ".";
  protected final String TEXT_29 = ":\"\");";
  protected final String TEXT_30 = NL + "                    }\t\t\t\t\t" + NL + "\t\t\t\t\t" + NL + "\t\t\t\t\tao_";
  protected final String TEXT_31 = ".VerifyAddress();";
  protected final String TEXT_32 = NL + "                                                ";
  protected final String TEXT_33 = ".";
  protected final String TEXT_34 = "=";
  protected final String TEXT_35 = ".";
  protected final String TEXT_36 = ";";
  protected final String TEXT_37 = NL + "                                        if(geopoint_owned__";
  protected final String TEXT_38 = " && geocoder_exists_";
  protected final String TEXT_39 = ") { " + NL + "                                            geocoder_";
  protected final String TEXT_40 = ".GeoPoint(ao_";
  protected final String TEXT_41 = ".GetAddressKey(),\"\",\"\");" + NL + "                                        } else if(geocode_owned__";
  protected final String TEXT_42 = " && geocoder_exists_";
  protected final String TEXT_43 = ") {" + NL + "                                            geocoder_";
  protected final String TEXT_44 = ".GeoCode(ao_";
  protected final String TEXT_45 = ".GetZip(),ao_";
  protected final String TEXT_46 = ".GetPlus4());" + NL + "                                        }" + NL;
  protected final String TEXT_47 = NL + "                                               if((geopoint_owned__";
  protected final String TEXT_48 = " && geocoder_exists_";
  protected final String TEXT_49 = ") || (geocode_owned__";
  protected final String TEXT_50 = " && geocoder_exists_";
  protected final String TEXT_51 = ")) {";
  protected final String TEXT_52 = NL + "                                                  ";
  protected final String TEXT_53 = ".";
  protected final String TEXT_54 = " = geocoder_";
  protected final String TEXT_55 = ".";
  protected final String TEXT_56 = "() + \"\";" + NL + "                                               } else {";
  protected final String TEXT_57 = NL + "                                                  ";
  protected final String TEXT_58 = ".";
  protected final String TEXT_59 = " = \"\";" + NL + "                                               }";
  protected final String TEXT_60 = "                                         ";
  protected final String TEXT_61 = NL + "                                              ";
  protected final String TEXT_62 = ".";
  protected final String TEXT_63 = " = ao_";
  protected final String TEXT_64 = ".";
  protected final String TEXT_65 = "() + \"\";";
  protected final String TEXT_66 = "                                        " + NL + "\t\t                                ";
  protected final String TEXT_67 = ".COMPANY_STANDARDIZED = ao_";
  protected final String TEXT_68 = ".GetCompany();" + NL + "\t\t\t\t\t\t\t\t\t\t";
  protected final String TEXT_69 = ".ADDRESSLINE1_STANDARDIZED = ao_";
  protected final String TEXT_70 = ".GetAddress();" + NL + "\t\t                                ";
  protected final String TEXT_71 = ".ADDRESSLINE2_STANDARDIZED = ao_";
  protected final String TEXT_72 = ".GetAddress2()  + \"  \" + ao_";
  protected final String TEXT_73 = ".GetParsedGarbage();" + NL + "\t\t\t\t\t\t\t\t\t\t";
  protected final String TEXT_74 = ".CITY_STANDARDIZED = ao_";
  protected final String TEXT_75 = ".GetCity();" + NL + "\t\t\t\t\t\t\t\t\t\t";
  protected final String TEXT_76 = ".STATE_STANDARDIZED = ao_";
  protected final String TEXT_77 = ".GetState();" + NL + "\t\t\t\t\t\t\t\t\t\tif(ao_";
  protected final String TEXT_78 = ".GetPlus4()!=null && ao_";
  protected final String TEXT_79 = ".GetPlus4().trim().length() >0) {\t\t\t" + NL + "\t\t\t\t\t\t\t\t\t\t\t";
  protected final String TEXT_80 = ".POSTAL_STANDARDIZED = ao_";
  protected final String TEXT_81 = ".GetZip() + \"-\"  + ao_";
  protected final String TEXT_82 = ".GetPlus4();" + NL + "\t\t\t\t\t\t\t\t\t\t}else {\t\t\t" + NL + "\t\t\t\t\t\t\t\t\t\t\t";
  protected final String TEXT_83 = ".POSTAL_STANDARDIZED = ao_";
  protected final String TEXT_84 = ".GetZip();" + NL + "\t\t\t\t\t\t\t\t\t\t}" + NL + "\t\t\t\t\t\t\t\t\t\t";
  protected final String TEXT_85 = ".COUNTRY_STANDARDIZED = ao_";
  protected final String TEXT_86 = ".GetCountryCode();" + NL + "\t\t\t\t\t\t\t\t\t\t";
  protected final String TEXT_87 = ".RESULTS_CODE = ao_";
  protected final String TEXT_88 = ".GetResults(); " + NL + "\t\t\t\t\t\t\t\t\t    ";
  protected final String TEXT_89 = NL;

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    
	CodeGeneratorArgument codeGenArgument = (CodeGeneratorArgument) argument;
	INode node = (INode)codeGenArgument.getArgument();
	String cid = node.getUniqueName();

	List<Map<String, String>> input_address = (List<Map<String, String>>)ElementParameterParser.getObjectValue(node, "__INPUT_ADDRESS__");
    List<Map<String, String>> output_address = (List<Map<String, String>>)ElementParameterParser.getObjectValue(node, "__OUTPUT_ADDRESS__");
    List<String> geoOutputColumns = java.util.Arrays.asList("GetLongitude","GetLatitude","GetResults","GetCensusTract","GetCensusBlock","GetPlaceName","GetPlaceCode","GetCBSACode","GetCBSATitle","GetCBSALevel","GetCBSADivisionCode","GetCBSADivisionTitle","GetCBSADivisionLevel");

    	
	List<IMetadataTable> metadatas = node.getMetadataList();
	if ((metadatas!=null)&&(metadatas.size()>0)) {
		IMetadataTable metadata = metadatas.get(0);
		if (metadata!=null) {
			List< ? extends IConnection> conns = node.getIncomingConnections();
			for (IConnection conn : node.getIncomingConnections()) {
				if(conn!=null && conn.getLineStyle().hasConnectionCategory(IConnectionCategory.DATA)){
				
				    List<IMetadataColumn> columnsInput=conn.getMetadataTable().getListColumns();

    stringBuffer.append(TEXT_1);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_2);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_3);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_4);
    
                        String inputAddressValue = input_address.get(0).get("INPUT_COLUMN");

    stringBuffer.append(TEXT_5);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_6);
    stringBuffer.append(conn.getName() );
    stringBuffer.append(TEXT_7);
    stringBuffer.append( inputAddressValue );
    stringBuffer.append(TEXT_8);
    stringBuffer.append(conn.getName() );
    stringBuffer.append(TEXT_9);
    stringBuffer.append( inputAddressValue );
    stringBuffer.append(TEXT_10);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_11);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_12);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_13);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_14);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_15);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_16);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_17);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_18);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_19);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_20);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_21);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_22);
                      
                       for (Map<String, String> input : input_address) {
                          String input_address_field = input.get("ADDRESS_FIELD");
                          String input_column = input.get("INPUT_COLUMN");

    stringBuffer.append(TEXT_23);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_24);
    stringBuffer.append( input_address_field );
    stringBuffer.append(TEXT_25);
    stringBuffer.append(conn.getName() );
    stringBuffer.append(TEXT_26);
    stringBuffer.append( input_column );
    stringBuffer.append(TEXT_27);
    stringBuffer.append(conn.getName() );
    stringBuffer.append(TEXT_28);
    stringBuffer.append( input_column );
    stringBuffer.append(TEXT_29);
                    
                      }

    stringBuffer.append(TEXT_30);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_31);
    
					List< ? extends IConnection> connsout = node.getOutgoingConnections(); 
	                if (connsout!=null) {
    	            	List<IMetadataColumn> columnsout = metadata.getListColumns();
            	        for (int i=0;i<connsout.size();i++) {
            	        	IConnection connout = connsout.get(i);
                    	    if(connout.getLineStyle().hasConnectionCategory(IConnectionCategory.DATA)) {
                                       // set right column values from the left column which name is the same
                                       for (int j = 0; j < columnsout.size(); j++) {
                                          IMetadataColumn columnOutput=columnsout.get(j);
                                          for (int h = 0; h < columnsInput.size(); h++) {
                                             IMetadataColumn columnInput=columnsInput.get(h);
                                             if (columnOutput.getLabel().equals(columnInput.getLabel())) {
                                                String label=columnOutput.getLabel();

    stringBuffer.append(TEXT_32);
    stringBuffer.append(connout.getName() );
    stringBuffer.append(TEXT_33);
    stringBuffer.append(label);
    stringBuffer.append(TEXT_34);
    stringBuffer.append(conn.getName() );
    stringBuffer.append(TEXT_35);
    stringBuffer.append(label);
    stringBuffer.append(TEXT_36);
    
                                                break;
                                            }
                                          }
                                        }

    stringBuffer.append(TEXT_37);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_38);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_39);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_40);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_41);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_42);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_43);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_44);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_45);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_46);
    
                                       for (Map<String, String> output : output_address) {
                                           String output_address_field = output.get("ADDRESS_FIELD");
                                           if("GetAreaCode".equals(output_address_field)) {
                                               // GetAreaCode the same as before we output is ""
                                               continue;
                                           }
                                           
                                           String output_column = output.get("OUTPUT_COLUMN");
                                           
                                           if(geoOutputColumns.contains(output_address_field)) {

    stringBuffer.append(TEXT_47);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_48);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_49);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_50);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_51);
    stringBuffer.append(TEXT_52);
    stringBuffer.append(connout.getName() );
    stringBuffer.append(TEXT_53);
    stringBuffer.append( output_column );
    stringBuffer.append(TEXT_54);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_55);
    stringBuffer.append( output_address_field );
    stringBuffer.append(TEXT_56);
    stringBuffer.append(TEXT_57);
    stringBuffer.append(connout.getName() );
    stringBuffer.append(TEXT_58);
    stringBuffer.append( output_column );
    stringBuffer.append(TEXT_59);
    
                                           } else {

    stringBuffer.append(TEXT_60);
    stringBuffer.append(TEXT_61);
    stringBuffer.append(connout.getName() );
    stringBuffer.append(TEXT_62);
    stringBuffer.append( output_column );
    stringBuffer.append(TEXT_63);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_64);
    stringBuffer.append( output_address_field );
    stringBuffer.append(TEXT_65);
    
                                           }
                                        }

    stringBuffer.append(TEXT_66);
    stringBuffer.append(connout.getName() );
    stringBuffer.append(TEXT_67);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_68);
    stringBuffer.append(connout.getName() );
    stringBuffer.append(TEXT_69);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_70);
    stringBuffer.append(connout.getName() );
    stringBuffer.append(TEXT_71);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_72);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_73);
    stringBuffer.append(connout.getName() );
    stringBuffer.append(TEXT_74);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_75);
    stringBuffer.append(connout.getName() );
    stringBuffer.append(TEXT_76);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_77);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_78);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_79);
    stringBuffer.append(connout.getName() );
    stringBuffer.append(TEXT_80);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_81);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_82);
    stringBuffer.append(connout.getName() );
    stringBuffer.append(TEXT_83);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_84);
    stringBuffer.append(connout.getName() );
    stringBuffer.append(TEXT_85);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_86);
    stringBuffer.append(connout.getName() );
    stringBuffer.append(TEXT_87);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_88);
    							}
						}
					}
				}
			}
		}
	}

    stringBuffer.append(TEXT_89);
    return stringBuffer.toString();
  }
}
