package org.talend.designer.codegen.translators.naturallanguageprocessing;

import org.talend.core.model.process.INode;
import org.talend.core.model.process.ElementParameterParser;
import org.talend.core.model.metadata.IMetadataTable;
import org.talend.core.model.metadata.IMetadataColumn;
import org.talend.core.model.process.IConnection;
import org.talend.core.model.process.EConnectionType;
import org.talend.designer.common.BigDataCodeGeneratorArgument;
import org.talend.core.model.metadata.types.JavaTypesManager;
import org.talend.core.model.process.IConnectionCategory;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.ArrayList;
import java.lang.StringBuilder;

public class TNLPPreprocessingSparkcodeJava
{
  protected static String nl;
  public static synchronized TNLPPreprocessingSparkcodeJava create(String lineSeparator)
  {
    nl = lineSeparator;
    TNLPPreprocessingSparkcodeJava result = new TNLPPreprocessingSparkcodeJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "static final int outSchemaSize_";
  protected final String TEXT_2 = " = ";
  protected final String TEXT_3 = ";" + NL + "public static class ";
  protected final String TEXT_4 = "Preprocessing implements org.apache.spark.api.java.function.Function<";
  protected final String TEXT_5 = ", ";
  protected final String TEXT_6 = ">{" + NL + "    private String col;" + NL + "    private org.talend.dataquality.nlp.toolkit.AbstractToolkit nlp;" + NL + "    private Boolean cleanTags;" + NL + "    public ";
  protected final String TEXT_7 = "Preprocessing(String col, org.talend.dataquality.nlp.toolkit.AbstractToolkit nlp, Boolean cleanTags){" + NL + "        this.col = col;" + NL + "        this.nlp = nlp;" + NL + "        this.cleanTags = cleanTags;" + NL + "    }" + NL + "" + NL + "    @Override" + NL + "    public ";
  protected final String TEXT_8 = " call(";
  protected final String TEXT_9 = " row){";
  protected final String TEXT_10 = NL + "        ";
  protected final String TEXT_11 = " tmpStruct = new ";
  protected final String TEXT_12 = "();" + NL;
  protected final String TEXT_13 = NL + "            tmpStruct.put(";
  protected final String TEXT_14 = ", row.get(\"";
  protected final String TEXT_15 = "\"));";
  protected final String TEXT_16 = NL + "        String text = cleanTags ? org.jsoup.Jsoup.parse((String) row.get(col)).text() : (String) row.get(col);" + NL + "        tmpStruct.put(outSchemaSize_";
  protected final String TEXT_17 = "-1, org.apache.commons.lang3.StringUtils.join(nlp.tokenization(nlp.sentenceSplit(text)),'\\t'));" + NL + "        return tmpStruct;" + NL + "    }" + NL + "}";

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
     
final BigDataCodeGeneratorArgument codeGenArgument = (BigDataCodeGeneratorArgument) argument;
final INode node = (INode)codeGenArgument.getArgument();
final String cid = node.getUniqueName();
List<? extends IConnection> inConns = node.getIncomingConnections(EConnectionType.FLOW_MAIN);
List< ? extends IConnection> outConns = node.getOutgoingConnections();
IMetadataTable inputMetadataTable = null;
IMetadataTable outputMetadataTable = null;
String inConnName = "";
String inConnTypeName = "";
String outConnName = "";
String outConnTypeName = "";
List<IMetadataColumn> outputColumns = null;
List<IMetadataColumn> inputColumns = null;
StringBuilder pipeline = new StringBuilder("");
if (inConns.size() == 1){
    inConnName = inConns.get(0).getName();
    inputMetadataTable = inConns.get(0).getMetadataTable();
    inConnTypeName = codeGenArgument.getRecordStructName(inConns.get(0));
    inputColumns = inputMetadataTable.getListColumns();
}
if (outConns.size() == 1){
    outConnName = outConns.get(0).getName();
    outputMetadataTable = outConns.get(0).getMetadataTable();
    outConnTypeName = codeGenArgument.getRecordStructName(outConns.get(0));
    outputColumns = outputMetadataTable.getListColumns();
}

    stringBuffer.append(TEXT_1);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_2);
    stringBuffer.append(outputColumns.size());
    stringBuffer.append(TEXT_3);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_4);
    stringBuffer.append(inConnTypeName);
    stringBuffer.append(TEXT_5);
    stringBuffer.append(outConnTypeName);
    stringBuffer.append(TEXT_6);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_7);
    stringBuffer.append(outConnTypeName);
    stringBuffer.append(TEXT_8);
    stringBuffer.append(inConnTypeName);
    stringBuffer.append(TEXT_9);
    stringBuffer.append(TEXT_10);
    stringBuffer.append(outConnTypeName);
    stringBuffer.append(TEXT_11);
    stringBuffer.append(outConnTypeName);
    stringBuffer.append(TEXT_12);
    
        for(int i = 0; i < inputColumns.size(); i++) {
            IMetadataColumn column = inputColumns.get(i);
            String colLabel=column.getLabel();
        
    stringBuffer.append(TEXT_13);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_14);
    stringBuffer.append(colLabel);
    stringBuffer.append(TEXT_15);
    
          }
        
    stringBuffer.append(TEXT_16);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_17);
    return stringBuffer.toString();
  }
}
