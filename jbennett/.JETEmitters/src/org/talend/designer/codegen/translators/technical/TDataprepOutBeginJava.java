package org.talend.designer.codegen.translators.technical;

import org.talend.core.model.process.INode;
import org.talend.designer.codegen.config.CodeGeneratorArgument;
import org.talend.core.model.process.ElementParameterParser;
import org.talend.core.model.metadata.IMetadataTable;
import org.talend.core.model.metadata.IMetadataColumn;
import org.talend.core.model.process.IConnection;
import org.talend.core.model.process.IConnectionCategory;
import java.util.List;
import org.talend.core.model.metadata.types.JavaType;
import org.talend.core.model.metadata.types.JavaTypesManager;

public class TDataprepOutBeginJava
{
  protected static String nl;
  public static synchronized TDataprepOutBeginJava create(String lineSeparator)
  {
    nl = lineSeparator;
    TDataprepOutBeginJava result = new TDataprepOutBeginJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "";
  protected final String TEXT_2 = NL;
  protected final String TEXT_3 = NL;
  protected final String TEXT_4 = NL;
  protected final String TEXT_5 = NL;
  protected final String TEXT_6 = " " + NL + "\tfinal String decryptedPassword_";
  protected final String TEXT_7 = " = routines.system.PasswordEncryptUtil.decryptPassword(";
  protected final String TEXT_8 = ");";
  protected final String TEXT_9 = NL + "\tfinal String decryptedPassword_";
  protected final String TEXT_10 = " = ";
  protected final String TEXT_11 = "; ";
  protected final String TEXT_12 = NL + NL + "String apiurl_";
  protected final String TEXT_13 = " = ";
  protected final String TEXT_14 = ";" + NL + "" + NL + "// Getting authentication tokent" + NL + "final String encoding_";
  protected final String TEXT_15 = " = \"UTF-8\";" + NL + "org.apache.http.client.fluent.Request request_";
  protected final String TEXT_16 = " = org.apache.http.client.fluent.Request.Post(apiurl_";
  protected final String TEXT_17 = "+\"/login?username=\"+java.net.URLEncoder.encode(";
  protected final String TEXT_18 = ", encoding_";
  protected final String TEXT_19 = ")+\"&password=\"+java.net.URLEncoder.encode(decryptedPassword_";
  protected final String TEXT_20 = ", encoding_";
  protected final String TEXT_21 = ")+\"&client-app=studio\");" + NL + "org.apache.http.HttpResponse response_";
  protected final String TEXT_22 = " = request_";
  protected final String TEXT_23 = ".execute().returnResponse();" + NL + "org.talend.http.HttpUtil.handHttpResponse(response_";
  protected final String TEXT_24 = ");" + NL + "final org.apache.http.Header authorisationHeader_";
  protected final String TEXT_25 = " = response_";
  protected final String TEXT_26 = ".getFirstHeader(\"Authorization\");" + NL + "resourceMap.put(\"apiurl_";
  protected final String TEXT_27 = "\", apiurl_";
  protected final String TEXT_28 = ");" + NL + "resourceMap.put(\"authorisationHeader_";
  protected final String TEXT_29 = "\", authorisationHeader_";
  protected final String TEXT_30 = ");" + NL;
  protected final String TEXT_31 = NL;
  protected final String TEXT_32 = NL + NL + "String \tpreparationId_";
  protected final String TEXT_33 = " = null;" + NL + "String \tpreparationVersionID_";
  protected final String TEXT_34 = " = null;";
  protected final String TEXT_35 = NL + "\t\t//-- Get preparation id using the preparation full path" + NL + "\t\tString \tprepIdByPathURL_";
  protected final String TEXT_36 = " = apiurl_";
  protected final String TEXT_37 = "+\"/api/preparations?path=\"+java.net.URLEncoder.encode(";
  protected final String TEXT_38 = ", encoding_";
  protected final String TEXT_39 = ")+\"&client-app=studio\";" + NL + "\t\torg.apache.http.client.fluent.Request reqGetPrepIdByPath_";
  protected final String TEXT_40 = " = org.apache.http.client.fluent.Request.Get(prepIdByPathURL_";
  protected final String TEXT_41 = ");" + NL + "\t\treqGetPrepIdByPath_";
  protected final String TEXT_42 = ".addHeader(\"Authorization\",authorisationHeader_";
  protected final String TEXT_43 = ".getValue());" + NL + "\t\torg.apache.http.HttpResponse reqGetPrepIdByPathResponse_";
  protected final String TEXT_44 = " = reqGetPrepIdByPath_";
  protected final String TEXT_45 = ".execute().returnResponse();" + NL + "\t\tif (reqGetPrepIdByPathResponse_";
  protected final String TEXT_46 = ".getStatusLine().getStatusCode() != 200) {" + NL + "\t\t\tthrow new org.apache.http.client.HttpResponseException(reqGetPrepIdByPathResponse_";
  protected final String TEXT_47 = ".getStatusLine().getStatusCode(), " + NL + "\t\t\t\treqGetPrepIdByPathResponse_";
  protected final String TEXT_48 = ".getStatusLine().getReasonPhrase());" + NL + "\t\t}" + NL + "\t\torg.apache.http.HttpEntity entity_";
  protected final String TEXT_49 = " = reqGetPrepIdByPathResponse_";
  protected final String TEXT_50 = ".getEntity();" + NL + "\t\tif (entity_";
  protected final String TEXT_51 = " == null || entity_";
  protected final String TEXT_52 = ".getContent() == null) {" + NL + "            throw new org.apache.http.client.ClientProtocolException(\"Dataprep response contains no content\");" + NL + "        }" + NL + "        final com.fasterxml.jackson.databind.JsonNode prepInfo_";
  protected final String TEXT_53 = " = new com.fasterxml.jackson.databind.ObjectMapper().readTree(entity_";
  protected final String TEXT_54 = ".getContent());" + NL + "        if(prepInfo_";
  protected final String TEXT_55 = " == null || prepInfo_";
  protected final String TEXT_56 = ".get(0) == null " + NL + "        \t|| !prepInfo_";
  protected final String TEXT_57 = ".get(0).has(\"id\") || prepInfo_";
  protected final String TEXT_58 = ".get(0).get(\"id\").textValue() == null || prepInfo_";
  protected final String TEXT_59 = ".get(0).get(\"id\").textValue().isEmpty()){" + NL + "        \t" + NL + "        \tthrow new RuntimeException(\"No preparation was found at \\\"\"+";
  protected final String TEXT_60 = "+\"\\\". Please check that the preparation exist at the indicated path.\");" + NL + "        }" + NL + "        preparationId_";
  protected final String TEXT_61 = " = prepInfo_";
  protected final String TEXT_62 = ".get(0).get(\"id\").textValue();" + NL + "        " + NL + "        ";
  protected final String TEXT_63 = " " + NL + "\t    \t//-- Get preparation version id using version name" + NL + "\t    \tif(";
  protected final String TEXT_64 = " != null " + NL + "\t    \t    &&  !";
  protected final String TEXT_65 = ".isEmpty() " + NL + "\t    \t\t&&  !\"\\\"\\\"\".equals(";
  protected final String TEXT_66 = ")" + NL + "\t    \t\t&&  !\"head\".equals(";
  protected final String TEXT_67 = ")" + NL + "\t    \t\t&&  !\"current state\".equals(";
  protected final String TEXT_68 = ".toLowerCase())){" + NL + "\t\t        String \tprepVersionsById_";
  protected final String TEXT_69 = " = apiurl_";
  protected final String TEXT_70 = "+\"/api/preparations/\"+preparationId_";
  protected final String TEXT_71 = "+\"/versions?client-app=studio\";" + NL + "\t\t\t\torg.apache.http.client.fluent.Request reqGetPrepVersionsById_";
  protected final String TEXT_72 = " = org.apache.http.client.fluent.Request.Get(prepVersionsById_";
  protected final String TEXT_73 = ");" + NL + "\t\t\t\treqGetPrepVersionsById_";
  protected final String TEXT_74 = ".addHeader(\"Authorization\",authorisationHeader_";
  protected final String TEXT_75 = ".getValue());" + NL + "\t\t\t\torg.apache.http.HttpResponse reqGetPrepVersionsByIdResponse_";
  protected final String TEXT_76 = " = reqGetPrepVersionsById_";
  protected final String TEXT_77 = ".execute().returnResponse();" + NL + "\t\t\t\tif (reqGetPrepVersionsByIdResponse_";
  protected final String TEXT_78 = ".getStatusLine().getStatusCode() != 200) {" + NL + "\t\t\t\t\tthrow new org.apache.http.client.HttpResponseException(reqGetPrepVersionsByIdResponse_";
  protected final String TEXT_79 = ".getStatusLine().getStatusCode(), " + NL + "\t\t\t\t\t\treqGetPrepVersionsByIdResponse_";
  protected final String TEXT_80 = ".getStatusLine().getReasonPhrase());" + NL + "\t\t\t\t}" + NL + "\t\t\t\tif (reqGetPrepVersionsByIdResponse_";
  protected final String TEXT_81 = ".getEntity() == null || reqGetPrepVersionsByIdResponse_";
  protected final String TEXT_82 = ".getEntity().getContent() == null) {" + NL + "\t\t            throw new org.apache.http.client.ClientProtocolException(\"Dataprep response contains no content\");" + NL + "\t\t        }" + NL + "\t\t        final com.fasterxml.jackson.databind.JsonNode prepVersions_";
  protected final String TEXT_83 = " = new com.fasterxml.jackson.databind.ObjectMapper().readTree(reqGetPrepVersionsByIdResponse_";
  protected final String TEXT_84 = ".getEntity().getContent());" + NL + "\t\t        if(prepVersions_";
  protected final String TEXT_85 = " != null && prepVersions_";
  protected final String TEXT_86 = ".size() != 0){" + NL + "\t\t        \tjava.util.Iterator<com.fasterxml.jackson.databind.JsonNode> versionsIT_";
  protected final String TEXT_87 = " = prepVersions_";
  protected final String TEXT_88 = ".iterator();" + NL + "\t\t        \twhile(versionsIT_";
  protected final String TEXT_89 = ".hasNext()){" + NL + "\t\t        \t\tcom.fasterxml.jackson.databind.JsonNode v_";
  protected final String TEXT_90 = " = versionsIT_";
  protected final String TEXT_91 = ".next();" + NL + "\t\t        \t\tif(v_";
  protected final String TEXT_92 = ".has(\"name\")  && v_";
  protected final String TEXT_93 = ".get(\"name\").textValue() != null && !v_";
  protected final String TEXT_94 = ".get(\"name\").textValue().isEmpty()" + NL + "\t\t        \t\t\t&& ";
  protected final String TEXT_95 = ".equals(v_";
  protected final String TEXT_96 = ".get(\"name\").textValue())" + NL + "\t\t        \t\t\t&& v_";
  protected final String TEXT_97 = ".has(\"id\")  && v_";
  protected final String TEXT_98 = ".get(\"id\").textValue() != null && !v_";
  protected final String TEXT_99 = ".get(\"id\").textValue().isEmpty()){" + NL + "\t\t        \t\t\t" + NL + "\t\t        \t\t\tpreparationVersionID_";
  protected final String TEXT_100 = " = v_";
  protected final String TEXT_101 = ".get(\"id\").textValue();" + NL + "\t\t        \t\t\tbreak;" + NL + "\t\t        \t\t}" + NL + "\t\t        \t}" + NL + "\t\t        }" + NL + "\t\t        " + NL + "\t\t        if(preparationVersionID_";
  protected final String TEXT_102 = " == null){" + NL + "\t\t        \tthrow new RuntimeException(\"Preparation successfully  found at \\\"\"+";
  protected final String TEXT_103 = "+\"\\\", but no version \\\"\" + ";
  protected final String TEXT_104 = "+\"\\\" was found. Please check that your preparation has the indicated version.\");" + NL + "\t\t        }" + NL + "\t        }" + NL + "\t   ";
  protected final String TEXT_105 = NL + "\t   \t";
  protected final String TEXT_106 = NL + "\tpreparationId_";
  protected final String TEXT_107 = " = ";
  protected final String TEXT_108 = ";" + NL + "\tpreparationVersionID_";
  protected final String TEXT_109 = " = \"";
  protected final String TEXT_110 = "\";";
  protected final String TEXT_111 = NL + NL + "//Check the preparation ID" + NL + "if(preparationId_";
  protected final String TEXT_112 = " == null || preparationId_";
  protected final String TEXT_113 = ".isEmpty()){" + NL + "\tthrow new RuntimeException(\"Preparation id can't be null\");" + NL + "}" + NL + "" + NL + "" + NL + "//Set the preparation version id to \"head\"  if no version was provided in the parameters" + NL + "if(preparationVersionID_";
  protected final String TEXT_114 = " == null || preparationVersionID_";
  protected final String TEXT_115 = ".isEmpty()){ " + NL + "\tpreparationVersionID_";
  protected final String TEXT_116 = " = \"head\";" + NL + "} " + NL + "" + NL + "final String prepContentURL_";
  protected final String TEXT_117 = " = apiurl_";
  protected final String TEXT_118 = "+\"/api/preparations/\"+ preparationId_";
  protected final String TEXT_119 = " +\"/versions/\"+ preparationVersionID_";
  protected final String TEXT_120 = " +\"/content\";" + NL + "final String runId_";
  protected final String TEXT_121 = " = java.util.UUID.randomUUID().toString(); // run id to track the batch execution." + NL + "final java.net.URL url_";
  protected final String TEXT_122 = " = new java.net.URL(prepContentURL_";
  protected final String TEXT_123 = "+\"?runId=\"+runId_";
  protected final String TEXT_124 = ");" + NL + "" + NL + "/**" + NL + "* Send/Recieved records to data prep server and publish the result to the preparedRecordsQueue Queue" + NL + "**/" + NL + "class RequestsRunner_";
  protected final String TEXT_125 = " implements Runnable{" + NL + "" + NL + "\tprivate static final int OUTPUT_BUFFER_SIZE = 4096;" + NL + "\t" + NL + "\tprivate final java.io.File incommingRecords;" + NL + "\tprivate final java.util.concurrent.BlockingQueue<com.fasterxml.jackson.databind.JsonNode> preparedRecordsQueue;" + NL + "\tprivate final String authorizationToken;" + NL + "\tprivate final java.net.URL url;" + NL + "\tprivate final com.fasterxml.jackson.databind.JsonNode endNode;" + NL + "\tprivate final com.fasterxml.jackson.databind.ObjectMapper objectMapper;" + NL + "\t" + NL + "\tpublic RequestsRunner_";
  protected final String TEXT_126 = "(final java.io.File incommingRecords," + NL + "\t\tfinal java.util.concurrent.BlockingQueue<com.fasterxml.jackson.databind.JsonNode> preparedRecordsQueue," + NL + "\t\tfinal java.net.URL url," + NL + "\t\tfinal String authorizationToken) {" + NL + "\t    " + NL + "\t    super();" + NL + "\t    this.incommingRecords = incommingRecords;" + NL + "\t    this.preparedRecordsQueue = preparedRecordsQueue;" + NL + "\t    this.authorizationToken = authorizationToken;" + NL + "\t    this.url = url;" + NL + "\t\tthis.objectMapper = new com.fasterxml.jackson.databind.ObjectMapper();" + NL + "\t    this.endNode = objectMapper.createObjectNode().put(\"jobResult\", \"done\");" + NL + "\t}" + NL + "\t" + NL + "\t @Override" + NL + "    public void run() {" + NL + "        " + NL + "        final java.nio.charset.Charset charset = java.nio.charset.Charset.forName(encoding_";
  protected final String TEXT_127 = ");" + NL + "        " + NL + "\t\tint recordsCount = 0;" + NL + "\t\tjava.net.HttpURLConnection httpc = null;" + NL + "\t\ttry(java.io.BufferedReader br = new java.io.BufferedReader(new java.io.FileReader(this.incommingRecords))) {" + NL + "\t\t\t" + NL + "\t\t    httpc = newHttpConnection();" + NL + "\t\t\tjava.io.OutputStream out = httpc.getOutputStream();" + NL + "\t\t    java.io.OutputStreamWriter osw = new java.io.OutputStreamWriter(out, charset);" + NL + "\t\t\tfor(String record; (record = br.readLine()) != null;) {" + NL + "\t\t\t\tif(record != null && !record.isEmpty()){\t\t\t\t" + NL + "\t\t\t\t\tif(recordsCount == 0){" + NL + "\t\t\t\t\t\tosw.write(\"{\\\"records\\\":[\");" + NL + "\t\t\t\t\t}else{" + NL + "\t\t\t\t\t\tosw.write(\",\");" + NL + "\t\t\t\t\t}" + NL + "\t\t\t\t\tosw.write(record);" + NL + "\t\t\t\t\trecordsCount++;" + NL + "\t\t\t\t}" + NL + "\t\t\t\t" + NL + "\t\t\t\tif(recordsCount > 10000){" + NL + "\t\t\t\t\tosw.write(\"]}\");" + NL + "\t\t\t\t\tosw.close();" + NL + "\t\t\t\t\t" + NL + "\t    \t\t\tfinal java.io.InputStream in = httpc.getInputStream();" + NL + "\t    \t\t\thandleResponse(httpc.getResponseCode(), in, httpc.getErrorStream());" + NL + "\t    \t\t\tin.close();" + NL + "\t    \t\t\tout.close();\t\t\t\t\t" + NL + "\t\t\t\t\t" + NL + "\t\t\t\t\trecordsCount = 0;" + NL + "\t\t\t\t    httpc = newHttpConnection();" + NL + "\t\t\t\t    out = httpc.getOutputStream();" + NL + "\t\t    \t    osw = new java.io.OutputStreamWriter(out, charset); \t\t\t\t" + NL + "\t\t\t\t}\t" + NL + "\t    \t}" + NL + "\t\t\t" + NL + "\t\t\tif(recordsCount != 0){" + NL + "\t\t\t\tosw.write(\"]}\");" + NL + "\t\t\t\tosw.close();" + NL + "\t\t\t\tfinal java.io.InputStream in = httpc.getInputStream();\t" + NL + "\t    \t\thandleResponse(httpc.getResponseCode(), in, httpc.getErrorStream());" + NL + "\t    \t\tin.close();" + NL + "\t    \t\tout.close();       \t\t" + NL + "        \t}" + NL + "\t\t\t\t    \t" + NL + "\t\t}catch (java.io.IOException e) {\t\t\t\t" + NL + "\t\t\tSystem.err.println(\"Unexpected error: \"+ e);" + NL + "\t\t} " + NL + "\t\t" + NL + "\t\t//At this point we offer the endNode to notify the receiver to stop waiting for records" + NL + "\t\tthis.preparedRecordsQueue.offer(this.endNode);" + NL + "\t\t" + NL + "\t\ttry{" + NL + "\t\t\tincommingRecords.delete();" + NL + "\t\t}catch(Exception e){" + NL + "\t\t\t//Clean records tmp file quietly" + NL + "\t\t}" + NL + "    }" + NL + "    " + NL + "    private java.net.HttpURLConnection newHttpConnection() throws IOException{" + NL + "    " + NL + "    \t\tjava.net.HttpURLConnection httpc = (java.net.HttpURLConnection) this.url.openConnection();" + NL + "\t\t    httpc.setRequestMethod(\"POST\");" + NL + "\t\t    httpc.setRequestProperty(\"Authorization\", this.authorizationToken);" + NL + "\t\t    httpc.setRequestProperty(\"Content-Type\", \"application/json\");" + NL + "\t\t    httpc.setRequestProperty(\"Accept\", \"application/json, text/plain\");" + NL + "\t\t    httpc.setRequestProperty(\"Transfer-Encoding\", \"chunked\");" + NL + "\t\t    httpc.setRequestProperty(\"keep-alive\", \"true\");" + NL + "\t\t    httpc.setDoOutput(true);" + NL + "\t\t    " + NL + "\t\t    return httpc;" + NL + "    }" + NL + "    " + NL + "    private void handleResponse(final int respCode, final java.io.InputStream in, java.io.InputStream errorStream){" + NL + "\t    " + NL + "\t    try {  " + NL + "\t\t    final com.fasterxml.jackson.databind.JsonNode rootNode = this.objectMapper.readTree(in);" + NL + "\t\t\tfinal com.fasterxml.jackson.databind.JsonNode recordsNode = rootNode.path(\"records\");    " + NL + "\t\t    this.preparedRecordsQueue.add(rootNode.path(\"records\"));" + NL + "\t\t" + NL + "\t\t} catch (final java.io.IOException e) {" + NL + "\t\t    try {" + NL + "\t\t\t\tfinal byte[] buf = new byte[OUTPUT_BUFFER_SIZE];" + NL + "\t\t\t\tfinal java.io.ByteArrayOutputStream response = new java.io.ByteArrayOutputStream();" + NL + "\t\t\t\tint l=0;" + NL + "\t\t\t\twhile (-1 != (l = errorStream.read(buf))) {" + NL + "\t\t\t\t    response.write(buf, 0, l);" + NL + "\t\t\t\t}" + NL + "\t\t\t\terrorStream.close(); // close the errorstream" + NL + "\t\t\t\tSystem.err.println(\"ERROR[\" + respCode + \"]: \" + new String(response.toByteArray()));" + NL + "\t\t    } catch (final IOException ex) {" + NL + "\t\t\t\tSystem.err.println(\"ERROR: while reading http reponse error, Cause: \"+ex.getLocalizedMessage());" + NL + "\t\t    }" + NL + "\t\t}    " + NL + "    }" + NL + "}" + NL + "" + NL + "final java.io.File incommingRecords_";
  protected final String TEXT_128 = " = java.io.File.createTempFile(\"incommingRecords_\"+System.currentTimeMillis(), \"tmp\");" + NL + "final java.io.PrintWriter incommingRecordsWriter_";
  protected final String TEXT_129 = " = new java.io.PrintWriter(new java.io.BufferedWriter(new java.io.FileWriter(incommingRecords_";
  protected final String TEXT_130 = ", true)));" + NL + "final com.fasterxml.jackson.databind.ObjectMapper objectMapper_";
  protected final String TEXT_131 = " = new com.fasterxml.jackson.databind.ObjectMapper();" + NL + "final java.util.Map<String,String> recordMap_";
  protected final String TEXT_132 = " = new java.util.HashMap<>();";

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    stringBuffer.append(TEXT_1);
    
CodeGeneratorArgument codeGenArgument = (CodeGeneratorArgument) argument;
INode node = (INode)codeGenArgument.getArgument();
String cid = node.getUniqueName();
List<? extends IConnection> incomingConnections = node.getIncomingConnections();

if((incomingConnections==null)&&(incomingConnections.isEmpty())) {
	return stringBuffer.toString();
}

IConnection inputConn = null;
for(IConnection incomingConnection : incomingConnections) {
	if(incomingConnection.getLineStyle().hasConnectionCategory(IConnectionCategory.DATA)) {
		inputConn = incomingConnection;
	}
}

if(inputConn==null) {
	return stringBuffer.toString();
}

List<IMetadataTable> metadatas = node.getMetadataList();
if ((metadatas==null)||(metadatas.isEmpty())) {
	return stringBuffer.toString();
}

IMetadataTable metadata = metadatas.get(0);

if(metadata == null) {
	return stringBuffer.toString();
}

    stringBuffer.append(TEXT_2);
    stringBuffer.append(TEXT_3);
    //shared between DI and Spark components
    stringBuffer.append(TEXT_4);
    
String 	url = ElementParameterParser.getValue(node, "__URL__");
String 	username = ElementParameterParser.getValue(node, "__USERNAME__");
String 	passwordFieldName = "__PASSWORD__";

    stringBuffer.append(TEXT_5);
    if (ElementParameterParser.canEncrypt(node, passwordFieldName)) {
    stringBuffer.append(TEXT_6);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_7);
    stringBuffer.append(ElementParameterParser.getEncryptedValue(node, passwordFieldName));
    stringBuffer.append(TEXT_8);
    } else {
    stringBuffer.append(TEXT_9);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_10);
    stringBuffer.append( ElementParameterParser.getValue(node, passwordFieldName));
    stringBuffer.append(TEXT_11);
    }
    stringBuffer.append(TEXT_12);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_13);
    stringBuffer.append(url);
    stringBuffer.append(TEXT_14);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_15);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_16);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_17);
    stringBuffer.append(username );
    stringBuffer.append(TEXT_18);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_19);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_20);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_21);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_22);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_23);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_24);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_25);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_26);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_27);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_28);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_29);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_30);
    stringBuffer.append(TEXT_31);
    //shared between DI and Spark components
    stringBuffer.append(TEXT_32);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_33);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_34);
    
//Get the preparation ID and the version ID according to the preparation selection mode (id or path)
boolean usePrepPath = ElementParameterParser.getBooleanValue(node,"__USE_PREP_PATH__");
if(usePrepPath){
	String prepPath = ElementParameterParser.getValue(node,"__PREPARATION_PATH__");
	String prepPathVersionName = ElementParameterParser.getValue(node,"__PREPARATION_VERSION__");

    stringBuffer.append(TEXT_35);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_36);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_37);
    stringBuffer.append(prepPath );
    stringBuffer.append(TEXT_38);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_39);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_40);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_41);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_42);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_43);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_44);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_45);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_46);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_47);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_48);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_49);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_50);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_51);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_52);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_53);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_54);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_55);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_56);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_57);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_58);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_59);
    stringBuffer.append(prepPath );
    stringBuffer.append(TEXT_60);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_61);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_62);
     if(prepPathVersionName != null && !prepPathVersionName.isEmpty()) { // START of IF Get Version ID using Version Name 
    stringBuffer.append(TEXT_63);
    stringBuffer.append(prepPathVersionName );
    stringBuffer.append(TEXT_64);
    stringBuffer.append(prepPathVersionName );
    stringBuffer.append(TEXT_65);
    stringBuffer.append(prepPathVersionName );
    stringBuffer.append(TEXT_66);
    stringBuffer.append(prepPathVersionName );
    stringBuffer.append(TEXT_67);
    stringBuffer.append(prepPathVersionName );
    stringBuffer.append(TEXT_68);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_69);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_70);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_71);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_72);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_73);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_74);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_75);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_76);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_77);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_78);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_79);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_80);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_81);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_82);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_83);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_84);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_85);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_86);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_87);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_88);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_89);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_90);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_91);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_92);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_93);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_94);
    stringBuffer.append(prepPathVersionName );
    stringBuffer.append(TEXT_95);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_96);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_97);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_98);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_99);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_100);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_101);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_102);
    stringBuffer.append(prepPath );
    stringBuffer.append(TEXT_103);
    stringBuffer.append(prepPathVersionName );
    stringBuffer.append(TEXT_104);
     }	// END of IF Get Version ID using Version Name 
    stringBuffer.append(TEXT_105);
    }else{ 
    stringBuffer.append(TEXT_106);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_107);
    stringBuffer.append(ElementParameterParser.getValue(node, "__PREPARATION_ID__"));
    stringBuffer.append(TEXT_108);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_109);
    stringBuffer.append(ElementParameterParser.getValue(node, "__VERSION_ID__"));
    stringBuffer.append(TEXT_110);
    }
    stringBuffer.append(TEXT_111);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_112);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_113);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_114);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_115);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_116);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_117);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_118);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_119);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_120);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_121);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_122);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_123);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_124);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_125);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_126);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_127);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_128);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_129);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_130);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_131);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_132);
    return stringBuffer.toString();
  }
}
