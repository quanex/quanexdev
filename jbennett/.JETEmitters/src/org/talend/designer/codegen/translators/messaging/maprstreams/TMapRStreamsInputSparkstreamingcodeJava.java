package org.talend.designer.codegen.translators.messaging.maprstreams;

import org.talend.core.model.process.INode;
import org.talend.designer.common.BigDataCodeGeneratorArgument;
import org.talend.designer.common.tmaprstreamsinput.TMapRStreamsInputUtil;
import org.talend.hadoop.distribution.kafka.SparkStreamingKafkaVersion;

public class TMapRStreamsInputSparkstreamingcodeJava
{
  protected static String nl;
  public static synchronized TMapRStreamsInputSparkstreamingcodeJava create(String lineSeparator)
  {
    nl = lineSeparator;
    TMapRStreamsInputSparkstreamingcodeJava result = new TMapRStreamsInputSparkstreamingcodeJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "\t";
  protected final String TEXT_2 = NL + NL + "public static class ";
  protected final String TEXT_3 = "_ValueDeserializer implements org.apache.kafka.common.serialization.Deserializer<";
  protected final String TEXT_4 = "> {" + NL + "" + NL + "\tprivate org.apache.kafka.common.serialization.StringDeserializer stringDeserializer;" + NL + "\t" + NL + "\tpublic ";
  protected final String TEXT_5 = "_ValueDeserializer() {" + NL + "\t\t// empty" + NL + "\t}" + NL + "" + NL + "\tpublic void configure(java.util.Map<java.lang.String,?> configs, boolean isKey) {" + NL + "\t\tstringDeserializer = new org.apache.kafka.common.serialization.StringDeserializer();" + NL + "\t\tstringDeserializer.configure(configs, isKey);" + NL + "\t}" + NL + "" + NL + "\tpublic ";
  protected final String TEXT_6 = " deserialize(String topic, byte[] data) {" + NL + "\t\t";
  protected final String TEXT_7 = " result = new ";
  protected final String TEXT_8 = "();";
  protected final String TEXT_9 = NL + "\t\t    String line = stringDeserializer.deserialize(topic, data);" + NL + "\t\t    result.";
  protected final String TEXT_10 = " = line;";
  protected final String TEXT_11 = NL + "\t\t    result.";
  protected final String TEXT_12 = " = java.nio.ByteBuffer.wrap(data);";
  protected final String TEXT_13 = NL + "\t\treturn result;" + NL + "    }" + NL + "" + NL + "\tpublic void close() {" + NL + "\t\t// nothing" + NL + "\t}" + NL + "}" + NL + "" + NL + "public static class ";
  protected final String TEXT_14 = "_KeyDeserializer implements org.apache.kafka.common.serialization.Deserializer<org.apache.hadoop.io.NullWritable> {" + NL + "" + NL + "\tpublic ";
  protected final String TEXT_15 = "_KeyDeserializer() {" + NL + "\t\t// empty" + NL + "\t}" + NL + "\t" + NL + "\tpublic void configure(java.util.Map<java.lang.String,?> configs, boolean isKey) {" + NL + "\t\t// nothing" + NL + "\t}" + NL + "" + NL + "\tpublic org.apache.hadoop.io.NullWritable deserialize(String topic, byte[] data) {" + NL + "\t    return org.apache.hadoop.io.NullWritable.get();" + NL + "\t}" + NL + "" + NL + "\tpublic void close() {" + NL + "\t\t// nothing" + NL + "\t}" + NL + "}";
  protected final String TEXT_16 = NL + "\t";
  protected final String TEXT_17 = NL + NL + "public static class ";
  protected final String TEXT_18 = "_ValueDeserializer implements org.apache.kafka.common.serialization.Deserializer<";
  protected final String TEXT_19 = "> {" + NL + "" + NL + "\tprivate org.apache.kafka.common.serialization.StringDeserializer stringDeserializer;" + NL + "\t" + NL + "\tpublic ";
  protected final String TEXT_20 = "_ValueDeserializer() {" + NL + "\t\t// empty" + NL + "\t}" + NL + "" + NL + "\tpublic void configure(java.util.Map<java.lang.String,?> configs, boolean isKey) {" + NL + "\t\tstringDeserializer = new org.apache.kafka.common.serialization.StringDeserializer();" + NL + "\t\tstringDeserializer.configure(configs, isKey);" + NL + "\t}" + NL + "" + NL + "\tpublic ";
  protected final String TEXT_21 = " deserialize(String topic, byte[] data) {" + NL + "\t\t";
  protected final String TEXT_22 = " result = new ";
  protected final String TEXT_23 = "();";
  protected final String TEXT_24 = NL + "\t\t    String line = stringDeserializer.deserialize(topic, data);" + NL + "\t\t    result.";
  protected final String TEXT_25 = " = line;";
  protected final String TEXT_26 = NL + "\t\t    result.";
  protected final String TEXT_27 = " = java.nio.ByteBuffer.wrap(data);";
  protected final String TEXT_28 = NL + "\t\treturn result;" + NL + "    }" + NL + "" + NL + "\tpublic void close() {" + NL + "\t\t// nothing" + NL + "\t}" + NL + "}" + NL + "" + NL + "public static class ";
  protected final String TEXT_29 = "_KeyDeserializer implements org.apache.kafka.common.serialization.Deserializer<org.apache.hadoop.io.NullWritable> {" + NL + "" + NL + "\tpublic ";
  protected final String TEXT_30 = "_KeyDeserializer() {" + NL + "\t\t// empty" + NL + "\t}" + NL + "\t" + NL + "\tpublic void configure(java.util.Map<java.lang.String,?> configs, boolean isKey) {" + NL + "\t\t// nothing" + NL + "\t}" + NL + "" + NL + "\tpublic org.apache.hadoop.io.NullWritable deserialize(String topic, byte[] data) {" + NL + "\t    return org.apache.hadoop.io.NullWritable.get();" + NL + "\t}" + NL + "" + NL + "\tpublic void close() {" + NL + "\t\t// nothing" + NL + "\t}" + NL + "}" + NL + "" + NL + "public static class ";
  protected final String TEXT_31 = "_Function implements org.apache.spark.api.java.function.PairFunction<org.apache.kafka.clients.consumer.ConsumerRecord<NullWritable, ";
  protected final String TEXT_32 = ">, NullWritable, ";
  protected final String TEXT_33 = "> {" + NL + "" + NL + "\tpublic ";
  protected final String TEXT_34 = "_Function() {" + NL + "\t\t// empty" + NL + "\t}" + NL + "\t" + NL + "\t@Override" + NL + "\tpublic scala.Tuple2<NullWritable, ";
  protected final String TEXT_35 = "> call(org.apache.kafka.clients.consumer.ConsumerRecord<NullWritable, ";
  protected final String TEXT_36 = "> record) {" + NL + "\t\treturn new scala.Tuple2(record.key(), record.value());" + NL + "\t}" + NL + "" + NL + "}";
  protected final String TEXT_37 = NL;

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    
BigDataCodeGeneratorArgument codeGenArgument = (BigDataCodeGeneratorArgument) argument;
INode node = (INode)codeGenArgument.getArgument();
String cid = node.getUniqueName();

TMapRStreamsInputUtil tMapRStreamsInputUtil = new TMapRStreamsInputUtil(node);
SparkStreamingKafkaVersion sparkStreamingKafkaVersion = tMapRStreamsInputUtil.getSparkStreamingKafkaVersion();

    
if (SparkStreamingKafkaVersion.MAPR_5X0_KAFKA.equals(sparkStreamingKafkaVersion)) {
	// Special case only for some MapR distributions 

    stringBuffer.append(TEXT_1);
    
String outStructName = codeGenArgument.getRecordStructName(tMapRStreamsInputUtil.getOutgoingConnection());

    stringBuffer.append(TEXT_2);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_3);
    stringBuffer.append(outStructName);
    stringBuffer.append(TEXT_4);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_5);
    stringBuffer.append(outStructName);
    stringBuffer.append(TEXT_6);
    stringBuffer.append(outStructName);
    stringBuffer.append(TEXT_7);
    stringBuffer.append(outStructName);
    stringBuffer.append(TEXT_8);
    
		if("STRING".equals(tMapRStreamsInputUtil.getOutputType())) {

    stringBuffer.append(TEXT_9);
    stringBuffer.append(tMapRStreamsInputUtil.getOutgoingColumnName());
    stringBuffer.append(TEXT_10);
    
		} else if("BYTES".equals(tMapRStreamsInputUtil.getOutputType())) {

    stringBuffer.append(TEXT_11);
    stringBuffer.append(tMapRStreamsInputUtil.getOutgoingColumnName());
    stringBuffer.append(TEXT_12);
    
		}

    stringBuffer.append(TEXT_13);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_14);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_15);
    
} else if (SparkStreamingKafkaVersion.MAPR_600_KAFKA.equals(sparkStreamingKafkaVersion)) {

    stringBuffer.append(TEXT_16);
    
String outStructName = codeGenArgument.getRecordStructName(tMapRStreamsInputUtil.getOutgoingConnection());

    stringBuffer.append(TEXT_17);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_18);
    stringBuffer.append(outStructName);
    stringBuffer.append(TEXT_19);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_20);
    stringBuffer.append(outStructName);
    stringBuffer.append(TEXT_21);
    stringBuffer.append(outStructName);
    stringBuffer.append(TEXT_22);
    stringBuffer.append(outStructName);
    stringBuffer.append(TEXT_23);
    
		if("STRING".equals(tMapRStreamsInputUtil.getOutputType())) {

    stringBuffer.append(TEXT_24);
    stringBuffer.append(tMapRStreamsInputUtil.getOutgoingColumnName());
    stringBuffer.append(TEXT_25);
    
		} else if("BYTES".equals(tMapRStreamsInputUtil.getOutputType())) {

    stringBuffer.append(TEXT_26);
    stringBuffer.append(tMapRStreamsInputUtil.getOutgoingColumnName());
    stringBuffer.append(TEXT_27);
    
		}

    stringBuffer.append(TEXT_28);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_29);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_30);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_31);
    stringBuffer.append(outStructName);
    stringBuffer.append(TEXT_32);
    stringBuffer.append(outStructName);
    stringBuffer.append(TEXT_33);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_34);
    stringBuffer.append(outStructName);
    stringBuffer.append(TEXT_35);
    stringBuffer.append(outStructName);
    stringBuffer.append(TEXT_36);
    
} 

    stringBuffer.append(TEXT_37);
    return stringBuffer.toString();
  }
}
