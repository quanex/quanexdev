package org.talend.designer.codegen.translators.technical;

import org.talend.core.model.process.INode;
import org.talend.designer.codegen.config.CodeGeneratorArgument;
import java.util.List;
import org.talend.core.model.metadata.IMetadataTable;
import org.talend.core.model.process.EConnectionType;
import org.talend.core.model.utils.NodeUtil;
import org.talend.core.model.process.IConnection;

public class TDataprepOutFinallyJava
{
  protected static String nl;
  public static synchronized TDataprepOutFinallyJava create(String lineSeparator)
  {
    nl = lineSeparator;
    TDataprepOutFinallyJava result = new TDataprepOutFinallyJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "";
  protected final String TEXT_2 = NL + NL + "final Object authorisationHeader_";
  protected final String TEXT_3 = " = resourceMap.get(\"authorisationHeader_";
  protected final String TEXT_4 = "\");" + NL + "if(authorisationHeader_";
  protected final String TEXT_5 = "!=null){//have login, need to logout" + NL + "    org.apache.http.client.fluent.Request logout_";
  protected final String TEXT_6 = " = org.apache.http.client.fluent.Request.Post((String)resourceMap.get(\"apiurl_";
  protected final String TEXT_7 = "\")+\"/logout?client-app=studio\").addHeader((org.apache.http.Header)authorisationHeader_";
  protected final String TEXT_8 = ");" + NL + "    " + NL + "    org.apache.http.HttpResponse response_";
  protected final String TEXT_9 = " = logout_";
  protected final String TEXT_10 = ".execute().returnResponse();" + NL + "    " + NL + "    org.talend.http.HttpUtil.handHttpResponse(response_";
  protected final String TEXT_11 = ");" + NL + "}";

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    stringBuffer.append(TEXT_1);
    
CodeGeneratorArgument codeGenArgument = (CodeGeneratorArgument) argument;
INode node = (INode)codeGenArgument.getArgument();
String cid = node.getUniqueName();

    stringBuffer.append(TEXT_2);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_3);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_4);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_5);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_6);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_7);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_8);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_9);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_10);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_11);
    return stringBuffer.toString();
  }
}
