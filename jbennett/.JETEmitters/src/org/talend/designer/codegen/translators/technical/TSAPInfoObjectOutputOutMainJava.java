package org.talend.designer.codegen.translators.technical;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.talend.core.model.metadata.IMetadataColumn;
import org.talend.core.model.metadata.IMetadataTable;
import org.talend.core.model.metadata.types.JavaType;
import org.talend.core.model.metadata.types.JavaTypesManager;
import org.talend.core.model.process.ElementParameterParser;
import org.talend.core.model.process.IConnection;
import org.talend.core.model.process.IConnectionCategory;
import org.talend.core.model.process.INode;
import org.talend.designer.codegen.config.CodeGeneratorArgument;
import org.talend.core.model.utils.TalendTextUtils;

public class TSAPInfoObjectOutputOutMainJava
{
  protected static String nl;
  public static synchronized TSAPInfoObjectOutputOutMainJava create(String lineSeparator)
  {
    nl = lineSeparator;
    TSAPInfoObjectOutputOutMainJava result = new TSAPInfoObjectOutputOutMainJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "";
  protected final String TEXT_2 = NL + "\tdata_";
  protected final String TEXT_3 = ".appendRow();";
  protected final String TEXT_4 = "   \t\t\t\t" + NL + "\t\t    if(";
  protected final String TEXT_5 = ".";
  protected final String TEXT_6 = " != null) {";
  protected final String TEXT_7 = NL + "\t\t\tdata_";
  protected final String TEXT_8 = ".setString(";
  protected final String TEXT_9 = ", ";
  protected final String TEXT_10 = ".";
  protected final String TEXT_11 = ");";
  protected final String TEXT_12 = NL + "\t\t\tdata_";
  protected final String TEXT_13 = ".setInteger(";
  protected final String TEXT_14 = ", ";
  protected final String TEXT_15 = ".";
  protected final String TEXT_16 = ");";
  protected final String TEXT_17 = NL + "\t\t\tdata_";
  protected final String TEXT_18 = ".setLong(";
  protected final String TEXT_19 = ", ";
  protected final String TEXT_20 = ".";
  protected final String TEXT_21 = ");";
  protected final String TEXT_22 = NL + "\t\t\tdata_";
  protected final String TEXT_23 = ".setShort(";
  protected final String TEXT_24 = ", ";
  protected final String TEXT_25 = ".";
  protected final String TEXT_26 = ");";
  protected final String TEXT_27 = NL + "\t\t\tdata_";
  protected final String TEXT_28 = ".setDate(";
  protected final String TEXT_29 = ", ";
  protected final String TEXT_30 = ".";
  protected final String TEXT_31 = ");";
  protected final String TEXT_32 = NL + "\t\t\tdata_";
  protected final String TEXT_33 = ".setByte(";
  protected final String TEXT_34 = ", ";
  protected final String TEXT_35 = ".";
  protected final String TEXT_36 = ");";
  protected final String TEXT_37 = NL + "\t\t\tdata_";
  protected final String TEXT_38 = ".setDouble(";
  protected final String TEXT_39 = ", ";
  protected final String TEXT_40 = ".";
  protected final String TEXT_41 = ");";
  protected final String TEXT_42 = NL + "\t\t\tdata_";
  protected final String TEXT_43 = ".setFloat(";
  protected final String TEXT_44 = ", ";
  protected final String TEXT_45 = ".";
  protected final String TEXT_46 = ");";
  protected final String TEXT_47 = NL + "\t\t\tdata_";
  protected final String TEXT_48 = ".setBigDecimal(";
  protected final String TEXT_49 = ", ";
  protected final String TEXT_50 = ".";
  protected final String TEXT_51 = ");";
  protected final String TEXT_52 = NL + "\t\t\tdata_";
  protected final String TEXT_53 = ".setBigInteger(";
  protected final String TEXT_54 = ", ";
  protected final String TEXT_55 = ".";
  protected final String TEXT_56 = ");";
  protected final String TEXT_57 = NL + "\t\t\tdata_";
  protected final String TEXT_58 = ".setString(";
  protected final String TEXT_59 = ", String.valueOf(";
  protected final String TEXT_60 = ".";
  protected final String TEXT_61 = "));";
  protected final String TEXT_62 = "   \t\t\t\t" + NL + "\t\t    }";

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    stringBuffer.append(TEXT_1);
    
	CodeGeneratorArgument codeGenArgument = (CodeGeneratorArgument) argument;
	INode node = (INode)codeGenArgument.getArgument();
	String cid = node.getUniqueName();
	
	List<? extends IConnection> inputConnections = node.getIncomingConnections();
	if((inputConnections == null) || (inputConnections.size() == 0)) {
		return "";
	}
	
	IConnection inputConnection = null;
	for(IConnection inputConn : inputConnections) {
		if(inputConn.getLineStyle().hasConnectionCategory(IConnectionCategory.DATA)) {
			inputConnection = inputConn;
			break;
		}
	}
	
	if(inputConnection == null) {
		return "";
	}
	
	List<IMetadataTable> metadatas = node.getMetadataList();
	if ((metadatas == null) && (metadatas.size() == 0) || (metadatas.get(0) == null)) {
		return "";
	}
	IMetadataTable metadata = metadatas.get(0);
	
	List<IMetadataColumn> columnList = metadata.getListColumns();
	if((columnList == null) || (columnList.size() == 0)) {
		return "";
	}

    stringBuffer.append(TEXT_2);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_3);
    
	for(int i=0;i<columnList.size();i++) {
		IMetadataColumn column = columnList.get(i);
	    String typeToGenerate = JavaTypesManager.getTypeToGenerate(column.getTalendType(),column.isNullable());
	    JavaType javaType = JavaTypesManager.getJavaTypeFromId(column.getTalendType());
	    String dbType = column.getType();
	    
		boolean isPrimitive = JavaTypesManager.isJavaPrimitiveType( javaType, column.isNullable());
		if(!isPrimitive) {

    stringBuffer.append(TEXT_4);
    stringBuffer.append(inputConnection.getName() );
    stringBuffer.append(TEXT_5);
    stringBuffer.append(column.getLabel() );
    stringBuffer.append(TEXT_6);
    
		} 
		if(javaType == JavaTypesManager.STRING) {

    stringBuffer.append(TEXT_7);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_8);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_9);
    stringBuffer.append(inputConnection.getName() );
    stringBuffer.append(TEXT_10);
    stringBuffer.append(column.getLabel() );
    stringBuffer.append(TEXT_11);
    
		} else if(javaType == JavaTypesManager.INTEGER) {

    stringBuffer.append(TEXT_12);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_13);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_14);
    stringBuffer.append(inputConnection.getName() );
    stringBuffer.append(TEXT_15);
    stringBuffer.append(column.getLabel() );
    stringBuffer.append(TEXT_16);
    
		} else if(javaType == JavaTypesManager.LONG) {

    stringBuffer.append(TEXT_17);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_18);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_19);
    stringBuffer.append(inputConnection.getName() );
    stringBuffer.append(TEXT_20);
    stringBuffer.append(column.getLabel() );
    stringBuffer.append(TEXT_21);
    
		} else if(javaType == JavaTypesManager.SHORT) {

    stringBuffer.append(TEXT_22);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_23);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_24);
    stringBuffer.append(inputConnection.getName() );
    stringBuffer.append(TEXT_25);
    stringBuffer.append(column.getLabel() );
    stringBuffer.append(TEXT_26);
    
		} else if(javaType == JavaTypesManager.DATE) {

    stringBuffer.append(TEXT_27);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_28);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_29);
    stringBuffer.append(inputConnection.getName() );
    stringBuffer.append(TEXT_30);
    stringBuffer.append(column.getLabel() );
    stringBuffer.append(TEXT_31);
    
		} else if(javaType == JavaTypesManager.BYTE) {

    stringBuffer.append(TEXT_32);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_33);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_34);
    stringBuffer.append(inputConnection.getName() );
    stringBuffer.append(TEXT_35);
    stringBuffer.append(column.getLabel() );
    stringBuffer.append(TEXT_36);
    
		} else if(javaType == JavaTypesManager.DOUBLE) {

    stringBuffer.append(TEXT_37);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_38);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_39);
    stringBuffer.append(inputConnection.getName() );
    stringBuffer.append(TEXT_40);
    stringBuffer.append(column.getLabel() );
    stringBuffer.append(TEXT_41);
    
		} else if(javaType == JavaTypesManager.FLOAT) {

    stringBuffer.append(TEXT_42);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_43);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_44);
    stringBuffer.append(inputConnection.getName() );
    stringBuffer.append(TEXT_45);
    stringBuffer.append(column.getLabel() );
    stringBuffer.append(TEXT_46);
    
		} else if(javaType == JavaTypesManager.BIGDECIMAL) {

    stringBuffer.append(TEXT_47);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_48);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_49);
    stringBuffer.append(inputConnection.getName() );
    stringBuffer.append(TEXT_50);
    stringBuffer.append(column.getLabel() );
    stringBuffer.append(TEXT_51);
    
		} else if(javaType == JavaTypesManager.OBJECT && "BIG_INTEGER".equals(dbType)) {

    stringBuffer.append(TEXT_52);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_53);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_54);
    stringBuffer.append(inputConnection.getName() );
    stringBuffer.append(TEXT_55);
    stringBuffer.append(column.getLabel() );
    stringBuffer.append(TEXT_56);
    
		} else {

    stringBuffer.append(TEXT_57);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_58);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_59);
    stringBuffer.append(inputConnection.getName() );
    stringBuffer.append(TEXT_60);
    stringBuffer.append(column.getLabel() );
    stringBuffer.append(TEXT_61);
    
		}
		if(!isPrimitive) {

    stringBuffer.append(TEXT_62);
    
		} 
	}

    return stringBuffer.toString();
  }
}
