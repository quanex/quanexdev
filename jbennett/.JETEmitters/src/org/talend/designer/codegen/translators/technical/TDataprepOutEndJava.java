package org.talend.designer.codegen.translators.technical;

import org.talend.core.model.process.INode;
import org.talend.designer.codegen.config.CodeGeneratorArgument;
import org.talend.core.model.process.ElementParameterParser;
import org.talend.core.model.metadata.IMetadataTable;
import org.talend.core.model.metadata.IMetadataColumn;
import org.talend.core.model.process.IConnection;
import org.talend.core.model.process.IConnectionCategory;
import java.util.List;
import org.talend.core.model.metadata.types.JavaType;
import org.talend.core.model.metadata.types.JavaTypesManager;

public class TDataprepOutEndJava
{
  protected static String nl;
  public static synchronized TDataprepOutEndJava create(String lineSeparator)
  {
    nl = lineSeparator;
    TDataprepOutEndJava result = new TDataprepOutEndJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "";
  protected final String TEXT_2 = NL + NL + NL + "incommingRecordsWriter_";
  protected final String TEXT_3 = ".close();" + NL + "" + NL + "final java.util.concurrent.BlockingQueue<com.fasterxml.jackson.databind.JsonNode> preparedRecordsQueue_";
  protected final String TEXT_4 = " = new java.util.concurrent.LinkedBlockingQueue<>();" + NL + "RequestsRunner_";
  protected final String TEXT_5 = " requestsRunner_";
  protected final String TEXT_6 = " = new RequestsRunner_";
  protected final String TEXT_7 = "(incommingRecords_";
  protected final String TEXT_8 = ", " + NL + "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tpreparedRecordsQueue_";
  protected final String TEXT_9 = ", " + NL + "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\turl_";
  protected final String TEXT_10 = "," + NL + "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tauthorisationHeader_";
  protected final String TEXT_11 = ".getValue());\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t" + NL + "Thread requestsRunnerThread_";
  protected final String TEXT_12 = " = new Thread(requestsRunner_";
  protected final String TEXT_13 = ", \"Thread-TDP-Runner\");" + NL + "requestsRunnerThread_";
  protected final String TEXT_14 = ".start();";

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    stringBuffer.append(TEXT_1);
    
CodeGeneratorArgument codeGenArgument = (CodeGeneratorArgument) argument;
INode node = (INode)codeGenArgument.getArgument();
String cid = node.getUniqueName();
List<? extends IConnection> incomingConnections = node.getIncomingConnections();

if((incomingConnections==null)&&(incomingConnections.isEmpty())) {
	return stringBuffer.toString();
}

IConnection inputConn = null;
for(IConnection incomingConnection : incomingConnections) {
	if(incomingConnection.getLineStyle().hasConnectionCategory(IConnectionCategory.DATA)) {
		inputConn = incomingConnection;
	}
}

if(inputConn==null) {
	return stringBuffer.toString();
}

List<IMetadataTable> metadatas = node.getMetadataList();
if ((metadatas==null)||(metadatas.isEmpty())) {
	return stringBuffer.toString();
}

IMetadataTable metadata = metadatas.get(0);

if(metadata == null) {
	return stringBuffer.toString();
}


String uname = ElementParameterParser.getValue(node, "__UNAME__");

    stringBuffer.append(TEXT_2);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_3);
    stringBuffer.append(uname);
    stringBuffer.append(TEXT_4);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_5);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_6);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_7);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_8);
    stringBuffer.append(uname);
    stringBuffer.append(TEXT_9);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_10);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_11);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_12);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_13);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_14);
    return stringBuffer.toString();
  }
}
