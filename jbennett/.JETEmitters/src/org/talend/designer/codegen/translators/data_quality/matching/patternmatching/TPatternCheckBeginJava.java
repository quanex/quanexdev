package org.talend.designer.codegen.translators.data_quality.matching.patternmatching;

import org.talend.core.model.process.INode;
import org.talend.core.model.process.ElementParameterParser;
import org.talend.designer.codegen.config.CodeGeneratorArgument;
import org.talend.core.model.process.IConnection;
import org.talend.core.model.metadata.IMetadataTable;
import java.util.List;

public class TPatternCheckBeginJava
{
  protected static String nl;
  public static synchronized TPatternCheckBeginJava create(String lineSeparator)
  {
    nl = lineSeparator;
    TPatternCheckBeginJava result = new TPatternCheckBeginJava();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "";
  protected final String TEXT_2 = NL + "    String stringpattern_";
  protected final String TEXT_3 = "=";
  protected final String TEXT_4 = ";";
  protected final String TEXT_5 = NL + NL + "\tboolean result_";
  protected final String TEXT_6 = "=false;\t" + NL + "\tint nb_line_";
  protected final String TEXT_7 = " = 0;" + NL + "\tint nb_line_ok_";
  protected final String TEXT_8 = " = 0;" + NL + "\tint nb_line_reject_";
  protected final String TEXT_9 = " = 0;";
  protected final String TEXT_10 = NL + "\tjava.util.regex.Pattern pattern_";
  protected final String TEXT_11 = " = null;" + NL + "\ttry {" + NL + "    \tpattern_";
  protected final String TEXT_12 = " = java.util.regex.Pattern.compile(stringpattern_";
  protected final String TEXT_13 = ");" + NL + "    } catch (java.util.regex.PatternSyntaxException pe_";
  protected final String TEXT_14 = ") {" + NL + "    \tSystem.err.println(\"Error in component tPatternCheck: the pattern defined contains errors\"); " + NL + "        throw pe_";
  protected final String TEXT_15 = ";" + NL + "   \t}";
  protected final String TEXT_16 = NL;

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    stringBuffer.append(TEXT_1);
    
CodeGeneratorArgument codeGenArgument = (CodeGeneratorArgument) argument;
INode node = (INode)codeGenArgument.getArgument();
String cid = node.getUniqueName();

List<IMetadataTable> metadatas = node.getMetadataList();
//may be dq pattern
String PatternList = ElementParameterParser.getValue(node, "__PATTERN_REGEX__");

    stringBuffer.append(TEXT_2);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_3);
    stringBuffer.append(PatternList.replace("\\\\", "\\").replace("\\", "\\\\"));
    stringBuffer.append(TEXT_4);
     

if ((metadatas!=null)&&(metadatas.size()>0)) {
    IMetadataTable metadata = metadatas.get(0);
    if (metadata!=null) {
    	List<? extends IConnection> conns = node.getOutgoingSortedConnections();

    stringBuffer.append(TEXT_5);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_6);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_7);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_8);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_9);
    
		if (conns != null) {

    stringBuffer.append(TEXT_10);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_11);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_12);
    stringBuffer.append(cid );
    stringBuffer.append(TEXT_13);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_14);
    stringBuffer.append(cid);
    stringBuffer.append(TEXT_15);
    
		}
    }
}

    stringBuffer.append(TEXT_16);
    return stringBuffer.toString();
  }
}
