package routines;

import java.io.FileNotFoundException;
import java.util.Map;
import java.util.UUID;

/*
 * user specification: the function's comment should contain keys as follows: 1. write about the function's comment.but
 * it must be before the "{talendTypes}" key.
 * 
 * 2. {talendTypes} 's value must be talend Type, it is required . its value should be one of: String, char | Character,
 * long | Long, int | Integer, boolean | Boolean, byte | Byte, Date, double | Double, float | Float, Object, short |
 * Short
 * 
 * 3. {Category} define a category for the Function. it is required. its value is user-defined .
 * 
 * 4. {param} 's format is: {param} <type>[(<default value or closed list values>)] <name>[ : <comment>]
 * 
 * <type> 's value should be one of: string, int, list, double, object, boolean, long, char, date. <name>'s value is the
 * Function's parameter name. the {param} is optional. so if you the Function without the parameters. the {param} don't
 * added. you can have many parameters for the Function.
 * 
 * 5. {example} gives a example for the Function. it is optional.
 */
public class QuanexUtils {

    /**
     * helloExample: not return value, only print "hello" + message.
     * 
     * 
     * {talendTypes} String
     * 
     * {Category} User Defined
     * 
     * {param} string("world") input: The string need to be printed.
     * 
     * {example} helloExemple("world") # hello world !.
     */
    public static void helloExample(String message) {
        if (message == null) {
            message = "World"; //$NON-NLS-1$
        }
        System.out.println("Hello " + message + " !"); //$NON-NLS-1$ //$NON-NLS-2$
    }

    /**
     * helloExample: not return value, only print "hello" + message.
     * 
     * 
     * {talendTypes} String
     * 
     * {Category} User Defined
     * 
     * {param} string("world") input: The string need to be printed.
     * 
     * {example} helloExemple("world") # hello world !.
     */
    public static Map<String, Object> InitializeGlobalIntegers(Map<String, Object> aMap, Integer initValue, String ... vars) {
        for (String var : vars) {
            aMap.put(var, initValue);
        }
        return aMap;
    }

    public static Map<String, Object> InitializeGlobalStrings(Map<String, Object> aMap, String initValue, String ... vars) {
        for (String var : vars) {
            aMap.put(var, initValue);
        }
        return aMap;
    }

    public static Map<String, Object> InitializeGlobalBooleans(Map<String, Object> aMap, Boolean initValue, String ... vars) {
        for (String var : vars) {
            aMap.put(var, initValue);
        }
        return aMap;
    }

    public static Map<String, Object> QuanexInitializeProcess(String logDir,
                                                              String taskName,
                                                              String tempDir,
                                                              String nextAction,
                                                              Map<String,Object> globalMap) throws FileNotFoundException{
        Long jobStart = System.currentTimeMillis();
        String logTimeStamp = TalendDate.getDate("CCYYMMDDhhmmss");
        String logFile = logDir + "/" + taskName.trim().replace(" ", "_") + "_" + logTimeStamp + ".log";
        String uuid = UUID.randomUUID().toString().replace("-", "");
        String myTempDir = tempDir + uuid;
        String contextEmailSubject = "Initial context dump for " + taskName;
        String contextEmailMessage = "The attached file contains the initial context dump for " + taskName;

        QuanexStrings.quanexLog("redirecting output to " + logFile);

        // Output Logs to Console and to File
        java.io.File outputFile = new java.io.File(logFile);
        System.setOut(new java.io.PrintStream(new java.io.FileOutputStream(outputFile, true), true));

        /* long */
        globalMap.put("jobStart", jobStart);

        /* Non-NullString */
        globalMap.put("contextDumpEmailMessage", contextEmailMessage);
        globalMap.put("contextDumpEmailSubject", contextEmailSubject);
        globalMap.put("logFile", logFile);
        globalMap.put("logTimeStamp", logTimeStamp);
        globalMap.put("myAction", nextAction);
        if ( !QuanexStrings.isEmpty(tempDir)) {
            globalMap.put("tempDir", myTempDir);
        }
        globalMap.put("uuid", uuid);

        /* Booleans */
        globalMap.put("hasError", false);
        globalMap.put("jobStatus", false);
        globalMap.put("runMain", false);
        globalMap.put("tempCreated", false);
        return globalMap;
    }
}
